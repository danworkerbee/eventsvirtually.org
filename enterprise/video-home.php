<?php
/**
 * filename: home.php
 * description: this will be the template for the home page
 * author: Jullie Quijano
 * date created: 2014-03-25
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: Home
 */
global $wb_ent_options, $current_lang, $moretext, $curlang, $catlibrary;
$current_lang = get_locale();

get_header();

switch ($wb_ent_options['channelformat']) {
    case 'feature':
        $categoryId = $wb_ent_options['videocats']['feature'.$cur_lang];        
        break;
    case 'webcast':
        $categoryId = $wb_ent_options['videocats']['webcast'.$cur_lang];
        break;
    case 'library':
        $categoryId = $wb_ent_options['videocats']['library'.$cur_lang];
        break;
    case 'playlist':
        $categoryId = $wb_ent_options['videocats']['playlist'.$cur_lang];
        break;
}

$postId = wb_get_most_recent($categoryId);

if( trim($postId) == '' ){
   $postId = wb_get_most_recent($categoryId);   
}

if ( $postId == '' ){
	$postId = wb_get_most_recent($wb_ent_options['videocats']['library'.$cur_lang]);
}

$video = wb_get_post_details($postId);

if ($wb_ent_options['sharetype'] == 'floating' && $wb_ent_options['hasshare'] && function_exists(sharebar)) {
    sharebar();
}

$videoDesc = '';
if( $wb_ent_options['vidshortcode']['enabled'] ){
   $videoDesc = removeShortCode($wb_ent_options['vidshortcode']['tag'],$video['desc']);   
}
else{
   $videoDesc = $video['desc'];
}
?>
<div id="wb_ent_content" class="clearfix row-fluid">

    <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">



        <div class="row-fluid featured-post-div">
            <ul class="thumbnails">
                <?php 
                if($wb_ent_options['hascustfeatvideo']){
                $liclass= 'span12';
                $featuredStill =   $video['videoStill'];
                $imgstyle = 'height: auto;';
                }
                else{
                $liclass= 'span6';
                $featuredStill =   $video['largeThumb'];
                $imgstyle = 'width: 308px; height: 173px;';
                }
                ?>
                <li class="<?php echo $liclass; ?>">
                    <?php            
                    if($wb_ent_options['hascustfeatvideoplayer']){
            
                    
                    $miniplayer = true;
                    include ( get_template_directory() .'/includes/widgets/player.php');                    
                    
                      
                     }else{                 
                    ?> 
                    
                    <a href="<?php echo $video['postLink'] ?>"><img id="featuredStill" class="img-responsive" style="<?php echo $imgstyle; ?>" src="<?php echo $featuredStill; ?>" alt="<?php echo wb_format_string( $video['title'], false, true, 50, '...'); ?>" /></a>
                    <?php } ?>
                </li>
                <li class="<?php echo $liclass; ?>">
                    <a href="<?php echo $video['postLink'] ?>"><h3 class="h3-title"><?php echo wb_format_string( $video['title'], false, true, 50, '...'); ?></h3></a>
                    <p>
                       <?php 
                       
                        
                         
                       if( trim($videoDesc) != '' ){
                          echo wb_format_string( $videoDesc, false, true, 190, '...');
                           ?>
                          <a href="<?php echo $video['postLink'] ?>"><span class="link"><?php _e( 'more', 'enterprise' ); ?></span></a>
                          <?php                             
                       }                                            
                       ?> 
                    </p>
                    
                     <?php										 
												if($wb_ent_options['videolistinfo']['keywordlimit']){ $wb_keyword_limit = $wb_ent_options['videolistinfo']['keywordlimit']; }
												else { $wb_keyword_limit = 0; }
                            if ( is_array($video['tags']) && count($video['tags']) > 0 && $wb_keyword_limit != 0) {
                                ?>
                                <p class="tags">
                                    <span class="tags-title"><?php _e( '[Tags:]', 'enterprise' ); ?></span>                    
                                    <?php
                                    $count = 0;
                                    $tagTotal = count($video['tags']);
                                    
																		foreach ($video['tags'] as $currentTag) {
																			$count++;
																			if($count == $wb_ent_options["videolistinfo"]["keywordlimit"]){ ?>
																				<a rel="tag" href="/keyword/<?php echo $currentTag->slug; ?>" class="label"><?php echo $currentTag->name; ?><?php if($tagTotal > 1 && $count != $tagTotal){echo '<span class="hidden hidden_tag">,</span>';}?></a>
																				
																				<?php
																				} else if($count > $wb_ent_options["videolistinfo"]["keywordlimit"]){ ?>
																					<a  rel="tag" href="/keyword/<?php echo $currentTag->slug; ?>" class="label hidden_tag hidden"><?php echo $currentTag->name; ?><?php if($tagTotal > 1 && $count != $tagTotal){echo ',';}?></a>
																				<?php } else { ?>
																				<a rel="tag" href="/keyword/<?php echo $currentTag->slug; ?>" class="label"><?php echo $currentTag->name; ?><?php if($tagTotal > 1 && $count != $tagTotal){echo ',';}?></a>
																				<?php
																			}
																		}
																		if($count > $wb_ent_options["videolistinfo"]["keywordlimit"]){
																			?>
																			<a href="#" class="show_hidden_home_tags"><span class="link">[more]</span></a>
																			<script>
																				$(document).ready(function(event){
																					$(".show_hidden_home_tags").click(function(){
																						if($(".hidden_tag").is(':visible')){
																							$(".show_hidden_home_tags").text("[more]");																							
																						} else {
																							$(".show_hidden_home_tags").text("[less]");
																						}
																						$(".hidden_tag").toggleClass('hidden not_hidden');
																						event.stopPropagation();
																					});
																				});
																			</script>
																			<?php 
																		}
                                    $count = 0;
                                    ?>                                        
                                </p> 
                                <?php
                            }
                            ?>

                </li>
            </ul>
            <?php
            if( $wb_ent_options['homewidebanner']['enabled'] === true || $wb_ent_options['homewidebanner']['enabled'] == 'true' ){
                if( !is_array($analytics) ){
                    $analytics = $wb_ent_options['analytics'];
                }

                if ( trim($wb_ent_options['homewidebanner']['target']) != ''){
                   $target = $wb_ent_options['homewidebanner']['target'];
                }
                else{
                   $target = "_blank";
                }   
            
                $currentCustomAdCode = $wb_ent_options['homewidebanner']['customcode'];
                $currentAdImage = $wb_ent_options['homewidebanner']['image'];
                $currentOrderNumber = $wb_ent_options['homewidebanner']['ordernumber'];
                $currentAdURL = $wb_ent_options['homewidebanner']['url'];
                $currentCustomAdCode = $wb_ent_options['homewidebanner']['customcode'];

                if( trim($currentCustomAdCode) != '' ){
                    echo html_entity_decode($currentCustomAdCode);
                }
                else if ( trim($currentAdImage) != '') {

                   echo '
                      <div id="adActionDiv">
                         <script type="text/javascript">
                            ';
                            $imageFilenameTemp = explode('/', $currentAdImage);
                            $imageFilename = $imageFilenameTemp[ (count($imageFilenameTemp)- 1) ];
                            foreach ($analytics as $key=>$tracker) {
                                if( isset($currentAdOrderNumber) && trim($currentAdOrderNumber) != ''){                          
                                    echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner View\', \'' . $currentAdOrderNumber . '_468x60_' . $imageFilename . '\');';
                                } else {                            
                                    echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner View\', \'' . '468x60_' . $imageFilename . '\');';
                                }               
                            }
                            echo '
                         </script>			
                         <a target="'.$target.'" onclick="';
                            foreach ($analytics as $key=>$tracker) {
                               if( isset($currentAdOrderNumber) && trim($currentAdOrderNumber) != ''){
                                    echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner Click\', \'' . $currentAdOrderNumber . '_468x60_' . $imageFilename . '\');';
                                } else {
                                    echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner Click\', \'' . '468x60_' . $imageFilename . '\');';
                               }
                            }
                            echo '"'; 
                            echo ' href="'.$currentAdURL.'"';
                            echo '>';


                            $tempArray =  explode('.', $currentAdImage);
                            if( ($tempArray[count($tempArray)-1]) == 'swf' ){
                            ?>
                               <object width="468" height="60">
                                  <param name="movie" value="<?php echo $currentAdImage; ?>">
                                  <param name="wmode" value="transparent" />
                                  <embed wmode="transparent" src="<?php echo $currentAdImage; ?>" width="468" height="60">
                                  </embed>
                               </object>	
                            <?php
                            }
                            else{
                               echo '<img src="'.$currentAdImage.'" width="468" height="60" alt="'.$currentAdTitle.'" />';
                            }	

                            echo '</a>
                         <div style="clear:both; width:100%">&nbsp;</div>
                      </div>';
                }
            }
            
              
            
            if ($wb_ent_options['hasshare']) {
                if ($wb_ent_options['sharetype'] == 'floating' && function_exists(sharebar_horizontal)) {
                    sharebar_horizontal();
                    //echo '<br clear="all" />';
                }
                if( (is_user_logged_in() && current_user_can('upload_files')) ){
                	include ( get_template_directory() . '/includes/widgets/wb-new-share.php');
                }
            }
            
         
            
            
  
            ?>   
        </div>       

        <div style="clear:both;"></div>         
        
        <?php
        include ( get_template_directory() . '/includes/widgets/videoSearchWidget.php');
        ?>
    </div> <!-- end #wb_ent_main -->

    <?php get_sidebar(); // sidebar 1  ?>

</div> <!-- end content -->

<?php
get_footer();
?>
