<?php
/**
 * filename: wb-custom-login.php
 * description: custom login page for wordpress
 * author: Jullie Quijano
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: WB Custom Login Page
 */

global $wb_ent_options;
/*
 * custom redirect
 */
$loginRedirect = '/';
if( isset($_GET['redir']) && trim($_GET['redir'])!= '' ){
    $loginRedirect = $_GET['redir'];
}


/*
 * there will be modes of display
 *  -full page
 *  -form only
 *      $_GET['dmode'] = 'form'
 */
$displayMode = 'full';
if( isset($_GET['dmode']) && trim($_GET['dmode'])!= '' ){
    $displayMode = $_GET['dmode'];
}


//processing login
$errorMessage = '';
$successMessage = '';
if( trim($_POST['loginButton']) == "Login" ){
    $loginEmailAddress = sanitize_text_field($_POST['email']);
    $loginPassword = sanitize_text_field($_POST['pwd']);
    if( email_exists( $loginEmailAddress ) ){
        $userId = email_exists( $loginEmailAddress );
        $userData = get_userdata( $userId );
        $userLogin = $userData->user_login;
        
        $creds['user_login'] = $userLogin;
	$creds['user_password'] = $loginPassword;
        $userLoggedIn = wp_signon( $creds, false );
        if(is_wp_error($userLoggedIn)){
            $errorMessage = 'Invalid email address or incorrect password.';
        }         
    }
    elseif( !is_null(username_exists( $loginEmailAddress )) ){ 
        $creds['user_login'] = $loginEmailAddress;
	$creds['user_password'] = $loginPassword;
        $userLoggedIn = wp_signon( $creds, false );
        if(is_wp_error($userLoggedIn)){
            $errorMessage = 'Invalid email address or incorrect password.';
        }         
    }
    else{
        $errorMessage = 'Invalid email address or incorrect password.';
    }   
}


if( trim($_POST['recoverButton']) == "Reset Password" ){
    $loginEmailAddress = sanitize_text_field($_POST['email']);
    
    if( wb_retrieve_password($loginEmailAddress) === false ){
        $errorMessage = 'Invalid email address or username.';
    }
    else{
        $successMessage = 'Check your e-mail for the confirmation link. <a href="javascript: window.parent.Shadowbox.close();">Click here to close this box</a>';
    }

}

if( $displayMode == 'full' && ( is_user_logged_in() || ( trim($_POST['loginButton']) == "Login" && trim($errorMessage) == '' ) ) ){
    header ('location: '.$loginRedirect);
    exit;
}

if( $displayMode == 'full'){
    get_header();
}
else{
?>
<!doctype html>  
<!--[if IEMobile 7 ]> <html lang="en-US"class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html lang="en-US" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en-US" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-US" class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html lang="en-US" class="no-js"><!--<![endif]-->
    <head>
        <?php
        if( (trim($_POST['loginButton']) == "Login" && trim($errorMessage) == '' ) ){
        ?>
        <script type="text/javascript">
            window.parent.location.reload();
        </script>
        <?php
        }
        elseif(  is_user_logged_in() ){
        ?>
        <script type="text/javascript">
            window.parent.Shadowbox.close();
        </script>
        <?php            
        }
        ?>
        <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--
    put custom styling here for when it's not a full page    
    -->
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/library/js/responsive/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/library/js/responsive/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap3.3.4.min.css">

<!-- Latest compiled and minified JavaScript -->

    <style>
    #viewingTips label{padding:0;margin: 15px 0 5px;font-weight:normal;text-transform:uppercase;}
    #viewingTips .btnsubmit{margin: 20px 0;}
    #viewingTips .alert-danger{margin:0 0 5px 0;}
    #viewingTips h1{font-size: 28px;text-transform:uppercase;color:#337ab7;}
    #loginForm, #recoverForm{padding:0 5px;}
    .clear{clear:both;}
    </style>
    </head>
    <body>
<?php
} 
$recoverPasswordDisplay = 'style="display: none;"';
$loginFormDisplay = '';
if( trim($_POST['recoverButton']) == "Reset Password" ){
    $recoverPasswordDisplay = '';   
    $loginFormDisplay = 'style="display: none;"';
}
?>

        <div id="wb_ent_content" class="clearfix row-fluid">
            <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
                <div id="viewing-tips">                    
                    <div id="viewingTips">              
                        <form action="" method="post" id="loginForm" class="container-fluid" <?php echo $loginFormDisplay; ?>>
                            <h1 class="pagetitle col-lg-12">Login</h1>
                            <?php
                            if ( trim($_POST['loginButton']) == "Login" && trim($errorMessage) != '' ){
                                echo '<div class="alert alert-danger clear" role="alert">'.$errorMessage.'</div>';
                            }
                            elseif ( trim($_POST['loginButton']) == "Login" && is_wp_error($userLoggedIn) ){
                                echo '<div class="alert alert-danger clear" role="alert">'.$userLoggedIn->get_error_message().'</div>';
                            }
                            ?>                                    
                          <div class="container-fluid">
                              <label class="col-xs-12">Email Address:</label> <input name="email" type="text" class="col-xs-12"/>
                          </div>                          
                          <div class="container-fluid">
                              <label class="col-xs-12">Password: </label><input name="pwd" type="password" class="col-xs-12" />
                          </div>
                          <div class="container-fluid btnsubmit">
                            <input type="submit" name="loginButton" class="btn btn-info btn-login-custom col-xs-4" value="Login" />       
                            <a class="col-xs-6" href="javascript: void(0);" onclick="javascript: document.getElementById('recoverForm').style.display = 'block'; document.getElementById('loginForm').style.display = 'none';">
                                Lost your password?
                            </a>
                          </div>
                        </form>  
                        <form action="" method="post" id="recoverForm" <?php echo $recoverPasswordDisplay; ?> class="container-fluid">
                            <h1 class="pagetitle col-xs-12">Password Recovery</h1>
                            <p class="container-fluid">Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.</p>
                            <?php
                            if ( trim($_POST['recoverButton']) == "Reset Password" && trim($errorMessage) != '' ){
                                echo '<div class="alert alert-danger" role="alert">'.$errorMessage.'</div>';
                            }
                            elseif ( trim($_POST['recoverButton']) == "Reset Password" && trim($successMessage) != '' ){
                                echo '<div class="alert alert-success" role="alert">'.$successMessage.'</div>';
                            }
                            ?>                               
                          <div class="container-fluid">
                            <label class="col-xs-12">Username or email: </label><input name="email" type="text" class="col-xs-12" />
                          </div>           
                          <div class="container-fluid btnsubmit">
                            <input class="btn btn-default col-xs-4" type="submit" name="recoverButton" value="Reset Password" />     
                            <a class="col-xs-8" href="javascript: void(0);" onclick="javascript: document.getElementById('recoverForm').style.display = 'none'; document.getElementById('loginForm').style.display = 'block';">
                                Login with email address and password
                            </a>
                          </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php
if( $displayMode == 'full'){
    get_sidebar();
    get_footer();
}
else{
?>
    </body>
</html>            
<?php            
}

function wb_retrieve_password($user_login){
    global $wpdb, $wp_hasher;

    $user_login = sanitize_text_field($user_login);

    if ( empty( $user_login) ) {
        return false;
    } else if ( strpos( $user_login, '@' ) ) {
        $user_data = get_user_by( 'email', trim( $user_login ) );
        if ( empty( $user_data ) )
           return false;
    } else {
        $login = trim($user_login);
        $user_data = get_user_by('login', $login);
    }

    do_action('lostpassword_post');


    if ( !$user_data ) return false;

    // redefining user_login ensures we return the right case in the email
    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;

    do_action('retreive_password', $user_login);  // Misspelled and deprecated
    do_action('retrieve_password', $user_login);

    $allow = apply_filters('allow_password_reset', true, $user_data->ID);

    if ( ! $allow )
        return false;
    else if ( is_wp_error($allow) )
        return false;

    $key = wp_generate_password( 20, false );
    do_action( 'retrieve_password_key', $user_login, $key );

    if ( empty( $wp_hasher ) ) {
        require_once ABSPATH . 'wp-includes/class-phpass.php';
        $wp_hasher = new PasswordHash( 8, true );
    }
    $hashed = $wp_hasher->HashPassword( $key );
    $wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user_login ) );

    
    $message = ' 
        <!DOCTYPE html>
        <html>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>';
    
    $message .= sprintf(__('%s'), $blogname);
    
    $message .= '
        </title>
        </head>
        <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
        <div style=" background-color: #f5f5f5; width:100%; -webkit-text-size-adjust:none !important; margin:0; padding: 70px 0 70px 0; ">
                <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                <tr>
                        <td align="center" valign="top">
                                <div id="template_header_image">
                                </div>
                                <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container" style=" box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important; border-radius:6px !important; background-color: #fdfdfd; border: 1px solid #dcdcdc; border-radius:6px !important; ">
                                <tr>
                                        <td align="center" valign="top">
                                                <!-- Header -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header" style=" background-color: #557da1; color: #ffffff; border-top-left-radius:6px !important; border-top-right-radius:6px !important; border-bottom: 0; font-family:Arial; font-weight:bold; line-height:100%; vertical-align:middle; " bgcolor="#557da1">
                                                <tr>
                                                        <td>
                                                                <h1 style=" color: #ffffff; margin:0; padding: 28px 24px; text-shadow: 0 1px 0 #7797b4; display:block; font-family:Arial; font-size:30px; font-weight:bold; text-align:left; line-height: 150%; ">Password Reset Instructions</h1>
                                                        </td>
                                                </tr>
                                                </table>
                                                <!-- End Header -->
                                        </td>
                                </tr>
                                <tr>
                                        <td align="center" valign="top">
                                                <!-- Body -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
                                                <tr>
                                                        <td valign="top" style=" background-color: #fdfdfd; border-radius:6px !important; ">
                                                                <!-- Content -->
                                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                <tr>
                                                                        <td valign="top">
                                                                                <div style=" color: #737373; font-family:Arial; font-size:14px; line-height:150%; text-align:left; ">
                                                                                        <p>';
    $message .= __('Someone requested that the password be reset for the following account:');     
    $message .= '
                                                                                        </p>
                                                                                        <p>';
    
    $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
    $message .= '                                                                                                
                                                                                        </p>
                                                                                        <p>
                                                                                        ';
    $message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
    $message .= '                                                                                                
                                                                                        </p>
                                                                                        <p>
                                                                                        ';
    $message .= __('To reset your password, visit the following address:');
    $message .= '                                                                                                            
                                                                                        </p>
                                                                                        <p>
                                                                                            <a href="';
    
    $message .= network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login');
    $message .= '" style="color:#505050; font-weight:normal; text-decoration:underline">
        ';
    $message .= __('Click here to reset your password');
    $message .= '</a>
                                                                                        </p>
                                                                                        <p>
                                                                                        </p>
                                                                                </div>
                                                                        </td>
                                                                </tr>
                                                                </table>
                                                                <!--
        End Content -->
                                                        </td>
                                                </tr>
                                                </table>
                                                <!-- End Body
        -->
                                        </td>
                                </tr>
                                <tr>
                                        <td align="center" valign="top">
                                                <!-- Footer -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer" style=" border-top:0; -webkit-border-radius:6px; ">
                                                <tr>
                                                        <td valign="top">
                                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                                <tr>
                                                                        <td colspan="2" valign="middle" id="credit" style=" border:0; color: #99b1c7; font-family: Arial; font-size:12px; line-height:125%; text-align:center; ">
                                                                        </td>
                                                                </tr>
                                                                </table>
                                                        </td>
                                                </tr>
                                                </table>
                                                <!-- End Footer
        -->
                                        </td>
                                </tr>
                                </table>
                        </td>
                </tr>
                </table>
        </div>
        </body>
        </html>';    
    
    
    
    
    
    

    
    
    
    
    
    
    
    

    if ( is_multisite() )
        $blogname = $GLOBALS['current_site']->site_name;
    else
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

    $title = sprintf( __('[%s] Password Reset'), $blogname );

    $title = apply_filters('retrieve_password_title', $title);
    $message = apply_filters('retrieve_password_message', $message, $key);

    add_filter( 'wp_mail_content_type', 'set_html_content_type' );
    if ( $message && !wp_mail($user_email, $title, $message) )
        wp_die( __('The e-mail could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function...') );
    
    remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
    
}



function set_html_content_type() {
	return 'text/html';
}
?>