<?php
/**
 * filename: wb-sso-redirect-page.php
 * description: this template will be used for SSO with Impak when users are sent 
 *    to the client's login page and redirected back to the platform. This template
 *    requires a token saved in the $_COOKIE['token'] by Impak as well the url 
 *    to redirect users to. When sending users to the client's login page, the 
 *    ReturnUrl needs to be urlencoded for the redirection to work properly.
 *    example: https://www.schoolcounselor.org/login.aspx?ReturnUrl=https%3A%2F%2Fvideos.schoolcounselor.org%2Fredirect%3Fp%3Dhttps%3A%2F%2Fvideos.schoolcounselor.org%2Framp-scoring-rubric-webinar-section-1-vision-statement
 * author: Jullie Quijano
 * date created: 2018-01-03
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: SSO Redirect Page(Impak)
 */
$workerbee_security_token = ''; //impak provided security token
$impak_client_host = ''; //url for the client example asca.impakadvance.com
$user_role_to_add = ''; //user role to create/use for the members example asca_members
$user_role_label = ''; //nice name for user roles example ASCA Members
$default_password = ''; //default password to use when creating new user
$redirectLink = $_GET['p'];

global $wb_ent_options;

if( trim($_COOKIE['token']) != '' ){
  $user_token = trim($_COOKIE['token']);
  
  //authenticate token
  $curlstring = 'https://'.$impak_client_host.'/higher_logic_service/service.asmx/ValidateAuthenticationToken';
  $fields = array(
    'strSecurityKey' => $workerbee_security_token,
    'strToken' => $user_token,   
  );
  foreach ($fields as $key => $value) {
    $fields_string .= $key . '=' . $value . '&';
  }
  rtrim($fields_string, '&');

  $ch = curl_init();
  $timeout = 0; // set to zero for no timeout
  curl_setopt($ch, CURLOPT_URL, $curlstring);  
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
  curl_setopt($ch, CURLOPT_POST, count($fields));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
  $user_status_id = curl_exec($ch);
  curl_close($ch);
 
  $xml_parser = xml_parser_create();
  xml_parse_into_struct($xml_parser, $user_status_id, $xml_parse_values, $index);
  xml_parser_free($xml_parser);
  
  //echo "\r\n".'$xml_parse_values is '.print_r($xml_parse_values, true);

  $user_status_key = array_search('STATUS_ID', array_column($xml_parse_values, 'tag'));  
  $user_status_id = $xml_parse_values[$user_status_key]['value'];
  
  //get user info  
  $curlstring2 = 'https://'.$impak_client_host.'/higher_logic_service/service.asmx/GetMemberInfo';
  $fields2 = array(
    'strSecurityKey' => $workerbee_security_token,
    'dblStatus_Id' => $user_status_id,   
  );
  foreach ($fields2 as $key => $value) {
    $fields_string2 .= $key . '=' . $value . '&';
  }
  rtrim($fields_string2, '&');

  $ch = curl_init();
  $timeout = 0; // set to zero for no timeout
  curl_setopt($ch, CURLOPT_URL, $curlstring2);  
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
  curl_setopt($ch, CURLOPT_POST, count($fields2));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string2);
  $user_member_info = curl_exec($ch);
  curl_close($ch);
  //echo "\r\n".'$user_member_info is '.$user_member_info;
  
  $xml_parser2 = xml_parser_create();
  xml_parse_into_struct($xml_parser2, $user_member_info, $xml_member_info_parse_values, $index);
  xml_parser_free($xml_parser2);
  
  //echo "\r\n".'$xml_member_info_parse_values is '.print_r($xml_member_info_parse_values, true);
  
  //get required values
  $user_email_key = array_search('EMAIL', array_column($xml_member_info_parse_values, 'tag'));        
  $user_email = $xml_member_info_parse_values[$user_email_key]['value'];
  
  $user_first_name_key = array_search('FIRST_NAME', array_column($xml_member_info_parse_values, 'tag'));        
  $user_first_name = $xml_member_info_parse_values[$user_first_name_key]['value'];

  $user_last_name_key = array_search('LAST_NAME', array_column($xml_member_info_parse_values, 'tag'));        
  $user_last_name = $xml_member_info_parse_values[$user_last_name_key]['value'];
  
  /*
   * Other search
   * ADDRESS_LINE_1
   * ADDRESS_CITY
   * ADDRESS_STATE_CODE
   * ADDRESS_ZIP
   * NUMBER
   */
  
  if (trim($user_email) != '') {
    //echo "\r\n".'email not blank';

    if (!function_exists('get_role')) {
      //echo "\r\n".'function get_role does not exist';
    } else {
      //echo "\r\n".'function get_role exists';
    }
    
    $getRoleValue = get_role($user_role_to_add);
    //echo "\r\n".'$getRoleValue is ' . print_r($getRoleValue, true);

    if (!is_object($getRoleValue)) {
      //echo "\r\n".'create new role';
      global $wp_roles;
      if (!isset($wp_roles)) {
        $wp_roles = new WP_Roles();
      }

      $custRole = $wp_roles->get_role('subscriber');
      //echo "\r\n".'$custRole is ' . print_r($custRole, true);

      $wp_roles->add_role($user_role_to_add, $user_role_label, $custRole->capabilities);
    }

    $emailAddress = trim($user_email);

    $emailExists = email_exists($user_email);
    //echo "\r\n".'$emailExists is ' . $emailExists;
    $usernameExists = username_exists($emailAddress);
    //echo "\r\n".'$usernameExists is ' . $usernameExists;

    //check if user exists
    if ($emailExists || $usernameExists) {

      $userId = (is_numeric($emailExists)) ? $emailExists : ( (is_numeric($emailAddress)) ? $emailAddress : 0 );
      $user_info = get_userdata($userId);
      //echo "\r\n".'$user_info is ' . print_r($user_info, true);

      wp_set_auth_cookie($userId, true);
    }
    else {
      $createNewUser = true;
      $userLoggedIn = false;
      //echo "\r\n".'user does not exist';
    }
    
    if ($createNewUser) {
      //echo "\r\n".'creating new user';

      $userdata = array(
          'user_login' => $emailAddress,
          'user_email' => $emailAddress,
          'first_name' => $user_first_name,
          'last_name' => $user_last_name,
          'role' => $user_role_to_add,
          'user_pass' => $default_password
      );
      //echo "\r\n".'$userdata is ' . print_r($userdata, true);

      $user_id = wp_insert_user($userdata);
      //echo "\r\n".'$user_id is ' . print_r($user_id, true);

      if (is_wp_error($user_id)) {
        $displayMessage = true;
        $messageText = 'Please enter a valid email address.';
        $userLoggedIn = false;
      } else {
        $creds = array();
        $creds['user_login'] = $emailAddress;
        $creds['user_password'] = $default_password;
        $creds['remember'] = true;
        //echo "\r\n".'$creds last is ' . print_r($creds, true);

        $user = wp_signon($creds, true);
        //echo "\r\n".'$user is ' . print_r($user, true);

        if (is_wp_error($user)) {
          $createNewUser = true;
          $userLoggedIn = false;
          $displayMessage = true;
          $messageText = 'Please enter a valid email address.';
          //echo "\r\n".'wp_error ';
        } else {
          $userLoggedIn = true;
          //echo "\r\n".'no error ';
        }
      }
    }
    
    if (is_user_logged_in()) {
      //echo "\r\n".'user_is logged in ';
      if (trim($redirectLink) != '') {
      header('location: ' . $redirectLink);
      } else {
      header('location: /');
      }
    }
    else{
      //echo "\r\n".'user_is NOT logged in ';
      if (trim($redirectLink) != '') {
      header('location: ' . $redirectLink);
      } else {
      header('location: /');
      }
    }

    exit;
  }
}
else{
header('location: /');
}
?>