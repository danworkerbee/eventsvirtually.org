<?php

include("../../includes/config.php");

$unique_id = CHANNEL_UNIQUE_ID ; //get this from the channel-embed-popup.js
$clientName = CLIENT_NAME;
$embedChannelSite = CHANNEL_SITE;


$embed_code = '<!-- Start of '.$clientName.' Embed Widget code -->
<div id="'.$unique_id.'"><script type="text/javascript">var ptc = (("https:" == document.location.protocol) ? "https://" : "http://"); document.write(unescape("%3Cscript src=\'" + ptc + "'.$embedChannelSite.'/embed-widgets/popup/channel-embed-popup.js\' type=\'text/javascript\'%3E%3C/script%3E"));</script></div><!-- End of '.$clientName.' Embed Widget -->';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
	<head>
		<title><?php echo $clientName; ?> Embed Widget</title>
		<link rel="shortcut icon" href="favicon.ico" />
	</head>
		
	<body>
		<div style="width: 500px; margin: 20px auto;">
			<?php echo $embed_code; ?>
			<br />
	
			<textarea style="width: 500px; height: 120px; font-size: 12px; color: rgb(51, 51, 51);" id="embedCodeInput"><?php echo $embed_code; ?></textarea>
		</div>
	</body>
	
</html>

<?php
// embed widget for channels
// external site usage
// channelID = xxx
// wrapper = 300x250 !important
?>
