<?php
/*
** filename: 		embed-widget.php
** description:	this file will display the embed code for the embed widget. can be used for regular video format or
**						for HD format. can be used with either http or https protocols.
** dependencies:	
**	date created:	February 28, 2011
**
*/			


if(isIncluded()){
	$base_url = '../';
	include("../../../embed-widget-config.php");	
}
else{
	$client = 'ceo';
	include("../../embed-widget-config.php");
	$base_url = '';	
}

mysql_select_db ($DBname);

$query = sprintf("SELECT p.ID, p.post_title, p.post_date, p.post_name, p.post_content, p.post_date, p.guid, b.media_id, b.media_thumb FROM wp_posts p, wb_media b
WHERE p.ID=b.post_id
AND p.post_status='publish'
AND p.post_type='post'
ORDER BY p.post_date DESC LIMIT 1");

$result = mysql_query($query);
$i = 0;

while ($row = mysql_fetch_array($result)) {
	
	if($i == 0){
		$numwords = $main_numwords;
		$numchars = $main_numchars;
		$thumbWidth = 120;
		
		if($isHD){
			$thumbHeight = 68;
		}
		else{
			$thumbHeight = 90;		
		}		
	}
	else{
		$numwords = $other_numwords;
		$numchars = $other_numchars;
		$thumbWidth = 73;		
		
		if($isHD){
			$thumbHeight = 41;
		}
		else{
			$thumbHeight = 55;		
		}			
	}
	

	
	$post_id[$i] = $row['ID'];	
	$media_id[$i] = $row['media_id'];		
	
	$post_date_array = explode(' ', $row['post_date']);
	$post_date = explode('-', $post_date_array[0]);
	$post_year = $post_date[0];
	$post_month = $post_date[1];
	$post_day = $post_date[2];
	
	$post_title[$i] = truncateString($row['post_title'], $numwords, $numchars, true);
	$post_title_full[$i] = str_replace("'", "", $row['post_title']);		
	$post_desc[$i] = str_replace("�", "'", truncateString($row['post_content'], $desc_numwords, $desc_numchars, true));
	$post_date[$i] = $row['post_date'];
	$media_thumb[$i] = $row['media_thumb'];

	if( strpos( $row['guid'], "?p=") ){
		$post_link[$i] = 'http://'.$channelSite.'/video-library/'.$row['post_name'];
	}
	else{
		$post_link[$i] = $row['guid'];		
	}
	
	if (!$media_thumb[$i] || $media_thumb[$i] =='' || $media_thumb[$i] =='nothing') {
		//echo $archive[$i]['videoid'].' '.$token;
		$ch = curl_init();
		$timeout = 0; // set to zero for no timeout
		curl_setopt ($ch, CURLOPT_URL, 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id='.$media_id[$i].'&video_fields=thumbnailURL,FLVURL&media_delivery=http&token='.$token );
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$file_contents = curl_exec($ch);
		curl_close($ch);
		//echo '<!--'.$file_contents.'-->';
		$returndata = json_decode($file_contents);							
		$media_thumb[$i] = $returndata->{"thumbnailURL"};		
	}	
	
	
	//check that folder if file exists
	$directory_folder = $base_url.$client.'/thumbnails/'.$post_year.'/'.$post_month.'/'.$post_day;
	if(!file_exists($directory_folder)){			
		mkdir($directory_folder, 0777, true);			
	}

	$image_name = $row['post_name'].'.jpg';	
	$filename = $directory_folder.'/'.$image_name;	

	
	
	if(!file_exists($filename)){				
		copy($media_thumb[$i], $filename);
		createThumbnail($image_name, $directory_folder, '', $thumbWidth, $thumbHeight, 100);			
	}		
		
	
	
	$media_thumb[$i] = $filename;


	$i++;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
	<head>		
		<title><?php echo $clientName; ?> Embed Widget</title>			
		<!--[if !IE]><!-->
        <link rel="stylesheet" type="text/css" href="<?php echo $base_url.$client;?>/channel-embed.css" />
		 <!--<![endif]-->		
		<!--[if IE]>
        <link rel="stylesheet" type="text/css" href="<?php echo $base_url.$client;?>/channel-embed-ie.css" />
		<![endif]-->
		<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		
		<script type="text/javascript">
			try {
				var pageTracker1 = _gat._getTracker("<?php echo $embed_analytics; ?>");
				pageTracker1._trackPageview();
			} catch(err) {}
		</script>
		<style>
		body{
			font-size: 14px;
			padding: 0px;
			margin: 0px;
		}
		a{
			text-decoration: none;	
			color: #fff;
		}
		a:hover{
			text-decoration: underline;	
		}
		#wrapper{
			width: 300px;
			height: 250px;
			border: 1px solid #efefef;
			margin: 0px;
			padding: 0px;
		}
		#header{
			background: #fff url('/embed-widgets/ceo/images/vistage/vistageEmbedWidget_CEOLogo.png') no-repeat;	
			height: 52px;
			width: 300px;
			padding: 0px;
			margin: 0px;
		}
		#content{
			background: #fff url('/embed-widgets/ceo/images/vistage/jpDeJoria.jpg') -60px 0 no-repeat;				
			width: 300px;
			height: 198px;
			padding: 0px;
			margin: 0px;			
		}
		h2{
			font-size: 18px;
			color: #fff;
			margin: 0px 10px;
			text-align: right;
			padding-top: 20px;
			text-align: right;			
		}
		h3{
			color: #FFFFFF;			
			font-size: 14px;
			margin: 10px;
			text-align: right;
			width: 125px;
			float: right;
		}
		#videoInfo{
			height: 150px;
			width: 125px;
			float: right;
		}
		#watchNowDiv{
			clear: both;
			color: #fff;
			text-align: right;
			margin: 10px;
			font-weight: bold;
			padding-right: 20px;
		}
		#watchNowImg{
			vertical-align: middle;	
		}
		#watchNowDiv a:hover{
			text-decoration: none;	
		}		
		</style>
		<script = type="text/javascript">
		function watchEmbedNow(){
			var embedPlayer = window.parent.embedPlayer;
			embedPlayer.innerHTML = 'Displaying the thingy';
		}
		</script>
		
	</head>
	<body>		
		<div id="wrapper">
			<div id="header">
				This week on the
			</div>
			<div id="content">
				<div id="videoInfo">
					<h2>
						<a href="">CEO Name</a>
					</h2>
					<h3>This is the topic. This is the topic.</h3>
				</div>
				<div id="watchNowDiv">
					<a href="" onclick="watchEmbedNow(); return false;">	
						<img id="watchNowImg" src='/embed-widgets/ceo/images/vistage/vistageEmbedWidget_play.png' />
						<span id="wathNowText" >Watch Now</span>
					</a>
				</div>				
			</div>
		</div><!-- wrapper -->		
	</body>	
</html>
<?php

//this function will truncate a string to a maximum number of words or characters
function truncateString($input_string, $max_words, $max_chars, $strip_html=false){
	$encoding = 'UTF-8';
	
	$input_string = trim($input_string);
	
	if($strip_html){
		$input_string = strip_tags($input_string);
	}
	
	if( (mb_strlen($input_string,$encoding)) <= $max_chars ){
		return $input_string;
	}
	
	//truncate according to number of characters
	$string_chars = mb_substr($input_string,0,$max_chars,$encoding);
	
	//truncate according to number of words
	$temp_array = array_slice( explode(" ", $input_string), 0, $max_words );
	
	$string_words = implode( " ", $temp_array);	
	
	if( (mb_strlen($string_words,$encoding)) > $max_chars ){		
		$temp_array = explode(" ", $string_chars);
		$temp_array = array_slice( $temp_array, 0, count($temp_array)-1);
		$string_words = implode(" ", 	$temp_array);	
	}
	
	return $string_words.'...';
	
}


function applyProtocol($url_string){
	$final_url = '';
	$protocol;	
	if( $_SERVER['HTTPS'] == 'on' ){
		$protocol = "https";
	}
	else{
		$protocol = "http";
	}
	
	if($url_string == ''){
		$final_url = $protocol;
	}
	else if( (substr($url_string, 0, 6) == 'https:') && ($protocol == 'http') ){
		$final_url = 'http:'.substr($url_string, 6, strlen($url_string));
	}
	else if( (substr($url_string, 0, 5) == 'http:') && ($protocol == 'https') ){
		$final_url = 'https:'.substr($url_string, 5, strlen($url_string));
	}
	else{
		$final_url = $url_string;
	}
	return $final_url;
}

function isIncluded(){
	$isIncluded = false;
	if( strtolower(realpath(__FILE__)) != strtolower(realpath($_SERVER['SCRIPT_FILENAME'])) ){
		$isIncluded = true;
	}
	
	return $isIncluded;
}

//will get the path for current date and client
function getDirectoryPath($client_code, $directory_type, $formattedDate='', $storyTitle=''){
	
	if($formattedDate == 'NA'){
		$directory_folder = "$directory_type/$client_code";
	}
	else if($formattedDate != ''){
		$directory_folder = "$directory_type/$client_code/$formattedDate";
	}
	else{
		//get today's year, month and date
		$today = getdate();
		$year = $today['year'];
		$month = $today['mon'];
		$day = $today['mday'];			
		$directory_folder = "$directory_type/$client_code/$year/$month/$day";
	}
	
	if($storyTitle != ''){
		$directory_folder .= '/'.$storyTitle;
	}
	
	//check if the directory exists, if not, create it
	if(!file_exists($directory_folder)){			
		mkdir($directory_folder, 0777, true);			
	}
	
	return $directory_folder;
}


//this function will create a thumbnail with specified dimensions
function createThumbnail($img, $imgPath, $suffix, $newWidth, $newHeight, $quality)
{
  // Open the original image.
  $original = imagecreatefromjpeg("$imgPath/$img") or die("Error Opening original");
  list($width, $height, $type, $attr) = getimagesize("$imgPath/$img");
 
  // Resample the image.
  $tempImg = imagecreatetruecolor($newWidth, $newHeight) or die("Cant create temp image");
  imagecopyresized($tempImg, $original, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height) or die("Cant resize copy");
 
  // Create the new file name.
  $newNameE = explode(".", $img);
  $newName = ''. $newNameE[0] .''. $suffix .'.'. $newNameE[1] .'';
 
  // Save the image.
  imagejpeg($tempImg, "$imgPath/$newName", $quality) or die("Cant save image");
 
  // Clean up.
  imagedestroy($original);
  imagedestroy($tempImg);
  return true;
}

//exit: script
//updated: Mar 4, 2011
?>
