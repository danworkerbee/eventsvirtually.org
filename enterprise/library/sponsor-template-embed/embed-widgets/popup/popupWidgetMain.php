<?php
/************************************************************************
NOT IN USE
*/
























<?php
/*
** filename: 		embed-widget.php
** description:	this file will display the embed code for the embed widget. can be used for regular video format or
**						for HD format. can be used with either http or https protocols.
** dependencies:	
**	date created:	February 28, 2011
**
*/
/*
Change Log:
	April 5, 2012		[Jullie]made adjustments on the ceo name and the thumb to accomodate the changes to the "BEST of 
							CEO TV" videos
	January 9, 2012	[Jullie]added code to allow custom styling depending on how the video still looks like
*/

$today = strtotime('now');	
$startDate = strtotime('March 26, 2012 4:00am');
$endDate = strtotime('April 2, 2012 4:00am');

if($today < $endDate && $today > $startDate){
$customStyle = '
<style>
	h2{
		margin-top: 90px;
		margin-right: 5px;
		padding-top: 0;
	}
	#videoInfo h2, #videoInfo h3{
		/*color: #000;*/
	}
</style>
';
}

include("../../includes/config.php");	


if( EMBED_MODE == 'dynamic' ){
	
	$sql = sprintf("
		SELECT ID, post_title, post_content, post_name, media_id  
		FROM wp_posts p, wb_media m
		WHERE p.ID = m.post_id 
		AND post_type='post' 
		AND post_status='publish' 
		ORDER BY post_date DESC LIMIT 1");
	$result = mysql_query($sql) or die(mysql_error());
	$row = mysql_fetch_array($result);
	
	$postId = $row['ID'];
	$post_title = $row['post_title'];
	$short_desc = truncateString($row['post_content'], $numwords, $numchars, false);
	$long_desc = $row['post_content'];
	$post_name = $row['post_name'];
	
	//get the CEO Name
	$tempArray = explode(' - ', $row['post_title']);
	$ceoName = ucwords($tempArray[0]);
	
	if( $ceoName == 'Best Of CEO TV' ){
		$ceoName = ucwords($tempArray[1]);		
	}
	
	$ceoTopic = ucwords($tempArray[2]);
	
	$ceoImage = str_replace(' ', '', $ceoName) . '300x198.jpg';
	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
	<head>		
		<title><?php echo $clientName; ?> Embed Widget</title>			
		<!--[if !IE]><!-->
        <link rel="stylesheet" type="text/css" href="/embed-widgets/default/channel-embed.css" />
		 <!--<![endif]-->		
		<!--[if IE]>
        <link rel="stylesheet" type="text/css" href="/embed-widgets/default/channel-embed-ie.css" />
		<![endif]-->
		<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		
		<script type="text/javascript">
			try {
				var pageTracker1 = _gat._getTracker("<?php echo $analytics[1]["code"]; ?>");
				pageTracker1._trackPageview();
			} catch(err) {}
		</script>
		<script = type="text/javascript">
		function watchEmbedNow(){
			var embedPlayer = window.parent.embedPlayer;
			embedPlayer.innerHTML = 'Displaying the thingy';
		}
		function redirectParent(redirectUrl){
			window.parent.location = redirectUrl;
		}
		</script>	
		<style>
			body{
				font-size: 14px;
				padding: 0px;
				margin: 0px;
				font-family: Arial,Tahoma,Verdana,Georgia,serif;
				outline: none;
			}
			a{
				text-decoration: none;	
				color: #fff;
				outline: none;
			}
			a:hover{
				text-decoration: underline;
				outline: none;
			}
			#wrapper{
				width: 240px;
				height: 120px;
				border: 0px solid #efefef;
				margin: 0px;
				padding: 0px;
			}
		</style>
		<?php
		echo $customStyle;
		?>
	</head>
	<body>				
		<div id="wrapper">
			<?php
			/** CEO Show markup ----
			<style>		
			#header{
				/*background: url("/embed-widgets/ceo/images/vistage/vistageEmbedWidget_CEOLogo.png") no-repeat scroll 0 0*-/ #FFFFFF;
				height: 55px;
				margin: 0;
				padding: 0;
				width: 300px;
			}			
			#header #headerTitle{
				float: left;
				height: 35px;
				margin-left: 10px;
				padding-top: 17px;
				width: 83px;
			}
			#header #headerLogo{
				float: right;
				height: 30px;
				width: 200px;			
			}
			#header #headerLogo img{
				border: 0px none;
			}
			#content{
				/*background: #fff url('/embed-widgets/ceo/images/vistage/<?php echo $ceoImage; ?>') 0 0 no-repeat;*-/				
				width: 300px;
				height: 198px;
				padding: 0px;
				margin: 0px;			
			}
			h2{
				font-size: 18px;
				color: #fff;
				margin: 0px 10px;
				text-align: right;
				padding-top: 20px;
				text-align: right;		
				outline: none;
			}
			h3{
				color: #FFFFFF;			
				font-size: 14px;
				margin: 10px;
				text-align: right;
				width: 125px;
				float: right;
				outline: none;
			}
			#videoInfo{
				height: 150px;
				width: 180px;
				float: right;
			}
			</style>
			<div id="header">
				<div id="headerTitle">This week on</div>
				<div id="headerLogo">
					<a href="javascript: watchEmbedNow()">
						<img src="/embed-widgets/images/Naylor-Banner.jpg" alt="Watch Association TV" />
					</a>
				</div>
			</div>
			<div id="content">
				<div id="videoInfo">
					<h2>
					<?php echo $ceoName; ?>
					</h2>
					<h3><?php echo $ceoTopic; ?></h3>
				</div>
			</div>
			-- */
			?>
			<a href="javascript: watchEmbedNow('<?php echo CHANNEL_UNIQUE_ID; ?>'); return false;">
				<img src="/embed-widgets/images/Naylor-Banner.jpg" alt="Watch Association TV" />
			</a>
		</div><!-- wrapper -->		
	</body>	
</html>
<?php

//this function will truncate a string to a maximum number of words or characters
function truncateString($input_string, $max_words, $max_chars, $strip_html=false){
	$encoding = 'UTF-8';
	
	$input_string = trim($input_string);
	
	if($strip_html){
		$input_string = strip_tags($input_string);
	}
	
	if( (mb_strlen($input_string,$encoding)) <= $max_chars ){
		return $input_string;
	}
	
	//truncate according to number of characters
	$string_chars = mb_substr($input_string,0,$max_chars,$encoding);
	
	//truncate according to number of words
	$temp_array = array_slice( explode(" ", $input_string), 0, $max_words );
	
	$string_words = implode( " ", $temp_array);	
	
	if( (mb_strlen($string_words,$encoding)) > $max_chars ){		
		$temp_array = explode(" ", $string_chars);
		$temp_array = array_slice( $temp_array, 0, count($temp_array)-1);
		$string_words = implode(" ", 	$temp_array);	
	}
	
	return $string_words.'...';
	
}


function applyProtocol($url_string){
	$final_url = '';
	$protocol;	
	if( $_SERVER['HTTPS'] == 'on' ){
		$protocol = "https";
	}
	else{
		$protocol = "http";
	}
	
	if($url_string == ''){
		$final_url = $protocol;
	}
	else if( (substr($url_string, 0, 6) == 'https:') && ($protocol == 'http') ){
		$final_url = 'http:'.substr($url_string, 6, strlen($url_string));
	}
	else if( (substr($url_string, 0, 5) == 'http:') && ($protocol == 'https') ){
		$final_url = 'https:'.substr($url_string, 5, strlen($url_string));
	}
	else{
		$final_url = $url_string;
	}
	return $final_url;
}

function isIncluded(){
	$isIncluded = false;
	if( strtolower(realpath(__FILE__)) != strtolower(realpath($_SERVER['SCRIPT_FILENAME'])) ){
		$isIncluded = true;
	}
	
	return $isIncluded;
}

//will get the path for current date and client
function getDirectoryPath($client_code, $directory_type, $formattedDate='', $storyTitle=''){
	
	if($formattedDate == 'NA'){
		$directory_folder = "$directory_type/$client_code";
	}
	else if($formattedDate != ''){
		$directory_folder = "$directory_type/$client_code/$formattedDate";
	}
	else{
		//get today's year, month and date
		$today = getdate();
		$year = $today['year'];
		$month = $today['mon'];
		$day = $today['mday'];			
		$directory_folder = "$directory_type/$client_code/$year/$month/$day";
	}
	
	if($storyTitle != ''){
		$directory_folder .= '/'.$storyTitle;
	}
	
	//check if the directory exists, if not, create it
	if(!file_exists($directory_folder)){			
		mkdir($directory_folder, 0777, true);			
	}
	
	return $directory_folder;
}


//this function will create a thumbnail with specified dimensions
function createThumbnail($img, $imgPath, $suffix, $newWidth, $newHeight, $quality)
{
  // Open the original image.
  $original = imagecreatefromjpeg("$imgPath/$img") or die("Error Opening original");
  list($width, $height, $type, $attr) = getimagesize("$imgPath/$img");
 
  // Resample the image.
  $tempImg = imagecreatetruecolor($newWidth, $newHeight) or die("Cant create temp image");
  imagecopyresized($tempImg, $original, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height) or die("Cant resize copy");
 
  // Create the new file name.
  $newNameE = explode(".", $img);
  $newName = ''. $newNameE[0] .''. $suffix .'.'. $newNameE[1] .'';
 
  // Save the image.
  imagejpeg($tempImg, "$imgPath/$newName", $quality) or die("Cant save image");
 
  // Clean up.
  imagedestroy($original);
  imagedestroy($tempImg);
  return true;
}

//exit: script
//updated: Mar 4, 2011
?>
