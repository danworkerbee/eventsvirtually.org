<?php
/*
Change log:
	April 5, 2012		[Jullie]made adjustments on the ceo name and the thumb to accomodate the changes to the "BEST of 
							CEO TV" videos
	January 16, 2012	[Jullie]added if statement to change the name under the thumbnail if it's too long;
*/
include '../../wp-config.php';
include '../../includes/config.php';
include '../../includes/brightcove-tokens.php';

$channelName = str_replace(" ", "", CLIENT_CHANNEL);
$siteName = CLIENT_CHANNEL;
$embedFilesRoot = CHANNEL_SITE; //this is where the embed-widget files are located

$uniqueId = CHANNEL_UNIQUE_ID;
$channelUniqueId = CHANNEL_UNIQUE_ID;
$token = TOKEN_READ;

$embedPlayerId = PLAYER_EMBED;
$embedPublisherId = BC_PUBLISHER;
$currentChannelSite = CHANNEL_SITE;

$fbAppId = FB_APP_ID;
$shareThisAPI = SHARETHIS_PUB_ID;
$workerbeeTracker = $analytics[1]['code'];

/** 
COMPONENTS
*/
$embedMode = 'static';			//will display the most recent cast if set to dynamic; if static, you have to specify the 
										//media id in the next
$hasHeader = true;
$hasDesc = false;
$hasTags = false;
$descMode = 'expand';			//can be either expand or link
$hasWideBanner = true;
$hasArchive = false;
$hasSharing = true;

/**
STATIC EMBED Values
*/	
//will be used if EMBED_MODE is set to static
$staticEmbedVideoInfo = array(
	'media_id' => '1561023697001',
	'title' => 'Naylor Introduces New Video Service for Associations',
	'desc' => 'The power of video to enhance and elevate your member communications is undeniable. Watch Charles Popper, Vice President of Association Relations for Naylor discuss this new product and service available from Naylor.',
	'tags' => '',
	'playerId' => '1560789298001',
	'publisherId' => '1545417469001',
	'channelSite' => 'www.naylor.com', //this is used for the og tags and share links; just place the domain/site without the http:// and no trailing slashes
	'archiveCount' => 0 //set to zero so as not to display archived posts
	);

/**
HEADER info
*/
$headerImage = "../../embed-widgets/images/ATVEmbedHeader.png"; //the image displayed for the header
$headerClickThrough = "javascript: void(0);"; //where


/**
WIDE Banner info
*/
$bannerImage = '../../embed-widgets/images/Naylor-Association-468x60.jpg';
$bannerLink = 'http://tv.associationadviser.com/naylor_registration';
$bannerTitle = 'Register with Naylor';

/**
STYLE Options
*/
$topBorder = '6px solid #434343';
$bottomBorder = '6px solid #434343';
$numwords = 55;
$numchars = 140;

if( $embedMode == 'static' ){
	$titleClickthrough = 'javascript: void(0);';
	$post_name = '';
	$post_title = $staticEmbedVideoInfo['title']; //the title of the post
	$long_desc = $staticEmbedVideoInfo['desc']; //the title of the post
	$short_desc = truncateString($long_desc, $numwords, $numchars, false);
	$postTagsCount = 0; //set to zero so that the Keywords won't be displayed; set to 1 or more for keywords
	$postTagString = $staticEmbedVideoInfo['tags']; //comma separated keywords, can use <a> tags as well
	
	$token = TOKEN_READ; //if different from the config
	$media_id = $staticEmbedVideoInfo['media_id']; //the video id
	
		//get the mp4 video link for the video
	// REQUEST INFO FROM BRIGHTCOVE	
	$ch 			= curl_init();
	$timeout 		= 0; // set to zero for no timeout
	curl_setopt ($ch, CURLOPT_URL, 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id='.$media_id.'&video_fields=id,name,startDate,publishedDate,length,shortDescription,longDescription,thumbnailURL,renditions,FLVURL&media_delivery=http&token='.$token );
	
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$file_contents 	= curl_exec($ch);
	//echo $file_contents;
	curl_close($ch);
	$returndata = json_decode($file_contents);
	$videoMP4Link 		= $returndata->{"FLVURL"}; 
	$videoThumb = $returndata->thumbnailURL; //thumbnail for this video
	
	$embedPlayerId = $staticEmbedVideoInfo['playerId']; //if player is different than the one setup in the config
	$embedPublisherId = $staticEmbedVideoInfo['publisherId']; //if publisher is different than the one setup in the config
	
	$currentChannelSite = $staticEmbedVideoInfo['channelSite']; 
	$embedFilesRoot = CHANNEL_SITE; //this is where the embed-widget files are located
	$archiveVidsCount = $staticEmbedVideoInfo['archiveCount']; //set to greater than zero to display archive vids	
	
	
}
else if( $embedMode == 'dynamic' ){
	$base_url = '';
	
	try {
		$dbh = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.'', DB_USER, DB_PASSWORD); 
			  
		$getCurrentCast = $dbh->prepare('
			SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content 
			FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media b
			WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
			AND tt.term_id=t.term_id
			AND tr.object_id=p.ID
			AND p.ID=b.post_id
			AND p.post_status="publish"
			AND t.term_id = 12
			GROUP BY b.media_id
			ORDER BY p.post_date DESC LIMIT 1
		');
		//$getCurrentCast->bindParam(1, CAT_CAST);	
		$getCurrentCast->execute();		
		$getCurrentCastNumrows = $getCurrentCast->rowCount();
		$getCurrentCastResult = $getCurrentCast->fetch();
			
		$getCurrentCast= null;  
		$dbh = null; 
				  
	}catch (PDOException $e) { 
		echo "Error!: Could not connect to DB";
	}				
	
	//print_r($getCurrentCastResult);
	
	$postId = $getCurrentCastResult['ID'];
	$post_title = $getCurrentCastResult['post_title'];	
	$long_desc = strip_tags($getCurrentCastResult['post_content']);
	//$long_desc = preg_replace("/<ul.*>(.)*</ul>/", "", $getCurrentCastResult['post_content']);
	$short_desc = truncateString($long_desc, $numwords, $numchars);
	$post_name = $getCurrentCastResult['post_name'];
	$media_id = $getCurrentCastResult['media_id'];
	
	//echo '$long_desc '.$long_desc;
	
	//get the mp4 video link for the video
	// REQUEST INFO FROM BRIGHTCOVE	
	$ch 			= curl_init();
	$timeout 		= 0; // set to zero for no timeout
	curl_setopt ($ch, CURLOPT_URL, 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id='.$media_id.'&video_fields=id,name,startDate,publishedDate,length,shortDescription,longDescription,thumbnailURL,renditions,FLVURL&media_delivery=http&token='.$token );
	
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$file_contents 	= curl_exec($ch);
	//echo $file_contents;
	curl_close($ch);
	$returndata = json_decode($file_contents);
	$videoMP4Link 		= $returndata->{"FLVURL"}; 


	try {
		$dbh = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.'', DB_USER, DB_PASSWORD); 
			  
		$getKeywords = $dbh->prepare('
			SELECT t.term_id, t.name, t.slug
			FROM wp_posts p, wp_terms t, wp_term_taxonomy tt, wp_term_relationships tr
			WHERE p.ID = tr.object_id
			AND tr.term_taxonomy_id = tt.term_taxonomy_id
			AND tt.term_id = t.term_id
			AND tt.taxonomy = "post_tag"
			AND p.ID = ?
		');
		$getKeywords->bindParam(1, $postId);	
		$getKeywords->execute();		
		$getKeywordsNumrows = $getKeywords->rowCount();
		$getKeywordsResult = $getKeywords->fetchAll();
			
		$getKeywords= null;  
		$dbh = null; 
				  
	}catch (PDOException $e) { 
		echo "Error!: Could not connect to DB";
	}		
	
	foreach($getKeywordsResult as $row){
		$postTags[$row['term_id']] = '<a href="http://'.$currentChannelSite.'/keyword/'.$row['slug'].'">'.$row['name'].'</a>';
	}
	
	$postTagString = implode(', ', $postTags);
	
	if( $hasArchive === true ){				
		//get the last 4 archived vids
		try {
			$dbh = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.'', DB_USER, DB_PASSWORD); 
				  
			$getArchiveVids = $dbh->prepare('
				SELECT ID, post_title, post_content, post_name, media_id, media_thumb  
				FROM wp_posts p, wb_media m
				WHERE p.ID = m.post_id 
				AND post_type="post" 
				AND post_status="publish"	
				AND p.ID <> ?
				ORDER BY ID DESC LIMIT 4
			');
			$getArchiveVids->bindParam(1, $postId);	
			$getArchiveVids->execute();		
			$getArchiveVidsNumrows = $getArchiveVids->rowCount();
			$getArchiveVidsResult = $getArchiveVids->fetchAll();
				
			$getArchiveVids= null;  
			$dbh = null; 
					  
		}catch (PDOException $e) { 
			echo "Error!: Could not connect to DB";
		}						
		
		$archiveVids = array();
		foreach($getArchiveVidsResult as $row){
			$archiveVids[$row['ID']] = array();
			$archiveVids[$row['ID']]['post_id'] = $row['ID'];
			$archiveVids[$row['ID']]['post_title'] = $row['post_title'];
			/**
			if token is required
			$archiveVids[$row['ID']]['post_name'] = 'javascript: redirectParent(\'http://'.$currentChannelSite.'/vistage-videos?token=viewVideosVistage&vid='.$row['post_name'].'\');';*/
			
			$archiveVids[$row['ID']]['post_name'] = 'http://'.$currentChannelSite.'/'.$row['post_name'];
			
			//get the thumb	
			$archiveVids[$row['ID']]['thumb'] = $row['media_thumb'];	
		}
		
	}	
	
	
	
	$titleClickthrough = 'http://'.$currentChannelSite.'/'.$post_name;
	
}

if( $_SERVER['HTTPS'] == 'on' ){
	$protocol = "https";
}
else{
	$protocol = "http";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
	<head>
		<meta name="Keywords" content="<?php echo strip_tags($postTagString); ?>" />
		<meta name="Description" content="<?php echo strip_tags($short_desc); ?>" />
		<meta property="og:title" content="<?php echo strip_tags($post_title); ?>" />
		<meta property="og:type" content="tv_show" />
		<meta property="og:url" content="http://<?php echo $currentChannelSite; ?>/<?php echo $post_name; ?>" />
		<meta property="og:image" content="<?php echo $videoThumb; ?>" />
		<meta property="og:site_name" content="<?php echo strip_tags($siteName); ?>" />
		<meta property="og:description" content="<?php echo strip_tags($short_desc); ?>" />
		<link rel="image_src" type="image/jpeg" href="<?php echo $videoThumb; ?>" />
		<meta property="fb:app_id" content="<?php echo $fbAppId; ?>" />
		<meta property="og:locale" content="en_US" />	
		<style>
		html,body{
			margin: 0;
			padding: 0;
		}
		#<?php echo $uniqueId;?>{
			border: 0 solid #525252;
			margin: 0 auto;
			overflow: hidden;
			width: 530px;
		}
		#<?php echo $channelName;?>Div<?php echo $postId;?> {
			position:relative;
			color:#000;
			width: 545px;
			margin: 0px;
			background-color:#fff;
			border:solid 1px #fff;
			outline: none;
		}
		#embedDiv<?php echo $uniqueId;?> {
			background-color: #FFFFFF;
			bottom: 10px;
			font-family: Arial, "Trebuchet MS", Helvetica,"Myriad Web Pro","Myriad Pro", sans-serif;
			font-size: 12px;
			left: 0;
			margin: 0 auto;
			/*min-height: 475px;*/
			padding: 0;
			position: relative;
			top: 0;
			width: 515px;
		}
		#embedDiv<?php echo $uniqueId;?>div {
			position:relative;
			min-height:20px;
		}
		#embedDiv<?php echo $uniqueId;?> h2,#embedDiv<?php echo $uniqueId;?> h3,#embedDiv<?php echo $uniqueId;?> h4, #archiveVidsDiv<?php echo $uniqueId;?> h2, #archiveVidsDiv<?php echo $uniqueId;?> h3, #archiveVidsDiv<?php echo $uniqueId;?> h4 {
			text-align:left;
			background:none;
			text-shadow:none 0px #fff !important;
			font-family: Arial, "Trebuchet MS", Helvetica,"Myriad Web Pro","Myriad Pro", sans-serif !important;
			padding:0px;
			width:450px !important;
			vertical-align:-20% !important;
		}
		#embedDiv<?php echo $uniqueId;?> img {
			margin:0px;
			padding:0px;
			margin-right:5px;
			border:0px;
		}
		#embedDiv<?php echo $uniqueId;?> h2 {
			border: medium none;
			color: #010101;
			font-size: 18px;
			font-weight: bold;
			margin: 12px 0 !important;
			text-transform: none;		
			letter-spacing: -1px;
		}
		#embedDiv<?php echo $uniqueId;?> h4 {
			font-weight:normal;
			margin:7px 5px !important;
			font-size:100%;
			width:450px;
		}
		#embedDiv<?php echo $uniqueId;?> p {
			/*padding-bottom:5px;*/
			color: #000000;
			font-family: Arial, "Trebuchet MS", Helvetica,"Myriad Web Pro","Myriad Pro", sans-serif !important;
			font-size: 13px;
			margin: 5px 0 !important;
			text-shadow: none !important;
		}
		#embedDiv<?php echo $uniqueId;?> a {
			color:#010101;
			font-family: Arial, "Trebuchet MS", Helvetica,"Myriad Web Pro","Myriad Pro", sans-serif !important;
			text-decoration:none;
			text-shadow:none !important;
			border:none !important;
		}
		#embedDiv<?php echo $uniqueId;?> #<?php echo $channelName;?>Header {
			/*border-bottom: 6px solid #434343*/
		}
		#embedDiv<?php echo $uniqueId;?> div#postedContent {
			/*min-height: 50px;*/
			padding: 0 !important;
			width: 515px;			
		}
		#embedDiv<?php echo $uniqueId;?> #bannerAd {
			width:468px;
			height:60px;
			margin:10px auto;
		}
		#embedDiv<?php echo $uniqueId;?> .bannerImg {
			width:468px;
			height:60px;
			#color:#fff;
		}
		#embedDiv<?php echo $uniqueId;?> #videoEncoding {
			height: 285px;
			margin: 0px auto 5px;
			width: 508px;
		}
		#embedDiv<?php echo $uniqueId;?> #popupDiv {
			width:440px;
			height:502px;
			visibility:hidden;
			position:relative;
			display:none;
			z-index:1000;
			border:1px #aaa solid;
			margin:20px auto 30px;
		}
		#embedDiv<?php echo $uniqueId;?> #popupDiv iframe#adForm {
			width:425px;
			height:475px;
			border:none;
			padding:0px;
		}
		#embedDiv<?php echo $uniqueId;?> #popupDiv #exitButton {
			width:41px;
			height:20px;
		}
		#embedDiv<?php echo $uniqueId;?> #popupDiv #exitButton a#closeImg {
			position:relative;
			left:380px;
			top:5px;
			padding:10px;
			padding-bottom:0px;
			margin:0px;
			width:41px;
			z-index:2;
			color:#295A2D;
			font-family: Arial, "Trebuchet MS", Helvetica,"Myriad Web Pro","Myriad Pro", sans-serif !important;
			text-decoration:none;
			font-size:small;
			border:none;
		}
		#embedDiv<?php echo $uniqueId;?> #popupDiv a#closeImg img {
			border:none;
		}
		#postedContent .more_less_link {
			font:13px/1.231 arial,helvetica,clean,sans-serif;
			color:#777777;
			font-size:10px;
			text-transform:uppercase;
		}
		#embedDiv<?php echo $uniqueId;?> #sharingAndEmbed {			
			height: 55px;
			padding-bottom: 5px;
		}
		#embedDiv<?php echo $uniqueId;?> #sharingAndEmbed #share-box {
			font-size:11px;
			color:#777;
			margin-bottom: 2px;
		}
		#embedDiv<?php echo $uniqueId;?> #sharingAndEmbed #sharingDiv {
			width:266px;
			float:left;
		}
		#embedDiv<?php echo $uniqueId;?> #sharingAndEmbed #permalink_embed {
			width:245px;
			float:right;
		}
		#embedDiv<?php echo $uniqueId;?> #sharingAndEmbed #permalink_embed table {
			height:20px;
		}
		#embedDiv<?php echo $uniqueId;?> #permalink_embed input {
			width:112px;
			margin:0px;
		}
		html>body #embedDiv<?php echo $uniqueId;?> #permalink_embed input {
			width:112px;
			margin:5px 0px;
		}
		#embedDiv<?php echo $uniqueId;?> #permalink_embed .labelDiv {
			width:50px;
			display:inline-block;
			margin-left:5px;
			margin-bottom: 2px;
		}
		#embedDiv<?php echo $uniqueId;?> #permalink_embed label {
			color:#777;
			font-size:11px;			
		}
		#embedDiv<?php echo $uniqueId;?> #keywords {
			margin: 5px 0;
		}				
		html>body #embedDiv<?php echo $uniqueId;?> #permalink_embed input{
		margin: 3px 0px\0/;
		}
		
		#<?php echo $uniqueId;?> #bannerDiv<?php echo $uniqueId;?>{
			margin: 5px auto;
			width: 468px;
			border: none;
		}
		
		#<?php echo $uniqueId;?> #bannerDiv<?php echo $uniqueId;?> img{
			border: none;
		}
		
		hr#topDivider{
			border: <?php echo $topBorder; ?>;	
		}
		
		hr#bottomDivider{
			border: <?php echo $bottomBorder; ?>;	
		}
		
		#<?php echo $uniqueId;?> hr#topDivider, #<?php echo $uniqueId;?> hr#bottomDivider{
			border-left: 0;
			border-bottom: 0;
			border-right: 0;
			margin: 0;
			padding: 0;
		}
		
		#archiveVidsDiv<?php echo $uniqueId;?>{
			position:relative;
			bottom:10px;
			top:10px;
			left:0px;
			margin:0px;
			padding:0px;
			background-color:#fff;
			width:525px;
			height:130px;
			margin:0px auto;
			font-family: Arial, "Trebuchet MS", Helvetica,"Myriad Web Pro","Myriad Pro", sans-serif;
			font-size: 12px;						
		}
		#archiveVidsDiv<?php echo $uniqueId;?> ul{
			display: block;
			margin: 5px auto;
			padding: 0;
		}
		#archiveVidsDiv<?php echo $uniqueId;?> ul li{
			display: inline-block;
			list-style: none outside none;
			margin: 0 8px;
			width: 110px;
			font-size: 12px;
		}
		#archiveVidsDiv<?php echo $uniqueId;?> ul li a{
			text-decoration: none;
			color: #181b45;
			font-weight: bold;
		}
		#archiveVidsDiv<?php echo $uniqueId;?> ul li a:visited{
			text-decoration: none;
			color: #181b45;
		}		
		#archiveVidsDiv<?php echo $uniqueId;?> ul li a img{
			border: 0px none;
		}
		#archiveVidsDiv<?php echo $uniqueId;?> h2 {
			font-weight:normal;
			margin: 0px 5px !important;
			border:medium none;
			color:#295A2D;
			font-size: 14px;
			font-weight: bold;
			margin-bottom:10px;
			text-transform:none;
		}
		
		</style>
		
		<!--[if IE]>
		<style>
		#embedDiv<?php echo $uniqueId;?> h2 {
			margin-top: 8px !important;
			margin-bottom: 8px !important;
		}
		#archiveVidsDiv<?php echo $uniqueId;?>{
			position:relative;
			bottom:10px;
			top:10px;
			left:0px;
			margin:0px;
			padding:0px;
			background-color:#fff;
			width:525px;
			margin-bottom: 12px;
			margin:0px auto;
			font-family: Arial, "Trebuchet MS", Helvetica,"Myriad Web Pro","Myriad Pro", sans-serif;
			font-size:small;
			margin-bottom: 12px;				
		}
		#archiveVidsDiv<?php echo $uniqueId;?> table{
			display: block;
			margin: 5px auto;
			padding: 0;
			text-align: center;
		}

		#archiveVidsDiv<?php echo $uniqueId;?> table td{
			display: inline-block;
			list-style: none outside none;
			margin: 2px 8px;
			width: 25%;
			font-size: 12px;
		}
		#archiveVidsDiv<?php echo $uniqueId;?> table td a{
			text-decoration: none;
			color: #181b45;
			font-weight: bold;
		}
		#archiveVidsDiv<?php echo $uniqueId;?> table td a:visited{
			text-decoration: none;
			color: #181b45;
		}		
		#archiveVidsDiv<?php echo $uniqueId;?> table td a img{
			border: 0px none;
		}
		#archiveVidsDiv<?php echo $uniqueId;?> #archiveCeoName{
			margin-top: 4px;
			width: 125px;						
		}
		
		#archiveVidsDiv<?php echo $uniqueId;?> h2 {
			font-weight:normal;
			margin: 0px 5px !important;
			border:medium none;
			color:#295A2D;
			font-size: 14px;
			margin-bottom:10px;
			font-size: 14px;
			text-transform:none;
		}

		</style>		
		
		<![endif]-->
		
		<script src="<?php echo $protocol; ?>://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
		<script src="<?php echo $protocol; ?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js" type="text/javascript"></script>		
		

		

		
		<script type="text/javascript">
		var switchTo5x=true;
		</script>
		<?php
		if( $hasSharing ){
		?>
		<script type="text/javascript">
		try { stLight.options({
				publisher: '<?php echo SHARETHIS_PUB_ID; ?>', 
				tracking: 'google',
				 publisherGA:"<?php echo $workerbeeTracker; ?>"
				});
		} 
		catch(err) {}		
		</script>		
		<?php
		}
			if( $_SERVER['HTTPS'] == 'on' ){
		?>
		<script language="JavaScript" type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>
		<script type="text/javascript" src="https://www.google-analytics.com/ga.js"></script>		
		<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
		<?php
			}
			else{
		?>
		<script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>
		<script type="text/javascript" src="http://www.google-analytics.com/ga.js"></script>			
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<?php
			}
		?>		
		
		<?php 
		global $analytics;
		foreach ($analytics as $key=>$tracker) {
			if( $tracker['name'] == 'Workerbee' ){
			echo '<!-- '.$tracker['name'].' -->'; 
		?>
		
			<script type="text/javascript">
				try {
				var pageTracker<?= $key ?> = _gat._getTracker("<?= $tracker['code'] ?>");
				pageTracker<?= $key ?>._trackPageview();
				} catch(err) {}
			</script>
		<?php 
			}
		} 
		?>
		
		<script type="text/javascript">
		function toggleMoreInfo(postId){
			var excerpt_par = document.getElementById('postDesc'+ postId + 'Excerpt');
			var complete_par = document.getElementById('postDesc'+ postId + 'Complete');
			
			if(excerpt_par.style.display == 'none'){
				complete_par.style.display = 'none';
				excerpt_par.style.display = 'block';
			}
			else if(complete_par.style.display == 'none'){
				complete_par.style.display = 'block';
				excerpt_par.style.display = 'none';
			}		
		}
		
		function styleShareThis(){
			$('.stButton').css('marginLeft','0px');
			$('.stButton').css('marginRight','0px');				
		}	
		
		function redirectParent(redirectUrl){
			window.parent.location = redirectUrl;
		}
		</script>
		
		<!--[if gt IE 7]>
		<script type="text/javascript">
		$(function() {			
				setTimeout('styleShareThis()', 500);			
		});
		</script>
		<![endif]-->
		<!--[if !IE]><!-->
		<script type="text/javascript">
		$(function() {			
			//setTimeout('styleShareThis()', 500);			
		});
		</script>		
		<!--<![endif]-->
				
	</head>
	<body>
		<div name="headerDiv<?php echo $uniqueId; ?>" id="<?php echo $channelUniqueId; ?>">			
			<div id="embedDiv<?php echo $uniqueId; ?>">
				<?php
				if( $hasHeader && trim($headerImage) != '' ){
				?>
				<div id="<?php echo $channelName;?>Header">
					<a href="<?php echo $headerClickThrough; ?>" style="cursor: default;"><img src="<?php echo $headerImage; ?>"></a>
				</div>
				<?php
				}
				?>				
				<hr id="topDivider" />
				<div id="postedContent">
				<h2><a href="<?php echo $titleClickthrough; ?>" style="cursor: default;"><?php echo $post_title; ?></a></h2>
					<?php
					if( $hasDesc && $expandDesc ){
					?>
					<p id="postDesc<?php echo $postId;?>Complete" style="display: none;">
						<?php echo $long_desc; ?> <a class="more_less_link" href="javascript:void(0)" onclick="toggleMoreInfo(<?php echo $postId;?>); return false"> &lt;&lt; Less </a>
					</p>
					<p id="postDesc<?php echo $postId;?>Excerpt">
						<?php echo $short_desc; ?> <a class="more_less_link" href="javascript:void(0)" onclick="toggleMoreInfo(<?php echo $postId;?>); return false">More &gt;&gt; </a>
					</p>
					<?php
					}
					else if( $hasDesc ){
					?>
					<p id="postDesc<?php echo $postId;?>Excerpt">
						<?php echo $short_desc; ?> <a class="more_less_link" href="<?php echo $titleClickthrough; ?>">More &gt;&gt; </a>
					</p>
					<?php
					}
					if( $hasTags && $getKeywordsNumrows > 0){
					?>
					<div id="keywords">
						<div id="postKeywords264Complete">
							Keywords: <?php echo $postTagString; ?>
						</div>
					</div>
					<?php
					}
					?>
				</div>
				<div id="videoEncoding">
					<?php
					if(strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPod') || strstr($_SERVER['HTTP_USER_AGENT'],'iPad')) {
					?>
					<video id="<?php echo $channelName; ?>VidHtml5" width="508" height="285.75" controls="controls" autoplay="autoplay" >
						<source type="video/mp4; codecs='avc1.42E01E, mp4a.40.2'" src="<?php echo $videoMP4Link; ?>" >
					</video>					
					<?php
					}
					else{
					?>					
					<!-- Start of Brightcove Player -->
					<div style="display:none"></div>
					<!--By use of this code snippet, I agree to the Brightcove Publisher T and C found at http://corp.brightcove.com/legal/terms_publisher.cfm. -->
					<object id="myExperience<?php echo $media_id; ?>" class="BrightcoveExperience"  style="margin: 5px auto;">
						<param name="bgcolor" value="#FFFFFF" />
						<param name="width" value="508" />
						<param name="height" value="285" />
						<param name="playerID" value="<?php echo $embedPlayerId; ?>" />
						<param name="publisherID" value="<?php echo $embedPublisherId; ?>"/>
						<param name="isVid" value="true" />
						<param name="isUI" value="true" />
						<param name="wmode" value="transparent" />
						<param name="@videoPlayer" value="<?php echo $media_id; ?>" />
					<?php
						if( $_SERVER['HTTPS'] == 'on' ){
							echo '<param name="secureConnections" value="true" />';
						}
					?>
					</object><!-- End of Brightcove Player -->									
					<?php
					}
					?>

				</div>
				<?php
				if( $hasSharing ){
				?>
				<div id="sharingAndEmbed">
					<div id="sharingDiv">
						<div id="share-box">
							Share/email this video
						</div>
						<div id="share-this-video">
							<span class='st_twitter_large' ></span>
							<span class='st_facebook_large' ></span>
							<span class='st_linkedin_large' ></span>
							<img style="margin-bottom: 2px; cursor: pointer;" src="<?php echo $protocol; ?>://<?php echo $embedFilesRoot; ?>/embed-widgets/images/googleplus.png"									onclick="javascript:(function(){var w=480;var h=380;var x=Number((window.screen.width-w)/2);var y=Number((window.screen.height-h)/2);window.open('https://plusone.google.com/_/+1/confirm?hl=en&url='+encodeURIComponent(location.href)+'&title='+encodeURIComponent(document.title),'','width='+w+',height='+h+',left='+x+',top='+y+',scrollbars=no');})();" />		
									

                     <!-- adding qr code -->
                     <script type="text/javascript"> function getBitly(){ $.getJSON( 'http://api.bit.ly/v3/shorten?', { 'format': 'json', 'login': 'videoqrcode', 'apiKey': 'R_4fe5b3cdf79e449b9aa43d63b9ae11ad', 'longUrl': 'http://<?php echo $currentChannelSite; ?>/<?php echo $post_name; ?>?utm_campaign=qrCode&utm_source=qrCode&utm_medium=share&utm_content='+encodeURIComponent($('meta[property="og:title"]').attr('content'))+'&utm_name='+encodeURIComponent($('meta[property="og:description"]').attr('content')) }, function(response) { var w=350; var h=350; var x=Number((window.screen.width-w)/2); var y=Number((window.screen.height-h)/2); window.open(response.data.url+'.qrcode','_blank','width='+w+',height='+h+',left='+x+',top='+y+',scrollbars=no'); } ); } </script>
                     <a href="javascript: getBitly();"> <span style="margin-left: 3px !important;" > <img src="/images/qrIcon.jpg" title="QR Code" /> </span> </a>
                     <!-- end qr code --> 
							
							<?php/*
							<span class='st_sharethis_large' ></span>
							
							*/?>
							<span class='st_email_large' ></span>							
						</div>
					</div>
					<div id="permalink_embed">
						<!--[if !IE]><!-->
						<div class="labelDiv">
							<!--<![endif]-->
							<!--[if IE]><table><![endif]-->
							<!--[if IE]><tr><td  colspan="3" ><![endif]-->
							<label>Permalink</label>
							<!--[if !IE]><!-->
						</div>
						<!--<![endif]-->
						<!--[if IE]></td><tr><td colspan="2"><![endif]-->
						<!--[if !IE]><!-->
						<br>
						<!--<![endif]-->
						<input class="text" name="shareLink" value="http://<?php echo $currentChannelSite; ?>/<?php echo $post_name; ?>" style="margin: 0px 5px 0px 10px; width: 150px;" type="text">
						<!--[if IE]></td><td><![endif]-->
						<object type="application/x-shockwave-flash" data="<?php echo $protocol; ?>://<?php echo $embedFilesRoot; ?>/swf/copyButton.swf?copyText=http://<?php echo $currentChannelSite; ?>/<?php echo $post_name; ?>" height="14" width="68"><param name="movie" value="<?php echo $protocol; ?>://<?php echo $embedFilesRoot; ?>/swf/copyButton.swf?copyText=http://<?php echo $currentChannelSite; ?>/<?php echo $post_name; ?>"><param name="WMODE" value="transparent"></object>
						<!--[if IE]></td></tr><![endif]-->
						<!--[if IE]></table><![endif]-->
					</div>
				</div>
				<hr id="bottomDivider" />
			</div>
			<?php
			}
			?>
			
			<?php
			if( $hasWideBanner && trim($bannerImage) != '' ){
			?>
			<div id="bannerDiv<?php echo $uniqueId;?>">
				<a onclick="<?php
					foreach ($analytics as $key=>$tracker) {
						if( $tracker['name'] == 'Workerbee'){
						echo 'pageTracker'.$key.'._trackEvent(\'Embed\', \'Banner Click\', \''.$channelName. 'PopUpEmbed - ' .$bannerTitle.' - '.$bannerLink.'\');';
						}
					}
					?>" href="<?php echo $bannerLink; ?>" target="_TOP"><img src="<?php echo $bannerImage; ?>" alt="<?php echo $bannerTitle; ?>" /></a>			
			</div>
			<?php
			}
			?>
			
			<?php
			if($getArchiveVidsNumrows > 0){
			?>
			<div id="archiveVidsDiv<?php echo $uniqueId;?>">
				<h2>Watch more Videos in the Archive</h2>
				<!--[if !IE]><!-->
				<ul>
				<!--<![endif]-->
				<!--[if IE]><table><tr><![endif]-->
				<?php
				foreach($archiveVids as $currentVid){										
				?>
					<!--[if !IE]><!-->
					<li>
					<!--<![endif]-->
					<!--[if IE]><td><![endif]-->
						<a href="<?php echo $currentVid['post_name']; ?>" taget="_TOP">
							<img src="<?php echo $currentVid['thumb']; ?>" alt="<?php echo $currentVid['post_title']; ?>"/>
							<!--[if IE]><div id="archiveCeoName"><![endif]-->
							<?php echo truncateString($currentVid['post_title'], 5, 30, true); ?>
							<!--[if IE]></div><![endif]-->
						</a>
					<!--[if IE]></td><![endif]-->
					<!--[if !IE]><!-->
					</li>
					<!--<![endif]-->
				<?php
				}
				?>
				<!--[if IE]></tr></table><![endif]-->
				<!--[if !IE]><!-->
				</ul>
				<!--<![endif]-->
			</div>
			<?php
			}
			?>
		</div>

		<style>
		#embedDiv<?php echo $uniqueId;?> #sharingAndEmbed #share-box .stButton{
			margin-left: 0px;
			margin-right: 0px;
		}
		</style>
	</body>
</html>
<?php
//this function will truncate a string to a maximum number of words or characters
function truncateString($input_string, $max_words, $max_chars, $strip_html=false){
	$encoding = 'UTF-8';
	
	$input_string = trim($input_string);
	
	if($strip_html){
		$input_string = strip_tags($input_string);
	}
	
	if( (mb_strlen($input_string,$encoding)) <= $max_chars ){
		
		return $input_string;
	}
	
	//truncate according to number of characters
	$string_chars = mb_substr($input_string,0,$max_chars,$encoding);
	
	//truncate according to number of words
	$temp_array = array_slice( explode(" ", $input_string), 0, $max_words );
	
	$string_words = implode( " ", $temp_array);	
	
	if( (mb_strlen($string_words,$encoding)) > $max_chars ){		
		$temp_array = explode(" ", $string_chars);
		$temp_array = array_slice( $temp_array, 0, count($temp_array)-1);		
		$string_words = implode(" ", 	$temp_array);
		$string_words .= '...';
	}
	return $string_words;
	
}


function applyProtocol($url_string){
	$final_url = '';
	$protocol;	
	if( $_SERVER['HTTPS'] == 'on' ){
		$protocol = "https";
	}
	else{
		$protocol = "http";
	}
	
	if($url_string == ''){
		$final_url = $protocol;
	}
	else if( (substr($url_string, 0, 6) == 'https:') && ($protocol == 'http') ){
		$final_url = 'http:'.substr($url_string, 6, strlen($url_string));
	}
	else if( (substr($url_string, 0, 5) == 'http:') && ($protocol == 'https') ){
		$final_url = 'https:'.substr($url_string, 5, strlen($url_string));
	}
	else{
		$final_url = $url_string;
	}
	return $final_url;
}
?>
