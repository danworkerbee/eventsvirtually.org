/*
** filename:     channel-embed.js
** description:  this file dynamically adds scripts and contents to the page to display the embed widget
** Author:       WorkerBee TV
** Date Created: February 28, 2011
** Version:      0.1
*/

var headTag;

//the allowed domains should only include the domain name, do not include subdomains(eg. USE 'mydomain.com' and NOT 'www.mydomain.com' or 'files.mydomain.com')
//var allowedDomains = Array('theceoshowonline.com', 'ceoshow.tv', 'wbtvserver.com', 'websitevideocenter.com');

addLoadEventChannelEmbed(displayEmbedWidget('TheCEOTVShowwithRobertReiss4791541', 'ceo'));

function displayEmbedWidget(channelUniqueId, clientCode){
	var workingDiv;
	var channelSite;
	var current_domain = document.domain;
	
	/*
	//for checking domains
	var counter = 0;
	for(var i=0; i<allowedDomains.length; i++){
		//TODO: use regex next time
		if( current_domain.indexOf(allowedDomains[i]) != -1 ){						
			counter++;
		}		
	}
	
	//if current domain is not in the allowed list
	if(counter == 0){		
		return;		
	}
	else{	
	*/
		headTag = document.getElementsByTagName('head')[0];	
		
		workingDiv = document.getElementById(channelUniqueId);

		workingDiv.style.width = '300px';
		workingDiv.style.margin = '10px auto';
		
		workingDiv.innerHTML += '<iframe id="embed-page" scrolling="no" frameborder="0" style="width: 300px; height: 250px; margin: 10px auto;" src="'+applyProtocol('https://websitevideocenter.com/embed-widgets/'+clientCode+'/index2.php')+'" ></iframe>';
		
	//}
}

function addLoadEventChannelEmbed(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            eval(func);
        }
    }
}

function applyProtocol(url_string){
	var final_url = '';
	var protocol = (("https:" == document.location.protocol) ? "https" : "http");
	
	if(url_string == ''){
		final_url = protocol;
	}
	else if( (url_string.substring(0, 6) == 'https:') && (protocol == 'http') ){
		final_url = 'http:'+url_string.substring(6, url_string.length);
	}
	else if( (url_string.substring(0, 5) == 'http:') && (protocol == 'https') ){
		final_url = 'https:'+url_string.substring(5, url_string.length);
	}
	else{
		final_url = url_string;
	}
	
	return final_url;
}


//exit: script
//updated: Feb 28, 2011
