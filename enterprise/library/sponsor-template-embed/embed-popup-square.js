/*
filename:		channel-embed-popup.js
description:	this file dynamically adds scripts and contents to the page to display the embed widget; this version 
					will display a banner/image and then will have the video player popup when the banner or a link on the image is clicked
Author:			Jullie Quijano

Change Log:
	April 13, 2012		[Jullie]created file from channel-embed-vistage.js

*/

var headTag;
var clientCode
var workingDiv;
var channelSite;
var clientFullName = 'Association TV';
var currentClientCode = 'naylor';
var currentClientUniqueId = 'naylorATV357216s';
var currentEmbedDivId = 'naylorAATV846s';
var clientDomain = 'http://tv.associationadviser.com/';
var mainPageWidth = 300;
var mainPageHeight = 250;
var mainPageBorderStyle = '0 none transparent'
var mainBannerImage = 'NaylorVideoSolutionsForMarketers.jpg';

var playerPageWidth = 530;
var playerPageHeight = 551;

//this is for custom styling when necessary
var d = new Date();
var todaysDate = d.getTime();

var startD = new Date("March 26, 2012 4:00:00");
var startDate = startD.getTime();

var endD = new Date("April 2, 2012 4:00:00");
var endDate = endD.getTime();
var customStyle = '';

if(todaysDate < endDate && todaysDate > startDate){
	customStyle = '	#watchNowDiv{ padding-right: 0px;} ';
}

//the allowed domains should only include the domain name, do not include subdomains(eg. USE 'mydomain.com' and NOT 'www.mydomain.com' or 'files.mydomain.com')
//var allowedDomains = Array('theceoshowonline.com', 'ceoshow.tv', 'wbtvserver.com', 'websitevideocenter.com');

addLoadEventChannelEmbed(displayEmbedWidget(currentClientUniqueId, currentClientCode));

function displayEmbedWidget(channelUniqueId, clientCode){
	var current_domain = document.domain;
	
	/*
	//for checking domains
	var counter = 0;
	for(var i=0; i<allowedDomains.length; i++){
		//TODO: use regex next time
		if( current_domain.indexOf(allowedDomains[i]) != -1 ){						
			counter++;
		}		
	}
	
	//if current domain is not in the allowed list
	if(counter == 0){		
		return;		
	}
	else{	
	*/
		headTag = document.getElementsByTagName('head')[0];	
		bodyTag = document.getElementsByTagName('body')[0];
		
		
		//add shadowbox script
		var shadowBoxScript = document.createElement('script');
		shadowBoxScript.setAttribute('type', 'text/javascript');
		shadowBoxScript.setAttribute('src', applyProtocol(clientDomain+'embed-widgets/js/shadowbox.js') );
		headTag.appendChild(shadowBoxScript);
		
		//adding the scripts for the brightcove api
		var bcScript = document.createElement('script');
		bcScript.setAttribute("type", "text/javascript");	
		
		if("https:" == document.location.protocol){
			bcScript.setAttribute("src", applyProtocol("https://sadmin.brightcove.com/js/BrightcoveExperiences.js") );
		}
		else{
			bcScript.setAttribute("src", applyProtocol("http://admin.brightcove.com/js/BrightcoveExperiences.js") );
		}
		
		headTag.appendChild(bcScript);
		
		
		//adding the scripts for the brightcove api
		var bcScript2 = document.createElement('script');
		bcScript2.setAttribute("type", "text/javascript");	
		
		if("https:" == document.location.protocol){
			bcScript2.setAttribute("src", applyProtocol("https://sadmin.brightcove.com/js/APIModules_all.js") );
		}
		else{
			bcScript2.setAttribute("src", applyProtocol("http://admin.brightcove.com/js/APIModules_all.js") );
		}		
		headTag.appendChild(bcScript2);		
		
		//add shadowbox style
		var shadowBoxStyle = document.createElement('link');
		shadowBoxStyle.setAttribute('type', 'text/css');
		shadowBoxStyle.setAttribute('rel', 'stylesheet');
		shadowBoxStyle.setAttribute('href', applyProtocol(clientDomain+'embed-widgets/js/shadowbox.css') );
		headTag.appendChild(shadowBoxStyle);		
		
		
		//initialize shadowbox
		var shadowBoxScript2 = document.createElement('script');
		shadowBoxScript.setAttribute('type', 'text/javascript');
		shadowBoxScript2.text = '';
		headTag.appendChild(shadowBoxScript2);		
		
		
		
		workingDiv = document.getElementById(channelUniqueId);

		workingDiv.style.width = mainPageWidth+'px';
		workingDiv.style.margin = '10px auto';
		
		/*
		workingDiv.innerHTML += '<iframe id="embed-page" scrolling="no" frameborder="0" style="width: '+mainPageWidth+'px; height: '+mainPageHeight+'px; margin: 10px auto; border: '+mainPageBorderStyle+';" src="'+applyProtocol(clientDomain+'embed-widgets/popup/popupWidgetMain.php')+'" ></iframe>';
		*/
		var embedPlayer = document.createElement('div');
		embedPlayer.setAttribute('id', currentEmbedDivId);
	
		
		//put the watch now link on top of the iframe
		//get the position of the working div	
		tempWorkingDiv = workingDiv;
		var curleft = curtop = 0;
		if (tempWorkingDiv.offsetParent) {
			do {
				curleft += tempWorkingDiv.offsetLeft; //will be the left of the frame
				curtop += tempWorkingDiv.offsetTop; //will be the top of the frame
			} while (tempWorkingDiv = tempWorkingDiv.offsetParent);
		}										
		
		//alert('left :'+curleft+'\ntop: '+curtop);
		
		/* */
		//styling for the watchNowDiv for CEO
		var watchNowStyle = '';
		watchNowStyle += '#watchNowDiv{ \n';
		watchNowStyle += '	clear: both;\n';
		watchNowStyle += '	color: #fff;\n';
		watchNowStyle += '	text-align: right;\n';		
		watchNowStyle += '	font-weight: bold;\n';
		watchNowStyle += '	padding-right: 20px;\n';
		watchNowStyle += '	position: relative;';
		
		/** CEO Show
		watchNowStyle += '	margin: 10px;\n';
		watchNowStyle += '	top: -70px;';
		watchNowStyle += '	left: 0px;';
		*/
		watchNowStyle += '}\n';
		watchNowStyle += '#watchNowDiv #watchNowText{\n';
		watchNowStyle += '	font-size: 16px;\n';
		watchNowStyle += '	font-weight: bold;	\n';
		watchNowStyle += '	color: #fff;';	
		watchNowStyle += '	font-family: Arial,Tahoma,Verdana,Georgia,serif;';
		watchNowStyle += '}\n';
		watchNowStyle += '#watchNowImg{\n';
		watchNowStyle += '	vertical-align: -14px;\n';	
		watchNowStyle += '	border: 0px none;';
		watchNowStyle += '}\n';
		watchNowStyle += '#watchNowDiv a{\n';
		watchNowStyle += '	text-decoration: none;\n';
		watchNowStyle += '	outline: none;';
		watchNowStyle += '}	\n';
		watchNowStyle += '#closeBoxDiv{\n';
		watchNowStyle += '	height: 27px;\n';
		watchNowStyle += '	background-color: #e8e8e8;\n';
		watchNowStyle += '   padding: 0 5px;\n';
		watchNowStyle += '}\n';
		watchNowStyle += '#closeBoxDiv a{\n';
		watchNowStyle += '   color: #656766;\n';
		watchNowStyle += '   float: right;\n';
		watchNowStyle += '   font-family: arial;\n';
		watchNowStyle += '   font-size: 12px;\n';
		watchNowStyle += '   margin-top: 6px;\n';
		watchNowStyle += '   text-decoration: none;\n';
		watchNowStyle += '}\n';
		watchNowStyle += '#sb-wrapper{   \n';
		watchNowStyle += '   width: '+(playerPageWidth+10)+'px !important;\n';		
		watchNowStyle += '}   \n';
		watchNowStyle += '#sb-wrapper-inner{   \n';
		watchNowStyle += '   height: '+(playerPageHeight+30)+'px !important;\n';		
		watchNowStyle += '}   \n';
	
		watchNowStyle += '   \n';
		watchNowStyle += '   \n';
		
		/**
		Whatever style you add here should be added on the if statement below
		*/
		
		watchNowStyle += '	'+customStyle;


    
    
    
    
    
    
    
        
			
			
				
		
		var watchNowStyleTag = document.createElement('style');
		watchNowStyleTag.setAttribute('type', 'text/css');
			
		//if IE 8 or lower
		if( (navigator.userAgent.indexOf('MSIE 8') != -1) || (navigator.userAgent.indexOf('MSIE 7') != -1) ){
			
			/** CEO SHOW
			workingDiv.innerHTML += '<style> #watchNowDiv{ clear: both; color: #fff; text-align: right; margin: 10px; font-weight: bold; padding-right: 20px; position: relative; top: -70px; left: 0px; outline: none; } #watchNowDiv #watchNowText{ font-size: 18px; font-family: Arial,Tahoma,Verdana,Georgia,serif; font-weight: bold; } #watchNowImg{ vertical-align: middle; border: 0px none; } #watchNowDiv a{ text-decoration: none; border: 0px none; color: #fff; outline: none; }</style><div id="watchNowDiv"><a href="javascript:void(0)" onclick="watchEmbedNow(\''+channelUniqueId+'\'); return false;"><img id="watchNowImg" src="'+ applyProtocol(clientDomain+'embed-widgets/ceo/images/vistage/vistageEmbedWidget_play.png')+'" /><span id="watchNowText" >Watch Now</span></a></div>';
			/* */
			/*
			workingDiv.text += '<style> #watchNowDiv{ clear: both; color: #fff; text-align: right; margin: 10px; font-weight: bold; padding-right: 20px; position: relative; top: -70px; left: 0px; outline: none; } #watchNowDiv #watchNowText{ font-size: 18px; font-family: Arial,Tahoma,Verdana,Georgia,serif; font-weight: bold; } #watchNowImg{ vertical-align: middle; border: 0px none; } #watchNowDiv a{ text-decoration: none; border: 0px none; color: #fff; outline: none; }#closeBoxDiv { background-color: #E8E8E8; height: 27px; padding: 0 5px; }#closeBoxDiv a { color: #656766; float: right; font-family: arial; font-size: 12px; margin-top: 6px; text-decoration: none; }</style><div id="watchNowDiv"><a href="javascript:void(0)" onclick="watchEmbedNow(\''+channelUniqueId+'\'); return false;"><img id="watchNowImg" src="'+ applyProtocol(clientDomain+'embed-widgets/images/Naylor-Banner-V2.jpg')+'" /></a></div>';
			*/
			var rules = document.createTextNode('#watchNowDiv{ clear: both; color: #fff; text-align: right; margin: 10px; font-weight: bold; padding-right: 20px; position: relative; top: -70px; left: 0px; outline: none; } #watchNowDiv #watchNowText{ font-size: 18px; font-family: Arial,Tahoma,Verdana,Georgia,serif; font-weight: bold; } #watchNowImg{ vertical-align: middle; border: 0px none; } #watchNowDiv a{ text-decoration: none; border: 0px none; color: #fff; outline: none; }#closeBoxDiv { background-color: #E8E8E8; height: 27px; padding: 0 5px; }#closeBoxDiv a { color: #656766; float: right; font-family: arial; font-size: 12px; margin-top: 6px; text-decoration: none; } #sb-wrapper{width: '+(playerPageWidth+10)+'px !important;} #sb-wrapper-inner{height: '+(playerPageHeight+30)+'px !important;}');
			
			var ieStyling = document.createElement('style');
			ieStyling.type = 'text/css';
			
			if(ieStyling.styleSheet){
				ieStyling.styleSheet.cssText = rules.nodeValue;
			}
			else{
				ieStyling.appendChild(rules);
			}
			
			var imageTag = document.createElement('img'); 
			imageTag.src = applyProtocol(clientDomain+'sponsor-template-embed/images/'+mainBannerImage);
			imageTag.id = 'watchNowImg';
			
			
			var anchorTag = document.createElement('a'); 
			anchorTag.href = "javascript:void(0)";
			anchorTag.onclick = function() {
				watchEmbedNow(channelUniqueId); 
				return false;
			};
			
			anchorTag.appendChild(imageTag);
			
			workingDiv.appendChild(ieStyling);
			workingDiv.appendChild(anchorTag);
			
			
			
		}
		else{
			watchNowStyleTag.innerHTML = watchNowStyle;
			
			workingDiv.appendChild(watchNowStyleTag);			
			
			var watchNowDiv = document.createElement('div');
			watchNowDiv.setAttribute('id', 'watchNowDiv');
			/**  CEO SHOW
			watchNowDiv.innerHTML = '<a href="javascript:void(0)" onclick="watchEmbedNow(\''+channelUniqueId+'\'); return false;"><img id="watchNowImg" src="'+ applyProtocol(clientDomain+'embed-widgets/ceo/images/vistage/vistageEmbedWidget_play.png')+ '" /><span id="watchNowText" >Watch Now</span></a>';	
			workingDiv.appendChild(watchNowDiv);
			/* */
			
			watchNowDiv.innerHTML = '<a href="javascript:void(0)" onclick="watchEmbedNow(\''+channelUniqueId+'\'); return false;"><img id="watchNowImg" src="'+ applyProtocol(clientDomain+'sponsor-template-embed/images/'+mainBannerImage)+ '" /></a>';	
			workingDiv.appendChild(watchNowDiv);
			
		}
		
		
	
		
		
		
}


function watchEmbedNow(channelUniqueId){
	Shadowbox.init({
		skipSetup: true
	});
	var workingDiv = document.getElementById(channelUniqueId);
	
	/*
	//get the dimensions of the screen
	var iFrameX = (screen.availWidth - 545) /2;
	var iFrameY = (screen.availHeight - 750) / 2;	
	
	alert("iFrameX: " + iFrameX);
	alert("iFrameY: " + iFrameY);
	*/
	
	/*
	//iFrame for the video embed
	var videoEmbedFrame = document.createElement('iframe');
	videoEmbedFrame.setAttribute('id','videoEmbed');
	videoEmbedFrame.setAttribute('scrolling','no');
	videoEmbedFrame.setAttribute('frameborder','0');
	videoEmbedFrame.setAttribute('src',applyProtocol('https://websitevideocenter.com/embed-widgets/ceo/vistagePlayer.php'));
	videoEmbedFrame.setAttribute('style','width: 545px; height: 750px; margin: 10px auto;');
	*/
	
	var videoEmbedFrame = '<center><div id="closeBoxDiv" style="background-color: #E8E8E8; height: 23px; padding: 0 5px;"><a style="color: #656766; float: right; font-family: arial; font-size: 12px; margin-top: 4px; text-decoration: none;" href="javascript: Shadowbox.close();">Close <strong>or ESC Key</a></div><iframe id="embed-page" scrolling="no" frameborder="0" style="width: '+playerPageWidth+'px; height: '+playerPageHeight+'px; margin: 0 auto;" src="'+applyProtocol(clientDomain+'sponsor-template-embed/embedPopupPlayer.php?loc='+location.href)+'" ></iframe></center>';
	
	//workingDiv.appendChild(videoEmbedFrame);	
    Shadowbox.open({
			content:    videoEmbedFrame,
			player:     "html",        
			height:     playerPageHeight,
			width:      playerPageWidth,
			modal:			false,        			
			options: { 
				viewportPadding: "0",
				handleOversize: "none",  
				animate:		false,
				onFinish: checkBrowserSize        	  
			}
    });	
    
}

function checkBrowserSize(){
	if (document.body && document.body.offsetWidth) {
		winW = document.body.offsetWidth;
		winH = document.body.offsetHeight;
	}
	if (document.compatMode=='CSS1Compat' && document.documentElement && document.documentElement.offsetWidth ) {
		winW = document.documentElement.offsetWidth;
		winH = document.documentElement.offsetHeight;
	}
	if (window.innerWidth && window.innerHeight) {
		winW = window.innerWidth;
		winH = window.innerHeight;
	}
	
	
	if( document.getElementById('sb-container') ){
		if(winW <= (playerPageWidth + 40) ){
			document.getElementsByTagName('body')[0].style.overflow = 'scroll';
			document.getElementById('sb-container').style.position = 'absolute';
			document.getElementById('sb-overlay').style.position = 'fixed';			
			//alert(winW);
		}
		else{
			document.getElementById('sb-container').style.position = 'fixed';
		}
		
		if(winH <= (playerPageHeight + 40) ){
			document.getElementsByTagName('body')[0].style.overflow = 'scroll';
			document.getElementById('sb-container').style.position = 'absolute';
			document.getElementById('sb-overlay').style.position = 'fixed';
			//alert(winH);
		}	
		else{
			document.getElementById('sb-container').style.position = 'fixed';
		}
		
	}
}

function addLoadEventChannelEmbed(func) {
    var oldonload = window.onload;
    
    try{
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            eval(func);
        }    	 
    }
    catch(e){
    	 window.onload = func;
    }
}

function applyProtocol(url_string){
	var final_url = '';
	var protocol = (("https:" == document.location.protocol) ? "https" : "http");
	
	if(url_string == ''){
		final_url = protocol;
	}
	else if( (url_string.substring(0, 6) == 'https:') && (protocol == 'http') ){
		final_url = 'http:'+url_string.substring(6, url_string.length);
	}
	else if( (url_string.substring(0, 5) == 'http:') && (protocol == 'https') ){
		final_url = 'https:'+url_string.substring(5, url_string.length);
	}
	else{
		final_url = url_string;
	}
	
	return final_url;
}

window.onresize = function(event) {
	checkBrowserSize();
}


