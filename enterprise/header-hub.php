<?php
/**
 * filename: header.php
 * description: this will have all the information needed on the header of each page
 * author: Jullie Quijano
 * date created: 2014-03-24
 */
global $wp_query, $wb_ent_options, $postId, $privatePostsIds, $unlistedPostsIds, $current_lang, $curlang, $catlibrary, $moretext, $lesstext;

$wb_ent_options = get_option('wb_ent_options');

$srvaddress = $wb_ent_options['serveruser'].".".$wb_ent_options['serveraccount'];
if( $_SERVER['HTTPS'] == 'on' ){
   $protocol = "https";
}
else{
   $protocol = "http";
}
if (($_SERVER['HTTP_HOST'] == $srvaddress."/home") || ($_SERVER['HTTP_HOST'] == $srvaddress) && $wb_ent_options["channeldomain"] != $srvaddress)
{
header('Location: '.$protocol.'://'.$wb_ent_options["channeldomain"]);
exit;
}

$current_lang = get_locale();
$moretext = 'more'; // for the description [more]
switch($current_lang){
        case 'en_US' :
        $catlibrary =  $wb_ent_options['videocats']['library']; // English Library
        $curlang = ''; // used to select the library EN when en_US
        break;
        case 'fr_FR' :
        $catlibrary = $wb_ent_options['videocats']['library-fr_FR']; // French Library
        $curlang = '-'.$current_lang; // used to select the library FR when fr_FR
        $moretext = 'Plus'; // for the desctiption [plus]
        $lesstext = 'Moins';
        break; 
        default:
          $catlibrary =  $wb_ent_options['videocats']['library'];
          $curlang = '';
          break;
        }
        
        
/*        
$home = ''; // use the home page header and footer if page is home-en of home-fr
if( is_page('home') || is_page('page-daccueil')){
$home = '-home';
}*/        
$ipRestrictionArray = explode(',', $wb_ent_options['workerbeeip'] );

$allowedIps = array();
foreach($ipRestrictionArray as $currentIp){
    if(trim($currentIp) != '' ){
        $allowedIps[] = trim($currentIp);
    }
    
}

$userIp = trim($_SERVER['REMOTE_ADDR']);

if( ($wb_ent_options['devmode'] && !in_array($userIp, $allowedIps)) || trim($wb_ent_options['workerbeeip']) == '' )
{
echo 'Please check back later. Thanks.';
exit;
}  

$current_page = getCurrentAPFPage();


//get current postId
if( $wb_ent_options['hashome'] && (trim($current_page) == $wb_ent_options['homelink']) ){
    
   if($wb_ent_options['channelformat'] == 'feature' && $wb_ent_options['videocats']['feature'] != 0){
      $postId = wb_get_most_recent($wb_ent_options['videocats']['feature']);
   }
   else if($wb_ent_options['channelformat'] == 'webcast' && $wb_ent_options['videocats']['webcast'] != 0){
      $postId = wb_get_most_recent($wb_ent_options['videocats']['webcast']);
   }
   else{
      $postId = wb_get_most_recent($wb_ent_options['videocats']['library']);
   }   
}
else if( trim($current_page) == $wb_ent_options['homelink'] )  {
   $postId = wb_get_most_recent($wb_ent_options['videocats']['library']);
}  
else{
   $postId = $wp_query->post->ID;  
}

//echo '$postId is '.$postId;

$video = wb_get_post_details($postId);



if (trim($post_id) != '' && (is_null($video) || count($video) < 1)) {
    $video = wb_get_page_details($postId);
}
?><!doctype html>  
<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
    <head>
<?php
if (isset($wb_ent_options['customstyle']['favicon']) && trim($wb_ent_options['customstyle']['favicon']) != '') {
    echo '<link rel="shortcut icon" href="' . $wb_ent_options['customstyle']['favicon'] . '" />';
}
?>

        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<?php
if ($_SERVER['REMOTE_ADDR'] == $wb_ent_options['workerbeeip']) {
    ?>
            <meta http-equiv="Expires" content="Tue, 01 Jan 2000 12:12:12 GMT">
            <meta http-equiv="Pragma" content="no-cache">
            <?php
        }
        if (in_category($wb_ent_options['videocats']['unlisted'], $postId)) {
            ?>
            <meta name="robots" content="noindex">
            <?php
        }
        ?>        
        <meta name="Title" content="<?php echo $wb_ent_options['channelname'] ?>" />
        <meta name="Keywords" content="<?php echo strip_tags(get_the_tags()); ?>" />
        <?php 
          if($video['description']){ ?>
            <meta name="Description" content="<?php echo wb_format_string($video['description'], false, true, $maxChar = 100, '... '); ?>" />        
          <?php } else {?>
            <meta name="Description" content="<?php echo $wb_ent_options['channeldesc']?>"/>
          <?php } ?>
        <title><?php
        if (is_single()) {
            if( ( !is_user_logged_in() ) && (in_array( $postId,  $privatePostsIds)  ) ){
                bloginfo('name');
                print ' | Not Found';
            }
            else{
                single_post_title();
            }
        } elseif (is_home() || is_front_page()) {
            bloginfo('name');
            print ' | ';
            bloginfo('description');
            get_page_number();
        } elseif (is_page()) {
            single_post_title('');
        } elseif (is_search()) {
            bloginfo('name');
            print ' | Search results for ' . wp_specialchars($s);
        } elseif (is_404()) {
            bloginfo('name');
            print ' | Not Found';
        } else if(is_tag()){
            bloginfo('name');
          echo " | ";
          single_tag_title();
        } else {
            bloginfo('name');
            wp_title('|');
        }

        echo ' - ' . $wb_ent_options['channelname'];        
        ?></title>   
        
        <?php
            $post_title = $wb_ent_options['channelname'];
            $post_permalink = 'https://'.$wb_ent_options['channeldomain'];
            $post_thumb = $wb_ent_options['defaultsmlthumb'];
            $post_desc = $wb_ent_options['channeldesc'];

         if($video){ 
            $post_title = htmlentities($video['title']);
            $post_permalink = $video['postLink'];
            $post_thumb = $video['largeThumb'];
            $post_desc = wb_format_string($video['desc'], false, true, 100,'... ');
         }        
        ?>
        <meta property="og:title" content="<?php echo $post_title; ?>" />
        <meta property="og:type" content="tv_show" />
        <meta property="og:url" content="<?php echo $post_permalink; ?>" />
        <meta property="og:image" content="<?php echo $post_thumb; ?>" />
        <meta property="og:site_name" content="<?php echo $wb_ent_options['channelname']; ?>" />
        <meta property="og:description" content="<?php echo $post_desc; ?>" />
        <link rel="image_src" type="image/jpeg" href="<?php echo $post_thumb; ?>" />   
        <meta property="fb:app_id" content="<?php echo $wb_ent_options['fbappid']; ?>" />        
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">






        <!-- wordpress head functions -->
<?php wp_head(); ?>
        <!-- end of wordpress head -->

        <!-- theme options from options panel -->
<?php get_wpbs_theme_options(); ?>

        <!-- typeahead plugin - if top nav search bar enabled -->
<?php require_once(get_template_directory() . '/includes/typeahead.php'); ?>

        <!-- show message to upgrade browser -->
        <script type="text/javascript"> 
        var $buoop = {}; 
        $buoop.ol = window.onload; 
        window.onload=function(){ 
         try {if ($buoop.ol) $buoop.ol();}catch (e) {} 
         var e = document.createElement("script"); 
         e.setAttribute("type", "text/javascript"); 
         e.setAttribute("src", "//browser-update.org/update.js"); 
         document.body.appendChild(e); 
        } 
        </script> 

        <script type="text/javascript">
            if (document.all
                    && !window.opera
                    && typeof HTMLDocument == 'object'
                    && typeof HTMLCollection == 'object')//filter IE8
            {
                HTMLCollection.prototype.item =
                        function(id)
                        {
                            return((this == document.all)
                                    ? document.getElementById(id)//document.all
                                    : this[id]//maintain native behaviour for other collections
                                    );
                        }
            }
            if (typeof console == "undefined") {
                this.console = {log: function() {
                    }};
            }

        </script>

        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/library/js/responsive/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/library/js/responsive/jquery-ui.min.js"></script>



        <!--             RESPONSIVE MENU                      -->

        <!-- NEW MENU -->
        <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive/responsive-menu.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive/jquery.mmenu.all.css" />
        <style type="text/css">
            .mm-menu li.img a
            {
                font-size: 16px;
            }
            .mm-menu li.img a img
            {
                float: left;
                margin: -5px 10px -5px 0;
            }
            .mm-menu li.img a small
            {
                font-size: 12px;
            }
            html{
                margin-top: 0 !important;
            }
        </style>




        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/library/js/responsive/jquery.mmenu.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/library/js/responsive/jquery.mmenu.searchfield.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/library/js/responsive/jquery.mmenu.header.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/library/js/responsive/jquery.mmenu.labels.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/library/js/responsive/jquery.mmenu.counters.js"></script>
        <link rel="stylesheet" href="<?php echo get_site_url(); ?>/wp-content/plugins/workerbee-live-lms-ppv-v2/js/shadowbox/shadowbox.css?v=<?= time(); ?>" type="text/css" media="all" />
        <script type="text/javascript" src="<?php echo get_site_url(); ?>/wp-content/plugins/workerbee-live-lms-ppv-v2/js/shadowbox/shadowbox.js"></script>        
        <script type="text/javascript" src="<?php echo get_site_url(); ?>/wp-content/plugins/workerbee-live-lms-ppv-v2/js/shadowbox/shadowbox-quirks.js"></script>         

        <script type="text/javascript">

            (function($) {
                // The menu on the left
                $(function() {
                var language = "<?php echo $current_lang; ?>";
                 //alert(language);
                  //alert("hi");
                if(language == "fr_FR"){
                
                $('#share-box').html('Partager');
                }
                <?php if($wb_ent_options[responsivemenu] == 'left'){ ?>
                    $('nav#menu-left').mmenu();
                  <?php } else if($wb_ent_options[responsivemenu] == 'right') { ?>
                    // The menu on the right

                     
                     var $menu = $('nav#menu-left');
                     
                     $menu.mmenu({
                     position : 'right',
                     classes  : 'mm-light',
                     counters : true,     
                     labels  : {
                     fixed  : !$.mmenu.support.touch
                     }
                     });

                
                <?php } else{ ?>
                  $('nav#menu-left').mmenu();
                <?php } ?>
                    });

            })(jQuery);
        </script>
        <?php

 if($wb_ent_options[responsivemenu] == 'right'){ ?>
<style>
#header > a:first-child, .header > a:first-child {
  right:10px;
  left: auto;
}
</style> 
<?php } ?> 
        <!-- WB JavaScript -->
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/sitescript.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.jtruncate.pack.js"></script>
        <script type="text/javascript">var switchTo5x = true;</script>

        <?php
        if( $_SERVER['HTTPS'] == 'on' ){
        ?>
        <script language="JavaScript" type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>
        <script src="https://sadmin.brightcove.com/js/APIModules_all.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://www.google-analytics.com/ga.js"></script>     
        <script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>        
        <?php
        }
        else{
        ?>
        <script src="http://admin.brightcove.com/js/APIModules_all.js" type="text/javascript"></script>
        <script src="http://admin.brightcove.com/js/BrightcoveExperiences.js" type="text/javascript"></script> 
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>       
        <?php
        }   
        ?>
        


        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.idTabs.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/more_info_functions.js"></script>     

        
        
        <script type="text/javascript">
            stLight.options({
                publisher: '<?php echo $wb_ent_options['sharethispubid']; ?>',
                tracking: 'google',
                doNotHash: true,
                doNotCopy: true,
                hashAddressBar: false
            });
        </script>
        <!-- Changing the $playerWidth and $playerHeight -->
        <style>
            
            <?php
            $mobile = mobile_device_detect();
            if (is_array($mobile)) {
                ?>
                #sharebar #commentButton{
                    display: none !important;
                }
    <?php
} else {
    ?>
                #sharebar #commentButton{
                    display: inline-block !important;
                }   
    <?php
}
?>
        </style>

        <!-- WB Stylesheets  -->
        <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/library.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/reset.css" />  
        <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/site-style.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/tabs.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsive/wbresponsive.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/wb-enterprise.css" />
        <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700|Open+Sans:700,600,400' rel='stylesheet' type='text/css'>
        <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/override.css" />
        <!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsive/ie8-fix.css" />
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/ie8-fix.js"></script>
        <![endif]-->
        <!--[if gt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsive/ie.css" />       
        <![endif]-->

        <!--[if (gte IE 6)&(lte IE 8)]>
        <script type="text/javascript" src="/js/ama/selectivizr.js"></script> 
        <![endif]-->
        <?php
        if (isset($wb_ent_options['customstyle']['customie8css']) && trim($wb_ent_options['customstyle']['customie8css']) != '') {
            $customCssArray = explode(',', trim($wb_ent_options['customstyle']['customie8css']));
            foreach ($customCssArray as $currentCss) {
                ?>
                <!--[if lt IE 9]>
                <link rel="stylesheet" type="text/css" href="<?php echo trim($currentCss); ?>" />
                <![endif]-->
                <?php
            }
        }
        ?>
        <?php
        if (isset($wb_ent_options['customstyle']['customcss']) && trim($wb_ent_options['customstyle']['customcss']) != '') {
            $customCssArray = explode(',', trim($wb_ent_options['customstyle']['customcss']));
            foreach ($customCssArray as $currentCss) {
                ?>
                <link rel="stylesheet" type="text/css" href="<?php echo trim($currentCss); ?>" />
                <?php
            }
        }
        ?>

        <?php
        if (isset($wb_ent_options['customstyle']['customjs']) && trim($wb_ent_options['customstyle']['customjs']) != '') {
            $customJsArray = explode(',', trim($wb_ent_options['customstyle']['customjs']));
            foreach ($customJsArray as $currentJs) {
                ?>            
                <script type="text/javascript" src="<?php echo trim($currentJs); ?>"></script>
        <?php
    }
}
?>            
	<script src='https://www.google.com/recaptcha/api.js'></script>
    </head>

    <body <?php body_class(); ?>>   
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                    <?php
                    foreach ($wb_ent_options['analytics'] as $key => $tracker) {
                        echo '<!-- ' . $tracker['name'] . ' -->';
                        ?>

                ga('create', '<?php echo $tracker['code']; ?>', 'auto', {'name': 'pageTracker<?php echo $key; ?>'});
                ga('pageTracker<?php echo $key; ?>.send', 'pageview');
                
                        <?php
                        if( trim($_GET['utm_source']) != '' && trim($_GET['utm_medium']) != '' && trim($_GET['utm_campaign']) != '' ){
                        ?>
                ga('pageTracker<?php echo $key; ?>.set', 'campaignName', '<?php echo $_GET['utm_campaign']; ?>');
                ga('pageTracker<?php echo $key; ?>.set', 'campaignSource', '<?php echo $_GET['utm_source']; ?>');
                ga('pageTracker<?php echo $key; ?>.set', 'campaignMedium', '<?php echo $_GET['utm_medium']; ?>');
                ga('pageTracker<?php echo $key; ?>.set', 'campaignKeyword', '<?php echo $_GET['utm_term']; ?>');
                ga('pageTracker<?php echo $key; ?>.set', 'campaignContent', '<?php echo $_GET['utm_content']; ?>');                 
                        <?php
                        }
                        else{
                        ?>
                ga('pageTracker<?php echo $key; ?>.set', 'campaignName', 'Channel');
                ga('pageTracker<?php echo $key; ?>.set', 'campaignSource', 'APF');
                ga('pageTracker<?php echo $key; ?>.set', 'campaignMedium', '<?php echo (mobile_device_detect()) ? 'mobile' : 'desktop'; ?>');                              
                        <?php
                        }
                    }
                    ?>
        </script>
        
    
        <header role="banner" class="global-branding visible-desktop" id="<?php echo $wb_ent_options['customstyle']['headerid']; ?>">
            <div class="">
                <div class="clear-fix " id="<?php echo $wb_ent_options['customstyle']['headercontainer']; ?>" style="">
        <?php
        if (trim($wb_ent_options['customstyle']['headerhtml'.$curlang])) {
            //echo html_entity_decode($wb_ent_options['customstyle']['headerhtml'.$curlang]);
            include "wb-client-header-hub.php";
        } else if (trim($wb_ent_options['customstyle']['headerlogo'])) {
            ?>
                    <div id="def_header_container">
                        <a href="/" target="_top" title="<?php echo $wb_ent_options['channelname ']; ?>"><img src="<?php echo $wb_ent_options['customstyle']['headerlogo']; ?>" alt="<?php echo $wb_ent_options['channelname ']; ?>" /></a>
                    </div>    
            <?php
        }
        ?>
                </div>
            </div>
        </header>
        <!-- end header -->

<?php

include ( get_template_directory() . '/includes/navigation.php');

?>

<?php
if ($wb_ent_options['sharetype'] == 'floating') {
    ?>
            <!-- if floating share includes comment button -->
            <script type="text/javascript">
                function goToComment() {
                    if ($('#comments').length > 0) {
                        window.location = window.location + '#comment';
                        return false;
                    }
                    else {
                        window.location = $('meta[property="og:url"]').attr('content') + '#comment';
                        return false;
                    }

                }
            </script>
            <!-- facebook like -->

            <div id="fb-root"></div>

            <script>(function(d, s, id) {

                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                        return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=<?php echo $wb_ent_options['fbappid']; ?>";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>            
    <?php
}

if ($wb_ent_options['videolistinfo']['usestatic']) {
    ?>
            <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/resources/postInfo.json?v=<?php echo time(); ?>"></script>
    <?php
}
?>
<div id="" class="clearfix row-fluid">
<?php include ( get_template_directory() . '/includes/widgets/adsLeaderboardswCustom.php');  ?>
</div>