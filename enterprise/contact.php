<?php
/**
 * filename: page.php
 * description: this will be the default template to be used for the theme
 * author: Jullie Quijano
 * date created: 2014-03-25
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: Contact Page
 */
global $wb_ent_options, $wb_spam_exist;
get_header();
$wb_spam_exist = false;
$max = 199;
$uploadMax = 9;
$error = 0;
$errormsg = '';

$linkID = time();
$linkID .= (mt_rand(1, 9999999999)); // Random # between 1 and 9,999,999,999

$ipRestrictionArray = explode(',', $wb_ent_options['workerbeeip'] );
$allowedIps = array();
foreach($ipRestrictionArray as $currentIp){
    if(trim($currentIp) != '' ){
        $allowedIps[] = trim($currentIp);
    }
}
$userIp = trim($_SERVER['REMOTE_ADDR']);

if( !$wb_ent_options['contact']['canada'] ) {
$cCountrySelection = array("United States", "Canada", "Mexico", "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Terr.", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands/Malvinas", "Faroe Islands", "Fiji", "Finland", "France", "France", "Metropolitan", "French Guiana", "French Polynesia", "French Southern Terr.", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard &amp; McDonald Is.", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Lao People's Dem. Rep.", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagacar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Is.", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "St. Vincent &amp; Grenadines", "Samoa", "San Marino", "Sao Tome &amp; Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "S.Georgia/S.Sandwich Is.", "Spain", "Sri Lanka", "St. Helena", "St. Pierre &amp; Miquelon", "Sudan", "Suriname", "Svalbard &amp; Jan Mayen Is.", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "U.S. Minor Outlying Is.", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican (Holy See)", "Venezuela", "Viet Nam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis &amp; Futuna Is.", "Western Sahara", "Yemen", "Yugoslavia", "Zaire", "Zambia", "Zimbabwe");
}else{
$cCountrySelection = array("Canada", "United States", "Mexico", "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Terr.", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands/Malvinas", "Faroe Islands", "Fiji", "Finland", "France", "France", "Metropolitan", "French Guiana", "French Polynesia", "French Southern Terr.", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard &amp; McDonald Is.", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Lao People's Dem. Rep.", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagacar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Is.", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "St. Vincent &amp; Grenadines", "Samoa", "San Marino", "Sao Tome &amp; Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "S.Georgia/S.Sandwich Is.", "Spain", "Sri Lanka", "St. Helena", "St. Pierre &amp; Miquelon", "Sudan", "Suriname", "Svalbard &amp; Jan Mayen Is.", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "U.S. Minor Outlying Is.", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican (Holy See)", "Venezuela", "Viet Nam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis &amp; Futuna Is.", "Western Sahara", "Yemen", "Yugoslavia", "Zaire", "Zambia", "Zimbabwe");
}
if (isset($_POST['cSubmit'])) {

	
    //new recaptcha
    $curlString = 'https://www.google.com/recaptcha/api/siteverify?secret='.$wb_ent_options['recaptchapriv'].'&response='.$_POST['g-recaptcha-response'];

     $ch = curl_init();
     $timeout = 0; // set to zero for no timeout
     curl_setopt($ch, CURLOPT_URL, $curlString);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
     $file_contents = curl_exec($ch);
     echo '<!-- $file_contents is '.print_r($file_contents, true).' -->';
     curl_close($ch);
     $returndata = json_decode($file_contents);
    
        
    
    echo '<!-- $returndata is '.print_r($returndata, true).' -->';
      
   
/*
    require_once('resources/recaptcha/recaptchalib.php');
    $privatekey = $wb_ent_options['recaptchapriv'];
    $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
*/
    //if (!$resp->is_valid) {
    if( !$returndata->success ){
        $error = 1;
        $errormsg .= "The reCAPTCHA wasn't entered correctly. Please try it again.<br />" .
           "<!--(reCAPTCHA said: " . $resp->error . ")-->";
    }

    function toDatabase($c, $linkID) {
        global $wpdb;

        $wpdb->insert(
        		'wb_contact',
        		array(
        				'link_id' 	=> $linkID,
        				'timestamp' => time(),
        				'name'	=> $c['fullname']['value'],
        				'email' 	=> $c['email']['value'],
        				'country' 	=> $c['country']['value'],
        				'category' 	=> $c['category']['value'],
        				'story' 	=> $c['question']['value'],
        				'firstname' => $c['firstname']['value'],
        				'lastname' 	=> $c['lastname']['value'],
        				'city'	=> $c['city']['value'],
        				'state' 	=> $c['state']['value'],
        				'phone' 	=> $c['phone']['value']
        		),
        		array( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
        		);
    
    }

    function sendEmails($c, $clientChannel, $contactURL, $uploadCount, $file, $to, $subject) {
        global $wb_spam_exist;
        /*
        if($c['fullname']['value'] != 'n/a'){
        $from = $c['fullname']['value'] .' <' . $c['email']['value'] . '>';
        $message = "Name: " . stripslashes($c['firstname']['value'])." ".$c['lastname']['value']." \r\n";
        }else{            
        $from = $c['firstname']['value'] ." ".$c['lastname']['value']. ' <' . $c['email']['value'] . '>';    
        $message = "Name: " . stripslashes($c['firstname']['value'])." ".$c['lastname']['value']." \r\n";
        }
        */

        //$from  = CLIENT_NAME . ' <' . CHANNEL_EMAIL . '>';
        global $wb_ent_options;
        $from  = $wb_ent_options['channelname'] . ' <' . $wb_ent_options['channelemail'] . '>';
        $headers = array();
        $headers[] = "From: $wb_ent_options[channelname] <$wb_ent_options[channelemail]>";
        //$headers[] = 'Bcc: Lourice 2 <lourice.capili@workerbeetv.com>';
        //$headers = '';
        //$headers .= 'From: ' . stripslashes($from) . "\r\n";
        if( ($wb_ent_options['devmode'] && !in_array($userIp, $allowedIps)) || trim($wb_ent_options['workerbeeip']) == '' )
        {
        $headers[] = 'Bcc: AJ Batac <aj.batac@workerbee.tv>, Dan Stevens <dan.stevens@workerbee.tv>, Workerbee TV <workerbeetv@gmail.com>';
        }else{
        $headers[] = 'Bcc: Lourice 2 <lourice.capili@workerbeetv.com>';
        }
        $name = stripslashes($c['firstname']['value'])." ".$c['lastname']['value'];
        if($c['fullname']['value'] && $c['fullname']['value']!= 'n/a'){
            $name = stripslashes($c['fullname']['value']);
        }
        $message = "Name: " . $name . " \r\n";
        if($c['email']['value'] != 'n/a'){
        $message .= "Email: " . stripslashes($c['email']['value'])." \r\n";
        }
        if($c['city']['value'] != 'n/a' || $c['state']['value'] != 'n/a' || $c['country']['value'] != 'n/a'){
        $message .= "Location: ".stripslashes($c['city']['value'])." ".stripslashes($c['state']['value']).", " . stripslashes($c['country']['value'])." \r\n";
        }
        else{
        //$message .= "Location: ".stripslashes($c['country']['value'])." \r\n";    
        }
        if($c['phone']['value'] != 'n/a'){
        $message .= "Phone: ".stripslashes($c['phone']['value'])." \r\n";
        }
        if($c['category']['value'] != 'n/a'){
        $message .= "Category: " . stripslashes($c['category']['value'])." \r\n";
        }
        if($c['question']['value'] != 'n/a'){
        $message .= "Question/Comment: " . stripslashes($c['question']['value'])." \r\n";
        }
        $message .= "Files: ";

        if ($uploadCount >= 1) {
            $message .= $uploadCount . PHP_EOL;
            foreach ($file as $f) {
                $message .= '	' . $contactURL . $f . PHP_EOL;
            }
        } else {
            $message .= "None";
        }
        //wp_mail( $to, 'subject', 'message', $headers, $attachments );
        
        function checkString($wb_string){
        	global $wb_spam_exist;
        	$filter_list = array();
       		array_push($filter_list, "и", "Й", "И", "в", "я", "Г", "Б", "з", "Я", "Дозировка","http","https","href", "выпуска", "Форма", "url", "shipping!", "today!", "here!", "fingertips!", "online!", "#1", "100% free", "50%", "100%", "xml", "XML");

        	foreach ($filter_list as $key) {
        		if (strpos($wb_string, $key) !== false) {
        			$wb_spam_exist = true;
        		}
        	}
        }
        
        checkString($message);
        
        if ( !$wb_spam_exist ){   
        	wp_mail($to, $subject, $message, implode("\r\n", $headers), "-f".$wb_ent_options['channelemail']);
        }else{
        	wp_mail("webmaster@workerbee.tv", "SPAM - " . $subject, $message, implode("\r\n", $headers), "-f".$wb_ent_options['channelemail']);
        }
        
       // mail($to, $subject, $message, implode("\r\n", $headers), "-f".$wb_ent_options['channelemail']);
    }

    $linkID = $_POST['linkID'];
    
    if(isset($_POST['cName'])){
    $c['fullname'] = array('value' => $_POST['cName'], 'name' => 'Full Name');
    }else{
    $c['fullname'] = array('value' => 'n/a', 'name' => 'Full Name');    
    }
    
    if(isset($_POST['cfirstName'])){
    $c['firstname'] = array('value' => $_POST['cfirstName'], 'name' => 'First Name');
    }else{
    $c['firstname'] = array('value' => 'n/a', 'name' => 'First Name');    
    }
    if(isset($_POST['clastName'])){
    $c['lastname'] = array('value' => $_POST['clastName'], 'name' => 'Last Name');
    }else{
        $c['lastname'] = array('value' => 'n/a', 'name' => 'Last Name');    
    }
    if(isset($_POST['cEmail'])){
    $c['email'] = array('value' => $_POST['cEmail'], 'name' => 'Email');
    }else{
        $c['email'] = array('value' => 'n/a', 'name' => 'Email');    
    }
    
    if(isset($_POST['cPhone'])){
    $c['phone'] = array('value' => $_POST['cPhone'], 'name' => 'Phone');
    }else{
        $c['phone'] = array('value' => 'n/a', 'name' => 'Phone');    
    }
    
    if(isset($_POST['cCity'])){
    $c['city'] = array('value' => $_POST['cCity'], 'name' => 'City');
    }else{
        $c['city'] = array('value' => 'n/a', 'name' => 'City');    
    }
    
    if(isset($_POST['cState'])){
    $c['state'] = array('value' => $_POST['cState'], 'name' => 'State');
    }else{
        $c['state'] = array('value' => 'n/a', 'name' => 'State');    
    }
    
    if(isset($_POST['cCountry'])){
    $c['country'] = array('value' => $_POST['cCountry'], 'name' => 'Country');
    }else{
        $c['country'] = array('value' => 'n/a', 'name' => 'Country');    
    }
    
    if(isset($_POST['cCategory'])){
    $c['category'] = array('value' => $_POST['cCategory'], 'name' => 'Category');
    }else{
        $c['category'] = array('value' => 'n/a', 'name' => 'Category');    
    }
    
    if(isset($_POST['cQuestion'])){
    $c['question'] = array('value' => $_POST['cQuestion'], 'name' => 'Question');
    }else{
        $c['question'] = array('value' => 'n/a', 'name' => 'Question');    
    }
    
    if(isset($_POST['cCountry'])){
    $c['country']['value'] = $cCountrySelection[$c['country']['value']];
    }else{
        $c['country']['value'] = array('value' => 'n/a', 'name' => 'Country');    
    }
    $uploadCount = $_POST['uploadCount'];
	
    /*
    $query = sprintf("SELECT * FROM wb_contact_files WHERE link_id='%s'", $linkID);
    $result = mysql_query($query);
    $uploadString = '';
    $i = 0;
    $file = array();
    while ($row = mysql_fetch_array($result)) {
        $file[$i] = $row['filename'];
        $uploadString .= $file[$i] . ' <span>uploaded and renamed.</span><br />';
        $i++;
    }
	*/
    
    foreach ($c as $item) {
        $len = strlen($item['value']);
        switch ($item['name']) {
            case 'Question' || 'Country' || 'Full Name' || 'Last Name' || 'First Name' || 'City' || 'State' :
                switch ($item['value']) {
                    case '':
                        $errormsg .= 'Please enter a value for ' . $item['name'] . '.<br />';
                        $error = 1;
                        break;
                    default: break;
                }
                break;
            case 'Phone':
              switch ($item['value']) {
                  case '':
                      $errormsg .= 'Please enter a value for ' . $item['name'] . '.<br />';
                      $error = 1;
                      break;
                  case ($len < 7):
                      $errormsg .= 'Sorry, the phone number you entered is not valid.<br />';
                      $error = 1;
                  break;
                  default: break;
                }
              break;
            default:
                switch ($item['value']) {
                    case '':
                        $errormsg .= 'Please enter a value for ' . $item['name'] . '.<br />';
                        $error = 1;
                        break;
                    case ($len > $max):
                        $errormsg .= 'Sorry, ' . $item['name'] . ' must be less than ' . $max . ' characters. Current length is ' . $len . '.<br />';
                        $error = 1;
                        break;
                    default: break;
                }
                break;
        }
    }

    switch ($c['email']['value']) {
        case '': break;
        case (!preg_match("/^[a-z0-9_-][a-z0-9._-]+@([a-z0-9][a-z0-9-]*\.)+[a-z]{2,6}$/i", $c['email']['value'])):
            $errormsg .= 'Sorry, the email address you entered is not valid.<br />';
            $error = 1;
            break;
        default: break;
    }
    
    /*
    switch ($c['phone']['value']){
      $phonelen = strlen($c['phone']);
      case '':      
        break;
      case ($phonelen < 7):
        $errormsg .= 'Sorry, the phone number you entered is not valid.<br />';
        $error = 1;
        break;
      default: 
        break;
    }*/

    switch ($error) {
        case 0: toDatabase($c, $linkID);
            sendEmails($c, $wb_ent_options['channelname'], get_site_url() . '/' . 'wp-content/uploads/' . date('Y') . '/' . date('m'), $uploadCount, $file, $wb_ent_options['notification']['contact']['recipient'], $wb_ent_options['notification']['contact']['subject']);
            //sendEmails($c, $wb_ent_options['channelname'], get_site_url() . '/' . 'wp-content/uploads/' . date('Y') . '/' . date('m'), $uploadCount, $file, 'lourice.capili@workerbeetv.com', $wb_ent_options['notification']['contact']['subject']);
            //print_r($c);
            break;
        //case 0: toDatabase($c, $linkID); sendEmails($c, CLIENT_CHANNEL, SITE_ROOT.PATH_CONTACT, $uploadCount, $file, 'heather.scott@workerbee.tv', CONFORM_SUBJECT); break;
        default: break;
    }
}
if (have_posts()) : while (have_posts()) : the_post();
        ?>
        <div id="wb_ent_content" class="clearfix row-fluid">
            <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
                <div id="viewing-tips">
                    <h1><?php the_title(); ?></h1>

                     <?php if( trim($wb_ent_options['contact']['desc'] )){ 
                        echo html_entity_decode($wb_ent_options['contact']['desc']);
                    }else {
                    ?>
                    <p><?= _e('Have questions about us or our services? Please complete the form below and let\'s get the conversation started.', 'enterprise') ?></p>                                    
                    <?php } ?>
                    <?php if(!$wb_ent_options['contact']['noform']){ ?>
                    <form name="contactForm" id="contactForm" class="form-style" action="<?= $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data" method="post">
                        <fieldset class="span8">
                            <?php
                            if ($error != 0) {
                                echo '<div class="errors">' . $errormsg . '<br /></div>';
                            } else if (isset($_POST['cSubmit'])) {
                                foreach ($analytics as $key => $tracker) {
                                    echo '
			<!-- ' . $tracker['name'] . ' -->
			<script type="text/javascript">
			pageTracker' . $key . '._trackEvent(\'Forms\', \'Submit\', \'Contact\');
			</script>' . PHP_EOL;
                                }
                                echo '<h3>Thanks for your input. We appreciate and value contributions from our viewers.</h3>';
                            }
                            ?>
                            <div<?php if (isset($_POST['cSubmit']) && $error == 0) echo ' style="display:none"'; ?>>

                                <input type="hidden" name="linkID" id="linkID" value="<?= @$linkID ?>" />
                                <?php if( $wb_ent_options['contact']['fullname'] ){ ?>
                                <label><?= _e('Name', 'enterprise') ?></label>
                                <input class="styled" type="text" name="cName" id="cfirstName" value="<?= @$c['fullname']['value'] ?>" />
                                <?php } ?>
                                <?php if( $wb_ent_options['contact']['firstname'] ){ ?>
                                <label><?= _e('First Name', 'enterprise') ?></label>
                                <input class="styled" type="text" name="cfirstName" id="cfirstName" value="<?= @$c['firstname']['value'] ?>" />
                                <?php } ?>
                                <?php if( $wb_ent_options['contact']['lastname'] ){ ?>
                                <label><?= _e('Last Name', 'enterprise') ?></label>
                                <input class="styled" type="text" name="clastName" id="clastName" value="<?= @$c['lastname']['value'] ?>" />
                                <?php } ?>
                                <?php if( $wb_ent_options['contact']['city'] ){ ?>
                                <label><?= _e('City', 'enterprise') ?></label>
                                <input class="styled" type="text" name="cCity" id="cCity" value="<?= @$c['city']['value'] ?>" />
                                <?php } ?>
                                <?php if( $wb_ent_options['contact']['state'] ){ 
                                if( !$wb_ent_options['contact']['canada'] ) { ?>                               
                                <label><?= _e('State', 'enterprise') ?></label>
                                <?php }else{ ?>
                                <label><?= _e('Province', 'enterprise') ?></label>
                                 <?php } ?>
                                <input class="styled" type="text" name="cState" id="cState" value="<?= @$c['state']['value'] ?>" />
                                <?php } ?>
                                <?php if( $wb_ent_options['contact']['country'] ){ ?>
                                <label><?= _e('Country', 'enterprise') ?></label>
                                <select name="cCountry" id="cCountry">
                                    <?php
                                    foreach ($cCountrySelection as $key => $country) {
                                        if ($c['country']['value'] == $country)
                                            echo '<option value="' . $key . '" selected="selected">' . $country . '</option>
			';
                                        else
                                            echo '<option value="' . $key . '">';
                                            printf(__('%s', 'enterprise'),  $country);
                                            echo '</option>
			';
                                    }
                                    ?>
                                </select>
                                
                                <?php } ?>
                                <?php if( $wb_ent_options['contact']['email'] ){ ?>
                                <label><?= _e('Email', 'enterprise') ?></label>
                                <input class="styled" type="text" name="cEmail" id="cEmail" value="<?= @$c['email']['value'] ?>" />
                                <?php } ?>
                                <?php if( $wb_ent_options['contact']['phone'] ){ ?>
                                <label><?= _e('Phone', 'enterprise') ?></label>
                                <input class="styled" type="text" name="cPhone" id="cPhone" value="<?= @$c['phone']['value'] ?>" />
                                <?php } ?>

                                <?php if( $wb_ent_options['contact']['category'] ){ ?>
                                <label><?= _e('Category', 'enterprise') ?></label>
                                <select name="cCategory" id="cCategory">
                                    <?php
                                    $category = array('Content Submission', 'General Inquiry', 'Videos', 'Technical Question', 'Marketing and Advertising', 'Suggestion', 'Troubleshooting');
                                    $categoryfr_FR =  array('Commentaires sur le contenu', 'Renseignements généraux', 'Vidéos', 'Information technique', 'Marketing et publicité', 'Suggestion', 'Dépannage');
                                    if($curlang != "-fr_FR"){
                                    $selectedCategory =  $category;
                                    }else{
                                    $selectedCategory  = $categoryfr_FR;
                                    }
                                    
                                    foreach ($selectedCategory as $cat) {
                                        echo '<option value="' . $cat . '"' . (($cat == $c['category']['value']) ? ' selected="selected"' : '') . '>';
                                        printf(__('%s', 'enterprise'), $cat);
                                        echo ' </option>' . PHP_EOL;
                                    }
                                    ?>
                                </select>
                                <?php } ?>

                                <?php if( $wb_ent_options['contact']['question'] ){ ?>
                                <label><?= _e('Question / Comment', 'enterprise') ?></label>
                                <textarea rows="5" name="cQuestion" id="cQuestion"><?= @$c['question']['value'] ?></textarea>
                                <?php } ?>
                                <div class="spacing"></div>

                                <!--
                                <div id="attachments">
                                   <h5>If you would like to send us photos, or anything else, please use this form.</h5>
                                   <ul>
                                      <li>You can add up to 9 files.</li>
                                      <li>Files must be no larger than 100 MB.</li>
                                      <li>Please ensure that images are high-resolution JPEGs and video files are 640x480 .MOV format.</li>
                                   </ul>
                       
                                   <div id="uploadWrap"<?php if ($uploadCount >= $uploadMax) echo ' style="display:none"' ?>>
                                      <input type="hidden" name="uploadCount" id="uploadCount" value="<?= @$uploadCount ?>" /><br />
                                      <div id="upload">You have a problem with your javacript</div>
                                   </div>
                       
                                   <div id="filesUploaded" class="uploadsDiv"><?= @$uploadString ?></div><br />
                                </div>
                                -->
                                <?php
                                /*
                                require_once('resources/recaptcha/recaptchalib.php');
                                $publickey = $wb_ent_options['recaptchapub']; // you got this from the signup page
                                echo recaptcha_get_html($publickey, null, true);
                                * *
                                */
                                ?>
                                <div class="g-recaptcha" data-sitekey="<?php echo $wb_ent_options['recaptchapub']; ?>"></div>
                                <input type="submit" name="cSubmit" id="cSubmit" value="<?= _e('Send', 'enterprise') ?>" class="btn btn-primary" />

                            </div>
                        </fieldset>
                    </form>
                    <?php } ?>    

                    <?php
                endwhile;
            else:
                ?>
                <p>&nbsp;</p>
            <?php endif; ?>

        </div>
    </div>
    <?php
    get_sidebar();
    get_footer();
    ?>
