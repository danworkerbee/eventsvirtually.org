<?php
/**
 * filename: category.php
 * description: this will be the template to be used to display categories
 * author: Jullie Quijano
 * date created: 2014-03-25
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise
 * Template Name: On Demand Preview (N, page-category)
 */
 
global $wb_ent_options;

/*Get meta data*/
/*Title and desc would be title and description of page*/
$get_wb_page_cat_footer_banner_img = get_post_meta( $post->ID, 'wb_page_cat_footer_banner_img', true); //banner at the footer (example subscribe banner 468x60)
$get_wb_page_cat_footer_banner_url = get_post_meta( $post->ID, 'wb_page_cat_footer_banner_url', true); //click through url of banner
$get_wb_page_cat_header_banner_img = get_post_meta( $post->ID, 'wb_page_cat_header_banner_img', true); //banner at the header (example subscribe banner 468x60)
$get_wb_page_cat_header_banner_url = get_post_meta( $post->ID, 'wb_page_cat_header_banner_url', true); //click through
$get_wb_page_cat_footer_text = get_post_meta( $post->ID, 'wb_page_cat_footer_text', true); //footer text
$get_wb_page_cat_override_css = get_post_meta( $post->ID, 'wb_page_cat_override_css', true); //override css file
$get_wb_page_cat_custom_title = get_post_meta( $post->ID, 'wb_page_cat_custom_title', true); //if you want the title of page to be unique from <title>title | channel</title>

//get current page for title and content
$current_page = get_post();
get_header();
sharebar();
?>
<style>
.wb_page_cat_span12 img{max-height:168}.row-fluid .span9{width:45%;}
#wb_page_cat_footer,#wb_page_cat_header{text-align:center;padding:10px;}
#keyword_cloud, #archiveWidgetDiv, .anythingSlider {display:none !important}
#video-results{margin:20px 0}
</style> 
<?php
if( !empty($get_wb_page_cat_override_css) ){ ?>
<link rel="stylesheet" type="text/css" href="<?php echo $get_wb_page_cat_override_css ?>">
<?php } ?>
<div id="wb_ent_content" class="clearfix row-fluid">
  <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
		
			<div class="page-header page_cat"><h1 class="pagetitle">
				<?php if( !empty($get_wb_page_cat_custom_title) ){
					echo $get_wb_page_cat_custom_title;
				} else {
					echo $current_page->post_title; 
				} ?>
			</h1></div>			
			<div id="wb_divheaderinfo">
				<p><?php echo $current_page->post_content; ?></p>
				<?php sharebar_horizontal(); ?>
				<div id="wb_page_cat_header">
					<a href="<?php echo $get_wb_page_cat_header_banner_url ?>"><img src="<?php echo $get_wb_page_cat_header_banner_img ?>" /></a>
				</div>
			</div>	
			<?php //display coming-soon posts ?>
      <div id="video-results" class="video-list-1">
				<?php 
				//query posts from category in custom field meta, show unlimited					
				query_posts( array ('meta_key' => 'wb_ppv_prod_display_order', 
					'orderby' => 'meta_value_num', 
					'order' => 'ASC', 
					'post_type'=> 'product',
					'product_cat' => 'coming-soon', 
					'posts_per_page'=> -1 ) );
				if (have_posts()) : ?>	
					<ul class="thumbnails">    
						<?php while (have_posts()) : the_post(); ?>
							<?php
							$post_id = $wp_query->post->ID;
							$video = wb_get_post_details($post_id);
							
							$image = '<img src="' . $video['smlThumb'] . '" alt="' . $video['title'] . '" />';
							$postContent = wb_format_string($video['desc'], false, false, 150, '... <a href="' . get_permalink() . '"><span class="more-link">[more]</span></a>');
							?>
							<li class="wb_page_cat_span12">
								<div class="thumbnail no-style">
									<?php //show the featured imaged
									the_post_thumbnail('wb-product-image-thumb'); ?>
								</div>
								<div class="span9">
									<h3><?php the_title(); ?></h3>
									<?php //trim content
									$content = get_the_content();
									$trimmed_content = wp_trim_words( $content, 25, '... <a href="'. get_permalink() .'">&nbsp;[ more ]</a>' ); ?>
									<p><?php echo $content; ?></p>
									<?php if (count($video['tags']) > 0) { ?>
										<p class="tags"><?php echo $video['taglisthtml']; ?></p> 
									<?php	}	?>
								</div>
							</li>
						<?php endwhile; ?>						
					</ul>
			</div>
						<?php 
						// Find total number of pages
						$pageNumber = (get_query_var('paged')) ? get_query_var('paged') : 1;
						$theQuery = $wp_query->request;
						$theQuery = substr_replace($theQuery, "", strpos($theQuery, "LIMIT"), strlen($theQuery));
						$get_max_pages = $wpdb->query($theQuery);
						$max_pages = ceil(($get_max_pages) / $wb_ent_options['videolistinfo']['vidsperpage']);
						if ($max_pages > 1) {	?>
							<div class="pageNav">
								<div id="paginationLeft">&nbsp;<?php previous_posts_link('&laquo; Previous') ?></div>
								<div id="paginationMid">Page <?= $pageNumber ?> of <?= $max_pages ?></div>
								<div id="paginationRight"><?php next_posts_link('Next &raquo;') ?>&nbsp;</div>
							</div> 
						<?php	}	?>
					<?php else : ?>
						<h1 class="center">No videos found.</h1>
						<p>Try a different search or:</p>
						<?php
						$query = $wpdb->query("SELECT post_name FROM wp_posts
							WHERE post_type='post'
							AND post_status='publish'
							ORDER BY RAND()
							LIMIT 1");
							
							foreach ($query as $row) {
								$videoname = $row['post_name'];
							}
						?>
						<div id="linkList404">
							<p>&raquo; <a href="<?php echo get_site_url() . '/' . $videoname ?>">Watch a Random Video</a></p>
							<p>&raquo; <a href="<?php echo get_site_url() . '/' ?>">Go Back to Library</a></p>
							<p>&raquo; <a href="<?php echo get_site_url() . '/' ?>viewing-tips">Read Viewing Tips</a></p>
							<p>&raquo; <a href="<?php echo get_site_url() . '/' ?>contact">Contact Us</a></p>
						</div>            
						<p>&nbsp;</p>
					<?php endif; ?>
					<div id="wb_page_cat_footer">
						<?php echo $get_wb_page_cat_footer_text; ?>
						<a href="<?php echo $get_wb_page_cat_footer_banner_url ?>"><img src="<?php echo $get_wb_page_cat_footer_banner_img ?>" /></a>
					</div>
		</div>
		<?php
		get_sidebar();
		get_footer();
    ?>
