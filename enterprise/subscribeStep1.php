<?php
/**
 * filename: subscribeMc.php
 * description: this will be the template used for the Mailchimp Subscribe Form
 * author: Jullie Quijano
 * date created: 2014-05-02
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: Subscribe Template Step 1
 */
global $wb_ent_options;
get_header();
if (have_posts()) : while (have_posts()) : the_post();
        ?>
<style type="text/css">
    #wb_ent_main{
        overflow: visible;
    }
    #subscribe-email label, #subscribe-email .subs-input,  form input{
        display: block;
        width: 97%;
    }
    #subscribe-email .error{
        color: red;
        font-size: small;
        margin: 10px 0;
    }
    span.required{
        color: #c60;
        font-size: 200%;
    }
    
</style>
<?php

$apfSubscriberRole = $wb_ent_options['mailchimpapi']['rolename'];
if (isset($_POST['submit'])) {
  
 //Check to make sure sure that a valid email address is submitted
    if (trim($_POST['user_email']) === '') {
        $emailError = 'You forgot to enter your email address.';
        $hasError = true;
    } else if (!eregi("^[A-Z0-9._%-\+]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['user_email']))) {
        $emailError = 'Please enter a valid email address';
        $hasError = true;
    } else { // if user email is valid
        $email = trim($_POST['user_email']);
        $user_id = email_exists( $email );        
        if( is_numeric($user_id) ){
           $createNewUser = false;           
           //redirect to subscribe
        }else{
          $createNewUser = true;  
        }
        
        if($createNewUser){
            $userdata = array(
                'user_login' => str_replace( array('+', '!', '#', '$', '%', '&', '\'', '*', '+', '-', '/', '=', '?', '^', '`', '{', '|', '}', '~'), '', $email),
                'user_email'  =>  $email,
                'role'    =>  $apfSubscriberRole,
                'user_pass'   =>  strtolower('wbtv'.$email.'free')
            );
            
            $user_id = wp_insert_user( $userdata );
            if( is_wp_error($user_id) ) {
               $error_string = $user_id->get_error_message();
                echo '<div id="message" class="error" stlye="display:block;clear:both;"><p>' . $error_string . '</p></div>';
            }         
            else{
                  echo "user succesfully added";                       
                  setcookie("wb_sub_email", $email, NULL, "/");
                  wp_redirect("subscribe-mc");
            }
         }// end of if(createNewUser)
         else{
            setcookie("wb_sub_email", $email, NULL, "/");
            wp_redirect("subscribe-mc");
         }
         
    }
    
}
?>
        <div id="wb_ent_content" class="clearfix row-fluid">
            <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
                <div id="viewing-tips">
                    <!--<h1><?php //the_title(); ?></h1>-->
                    <?php if($wb_ent_options['mailchimpform-client']) {?>
                    <h1 class="pagetitle"><?php printf(__('Never miss a new episode. Subscribe to %s\' newsletter.', 'enterprise'), $wb_ent_options['channelname']); ?></h1>
                    <?php }else {?>
                    <h1 class="pagetitle"><?php printf(__('Never miss a new episode. Subscribe to %s\'s newsletter.', 'enterprise'), $wb_ent_options['channelname']); ?></h1>
                    <?php } ?>
                    <?php
                    if (trim($wb_ent_options['clientprivacyurl']) != '' || trim($wb_ent_options['clienttermsurl']) != '') {
                        ?>
                        <p style="color: #535353; font-size: 12px;"><?php printf(__('View our <a href="%s" title="Terms of Use">Terms of Use</a> and <a href="%s" title="Privacy Policy">Privacy Policy</a>.', 'enterprise'),$wb_ent_options['clienttermsurl'], trim($wb_ent_options['clientprivacyurl']));  ?></p>
                        <?php
                    }
                    
                    ?>
                        <p>By subscribing you consent to receiving notifications that could include educational material and promotional content
                        relating to association events, products, and services.</p>    
                    <br />
                    <?php if (isset($hasError)) { ?>

                    <div class="alert alert-danger">
                        <?php echo $emailError; ?>
                    </div>            


                    <?php } ?>
                    <form id="subscribe-email" method="post" action="<?= $_SERVER['REQUEST_URI'] ?>">
                        <label>Email Address<span class="required">*</span></label>
                        <input type="email" class="subs-input" name="user_email" id="user_email" required />
                        <button type="submit" class="btn" name="submit">Submit</button>
                    </form>
                  
                    <?php the_content(); ?>


                <?php endwhile;
            endif;
            ?>
        </div>
    </div>
    
    <script type='text/javascript' src='http://cdn.jsdelivr.net/jquery.validation/1.14.0/jquery.validate.min.js'></script>
      <script type='text/javascript'>
          $("#subscribe-email").validate();
                    /*
                    $("#subscribe-email").validate({
                        submitHandler: function(form) {
                          form.submit();
                        }
                       });
                       */
                    </script>
<?php
get_sidebar();
get_footer();
?>