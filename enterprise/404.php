<?php
/**
 * filename: 404.php
 * description: this will be the template to be used when a page is not found
 * author: Jullie Quijano
 * date created: 2014-04-15
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: 404
 */
global $wb_ent_options, $wpdb;
get_header();
?>
<div id="wb_ent_content" class="clearfix row-fluid">
    <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
        <div id="viewing-tips">

            <h1 class="center">The page you requested does not exist.</h1>
            <p>Try a different search or:</p>
            <?php
	            $args = array(
	            		'numberposts'   => 1,
	            		'post_type'     => 'post',
	            		'category'		=> $wb_ent_options['videocats']['library'],
	            		'orderby'       => 'rand'
	            );
	            $results = get_posts( $args );
                $wb_random_post = "home";
	            if ( $results[0]->post_name != "" )
	            	$wb_random_post = $results[0]->post_name;
	        ?>
            <div id="linkList404">
                <p>&raquo; <a href="<?php echo get_site_url() . '/' . $wb_random_post; ?>">Watch a Random Video</a></p>
                <p>&raquo; <a href="<?php echo get_site_url() . '/home'; ?>">Go Back to Library</a></p>
                <p>&raquo; <a href="<?php echo get_site_url() . '/'; ?>viewing-tips">Read Viewing Tips</a></p>
                <p>&raquo; <a href="<?php echo get_site_url() . '/'; ?>contact">Contact Us</a></p>
            </div>                

        </div>
    </div>
<?php
get_sidebar();
get_footer();
?>
