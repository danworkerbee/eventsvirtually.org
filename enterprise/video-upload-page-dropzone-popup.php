<?php
/**
* filename: video-upload-page.php
* description: page for users to upload their video to the site into a post as a draft
* 
* @package WordPress
* @subpackage Enterprise

* Template Name: Video Upload Page - Dropzone Popup
*/
// if($_POST){
	// var_dump($_POST);
// }
remove_filter('template_redirect', 'redirect_canonical'); 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
global $wb_ent_options, $video, $wpdb, $user_portfolio, $portfolio_cat_array;
$wb_ent_options = get_option('wb_ent_options');
$wb_channels_options = get_option('wb_channels_options');
$wb_school_tv_options = get_option('wb_school_tv_options');
//echo '<!-- $wb_school_tv_options is '.print_r($wb_school_tv_options, true).' -->';

/*get external url values*/
if(ISSET($_GET['form']) && $_GET['form']){
	// var_dump($_GET['name']);
	if($_GET['fname']){$get_external_name = sanitize_text_field($_GET['fname']);}
	if($_GET['company']){$get_external_company = sanitize_text_field($_GET['company']);}
	if($_GET['phone']){$get_external_phone = sanitize_text_field($_GET['phone']);}
	if($_GET['email']){$get_external_email = sanitize_text_field($_GET['email']);}
	if($_GET['memberid']){$get_external_memberid = sanitize_text_field($_GET['memberid']);}
}

/**/

$video_cats_default = array("420");
$msg = '';
$user_portfolio = '';
// $recipient = 'Dan Stevens <dan.stevens@workerbee.tv>, AJ Batac <aj.batac@workerbee.tv>';
$recipient = 'zen.lu@workerbee.tv,webmaster@workerbee.tv';

if( !is_user_logged_in() ){
   //header('location: /');
   $userid = 13; # Video Uploader subscriber
   $current_user = get_userdata($userid);
   //echo '<!-- $current_user is '.print_r($current_user, true).' -->';
   $user_id = $current_user->ID;
   $current_user_roles = $current_user->roles;
   //$user_portfolio = get_user_meta($user_id, 'portfolio_id', true);   
}
else{
   get_currentuserinfo();
   $current_user = get_userdata( $user_ID );   
   $user_id = $current_user->ID;
   $current_user_roles = $current_user->roles;   
}
echo "<style>#wbc-content{background-color:white;padding: 10px;}
.alert-danger{color: red; border: 1px solid red; padding: 10px;}
#fileUploadProgress span{display:block}
#wbc-content .container .row{ margin-left:0}</style>";
echo '<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">';
header("X-Robots-Tag: noindex,nofollow");
// get_header( 'wbchannels' );


if( !empty($_GET['pId']) && !( isset($_POST['submitVideoButton']) && trim($_POST['submitVideoButton']) == 'Submit' )){
   $current_post_id = intval( $_GET['pId'] );
   
   //get the current information for post
   $current_post = get_post($current_post_id, ARRAY_A);
   $video_title = $current_post['post_title'];
   $video_desc = $current_post['post_content']; 
	 
	 $wb_upload_get_post_meta = get_post_meta($current_post_id);
	 // var_dump($wb_upload_get_post_meta);
   $wb_upload_name = $wb_upload_get_post_meta['wb_uploader_upload_name']['0']; 
   $wb_upload_company = $wb_upload_get_post_meta['wb_uploader_upload_company']['0']; 
   $wb_upload_email = $wb_upload_get_post_meta['wb_uploader_upload_email']['0']; 
   $wb_upload_phone = $wb_upload_get_post_meta['wb_uploader_upload_phone']['0'];
   $wb_upload_memberid = $wb_upload_get_post_meta['wb_uploader_upload_memberid']['0'];
	 
   $tag_objects = wp_get_post_terms( $current_post_id, 'post_tag' );
   $tag_array = array();
   foreach($tag_objects as $current_tag){
      $tag_array[] = $current_tag->name;
   }  
   $video_tags_string = implode(',', $tag_array);  
   
   $current_post_media = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wb_media WHERE post_id=%s", $current_post_id) );
      
}

$errorMsgArray = array();
$errorMsg = 0;
if( isset($_POST['submitVideoButton']) && trim($_POST['submitVideoButton']) == 'Submit' ){
   include '../../plugins/workerbee-channels-v2/resources/bc-mapi.php';
		// require_once (ABSPATH . '/wp-content/plugins/workerbee-channels-v2/resources/bc-mapi.php');
   $cat_objects = wp_get_post_terms( $current_post_id, 'post_tag' );
   //echo '<!-- debug: $cat_objects is '.print_r($cat_objects,true).' -->';
   $cats_to_remove = array();
   foreach($cat_objects as $current_object){
      $cats_to_remove[] = $current_object->term_id;
   }
   //echo '<!-- debug: $cats_to_remove is '.print_r($cats_to_remove,true).' -->';
   $remove_tags = wp_remove_object_terms( $current_post_id, $cat_objects, 'post_tag' );
   //echo '<!-- debug: $remove_tags is '.$remove_tags.' or '.print_r($remove_tags,true).' -->';
   
   $cat_objects = wp_get_post_terms( $current_post_id, 'channel' );
   //echo '<!-- debug: $cat_objects is '.print_r($cat_objects,true).' -->';
   $cats_to_remove = array();
   foreach($cat_objects as $current_object){
      $cats_to_remove[] = $current_object->term_id;
   }
   //echo '<!-- debug: $cats_to_remove is '.print_r($cats_to_remove,true).' -->';   
   $remove_channels = wp_remove_object_terms( $current_post_id, $cats_to_remove, 'channel' );   
   //echo '<!-- debug: $remove_channels is '.$remove_channels.' or '.print_r($remove_channels,true).' -->';
      
   
   $video_title = sanitize_text_field($_POST['title']);
   $video_description = sanitize_text_field($_POST['description']);
   // $video_keywords = sanitize_text_field($_POST['keywords']);
   $wb_upload_name = sanitize_text_field($_POST['wb_upload_name']);
   $wb_upload_company = sanitize_text_field($_POST['wb_upload_company']);
   $wb_upload_email = sanitize_text_field($_POST['wb_upload_email']);
   $wb_upload_phone = sanitize_text_field($_POST['wb_upload_phone']);
   $wb_upload_memberid = sanitize_text_field($_POST['wb_upload_memberid']);
	 
    if( trim($wb_upload_name) == '' ){
      $errorMsg = 1;
      $errorMsgArray[] = 'Please enter your name.';
    }
    if( trim($wb_upload_memberid) == '' ){
      $errorMsg = 1;
      $errorMsgArray[] = 'Please enter your member id.';
    }
    if( trim($wb_upload_company) == '' ){
      $errorMsg = 1;
      $errorMsgArray[] = 'Please enter your company.';
    }
    if( trim($wb_upload_email) == '' ){
      $errorMsg = 1;
      $errorMsgArray[] = 'Please enter your email.';
    }
		
    if( trim($video_title) == '' ){
      $errorMsg = 1;
      $errorMsgArray[] = 'Please enter a video title.';
    }
    if( trim($video_description) == '' ){
      $errorMsg = 1;
      $errorMsgArray[] = 'Please enter a video description.';
    }
		
   $video_desc = sanitize_text_field($_POST['description']);
   $video_tags_string = sanitize_text_field($_POST['keywords']);  
   // $video_cats = $_POST['categories'];
   $video_file_name = ( isset($_POST['uploadedFileName']) && trim($_POST['uploadedFileName']) != '' ) ? trim($_POST['uploadedFileName']) : '';
   // $video_file_name = ( isset($_POST['files']) && trim($_POST['files']) != '' ) ? trim($_POST['files']) : '';
   $video_delete_url = $_POST['uploadedFileDeleteUrl'];
   // $post_status = ( isset($wb_channels_options['video_upload']['default_status']) && trim($wb_channels_options['video_upload']['default_status']) != '' ) ? $wb_channels_options['video_upload']['default_status'] : 'pending';
   $post_status = 'draft';
   
    if( !$errorMsg ){
      if( trim($video_file_name) != '' ){
				
					if( !empty($current_post_id) ){
							$post = array(
									 'ID'             => $current_post_id, // Are you updating an existing post?
									 'post_content'   => $video_desc, // The full text of the post.
									 'post_title'     => $video_title, // The title of your post.         
									 'post_type'      => 'post', // Default 'post'.
									 'ping_status'    => 'closed',
									 'comment_status' => 'closed',
									 'post_category'  => $video_cats, // Default empty.
									 'tags_input'     => $video_tags_string,
							);    
							$final_post_id = wp_update_post( $post, true );
					}
					else{
							$post = array(
									 'post_content'   => $video_desc, // The full text of the post.
									 'post_title'     => $video_title, // The title of your post.
									 'post_status'    => $post_status, // Default 'draft'.
									 'post_type'      => 'post', // Default 'post'.
									 'ping_status'    => 'closed',
									 'comment_status' => 'closed',
									 'post_category'  => $video_cats_default, // Default empty.
									 'tags_input'     => $video_tags_string,
									 'post_author'      =>   $user_id
							);          
							$final_post_id = wp_insert_post( $post, true );
					}
				
          //send video to brightcove
          $bc = new BCMAPI(
               $wb_ent_options['brightcoveinfo']['readurltoken'],
               $wb_ent_options['brightcoveinfo']['writetoken']
          );

          $metaData = array(
               'name' => $video_title,
               'shortDescription' => substr ( $video_desc , 0 , 245 ).'...'
          ); 

          $params = array(
               'create_multiple_renditions' => TRUE,  
               'encode_to' => 'MP4'
          ); 

          $file =  $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/workerbee-channels-v2/js/jQueryFileUpload/server/php/files/'.$video_file_name;
          
          // Create a try/catch
          try {
                   // Upload the video and save the video ID
                   $media_id = $bc->createMedia('video', $file, $metaData, $params);    

                   //get thumbnail
                   $video = $bc->find('videoById', $media_id);
                   $media_thumb = $video->thumbnailURL ;
									// var_dump($media_thumb);
                   //add video id to media table
                   $insert_videoid = $wpdb->query( $wpdb->prepare(
                        "INSERT INTO wb_media (
                            post_id, media_type, media_id, src, status, media_thumb, youtube
                        ) VALUES (
                            '%s', 'video', '%s', '', '1', '%s', '0'
                        ) ON DUPLICATE KEY UPDATE
                            media_id='%s',
                            media_thumb='%s'",
                        $final_post_id, $media_id, $media_thumb,
                        $media_id, $media_thumb
                   ) );           

          } catch(Exception $error) {
                   // Handle our error        
                   $errorMsg = 1;
          }        
      }
      elseif( !empty($_GET['pId']) ){
          $media_id = $current_post_media[0]->media_id;
      }
      else{
          $errorMsg = 1;
          $errorMsgArray[] = 'Please upload a video.';
      }
    }

   if( is_numeric($final_post_id) && $final_post_id != 0 && !$errorMsg ){
      $msg = 'success';
      
      update_post_meta( $final_post_id, 'wb_ppv_prod_video_id', $media_id );
      update_post_meta( $final_post_id, 'wb_twitterlivefeed_meta_showhide', 'default' );
      update_post_meta( $final_post_id, 'wb_twitterlivefeed_meta_autobitly_enable', 'default' );
      update_post_meta( $final_post_id, 'mobile_option_RSS', '{"pushMobile":1,"makeFeature":0}' );
			
			update_post_meta( $final_post_id, 'wb_uploader_upload_name', $wb_upload_name );
			update_post_meta( $final_post_id, 'wb_uploader_upload_company', $wb_upload_company );
			update_post_meta( $final_post_id, 'wb_uploader_upload_email', $wb_upload_email );
			update_post_meta( $final_post_id, 'wb_uploader_upload_phone', $wb_upload_phone );
			update_post_meta( $final_post_id, 'wb_uploader_upload_memberid', $wb_upload_memberid );
			update_post_meta( $final_post_id, 'wb_uploader_upload_form', 'true' );
      
      
      // $channel_info = wb_channels_get_channel_info($channel_id_to_add, 'full');  
			
			$channel_access = json_decode($channel_info['videoAccessInfo'], true);
						
			//add video library categories
			if( !$channel_info['is_portfolio'] && $channel_access['viewAccess']['mode'] != 'public' ){
				// wp_set_post_terms( $final_post_id, $wb_ent_options['videocats']['private'], 'category', false );
				wp_remove_object_terms( $current_post_id, array($wb_ent_options['videocats']['library']), 'category' );  
			}
			else{
				// wp_set_post_terms( $final_post_id, $wb_ent_options['videocats']['unlisted'], 'category', false );
				wp_remove_object_terms( $current_post_id, array($wb_ent_options['videocats']['library']), 'category' );  
			}
      
			if( count($video_cats) > 0 ){
				array_push($video_cats, $wb_channels_options['channelsId'],$channel_info['categoryId']);
			}
			else{
				$video_cats = array($wb_channels_options['channelsId'],$channel_info['categoryId']);
			}
      
      // wp_set_post_terms( $final_post_id, $video_cats, 'channel', true );
      
      $product_cats_to_add = array($channel_info['productCategoryId']);
      wp_set_post_terms( $final_post_id, $terms_to_add, 'product_cat', true );            
      
      //execute script to publish video
      // shell_exec("/usr/local/bin/php /home/{$wb_ent_options['serveruser']}/{$wb_ent_options['publicdir']}/wp-content/plugins/workerbee-channels-v2/resources/check_bc_processing.php '".escapeshellarg($final_post_id)."' '".escapeshellarg($media_id)."' >> /home/{$wb_ent_options['serveruser']}/{$wb_ent_options['publicdir']}/bc_processing.log &");    
      
      if( empty($_GET['pId']) ){
				//send notification
				// $sender = $wb_channels_options['videoUploadInfo']['emailSender'];
				$sender = $wb_ent_options["channelname"]." <".$wb_ent_options["channelemail"].">";

				$subject = 'A new video has been uploaded';
				$messageHeading = 'Video Uploaded';

				$uploading_user_name = $wb_upload_name;
				$messageBody = '
            <p>Dear Administrator,</p>

            <p>A new video has been uploaded by a member.</p>
            <p>Please be advised that the video may take up to 30 minutes or longer to process, depending on its size. Once the video is processed, you will be able to preview the video as a saved draft and publish. </p>            
            <p>
						Full Name: '.stripslashes($wb_upload_name).'<br />
						Phone Number: '.stripslashes($wb_upload_phone).'<br />
						Email: '.stripslashes($wb_upload_email).'<br /><br />
            Video Title: '.stripslashes($video_title).'<br />
            Video Description: '.stripslashes($video_desc).'<br />
            Keywords: '.stripslashes($video_tags_string).'<br />
            Edit Video URL: <a href="http://'.$wb_ent_options[channeldomain].'/wp-admin/post.php?post='.$final_post_id.'&action=edit">http://'.$wb_ent_options[channeldomain].'/wp-admin/post.php?post='.$final_post_id.'&action=edit</a><br />
            </p>
            ';
         $all_recipients = array();
         if( count($wb_channels_options['videoUploadInfo']['emailRecipientRoles']) > 0 ){

            foreach($wb_channels_options['videoUploadInfo']['emailRecipientRoles'] as $current_role){
               //get all users
               $args = array(
                  'role'         => $current_role,
                  'meta_key'     => 'wb_st_status',
                  'meta_compare' => '!=',
                  'meta_value'   => 'pending',                          
                  'orderby'      => 'login',
                  'order'        => 'ASC',
                );                
               $currentusers = get_users( $args );                
               $all_recipients = array_merge($all_recipients, $currentusers);       
            }        

            $all_emails = array();
            foreach($all_recipients as $current_user){
               $current_user_email = $current_user->data->user_email;

               if( !in_array($current_user_email, $all_emails) ){
                  $all_emails[] = $current_user_email;
               }

            }

            $recipientText = $wb_channels_options['videoUploadInfo']['emailRecipients'];
            // $recipient = implode(',', $all_emails).','.$recipientText;
						
						
         }
         wb_ent_send_mail($subject, $messageHeading, $messageBody, $recipient, $sender);
				 // wp_mail( $recipient, $subject, $messageBody, $messageHeading, $attachments );
      }
   }
   
}


if( trim($current_post_media[0]->media_id) != '' ){
?>


<style type="text/css">
   #uploader-div{
      display: none;
   }
   #playerDiv{
      display: block;
   }  
   #myExperience<?php echo $current_post_media[0]->media_id ?>{
    display: block;
    margin: 10px auto 10px;
   }
   #showVideoDivButton{
      display: none;
   }
</style> 
<?php
} else {
?>
<style type="text/css">
   #uploader-div{
      display: block;
   }  
   #playerDiv{
      display: none;
   }	 
</style>
<?php
}
?><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
// $.noConflict();
</script>
	 <div id="wbc-content" class="">
			<div class="container">    
          <div class="row">            
            <!--<div id="wbc-main-content" class="col-xs-12 col-sm-8 col-md-8 col-lg-8">-->
            <div id="wbc-main-content" class="span8 clearfix">						
							<!-- Page Content -->
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
								<div id="wbc-upload-video-page-body-paragraph">
									<?php if( !$insert_videoid){the_content();} ?>
								</div>
							<?php endwhile; endif; ?>
							<script>
								// $( "#wbc-upload-video-page-body-paragraph p" ).removeClass( "lead" ); 
							</script>							
              <!-- Upload Video Page Content -->
              <div id="wbc-upload-video-page">
                      <!-- Video Upload -->
              <div id="wbc-video-upload" class="wbc-form-div">                                          
                <?php 
                        if($msg == "success"){ 
                           if( $insert_videoid){
                              $success_header = 'YOUR UPLOAD IS BEING PROCESSED!';
                              $successMessage = 'You have successfully uploaded your video. Please be patient as we review and approve this submission. Once approved, you will receive an email to notify you that the video has been published.';
                           }
                           elseif($current_post_id == $final_post_id){
                              $success_header = 'VIDEO UPDATED!';
                              $successMessage = 'Good news! We have successfully received your video. Depending on the size of your file, it might take a few extra minutes to fully process and make your video viewable on the channel you uploaded it to, as well as your portfolio.';
                           }
                           ?>      
                           <h2 class="wbc-title-h4"><?php echo $success_header; ?></h2>      
                  <div class="alert alert-success" role="alert">
                    <p>
                      <?php echo $successMessage; ?>
                    </p>
											<button id="mhi-close-iframe">Close Window</button>
											<a href="#" class="uk-modal-close uk-close uk-close-alt"></a>
											<script>$( document ).ready(function() {
												$("#mhi-close-iframe").click(function () {
													event.preventDefault();
													console.log("yes");
													// $(".uk-modal-close").trigger("click");
													parent.window.postMessage("removetheiframe", "*");
												});
											});
										</script>
                  </div>
                
                <?php                        
                        }
                        else{                            
                           //echo '<!-- $portfolio_cat_array is '.print_r($portfolio_cat_array, true).' -->';
                           //get all categories under user's roles' channel                           
                           $taxonomies = array( 
                                    'channel'    
                           );                                  
                           
                              $category_to_list = $wb_ent_options['videocats']['library'];
                              $taxonomies = array( 
                                       'category'    
                              );                            
                           
                           $cat_args = array(
                                    'orderby'           => 'name', 
                                    'order'             => 'ASC',
                                    'hide_empty'        => false, 
                                    'exclude'           => array( $wb_channels_options['channelsId'], $wb_live_lms_ppv_options['productCategoryIds']['liveStreamCat'], $wb_live_lms_ppv_options['productCategoryIds']['lmsCat'], $wb_live_lms_ppv_options['productCategoryIds']['ppvCat'], $wb_live_lms_ppv_options['productCategoryIds']['bundlesCat'], $wb_live_lms_ppv_options['productCategoryIds']['superBundleCat'], $wb_live_lms_ppv_options['productCategoryIds']['catProdList'], $wb_live_lms_ppv_options['productCategoryIds']['freeAccessCat'] ),
                                    'exclude_tree'      => array(), 
                                    'include'           => array(),
                                    'number'            => '', 
                                    'fields'            => 'all', 
                                    'slug'              => '',
                                    'parent'            => $category_to_list,
                                    'hierarchical'      => false, 
                                    'child_of'          => null,
                                    'childless'         => false,
                                    'get'               => '', 
                                    'name__like'        => '',
                                    'description__like' => '',
                                    'pad_counts'        => false, 
                                    'offset'            => '', 
                                    'search'            => '', 
                                    'cache_domain'      => 'core'
                           ); 

                           $library_cats = get_terms($taxonomies, $cat_args); 
                           
                           $cat_objects = wp_get_post_terms( $current_post_id, $taxonomies );
                           $cat_array = array();
                           foreach($cat_objects as $current_cat){
                              $cat_array[] = $current_cat->term_id;
                           }  
                           
                           if( isset($_POST['submitVideoButton']) && trim($_POST['submitVideoButton']) == 'Submit' && $errorMsg && count($errorMsgArray) > 0 ){
                                           
                           ?>
                  <div class="alert alert-danger" role="alert">
                    <p>
                      <?php 
                                 if( count($errorMsgArray) == 1 ){
                                    echo $errorMsgArray[0]; 
                                 }
                                 else{
                                    echo '<ul>';
                                    foreach($errorMsgArray as $current_error){
                                       echo '<li>'.$current_error.'<li>';
                                    }
                                    echo '</ul>';
                                 }
                                 
                                 ?>
                    </p>
                  </div>                           
                           <?php
                           }                                 
                           ?>                
                <div role="tabpanel" id="myTab">
                  <!-- Nav tabs -->
                  <!--<ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#video-details" aria-controls="video-details" role="tab" data-toggle="tab" class="text-uppercase">1. Video Details</a></li>-->
                    <!-- <li role="presentation"><a href="#contests" aria-controls="contests" role="tab" data-toggle="tab" class="text-uppercase">2. Contests</a></li> -->
                    <!--<li role="presentation"><a href="#upload-video" aria-controls="upload-video" role="tab" data-toggle="tab" class="text-uppercase">2. Upload Video</a></li>
                  </ul>-->
									
                  <form class="wbc-form-style" id="channel-upload-form" method="POST">
                    <!-- Tab panes -->
                    <div class="tab-content" >
                      
                      <div role="tabpanel" class="tab-pane active" id="video-details">
                        <!--<h2 class="wbc-title-h4">Your Video details</h2>-->
												<div class="form-group">
													<label for="title">Name <span class="req">*</span></label>
													<input type="text" class="form-control" id="wb_upload_name" name="wb_upload_name" value="<?php if($get_external_name) {echo stripslashes($get_external_name);} else {echo stripslashes($wb_upload_name);} ?>" required>
												</div>
												<div class="form-group">
													<label for="title">Company <span class="req">*</span></label>
													<input type="text" class="form-control" id="wb_upload_company" name="wb_upload_company" value="<?php if($get_external_company) {echo stripslashes($get_external_company);} else {echo stripslashes($wb_upload_name);} ?>" required>
												</div>
												<div class="form-group">
													<label for="title">Phone Number</label>
													<input type="text" class="form-control" id="wb_upload_phone" name="wb_upload_phone" value="<?php if($get_external_phone) {echo stripslashes($get_external_phone);} else {echo stripslashes($wb_upload_name);} ?>" >
												</div>
												<div class="form-group">
													<label for="title">Email Address <span class="req">*</span></label>
													<input type="text" class="form-control" id="wb_upload_email" name="wb_upload_email" value="<?php if($get_external_email) {echo stripslashes($get_external_email);} else {echo stripslashes($wb_upload_name);} ?>" required>
												</div>
												<div class="form-group">
													<label for="title">Member ID <span class="req">*</span></label>
													<input type="text" class="form-control" id="wb_upload_memberid" name="wb_upload_memberid" value="<?php if($get_external_memberid) {echo stripslashes($get_external_memberid);} else {echo stripslashes($wb_upload_memberid);} ?>" required>
												</div>
                      
                      <div class="form-group">
                        <label for="title">Video Title <span class="req">*</span></label>
                        <input type="text" class="form-control" id="title" name="title" value="<?php echo stripslashes($video_title); ?>" required>
                      </div>
                      <div class="form-group">
                        <label for="description">Video Description <span class="req">*</span></label>
                        <textarea class="form-control" name="description" required><?php echo stripslashes($video_desc); ?></textarea>
                      </div>
                      <!--<div class="form-group">
                        <label for="keywords">Tags/Keywords <span style="text-transform: none;">(Separate with commas)</span></label>                        
                        <input type="keywords" class="form-control" id="keywords" name="keywords" value="<?php echo stripslashes($video_tags_string); ?>">                      
                      </div>-->
											<?php /*	CATEGORY SELECT
                      <div class="form-group">                    
                        <label for="categories">Categories</label>
                        <select multiple="multiple" id="select-categories" name="categories[]" 
                                                <?php if( trim($user_portfolio) != '' ){
                                                echo 'onchange="updatePortfolioTerms();"';
                                                }?>>
                          <?php 
                                       foreach($library_cats as $current_cat) { 
                                       ?>
                          <option value='<?php echo $current_cat->term_id; ?>'
                                       <?php
                                       if(in_array($current_cat->term_id, $cat_array) ){
                                          echo ' selected="selected" ';
                                       }
                                       if( trim($user_portfolio) != '' ){
                                          echo ' data-category-parent-id="'.$category_to_list.'" data-category-name="'.$current_cat->name.'" ';
                                       }
                                       ?>
                                       ><?php echo $current_cat->name; ?>
                                       
                                       </option>
                          <?php
                                          $args2 = array(
                                                   'orderby'           => 'name', 
                                                   'order'             => 'ASC',
                                                   'hide_empty'        => false, 
                                                   'exclude'           => array( $wb_channels_options['channelsId'], $wb_live_lms_ppv_options['productCategoryIds']['liveStreamCat'], $wb_live_lms_ppv_options['productCategoryIds']['lmsCat'], $wb_live_lms_ppv_options['productCategoryIds']['ppvCat'], $wb_live_lms_ppv_options['productCategoryIds']['bundlesCat'], $wb_live_lms_ppv_options['productCategoryIds']['superBundleCat'], $wb_live_lms_ppv_options['productCategoryIds']['catProdList'], $wb_live_lms_ppv_options['productCategoryIds']['freeAccessCat'] ),
                                                   'exclude_tree'      => array(), 
                                                   'include'           => array(),
                                                   'number'            => '', 
                                                   'fields'            => 'all', 
                                                   'slug'              => '',
                                                   'parent'            => $current_cat->term_id,
                                                   'hierarchical'      => false, 
                                                   'child_of'          => null,
                                                   'childless'         => false,
                                                   'get'               => '', 
                                                   'name__like'        => '',
                                                   'description__like' => '',
                                                   'pad_counts'        => false, 
                                                   'offset'            => '', 
                                                   'search'            => '', 
                                                   'cache_domain'      => 'core'
                                          ); 
                                          $library_topics = get_terms($taxonomies, $args2);              
                                          foreach($library_topics as $current_topic) { 
                                             //echo '<!-- $current_cat->name is '.$current_cat->name.' -->';
                                             //echo '<!-- $portfolio_cat_array[$current_cat->name] is -'.$portfolio_cat_array[$current_cat->name].' -->';
                                             //echo '<!-- $portfolio_cat_array[$current_cat->name]term_id is -'.$portfolio_cat_array[$current_cat->name]['term_id'].' -->';
                                             
                                             //echo '<!-- $current_topic->name is '.$current_topic->name.' -->';
                                             //echo '<!-- $portfolio_cat_array[$current_cat->name][$current_topic->name] is -'.$portfolio_cat_array[$current_cat->name][$current_topic->name].' -->';
                                             //echo '<!-- $portfolio_cat_array[$current_cat->name][$current_topic->name]term_id is -'.$portfolio_cat_array[$current_cat->name][$current_topic->name].' -->';                                             
                                       
                                          ?>
                                          <option value='<?php echo $current_topic->term_id; ?>'
                                             <?php
                                             if(in_array($current_topic->term_id, $cat_array) ){
                                                echo ' selected="selected" ';
                                                if( isset($portfolio_cat_array[$current_cat->name]) && is_numeric($portfolio_cat_array[$current_cat->name]['term_id']) ){
                                                   $portfolio_cats_to_add_array[] = $portfolio_cat_array[$current_cat->name]['term_id'];
                                                }
                                                if( isset($portfolio_cat_array[$current_cat->name][$current_topic->name]) && is_numeric($portfolio_cat_array[$current_cat->name][$current_topic->name]) ){
                                                   $portfolio_cats_to_add_array[] = $portfolio_cat_array[$current_cat->name][$current_topic->name];
                                                }                                                     
                                             }                                         
                                             
                                             if( trim($user_portfolio) != '' ){
                                                echo ' data-category-parent-id="'.$current_cat->term_id.'" data-category-name="'.$current_cat->name.'" data-topic-name="'.$current_topic->name.'" ';
                                             }
                                             ?>                                                    
                                          >&nbsp;&nbsp;&nbsp;<?php echo $current_topic->name; ?></option>
                                          <?php
                                          }
                                       }                                      
                                       //echo '<!-- $portfolio_cats_to_add_array is '.print_r($portfolio_cats_to_add_array, true).' -->';
                                       ?>                      
                        </select>
                      </div>
                      
                      <div class="text-center" id="wbc-select-btn">
                        <input  style="" class="btn btn-default btn-select-all" type="button" value="Select All" onclick="$('#select-categories').multiSelect('select_all');" />
                        <input  style="" class="btn btn-default btn-deselect-all" type="button" value="De-Select All" onclick="$('#select-categories').multiSelect('deselect_all');" />
                      </div>
											
											*/ ?>
											<div class="form-group">
                        <label for="keywords">Keywords <span style="text-transform: none;">(Separate with commas)</span> </label>                        
                        <input type="keywords" class="form-control" id="keywords" name="keywords" value="<?php echo stripslashes($video_tags_string); ?>">                      
                      </div>
                                 <?php
                                 if( trim($user_portfolio) != '' ){
                                    $check_add_to_channel = '';
                                                      $channel_info = wb_channels_get_channel_info($user_portfolio);
                                                      if( in_array($channel_info['categoryId'], $cat_array)  ){
                                                         $check_add_to_channel = ' checked="checked" ';
                                                      }
                                 ?>
                      <div class="checkbox">
                        <label class="font-normal" for="addToChannel">
                          <input type="checkbox" name="addToChannel" id="addToChannel" value="true" <?php echo $check_add_to_channel; ?> /> Add this video to My Portfolio                          
                        </label>
                      </div>
                      <?php
                                 }
                                 ?>
                      <!--<div class="text-right">
                        <button type="button" id="btn-next" class="btn btn-primary btn-upload">Next</button>
                      </div>
                      <div class="spacing"></div>-->
                    
                      
                      <!--</div>-->
                                 <!--
                      <div role="tabpanel" class="tab-pane" id="contests">
                         <h2 class="wbc-title-h4">Enter this video to win!</h2>
                         <?php for($i=1; $i<=3; $i++){ ?>
                         <hr />
                         <h3 class="wbc-title-contest">Contest Title <?= $i ?></h3>
                         <p class="contest-content">
                            Overview of contest here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id pulvinar felis, vitae laoreet quam. Nam a du mus metus semper dapibus. Proin et tincidunt dolor. Aliquam nibh massa, scelerisque sit amet lacus et, fermen facilisis tortor. Nullam varius justo a est rhoncus pulvinar.    
                         </p>
                         <a href="#" class="terms-link">Full terms and conditions</a>
                         <div class="checkbox">
                        <label class="font-normal">
                          <input type="checkbox"> I accept the terms and conditions - Enter my video in this contest                         
                        </label>
                      </div>
                      
                         <?php } ?>
                          <hr />
                          <div class="text-left col-lg-6 no-padding">                          
                            <a href="#video-details" aria-controls="video-details" role="tab" data-toggle="tab"><button type="button" id="btn-back" class="btn btn-primary btn-upload">Back</button></a>                                                       
                          </div>
                          <div class="text-right col-lg-6 no-padding">
                          <a href="#upload-video" aria-controls="upload-video" role="tab" data-toggle="tab"><button type="button" id="btn-next" class="btn btn-primary btn-upload">Next</button></a> 
                          </div>
                          <div class="clear"></div>
                      </div>
                -->
                     <!-- <div role="tabpanel" class="tab-pane" id="upload-video">-->
                       <!--<h2 class="wbc-title-h4">Upload your video</h2>-->
                        <?php /*
												<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                          <div class="panel panel-default">
                            <!--<div class="panel-heading collapsed" role="tab" id="headingOne" data-toggle='collapse' data-parent='#accordion' href='#collapseOne' aria-expanded='false' aria-controls='collapseOne'>
                              <div class="panel-title collapseOnetitle">
                                <h4 class="wbc-upload-h4">
                                  
                                    Which video formats can i upload?
                                  
                                  <a class='collapsed' data-toggle='collapse' href='#collapseOne' aria-expanded='false' aria-controls='collapseOne'>
                                    <span class="glyphicon glyphicon-plus pull-right"></span>
                                  </a> 
                                </h4>
                                
                              </div>
                            </div>-->
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                              <div class="panel-body">                                
                                <p class="contest-content"> 
                                                   You can upload just about any video file that is generated by your mobile device, as well as other common video formats, including MOV and AVI files. However, we recommend you upload an MP4 video file to optimize the playback on other smartphones, tablets, and computers.                                                  
                                  <!--
                                                   <h4 class="wbc-upload-h4">Need to convert your video?</h4>
                                  <ul>
                                    <li>
                                      <a href="#">conversionsite.com</a> 
                                    </li>
                                    <li>  
                                      <a href="#">anotherconversionsite.com</a> 
                                    </li>
                                    <li>  
                                      <a href="#">anothersite.com</a> 
                                    </li>
                                  </ul>                            
                                                   -->
                                </p>
                              </div>
                            </div>
                          </div>

                        </div>
*/ ?>												
                        <div id="wb-uploader-div-wrapper"> 
                                       <div id="uploader-div">
                          <div class="form-group" id="drop-zone" style="width: 98%; min-height: 205px; border: 5px dashed #d7d7d7;">  
                                          <h3 style="text-align: center; font-size: 28px; color: #aaa; margin-top: 30px;">
                                             Drop Video File Here
                                             <br /><br />
                                             or
                                          </h3>
                            <!--<input class="btn btn-default" type="file" id="videoUpload" name="files" value="Select a video to Upload" >-->
														<input class="btn btn-default" type="file" id="videoUpload" name="files" value="Select a video to Upload" data-url="/wp-content/plugins/workerbee-channels-v2/js/jQueryFileUpload/server/php/" style="display: none; visibility: hidden;" accept="video/*" >
                            <button style="display: block; width: 120px; margin:10px  auto;" class="btn btn-default" type="button" onclick="document.getElementById('videoUpload').click();">Select Video File</button>
                            <input type="hidden" id="uploadedFileName" name="uploadedFileName" />
                                          <input type="hidden" id="uploadedFileUrl" name="uploadedFileUrl" />
                                          <input type="hidden" id="uploadedFileDeleteUrl" name="uploadedFileDeleteUrl" />
                                          <div class="progress" id="fileUploadProgress" style="display: none;">
                                             <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                                <span class="sr-only">0%</span>
                                             </div>
                                          </div>
                                          
                          </div>
                                        <div class="text-left col-lg-12 no-padding" id="uploadedFileDiv" style="display: none; margin-bottom: 20px;">                          
                                           
                                        </div>
                                       </div>
                                       
                                       
                                       <div id="playerDiv">

                                             <div id="player">
                                                   <div style="display:none">

                                                   </div>

                                                   <?php
                                                   if( $_SERVER['HTTPS'] == 'on' ){
                                                   ?>
                                                   <script language="JavaScript" type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>
                                                   <script src="https://admin.brightcove.com/js/APIModules_all.js" type="text/javascript"></script> 
                                                   <?php
                                                   }
                                                   else{
                                                   ?>
                                                   <script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>
                                                   <script src="http://admin.brightcove.com/js/APIModules_all.js" type="text/javascript"></script> 
                                                   <?php                   
                                                   }
                                                   ?>

                                                   <div id="container1">
                                                         <object id="myExperience<?php echo $current_post_media[0]->media_id ?>" class="BrightcoveExperience" style="width:100%;">
                                                               <param name="bgcolor" value="#FFFFFF" />
                                                               <param name="width" value="640" />      
                                                               <param name="height" value="360" />
                                                               <param name="playerID" value="<?php echo $wb_channels_options['channelPlayers']['videoPlayerId']; ?>" />
                                                               <param name="publisherID" value="<?php echo $wb_ent_options['brightcoveinfo']['publisherid']; ?>"/>
                                                               <param name="isVid" value="true" />
                                                               <param name="isUI" value="true" />
                                                               <param name="wmode" value="transparent" />                                                            
                                                               <param name="@videoPlayer" value="<?php echo $current_post_media[0]->media_id; ?>" />
                                                               <param name="dynamicStreaming" value="true" />                
                                                               <?php
                                                               if( $_SERVER['HTTPS'] == 'on' ){
                                                                     echo '<param name="secureConnections" value="true" />';
                                                                     echo '<param name="secureHTMLConnections" value="true" />';
                                                               }
                                                               ?>                     
                                                         </object>
                                                   </div>
                                                   <!--[if gt IE 8]>
                                                   <script type="text/javascript">brightcove.createExperiences();</script>
                                                   <![endif]-->

<style>
#player #container1{position: relative;
      height: 0;
      padding-bottom: 56.25%;}
.BrightcoveExperience{position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;}
</style>
                                                   <?php
                                                   $mobile = mobile_device_detect();


                                                   if (is_array($mobile)) {
                                                         $device = $mobile['0'];


                                                         if ($mobile) {
                                                               ?>

                                                               <script type="text/javascript">
                                                                     var player,
                                                                                 APIModules,
                                                                                 videoPlayer,
                                                                                 experienceModule;

                                                                     function onTemplateLoad(experienceID) {
                                                                           console.log("EVENT: onTemplateLoad");
                                                                           player = brightcove.api.getExperience(experienceID);
                                                                           APIModules = brightcove.api.modules.APIModules;
                                                                     }

                                                                     function onTemplateReady(evt) {
                                                                           console.log("EVENT.onTemplateReady");
                                                                           videoPlayer = player.getModule(APIModules.VIDEO_PLAYER);
                                                                           experienceModule = player.getModule(APIModules.EXPERIENCE);
                                                                           videoPlayer.play();

                                                                           videoPlayer.getCurrentRendition(function(renditionDTO) {
                                                                                 var newPercentage = (renditionDTO.frameHeight / renditionDTO.frameWidth) * 100;
                                                                                 newPercentage = newPercentage + "%";
                                                                                 console.log("Video Width = " + renditionDTO.frameWidth + " and Height = " + renditionDTO.frameHeight);
                                                                                 console.log("New Percentage = " + newPercentage);
                                                                                 document.getElementById("container1").style.paddingBottom = newPercentage;
                                                                                 var evt = document.createEvent('UIEvents');
                                                                                 evt.initUIEvent('resize', true, false, 0);
                                                                                 window.dispatchevent(evt);
                                                                           });
                                                                     }

                                                                     window.onresize = function(evt) {
                                                                           console.log("in resize function");
                                                                           var playerDivWidth = $('#vid-post .outer-container').width();
                                                                           var playerDivHeight = ((playerDivWidth / 16) * 9);
                                                                           var resizeWidth = document.getElementById("myExperience<?php echo $current_post_media[0]->media_id ?>").offsetWidth,
                                                                                       resizeHeight = document.getElementById("myExperience<?php echo $current_post_media[0]->media_id ?>").offsetHeight;
                                                   //experienceModule.setSize(resizeWidth, resizeHeight);
                                                                           experienceModule.setSize(playerDivWidth, playerDivHeight);
                                                                     }
                                                               </script>

                                                               <?php
                                                         } // end of if($mobile)
                                                   } // end of is array($mobile)
                                                   ?>    

                                             </div>
                                       </div>
                                       <br />
																			 <?php
																			 $editVideoButtonsDiv = 'display: none;';
																			 if( !empty($_GET['pId']) && trim($current_post_media[0]->media_id) != '' ){
																				 $editVideoButtonsDiv = '';
																			 }
																			 ?>
																			 
                                       <div id="editVideoButtonsDiv" class="text-center col-lg-12" style="<?php echo $editVideoButtonsDiv; ?>">     
                                          <a id="showUploadDivButton" href="javascript: showUploadDiv();"  style="<?php echo $editVideoButtonsDiv; ?>"><button type="button" id="" class="btn btn-upload">Upload New Video</button></a>
                                          <a id="showVideoDivButton" href="javascript: showVideoDiv();" style="display: none;"><button type="button" id="" class="btn btn-upload">Cancel Upload</button></a>
                                       </div>                                       
                                       

                           <!--<div class="text-left col-lg-6 no-padding">                          
                            <button type="button" id="btn-back" class="btn btn-primary btn-upload">Back</button>                                                       
                          </div>-->
                          <div class="text-left col-lg-6 no-padding">
                                          <?php
                                          if( trim($user_portfolio) != '' ){                                            
                                             $portfolio_cats_to_add = implode(',', array_unique($portfolio_cats_to_add_array));
                                          ?>
                                          <input id="portfolio_cats_to_add" name="portfolio_cats_to_add" type="hidden" value="<?php echo $portfolio_cats_to_add; ?>" />
                                          <?php
                                          }
                                          ?>
                                          
                          <input type="submit" class="btn btn-primary btn-upload" value="Submit" name="submitVideoButton" id="submitVideoButton" />
                           
                          </div>
                          <!--<div class="text-right">
                            <p class="upload-content">An administrator will be notified that your video has been submitted</p>
                          </div>-->
                          <div class="clear"></div>
                                             
                          <!--<div class="spacing"></div>  -->
                          
                        </div>
                      </div>                    
                     
                    </div>
                  </form>
                </div>  
                <?php } ?>
              </div> 
                                             
              </div>
              <!--<div class="spacing"></div>-->
                  </div>
                  <div class="clear hidden"></div>
                  <?php
                  // get_sidebar( 'wbchannels' );
                  ?>                      
               </div>
            </div>
         </div>      
         <script>
				 function showUploadDiv(){
							$('#uploader-div').show();
							$('#playerDiv').hide();
							$('#showUploadDivButton').hide();			
							if( $('#container1 object').attr('id') != 'myExperience' ){
								$('#showVideoDivButton').show();								
							}
							else{
								$('#showVideoDivButton').hide();
							}			
						}
						
						function showVideoDiv(){
							if( $('#container1 object').attr('id') != 'myExperience' ){
								$('#uploader-div').hide();
								$('#playerDiv').show();
								$('#showUploadDivButton').show();
								$('#showVideoDivButton').hide();
								$('#uploadedFileName').html('');
								$('#uploadedFileDiv').html('');				
							}
						}		
					 </script>
					 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
					 <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars 
					 <link rel="stylesheet" href="<?php echo plugins_url( '/workerbee-channels-v2/js/jQueryFileshowVideoDiv/css/jquery.fileupload.css'); ?>">-->
         <link rel="stylesheet" href="<?php echo plugins_url( '/workerbee-channels-v2/js/jQueryFileUpload/css/jquery.fileupload-ui.css'); ?>">
         <!-- CSS adjustments for browsers with JavaScript disabled -->
         <noscript><link rel="stylesheet" href="<?php echo plugins_url( '/workerbee-channels-v2/js/jQueryFileUpload/css/jquery.fileupload-noscript.css'); ?>"></noscript>
         <noscript><link rel="stylesheet" href="<?php echo plugins_url( '/workerbee-channels-v2/js/jQueryFileUpload/css/jquery.fileupload-ui-noscript.css'); ?>"></noscript>
				 <!--uploader-->
				 <link rel="stylesheet" href="<?php echo plugins_url( '/workerbee-channels-v2/css/channelsv2.css'); ?>">
				 <link rel="stylesheet" href="<?php echo plugins_url( '/workerbee-channels-v2/css/overwrite.css'); ?>">
				 <script src="<?php echo plugins_url( '/workerbee-channels-v2/js/jquery.mmenu.min.all.js'); ?>" type="text/javascript"></script>
				 <script src="<?php echo plugins_url( '/workerbee-channels-v2/js/responsiveTruncator.js'); ?>" type="text/javascript"></script>
				 
				 <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
				 
				 
				 <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
				 <script src="http://d1gvnlj9wix88m.cloudfront.net/js/bootstrap.min.js"></script>
				 <script src="http://d1gvnlj9wix88m.cloudfront.net/js/jquery.multi-select.js" type="text/javascript"></script>
				 <script src="http://d1gvnlj9wix88m.cloudfront.net/js/jquery.validate.min.js" type="text/javascript"></script>
				 <script src="http://d1gvnlj9wix88m.cloudfront.net/js/additional-methods.min.js" type="text/javascript"></script>
				 <script src="<?php echo plugins_url( '/workerbee-channels-v2/js/jQueryFileUpload/js/vendor/jquery.ui.widget.js'); ?>" type="text/javascript"></script>
				 <script src="<?php echo plugins_url( '/workerbee-channels-v2/js/jQueryFileUpload/js/jquery.iframe-transport.js'); ?>" type="text/javascript"></script>				 
				 <script src="<?php echo plugins_url( '/workerbee-channels-v2/js/jQueryFileUpload/js/jquery.fileupload.js'); ?>" type="text/javascript"></script>
				 <script>
		$(function () {
				$('#videoUpload').fileupload({
						dataType: 'json',
						dropZone: $('#drop-zone'),
						add: function(e, data) {
              var uploadErrors = [];
              var reponse;
              var acceptFileTypes = /^video\/mp4$/i;
              if( jQuery.trim(jQuery('#uploadedVideoFileUrl').val()) != '' ){
                reponse = confirm('Are you sure you would like to upload a new file? This will replace '+jQuery('#uploadedVideoFileName').val()+'.');
              }
              
              if(reponse ==  true){
                data.submit();
              }
              else if(reponse ==  false){
                return;
              }              
              
              if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                  uploadErrors.push('Please upload an mp4 video file.');
              }
              if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000) {
                  uploadErrors.push('Filesize is too big');
              }
              

              if(uploadErrors.length > 0) {
                  alert(uploadErrors.join("\n"));
              } else {
                  data.submit();
              }
      }
			,done: function (e, data) {
							$('#submitVideoButton').removeAttr('disabled');
							$('#btn-back').removeAttr('disabled');
							$('#fileUploadProgress').hide();							
							$('#uploadedFileDiv').show();
							var uploadedFile = data.result.files[0];
							$('#uploadedFileDiv').html(uploadedFile.name+' was uploaded successfully.');
							$('#uploadedFileName').val(uploadedFile.name);
							$('#uploadedFileUrl').val(uploadedFile.url);
							$('#uploadedFileDeleteUrl').val(uploadedFile.deleteUrl);
							$('#editVideoButtonsDiv').show();
							if( $('#container1 object').attr('id') != 'myExperience' ){
								$('#showVideoDivButton').show();								
							}
							else{
								$('#showVideoDivButton').hide();
							}
						},
						progress: function (e, data) {
								var progress = parseInt(data.loaded / data.total * 100, 10);
								$('#submitVideoButton').attr('disabled', 'disabled');
								$('#btn-back').attr('disabled', 'disabled');
								$('#fileUploadProgress').show();
								$('#fileUploadProgress .progress-bar').css('width', progress+'%');
								$('#fileUploadProgress .progress-bar .sr-only').html(progress+'%');
								if(progress==100){
									$('#fileUploadProgress .progress-bar .sr-only').html("Upload has completed. Please wait while it processes.");
								}
						}
				});
		});
		</script>
		<style>
			
			@media (min-width: 768px){
				#wbc-main-content.col-sm-8{
						width: 65.667% !important;
						padding: 0 !important;
				}
			}
			#wbc-video-upload .nav-tabs,#wbc-video-upload .tab-content{
				border:0px !important;
			}
			@media (min-width: 1025px){
				#wbc-content{
					//border-left:#d2d9dd 1px solid;
					//border-right:1px solid #d2d9dd
				}
			}			
			#wb-uploader-div-wrapper{
				margin-top:40px
			}
			.form-control, #select-categories{
				width:100%
			}
			#video-details .form-group:nth-child(-n+4){
				width:45%;
				display:inline-block
			}
			#video-details .form-group:nth-child(2),#video-details .form-group:nth-child(4){
				float:right;
			}
			.form-group:nth-child(n+3){
				margin-top:20px;
			}
			input.form-control{
				border: 1px solid #777 !important;
				height:25px !important
			}
			textarea.form-control{
				border: 1px solid #777 !important;
				height:100px !important
			}
			#wbc-upload-video-page-body-paragraph > p{
				margin-top:0
			}
			#drop-zone{
				width:99%
			}
			#select-categories{
				height:100px !important
			}
			.wbc-form-div .tab-content{
				padding:15px 0 !important
			}
			#submitVideoButton{
				background-color:#284877;
				color: #ffffff;
				text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
				background-image: -moz-linear-gradient(top, #284877, #284877);
				background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#284877), to(#284877));
				background-image: -webkit-linear-gradient(top, #284877, #284877);
				background-image: -o-linear-gradient(top, #284877, #284877);
				background-image: linear-gradient(to bottom, #284877, #284877);
				background-repeat: repeat-x;
				border-color: #284877 #284877 #284877;
			}
			#wbc-content .container{
				 width:100% !important;
				 max-width:100% !important
			 }
			 #wbc-main-content{
					 //width: 65.81196581196582% !important;
					 float:left;padding:0 !important;
			 }
			 #wb_ent_sidebar1{
				 width: 31.623931623931625%;
				 max-width:300px
			 }
			 #wbc-upload-video-page-body-paragraph > p{
				 margin-top:0
			 }
			 @media (min-width: 1200px){
				 #wb_ent_sidebar1,.span4{
					 width:300px !important
				 }
			 }
			 #fileUploadProgress span.sr-only{
				 background-color:#337ab7;
				 color:white
			 }
			 .span8{
				 width:100% !important;
				 margin-left:0 !important
			 }
			 input[type="submit"]:disabled,input[disabled] {
				background: #C0C0C0;
				color: #ffffff;
				text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
				background-image: -moz-linear-gradient(top, #C0C0C0, #C0C0C0) !important;
				background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#C0C0C0), to(#C0C0C0)) !important;
				background-image: -webkit-linear-gradient(top, #C0C0C0, #C0C0C0) !important;
				background-image: -o-linear-gradient(top, #C0C0C0, #C0C0C0) !important;
				background-image: linear-gradient(to bottom, #C0C0C0, #C0C0C0) !important;
				background-repeat: repeat-x;
				border-color: #C0C0C0 #C0C0C0 #C0C0C0 !important;
			}
			@media (max-width: 767px){
				.hidden{display:block}
				#wbc-main-content,#wb_ent_sidebar1{width:100% !important;margin:0 auto 10px}
				#wbc-upload-video-page-body-paragraph{text-align:center}
				#wbc-upload-video-page-body-paragraph ol{text-align:left}
			}
			@media (max-width: 470px){
				#video-details .form-group:nth-child(-n+4){width:100%;}
			}
			@media only screen and (min-width: 1024px){
				.container {
						max-width: 1024px;
				}
			}
		</style>
<?php
// get_footer( 'wbchannels' );
?>