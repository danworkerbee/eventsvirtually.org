<?php
/**
 * filename: search.php
 * description: this will be the template to be used to display search results
 * author: Jullie Quijano
 * date created: 2014-04-15
 *
 *
 * @package WordPress
 * @subpackage Enterprise
 
 * Template Name: Keywords
 */
global $wb_ent_options, $wpdb, $privatePostsIds, $unlistedPostsIds, $paged, $page;
get_header();
?>
<div id="wb_ent_content" class="clearfix row-fluid">
    <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
        <div id="viewing-tips">

            <?php 
            $videoCounter = 0;
            //echo $paged;
            $s = get_search_query();
            $paged = $page;
            $args = array(
            		'post_type'			=> array('post','wb_audio'),
            		'paged' 			=> $paged,
            		'category__in' 		=> array( $catlibrary, $wb_ent_options['videocats']['podcast']),
            		'categoty__not_in' 	=> array($wb_ent_options['videocats']['unlisted'], $wb_ent_options['videocats']['private']),
            		's' 				=> $s,
            );
            
            query_posts( $args ); 
            if (have_posts()) : 
            ?>	

                <div class="page-header"><h1 class="pagetitle">Video Results for "<?php the_search_query(); ?>"</h1></div>
                <div id="video-results" class="video-list-1">
                    <ul class="thumbnails">                  



                        <?php while (have_posts()) : the_post(); ?>
                            <?php
                            $post_id = $wp_query->post->ID;
                            if( ( !is_user_logged_in() ) && (in_array( $post_id,  $privatePostsIds) || in_array( $post_id,  $unlistedPostsIds)  ) ){
                               continue; 
                            }
                            $videoCounter++;
                            $video = wb_get_post_details($post_id);
                            ?>

                            <?php
                            $image = '<img src="' . $video['smlThumb'] . '" alt="' . $video['title'] . '" />';
                            $postContent = wb_format_string($video['desc'], false, false, 170, '... <a href="' . get_permalink() . '"><span class="more-link">more</span></a>');
                            ?>



                            <li class="span12 wb-subpage-block-wrapper">
                                <div class="thumbnail no-style span4">
                                    <a href="<?php the_permalink(); ?>"> <?php echo $image; ?> </a>

                                </div>
                                <div class="span7">
                                    <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                                    <p><?php echo $postContent; ?></p>
                                    <?php
                                    if (count($video['tags']) > 0) {
                                        ?>
                                        <p class="tags">
                                            <?php echo $video['taglisthtml']; ?>
                                        </p> 
                                        <?php
                                    }
                                    ?>
                                </div>
                            </li>
                        <?php endwhile;?>
                    </ul>
                </div>
                <?php
                // Find total number of pages
                
                
                $pageNumber = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $theQuery = $wp_query->request;
                $theQuery = substr_replace($theQuery, "", strpos($theQuery, "LIMIT"), strlen($theQuery));
                $get_max_pages = $wpdb->query($theQuery);
                $max_pages = ceil(($get_max_pages) / $wb_ent_options['videolistinfo']['vidsperpage']);
                if ($max_pages > 1) {
                	$wb_prev = get_previous_posts_link(__('&laquo; Older Entries'));
                	$wb_next = get_next_posts_link(__('Newer Entries &raquo;'));
                	//echo $pageNumber;
                    ?>
                    <div class="pageNav">
                        <div id="paginationLeft">&nbsp;<?php 
                        if ( $wb_prev != "" ){
                        	echo '<a href="/?page='.($pageNumber-1).'&s='.$s.'" class="btn btn-info">&laquo; Previous</a>';
                        //previous_posts_link('&laquo; Previous') 
                        }
                        ?></div>
                        <div id="paginationMid">Page <?php echo $pageNumber ?> of <?php echo $max_pages ?></div>
                        <div id="paginationRight"><?php 
                        if ( $wb_next != "" ){
                        	echo '<a href="/?page='.($pageNumber+1).'&s='.$s.'" class="btn btn-info">Next &raquo;</a>';
                        	//next_posts_link('Next &raquo;') ;
                        }
                        ?>&nbsp;</div>
                    </div>
                    <?php
                }
                ?>


            <?php 
            endif;
          
            if( $videoCounter <= 0 ){
            ?>

                <h1 class="center">No videos found.</h1>
                <p>Try a different search or:</p>

                <?php
                $args = array(
                		'numberposts'   => 1,
                		'post_type'     => 'post',
                		'category'		=> $wb_ent_options['videocats']['library'],
                		'orderby'   	=> 'rand'
                );
                
                $results = get_posts( $args );
                $wb_random_post = "home";
                
                if ( $results[0]->post_name != "" )
                	$wb_random_post = $results[0]->post_name;
                	?>
	            <div id="linkList404">
	                <p>&raquo; <a href="<?php echo get_site_url() . '/' . $wb_random_post; ?>">Random Content</a></p>
	                <p>&raquo; <a href="<?php echo get_site_url() . '/home'; ?>">Go Back to Library</a></p>
	                <p>&raquo; <a href="<?php echo get_site_url() . '/'; ?>viewing-tips">Read Viewing Tips</a></p>
	                <p>&raquo; <a href="<?php echo get_site_url() . '/'; ?>contact">Contact Us</a></p>
	            </div>                

                <p>&nbsp;</p>

            <?php 
            }
            ?>


        </div>
    </div>
    <?php
    get_sidebar();
    get_footer();
    ?>