<?php
/**
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: Default Full Page
 */
get_header();
if (have_posts()) : while (have_posts()) : the_post(); 
?>
<div id="wb_ent_content" class="clearfix row-fluid">
	<div id="main" class="span12 clearfix" role="main" style="border: 0px solid black;">
    <div id="viewing-tips">
        <h1><?php the_title(); ?></h1>
    		<?php the_content(); ?>
    		
    
    		<?php endwhile; else: ?>
    		<p>&nbsp;</p>
    		<?php endif; ?>
      
    </div>
  </div>
  
<?php get_footer(); ?>