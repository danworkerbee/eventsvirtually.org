<?php
/*
Filename:     archiveWidgetJsonCron.php                                                 
Description:  this file will create a json file that will be used by the archiveWidget.php; this version only adds the list of 10 most recent posts and does not get the popular videos, episodes, or cat list videos.
Author:       Jullie Quijano
Version:      2.1.1
Change Log:
   November 13, 2012    [Jullie]updated the search and replace array
   November 6, 2012     [Jullie]fixed paths for cron
   November 5, 2012     [Jullie]added postPassword field for each post
   September 19, 2012   [Jullie]created the file
*/
error_reporting(E_ALL);
   header('Content-Type: text/plain; charset=utf-8');

   //list of characters to replace

   $strReplaceSearch = array(
   		"¡", "¢", "£", "¤", "¥", "¦", "§", "¨", "©", "ª",
   		"«", "¬", "®", "¯", "°", "±", "²", "³", "´",
   		"µ", "¶", "·", "¸", "¹", "º", "»", "¼", "½", "¾",
   		"¿", "×", "÷", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ",
   		"Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", "Ð",
   		"Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û",
   		"Ü", "Ý", "Þ", "ß", "à", "á", "â", "ã", "ä", "å",
   		"æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï",
   		"ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú",
   		"û", "ü", "ý", "þ", "ÿ", "  ", "\r", "\n", "\t", "\v",
   		"“", "‘", "’", "“", "”", "…", "–", "—", "“", "”", "-", " ", "•", "€", "™"
   );
   
   $strReplaceChars = array(
   		"&#161;", "&#162;", "&#163;", "&#164;", "&#165;", "&#166;", "&#167;", "&#168;", "&#169;", "&#170;",
   		"&#171;", "&#172;", "&#174;", "&#175;", "&#176;", "&#177;", "&#178;", "&#179;", "&#180;",
   		"&#181;", "&#182;", "&#183;", "&#184;", "&#185;", "&#186;", "&#187;", "&#188;", "&#189;", "&#190;",
   		"&#191;", "&#215;", "&#247;", "&#192;", "&#193;", "&#194;", "&#195;", "&#196;", "&#197;", "&#198;",
   		"&#199;", "&#200;", "&#201;", "&#202;", "&#203;", "&#204;", "&#205;", "&#206;", "&#207;", "&#208;",
   		"&#209;", "&#210;", "&#211;", "&#212;", "&#213;", "&#214;", "&#216;", "&#217;", "&#218;", "&#219;",
   		"&#220;", "&#221;", "&#222;", "&#223;", "&#224;", "&#225;", "&#226;", "&#227;", "&#228;", "&#229;",
   		"&#230;", "&#231;", "&#232;", "&#233;", "&#234;", "&#235;", "&#236;", "&#237;", "&#238;", "&#239;",
   		"&#240;", "&#241;", "&#242;", "&#243;", "&#244;", "&#245;", "&#246;", "&#248;", "&#249;", "&#250;",
   		"&#251;", "&#252;", "&#253;", "&#254;", "&#255;", " ", " ", " ", " ", " ",
   		"\"", "'", "'", "\"", "\"", "...", "-", "-", "\"", "\"", "-", " ", " ", "", "&#153;"
   );          
      
   $prewd = getcwd();
   
   echo '$prewd is '.$prewd.'<br />';
   chdir(realpath(dirname(__FILE__)));
   echo 'getcwd() is '.getcwd().'<br />';
   
   error_reporting(E_ALL);
   
   
   include_once '../../../../wp-config.php';
   
   echo 'connecting to database...<br />';
   // connecting to database
   $DBhost = DB_HOST;
   $DBname = DB_NAME;
   $DBuser = DB_USER;
   $DBpass = DB_PASSWORD;
      
   GLOBAL $bc_account_id, $access_token;
   $bc_account_id  =  $wb_ent_options['brightcoveinfo']['publisherid'];
   $access_token = wb_get_auth_token();
   
   //get theme options
    try {
          $dbh = new PDO('mysql:host='.$DBhost.';dbname='.$DBname.'', $DBuser, $DBpass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

          $getThemeOptions = $dbh->prepare("SELECT option_value FROM wp_options WHERE option_name = 'wb_ent_options'");

          $getThemeOptions->execute();      
          $getThemeOptionsNumrows = $getThemeOptions->rowCount();
          $getThemeOptionsResult = $getThemeOptions->fetch();

          $getThemeOptions= null;  
          $dbh = null; 

       }catch (PDOException $e) { 
          echo "Error!: Could not connect to DB";
       }          
   $wb_ent_options = unserialize($getThemeOptionsResult['option_value']);
  
   $libcat = $wb_ent_options['videocats']['library'];   
   $frenchcat = $wb_ent_options['videocats']['library-fr_FR'];
   $shortcodeChannel = $wb_ent_options['vidshortcode'];
   //$wp_languages = array('default', 'en_US');
   $default = array('default'=>array('catid'=>$libcat),
                         'en_US'=>array('catid'=>$libcat));
   $wp_languages  = $default;                     
   if( trim($frenchcat) != '' ){   
   $frencharray = array('fr_FR'=>array('catid'=>$frenchcat));
   $wp_languages = array_merge($default, $frencharray);
   }
   
   echo 'these are all the available languages in wordpress'.print_r($wp_languages, true);
   
   
   //echo '$getThemeOptionsResult is '.$getThemeOptionsResult['option_value'];
   //echo '$wb_ent_options is '.print_r($wb_ent_options, true);
   echo 'clientname is '.$wb_ent_options['clientname'];
   
   if (!class_exists('SimpleImage'))require_once('SimpleImage.php'); 
   
   //include the S3 class  
   if (!class_exists('S3'))require_once('S3.php'); 
   $s3 = new S3($wb_ent_options['amazons3info']['accesskey'], $wb_ent_options['amazons3info']['secretkey']); 
   
   $passwordProtectedCondition = " AND p.post_password   = '' ";
   if( $wb_ent_options['poststodisplay']['passwordprotected'] ){
       $passwordProtectedCondition = '';
   }
   
   //get private && unlisted videos
   $unlistedAndPrivateVids = array();
   

    try {
       $dbh = new PDO('mysql:host='.$DBhost.';dbname='.$DBname.'', $DBuser, $DBpass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));


       $catUnlisted = isset($wb_ent_options['videocats']['unlisted']) ? $wb_ent_options['videocats']['unlisted'] : 0 ;
       $catPrivate = isset($wb_ent_options['videocats']['private']) ? $wb_ent_options['videocats']['private'] : 0 ;           

       if( $wb_ent_options['vidshortcode']['enabled'] ){
          $getPrivateUnlistedPosts = $dbh->prepare("
             SELECT p.ID, p.post_title, p.post_name, p.post_content 
                FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
                WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
                AND tt.term_id=t.term_id
                AND tr.object_id=p.ID         
                AND p.post_status='publish'
                $passwordProtectedCondition
                AND ( t.term_id=?
                OR t.term_id=? )
                GROUP BY p.ID
                ORDER BY p.post_date DESC 
          ");

          $getPrivateUnlistedPosts->bindParam(1, $catUnlisted);
          $getPrivateUnlistedPosts->bindParam(2, $catPrivate);
       }
       else{
          $getPrivateUnlistedPosts = $dbh->prepare("
             SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content 
             FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media b
             WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
             AND tt.term_id=t.term_id
             AND tr.object_id=p.ID
             AND p.ID=b.post_id
             $passwordProtectedCondition
             AND p.post_status='publish'
             AND ( t.term_id=?
             OR t.term_id=? )
             GROUP BY p.ID
             ORDER BY p.post_date DESC
          ");

          $getPrivateUnlistedPosts->bindParam(1, $catUnlisted);
          $getPrivateUnlistedPosts->bindParam(2, $catPrivate);


       }           

       $getPrivateUnlistedPosts->execute();      
       $getPrivateUnlistedPostsNumrows = $getPrivateUnlistedPosts->rowCount();
       $getPrivateUnlistedPostsResult = $getPrivateUnlistedPosts->fetchAll();

       $getPrivateUnlistedPosts= null;  
       $dbh = null; 

    }catch (PDOException $e) { 
       echo "Error!: Could not connect to DB";
    }       

   foreach ($getPrivateUnlistedPostsResult as $currentPrivateUnlistedPost) {
      $unlistedAndPrivateVids[] = $currentPrivateUnlistedPost['ID'];
   }
   
   
   echo '$getPrivateUnlistedPostsResult is '.print_r($getPrivateUnlistedPostsResult);
  $archiveWidgetVideoArray = array();
  foreach($wp_languages as $key => $lang){
  echo 'This is the category:'.$lang['catid']; 
   //get all the posts
   try {
      $dbh = new PDO('mysql:host='.$DBhost.';dbname='.$DBname.'', $DBuser, $DBpass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
           
           
      if( $wb_ent_options['vidshortcode']['enabled'] ){
         $getAllPosts = $dbh->prepare("SELECT p.ID, p.post_title, p.post_name, p.post_content, p.post_password 
               FROM wp_posts p
               WHERE post_type = 'post' 
               AND (p.post_status='publish')   
               $passwordProtectedCondition
               ORDER BY p.post_date DESC
               LIMIT 10
               ");
      }
      else{
         $getAllPosts = $dbh->prepare("SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content, p.post_password 
            FROM wp_posts p, wb_media b, wb_media m, wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t
            WHERE post_type = 'post' 
            AND tt.term_taxonomy_id = tr.term_taxonomy_id
            AND tt.term_id=t.term_id
            AND tr.object_id=p.ID 
            AND p.ID=b.post_id
            AND (p.post_status='publish')  
            $passwordProtectedCondition
            AND t.term_id=?
            GROUP BY p.ID
            ORDER BY p.post_date DESC
            LIMIT 10
            ");
            $getAllPosts->bindParam(1, $lang['catid']);
      }                    
         
      $getAllPosts->execute();      
      $getAllPostsNumrows = $getAllPosts->rowCount();
      $getAllPostsResult = $getAllPosts->fetchAll();
               
      $getAllPosts= null;  
      $dbh = null; 
              
   }catch (PDOException $e) { 
      echo "Error!: Could not connect to DB";
   }       

    if( !file_exists('/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/') ){
        mkdir('/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/');
    }

    $videoStillDir = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/stills/';
    if( !file_exists($videoStillDir) ){
       mkdir($videoStillDir);
    }
    
    $slidesDir = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/slides/';
    if( !file_exists() ){
       mkdir($slidesDir);
    }
    
    $thumbsDir = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/thumbs/';
    if( !file_exists($thumbsDir) ){
       mkdir($thumbsDir);
    }   
    
      chmod($videoStillDir, 0777);
      chown($videoStillDir, 48);
      
      chmod($slidesDir, 0777);
      chown($slidesDir, 48);
      
      chmod($thumbsDir, 0777);
      chown($thumbsDir, 48);
   
   
     
   $archiveWidgetVideo = array();
   foreach ($getAllPostsResult as $currentVideo)
   {
         if( in_array($currentVideo['ID'], $unlistedAndPrivateVids) ){
            continue;               
         }
      
      
      //print_r($currentVideo);
      echo'
      shortcode enabled is '.($wb_ent_options['vidshortcode']['enabled']).'-
      pregmatch mode is '.(preg_match_all('/\['.$wb_ent_options['vidshortcode']['tag'].'(.*)videoid(.*)="(.*)"(.*)mode(.*)'.$shortcodeChannel['name'].'(.*)\/\]/', $currentVideo['post_content'], $match)).'-
      pregmatch is '.(preg_match_all('/\['.$wb_ent_options['vidshortcode']['tag'].'(.*)videoid(.*)="(.*)"(.*)\/\]/', $currentVideo['post_content'], $match)).'-';
      
      $curlString = '';
      if( $wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\['.$wb_ent_options['vidshortcode']['tag'].'(.*)videoid(.*)="(.*)"(.*)mode(.*)'.$shortcodeChannel['name'].'(.*)\/\]/', $currentVideo['post_content'], $match) >= 1 ){         
         $archiveWidgetVideo[$currentVideo['ID']]['id'] = $match[3][0];
         $curlString = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id='.$archiveWidgetVideo[$currentVideo['ID']]['id'].'&video_fields=id,name,videoStillURL,thumbnailURL&media_delivery=http&token='.$shortcodeChannel['token'].'&v='.time();            
      }
      else if( $wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\['.$wb_ent_options['vidshortcode']['tag'].'(.*)videoid(.*)="(.*)"(.*)\/\]/', $currentVideo['post_content'], $match) >= 1 ){
         $archiveWidgetVideo[$currentVideo['ID']]['id'] = $match[3][0];
         $curlString = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id='.$archiveWidgetVideo[$currentVideo['ID']]['id'].'&video_fields=id,name,videoStillURL,thumbnailURL&media_delivery=http&token='.$wb_ent_options['brightcoveinfo']['readurltoken'].'&v='.time();             
      }
      else{
         if( trim($currentVideo['media_id']) != '' ){
            $archiveWidgetVideo[$currentVideo['ID']]['id'] = $currentVideo['media_id'];
         }
         else{
            $noShortCodeSql = sprintf("SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content 
            FROM wp_posts p, wb_media b
            WHERE post_type = 'post' 
            AND p.ID=b.post_id
            AND (p.post_status='publish')    
            $passwordProtectedCondition
            AND p.ID = '%s'
            GROUP BY p.ID
            ORDER BY p.post_date DESC  
            LIMIT 1
            ", $currentVideo['ID']);
            $noShortCodeResult = mysql_query($noShortCodeSql); 
            $noShortCodeCurrentVideo = mysql_fetch_array($noShortCodeResult);
            
            $archiveWidgetVideo[$currentVideo['ID']]['id'] = $noShortCodeCurrentVideo['media_id'];
         }
         
         if( trim($archiveWidgetVideo[$currentVideo['ID']]['id']) != '' ){
         	 $curlString = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id='.$archiveWidgetVideo[$currentVideo['ID']]['id'].'&video_fields=id,name,videoStillURL,thumbnailURL&media_delivery=http&token='.$wb_ent_options['brightcoveinfo']['readurltoken'].'&v='.time();
         }
         
      }
      
      echo '$curlString is '.$curlString;
      
      $archiveWidgetVideo[$currentVideo['ID']]['post_id'] = $currentVideo['ID'];
      $archiveWidgetVideo[$currentVideo['ID']]['title'] = str_replace($strReplaceSearch, $strReplaceChars, trim($currentVideo['post_title']));
      $archiveWidgetVideo[$currentVideo['ID']]['titleShort'] = limit($archiveWidgetVideo[$currentVideo['ID']]['title'], 60);  
      
      if( $wb_ent_options['vidshortcode']['enabled'] ){
         $archiveWidgetVideo[$currentVideo['ID']]['desc'] = str_replace($strReplaceSearch, $strReplaceChars, strip_tags( removeShortCode($wb_ent_options['vidshortcode']['tag'], trim($currentVideo['post_content'])) ) );    
         $archiveWidgetVideo[$currentVideo['ID']]['descShort'] = limit($archiveWidgetVideo[$currentVideo['ID']]['desc'], 80 );
      }
      else{
         $archiveWidgetVideo[$currentVideo['ID']]['desc'] = str_replace($strReplaceSearch, $strReplaceChars, strip_tags( trim($currentVideo['post_content']) ) );    
         $archiveWidgetVideo[$currentVideo['ID']]['descShort'] = limit( $archiveWidgetVideo[$currentVideo['ID']]['desc'], 80 );         
      }
      
      if ($archiveWidgetVideo[$currentVideo['ID']]['descShort']!=trim($currentVideo['desc'])) {
         $archiveWidgetVideo[$currentVideo['ID']]['descShort'].=' <a href="'.$siteRoot.$currentVideo['post_name'].'"><span class="link">[more]</span></a>';
      }
      
      $archiveWidgetVideo[$currentVideo['ID']]['permalink'] = $currentVideo['post_name'];

      echo '$archiveWidgetVideo[$currentVideo[ID]] is '.$archiveWidgetVideo[$currentVideo['ID']];
      
      $videoStill = '';
      if( trim($curlString) != '' ){  
      	/*
	      $ch = curl_init();
	      $timeout = 0; // set to zero for no timeout
	      curl_setopt ($ch, CURLOPT_URL, $curlString );
	      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
	      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	      $file_contents = curl_exec($ch);
	      curl_close($ch);
	      $returndata = json_decode($file_contents);
	      $videoStill = $returndata->videoStillURL;
	   */   
	      $wb_post_image_information = wb_get_video_images($archiveWidgetVideo[$currentVideo['ID']]['id']);
	      $wb_post_image_information_arr = json_decode($wb_post_image_information, true);
	      
	      $wb_new_api_thumbnail = $wb_post_image_information_arr['thumbnail']['src'];
	      $wb_new_api_poster = $wb_post_image_information_arr['poster']['src'];
	      $videoStill = $wb_new_api_poster;
	      
	     // echo "new bc api videostill " . $videoStill;
      }
	      
      
      echo 'debug: $postID is '.$currentVideo['ID'].'<br />';
      echo 'debug: $mediaId is '.$archiveWidgetVideo[$currentVideo['ID']]['id'].'<br />';
      echo 'debug: $videoStill is '.$videoStill.'<br />';
      echo 'debug: $videoStill is <img src="'.$videoStill.'" /> <br />';
      
      if( trim($videoStill) == '' || is_null($videoStill) || trim($archiveWidgetVideo[$currentVideo['ID']]['id']) == '' || is_null($archiveWidgetVideo[$currentVideo['ID']]['id']) ){
         $videoStill = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].$wb_ent_options['defaultstill'];
      }
      
      $destinationFile = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/stills/'.$currentVideo['ID'].'_still.jpg';
      chmod($destinationFile, 0777);
      
      
      if( !copy($videoStill, $destinationFile) ){
          echo 'error copying $videoStill '.$videoStill;
      }
      chmod($destinationFile, 0777);
      chown($destinationFile, 48);
            
      echo 'debug: $videoStill is '.$videoStill;
      
      $image = new SimpleImage();
      $image->load('/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/stills/'.$currentVideo['ID'].'_still.jpg');
      
      /**
      save video still on s3
      */
      $videoStillFileName = 'images/stills/'.$currentVideo['ID'].'_still.jpg';
      $filePath = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/stills/'.$currentVideo['ID'].'_still.jpg';
      
      echo 'debug: $filePath is '.$filePath;
      echo 'debug: channelbucket is '.$wb_ent_options['amazons3info']['channelbucket'];
      echo 'debug: $videoStillFileName is '.$videoStillFileName;
      
      if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $videoStillFileName, S3::ACL_PUBLIC_READ)) {  
         echo "debug: We successfully uploaded your file.<br />";  
      }else{           
         echo "debug: Something went wrong while uploading your file... sorry.<br />";  
      } 
      
      
      
      /**
      create large-sized thumb
      */
      $image->resize(375,211);
      $destinationFileLrgThumb = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/thumbs/'.$currentVideo['ID'].'_largeThumb.jpg';
      $image->save($destinationFileLrgThumb, IMAGETYPE_JPEG, 100, 0777);
      
      $largeThumbFileName = 'images/thumbs/'.$currentVideo['ID'].'_largeThumb.jpg';
      $filePath = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/thumbs/'.$currentVideo['ID'].'_largeThumb.jpg';
      
      echo 'debug: $filePath is '.$filePath;
      echo 'debug: channelbucket is '.$wb_ent_options['amazons3info']['channelbucket'];
      echo 'debug: $largeThumbFileName is '.$largeThumbFileName;
      
      if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $largeThumbFileName, S3::ACL_PUBLIC_READ)) {  
         echo "debug: We successfully uploaded your file.<br />";  
      }else{           
         echo "debug: Something went wrong while uploading your file... sorry.<br />";  
      }        
      
      $archiveWidgetVideo[$currentVideo['ID']]['largeThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'].'/'.$largeThumbFileName;
      
      $largeThumbFileName = null;
      
      echo 'debug: largeThumb is '.$archiveWidgetVideo[$currentVideo['ID']]['largeThumb'].'<br />';
      echo 'debug: largeThumb is <img src="'.$archiveWidgetVideo[$currentVideo['ID']]['largeThumb'].'" /> <br />';
            
      
      
      /**
      create mid-sized thumb
      */
      $image->resize(240,135);
      $destinationFileMidThumb = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/thumbs/'.$currentVideo['ID'].'_midThumb.jpg';  
      $image->save($destinationFileMidThumb, IMAGETYPE_JPEG, 100, 0777);
      
      $midThumbFileName = 'images/thumbs/'.$currentVideo['ID'].'_midThumb.jpg';
      $filePath = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/thumbs/'.$currentVideo['ID'].'_midThumb.jpg';
      
      echo 'debug: $filePath is '.$filePath;
      echo 'debug: channelbucket is '.$wb_ent_options['amazons3info']['channelbucket'];
      echo 'debug: $midThumbFileName is '.$midThumbFileName;      
      
      if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $midThumbFileName, S3::ACL_PUBLIC_READ)) {  
         echo "debug: We successfully uploaded your file.<br />";  
      }else{  
         echo "debug: Something went wrong while uploading your file... sorry.<br />";  
      }        
      
      $archiveWidgetVideo[$currentVideo['ID']]['midThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'].'/'.$midThumbFileName;
      
      $midThumbFileName = null;
      
      echo 'debug: midThumb is '.$archiveWidgetVideo[$currentVideo['ID']]['midThumb'].'<br />';
      echo 'debug: midThumb is <img src="'.$archiveWidgetVideo[$currentVideo['ID']]['midThumb'].'" /> <br />';
      
      
      $image->resize(160,90);
      $destinationFileSlide = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/slides/'.$currentVideo['ID'].'_slide.jpg';   
      $image->save($destinationFileSlide, IMAGETYPE_JPEG, 100, 0777);
      
      $slideFileName = 'images/slides/'.$currentVideo['ID'].'_slide.jpg';
      $filePath = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/slides/'.$currentVideo['ID'].'_slide.jpg';
      
      echo 'debug: $filePath is '.$filePath;
      echo 'debug: channelbucket is '.$wb_ent_options['amazons3info']['channelbucket'];
      echo 'debug: $slideFileName is '.$slideFileName;        
      
      if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $slideFileName, S3::ACL_PUBLIC_READ)) {  
          echo "debug: We successfully uploaded your file.<br />";  
      }else{  
          echo "debug: Something went wrong while uploading your file... sorry.<br />";  
      }        
      
      $archiveWidgetVideo[$currentVideo['ID']]['slide'] = $wb_ent_options['amazons3info']['channelbucketurl'].'/'.$slideFileName;
      //$archiveWidgetVideo[$currentVideo['ID']]['thumb'] = '/images/thumbs/'.$currentVideo['ID'].'_thumb.jpg';
      echo 'debug: $slide is '.$archiveWidgetVideo[$currentVideo['ID']]['slide'].'<br />';
      
      echo 'debug: $slide is <img src="'.$archiveWidgetVideo[$currentVideo['ID']]['slide'].'" /><br />';
      
      /**
      create actual thumb
      */
      $image->resize(96,54);
      $destinationFileThumb = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/thumbs/'.$currentVideo['ID'].'_thumb.jpg';
      $image->save($destinationFileThumb, IMAGETYPE_JPEG, 100, 0777);
      
      $thumbFileName = 'images/thumbs/'.$currentVideo['ID'].'_thumb.jpg';
      $filePath = '/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/images/thumbs/'.$currentVideo['ID'].'_thumb.jpg';
      
      echo 'debug: $filePath is '.$filePath;
      echo 'debug: channelbucket is '.$wb_ent_options['amazons3info']['channelbucket'];
      echo 'debug: $thumbFileName is '.$thumbFileName;           
      
      if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $thumbFileName, S3::ACL_PUBLIC_READ)) {  
         echo "debug: We successfully uploaded your file.<br />";  
      }else{  
         echo "debug: Something went wrong while uploading your file... sorry.<br />";  
      }        
      
      $archiveWidgetVideo[$currentVideo['ID']]['thumb'] = $wb_ent_options['amazons3info']['channelbucketurl'].'/'.$thumbFileName;
      
      $thumbFileName = null;
      
      echo 'debug: $thumb is '.$archiveWidgetVideo[$currentVideo['ID']]['thumb'].'<br />';
      echo 'debug: $thumb is <img src="'.$archiveWidgetVideo[$currentVideo['ID']]['thumb'].'" /> <br />';      

   }//end of first foreach
   
//echo 'debug: $archiveWidgetVideo is '.print_r($archiveWidgetVideo, true).'<br />';     
   

switch($key){
  case 'default' :
    $archiveVideoArray = array();
    foreach($archiveWidgetVideo as $index => $value){
    $archiveVideoArray[] = $index;
    }
    $archiveWidgetArray = $archiveWidgetVideo;
    array_push($archiveWidgetVideoArray, $archiveWidgetVideo);
    break;
  case 'en_US' :    
    $archiveVideoArray = array();
    foreach($archiveWidgetVideo as $index => $value){
    $archiveVideoArray[] = $index;
    }
    $archiveWidgetArray = $archiveWidgetVideo; 
    array_push($archiveWidgetVideoArray, $archiveWidgetVideoen_US);
    break;
  case 'fr_FR' :    
    $archiveVideoArrayfr_FR = array();    
    foreach($archiveWidgetVideo as $index => $value){
    $archiveVideoArrayfr_FR[] = $index;    
    }
    $archiveWidgetArrayfr_FR = $archiveWidgetVideo;
    echo '--17'.print_r($archiveVideoArrayfr_FR, true);
    //$archiveWidgetVideofr_FR = $archiveWidgetVideo;
    array_push($archiveWidgetVideoArray, $archiveWidgetVideofr_FR);
    break;  
}

//}// end of foreach
}//end of second foreach FROM ARCHIVE

echo 'debug: $archiveWidgetVideo --12<br />'; 
foreach ($archiveWidgetVideoArray as $arr){
print_r($arr);
}

echo 'debug: $postInfoCount is '.count($archiveWidgetVideo).'<br />';

echo 'debug: $archiveVideoArray is '.print_r($archiveVideoArray, true).'<br />'; 

$fileToWrite = "var postInfo = ".json_encode($archiveWidgetArray)."; var videoArchive = ".json_encode($archiveVideoArray)."; var videoArchivefr_FR = ".json_encode($archiveVideoArrayfr_FR)."; var postInfofr_FR = ".json_encode($archiveWidgetArrayfr_FR).";";
$file = fopen("postInfo.json", "w") or die("Can't create json file");   // Open
fwrite($file, $fileToWrite);  // Write to file
fclose($file);                // Close 
   
$nullData = strpos($fileToWrite, 'null');
if( $nullData!== false && $nullData > 0 && trim($wb_ent_options['videolistinfo']['videocats']) != '' && ($wb_ent_options['videolistinfo']['videocats'] != $wb_ent_options['videocats']['webcast']) && ($wb_ent_options['videolistinfo']['videocats'] != $wb_ent_options['videocats']['library']) ){
   mail("jullie.quijano@workerbee.tv", $wb_ent_options['clientname']." postInfoJson Cron Null data", "Null data was found at ".$nullData."\r\n".print_r($fileToWrite, true), 'From: webmaster@workerbee.tv' . "\r\n" . 'Reply-To: webmaster@workerbee.tv' . "\r\n" . 'X-Mailer: PHP/' . phpversion());
}  
      

chdir ( $prewd ); 
   
function sortByOneKey(array $array, $key, $asc = true) {
    $result = array();
        
    $values = array();
    foreach ($array as $id => $value) {
        $values[$id] = isset($value[$key]) ? $value[$key] : '';
    }
        
    if ($asc) {
        asort($values);
    }
    else {
        arsort($values);
    }
        
    foreach ($values as $key => $value) {
        $result[$key] = $array[$key];
    }
        
    return $result;
}  


function limit($str,$length,$end='...',$encoding=null){
    if(!$encoding) $encoding = 'UTF-8';
    $str = trim($str);
    $len = mb_strlen($str,$encoding);
    if($len <= $length) return $str;
    else {
        $return = mb_substr($str,0,$length,$encoding);
        return (preg_match('/^(.*[^\s])\s+[^\s]*$/', $return, $matches) ? $matches[1] : $return).$end;
    }
}

// Generate Authentication Token //
function wb_get_auth_token(){
	GLOBAL $bc_account_id;
	$data 			= array();
	$id 			= "559779e8e4b072e9641b841d"; //from settings not sure if this is needed
	$client_id     	= "07aa7b60-2e1c-4775-8bc5-e3f5220d2d8d"; //API From settings
	$client_secret 	= "6a8-bE1KqdzoAbxupjTiDpEriPI4lZHUDS2mxDfU697lWuCRJNRJlJchL-ZbUCsPmC3lj-r8uram6vpPKldFmg"; //API From settings
	$auth_string   	= "{$client_id}:{$client_secret}";
	$request       	= "https://oauth.brightcove.com/v4/access_token?grant_type=client_credentials";
	$ch            	= curl_init($request);
	curl_setopt_array($ch, array(
			CURLOPT_POST           => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_SSL_VERIFYPEER => FALSE,
			CURLOPT_USERPWD        => $auth_string,
			CURLOPT_HTTPHEADER     => array('Content-type: application/x-www-form-urlencoded'),
			CURLOPT_POSTFIELDS => $data
	));
	$response = curl_exec($ch);
	curl_close($ch);
	// Check for errors
	if ($response === FALSE) {
		die(curl_error($ch));
	}
	// Decode the response
	$responseData = json_decode($response, TRUE);
	$access_token = $responseData["access_token"];
	return $access_token;
}
//Return information both poster and thumbnail
function wb_get_video_images( $video_id ){
	GLOBAL $bc_account_id, $access_token;

	//$access_token = wb_get_auth_token();

	$url = "https://cms.api.brightcove.com/v1/accounts/$bc_account_id/videos/$video_id/images";
	$header = array();
	$access = 'Authorization: Bearer ' . $access_token;
	$header[] = 'Content-Type: application/json';
	$header[] = $access;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response= curl_exec($ch);
	curl_close($ch);
	return $response;
}
?>
