<?php
/**
 * filename: ajaxFunctions.php
 * description: this will have functions used for ajax calls
 * author: Jullie Quijano
 * date created: 2014-04-16
 * last update: 2017-12-14 [Mark] Replaced old BC api calls
 */

error_reporting(0);
$prewd = getcwd();
chdir(realpath(dirname(__FILE__)));

if (!defined(DB_NAME) && !defined(SHORTINIT)) {
    define('SHORTINIT', true);
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php' );
}

global $wb_ent_options, $postId, $debugMode, $numOfVidsLoad, $current_lang, $bc_account_id;

$debugMode = true;
$wb_ent_options = wb_get_option('wb_ent_options');
$bc_account_id = $wb_ent_options['brightcoveinfo']['publisherid'];
$numOfVidsLoad = ( intval($wb_ent_options['videolistinfo']['vidsperpagehome'] / 3) ) * 3;



//get the function
$fx = ( isset($_POST['fx']) && trim($_POST['fx']) != '' ) ? $_POST['fx'] : null;
$lang = ( isset($_POST['lang']) && trim($_POST['lang']) != '' ) ? $_POST['lang'] : null;

switch ($fx) {
    case 'getSubCats':
        if (isset($_POST['catId']) && trim($_POST['catId']) != '') {
            getSubCats($_POST['catId']);
        }
        break;
    case 'getCatVids':
        if (isset($_POST['catId']) && trim($_POST['catId']) != '') {
            getCatVids($_POST['catId'], $_POST['currentPostId'], $lang);
        }
        break;
    case 'getSearchVids':
        if (isset($_POST['searchWord']) && trim($_POST['searchWord']) != '') {
            $useInternal = false;
            getSearchVids($_POST['searchWord'], $lang, $_POST['currentPostId']);
        }
        break;
    case 'loadMoreVids':
        $useInternal = false;
        loadMoreVids($_POST);
        break;
}

function getSubCats($categoryId) {
    global $debugMode, $wpdb, $numOfVidsLoad;

    $response = array();

   try {
        $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

        $getSubCategories = $dbh->prepare("
            SELECT t.term_id, t.name, t.slug
            FROM wp_terms t, wp_term_taxonomy tt, wp_term_relationships tr, wp_posts p
            WHERE tt.term_id=t.term_id
            AND tt.taxonomy='category'
            AND tr.term_taxonomy_id=tt.term_taxonomy_id
            AND tr.object_id=p.ID
            AND p.post_status='publish'
            AND tt.parent=?
            GROUP BY t.term_id    
         ");

        $getSubCategories->bindParam(1, $categoryId);

        $getSubCategories->execute();
        $getSubCategoriesNumrows = $getSubCategories->rowCount();
        $getSubCategoriesResult = $getSubCategories->fetchAll();

        $getSubCategories = null;
        $dbh = null;
    } catch (PDOException $e) {
        echo "Error!: Could not connect to DB";
    }

    $allSubCats = array();

    foreach ($getSubCategoriesResult as $currentSubCat) {
        $allSubCats[$currentSubCat['term_id']]['term_id'] = $currentSubCat['term_id'];
        $allSubCats[$currentSubCat['term_id']]['name'] = $currentSubCat['name'];
        $allSubCats[$currentSubCat['term_id']]['slug'] = $currentSubCat['slug'];
    }

    if (count($allSubCats) > 0) {
        $response['debug'][] = "subcats found";
        $response['success'] = true;
        $response['subcats'] = $allSubCats;
    } else {
        $response['debug'][] = "no subcats found";
        $response['success'] = false;
    }

    if (!$debugMode) {
        unset($response['debug']);
    }

    echo json_encode($response);
    unset($response);
    exit;
}

function getCatVids($categoryId, $currentPostId, $current_lang) {
    global $wb_ent_options;

    if($current_lang != 'en_US' && $current_lang != ''){
    $cur_lang =  '-'.$current_lang;
    }else{
    $cur_lang = '';
    }
    if($categoryId == 'default'){
        $categoryId = $wb_ent_options['videocats']['library'.$cur_lang];
    }
    global $debugMode, $numOfVidsLoad, $wb_ent_options;

    $response = array();

    try {
        $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

        if (!$wb_ent_options['vidshortcode']['enabled']) {
            $getCatVideos = $dbh->prepare("SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content 
                FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media b
                WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
                AND tt.term_id=t.term_id
                AND tr.object_id=p.ID
                AND p.ID=b.post_id
                AND p.post_type = 'post'
                AND p.post_status='publish'
                AND t.term_id=?
                GROUP BY p.ID
                ORDER BY p.post_date DESC
                ");
            $getCatVideos->bindParam(1, $categoryId);
        } else {
            $getCatVideos = $dbh->prepare("SELECT p.ID,p.post_title, p.post_name, p.post_content 
                FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
                WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
                AND tt.term_id=t.term_id
                AND tr.object_id=p.ID        
                AND p.post_type = 'post'
                AND p.post_status='publish'
                AND t.term_id=?        
                GROUP BY p.ID
                ORDER BY p.post_date DESC
                ");
            $getCatVideos->bindParam(1, $categoryId);
        }

        $getCatVideos->execute();
        $getCatVideosNumrows = $getCatVideos->rowCount();
        $getCatVideosResult = $getCatVideos->fetchAll();

        $response['debug'][] = '$getCatVideosResult is ' . print_r($getCatVideosResult, true);
        $response['debug'][] = '$getCatVideosResult ID ' . $categoryId.$cur_lang.$current_lang;

        $getCatVideos = null;
        $dbh = null;
    } catch (PDOException $e) {
        echo "Error!: Could not connect to DB";
    }

    $allCatVideos = array();

    $videoCounter = 0;

    foreach ($getCatVideosResult as $currentCatVideo) {
        $response['debug'][] = ' inside foreach $getCatVideosResult where $currentCatVideo is ' . print_r($currentCatVideo, true);

        $allCatVideos['_'.$currentCatVideo['ID']]['postId'] = $currentCatVideo['ID'];
        $allCatVideos['_'.$currentCatVideo['ID']]['mediaId'] = $currentCatVideo['media_id'];
        $allCatVideos['_'.$currentCatVideo['ID']]['smlThumb'] = $currentCatVideo['media_thumb'];
        $allCatVideos['_'.$currentCatVideo['ID']]['title'] = wb_format_string($currentCatVideo['post_title'], false, true, $wb_ent_options['videolistinfo']['titlelimit'], '... ');
        $allCatVideos['_'.$currentCatVideo['ID']]['postName'] = $currentCatVideo['post_name'];
        
        $allCatVideos['_'.$currentCatVideo['ID']]['desc'] = wb_format_string($currentCatVideo['post_content'], false, true, $wb_ent_options['videolistinfo']['desclimit'], '... <a href="/?p=' . $currentCatVideo['ID'] . '"><span class="link">[more]</span></a>');
        
        $wb_shortcode_video_id = "";
        if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)mode(.*)' . $wb_ent_options['vidshortcode']['name'] . '(.*)\/\]/', $currentCatVideo['post_content'], $match) >= 1) {
            $allCatVideos['_'.$currentCatVideo['ID']]['mediaId'] = $match[3][0];
            $wb_shortcode_video_id = $match[3][0];
          } else if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)\/\]/', $currentCatVideo['post_content'], $match) >= 1) {
            $allCatVideos['_'.$currentCatVideo['ID']]['mediaId'] = $match[3][0];
            $wb_shortcode_video_id = $match[3][0];
        } else {
            $allCatVideos['_'.$currentCatVideo['ID']]['mediaId'] = $currentCatVideo['media_id'];
            $wb_shortcode_video_id = $currentCatVideo['media_id'];
        }


        $getBrightcoveInfo = false;

        $allCatVideos['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';
        $allCatVideos['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';

        $headers = get_headers($wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg');
        if (preg_match('/^HTTP\/\d\.\d\s+(200|301|302)/', $headers[0])) {
        	if ( $wb_ent_options['cloudfrontinfo']['domainname'] != ""){
        		$allCatVideos['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['cloudfrontinfo']['domainname'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';
        	}else{
        		$allCatVideos['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';
        	}
        	$response['debug'][] = $currentCatVideo['ID'] . ' has midthumb';
        } else {
            $allCatVideos['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['defaultmidThumb'];
            $response['debug'][] = $currentCatVideo['ID'] . ' no midthumb';
            $getBrightcoveInfo = true;
        }


        //check if the midsize and the large thumbs are available
        $headers = get_headers($wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg');
        if (preg_match('/^HTTP\/\d\.\d\s+(200|301|302)/', $headers[0])) {
            $response['debug'][] = $currentCatVideo['ID'] . ' has largethumb';
            if ( $wb_ent_options['cloudfrontinfo']['domainname'] != ""){
            	$allCatVideos['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['cloudfrontinfo']['domainname'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';
            }else{
            	$allCatVideos['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';
            }
        } else {
            $response['debug'][] = $currentCatVideo['ID'] . ' no largethumb';
            $allCatVideos['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['defaultlrgthumb'];
            $getBrightcoveInfo = true;
        }        

        if ( $wb_ent_options['updatethumbs'] ) {
        	
            if ($allCatVideos && (trim($allCatVideos['_'.$currentCatVideo['ID']]['smlThumb']) == '' || trim($allCatVideos['_'.$currentCatVideo['ID']]['mediaId']) == '' || $getBrightcoveInfo )) {
               
            	if (trim($wb_shortcode_video_id) == '' && $wb_ent_options['vidshortcode']['enabled']) {
                    $allCatVideos['_'.$currentCatVideo['ID']]['mediaId'] = '';
                    $allCatVideos['_'.$currentCatVideo['ID']]['smlThumb'] = $wb_ent_options['defaultsmlthumb'];
                } else {
                	$allCatVideos['_'.$currentCatVideo['ID']]['smlThumb'] = wb_get_video_thumbnail($allCatVideos['_'.$currentCatVideo['ID']]['mediaId']);
                }

                try {
                    $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

                    $updateMedia = $dbh->prepare("UPDATE wb_media
                                    SET media_thumb=?
                                    WHERE media_id=?");
                    $updateMedia->bindParam(1, $allCatVideos['_'.$currentCatVideo['ID']]['smlThumb']);
                    $updateMedia->bindParam(2, $allCatVideos['_'.$currentCatVideo['ID']]['mediaId']);

                    $updateMedia->execute();
                    $updateMediaNumrows = $updateMedia->rowCount();
                    $updateMediaResult = $updateMedia->fetchAll();

                    $response['debug'][] = '$updateMediaResult is ' . print_r($updateMediaResult, true);

                    $updateMedia = null;
                    $dbh = null;
                } catch (PDOException $e) {
                    echo "Error!: Could not connect to DB";
                }

                $videoStill = wb_get_video_still($allCatVideos['_'.$currentCatVideo['ID']]['mediaId']);
                $allCatVideos['_'.$currentCatVideo['ID']]['videoStill'] = $videoStill;

                if (trim($videoStill) == '' || is_null($videoStill) || trim($allCatVideos['_'.$currentCatVideo['ID']]['mediaId']) == '' || is_null($allCatVideos['_'.$currentCatVideo['ID']]['mediaId'])) {
                    $videoStill = $wb_ent_options['defaultstill'];
                }


                if ($getBrightcoveInfo) {
                    $prewd = getcwd();
                    chdir(realpath(dirname(__FILE__)));

                    if (!class_exists('SimpleImage'))
                        require_once('SimpleImage.php');

                    //include the S3 class  
                    if (!class_exists('S3'))
                        require_once('S3.php');

                    $s3 = new S3($wb_ent_options['amazons3info']['accesskey'], $wb_ent_options['amazons3info']['secretkey']);

                    copy($videoStill, '../../../../images/stills/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_still.jpg');
                    $image = new SimpleImage();
                    $image->load('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/stills/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_still.jpg');

                    $image->resize(375, 211);
                    $image->save('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg', IMAGETYPE_JPEG, 100);

                   $largethumbFileName = 'images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';
                    $filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';

                    if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $largethumbFileName, S3::ACL_PUBLIC_READ)) {
                        $response['debug'][] = "debug: We successfully uploaded your file $largethumbFileName .<br />";
                    } else {
                        $response['debug'][] = "debug: Something went wrong while uploading your file $largethumbFileName... sorry.<br />";
                    }

                    $allCatVideos['_'.$currentCatVideo['ID']]['largethumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';

                    $image->resize(240, 135);
                    $image->save('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg', IMAGETYPE_JPEG, 100);

                    $midthumbFileName = 'images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';
                    $filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';

                    if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $midthumbFileName, S3::ACL_PUBLIC_READ)) {
                        $response['debug'][] = "debug: We successfully uploaded your file $midthumbFileName.<br />";
                    } else {
                        $response['debug'][] = "debug: Something went wrong while uploading your file $midthumbFileName... sorry.<br />";
                    }

                    $allCatVideos['_'.$currentCatVideo['ID']]['midthumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allCatVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';

                    chdir($prewd);
                }
            }
        }


        $videoCounter++;
        if ($videoCounter >= $numOfVidsLoad) {
            break;
        }
    }


    if ($getCatVideosNumrows > 0) {
        $response['debug'][] = "videos found";
        $response['success'] = true;
        $response['catVideos'] = $allCatVideos;
        $lastPageTotal = $getCatVideosNumrows % $numOfVidsLoad;
        $response['lastPageTotal'] = $lastPageTotal;

        $totalFullPages = intval($getCatVideosNumrows / $numOfVidsLoad);
        $response['totalFullPages'] = $totalFullPages;


        if ($vidPage) {
            $totalVids = (($totalFullPages - $vidPage) * $numOfVidsLoad) + $lastPageTotal;
        } else {
            $totalVids = ($totalFullPages * $numOfVidsLoad) + $lastPageTotal;
        }


        $response['totalVids'] = $totalVids;
    } else {
        $response['debug'][] = "no videos found";
        $response['success'] = false;
    }


    if (!$debugMode) {
        unset($response['debug']);
    }

    echo json_encode($response);
    unset($response);
    exit;
}

function loadMoreVids($postVars) {
    global $debugMode, $numOfVidsLoad, $wb_ent_options;
	$wb_ent_options = wb_get_option('wb_ent_options');
	$numOfVidsLoad = ( intval($wb_ent_options['videolistinfo']['vidsperpagehome'] / 3) ) * 3;    

    $response = array();
    
    $response['debug'][] = '$wb_ent_options is ' . print_r($wb_ent_options, true);
    $response['debug'][] = '$wb_ent_options[videolistinfo][vidsperpagehome] is ' . $wb_ent_options['videolistinfo']['vidsperpagehome'];

    $limit = intval($numOfVidsLoad)+1;

    if (isset($_POST['vidPage'])) {
        $vidPage = $_POST['vidPage'];
        $vidPage ++;
    } else {

       $vidPage = 1;
    }


    if ($vidPage == 1) {
        $start = 0;
        $vidPage = 1;
    } else {
        $start = (($vidPage - 1) * $limit);
    }

    $response['debug'][] = '$start is ' . $start;
    $response['debug'][] = '$limit is ' . $limit;
    
    //create exception list
    $exceptString = '';
    $exceptArray = array();

    $response['debug'][] = 'wb_ent_displayed_posts is ' . print_r($postVars['wb_ent_displayed_posts'], true);
    
    if( count($postVars['wb_ent_displayed_posts']) > 0 ){
        foreach( $postVars['wb_ent_displayed_posts'] as $currentDisplayedVideos ){
            $exceptArray[] = 'p.ID <> '.$currentDisplayedVideos;
        }
        array_unique($exceptArray);
        $exceptString = " AND ( ".implode(' AND ', $exceptArray)." )";
    }
    
	
    
    $response['debug'][] = '$exceptString is ' . $exceptString;    

    if (isset($postVars['categoryId']) && is_numeric($postVars['categoryId'])) {
    	    	
        $categoryId = $postVars['categoryId'];
		
        $categoryId =  $wb_ent_options['videocats']['library'] . ", " . $wb_ent_options['videocats']['podcast'];
        //$categoryId =7;
        try {
            $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

            if (!$wb_ent_options['vidshortcode']['enabled']) {
                $getSearchVids = $dbh->prepare("SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content 
             FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media b
             WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
             AND tt.term_id=t.term_id
             AND tr.object_id=p.ID
             AND p.ID=b.post_id
             AND p.post_type IN ('post', 'wb_audio') 
             AND p.post_status='publish'
             AND ( t.term_id=? OR t.term_id=".$wb_ent_options['videocats']['podcast'].")
             $exceptString
             GROUP BY p.ID
             ORDER BY p.post_date DESC
             LIMIT $limit");        
                $response['debug'][] = "SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content 
             FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media b
             WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
             AND tt.term_id=t.term_id
             AND tr.object_id=p.ID
             AND p.ID=b.post_id
             AND p.post_type IN ('post', 'wb_audio') 
             AND p.post_status='publish'
             AND ( t.term_id=? OR t.term_id=".$wb_ent_options['videocats']['podcast'].")
             $exceptString
             GROUP BY p.ID
             ORDER BY p.post_date DESC
             LIMIT $limit";
            }
            else{
                $getSearchVids = $dbh->prepare("SELECT p.ID, p.post_title, p.post_name, p.post_content 
             FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
             WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
             AND tt.term_id=t.term_id
             AND tr.object_id=p.ID             
             AND p.post_status='publish'
             AND post_type IN ('post', 'wb_audio') 
             AND ( t.term_id=? OR t.term_id=".$wb_ent_options['videocats']['podcast'].")
             $exceptString
             GROUP BY p.ID
             ORDER BY p.post_date DESC
             LIMIT $limit");       
                $response['debug'][] = "SELECT p.ID, p.post_title, p.post_name, p.post_content 
             FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
             WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
             AND tt.term_id=t.term_id
             AND tr.object_id=p.ID             
             AND p.post_status='publish'
             AND post_type IN ('post', 'wb_audio') 
             AND ( t.term_id=? OR t.term_id=".$wb_ent_options['videocats']['podcast'].")
             $exceptString
             GROUP BY p.ID
             ORDER BY p.post_date DESC
             LIMIT $limit";
            }


            $getSearchVids->bindParam(1, $categoryId, PDO::PARAM_INT);
            
            $getSearchVids->execute();
            $getSearchVidsResult = $getSearchVids->fetchAll();
            $getSearchVidsNumrows = $getSearchVids->rowCount();


            $lastPageTotal = $getSearchVidsNumrows % $numOfVidsLoad;
            $totalFullPages = intval($getSearchVidsNumrows / $numOfVidsLoad);
            //$totalVids = (($totalFullPages - $vidPage) * $numOfVidsLoad) + $lastPageTotal;
            $totalVids = $getSearchVidsNumrows;

            //print $dbh->errorCode();
            
            $response['debug'][] = '$categoryId is ' . $categoryId;
            $response['debug'][] = '$getSearchVidsNumrows is ' . $getSearchVidsNumrows;

            $getSearchVids = null;
            $dbh = null;
        } catch (PDOException $e) {
            echo "Error!: Could not connect to DB";
        }

        $allSearchVids = array();

        $videoCounter = 0;

        foreach ($getSearchVidsResult as $currentCatVideo) {

            $allSearchVids['_'.$currentCatVideo['ID']]['postId'] = $currentCatVideo['ID'];
            $allSearchVids['_'.$currentCatVideo['ID']]['mediaId'] = $currentCatVideo['media_id'];
            $allSearchVids['_'.$currentCatVideo['ID']]['smlThumb'] = $currentCatVideo['media_thumb'];
            $allSearchVids['_'.$currentCatVideo['ID']]['title'] = wb_format_string($currentCatVideo['post_title'], false, true, $wb_ent_options['videolistinfo']['titlelimit'], '... ');
            $allSearchVids['_'.$currentCatVideo['ID']]['postName'] = $currentCatVideo['post_name'];
            $allSearchVids['_'.$currentCatVideo['ID']]['desc'] = wb_format_string(strip_tags($currentCatVideo['post_content'],"<img>"), false, true, $wb_ent_options['videolistinfo']['desclimit'], '... <a href="/?p=' . $currentCatVideo['ID'] . '"><span class="link">[more]</span></a>');

			$wb_shortcode_video_id = "";
			
            if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)mode(.*)' . $wb_ent_options['vidshortcode']['name'] . '(.*)\/\]/', $currentCatVideo['post_content'], $match) >= 1) {
                $allSearchVids['_'.$currentCatVideo['ID']]['mediaId'] = $match[3][0];
                $wb_shortcode_video_id = $match[3][0];
            } else if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)\/\]/', $currentCatVideo['post_content'], $match) >= 1) {
                $allSearchVids['_'.$currentCatVideo['ID']]['mediaId'] = $match[3][0];
                $wb_shortcode_video_id = $match[3][0];
            } else {
                $allSearchVids['_'.$currentCatVideo['ID']]['mediaId'] = $currentCatVideo['media_id'];
                $wb_shortcode_video_id = $currentCatVideo['media_id'];
            }

            $getBrightcoveInfo = false;

            $allSearchVids['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_midThumb.jpg';
            $allSearchVids['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_largeThumb.jpg';
            
           //check if the midsize and the large thumbs are available
            $headers = get_headers($wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_midThumb.jpg');
            if (preg_match('/^HTTP\/\d\.\d\s+(200|301|302)/', $headers[0])) {
                $response['debug'][] = $currentCatVideo['ID'] . ' has midthumb';
                if ( $wb_ent_options['cloudfrontinfo']['domainname'] != ""){
                	$allSearchVids['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['cloudfrontinfo']['domainname'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_midThumb.jpg';
                }else{
                	$allSearchVids['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_midThumb.jpg';
                }
          	} else {
                $response['debug'][] = $currentCatVideo['ID'] . ' no midthumb';
                $allSearchVids['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['defaultmidThumb'];
                $getBrightcoveInfo = true;
            }


            //check if the midsize and the large thumbs are available
            $headers = get_headers($wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_largeThumb.jpg');
            if (preg_match('/^HTTP\/\d\.\d\s+(200|301|302)/', $headers[0])) {
                $response['debug'][] = $currentCatVideo['ID'] . ' has largethumb';
                if ( $wb_ent_options['cloudfrontinfo']['domainname'] != ""){
                	$allSearchVids['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['cloudfrontinfo']['domainname'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_largeThumb.jpg';
                }else{
                	$allSearchVids['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_largeThumb.jpg';
                }
      	  	} else {
                $response['debug'][] = $currentCatVideo['ID'] . ' no largethumb';
                $allSearchVids['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['defaultlrgthumb'];
                $getBrightcoveInfo = true;
            }            
            
            if ($wb_ent_options['updatethumbs']) {
                if ($allCatVideos && (
                						trim($allCatVideos['_'.$currentCatVideo['ID']]['smlThumb']) == '' || 
                						trim($allCatVideos['_'.$currentCatVideo['ID']]['mediaId']) == '' || 
                						$getBrightcoveInfo 
                					)
                	){
                     
                    if (trim($wb_shortcode_video_id) == '' && $wb_ent_options['vidshortcode']['enabled']) {
                        $allSearchVids['_'.$currentCatVideo['ID']]['mediaId'] = '';
                        $allSearchVids['_'.$currentCatVideo['ID']]['smlThumb'] = $wb_ent_options['defaultsmlthumb'];
                    } else {
                     	$allSearchVids['_'.$currentCatVideo['ID']]['smlThumb'] = wb_get_video_thumbnail($allSearchVids['_'.$currentCatVideo['ID']]['mediaId']);
                    }


                   try {
                        $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

                        $updateMedia = $dbh->prepare("UPDATE wb_media
                                    SET media_thumb=?
                                    WHERE media_id=?");
                        $updateMedia->bindParam(1, $allSearchVids['_'.$currentCatVideo['ID']]['smlThumb']);
                        $updateMedia->bindParam(2, $allSearchVids['_'.$currentCatVideo['ID']]['mediaId']);

                        $updateMedia->execute();
                        $updateMediaNumrows = $updateMedia->rowCount();
                        $updateMediaResult = $updateMedia->fetchAll();

                        $response['debug'][] = '$updateMediaResult is ' . print_r($updateMediaResult, true);

                        $updateMedia = null;
                        $dbh = null;
                    } catch (PDOException $e) {
                        echo "Error!: Could not connect to DB";
                    }

                    $videoStill = wb_get_video_still($allSearchVids['_'.$currentCatVideo['ID']]['mediaId']);
                    $allSearchVids['_'.$currentCatVideo['ID']]['videoStill'] = $videoStill;


                    if ($getBrightcoveInfo) {
                        $prewd = getcwd();
                        chdir(realpath(dirname(__FILE__)));

                        if (!class_exists('SimpleImage'))
                            require_once('SimpleImage.php');

                        //include the S3 class  
                        if (!class_exists('S3'))
                            require_once('S3.php');

                        $s3 = new S3($wb_ent_options['amazons3info']['accesskey'], $wb_ent_options['amazons3info']['secretkey']);

                        copy($videoStill, '../images/stills/' . $currentCatVideo['ID'] . '_still.jpg');
                        $image = new SimpleImage();
                        $image->load('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/stills/' . $currentCatVideo['ID'] . '_still.jpg');

                        $image->resize(375, 211);
                        $image->save('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_largeThumb.jpg', IMAGETYPE_JPEG, 100);

                        $largethumbFileName = 'images/thumbs/' . $currentCatVideo['ID'] . '_largeThumb.jpg';
                        $filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_largeThumb.jpg';

                        if ($s3->putObjectFile($filePath, AMAZON_S3_BUCKET, $largethumbFileName, S3::ACL_PUBLIC_READ)) {
                            $response['debug'][] = "debug: We successfully uploaded your file.<br />";
                        } else {
                            $response['debug'][] = "debug: Something went wrong while uploading your file... sorry.<br />";
                        }

                        $allSearchVids['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_largeThumb.jpg';


                        $image->resize(240, 135);
                        $image->save('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_midThumb.jpg', IMAGETYPE_JPEG, 100);

                        $midthumbFileName = 'images/thumbs/' . $currentCatVideo['ID'] . '_midThumb.jpg';
                        $filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_midThumb.jpg';

                        if ($s3->putObjectFile($filePath, AMAZON_S3_BUCKET, $midthumbFileName, S3::ACL_PUBLIC_READ)) {
                            $response['debug'][] = "debug: We successfully uploaded your file.<br />";
                        } else {
                            $response['debug'][] = "debug: Something went wrong while uploading your file... sorry.<br />";
                        }

                        $allSearchVids['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $currentCatVideo['ID'] . '_midThumb.jpg';
                        //$allSearchVids['_'.$currentCatVideo['ID']]['midThumb'] = $videoStill;
                        chdir($prewd);
                    }
                }
            }


            $videoCounter++;
            if ($videoCounter >= $wb_ent_options['videolistinfo']['vidsperpage']) {
                break;
            }
        }
    } 
    else if (isset($postVars['searchWord']) && trim($postVars['searchWord']) != '') {
        $response['debug'][] = 'inside search';
		
        $categoryId = $wb_ent_options['videocats']['library'];     
        
        $response['debug'][] = '$categoryId is '.$categoryId;
        
        $searchWord = trim($postVars['searchWord']);
        $response['debug'][] = '$searchWord is '.$searchWord;
        
        $searchWord = "%" . $searchWord . "%";
        $response['debug'][] = '$searchWord is '.$searchWord;
        
        $response['debug'][] = '$start is '.$start;
           
        try {
            $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

            if (!$wb_ent_options['vidshortcode']['enabled']) {
                $getSearchVids = $dbh->prepare("SELECT p.ID, p.post_date, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content 
                FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media b
                WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
                AND tt.term_id=t.term_id
                AND tr.object_id=p.ID
                AND p.ID=b.post_id
                AND p.post_type = 'post'
                AND p.post_status='publish'
                AND t.term_id=?
                AND(
                    p.post_title LIKE ? OR                    
                    p.post_content LIKE ? OR
                    p.post_name LIKE ? 
                )
                $exceptString
                GROUP BY p.ID
                ORDER BY p.post_date DESC
                LIMIT $limit");
                
                $response['debug'][] = "SELECT p.ID, p.post_date, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content 
                FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media b
                WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
                AND tt.term_id=t.term_id
                AND tr.object_id=p.ID
                AND p.ID=b.post_id
                AND p.post_type = 'post'
                AND p.post_status='publish'
                AND t.term_id=?
                AND(
                    p.post_title LIKE ? OR                    
                    p.post_content LIKE ? OR
                    p.post_name LIKE ? 
                )
                $exceptString
                GROUP BY p.ID
                ORDER BY p.post_date DESC
                LIMIT $limit";
                
                $getSearchVids->bindParam(1, $categoryId);
                $getSearchVids->bindParam(2, $searchWord);
                $getSearchVids->bindParam(3, $searchWord);
                $getSearchVids->bindParam(4, $searchWord);
                
            } else {
                $getSearchVids = $dbh->prepare("SELECT p.ID, p.post_date, p.post_title, p.post_name, p.post_content 
                FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
                WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
                AND tt.term_id=t.term_id
                AND tr.object_id=p.ID        
                AND p.post_status='publish'
                AND p.post_type = 'post'
                AND t.term_id=?       
                AND(
                    post_title LIKE ? OR                    
                    post_content LIKE ? OR
                    post_name LIKE ?    
                )                
                $exceptString
                GROUP BY p.ID
                ORDER BY p.post_date DESC
                ");
                
                $response['debug'][] = "SELECT p.ID, p.post_date, p.post_title, p.post_name, p.post_content 
                FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
                WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
                AND tt.term_id=t.term_id
                AND tr.object_id=p.ID        
                AND p.post_status='publish'
                AND p.post_type = 'post'
                AND t.term_id=?       
                AND(
                    post_title LIKE ? OR                    
                    post_content LIKE ? OR
                    post_name LIKE ?    
                )                
                $exceptString
                GROUP BY p.ID
                ORDER BY p.post_date DESC
                ";
                
                $getSearchVids->bindParam(1, $categoryId);
                $getSearchVids->bindParam(2, $searchWord);
                $getSearchVids->bindParam(3, $searchWord);
                $getSearchVids->bindParam(4, $searchWord);
                
            }
            

            $getSearchVids->execute();
            $getSearchVidsNumrows = $getSearchVids->rowCount();
            $getSearchVidsResult = $getSearchVids->fetchAll();

            $response['debug'][] = '$getSearchVidsResult is ' . print_r($getSearchVidsResult, true);
            $response['debug'][] = '$getSearchVidsNumrows is ' . $getSearchVidsNumrows;

            $getSearchVids = null;
            $dbh = null;
        } catch (PDOException $e) {
            echo "Error!: Could not connect to DB";
        }

        $allSearchVids = array();

        $videoCounter = 0;

        foreach ($getSearchVidsResult as $currentCatVideo) {
            $response['debug'][] = ' inside foreach $getSearchVideosResult where $currentCatVideo is ' . print_r($currentCatVideo, true);

            $allSearchVids['_'.$currentCatVideo['ID']]['postId'] = $currentCatVideo['ID'];
            $allSearchVids['_'.$currentCatVideo['ID']]['mediaId'] = $currentCatVideo['media_id'];
            $allSearchVids['_'.$currentCatVideo['ID']]['smlThumb'] = $currentCatVideo['media_thumb'];
            $allSearchVids['_'.$currentCatVideo['ID']]['title'] = wb_format_string($currentCatVideo['post_title'], false, true, $wb_ent_options['videolistinfo']['titlelimit'], '... ');
            $allSearchVids['_'.$currentCatVideo['ID']]['postName'] = $currentCatVideo['post_name'];
            $allSearchVids['_'.$currentCatVideo['ID']]['desc'] = wb_format_string($currentCatVideo['post_content'], false, true, $wb_ent_options['videolistinfo']['desclimit'], '... <a href="/?p=' . $currentCatVideo['ID'] . '"><span class="link">[more]</span></a>');

            $wb_shortcode_video_id = "";
            if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)mode(.*)' . $wb_ent_options['vidshortcode']['name'] . '(.*)\/\]/', $currentCatVideo['post_content'], $match) >= 1) {
                $allSearchVids['_'.$currentCatVideo['ID']]['mediaId'] = $match[3][0];
                $wb_shortcode_video_id = $match[3][0];
            } else if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)\/\]/', $currentCatVideo['post_content'], $match) >= 1) {
                $allSearchVids['_'.$currentCatVideo['ID']]['mediaId'] = $match[3][0];
                $wb_shortcode_video_id = $match[3][0];
            } else {
                $allSearchVids['_'.$currentCatVideo['ID']]['mediaId'] = $currentCatVideo['media_id'];
                $wb_shortcode_video_id  = $currentCatVideo['media_id'];
            }


            $getBrightcoveInfo = false;

           //$allSearchVids['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';
            //$allSearchVids['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';

            
            $headers = get_headers($wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg');
            if (preg_match('/^HTTP\/\d\.\d\s+(200|301|302)/', $headers[0])) {
            	if ( $wb_ent_options['cloudfrontinfo']['domainname'] != "") {
            		$allSearchVids['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['cloudfrontinfo']['domainname'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';
            	}else{
            		$allSearchVids['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';
            	}
            	$response['debug'][] = $currentCatVideo['ID'] . ' has midthumb';
            } else {
                $response['debug'][] = $currentCatVideo['ID'] . ' no midthumb';
                $allSearchVids['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['defaultmidThumb'];
                $getBrightcoveInfo = true;
            }


            //check if the midsize and the large thumbs are available
            $headers = get_headers($wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg');
            if (preg_match('/^HTTP\/\d\.\d\s+(200|301|302)/', $headers[0])) {
                $response['debug'][] = $currentCatVideo['ID'] . ' has largethumb';
                if ( $wb_ent_options['cloudfrontinfo']['domainname'] != ""){
                	$allSearchVids['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['cloudfrontinfo']['domainname'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';
                }else{
                	$allSearchVids['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';
                }
           	} else {
                $response['debug'][] = $currentCatVideo['ID'] . ' no largethumb';
                $allSearchVids['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['defaultlrgthumb'];
                $getBrightcoveInfo = true;
            }            

            if ($wb_ent_options['updatethumbs']) {



                if ($allSearchVids && (trim($allSearchVids['_'.$currentCatVideo['ID']]['smlThumb']) == '' || trim($allSearchVids['_'.$currentCatVideo['ID']]['mediaId']) == '' || $getBrightcoveInfo )) {
                  	
                	if (trim($wb_shortcode_video_id) == '' && $wb_ent_options['vidshortcode']['enabled']) {
                        $allSearchVids['_'.$currentCatVideo['ID']]['mediaId'] = '';
                        $allSearchVids['_'.$currentCatVideo['ID']]['smlThumb'] = $wb_ent_options['defaultsmlthumb'];
                    } else {
                        $allSearchVids['_'.$currentCatVideo['ID']]['smlThumb'] = wb_get_video_thumbnail($allSearchVids['_'.$currentCatVideo['ID']]['mediaId']);
                    }

                    try {
                        $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

                        $updateMedia = $dbh->prepare("UPDATE wb_media
                                    SET media_thumb=?
                                    WHERE media_id=?");
                        $updateMedia->bindParam(1, $allSearchVids['_'.$currentCatVideo['ID']]['smlThumb']);
                        $updateMedia->bindParam(2, $allSearchVids['_'.$currentCatVideo['ID']]['mediaId']);

                        $updateMedia->execute();
                        $updateMediaNumrows = $updateMedia->rowCount();
                        $updateMediaResult = $updateMedia->fetchAll();

                        $response['debug'][] = '$updateMediaResult is ' . print_r($updateMediaResult, true);

                        $updateMedia = null;
                        $dbh = null;
                    } catch (PDOException $e) {
                        echo "Error!: Could not connect to DB";
                    }


                    $videoStill = wb_get_video_still($allSearchVids['_'.$currentCatVideo['ID']]['mediaId']);
                    $allSearchVids['_'.$currentCatVideo['ID']]['videoStill'] = $videoStill;
                    if (trim($videoStill) == '' || is_null($videoStill) || trim($allSearchVids['_'.$currentCatVideo['ID']]['mediaId']) == '' || is_null($allSearchVids['_'.$currentCatVideo['ID']]['mediaId'])) {
                        $videoStill = $wb_ent_options['defaultstill'];
                    }


                    if ($getBrightcoveInfo) {
                        $prewd = getcwd();
                        chdir(realpath(dirname(__FILE__)));

                        if (!class_exists('SimpleImage'))
                            require_once('SimpleImage.php');

                        //include the S3 class  
                        if (!class_exists('S3'))
                            require_once('S3.php');

                        $s3 = new S3($wb_ent_options['amazons3info']['accesskey'], $wb_ent_options['amazons3info']['secretkey']);

                        copy($videoStill, '../../../../images/stills/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_still.jpg');
                        $image = new SimpleImage();
                        $image->load('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/stills/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_still.jpg');

                        $image->resize(375, 211);
                        $image->save('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg', IMAGETYPE_JPEG, 100);



                        $largethumbFileName = 'images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';
                        $filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';

                        if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $largethumbFileName, S3::ACL_PUBLIC_READ)) {
                            $response['debug'][] = "debug: We successfully uploaded your file $largethumbFileName .<br />";
                        } else {
                            $response['debug'][] = "debug: Something went wrong while uploading your file $largethumbFileName... sorry.<br />";
                        }

                        $allSearchVids['_'.$currentCatVideo['ID']]['largethumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';


                        $image->resize(240, 135);
                        $image->save('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg', IMAGETYPE_JPEG, 100);

                        $midthumbFileName = 'images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';
                        $filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';

                        if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $midthumbFileName, S3::ACL_PUBLIC_READ)) {
                            $response['debug'][] = "debug: We successfully uploaded your file $midthumbFileName.<br />";
                        } else {
                            $response['debug'][] = "debug: Something went wrong while uploading your file $midthumbFileName... sorry.<br />";
                        }

                        $allSearchVids['_'.$currentCatVideo['ID']]['midthumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';

                        chdir($prewd);
                    }
                }
            }

            $videoCounter++;
            if ($videoCounter >= $wb_ent_options['videolistinfo']['vidsperpage']) {
                break;
            }
        }
    }


    if ($getSearchVidsNumrows > 0) {
        $response['debug'][] = "videos found";
        $response['success'] = true;
        $response['searchVideos'] = $allSearchVids;
        $response['totalVids'] = $totalVids;
    } else {
        $response['debug'][] = "no videos found";
        $response['success'] = false;
    }


    if (!$debugMode) {
        unset($response['debug']);
    }

    echo json_encode($response);
    unset($response);
    exit;
}

function getSearchVids($searchWord, $current_lang) {
    global $debugMode, $numOfVidsLoad, $wb_ent_options;
    
     if($current_lang != 'en_US' && $current_lang!= 'en_CA' && $current_lang != ''){
    $cur_lang =  '-'.$current_lang;
    }else{
    $cur_lang = '';
    }
    $categoryId = $wb_ent_options['videocats']['library'.$cur_lang];
    
    $response = array();
    $searchWord = "%" . $searchWord . "%";
    try {
        //$dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD);
$dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
        if (!$wb_ent_options['vidshortcode']['enabled']) {
            $getSearchVideos = $dbh->prepare("SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content 
                FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media b
                WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
                AND tt.term_id=t.term_id
                AND tr.object_id=p.ID
                AND p.ID=b.post_id
                AND p.post_type = 'post'
                AND p.post_status='publish'
                AND t.term_id=?
                AND(
                    post_title LIKE ? OR                    
                    post_content LIKE ? OR
                    post_name LIKE ? 
                )
                GROUP BY p.ID
                ORDER BY p.post_date DESC
                ");
            $getSearchVideos->bindParam(1, $categoryId);
            $getSearchVideos->bindParam(2, $searchWord);
            $getSearchVideos->bindParam(3, $searchWord);
            $getSearchVideos->bindParam(4, $searchWord);
        } else {
            $getSearchVideos = $dbh->prepare("SELECT p.ID,p.post_title, p.post_name, p.post_content 
                FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
                WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
                AND tt.term_id=t.term_id
                AND tr.object_id=p.ID       
                AND p.post_type = 'post'
                AND p.post_status='publish'
                AND t.term_id=?       
                AND(
                    post_title LIKE ? OR                    
                    post_content LIKE ? OR
                    post_name LIKE ?    
                )                
                GROUP BY p.ID
                ORDER BY p.post_date DESC
                ");
            $getSearchVideos->bindParam(1, $categoryId);
            $getSearchVideos->bindParam(2, $searchWord);
            $getSearchVideos->bindParam(3, $searchWord);
            $getSearchVideos->bindParam(4, $searchWord);
        }

        $getSearchVideos->execute();
        $getSearchVideosNumrows = $getSearchVideos->rowCount();
        $getSearchVideosResult = $getSearchVideos->fetchAll();

        $response['debug'][] = '$getSearchVideosResult is ' . print_r($getSearchVideosResult, true);

        $getSearchVideos = null;
        $dbh = null;
    } catch (PDOException $e) {
        echo "Error!: Could not connect to DB";
    }

    $allSearchVideos = array();

    $videoCounter = 0;

    foreach ($getSearchVideosResult as $currentCatVideo) {
        $response['debug'][] = ' inside foreach $getSearchVideosResult where $currentCatVideo is ' . print_r($currentCatVideo, true);

        $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] = $currentCatVideo['ID'];
        $allSearchVideos['_'.$currentCatVideo['ID']]['mediaId'] = $currentCatVideo['media_id'];
        $allSearchVideos['_'.$currentCatVideo['ID']]['smlThumb'] = $currentCatVideo['media_thumb'];
        $allSearchVideos['_'.$currentCatVideo['ID']]['title'] = wb_format_string($currentCatVideo['post_title'], false, true, $wb_ent_options['videolistinfo']['titlelimit'], '... ');
        $allSearchVideos['_'.$currentCatVideo['ID']]['postName'] = $currentCatVideo['post_name'];
        $allSearchVideos['_'.$currentCatVideo['ID']]['desc'] = wb_format_string($currentCatVideo['post_content'], false, true, $wb_ent_options['videolistinfo']['desclimit'], '... <a href="/?p=' . $currentCatVideo['ID'] . '"><span class="link">[more]</span></a>');

        $wb_shortcode_video_id = "";
        
        if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)mode(.*)' . $wb_ent_options['vidshortcode']['name'] . '(.*)\/\]/', $currentCatVideo['post_content'], $match) >= 1) {
            $allSearchVideos['_'.$currentCatVideo['ID']]['mediaId'] = $match[3][0];
            $wb_shortcode_video_id = $match[3][0];
        } else if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)\/\]/', $currentCatVideo['post_content'], $match) >= 1) {
            $allSearchVideos['_'.$currentCatVideo['ID']]['mediaId'] = $match[3][0];
            $wb_shortcode_video_id = $match[3][0];
        } else {
            $allSearchVideos['_'.$currentCatVideo['ID']]['mediaId'] = $currentCatVideo['media_id'];
            $wb_shortcode_video_id = $currentCatVideo['media_id'];
        }

        $getBrightcoveInfo = false;

        $allSearchVideos['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';
        $allSearchVideos['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';

        $headers = get_headers($wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg');
        if (preg_match('/^HTTP\/\d\.\d\s+(200|301|302)/', $headers[0])) {
        	if ( $wb_ent_options['cloudfrontinfo']['domainname'] != "") {
        		$allSearchVideos['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['cloudfrontinfo']['domainname']. '/images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';
        		//$allSearchVids['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['cloudfrontinfo']['domainname'] . '/images/thumbs/' . $allSearchVids['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';
        	}else{
        		$allSearchVideos['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';
        	}
            $response['debug'][] = $currentCatVideo['ID'] . ' has midthumb';
        } else {
            $response['debug'][] = $currentCatVideo['ID'] . ' no midthumb';
            $allSearchVideos['_'.$currentCatVideo['ID']]['midThumb'] = $wb_ent_options['defaultmidThumb'];
            $getBrightcoveInfo = true;
        }


        //check if the midsize and the large thumbs are available
        $headers = get_headers($wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg');
        if (preg_match('/^HTTP\/\d\.\d\s+(200|301|302)/', $headers[0])) {
            $response['debug'][] = $currentCatVideo['ID'] . ' has largethumb';
            $allSearchVideos['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';
        } else {
            $response['debug'][] = $currentCatVideo['ID'] . ' no largethumb';
            $allSearchVideos['_'.$currentCatVideo['ID']]['largeThumb'] = $wb_ent_options['defaultlrgthumb'];
            $getBrightcoveInfo = true;
        }        
        

        if ($wb_ent_options['updatethumbs']) {

             if ($allSearchVideos && (trim($allSearchVideos['_'.$currentCatVideo['ID']]['smlThumb']) == '' || trim($allSearchVideos['_'.$currentCatVideo['ID']]['mediaId']) == '' || $getBrightcoveInfo )) {
                 	
             	if (trim($wb_shortcode_video_id) == '' && $wb_ent_options['vidshortcode']['enabled']) {
                    $allSearchVideos['_'.$currentCatVideo['ID']]['mediaId'] = '';
                    $allSearchVideos['_'.$currentCatVideo['ID']]['smlThumb'] = $wb_ent_options['defaultsmlthumb'];
                } else {
                	$allSearchVideos['_'.$currentCatVideo['ID']]['smlThumb'] = wb_get_video_thumbnail($allSearchVideos['_'.$currentCatVideo['ID']]['mediaId']);
                }

                try {
                    $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

                    $updateMedia = $dbh->prepare("UPDATE wb_media
                                    SET media_thumb=?
                                    WHERE media_id=?");
                    $updateMedia->bindParam(1, $allSearchVideos['_'.$currentCatVideo['ID']]['smlThumb']);
                    $updateMedia->bindParam(2, $allSearchVideos['_'.$currentCatVideo['ID']]['mediaId']);

                    $updateMedia->execute();
                    $updateMediaNumrows = $updateMedia->rowCount();
                    $updateMediaResult = $updateMedia->fetchAll();

                    $response['debug'][] = '$updateMediaResult is ' . print_r($updateMediaResult, true);

                    $updateMedia = null;
                    $dbh = null;
                } catch (PDOException $e) {
                    echo "Error!: Could not connect to DB";
                }


                $videoStill = wb_get_video_still($allSearchVideos['_'.$currentCatVideo['ID']]['mediaId']);
                
                $allSearchVideos['_'.$currentCatVideo['ID']]['videoStill'] = $videoStill;

                if (trim($videoStill) == '' || is_null($videoStill) || trim($allSearchVideos['_'.$currentCatVideo['ID']]['mediaId']) == '' || is_null($allSearchVideos['_'.$currentCatVideo['ID']]['mediaId'])) {
                    $videoStill = $wb_ent_options['defaultstill'];
                }


                if ($getBrightcoveInfo) {
                    $prewd = getcwd();
                    chdir(realpath(dirname(__FILE__)));

                    if (!class_exists('SimpleImage'))
                        require_once('SimpleImage.php');

                    //include the S3 class  
                    if (!class_exists('S3'))
                        require_once('S3.php');

                    $s3 = new S3($wb_ent_options['amazons3info']['accesskey'], $wb_ent_options['amazons3info']['secretkey']);


                    copy($videoStill, '../../../../images/stills/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_still.jpg');
                    $image = new SimpleImage();
                    $image->load('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/stills/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_still.jpg');

                    $image->resize(375, 211);
                    $image->save('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg', IMAGETYPE_JPEG, 100);

                    $largethumbFileName = 'images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';
                    $filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';

                    if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $largethumbFileName, S3::ACL_PUBLIC_READ)) {
                        $response['debug'][] = "debug: We successfully uploaded your file $largethumbFileName .<br />";
                    } else {
                        $response['debug'][] = "debug: Something went wrong while uploading your file $largethumbFileName... sorry.<br />";
                    }

                    $allSearchVideos['_'.$currentCatVideo['ID']]['largethumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_largeThumb.jpg';


                    $image->resize(240, 135);
                    $image->save('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg', IMAGETYPE_JPEG, 100);

                    $midthumbFileName = 'images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';
                    $filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';

                    if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $midthumbFileName, S3::ACL_PUBLIC_READ)) {
                        $response['debug'][] = "debug: We successfully uploaded your file $midthumbFileName.<br />";
                    } else {
                        $response['debug'][] = "debug: Something went wrong while uploading your file $midthumbFileName... sorry.<br />";
                    }

                    $allSearchVideos['_'.$currentCatVideo['ID']]['midthumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $allSearchVideos['_'.$currentCatVideo['ID']]['postId'] . '_midThumb.jpg';

                    chdir($prewd);
                }
            }
        }


        $videoCounter++;
        if ($videoCounter >= $numOfVidsLoad) {
            break;
        }
    }


    if ($getSearchVideosNumrows > 0) {
        $response['debug'][] = "videos found";
        $response['success'] = true;
        $response['searchVideos'] = $allSearchVideos;
        $lastPageTotal = $getSearchVideosNumrows % $numOfVidsLoad;
        $response['lastPageTotal'] = $lastPageTotal;

        $totalFullPages = intval($getSearchVideosNumrows / $numOfVidsLoad);
        $response['totalFullPages'] = $totalFullPages;


        if ($vidPage) {
            $totalVids = (($totalFullPages - $vidPage) * $numOfVidsLoad) + $lastPageTotal;
        } else {
            $totalVids = ($totalFullPages * $numOfVidsLoad) + $lastPageTotal;
        }


        $response['totalVids'] = $totalVids;
    } else {
        $response['debug'][] = "no videos found";
        $response['success'] = false;
    }


    if (!$debugMode) {
        unset($response['debug']);
    }

    echo json_encode($response);
    unset($response);
    exit;
}

function wb_get_option($optionName = '') {
    if (trim($optionName) != '') {
        if (function_exists(get_option)) {
            return get_option($optionName);
        } else {
            try {
                $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

                $getWpOptions = $dbh->prepare("
                    SELECT option_value 
                    FROM `wp_options` 
                    WHERE option_name = ?           
                 ");

                $getWpOptions->bindParam(1, $optionName);

                $getWpOptions->execute();
                $getWpOptionsNumrows = $getWpOptions->rowCount();
                $getWpOptionsResult = $getWpOptions->fetch();

                echo '$getWpOptionsResult is ' . print_r($getWpOptionsResult, true);

                $getWpOptions = null;
                $dbh = null;
            } catch (PDOException $e) {
                echo "Error!: Could not connect to DB";
            }
        }
    }
}

function wb_format_string($string, $hasHTML = false, $listToCsv = false, $maxChar = -1, $end = '... ') {
    global $wb_ent_options; 
    
    $string = preg_replace("/\[".$wb_ent_options['vidshortcode']['tag']." (.*)\/\]/i", "", $string);
    
    //list to csv
    if ($listToCsv && preg_match("/<[^<]+>/", $string, $m) != 0) {
        $string = preg_replace("'</li[^>]*?>.*?<li>'si", ", ", $string);
        //$string = strip_tags($string, "<a><img>"); //nyssa
        $string = strip_tags($string, "<a>");
    }

    //strip html first
    if (!$hasHTML) {
        //$string = strip_tags($string,"<img>"); //nyssa
        $string = strip_tags($string);
    }

    //truncate
    if ($maxChar > 0) {
        $encoding = 'UTF-8';
        $string = trim($string);

        if ((mb_strlen($string, $encoding)) <= $maxChar) {
            return $string;
        }

        $cutPos = $maxChar;
        $boundaryPos = mb_strrpos(mb_substr($string, 0, mb_strpos($string, ' ', $cutPos)), ' ');
        return mb_substr($string, 0, $boundaryPos === false ? $cutPos : $boundaryPos) . $end;
    }

    return $string;
}



// Generate Authentication Token //
function wb_get_auth_token_ajax(){
	global $bc_account_id;
	$data 			= array();
	$id 			= "559779e8e4b072e9641b841d"; //from settings
	$client_id     	= "07aa7b60-2e1c-4775-8bc5-e3f5220d2d8d"; //API From settings
	$client_secret 	= "6a8-bE1KqdzoAbxupjTiDpEriPI4lZHUDS2mxDfU697lWuCRJNRJlJchL-ZbUCsPmC3lj-r8uram6vpPKldFmg"; //API From settings
	$auth_string   	= "{$client_id}:{$client_secret}";
	$request       	= "https://oauth.brightcove.com/v4/access_token?grant_type=client_credentials";
	$ch            	= curl_init($request);
	curl_setopt_array($ch, array(
			CURLOPT_POST           => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_SSL_VERIFYPEER => FALSE,
			CURLOPT_USERPWD        => $auth_string,
			CURLOPT_HTTPHEADER     => array('Content-type: application/x-www-form-urlencoded'),
			CURLOPT_POSTFIELDS => $data
	));
	$response = curl_exec($ch);
	curl_close($ch);
	// Check for errors
	if ($response === FALSE) {
		die(curl_error($ch));
	}
	// Decode the response
	$responseData = json_decode($response, TRUE);
	$access_token = $responseData["access_token"];
	return $access_token;
}

//Return poster or video still 640 360
function wb_get_video_still( $video_id ){
	global $bc_account_id;
	
	$access_token = wb_get_auth_token_ajax();
	
	$url = "https://cms.api.brightcove.com/v1/accounts/$bc_account_id/videos/$video_id/images";
	$header = array();
	$access = 'Authorization: Bearer ' . $access_token;
	$header[] = 'Content-Type: application/json';
	$header[] = $access;
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response= curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	
	if ( $status != "404" ){
		$response_array = json_decode($response);
		$wb_https_image_link = "";
		
		foreach ( $response_array->poster->sources as $wb_current_image_source ){
			$wb_find_word = "https";
			$wb_pos = substr($wb_current_image_source->src, 0, 5);
			if ( $wb_pos == "https" ){
				$wb_https_image_link = $wb_current_image_source->src;
			}
			
		}
		
		if ( $wb_https_image_link != "" ){
			return $wb_https_image_link;
		}else{
			return $response_array->poster->src;
		}
	}else{
		return $bc_account_id;
	}
}

function wb_get_video_thumbnail( $video_id ){
	global $bc_account_id;
	
	$access_token = wb_get_auth_token_ajax();
	
	$url = "https://cms.api.brightcove.com/v1/accounts/$bc_account_id/videos/$video_id/images";
	$header = array();
	$access = 'Authorization: Bearer ' . $access_token;
	$header[] = 'Content-Type: application/json';
	$header[] = $access;
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response= curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	
	if ( $status != "404" ){
		$response_array = json_decode($response);
		$wb_https_image_link = "";
		
		foreach ( $response_array->thumbnail->sources as $wb_current_image_source ){
			$wb_find_word = "https";
			$wb_pos = substr($wb_current_image_source->src, 0, 5);
			if ( $wb_pos == "https" ){
				$wb_https_image_link = $wb_current_image_source->src;
			}
			
		}
		
		if ( $wb_https_image_link != "" ){
			return $wb_https_image_link;
		}else{
			return $response_array->thumbnail->src;
		}
	}else{
		return "";
	}
}
?>