<?php

/*
  Filename:     pageViewsCron.php
  Description:  this file will take the data from the wb_views_log save it to the wb_views_total
  Author:       Jullie Quijano
  Change Log:
  April 21, 2014  [Jullie]Created the file
 */

$prewd = getcwd();
chdir(realpath(dirname(__FILE__)));

error_reporting(E_ALL);

include_once '../../../../wp-config.php';

echo 'connecting to database...<br />';

// connecting to database
try {
    $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
    //$logQuery = $dbh->prepare("SELECT post_id, COUNT(post_id) FROM (SELECT post_id, permalink FROM wb_views_log GROUP BY post_id, ip_address, browser) AS selection GROUP BY post_id");
    $logQuery = $dbh->prepare(" SELECT post_id,COUNT(post_id) FROM wb_views_log AS selection GROUP BY post_id");
    
    $logQuery->execute();
    $logQueryNumrows = $logQuery->rowCount();
    $logQueryResult = $logQuery->fetchAll();

    $logQuery = null;
    $dbh = null;
} catch (PDOException $e) {
    echo "Error!: Could not connect to DB";
}

echo 'Setting up sql statement...<br />';

$failedUpdatePostId = array();


print_r($logQueryResult);

echo "here";
//exit();


foreach($logQueryResult as $logRow){
    $postId = $logRow['post_id'];
    $permalink = $logRow['permalink'];
    $views = $logRow['COUNT(post_id)'];

    echo 'Checking table for postId ' . $postId . '...<br />';
    
    try {
        $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

        $postQuery = $dbh->prepare("SELECT * FROM wb_views_total WHERE post_id = ?");

        $postQuery->bindParam(1, $postId);
        
        $postQuery->execute();
        $checkNumRows = $postQuery->rowCount();
        $checkResult = $postQuery->fetchAll();

        $postQuery = null;
        $dbh = null;
    } catch (PDOException $e) {
        echo "Error!: Could not connect to DB";
    }        
    
    $updateSql = '';
    if ($checkNumRows > 0) {
        
        try {
            $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

            $updateSql = $dbh->prepare("UPDATE wb_views_total 
				SET views = views + ?
				WHERE post_id = ?");

            $updateSql->bindParam(1, $views);
            $updateSql->bindParam(2, $postId);

            $updateSql->execute();
            $updateNumRows = $updateSql->rowCount();

            $updateSql = null;
            $dbh = null;
        } catch (PDOException $e) {
            echo "Error!: Could not connect to DB";
        }            
        echo 'postId exists, updating table...<br />';
        
    } else {
        
        try {
            $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

            $updateSql = $dbh->prepare("INSERT INTO wb_views_total 
				(post_id, views)
				VALUES
				(?, ?)");

            $updateSql->bindParam(1, $postId);
            $updateSql->bindParam(2, $views);

            $updateSql->execute();
            $updateNumRows = $updateSql->rowCount();

            $updateSql = null;
            $dbh = null;
        } catch (PDOException $e) {
            echo "Error!: Could not connect to DB";
        }                    
        
        echo 'postId does not exist, inserting into table...<br />';
    }
    
    if ($updateNumRows <= 0) {
        $failedUpdatePostId[] = $postId;
    }
}

$deleteSql = '';
if (count($failedUpdatePostId) > 0) {
    $deleteCondition = array();
    foreach ($failedUpdatePostId as $currentPostId) {
        $deleteCondition[] = ' post_id <> ' . $currentPostId;
    }
    $deleteSql = "DELETE FROM wb_views_log WHERE " . implode(" AND ", $deleteCondition);
    
    try {
        $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

        $deleteSql = $dbh->prepare("DELETE FROM wb_views_log WHERE " . implode(" AND ", $deleteCondition));

        $deleteSql->execute();
        $deleteNumRows = $deleteSql->rowCount();

        $updateSql = null;
        $dbh = null;
    } catch (PDOException $e) {
        echo "Error!: Could not connect to DB";
    }          
    
    
} else {
    try {
        $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

        $deleteSql = $dbh->prepare("DELETE FROM wb_views_log");

        $deleteSql->execute();
        $$deleteNumRows = $deleteSql->rowCount();

        $updateSql = null;
        $dbh = null;
    } catch (PDOException $e) {
        echo "Error!: Could not connect to DB";
    }              
}

echo 'deleting entries from table...<br />';

if ($deleteResult) {
    echo 'Successful Deletion...<br />';
}


chdir($prewd);
?>
