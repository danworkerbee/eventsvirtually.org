<?php
/*
Filename:     getChannelPosts.php                                               
Description:  this file will get all the posts on channel setup for the shortcode Show, format it to get the details
Author:       Jullie Quijano
Change Log:
	2017-14-2017   [Mark] Updated getting thumbnail from brightcove, function included in functions.php
	June 23, 2011  [Jullie]Created the file                                                                                                                                                                                                               
*/

	$prewd = getcwd();
	chdir(realpath(dirname(__FILE__)));
	
	error_reporting(E_ALL);

	include_once '../../../../wp-config.php';
	global $wb_ent_options, $bc_account_id;
	
	$response = array();
	$response['debug'] = 'connecting to database... ';
   
   if( $wb_ent_options['vidshortcode']['allowimport'] ){
      //get theme options
       try {
           $dbh = new PDO('mysql:host=localhost;dbname='.DB_NAME.'', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
   
           $getThemeOptions = $dbh->prepare("SELECT option_value FROM wp_options WHERE option_name = 'wb_ent_options'");
   
           $getThemeOptions->execute();      
           $getThemeOptionsNumrows = $getThemeOptions->rowCount();
           $getThemeOptionsResult = $getThemeOptions->fetch();
   
           $getThemeOptions= null;  
           $dbh = null; 
   
        }catch (PDOException $e) { 
           echo "Error!: Could not connect to DB";
        }          
      $wb_ent_options = unserialize($getThemeOptionsResult['option_value']);
      
      $response['debug'] = 'Setting up sql statement.. ';
      
      $DBhost = $wb_ent_options['vidshortcode']['dbhost'];
      $DBname = $wb_ent_options['vidshortcode']['dbname'];
      $DBuser = $wb_ent_options['vidshortcode']['dbuser'];
      $DBpass = $wb_ent_options['vidshortcode']['dbpass'];         
   }
   else{
      $DBhost = DB_HOST;
      $DBname = DB_NAME;
      $DBuser = DB_USER;
      $DBpass = DB_PASSWORD;         
   }
	
	if (!$wb_ent_options){
		$wb_ent_options = wb_get_option('wb_ent_options');
   	}
  	
   	$bc_account_id = $wb_ent_options['brightcoveinfo']['publisherid'];
 
   //get posts
    try {
        $dbh = new PDO('mysql:host='.$DBhost.';dbname='.$DBname.'', $DBuser, $DBpass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

        $getChannelPosts = $dbh->prepare("SELECT p.ID, p.post_title, p.post_content, p.post_name, p.post_date, m.media_id, m.media_thumb
		FROM wp_posts p, wb_media m
		WHERE p.ID = m.post_id 
		AND post_type='post' 
		AND post_status='publish'
		GROUP BY p.ID
		ORDER BY p.post_date DESC");

        $getChannelPosts->execute();      
        $getChannelPostsNumrows = $getChannelPosts->rowCount();
        $getChannelPostsResult = $getChannelPosts->fetchAll();

        $getChannelPosts= null;  
        $dbh = null; 

     }catch (PDOException $e) { 
        echo "Error!: Could not connect to DB";
     }             

	if( $getChannelPostsNumrows <= 0 ){
		$response['message'] = 'No videos were found.';
		$response['success'] = false;
		echo json_encode($response);
		exit;
		
	}

	$response['posts'] = array();	
	foreach( $getChannelPostsResult as $postRow ){
		
		$response['posts'][$postRow['ID']]['postId'] = $postRow['ID'];
		$response['posts'][$postRow['ID']]['postTitle'] = $postRow['post_title'];
      
      if(function_exists('wb_format_string')){
          $response['posts'][$postRow['ID']]['titleExcerpt'] = wb_format_string($postRow['post_title'], false, true, 40, '... ');
      }
      else{
          $response['posts'][$postRow['ID']]['titleExcerpt'] = $postRow['post_title'];
      }
 		
		$tempArray = explode('-', $postRow['post_title']);
		
		$response['posts'][$postRow['ID']]['topic'] = $tempArray[ count($tempArray)-1 ];
		
		$response['posts'][$postRow['ID']]['postContent'] = $postRow['post_content'];
		$response['posts'][$postRow['ID']]['permalink'] = $postRow['post_name'];
		$response['posts'][$postRow['ID']]['postDate'] = date('F j, Y', strtotime($postRow['post_date']) );
		$response['posts'][$postRow['ID']]['videoId'] = $postRow['media_id'];
		$response['posts'][$postRow['ID']]['thumb'] = wb_get_video_thumbnail_url($postRow['media_id']);
	}
	
	echo json_encode($response);

chdir ( $prewd );
?>
