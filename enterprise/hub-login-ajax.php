<?php
/**
 * filename: search.php
 * description: this will be the template to be used to display search results
 * author: WB WebTeam
 * date created: 2014-04-15
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: Hub Email Verification
 */
global $wpdb;
date_default_timezone_set('America/Winnipeg');

$email 		= filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
$wb_role	= $_POST['role'];
$wb_result 	= "failed";
$wb_currently_log = "";
//$wb_currently_log = "false";
$page_id = $_POST['page_id'];
$category_id = $_POST['category_id'];
$is_logged_in_already_meta = 'wb_vh_logged_in_hub_' . $category_id;
$restrict_user_logged_in = wp_validate_boolean( get_post_meta( $page_id, 'restrict_user_logged_in', true ) );

// if( $restrict_user_logged_in ) {
//     $loggedin_users = get_users(
//         array(
//             'fields' => array( 'user_email' ),
//             'meta_query' => array(
//                 array(
//                     'key' => 'wb_vh_user_logged_in',
//                     'value' => '1',
//                     'compare' => '='
//                 )
//             )
//         )
//     );

//     $user_emails = array();
//     foreach( $loggedin_users as $user ){
//         $user_emails[] = $user->user_email;
//     }

//     if( in_array( $email, $user_emails ) ) {
//         echo json_encode(array('currently_logged_to_other_device' => true, 'result' => $wb_result));
//     } 
// } 

if( trim( $email ) != "" ){
    $emailExists = email_exists($email);
    if ($emailExists) {
        $userId = (is_numeric($emailExists)) ? $emailExists : ( (is_numeric($email)) ? $email: 0 );
        $user_info = get_userdata($userId);
        //check if has session
        $sessions = WP_Session_Tokens::get_instance($userId);
        $all_sessions = $sessions->get_all();
        
        //if ( !$all_sessions ){
            if ( in_array($wb_role, $user_info->roles)){
                $wb_result = 'exist';
                wp_set_auth_cookie($userId, false);
                //db log if needed
                $wb_result = 'exist';

                $is_logged_in_already = get_user_meta( $userId, $is_logged_in_already_meta, true );
                if( $is_logged_in_already != 'yes' ) {
                    update_user_meta( $userId, $is_logged_in_already_meta, 'yes' );
                }
            }
        //}else{
            //$sessions->destroy_all();  //this will remove all existing sessions before logging in (per user id)
            //$wb_currently_log = " currently logged";
        //}
        //echo "<pre>";
        //print_r($user_info);
        //echo "</pre>";
    }
}

$win_time = date("Y-m-d h:i:sa");
$wb_ip = get_client_ip();
$wpdb->insert(
        'wb_email_login_log',
        array(
                'email_address' => $email,
                'status' => $wb_result . $wb_currently_log,
                'win_time' => $win_time,
                'ip_address' => $wb_ip
        ),
        array(
                '%s',
                '%s',
                '%s',
                '%s'
        )
        );
echo json_encode(array('currently_logged_in' => $wb_currently_log, 'result' => $wb_result));


//echo $wb_result;

function get_client_ip() {
	$ipaddress = '';
	if (getenv('HTTP_CLIENT_IP'))
		$ipaddress = getenv('HTTP_CLIENT_IP');
	else if(getenv('HTTP_X_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	else if(getenv('HTTP_X_FORWARDED'))
		$ipaddress = getenv('HTTP_X_FORWARDED');
	else if(getenv('HTTP_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_FORWARDED_FOR');
	else if(getenv('HTTP_FORWARDED'))
		$ipaddress = getenv('HTTP_FORWARDED');
	else if(getenv('REMOTE_ADDR'))
		$ipaddress = getenv('REMOTE_ADDR');
	else
		$ipaddress = 'UNKNOWN';
	return $ipaddress;
}
?>