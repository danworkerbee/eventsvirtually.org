<?php
/**
 * filename: wb-custom-login.php
 * description: custom login page for wordpress
 * author: Jullie Quijano
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: WB Custom Login Page - Additional Fields
 * Notes: 
 *  -all the fields that are not hidden are required fields
 *  -at the moment, only the email (email validation) and membernumber(regex validation) are functional
 * 
 */
global $wb_ent_options;


/*
 * temporary settings. this should be added to the theme settings
 */
$memberUserRole = 'free_user_role'; //what user role new members will be added to 
$emailValidation = 'email'; //can be hidden, email, database

$membershipValidation = 'hidden'; //can be hidden, database, selection(this means it should be in an array)
$membershipLabel = ''; //what the membership dropdown field is to be called
$membershipArray = null; //this will be a list of membership types to be displayed as options in the drop down
    /*
     * array will be as follows
            array(
             'Type 1' => 'value1'
            )
     * the type will be what's going to be displayed as an option and value will be the value for that option
     */
$membershipValidationArray = null; //only used when $membershipValidation is set to selection; this will be an array of the allowed values from above

$memberNumberValidation = 'regex'; //can be hidden, none, database, regex
$memberNumberLabel = 'Member Number'; //what the member number is to be called
$memberValidationRegex = '/\d{8}/'; //only used when $memberNumberValidation is set to regex; should include the slashes eg '/*.{0,5}/'

$passwordValidation = 'hidden'; //can be hidden, none, database	

$helpLink = array(
    'enabled' => true,
    'linkText' => 'Need help?',
    'linkUrl' => '/?p=58'
);
/*
 * custom redirect
 */
$loginRedirect = '/';
if (isset($_GET['redir']) && trim($_GET['redir']) != '') {
    $loginRedirect = $_GET['redir'];
}

/*
 * there will be modes of display
 *  -full page
 *  -form only
 *      $_GET['dmode'] = 'form'
 */
$displayMode = 'full';
if (isset($_GET['dmode']) && trim($_GET['dmode']) != '') {
    $displayMode = $_GET['dmode'];
}


//processing login
$errorMessage = '';
$errorArray = array();
$successMessage = '';
$createNewUser = false;
if (trim($_POST['loginButton']) == "Login") {
    $loginEmailAddress = sanitize_text_field($_POST['email']);
    $loginPassword = sanitize_text_field($_POST['pwd']);
    $loginMembership = sanitize_text_field($_POST['membership']);
    $loginMemberNumber = sanitize_text_field($_POST['memberNumber']);
    
    if( $emailValidation != 'hidden' && trim($loginEmailAddress) == '' ){
        $errorArray[] = 'Email address is required';
    }
    elseif( $emailValidation == 'email' && !filter_var($loginEmailAddress, FILTER_VALIDATE_EMAIL) ){
        $errorArray[] = 'Email address is invalid';
    }
    elseif( $emailValidation == 'database' && !email_exists($loginEmailAddress)) {
        $errorArray[] = 'Email address is invalid';
    }       
    
    if( $membershipValidation != 'hidden' && trim($loginMembership) == 'default' ){
        $errorArray[] = $membershipLabel.' is required';
    } 
    
    if( $memberNumberValidation != 'hidden' && trim($loginMemberNumber) == '' ){
        $errorArray[] = $memberNumberLabel.' is required';
    } 
    elseif( $memberNumberValidation == 'regex' && !preg_match($memberValidationRegex, $loginMemberNumber) ){
        $errorArray[] = $memberNumberLabel.' is invalid';
    }     
 
    
    if( count($errorArray) == 1 ){
        $errorMessage = $errorArray[0];
    }
    elseif( count($errorArray) > 1 ){
        $errorMessage = 'There are errors on the form: <ul>';
        foreach($errorArray as $currentError){
            $errorMessage .= '<li>'.$currentError.'</li>';
        }
        $errorMessage .= '</ul>';
    }
    else{
        //email database validation, member database validation, membership database validation
        
        //email validation and regex membernumber validation only
        if( $emailValidation == 'email' && filter_var($loginEmailAddress, FILTER_VALIDATE_EMAIL) && $memberNumberValidation == 'regex' && preg_match($memberValidationRegex, $loginMemberNumber) ){
            //check if email address exists
            if( email_exists($loginEmailAddress) ){
                $userId = email_exists($loginEmailAddress);
                update_user_meta( $userId, 'membernumber', $loginMemberNumber );
                update_user_meta( $userId, 'membership', 'member' );
                wp_set_auth_cookie($userId);
                
               $cookieValueArray = array(
                 'email' => $loginEmailAddress,
                 'ID' => $userId,
                 'newrole' => $memberUserRole
               );
               $cookieValueString = json_encode($cookieValueArray);
               $cookieName = $wb_ent_options['clientcode'].'nuRolAd'.$_SERVER['REMOTE_ADDR'];
               setcookie( $cookieName, $cookieValueString, time()+10*60*60, "/");
            }
            else{
                $createNewUser = true;                
            }
        }
        else{
            $errorMessage = 'Please check your Email Address and '.$memberNumberLabel;
        }
    }
    
    if( $createNewUser ){
       $userdata = array(
           'user_login'  =>  str_replace( array('+', '!', '#', '$', '%', '&', '\'', '*', '+', '-', '/', '=', '?', '^', '`', '{', '|', '}', '~'), '', $loginEmailAddress),
           'user_email'  =>  $loginEmailAddress,
           'role'    =>  $memberUserRole,
           'user_pass'   =>  strtolower('wbtv'.$loginEmailAddress.'member')
       );

       $userId = wp_insert_user( $userdata );
       

       if( is_numeric($userId)){
             update_user_meta( $userId, 'membernumber', $loginMemberNumber );
             update_user_meta( $userId, 'membership', 'member' );                          
             wp_set_auth_cookie( $userId, true );
       }               
       else{          
          $errorMessage = 'Please enter a valid email address.';
       }         

    }    
}


if ($displayMode == 'full' && ( is_user_logged_in() || ( trim($_POST['loginButton']) == "Login" && trim($errorMessage) == '' ) )) {
   header('location: ' . $loginRedirect);
   exit;
}

if ($displayMode == 'full') {
    get_header();
} else {
    ?>
    <!doctype html>  
    <!--[if IEMobile 7 ]> <html lang="en-US"class="no-js iem7"> <![endif]-->
    <!--[if lt IE 7 ]> <html lang="en-US" class="no-js ie6"> <![endif]-->
    <!--[if IE 7 ]>    <html lang="en-US" class="no-js ie7"> <![endif]-->
    <!--[if IE 8 ]>    <html lang="en-US" class="no-js ie8"> <![endif]-->
    <!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html lang="en-US" class="no-js"><!--<![endif]-->
        <head>
       <?php
       if ((trim($_POST['loginButton']) == "Login" && trim($errorMessage) == '') || is_user_logged_in() ) {
        ?>
                <script type="text/javascript">
                    window.parent.location.href = '<?php echo $loginRedirect; ?>';
                </script>
                <?php
            }
            ?>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!--
            put custom styling here for when it's not a full page    
            -->
            <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/library/js/responsive/jquery.min.js"></script>
            <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/library/js/responsive/jquery-ui.min.js"></script>
            <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap3.3.4.min.css">
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
            <!-- Latest compiled and minified JavaScript -->

            <style>
                #viewingTips {font-family: "Open Sans", Arial, sans-serif;}
                #viewingTips label{padding:0;margin: 15px 0 5px;font-weight:normal;text-transform:uppercase;}
                #viewingTips .btnsubmit{margin: 20px 0;}
                #viewingTips .alert-danger{margin:0 0 5px 0;}
                #viewingTips h1{font-size: 18px;text-transform:uppercase;color:#184975;font-weight:bold;}
                #loginForm, #recoverForm{padding:0 5px;}
                .btn-clia {
                background-color: #4ac0b4;
                border-color: #41a89c;
                }
                .btn-clia:hover{
                background-color: #41a89c;
                border-color: #389187;
                }
                .btnsubmit a{color:#41a89c;text-decoration:underline;}                
                .clear{clear:both;}
            </style>
        </head>
        <body>
    <?php
}
$recoverPasswordDisplay = 'style="display: none;"';
$loginFormDisplay = '';
if (trim($_POST['recoverButton']) == "Reset Password") {
    $recoverPasswordDisplay = '';
    $loginFormDisplay = 'style="display: none;"';
}
?>

        <div id="wb_ent_content" class="clearfix row-fluid">
            <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
                <div id="viewing-tips">                    
                    <div id="viewingTips">              
                        <form action="" method="post" id="loginForm" class="container-fluid" <?php echo $loginFormDisplay; ?>>
                            <h1 class="pagetitle col-lg-12">Sign in for unlimited access</h1>
                            <?php
                            if (trim($_POST['loginButton']) == "Login" && trim($errorMessage) != '') {
                                echo '<div class="alert alert-danger clear" role="alert">' . $errorMessage . '</div>';
                            } elseif (trim($_POST['loginButton']) == "Login" && is_wp_error($userLoggedIn)) {
                                echo '<div class="alert alert-danger clear" role="alert">' . $userLoggedIn->get_error_message() . '</div>';
                            }
                            
                            if( $emailValidation != 'hidden'){
                            ?>                                    
                            <div class="container-fluid">
                                <label class="col-xs-12">Email Address:</label> <input name="email" type="text" class="col-xs-12" value="<?php echo $loginEmailAddress; ?>"/>
                            </div> 
                            <?php
                            }
                            
                            if( $membershipValidation != 'hidden' && is_array($membershipArray) && count($membershipArray) > 0 ){
                            ?>                                    
                            <div class="container-fluid">
                                <label class="col-xs-12"><?php echo $membershipLabel; ?>:</label>
                                <select name="membership" class="col-xs-12">
                                    <option value="default"> Select membership </option>
                                <?php
                                foreach($membershipArray as $currentMembership => $currentValue){
                                    echo '<option value="'.$currentValue.'"';
                                    if($loginMembership == $currentValue){
                                        echo ' selected="selected" ';
                                    }
                                    echo '>'.$currentMembership.'</option>';
                                }
                                ?>
                                </select>
                            </div> 
                            <?php
                            }    
                            
                            if( $memberNumberValidation != 'hidden'){
                            ?>                                    
                            <div class="container-fluid">
                                <label class="col-xs-12"><?php echo $memberNumberLabel; ?>:</label> <input name="memberNumber" type="text" class="col-xs-12" value="<?php echo $loginMemberNumber; ?>"/>
                            </div> 
                            <?php
                            }                            
                            if( $passwordValidation != 'hidden'){
                            ?>
                            <div class="container-fluid">
                                <label class="col-xs-12">Password: </label><input name="pwd" type="password" class="col-xs-12" />
                            </div>
                            <?php
                            }                                                        
                            ?>
                            <div class="container-fluid btnsubmit">
                                <input type="submit" name="loginButton" class="btn btn-info btn-login-custom col-xs-4" value="Login" />       
                                <?php
                                if( $helpLink['enabled'] === true ){
                                ?>
                                <a class="col-xs-6" href="<?php echo $helpLink['linkUrl']; ?>" target="_TOP">
                                    <?php echo $helpLink['linkText']; ?>
                                </a>                                
                                <?php
                                }
                                ?>
                            </div>
                        </form>                          
                    </div>
                </div>
            </div>
        </div>
    <?php
    if ($displayMode == 'full') {
        get_sidebar();
        get_footer();
    } else {
        ?>
        </body>
    </html>            
    <?php
    }        
    ?>