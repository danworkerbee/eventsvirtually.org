<ul class="dropdown-menu">
	<li><a href="/all-episodes" class="noArrowNav"><?php _e( 'Recent Episodes', 'enterprise' ); ?></a></li>
    <li id="wb-browse-all-videos-nav"><a href="/all-videos" class="noArrowNav"><?php _e( 'All Posts', 'enterprise' ); ?></a></li>
    <li><a href="/most-viewed-videos" class="noArrowNav"><?php _e( 'Most Viewed', 'enterprise' ); ?></a></li>
    <?php 
    	if($wb_ent_options['wbtvnavigation']['addbrowseitem']){
       		echo $wb_ent_options['wbtvnavigation']['addbrowseitem'];
         }
	?>
    <!-- <li style="background-color: transparent;">
     	<div style="height: 0px; margin: 5px;">
        	<hr style="background-color: transparent; margin: 0 auto; width: 90%; border: 0px none  #0046AD; border-bottom: 1px solid  #0046AD;" class="wb-nav-hr"/>
        </div>
	</li> -->
    <?php
    	if (!isset($postCategories) || count($postCategories) <= 0) {
        	$postCategories = wb_get_categories();
        }
        foreach ($postCategories as $currentCat) { 
        	$clarrow = "";
            if ($currentCat['subCatNumRows'] <= 0) {                                
            	$clarrow = "noArrowNav" ;                                           
            }

            if(isset($_GET['mobile'])){
                //print_r($currentCat);
            }


	?>
	<li class="dropdown-submenu">
        <?php   ?>
   		<a href="<?= get_site_url().'/category/' . $currentCat['cat_slug'] ?>" class="<?= $clarrow ?>"><?php printf( __('%s', 'enterprise'), $currentCat['cat_name']); ?></a>
        <?php
        	if ($currentCat['subCatNumRows'] > 0) {
                //echo "dfd";
        ?>
        <ul class="dropdown-menu">
        <?php


        	foreach ($currentCat['subCat'] as $currentSubCat) {
            	$topics = get_categories( array( 'child_of' => $currentSubCat['subCat_id'], 'orderby' => 'name' ) );
                if ($currentSubCat['subCat_count'] > 0) {
                	if( count($topics) > 0 ){  
        ?>
        	<li class="dropdown-submenu">                         
            	<a class="dropdown-toggle" href="<?= get_site_url().'/category/' . $currentSubCat['subCat_slug'] ?>"><?php printf( __('%s', 'enterprise'), $currentSubCat['subCat_name']); ?></a>
                	<ul class="dropdown-menu">
	                <?php
		                foreach( $topics as $current_topic ){
                    ?>
	                    <li>
	                    	<a href="<?= get_site_url().'/category/' . $current_topic->slug ?>"><?php printf( __('%s', 'enterprise'), $current_topic->name); ?></a>    
                        </li>
                    <?php
                    	}
	               	?>
                    	<li>
	                        <a href="<?= get_site_url().'/category/' .  $currentSubCat['subCat_slug'] ?>">View All</a>    
                    	</li>
                  	</ul>
          	</li>
            		<?php  
             			}else{
                     ?>
            <li> 
            	<a href="<?= get_site_url().'/category/' . $currentSubCat['subCat_slug'] ?>"><?php printf( __('%s', 'enterprise'), $currentSubCat['subCat_name']); ?></a>
         	</li>
           	<?php  
             			}
                   	} // end of if
              	} // end of foreach 
           	?>
            <li>
           		<a href="<?= get_site_url().'/category/' .  $currentCat['cat_slug']  ?>">View All</a>    
         	</li>
   		</ul>
        <?php 
        	} 
      	?>
   	</li>
    <?php 
        } 
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );


    if ( is_plugin_active( 'workerbee-audio-posts/wb-audio-post.php' ) ){
    	$postAudioCategories = wb_get_audio_categories($wb_ent_options['videocats']['podcast'], true);
    	if($postAudioCategories){
   	?>
     <li style="background-color: transparent;">
     	<div style="height: 0px; margin: 5px;">
        	<hr style="background-color: transparent; margin: 0 auto; width: 90%; border: 0px none  #0046AD; border-bottom: 1px solid  #0046AD;" class="wb-nav-hr"/>
        </div>
	</li>

	<li class="dropdown-submenu" id="resp-podcast-li-nav">
		<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" id="resp-podcast-nav">Podcast</a>
		<ul class="dropdown-menu">
		<?php 
			foreach ($postAudioCategories as $currentCat) { 
		    	$clarrow = "";
		        if ($currentCat['subCatNumRows'] <= 0) {                                
		        	$clarrow = "noArrowNav" ;                                           
		        }  
		        ?>
            <li class="dropdown-submenu" id="sub_<?php echo $currentCat['cat_slug']; ?>">
            	<a href="<?= get_site_url().'/category/' . $currentCat['cat_slug'] ?>" class="<?= $clarrow ?>"><?php printf( __('%s', 'enterprise'), $currentCat['cat_name']); ?></a>
            </li>
            <?php
            	if ($currentCat['subCatNumRows'] > 0) { 
            	?>
               	<style>
               		li#sub_<?php echo $currentCat['cat_slug']; ?> a.mm-subopen:before{
               			content: '';
					    border-left-width: 0px;
					    border-left-style: solid;
					    display: block;
					    height: 100%;
					    position: absolute;
					    left: 20px;
					    top: 10px;
               		}
               		li#sub_<?php echo $currentCat['cat_slug']; ?> a.mm-subopen:hover::before{
               			content: '<?php echo html_entity_decode($currentCat['cat_name']); ?>';
               		}
               	</style>
               	<ul class="dropdown-menu">
               	<?php 
               		foreach ($currentCat['subCat'] as $currentSubCat) {
               			$topics = get_categories( array( 'child_of' => $currentSubCat['subCat_id'], 'orderby' => 'name' ) );
               			
               			$wb_args = array(
               					'category'         	=> $currentSubCat['subCat_id'],
               					'numberposts'		=> 10,
               					'orderby'          	=> 'date',
               					'order'            	=> 'DESC',
               					'post_type'       	=> 'wb_audio',
               					'post_status'      	=> 'publish'
               			);
               			
               			$wb_posts_array = get_posts( $wb_args );
               			print_r($wb_posts_array);
               		}
               	
               	
               	?>		
               	</ul>
            	<?php 	
            	}
		        
		        
			}
		?>
		</ul>
	</li>
    <?php 
    	}
    }
  	?>     
   	 
</ul>


<style type="text/css">
	#navinavi ul li ul.dropdown-menu ul.dropdown-submenu > a:after{
    	display: block;
    }
    li.dropdown-submenu a.mm-subopen {
    	width: 100%;
	    color: transparent;
	}
</style>