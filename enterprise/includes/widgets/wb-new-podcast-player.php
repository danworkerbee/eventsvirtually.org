<?php 
global $wb_ent_options;

$wb_podcast_bottom_link = ' style="display: none;" ';
if ( in_category( $wb_ent_options['videocats']['podcast'], $postId ) ){
    $wb_podcast_bottom_link = "";
    //Get podcast feed information
    $wb_post_cats = wp_get_post_categories( $postId );
    
    if (($key = array_search($wb_ent_options['videocats']['podcast'], $wb_post_cats)) !== false) {
        unset($wb_post_cats[$key]);
    }
    $wb_post_cats = array_values($wb_post_cats);
    $wb_feed_term_info = get_category( $wb_post_cats[0] );
    
    if ( $wb_feed_term_info->taxonomy == "category" ){
        //echo "<pre>";
        //print_r($wb_feed_term_info);
        //echo "</pre>";
        $wb_post_title = get_the_title($postId);
        if ( $wb_feed_term_info->name != "" ){
            $wb_podcast_feed            = $wb_feed_term_info->name;
            $wb_post_date = get_the_date( 'F j, Y' );
            $wb_podcast_artwork         = get_term_meta( $wb_feed_term_info->term_id, "wb_artwork_image", true );
            $wb_podcast_privacy         = get_term_meta( $wb_feed_term_info->term_id, "wb-privacy-type", true );
            $wb_podcast_show_type       = get_term_meta( $wb_feed_term_info->term_id, "wb_feed_show_type", true );
            $wb_podcast_author          = get_term_meta( $wb_feed_term_info->term_id, "wb_feed_author", true );
            $wb_podcast_additional_text = get_term_meta( $wb_feed_term_info->term_id, "wb-feed-additional-text", true );
            $wb_podcast_cat1            = get_term_meta( $wb_feed_term_info->term_id, "wb-feed-cat1", true );
            $wb_podcast_subcat1         = get_term_meta( $wb_feed_term_info->term_id, "wb-feed-subcat1", true );
            $wb_podcast_cat2            = get_term_meta( $wb_feed_term_info->term_id, "wb-feed-cat2", true );
            $wb_podcast_subcat2         = get_term_meta( $wb_feed_term_info->term_id, "wb-feed-subcat2", true );
            $wb_podcast_cat3            = get_term_meta( $wb_feed_term_info->term_id, "wb-feed-cat3", true );
            $wb_podcast_subcat3         = get_term_meta( $wb_feed_term_info->term_id, "wb-feed-subcat3", true );
            if ( $wb_podcast_artwork != "" ){
                $wb_artwork = $wb_podcast_artwork;
            }
        }
        
        $wb_podcast_feed_rss = WB_AUDIO_DIR_URL ."podcast-feed.php?podcast=" .  $wb_feed_term_info->term_id;
    }
}
?>
<script>
	function renderModal(selector) {
		console.log(selector);	
        $(selector).click();
	}
	function wb_copy_link() {
		var copyText = document.getElementById("wb-podcast-feed-rss");
		copyText.select();
		copyText.setSelectionRange(0, 99999); /*For mobile devices*/
		document.execCommand("copy");
	
		var tooltip = document.getElementById("wb-rss-tooltip");
		tooltip.innerHTML = "RSS link copied!";
	}

	function outFunc() {
		  var tooltip = document.getElementById("wb-rss-tooltip");
		  tooltip.innerHTML = "Copy to clipboard";
	}
	
</script>
<div class="wb-podcast-bottom-links-episode-info" data-toggle="modal" data-target="#wb-podcast-episode-info" style="display: none;"></div>
<div class="wb-podcast-bottom-links-subscribe" data-toggle="modal" data-target="#wb-podcast-subscribe" style="display: none;"></div>
<div class="modal fade" id="wb-podcast-episode-info" tabindex="-1" role="dialog" aria-labelledby="Episode Information" aria-hidden="true">
	<div class="modal-dialog" role="document">
    	<div class="modal-content">
        	<div class="modal-header wb-podcast-modal-header">
            	<h5 class="modal-title wb-podcast-modal-title" id="wb-podcast-modal-title">Episode Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                	<span aria-hidden="true">&times;</span>
               	</button>
       		</div>
            <div class="modal-body wb-podcast-content">
            	<h5 class="wb-podcast-feed-title-modal" ><?php echo $wb_podcast_feed; ?></h5>
            	<h4 class="wb-podcast-podcast-title-modal" ><?php echo $wb_post_title; ?></h4>
            	<h6 class="wb-podcaste-date"><?php echo $wb_post_date; ?></h6>
            	<?php the_content(); ?>
            </div>
            <!-- 
            <div class="modal-footer">
            	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
             -->
       	</div> 
  	</div>
</div>
<div class="modal fade" id="wb-podcast-subscribe" tabindex="-1" role="dialog" aria-labelledby="Episode Subscribe" aria-hidden="true">
	<div class="modal-dialog" role="document">
    	<div class="modal-content">
        	<div class="modal-header wb-podcast-modal-header">
            	<h5 class="modal-title wb-podcast-modal-title" id="exampleModalLongTitle">Subscribe</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                	<span aria-hidden="true">&times;</span> 
               	</button>
       		</div>
            <div class="modal-body wb-podcast-subscribe-body">
            	<div class="container-fluid">
            		<?php if ( $wb_podcast_privacy == "public" ){ ?>
                    <div class="row wb-podcast-feed-icon-row">
                      <div class="col-xs-6">
                      	<a href="#"><img class="wb-podcast-feed-subs" src="https://d2cc3c6p0yky96.cloudfront.net/images/wb-apple-podcast.png" /></a>
                      	<label class="wb-podcast-feed-name">Apple Podcasts</label>	
                      </div>
                      <div class="col-xs-6">
                      	<a href="#"><img class="wb-podcast-feed-subs" src="https://d2cc3c6p0yky96.cloudfront.net/images/wb-overcast.png" /></a>
                      	<label class="wb-podcast-feed-name">Overcast</label>
                      </div>
                    </div>
                    <div class="row wb-podcast-feed-icon-row">
                      <div class="col-xs-6">
                      	<a href="#"><img class="wb-podcast-feed-subs" src="https://d2cc3c6p0yky96.cloudfront.net/images/wb-pocketcast.png" /></a>
                      	<label class="wb-podcast-feed-name">Pocket Casts</label>	
                      </div>
                      <div class="col-xs-6">
                      	<a href="#"><img class="wb-podcast-feed-subs" src="https://d2cc3c6p0yky96.cloudfront.net/images/wb-google-podcast.png" /></a>
                      	<label class="wb-podcast-feed-name">Google Podcasts</label>
                      </div>
                    </div>
                    <div class="row wb-podcast-feed-icon-row">
                      <div class="col-xs-6">
                      	<a href="#"><img class="wb-podcast-feed-subs" src="https://d2cc3c6p0yky96.cloudfront.net/images/wb-spotify.png" /></a>
                      	<label class="wb-podcast-feed-name">Spotify</label>	
                      </div>
                      <div class="col-xs-6">
                      	<a href="#"><img class="wb-podcast-feed-subs" src="https://d2cc3c6p0yky96.cloudfront.net/images/wb-castro.png" /></a>
                      	<label class="wb-podcast-feed-name">Castro</label>
                      </div>
                    </div>
                    <?php } ?>
                    <div class="row wb-podcast-feed-icon-row">
                    	<div class="col-md-8">
                        	<img class="wb-podcast-feed-subs" src="https://d2cc3c6p0yky96.cloudfront.net/images/wb-generice-feed.png" />
                        	<input type="text" id="wb-podcast-feed-rss" value="<?php echo $wb_podcast_feed_rss; ?>" readonly/>
                        </div>
                        <div class="col-md-4 wb-podcast-copy-wrapper wb-tooltip-podcast">
                        	<button type="button" class="btn btn-secondary" id="wb-podcast-copy-link" onclick="wb_copy_link()" onmouseout="outFunc()">Copy RSS Link
                        		<span class="tooltiptext" id="wb-rss-tooltip">Copy to clipboard</span>
                        	</button>
                        	<a href="/contact" target="_blank" id="wb-need-help-link">Need help? Click here</a>
                        </div>
                    </div>
                 </div>
            </div>
            <!-- 
            <div class="modal-footer">
            	<div class="container-fluid">
                	
            	</div>
            </div>
             -->
       	</div>
  	</div>
</div>
<style>
    .wb-podcast-feed-icon-row .col-xs-6{
        width: 50%;
        padding: 0px 16px;
    }
    .wb-podcast-copy-wrapper a:hover{
        cursor: pointer;
    }
    #wb-need-help-link{
        font-size: 12px;
        font-family: "Noto Sans";
        font-weight: 600;
    }
    .wb-podcast-copy-wrapper{
        text-align: center;
        padding-top: 5px;
    }
    #wb-podcast-copy-link{
        font-size: 12px;
        font-family: "Noto Sans";
        width: 100%;
    }
    #wb-podcast-feed-rss{
        max-width: 235px;
        width: 100%;
        margin-left: 10px;
        padding: 5px;
    }
    .wb-podcast-subscribe-body > div,
    .wb-podcast-subscribe-body{
        background-color: #eee;
    }
    .wb-podcast-subscribe-body{
       padding: 20px 20px 10px;
    }
    .wb-podcast-feed-icon-row{
        margin-bottom: 12px;
        background-color: #eee;
        margin-left: 0px;
    }
    .wb-podcast-feed-name{
        color: #083050;
        font-family: "Noto Sans";
        vertical-align: bottom;
        margin-bottom: 5px;
        margin-left: 10px;
        font-weight: 600;
    }
    .wb-podcast-feed-subs{
        max-width: 44px;
        width: 100%;
        border-radius: 10px;
    }
    .wb-podcast-content p{
        font-size: 14px !important;
        font-family: "Noto Sans";
    }
    .wb-podcaste-date{
        font-size: 12px;
        font-family: "Noto Sans";
        font-weight: 600;
        color: #8397A7;
        margin-bottom: 20px;
    }
    .wb-podcast-feed-title-modal{
        font-size: 14px;
        font-family: "Noto Sans";
        margin-bottom: 5px;
    }
    .wb-podcast-podcast-title-modal{
        font-size: 16px;
        font-weight: 800;
        font-family: "Noto Sans";
        margin-bottom: 5px;
    }
    #wb-podcast-episode-info .modal-dialog{
        max-width: 350px;
        width: 100%;
        margin: 10% auto 0
    }
    #wb-podcast-episode-info{
    	width: 350px;
    	margin-left: -175px;
    }
    .wb-podcast-modal-title{
        font-weight: bold;
        font-size: 18px;
        color: #083050;
        font-family: "Noto Sans";
    }
    .wb-podcast-modal-header{
        background-color: #d9dfe4;
    }
    .resp-container {
        position: relative;
        overflow: hidden;
        padding-top: 23.25%;
    }
    .resp-iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border: 0;
    }
    @media (max-width: 1402px) {
        .resp-container {
            padding-top: 24.50%;
        }
    }
     @media (max-width: 1337px) {
        .resp-container {
            padding-top: 26%;
        }
    }
    @media (max-width: 1257px) {
        .resp-container {
            padding-top: 28.25%;
        }
    }
    @media (max-width: 1199px) {
        .resp-container {
            padding-top: 40.25%;
        }
    }
    @media (max-width: 767px) {
        .resp-container {
            padding-top: 48.25%;
        }
        .wb-podcast-feed-icon-row div #wb-podcast-feed-rss{
            max-width: 75%; 
        }
        #wb-podcast-copy-link{
            margin-top: 12px;
            max-width: 100% !important;
        }
        .wb-tooltip-podcast{
            max-width: 100%;
            flex: 0 0 100%;
        }
        div#wb-podcast-subscribe {
		     max-width: 100%; 
		     width: auto;
		     margin-left: auto; 
		}
		#wb-podcast-episode-info {
		    width: auto;
		    margin-left: auto;
		}
		div#wb-podcast-episode-info .modal-dialog {
		    max-width: 100%;
		    width: 100%;
		    margin: 0 auto;
		}
    }
    
    @media (max-width: 480px) {
        .wb-podcast-feed-icon-row .col-xs-6{
            width: 100%;
            padding: 5px 16px;
        }
        .wb-podcast-feed-icon-row{
            margin-bottom: 0px;
        }
        #wb-podcast-subscribe .modal-dialog {
            margin-top: 16%;
        }
    }
    
    #wb-podcast-subscribe .modal-dialog{
        margin-top: 0;
    }   
    #wb-podcast-subscribe{
    	max-width: 400px;
    	width: 100%;
   	 	margin-left: -200px;
    }
    
    .wb-tooltip-podcast #wb-podcast-copy-link #wb-rss-tooltip {
         visibility: hidden;
         width: 170px;
         background-color: #555;
         color: #fff;
         text-align: center;
         border-radius: 6px;
         padding: 5px;
         position: absolute;
         z-index: 1;
         bottom: 115%;
         left: 43%;
         margin-left: -75px;
         opacity: 0;
         transition: opacity 0.3s;
    }
     .wb-tooltip-podcast #wb-podcast-copy-link #wb-rss-tooltip::after {
         content: "";
         position: absolute;
         top: 100%;
         left: 50%;
         margin-left: -5px;
         border-width: 5px;
         border-style: solid;
         border-color: #555 transparent transparent transparent;
    }
     .wb-tooltip-podcast #wb-podcast-copy-link:hover #wb-rss-tooltip {
         visibility: visible;
         opacity: 1;
    }
    #wb-podcast-episode-info .modal-dialog {
	    max-width: 350px;
	    width: 100%;
	    margin: 0 auto;
	}
	.modal-header .close {
	    margin-top: -20px;
	}
</style>      
<!--    
<div class="resp-container">   class="resp-iframe"  -->                                           	
	<iframe id="wb-audio-player-frame" name="wb-audio-player-frame" style="width: 100%; min-height: 175px; border: none;"  src="/wp-content/plugins/workerbee-audio-posts/audio-player/wb-podcast-player.php?p_audio=<?php echo $postId; ?>"></iframe>
<!--  </div>	-->										  		