<?php
/*
  Filename:      download.php
  Description:
  Author:        Jullie Quijano
  Changes:
  				12-18-2017		[Mark] 	Discontinued using brightcove_curl function in function.php that uses old Brightcove Media API
  										Updated the download renditions list using new Brightcove API
  										Fixed Google Analytics download click event
 */

if (isset($video) && isset($video['mediaId']) && trim($video['mediaId']) != '') {
    ?>
    <div id="download" class="gradBox">
        <style>
            #leftCol #download h4{
                font-size: 110%;
                margin: 0px 0px 10px 20px;
                font-weight: bold;
                text-align: left;			
            }
            #leftCol #download div{
                margin-left: 20px;	
            }
            #leftCol #download p{
                margin-right: 20px;
            }	
            #leftCol #download .helpLink{
                font-size: 70%; 
                color: #676767;
            }
            #main #download{
                margin: 30px auto 40px;
                padding: 0 0 0 10px;
                text-align: left;
                max-width: 500px;   
                overflow: auto;
            }
            #main #download h4{
                font-size: 15px;
                margin: 0px 0px 10px;
                font-weight: bold;
                text-align: left;			
            }
            #main #download div{
                margin-left: 20px;	
                margin-right: 10px;
                display: block;
                float: left;
            }
            #main #download #donloadIcon{
                margin-left: 80px;
            }
            #main #download ul{
                clear: both;
            }
            #main #download p{
                float: left;
                margin-left: 20px;
                margin-top: 0;
            }	
            #main #download .helpLink{
                font-size: 70%; 
                color: #676767;
            }      
        </style>
        <h4>Download for PC/Mac, iPhone/iPod, Blackberry, Smartphone <a class="helpLink" href="<?php echo get_site_url() . '/' ?>viewing-tips" target="_new"> (Need help?)</a></h4>
        <div id="donloadIcon" style="display:block; float:left; margin-right: 10px;"><img name="downloadicon" src="/wp-content/themes/enterprise/images/downloadicon.gif" width="74" height="57" alt="Downloads" /></div>
            <?php
            
            $wb_video_sources = json_decode(wb_get_video_sources_mp4($video['mediaId']));
            $wb_video_sources_array = array();
            $wb_video_sources_array_sorted = array();
            $wb_video_sources_ctr = 0;
            $analytics = $wb_ent_options['analytics'];
            foreach ( $wb_video_sources as $current_video_source ){
            	if ( $current_video_source->src != "" && $current_video_source->size > 0 && strrpos( $current_video_source->src,'https' ) !== false ){
            		$wb_video_sources_array[$wb_video_sources_ctr]['url'] 		= $current_video_source->src;
            		$wb_video_sources_array[$wb_video_sources_ctr]['size'] 		= $current_video_source->size;
            		$wb_video_sources_array[$wb_video_sources_ctr]['rate'] 		= $current_video_source->encoding_rate;
            		$wb_video_sources_array[$wb_video_sources_ctr]['width'] 	= $current_video_source->width;
            		$wb_video_sources_array[$wb_video_sources_ctr]['height'] 	= $current_video_source->height;
            		$wb_video_sources_ctr++;
            	}            	
            }
            
            //sort result
       		function wb_usort($a,$b){
        		return ($a["size"] >= $b["size"]) ? -1 : 1;
        	}
        	usort($wb_video_sources_array, "wb_usort");
        	
        	if ( $wb_video_sources_array ){
                echo '<p>Right-click and select &quot;Save As&quot;</p>';
            	$wb_download_source_count = 0;
            	$wb_download_desc = "High";
            	echo "<div><ul>";
            	foreach ( $wb_video_sources_array as $wb_current_source ){
            		if ( $wb_download_source_count == 3 )
            			break;
            		$size = round($wb_current_source['size']/1024/1024, 1);
            		$rate = $wb_current_source['rate']/1000;
            		if ( $wb_download_source_count == 0 ){
            			$wb_download_desc = "High";
            		}elseif( $wb_download_source_count == 1 ){
            			$wb_download_desc = "Medium";
            		}else{
            			$wb_download_desc = "Low";
            		}
            		echo '<li><a href="' . $wb_current_source['url'] . '" download onclick="';
            		
            		foreach ($analytics as $key => $tracker) { 
            			if( trim($tracker['code']) != '' ){
            				echo 'ga(\'pageTracker'.$key.'.send\', \'event\', \'Videos\', \'Download\', \''.$video['title'].' | ' . $wb_download_desc . '\');';
            			}
            		}
            		echo '">' . $wb_download_desc . ' <span>(' . $wb_current_source['width'] . 'x' . $wb_current_source['height'] . ', ' . $size . 'MB, ' . $rate . 'kbps)</span></a></li>';
            		$wb_download_source_count++;
            	}
            	echo "</ul></div>";
            }else{
                echo '<p>Sorry, no downloads available.</p>';
            }
          	?>
    </div>
    <?php
}
?>