<?php
/*
Filename:      vid_topic.php                                                 
Description:   will select the categories and their subcategories and display them
Author:        				
*/

//error_reporting(E_ALL);

if (!isset($postCategories) || count($postCategories) <= 0) {
    $postCategories = wb_get_categories();
}


?>
<style>
.topicList{
	list-style: none;	
}
.topicList a{
	color: #000;	
}
#categories h5{
	color: #000000;	
}
#categories .subs-h{
	color: #000;	
}
#categories a:hover, #categories h5:hover{
	color : #FF0000 ;
}
#vid_topic li.parent h5{
	text-align: left;
	background-position: right center !important	;
	margin-bottom: 0px;	
   padding: 5px 20px;
}
#vid_topic .subTopicList{
	text-align: left;
	/*background: url("http://mhedappv.wbtvserver.com//images/arrow.gif") no-repeat scroll 10px top transparent;*/
	list-style-type: none;
	/*margin-left: 40px !important;*/
}
#vid_topic .subTopicList a{ 
	font-weight: 600;
	/*margin-right: 70px;*/
}
#vid_topic li.parent{
	 padding: 0;
}
#vid_topic .subTopicList{
	background-position: 10px !important;
	padding: 3px;
}
#vid_topic li.parent{
	list-style-type: none !important;
}
#categories ul li {
    margin-left: 0px;
    width: 100%;
}
#categories ul {
    margin-top: 0px;
    margin-bottom: 0px;
}
#vid_topic {
    background: url("/wp-content/themes/enterprise/images/bg_box.gif") repeat-x scroll left top #FFFFFF;
    //border: 1px solid #EEEEEE;
    margin: 0;
    padding: 0;
    text-align: left;
    width: 300px;
}
</style>
       <script type="text/javascript">
      /*
      This script controls the styling and functionality of the category/topic box on the sidebar(includes/box/vid_topic.php)
      */
      (function( $ ) {      
         $('#categories li ul').hide();
         <?php
         if($wb_ent_options['catlisttype'] == 'image'){
            $ulMargin = '85px';
            $arrowBgX = '5px';
            $arrowDownBgX = '4px';
            $arrowDownBgY = '4px';
         }
         else{
            $ulMargin = '15px';
            $arrowBgX = 'left';
            $arrowDownBgX = '10px';
            //$arrowDownBgY = 'top';
         }
         ?>
         
         //$('#categories li ul').css('margin-left', '<?php echo $ulMargin; ?>');
         
         //$("#categories h5").css('background','url("<?php echo get_site_url();?>/images/arrow.gif") <?php echo $arrowBgX; ?> center no-repeat');
         $("#categories .subCat h5").css('background','none');
         $("#categories h5").css('margin','0');
         
         //$("#categories h5").css('padding-left','25px');
         
         $('#categories h5').click(function() {
            if ($(this).next("#categories ul").is(":hidden")){
               //$(this).css('background','url("<?php echo get_site_url();?>/images/arrow-down.gif") <?php echo $arrowDownBgX.' '.$arrowDownBgY  ?>  no-repeat');
               $(this).css('background','url("/wp-content/themes/enterprise/images/bg_box.gif") repeat-x scroll left top #FFFFFF');
							 $(this).next("#categories ul").show();
						 }
            else{
               //$(this).css('background','url("<?php echo get_site_url();?>/images/arrow.gif") <?php echo $arrowBgX; ?> center no-repeat');
               $(this).css('background','url("/wp-content/themes/enterprise/images/bg_box.gif") repeat-x scroll left top #FFFFFF');
               //$(this).css('background','url("<?php echo get_site_url();?>/wp-content/themes/enterprise/images/bg_box.gif") <?php echo $arrowBgX; ?> center no-repeat');
							 $(this).next("#categories ul").hide();
						 }
               
            //$(this).next("#categories ul").slideToggle("fast");
         });
         
         $('#categories ul ul li').click(function() {
            //$('#pageContents').fadeOut('slow');
            $('#vid_list').slideDown("slow");
            $('#video_list').fadeOut("slow");
            
         });
         $('#categories ul li').hover(function() {
            $(this).css('cursor','pointer');
         });
         $('#categories ul ul li').hover(function() {
            //$(this).css({'cursor':'pointer', 'color':'#016891', 'padding-left':'8px',  'text-decoration':'none'});
         });
         $('#categories ul ul li').mouseout(function() {
            //$(this).css({'cursor':'arrow', 'color':'#000000', 'padding-left':'8px', 'text-decoration':'none'});
         });         

      })( jQuery );
      </script>
<?php

if($wb_ent_options['catlisttype'] == 'image'){
?>
<style>
#categories img{
	float: left;	
	display: block;
}

#categories h5 {
    display: block;
    margin: 0;
    width: 300px;
    font-size: 16px;
    font-weight: bold;   
    color:#362f25;
}
#categories h5:hover, #categories h5 a:hover, #vid_topic li.parent ul li:hover, #vid_topic li.parent ul li a:hover{
	color: #A19273;
}

#categories h5 a{
	padding: 0px;
}




#vid_topic li.parent {
	min-height: 30px;
}

#vid_topic li.parent ul{
    display: block;
    margin-left: 0px;
    margin-top: 0px;
}

#vid_topic li.parent ul li{
	list-style: none;	
	font-size: 12px;
	font-weight: bold;
	margin-bottom: 0px;
}

#vid_topic li.parent ul li .subCatCount{
	color: #E3852B;
}

</style>
<?php
}
//will check if there are posts for each category or subcategory
echo "<!-- split -->";
if(count($postCategories) > 0){  
  echo "<h5 class='subs-h' style='margin-bottom:5px;margin-top:5px;font-weight:bold'>";
  _e('Video Libraries', 'enterprise');
  echo "</h5>";
  foreach ($postCategories as $parent) { 
    $id = $parent['cat_id']; ?>
    <div class="vid_topic marginleft visible-desktop btnwb btn-block" style="margin-bottom:15px;padding:2px 12px;background: url('/wp-content/themes/enterprise/images/bg_box.gif') repeat-x scroll left top #FFFFFF;">
      <div class="categories">
        <?php
        echo '';
				if($wb_ent_options['catlisttype'] == 'image'){
					echo '<img src="'.get_site_url().'/images/thumbs/'.$parent['cat_slug'].'_cat.jpg" />';
				}

            $categoryPermalink = get_site_url().'/category/'.$parent['cat_slug'];
				echo '<a href="'.$categoryPermalink.'" style="text-decoration: none;"><li class="parent topicList"><h5 class="subs-h">';
        printf(__('%s', 'enterprise'), $parent['cat_name']);
        echo '</h5>'.PHP_EOL;
				echo '</li></a>'.PHP_EOL;	        
        ?>
      </div>
    </div>
<?php  }
} 
?>