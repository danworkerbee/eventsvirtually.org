<?php
/**
 * filename: player.php
 * description: this will display the actual code for the player
 * author: Jullie Quijano
 * date created: 2014-03-27
 */
global $wb_ent_options, $postId;
$postId = $wp_query->post->ID; 
$currentCast = wb_get_most_recent($wb_ent_options['videocats']['webcast']);
$featureVideo = wb_get_most_recent($wb_ent_options['videocats']['feature']);
$mostRecentVideo = wb_get_most_recent($wb_ent_options['videocats']['library']);


$wb_get_info = wb_get_video_cuepoints( $video['mediaId'] );

$hasCuePoints = false;
if( is_array( $wb_get_info ) && count( $wb_get_info ) > 0 ){
	$hasCuePoints = true;
}

if( $video['status'] == 'draft' || (isset($_GET['preview']) && $_GET['preview'] == "true") ){
			 $playerId = $wb_ent_options['bcplayers']['preview'];
}                
elseif( $hasCuePoints ){
			 $playerId = $wb_ent_options['bcplayers']['invidad'];
}                
else if($wb_ent_options['channelformat'] == 'feature'){

	 if ( $postId==$featureVideo ) {    
			$playerId = $wb_ent_options['bcplayers']['feature'];
	 }  
	 else {
			$playerId = $wb_ent_options['bcplayers']['library'];
	 }

}
else if($wb_ent_options['channelformat'] == 'webcast'){

	 if ( $postId==$currentCast ) { 
			$playerId = $wb_ent_options['bcplayers']['webcast'];
	 } else if ( $postId == $featureVideo ) {
			$playerId = $wb_ent_options['bcplayers']['feature'];
	 } 
	 else { 
			$playerId = $wb_ent_options['bcplayers']['library'];
	 }  

}
else{

	 $playerId = $wb_ent_options['bcplayers']['library'];

}

$wb_meta_poster = get_post_meta( $postId, 'wb_ppv_prod_video_still', true );
$wb_poster = 'poster="'.$wb_meta_poster.'"';

?>
<!-- start of player -->
<div id="playerDiv">
	<div id="player">
		<div style="display: block; position: relative; max-width: 100%;">
			<div style="display: block; padding-top: 56.25%;">
				<video id="WBplayer"  playsinline <?php echo $wb_poster; ?> data-video-id="<?php echo $video['mediaId']; ?>" data-account="<?php echo $wb_ent_options['brightcoveinfo']['publisherid']; ?>" data-player="<?php echo $playerId; ?>" data-embed="default" class="video-js" controls="" style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;"></video>
				<script src="//players.brightcove.net/<?php echo $wb_ent_options['brightcoveinfo']['publisherid']; ?>/<?php echo $playerId; ?>_default/index.min.js"></script>
			</div>
		</div>			        
	</div>
</div>
<?php 
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); //DO NOT forget id="WBplayer" to <video>
	if (is_plugin_active('workerbee-annotation/workerbee-annotation.php')){
		include(annotationPATH . "includes/annotation_APFplayer_inc.php");
	}
?>
<!-- end of player -->
