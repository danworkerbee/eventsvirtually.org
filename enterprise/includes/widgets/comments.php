<?php
/*
Filename:      comments.php                                                 
Description:   this include will display the comments section on the page
Author:        Jullie Quijano                                                                                                                                                                                                    
*/


if( !strstr($_SERVER['HTTP_USER_AGENT'],'MSIE 8') ){
   
?>
<div id="com_trans" class="visible-desktop">
	  <div class="heading4" id="comments" style="white-space:normal !important;"><a name="comment">Questions, comments, suggestions?</a></div>
	  <!-- START OF: disqus code -->
    	<div id="disqus_thread" style="margin-left:-5px; padding: 10px;"></div>
		<script type="text/javascript">
			 /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
			 var disqus_shortname = '<?php echo $wb_ent_options['disqussname']; ?>'; // required: replace example with your forum shortname
			 var disqus_identifier = '<?php echo ($video['postLink']) ?  $video['postLink'] :  get_site_url().'/'; ?>' ;
			 var disqus_url = '<?php echo ($video['postLink']) ? $video['postLink'] : get_site_url().'/'; ?>' ;
		
			 /* * * DON'T EDIT BELOW THIS LINE * * */
			 (function() {
				  var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
				  dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
				  (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
			 })();
		</script>
		<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
		<a href="http://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a>	  	  
	  <!-- END OF: disqus code -->
</div>
<?php
}

?>