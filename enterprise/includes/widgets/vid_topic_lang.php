<?
/*
Filename:      vid_topic.php                                                 
Description:   will select the categories and their subcategories and display them
Author:        
Change Log:
	August 23, 2011	[Jullie]added code for displaying images beside the categories;
	July 7, 2011	[Jullie]Removed sql queries and included list_categories.php instead;
	June 20, 2011	[Jullie]Modified css to make the category listing black
	June 15, 2011  [Ronald Iraheta] Added css for hover on the category links
	June 15, 2011  [Jullie]Modified how categories are to be displayed. The categories will be displayed as a link if 
						they do not have any sub categories. Other wise, it will expand when clicked on and display their sub categories.
   June 13, 2011  [Jullie]Modified the first sql statement to only select categories under the 'Library'
 
 						
*/
//error_reporting(E_ALL);
include 'resources/list_categories_lang.php';
?>
		<script type="text/javascript">
		/*
		This script controls the styling and functionality of the category/topic box on the sidebar(includes/box/vid_topic.php)
		*/
		$(function(){
		
			$('#categoriesLang li ul').hide();
			<?php
			if(HAS_TOPIC_LIST_IMG){
				$ulMargin = '85px';
				$arrowBgX = '5px';
				$arrowDownBgX = '4px';
				$arrowDownBgY = '4px';
			}
			else{
				$ulMargin = '15px';
				$arrowBgX = 'left';
				$arrowDownBgX = '10px';
				$arrowDownBgY = 'top';
			}
			?>
			
			$('#categoriesLang li ul').css('margin-left', '<?php echo $ulMargin; ?>');
			
			//$("#categoriesLang h5").css('background','url("<?php echo SITE_ROOT;?>/images/arrow.gif") <?php echo $arrowBgX; ?> center no-repeat');
			$("#categoriesLang h5").css('padding-left','25px');
			
			$('#categoriesLang h5').click(function() {
				if ($(this).next("#categoriesLang ul").is(":hidden")){
					$(this).css('background','url("<?php echo SITE_ROOT;?>/images/arrow-down.gif") <?php echo $arrowDownBgX.' '.$arrowDownBgY  ?>  no-repeat');
					$(this).next("#categoriesLang ul").show();
				}
				else {
					$(this).css('background','url("<?php echo SITE_ROOT;?>/images/arrow.gif") <?php echo $arrowBgX; ?> center no-repeat');
					$(this).next("#categoriesLang ul").hide();
				}
					
				//$(this).next("#categoriesLang ul").slideToggle("fast");
			});
			
			$('#categoriesLang ul ul li').click(function() {
				$('#pageContents').fadeOut('slow');
				$('#vid_list').slideDown("slow");
				$('#video_list').fadeOut("slow");
				
			});
			$('#categoriesLang ul li').hover(function() {
				$(this).css('cursor','pointer');
			});
			$('#categoriesLang ul ul li').hover(function() {
				$(this).css({'cursor':'pointer', 'color':'#016891', 'padding-left':'0px',  'text-decoration':'none'});
			});
			$('#categoriesLang ul ul li').mouseout(function() {
				$(this).css({'cursor':'arrow', 'color':'#000000', 'padding-left':'0px', 'text-decoration':'none'});
			});
		});
		</script>
<style>

#vid_topic_lang {
    background: url("/images/bg_box.gif") repeat-x scroll left top #DDDDDD;
    //border: 1px solid #EEEEEE;
    margin: 0 auto 10px;
    padding: 10px 0 0;
    text-align: left;
    max-width: 298px;
}
#vid_topic_lang li.parent {
    background: url("../images/bg_box.gif") repeat-x scroll left top #DDDDDD;
    border-bottom: 1px solid #DDDDDD;
    margin: 0 auto;
    padding: 8px;
    text-align: left;
}

#categoriesLang ul {
    margin-bottom: 10px;
}
#categoriesLang ul li {
    margin-left: 25px;
    max-width: 258px;
}

.topicList{
	list-style: none;	
}
.topicList a{
    color: #000000;
    display: block;
    width: 200px;
    margin-top: 5px;
}
#categoriesLang h5{
	color: #000;	
}

#categoriesLang a:hover, #categoriesLang h5:hover
{
	color : #016869 ;
}
</style>

<?php

if(HAS_TOPIC_LIST_IMG){
?>
<style>
#categoriesLang img{
	float: left;	
	display: block;
}

#categoriesLang h5 {
    display: block;
    margin: 18px 0 0 85px;
    width: 140px;
    font-size: 16px;
    font-weight: bold;   
    color:#362f25;
}
#categoriesLang h5:hover, #categoriesLang h5 a:hover, #vid_topic li.parent ul li:hover, #vid_topic li.parent ul li a:hover{
	color: #A19273;
}

#categoriesLang h5 a{
	padding: 0px;
}

#vid_topic li.parent {
	min-height: 55px;
}

#vid_topic li.parent ul{
    display: block;
    margin-left: 85px;
    margin-top: 20px;
}

#vid_topic li.parent ul li{
	list-style: none;	
	font-size: 12px;
	font-weight: bold;
	margin-bottom: 4px;
}

#vid_topic li.parent ul li .subCatCount{
	color: #E3852B;
}
</style>
<?php
}
//will check if there are posts for each category or subcategory
if($catNumRows > 0){
?>	
	<div id="vid_topic_lang" class="marginleft btnwb btn-block">
	<h4><?= _e('Browse Videos by Language', 'enterprise') ?></h4>
	<div id="categoriesLang">

	<ul>
	<?php
		foreach ($postOtherLangCats as $parent) {
			$id = $parent['cat_id'];
                
        if( $parent['cat_name'] == 'India: English' ){
          continue;
        }             
     
      
      
			
			//if the subCat exists for this parent category, it means that subCat has posts associated with it
			if($parent['subCatNumRows'] > 0){
				
				echo '<li class="parent topicList">';
				
				if(HAS_TOPIC_LIST_IMG){
					echo '<img src="'.SITE_ROOT.'images/thumbs/'.$parent['cat_slug'].'_cat.jpg" />';
				}
        
         
        
				echo '<h5>';
        printf(__('%s', 'enterprise'), $parent['cat_name']);
        echo '</h5>'.PHP_EOL;
				echo '	<ul>'.PHP_EOL;
				
				foreach($parent['subCat'] as $currentSubCat){
					echo '		<li class="subTopicList"><a href="'.SITE_ROOT.'category/'.$currentSubCat['subCat_slug'].'">';
					printf(__('%s', 'enterprise'), $currentSubCat['subCat_name']);
					if(HAS_TOPIC_COUNT){
						echo ' <span class="subCatCount">('.$currentSubCat['subCat_count'];
						/*
						if($currentSubCat['subCat_count'] > 1){
							echo ' videos';
						}
						else{
							echo ' video';
						}*/
						echo ')</span>';
					}
					echo '</a></li>'.PHP_EOL;		
				}
				echo '	</ul>'.PHP_EOL.'</li>'.PHP_EOL;
				
			}
			else{ //if the subCat doesn't exist, it means the post is associated with the parent category. display parent category as a link
				echo '<li class="parent topicList">';
				
				if(HAS_TOPIC_LIST_IMG){
					echo '<img src="'.SITE_ROOT.'images/thumbs/'.$parent['cat_slug'].'_cat.jpg" />';
				}
				
				echo '<h5><a href="'.SITE_ROOT.'category/'.$parent['cat_slug'].'" style="text-decoration: none;">';
        printf(__('%s', 'enterprise'), $parent['cat_name']);
        echo PHP_EOL;
				echo '</a></h5></li>'.PHP_EOL;	
			}
			
			
		}
					
	?>
			</ul>
		</div>
</div>
<?php	
}

?>