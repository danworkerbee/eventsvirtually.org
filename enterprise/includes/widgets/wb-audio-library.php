<?php
/*
Filename:      wb-audio-library.php                                                 
Description:   will select the categories and their subcategories and display them
Author:        				
*/
//error_reporting(E_ALL);
global $wb_ent_options;
if (!isset($post_audio_categories) || count($post_audio_categories) <= 0) {
	$post_audio_categories = wb_get_audio_categories($wb_ent_options['videocats']['podcast'], 1);
}
?>

<style>.topicList{list-style:none}.topicList a{color:#000}#pod_categories h5{color:#000}#pod_categories .subs-h{color:#000}#pod_categories a:hover,#pod_categories h5:hover{color:red}#podcast_topic li.parent h5{text-align:left;background-position:right center!important;margin-bottom:0;padding:5px 5px 5px 0}#podcast_topic li.parent h5 a{padding:0 0 0 20px;width:auto;display:block}#podcast_topic li.nosubtopic h5{padding:5px 0 5px 20px}#podcast_topic li.nosubtopic h5 a{padding:0}#podcast_topic .subTopicList{text-align:left;list-style-type:none}#podcast_topic li.parent{padding:0}#podcast_topic .subTopicList{background-position:10px!important;padding:3px}#podcast_topic li.parent{list-style-type:none!important}#pod_categories ul li{margin-left:0;width:100%}#pod_categories ul{margin-top:0;margin-bottom:0}#podcast_topic{margin:0 auto;padding:0;text-align:left;max-width:300px;width:auto}li.sub-third-level a{display:block;padding:4px 0 4px 60px!important;font-weight:400;width:auto;margin:0}div#wb_ent_sidebar1 #side-nav #podcast_topic{margin:0}#podcast_topic li.parent ul li{list-style:none;font-size:12px;font-weight:700;margin-bottom:0}#podcast_topic li.parent ul li .subCatCount{color:#e3852b}#podcast_topic li.parent{background:#fff;border-bottom:1px solid #ddd;margin:0 auto;text-align:left}#side-nav #podcast_topic #pod_categories ul li ul li:last-child{border-bottom:0!important}#side-nav #podcast_topic .subTopicList a{padding-left:40px;font-weight:400}#side-nav #podcast_topic .subTopicList{padding:0}#side-nav #podcast_topic .subTopicList a{padding:5px 0 5px 40px}</style>
<script type="text/javascript">
	/*
    	This script controls the styling and functionality of the category/topic box on the sidebar(includes/box/podcast_topic.php)
    */
	(function( $ ) {
    	<?php if(!$wb_ent_options['catlisttype']['collapse']){ ?>
        	$('#podcast_topic #pod_categories li ul').hide();
		 	$('#podcast_topic #pod_categories .subs-h').addClass("collapsed_boxarrow");
       	<?php }else{ ?>
		  	$('#podcast_topic #pod_categories li ul li ul').hide();
		  	$('#podcast_topic #pod_categories .subs-h').addClass("expanded_boxarrow");
		<?php } 
         	if($wb_ent_options['catlisttype'] == 'image'){
            	$ulMargin = '85px';
            	$arrowBgX = '5px';
            	$arrowDownBgX = '4px';
            	$arrowDownBgY = '4px';
         	}else{
            	$ulMargin = '15px';
            	$arrowBgX = 'left';
            	$arrowDownBgX = '10px';
         	}
         ?>
         $("#podcast_topic #pod_categories .subCat h5").css('background','none');
         $("#podcast_topic #pod_categories h5").css('margin','0');                  
         //$('#pod_categories .subs-h').addClass("collapsed_boxarrow");
         $('#podcast_topic #pod_categories .subs-h').click(function() {
					 if ($(this).next("ul").is(":hidden")){
						 $(this).next("ul").slideDown();
					 }
					 else{
						 $(this).next("ul").slideUp();
					 }
            //$(this).next("#pod_categories ul").slideToggle("fast");
             $('#podcast_topic #pod_categories  .subs-h').toggleClass("collapsed_boxarrow expanded_boxarrow");
         });
         $('#podcast_topic #pod_categories .topicList h5').click(function() {
							if ($(this).next("#podcast_topic #pod_categories .topicList ul").is(":hidden")){
								$(this).next("#podcast_topic #pod_categories .topicList ul").slideDown();
							}
							else{
								$(this).next("#podcast_topic #pod_categories .topicList ul").slideUp();
							}
              //$(this).next("#pod_categories .topicList ul").slideToggle("fast");
              $(this).children('.iconwb').toggleClass("icon-plus icon-minus");
         });
         $('#podcast_topic #pod_categories ul ul li').click(function() {            
            $('#vid_list').slideDown("slow");
            $('#video_list').fadeOut("slow");
            
         });
         $('#podcast_topic #pod_categories .topicList h5').mouseover(function(){
            $(this).css('color', '#ffffff');
            $(this).children('i').addClass('icon-white');
            $(this).children('a').css('color', '#ffffff');
         });
         $('#podcast_topic #pod_categories .topicList h5').mouseout(function(){
             $(this).css('color', '#333');
             $(this).children('i').removeClass('icon-white');
             $(this).children('a').css('color', '#333');
         });        
         $('#podcast_topic #pod_categories ul li').hover(function() {
            $(this).css('cursor','pointer');
         });
         $('#podcast_topic #pod_categories ul ul li').hover(function() {
            //$(this).css({'cursor':'pointer', 'color':'#016891', 'padding-left':'8px',  'text-decoration':'none'});
         });
         $('#podcast_topic #pod_categories ul ul li').mouseout(function() {
            //$(this).css({'cursor':'arrow', 'color':'#000000', 'padding-left':'8px', 'text-decoration':'none'});
         });         
      })( jQuery );
</script>
<?php
	if($wb_ent_options['catlisttype'] == 'image'){
?>
	<style>#pod_categories img{float:left;display:block}#pod_categories h5{display:block;margin:0;width:300px;font-size:16px;font-weight:700;color:#362f25}#pod_categories h5 a:hover,#pod_categories h5:hover,#podcast_topic li.parent ul li a:hover,#podcast_topic li.parent ul li:hover{color:#a19273}#pod_categories h5 a{padding:0}#podcast_topic li.parent{min-height:30px}#podcast_topic li.parent ul{display:block;margin-left:0;margin-top:0}</style>
<?php
	}

	//will check if there are posts for each category or subcategory
	if(count($post_audio_categories) > 0){
	?>	
		<div id="podcast_topic" class="wbc-sidemenu-style marginleft <?php echo ($wb_ent_options['hascatlist']['mobileview']) ? 'visible-desktop' : ''; ?>  btnwb btn-block">
			<div id="pod_categories">
				<ul>
					<li class="" style="padding-bottom: 0px; cursor: pointer; padding: 0px;" >
						<h5 class="subs-h" style=""><?= __('Podcast Library', 'enterprise') ?></h5>
						<ul style="display: block; " >
						<?php
							foreach ($post_audio_categories as $parent) {
								$id = $parent['cat_id'];
								//if the subCat exists for this parent category, it means that subCat has posts associated with it
								if($parent['subCatNumRows'] > 0 && ( $parent['cat_name'] != "MHEDA University" && $parent['cat_id'] != 235 ) ){
									echo '<li class="parent topicList subCat">';
									if($wb_ent_options['catlisttype'] == 'image'){
										echo '<img src="'.get_site_url().'/images/thumbs/'.$parent['cat_slug'].'_cat.jpg" />';
									}
                                	$categoryPermalink = get_site_url().'/category/'.$parent['cat_slug'];
		        				?>
		        					<h5 style="padding-left: 20px;"><?php printf(__('%s', 'enterprise'), $parent['cat_name']); ?><i class="icon-plus iconwb" style="float:right;margin-top:5px;"></i></h5><?= PHP_EOL ?>
		       					<?php				
									echo '	<ul>'.PHP_EOL;
									foreach($parent['subCat'] as $currentSubCat){
										$currentsubcat_count++;
										$topics = get_categories( array( 'child_of' => $currentSubCat['subCat_id'], 'orderby' => 'name' ) );
										if ($currentSubCat['subCat_count'] > 0) {
											if( count($topics) > 0 ){
												echo '<li class="parent topicList subCat" style="margin-bottom: -1px;">';
												$categoryPermalink = get_site_url().'/category/'.$parent['cat_slug'];
												?>
									        		<h5 style="padding-left: 40px;"><?php printf(__('%s', 'enterprise'), $currentSubCat['subCat_name']); ?><i class="icon-plus iconwb" style="float:right;margin-top:5px;"></i></h5><ul><?= PHP_EOL ?>
												<?php 
												foreach( $topics as $current_topic ){
													echo '<li class="sub-third-level" style="list-style-type: none; display: block;"><a href="' . get_site_url().'/category/' . $current_topic->slug . '">';
													printf( __('%s', 'enterprise'), $current_topic->name);
													echo '</a></li>';
												}
												echo '<li class="sub-third-level"><a href="'.get_site_url().'/category/'.$currentSubCat['subCat_slug'].'"  style="line-height: 20px; display:block;"  >View All</a></li>';
												echo "</ul></li>";	
											} else {
												echo "<!-- no sub topic " . $current_topic->name . " -->";
												echo '<li class="subTopicList" style="cursor: pointer;"><a href="'.get_site_url().'/category/'.$currentSubCat['subCat_slug'].'" style="line-height: 20px; display:block;">' .$currentSubCat['subCat_name'].'</a></li>';
											}
										} else {
											echo '<li class="subTopicList"><a href="'.get_site_url().'/category/'.$currentSubCat['subCat_slug'].'"  style="line-height: 20px; display:block;"  >';
											printf(__('%s', 'enterprise'), ltrim($currentSubCat['subCat_name'], '0')); //updated to remove 0 from left of string, sorting purposes.
											echo '</a></li>'.PHP_EOL;	
										}
									}
									if($currentsubcat_count > 0){
										echo '<li class="subTopicList" style="cursor: pointer;"><a href="'.get_site_url().'/category/'.$parent['cat_slug'].'" style="line-height: 20px; display:block;">View All</a></li>';
									}
									$currentsubcat_count = 0;
									echo '	</ul>'.PHP_EOL.'</li>'.PHP_EOL;
								}else{ //if the subCat doesn't exist, it means the post is associated with the parent category. display parent category as a link
									echo '';
									if($wb_ent_options['catlisttype'] == 'image'){
										echo '<img src="'.get_site_url().'/images/thumbs/'.$parent['cat_slug'].'_cat.jpg" />';
									}
            						$categoryPermalink = get_site_url().'/category/'.$parent['cat_slug'];
									echo '<li class="parent nosubtopic topicList"><h5><a href="'.$categoryPermalink.'" style="text-decoration: none;">';
        							printf(__('%s', 'enterprise'), $parent['cat_name']);
        							echo '</a></h5>'.PHP_EOL;
									echo '</li>'.PHP_EOL;	
								}
							}
    
    						if($wb_ent_options['hascat']['ondemand']){
      							echo '<a href="'.$wb_ent_options['hascat']['ondemandurl'].'" style="text-decoration: none;"><li class="parent topicList" style="cursor: pointer;"><h5 style="margin: 0px;">';
      							printf(__('%s', 'enterprise'), $wb_ent_options['hascat']['ondemandtitle']);
      							echo '</h5></li></a>';
    						}
						?>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	<?php	
		}
	?>