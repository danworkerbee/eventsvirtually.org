<?php
/*
 Filename:		videolistwidget.php
 Description:	will display tabbed list of videos
 Author:        Jullie Quijano
 */


global $postId;
$wb_tester = $_GET['wb'];
//echo 'videolistwidget $postId is '.$postId;

//error_reporting(E_ALL);
//check the kind of page calling this include
$current_page = explode('/', $_SERVER['REQUEST_URI']);
$main_script = explode('?', $current_page[1]);



//check if there are previous/latest posts
/*
 $query = sprintf("SELECT *
 FROM wp_posts
 WHERE post_type = 'post'
 AND post_status = 'publish'
 ");
 $result = mysql_query($query) or die(mysql_error());
 $allPostsCount = mysql_num_rows($result);
 */
/*
 include '/resources/list_related.php';
 include '/resources/list_popular.php';
 include '/resources/list_archive.php';
 */

$relatedVideoInfo = wb_get_related_vids($postId, $wb_ent_options['videolistwidget']['videocount']);
$relatedVideo = $relatedVideoInfo['posts'];

$archiveVideoInfo = wb_get_archive_vids(0, false, $wb_ent_options['videolistwidget']['videocount']);
$archiveVideo = $archiveVideoInfo['posts'];

$popularVideoInfo = wb_get_popular_vids('', false, $wb_ent_options['videolistwidget']['videocount']);
$popularVideo = $popularVideoInfo['posts'];


/*
 if ($wb_tester != ""){
 echo "<pre>";
 print_r($relatedVideo);
 echo "</pre>";
 }
 */

if ( ( $wb_ent_options['videolistwidget']['hasrelated'] && $relatedVideoInfo['numPosts'] > 0) || ( $wb_ent_options['videolistwidget']['hasarchive'] && $archiveVideoInfo['numPosts'] > 0 ) || ( $wb_ent_options['videolistwidget']['haspopular'] && $popularVideoInfo['numPosts'] > 0)) {
	?>

<script type="text/javascript">
    $(function() {
        $("#tabs").tabs();
        $("#relatedTab").css('backgroundColor', '#ffffff');
    });
</script>
<style>
    #video_list_main{
        overflow-x: hidden;
        overflow-y: hidden;
        padding: 0px;
        height: 417px;
        margin-bottom: 10px;
        margin-left: 0;
    }
    #video_list_main div div{
        padding: 10px 0px;
    }
    #video_list_main div div ul{
        max-width: 620px;
    }

    #video_list_main div div ul li{
        max-width: 285px;
        min-width: 285px;
        height: 180px;
        float: left;
        clear: none;
        padding: 15px 2px;
        margin: 0 6px;
    }

    #video_list_main .ui-tabs .ui-tabs-nav li{
        padding: 0px;
        border: 1px solid #D3D3D3;
        /*
        -webkit-border-top-left-radius: 4px;
        -webkit-border-top-right-radius: 4px;
        -moz-border-radius-topleft: 4px;
        -moz-border-radius-topright: 4px;
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
        */
    }
    .ui-corner-top{
        border-radius: 0;
    }
    .ui-tabs .ui-tabs-nav li a {
        padding: 5px 4px;
    }
    #nav-tabs-ul {
        /** width: 270px; **/
    }
    #mostViewed li{
        min-height: 75px;
    }

    #related {
        height: 364px;
        overflow-x: hidden;
        overflow-y: auto;
    }

    #latest {
        height: 364px;
        overflow-x: hidden;
        overflow-y: auto;
    }

    #mostViewed {
        height: 364px;
        overflow-x: hidden;
        overflow-y: auto;
    }
    .list img{
        width: auto;        
    }
    
    /*  border inside video listings */
ul#archive_list li,
ul#mostViewed_list li,
ul#related_list li{
	border: none !important;
}  

/* remove double below tabs of video listing */
.nav-tabs{
	border-bottom: none;
}

/* remove duplicate borders of tabs main */

#nav-tabs-ul > li.active > a, #nav-tabs-ul > li.active > a:focus, #nav-tabs-ul > li.active > a:hover {
    color: #555;
    cursor: default;
    background-color: #FFF;
    border-width: 0px;
    border-style: none;
    border-color: none;
    -moz-border-top-colors: none;
    -moz-border-right-colors: none;
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    border-image: none;
}
  #nav-tabs-ul > .ui-state-active::after{
    border-color: transparent;
    border-image: none;
    border-style: none;
    border-width: 0;
    background-color: #fff !important;
   }  
   #nav-tabs-ul > li > a{
    border-radius: 0;
   }
   #nav-tabs-ul > li > a:hover{
    /* color: #555; */
   }
#video_list_main #nav-tabs-ul > li {
    background-color: #F9F9F9 !important;
    border: 1px solid #ebebeb;
}
#wbc-nav-div {

}
/* spaces between tabs (related, recent, most viewed) */
#nav-tabs-ul{
    border: 0;
}
#nav-tabs-ul > li > a {
    margin-right: -1px;
	margin-left: -1px;
    line-height: 1.42857;
    border: 0px;
    border-radius: 0px 0px 0px 0px;
    background: none;
    color: #000;
    text-transform: uppercase;
}

#video_list_main #nav-tabs-ul > .ui-state-active > a,
#video_list_main #nav-tabs-ul > .ui-state-active > a:hover,
#video_list_main #nav-tabs-ul > li > a:focus{
    background-color: #FFF;
    color: #000; 
	border-left: 1px solid #EBEBEB;
	border-right: 1px solid #EBEBEB;
	border-bottom: none;
}

#video_list_main #nav-tabs-ul > li > a:hover {
    background-color: #2c5898;
    border-color: #FFF #FFF transparent;
    color: #FFF;
}
/*tab border */ 

#video_list_main #nav-tabs-ul > li {
    background-color: #F9F9F9;
    border-bottom: none;   
}

#nav-tabs-ul > li, .ui-tabs .ui-tabs-panel{
    border: 1px solid #ebebeb;
}

</style>
    <div id="video_list_main" class="visible-desktop gradBox list span12">
        <div id="tabs">		
            <ul id="nav-tabs-ul">					
                <?php
                if ($wb_ent_options['videolistwidget']['hasarchive'] && $archiveVideo ) {
                    ?>			
                    <li><a href="#latest">Recent</a></li>
                    <?php
                }
                if ( $wb_ent_options['videolistwidget']['hasrelated'] && count($relatedVideo) > 0) {
                    ?>			
                    <li id="relatedTab"><a href="#related">Related</a></li>
                    <?php
                }
                if ($wb_ent_options['videolistwidget']['haspopular'] && $popularVideo ) {
                    ?> 
                    <li id="mostViewedTab" style="margin-right: 0px;"><a href="#mostViewed"> Most Viewed</a></li>
                        <?php
                    }
                    ?>
            </ul>

            <?php
             //'$archiveVideoInfo[numPosts] is' . $archiveVideoInfo['numPosts'];
            if ( $wb_ent_options['videolistwidget']['hasarchive'] && $archiveVideoInfo['numPosts'] > 0) {
                echo '
	<div id="latest">
		<ul id="archive_list">';

                $counter = 0;
                foreach ($archiveVideo as $video) {
                    //print_r($video);
                   if( $counter >= $wb_ent_options['videolistwidget']['videocount'] ){
                      break;
                   }                   
                    echo '
            <li';

                    if ($counter == 0 || $counter == 1) {
                        echo ' style="border-top: 0px;"';
                    }

                    echo '>
               <a href="' . $video['postLink'] . '">';
                    if ($video['largeThumb'] == '' || $video['largeThumb'] == 'nothing') {
                        echo '
                  <img src="' . $wb_ent_options['defaultsmlthumb'] . '" alt="" />';
                    } else {
                        echo '
                  <img src="' . $video['largeThumb'] . '" alt="" />';
                    }
                    echo '
                  <h5>' . wb_format_string($video['title'], false, true, $wb_ent_options['videolistinfo']['titlelimit'], '...') . '</h5>
               </a>';
                    //echo '<p class="date">'.$date[0].'</p>';
                   // echo '
               //<p class="desc">' . wb_format_string($video['desc'], false, true, $wb_ent_options['videolistinfo']['desclimit'], '... <a href="' . $video['postLink'] . '" >[more]</a>') . '</p>';
                  //  echo '
               //<p class="tags">' . $video['taglisthtml'] . '</p>
                 echo '</li>';

                    $counter++;
                }

                echo '
		</ul>
	</div>';
            }
            
           // if ( $wb_ent_options['videolistwidget']['hasrelated'] && $relatedVideoInfo['numPosts'] > 0) {
            if ( $wb_ent_options['videolistwidget']['hasrelated'] && count($relatedVideo) > 0) {
            	
                echo '				
	<div id="related">
	
		
	
		<ul id="related_list">';
                $counter = 0;
                foreach ($relatedVideo as $video) {
                   if( $counter >= $wb_ent_options['videolistwidget']['videocount'] ){
                      break;
                   }
                    echo '
			<li';

                    if ($counter == 0 || $counter == 1) {
                        echo ' style="border-top: 0px;"';
                    }

                    echo '>
				<a href="' . $video['postLink'] . '">';
                    if ($video['largeThumb'] == '') {
                        echo '
					<img src="' . $wb_ent_options['defaultsmlthumb']. '" alt="' . $video['title'] . '" />';
                    } else {
                        echo '
					<img src="' . $video['largeThumb'] . '" alt="' . $video['title'] . '" />';
                    }
                    echo '
					<h5>' . wb_format_string($video['title'], false, true, $wb_ent_options['videolistinfo']['titlelimit'], '...') . '</h5>
				</a>';
                    //echo '<p class="date">'.$date[0].'</p>';
                  //  echo '
		//		<p class="desc">' . wb_format_string($video['desc'], false, true, $wb_ent_options['videolistinfo']['desclimit'], '... <a href="' . $video['postLink'] . '" >[more]</a>') . '</p>';
                    //echo '
			//	<p class="tags">' . $video['taglisthtml'] . '</p>
			echo '</li>';

                    $counter++;
                }


                echo '
		</ul>';
                echo '
	</div>';
            }

           

            if ( $wb_ent_options['videolistwidget']['haspopular'] && $popularVideoInfo['numPosts'] > 0) {
                echo '
	<div id="mostViewed">
		<ul id="mostViewed_list">';


                $counter = 0;
               
                foreach ($popularVideo as $video) {
                   if( $counter >= $wb_ent_options['videolistwidget']['videocount'] ){
                      break;
                   }                   

                    if ($video['postId'] == $post_id) {
                        continue;
                    } else {

                        echo '
				<li';

                        if ($counter == 0 || $counter == 1) {
                            echo ' style="border-top: 0px;"';
                        }

                        echo '>
					<a href="' . $video['postLink'] . '">';
                        if ($video['largeThumb'] == '' || $video['largeThumb'] == 'nothing') {
                            echo '
						<img src="' . $wb_ent_options['defaultsmlthumb'] . '" alt="' . $currentVideo['title'] . '" />';
                        } else {
                            echo '
						<img src="' . $video['largeThumb'] . '" alt="' . $currentVideo['title'] . '" />';
                        }
                        echo '
						<h5>' . wb_format_string($video['title'], false, true, $wb_ent_options['videolistinfo']['titlelimit'], '...') . '</h5>
					</a>';
                        //echo '<p class="date">'.$date[0].'</p>';
                        //echo '
						//<p class="desc">' . wb_format_string($video['desc'], false, true, $wb_ent_options['videolistinfo']['desclimit'], '... <a href="' . $video['postLink'] . '" >[more]</a>') . '</p>';
                        //echo '
						//<p class="tags">' . $video['taglisthtml'] . '</p>
				echo '</li>';

                        $counter++;
                    }
                }


                echo '
		</ul>
	</div>';
            }
            echo '			
	</div>';
            echo '</div>';
            ?>		

            <?php
        }
        ?>
