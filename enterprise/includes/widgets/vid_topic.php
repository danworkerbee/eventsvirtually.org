<?php
/*
Filename:      vid_topic.php                                                 
Description:   will select the categories and their subcategories and display them
Author:        

 						
*/

//error_reporting(E_ALL);
if (!isset($postCategories) || count($postCategories) <= 0) {
    $postCategories = wb_get_categories();
}
?>
<style>
   
#vid_topic h4{
    padding-left: 15px; 
    padding-bottom: 10px;
}    
.topicList{
	list-style: none;	
}
.topicList a{
	color: #000;	
}
#categories h5{
	color: #000;	
}

#categories a:hover, #categories h5:hover
{
	color : #FF0000 ;
}
</style>
      <script type="text/javascript">
      /*
      This script controls the styling and functionality of the category/topic box on the sidebar(includes/box/vid_topic.php)
      */
      (function( $ ) {
      
         $('#categories li ul').hide();
         <?php
         if($wb_ent_options['catlisttype'] == 'image'){
            $ulMargin = '85px';
            $arrowBgX = '5px';
            $arrowDownBgX = '4px';
            $arrowDownBgY = '4px';
         }
         else{
            $ulMargin = '15px';
            $arrowBgX = 'left';
            $arrowDownBgX = '10px';
            $arrowDownBgY = 'top';
         }
         ?>
         
         $('#categories li ul').css('margin-left', '<?php echo $ulMargin; ?>');
         
         $("#categories h5").css('background','url("<?php echo get_site_url();?>/images/arrow.gif") <?php echo $arrowBgX; ?> center no-repeat');
         $("#categories h5").css('padding-left','25px');
         
         $('#categories h5').click(function() {
            if ($(this).next("#categories ul").is(":hidden")){
               $(this).css('background','url("<?php echo get_site_url();?>/images/arrow-down.gif") <?php echo $arrowDownBgX.' '.$arrowDownBgY  ?>  no-repeat');
							 $(this).next("#categories ul").show();
						}
            else{
               $(this).css('background','url("<?php echo get_site_url();?>/images/arrow.gif") <?php echo $arrowBgX; ?> center no-repeat');
							 $(this).next("#categories ul").hide();
						}
            //$(this).next("#categories ul").slideToggle("fast");
            
         });
         
         $('#categories ul ul li').click(function() {
            //$('#pageContents').fadeOut('slow');
            $('#vid_list').slideDown("slow");
            $('#video_list').fadeOut("slow");
            
         });
         $('#categories ul li').hover(function() {
            $(this).css('cursor','pointer');
         });
         $('#categories ul ul li').hover(function() {
           // $(this).css({'cursor':'pointer', 'color':'#016891', 'padding-left':'0px',  'text-decoration':'none'});
         });
         $('#categories ul ul li').mouseout(function() {
          //  $(this).css({'cursor':'arrow', 'color':'#000000', 'padding-left':'0px', 'text-decoration':'none'});
         });
      })( jQuery );
      </script>
<?php

if($wb_ent_options['catlisttype'] == 'image'){
?>
<style>
#categories img{
	float: left;	
	display: block;
}

#categories h5 {
    display: block;
    margin: 18px 0 0 85px;
    width: 140px;
    font-size: 16px;
    font-weight: bold;   
    color:#362f25;
}
#categories h5:hover, #categories h5 a:hover, #vid_topic li.parent ul li:hover, #vid_topic li.parent ul li a:hover{
	color: #A19273;
}

#categories h5 a{
	padding: 0px;
}

#vid_topic li.parent {
	min-height: 55px;
}

#vid_topic li.parent ul{
    display: block;
    margin-left: 85px;
    margin-top: 20px;
}

#vid_topic li.parent ul li{
	list-style: none;	
	font-size: 12px;
	font-weight: bold;
	margin-bottom: 4px;
}

#vid_topic li.parent ul li .subCatCount{
	color: #E3852B;
}
</style>
<?php
}
//will check if there are posts for each category or subcategory
if(count($postCategories) > 0){
?>	
	<div id="vid_topic" class="marginleft visible-desktop btnwb btn-block">
	<h4><?= _e('Browse by Category &amp; Topic', 'enterprise') ?></h4>
	<div id="categories">

	<ul>
	<?php
		foreach ($postCategories as $parent) {
			$id = $parent['cat_id'];
			
			//if the subCat exists for this parent category, it means that subCat has posts associated with it
			if($parent['subCatNumRows'] > 0){
				
				echo '<li class="parent topicList">';
				
				if($wb_ent_options['catlisttype'] == 'image'){
					echo '<img src="'.get_site_url().'/images/thumbs/'.$parent['cat_slug'].'_cat.jpg" />';
				}
				echo '<h5>';
        printf(__('%s', 'enterprise'), $parent['cat_name']);
        echo '</h5>'.PHP_EOL;
				echo '	<ul>'.PHP_EOL;
				
				foreach($parent['subCat'] as $currentSubCat){
					echo '		<li class="subTopicList"><a href="'.get_site_url().'/category/'.$currentSubCat['subCat_slug'].'">';
					printf(__('%s', 'enterprise'), $currentSubCat['subCat_name']);
					if(HAS_TOPIC_COUNT){
						echo ' <span class="subCatCount">('.$currentSubCat['subCat_count'];
						
						if($currentSubCat['subCat_count'] > 1){
							_e(' videos', 'enterprise');
						}
						else{
							_e(' video', 'enterprise');
						}
						echo ')</span>';
					}
					echo '</a></li>'.PHP_EOL;		
				}
				echo '	</ul>'.PHP_EOL.'</li>'.PHP_EOL;
				
			}
			else{ //if the subCat doesn't exist, it means the post is associated with the parent category. display parent category as a link
				echo '<li class="parent topicList">';
				
				if($wb_ent_options['catlisttype'] == 'image'){
					echo '<img src="'.get_site_url().'/images/thumbs/'.$parent['cat_slug'].'_cat.jpg" />';
				}
				
				echo '<h5><a href="'.get_site_url().'/category/'.$parent['cat_slug'].'" style="text-decoration: none;">';
        printf(__('%s', 'enterprise'), $parent['cat_name']);
        echo PHP_EOL;
				echo '</a></h5></li>'.PHP_EOL;	
			}
			
			
		}
					
	?>
			</ul>
		</div>
</div>
<?php	
}
?>