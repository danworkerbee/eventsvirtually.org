<?php
/**
 * filename: adsLeaderboardswCustom.php
 * description: will display custom ads for the 468x60 banner below the video
 * author: Lourice Capili
 * date created: 2017-01-13
 *
 */
global $postId, $wb_ent_options, $majorAdsLB, $categoryAdsLB, $postAdsLB, $sitewideAdsLB, $included_bannersLB, $pageId;
$wb_ent_options 		= get_option('wb_ent_options');
$analytics 				= $wb_ent_options['analytics'];
$majorAdsLB				= array();
$categoryAdsLB			= array();
$postAdsLB				= array();
$sitewideAdsLB			= array();
$included_bannersLB		= array('leaderboardsw');
$leader_board_add		= array();

function wb_get_sitewide_bannersLB(){
    global $wb_ent_options, $sitewideAdsLB, $included_bannersLB;
    
    $sitewideAdsLB	= $wb_ent_options['banners']['leaderboardsw'];
    if($sitewideAdsLB['status'] != "active"){
        $sitewideAdsLB = array();
    }
}

function wb_get_category_bannersLB(){
    global $postId, $wb_ent_options, $categoryAdsLB, $included_bannersLB;
    if (is_category()) {
        $category = get_category(get_query_var('cat'));
        $cat_id = $category->cat_ID;
        $category_banner_sequence = get_term_meta($cat_id,'wb_ad_banner_sequence',true);
        foreach ( $category_banner_sequence as $current_banner_key ){
            if ( in_array( $current_banner_key, $included_bannersLB) ){
                $categoryBanner = get_term_meta($cat_id,$current_banner_key,true);
                array_push($categoryAdsLB, $categoryBanner);
            }
        }
    }else{
        if ( get_post_type($postId) == "post" ){
            $wb_categories = wp_get_post_categories($postId);
            rsort($wb_categories);
            //check if post's category has specific banners
            foreach ( $wb_categories as $current_category ){
                $category_banner_sequence = get_term_meta($current_category,'wb_ad_banner_sequence',true);
                if ( $category_banner_sequence ){
                    foreach ( $category_banner_sequence as $current_banner_key ){
                        if ( in_array( $current_banner_key, $included_bannersLB ) ){
                            $categoryBanner = get_term_meta($current_category,$current_banner_key,true);
                            array_push($categoryAdsLB, $categoryBanner);
                        }
                    }
                    if ( $categoryAds ){
                        break;
                    }
                }
            }
        }
    }
}

function wb_get_post_bannersLB(){
    global $postId, $wb_ent_options, $postAdsLB, $included_bannersLB;
    if ( get_post_type($postId) == "post" ){
        $post_banner_sequence = get_post_meta($postId,'wb_ad_banner_sequence',true);
        if ( $post_banner_sequence ){
            foreach ( $post_banner_sequence as $current_banner_key ){
                if ( in_array( $current_banner_key, $included_bannersLB ) ){
                    $postBanner = get_post_meta( $postId, $current_banner_key, true );
                    array_push($postAdsLB, $postBanner);
                }
            }
        }
    }
}

function wb_get_page_bannersLB(){
    global $pageId, $wb_ent_options, $postAdsLB, $included_bannersLB;
    if ( get_post_type($pageId) == "page" ){
        $post_banner_sequence = get_post_meta($pageId,'wb_ad_banner_sequence',true);
        if ( $post_banner_sequence ){
            foreach ( $post_banner_sequence as $current_banner_key ){
                if ( in_array( $current_banner_key, $included_bannersLB) ){
                    $postBanner = get_post_meta( $pageId, $current_banner_key, true );
                    array_push($postAdsLB, $postBanner);
                }
            }
        }
    }
}

$currentAdTitle = $wb_ent_options['channelname'] . " - Leaderboard Banner";

$wb_current_ID = get_the_ID();
// Check if it's a page
if(  get_post_type($wb_current_ID) == "page" ){
    $pageId = get_the_ID();
    wb_get_page_bannersLB();
    if ( $postAdsLB ){
        $leader_board_add = $postAdsLB[0];
    }
}elseif ( is_category() ){
    wb_get_category_bannersLB();
    if ( $categoryAdsLB ){
    	$leader_board_add = $categoryAdsLB[0];
    }
}else{
    wb_get_post_bannersLB();
    if ( $postAdsLB ){
        $leader_board_add = $postAdsLB[0];
    }else{
        wb_get_category_bannersLB();
        if ( $categoryAdsLB ){
        	$leader_board_add = $categoryAdsLB[0];
        }
    }
}

if ( !$leader_board_add ){
    wb_get_sitewide_bannersLB();
    $leader_board_add = $sitewideAdsLB;
}
//print_r($leader_board_add);
if ( $leader_board_add ){
    if ( $leader_board_add['image'] != "" ){
        //Leaderboard is image
        $imageFilenameTemp = explode('/', $leader_board_add[0]['image']);
        $imageFilename = $imageFilenameTemp[ (count($imageFilenameTemp)- 1) ];
        echo '
		<div id="wbLeaderboardsw" class="lbsw-container">
         <script type="text/javascript">
            ';
        foreach ($analytics as $key=>$tracker) {
            if( isset($currentAdOrderNumber) && trim($currentAdOrderNumber) != ''){
                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner View\', \'' . $currentAdOrderNumber . '_728x90_' . $imageFilename . '\');';
            } else {
                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner View\', \'' . '960x90_' . $imageFilename . '\');';
            }
        }
        echo '
         </script>
			<a target="'.$target.'" onclick="';
        foreach ($analytics as $key=>$tracker) {
            if( isset($currentAdOrderNumber) && trim($currentAdOrderNumber) != ''){
                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner Click\', \'' . $currentAdOrderNumber . '_728x90_' . $imageFilename . '\');';
            } else {
                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner Click\', \'' . '960x90_' . $imageFilename . '\');';
            }
        }
        echo '"';
        echo ' href="'.$leader_board_add['url'].'"';
        echo '>';
        $tempArray =  explode('.', $leader_board_add['image']);
        if( ($tempArray[count($tempArray)-1]) == 'swf' ){
            ?>
			<object width="728" height="60">
				<param name="movie" value="<?php echo $currentAdImage; ?>">
				<param name="wmode" value="transparent" />
				<embed wmode="transparent" src="<?php echo $currentAdImage; ?>" width="468" height="60">
				</embed>
			</object>	
		<?php
		}else{
			echo '<img src="'.$leader_board_add['image'].'" alt="'.$currentAdTitle.'" class="img-responsive btn-block" style="max-width: 728px; width:100%;margin: 0 auto;" />';
		}	
		echo '</a>
		</div>';
	}else{
		//Leaderboard is custom code
		if ($leader_board_add['customcode'] != "" ){
			echo html_entity_decode($leader_board_add['customcode']);
		}else{
			echo html_entity_decode($leader_board_add[0]['customcode']);
		}
	}
}
?>