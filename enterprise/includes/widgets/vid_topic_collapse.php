<?php
/*
Filename:      vid_topic.php
Description:   will select the categories and their subcategories and display them
Author:
*/

//error_reporting(E_ALL);

if (!isset($postCategories) || count($postCategories) <= 0) {
    $postCategories = wb_get_categories();
}
?>
<style>
.topicList{list-style: none}.topicList a{color: #000}#categories h5{color: #000000}#categories .subs-h{color: #000}#categories a:hover,#categories h5:hover{color: #FF0000}#vid_topic li.parent h5{text-align: left;background-position: right center !important;margin-bottom: 0px;padding: 5px 5px 5px 0}#vid_topic li.parent h5 a{padding: 0 0 0 20px;width: auto;display: block}#vid_topic li.nosubtopic h5{padding: 5px 0 5px 20px}#vid_topic li.nosubtopic h5 a{padding: 0}#vid_topic .subTopicList{text-align: left;list-style-type: none}#vid_topic li.parent{padding: 0}#vid_topic .subTopicList{background-position: 10px !important;padding: 3px}#vid_topic li.parent{list-style-type: none !important}#categories ul li{margin-left: 0px;width: 100%}#categories ul{margin-top: 0px;margin-bottom: 0px}#vid_topic{margin: 0 auto;padding: 0;text-align: left;max-width: 300px;width: auto}li.sub-third-level a{display: block;padding: 4px 0px 4px 60px !important;font-weight: normal;width: auto;margin: 0}
</style>

	<script type="text/javascript">
 		/*
      	This script controls the styling and functionality of the category/topic box on the sidebar(includes/box/vid_topic.php)
      	*/

      	$( document ).ready(function() {
      	    //console.log( "ready bee" );
      	});

      (function( $ ) {
          //console.log("workerbee");
      	<?php if(!$wb_ent_options['catlisttype']['collapse']){ ?>
      		$('#categories li ul').hide();
			$('#categories .subs-h').addClass("collapsed_boxarrow");
        <?php }else{ ?>
			$('#categories li ul li ul').hide();
		  	$('#categories .subs-h').addClass("expanded_boxarrow");
		<?php }
        	if($wb_ent_options['catlisttype'] == 'image'){
            	$ulMargin = '85px';
            	$arrowBgX = '5px';
            	$arrowDownBgX = '4px';
            	$arrowDownBgY = '4px';
         	}else{
            	$ulMargin = '15px';
            	$arrowBgX = 'left';
            	$arrowDownBgX = '10px';
            	//$arrowDownBgY = 'top';
         	}
        ?>
        	$("#categories .subCat h5").css('background','none');
         	$("#categories h5").css('margin','0');
         	//$('#categories .subs-h').addClass("collapsed_boxarrow");

         	$('#categories .subs-h').click(function() {
	    		if ($(this).next("ul").is(":hidden")){
					$(this).next("ul").slideDown(200);
				}else{
					$(this).next("ul").slideUp(200);
				}
	            //$(this).next("#categories ul").slideToggle("fast");
	            $('#categories  .subs-h').toggleClass("collapsed_boxarrow expanded_boxarrow");
         	});

         	$('#categories .topicList h5').click(function() {
				if ($(this).next("#categories .topicList ul").is(":hidden")){
					$(this).next("#categories .topicList ul").slideDown(200);
				}else{
					$(this).next("#categories .topicList ul").slideUp(200);
				}
              	//$(this).next("#categories .topicList ul").slideToggle("fast");
              	$(this).children('.iconwb').toggleClass("icon-plus icon-minus");
            });
        	/*
        	$('#categories .topicList h5 a').click(function() {
         	alert("clicked a");
         	});
         	*/
         	$('#categories ul ul li').click(function() {
            	$('#vid_list').slideDown(200);
            	$('#video_list').fadeOut(200);
         	});

         	$('#categories .topicList h5').mouseover(function(){
            	$(this).css('color', '#ffffff');
            	$(this).children('i').addClass('icon-white');
            	/* $(this).children('a').css('color', '#ffffff'); */
            	$(this).children('i').css('color', '#ffffff');
         	});

         	$('#categories .topicList h5').mouseout(function(){
            	/* $(this).css('color', '#000000'); */
             	$(this).children('i').removeClass('icon-white');
             	/* $(this).children('a').css('color', '#000000'); */
         	});

         	$('#categories ul li').hover(function() {
            	$(this).css('cursor','pointer');
         	});
         	$('#categories ul ul li').hover(function() {
            	//$(this).css({'cursor':'pointer', 'color':'#016891', 'padding-left':'8px',  'text-decoration':'none'});
         	});
         	$('#categories ul ul li').mouseout(function() {
            	//$(this).css({'cursor':'arrow', 'color':'#000000', 'padding-left':'8px', 'text-decoration':'none'});
         	});
      	})( jQuery );
	</script>
<?php
if($wb_ent_options['catlisttype'] == 'image'){
?>
<style>
#categories img{float: left;display: block}#categories h5{display: block;margin: 0;width: 300px;font-size: 16px;font-weight: bold;color:#362f25}#categories h5:hover,#categories h5 a:hover,#vid_topic li.parent ul li:hover,#vid_topic li.parent ul li a:hover{color: #A19273}#categories h5 a{padding: 0px}#vid_topic li.parent{min-height: 30px}#vid_topic li.parent ul{display: block;margin-left: 0px;margin-top: 0px}#vid_topic li.parent ul li{list-style: none;font-size: 12px;font-weight: bold;margin-bottom: 0px}#vid_topic li.parent ul li .subCatCount{color: #E3852B}
</style>
<?php
}
//will check if there are posts for each category or subcategory
if(count($postCategories) > 0){
?>	
	<div id="vid_topic" class="wbc-sidemenu-style marginleft <?php echo ($wb_ent_options['hascatlist']['mobileview']) ? 'visible-desktop' : ''; ?>  btnwb btn-block">
		<!-- <h4>Browse by Category &amp; Topic</h4> -->
		<div id="categories">
			<ul>

				<li class="" style="padding-bottom: 0px; cursor: pointer; padding: 0px;" > <?php

                    if(isset($_GET['lib'])){?>

                     <h5 class="subs-h" style=""><?= __('Library test', 'enterprise') ?></h5>
                        <?php } else { ?>

                        <h5 class="subs-h" style=""><?= __('Video Library', 'enterprise') ?></h5>
                        <?php } ?>


					<ul style="display: block;" >
					<?php
						foreach ($postCategories as $parent) {
							$id = $parent['cat_id'];
							//if the subCat exists for this parent category, it means that subCat has posts associated with it
							if($parent['subCatNumRows'] > 0 /*&& ( $parent['cat_name'] != "MHEDA University" && $parent['cat_id'] != 235 )*/ ){
								echo '<li class="parent topicList subCat">';
								if($wb_ent_options['catlisttype'] == 'image'){
									echo '<img src="'.get_site_url().'/images/thumbs/'.$parent['cat_slug'].'_cat.jpg" />';
								}
								$wb_cat_level_1 = '';
								$current_parent_name = $parent['cat_name'];
                                $categoryPermalink = get_site_url().'/category/'.$parent['cat_slug'];
                                //
                                if ( strlen($current_parent_name) > 12 ){
                                	$wb_cat_level_1 = " iconwb-first-level ";
                                }
			        			?>
			        				<h5 style="padding-left: 20px;"><label style="width: 85%;display: -webkit-inline-box;margin-bottom: 0px;"><?php printf(__('%s', 'enterprise'), $parent['cat_name']); ?></label><i class="icon-plus iconwb <?php echo $wb_cat_level_1;?> " style="float:right;margin-top:5px;"></i></h5><?= PHP_EOL ?>
			        				<!--  <h5><a style="width: 88%; display: inline-block;" href="<?= $categoryPermalink ?>"><?php printf(__('%s', 'enterprise'), $parent['cat_name']); ?></a><i class="icon-plus iconwb" style="float:right;margin-top:5px;"></i></h5><?= PHP_EOL ?>-->
			        			<?php				
								echo '	<ul>'.PHP_EOL;
								foreach($parent['subCat'] as $currentSubCat){
									$currentsubcat_count++;
									$topics = get_categories( array( 'child_of' => $currentSubCat['subCat_id'], 'orderby' => 'name' ) );
							
									if ($currentSubCat['subCat_count'] > 0) {
										if( count($topics) > 0 ){
											echo '<li class="parent topicList subCat" style="margin-bottom: -1px;">';
											$categoryPermalink = get_site_url().'/category/'.$parent['cat_slug'];
									?>
										<h5 style="padding-left: 40px;"><label style="width: 85%;display: -webkit-inline-box;margin-bottom: 0px;"><?php printf(__('%s', 'enterprise'), $currentSubCat['subCat_name']); ?></label><i class="icon-plus iconwb iconwb-second-level" style="float:right;margin-top:5px;"></i></h5><ul><?= PHP_EOL ?>
									<?php 
											foreach( $topics as $current_topic ){
												echo '<li class="sub-third-level" style="list-style-type: none; display: block;"><a href="' . get_site_url().'/category/' . $current_topic->slug . '">';
												printf( __('%s', 'enterprise'), $current_topic->name);
												echo '</a></li>';
											}
											echo '<li class="sub-third-level"><a href="'.get_site_url().'/category/'.$currentSubCat['subCat_slug'].'"  style="line-height: 20px; display:block;"  >View All</a></li>';
											echo "</ul></li>";	
										} else {
											echo "<!-- no sub topic " . $current_topic->name . " -->";
											echo '<li class="subTopicList" style="cursor: pointer;"><a href="'.get_site_url().'/category/'.$currentSubCat['subCat_slug'].'" style="line-height: 20px; display:block;">' .$currentSubCat['subCat_name'].'</a></li>';
										}
											
									} else {
										//echo "<!-- no sub cat " . $currentSubCat['subCat_slug'] . " -->";
										echo '<li class="subTopicList"><a href="'.get_site_url().'/category/'.$currentSubCat['subCat_slug'].'"  style="line-height: 20px; display:block;"  >';
										printf(__('%s', 'enterprise'), ltrim($currentSubCat['subCat_name'], '0')); //updated to remove 0 from left of string, sorting purposes.
							
										if($wb_ent_options['catlistcount']){
											/*echo ' <span class="subCatCount">('.$currentSubCat['subCat_count'];
											if($currentSubCat['subCat_count'] > 1){
												echo ' videos';
											}
											else{
												echo ' video';
											}
											echo ')</span>';
											*/

										}
											echo '</a></li>'.PHP_EOL;	
									}
								}

								if($currentsubcat_count > 0){
									echo '<li class="subTopicList" style="cursor: pointer;"><a href="'.get_site_url().'/category/'.$parent['cat_slug'].'" style="line-height: 20px; display:block;">View All</a></li>';
								}
								$currentsubcat_count=0;
								echo '	</ul>'.PHP_EOL.'</li>'.PHP_EOL;
							}else{ //if the subCat doesn't exist, it means the post is associated with the parent category. display parent category as a link
								echo '';
								if($wb_ent_options['catlisttype'] == 'image'){
									echo '<img src="'.get_site_url().'/images/thumbs/'.$parent['cat_slug'].'_cat.jpg" />';
								}

            					$categoryPermalink = get_site_url().'/category/'.$parent['cat_slug'];
								echo '<li class="parent nosubtopic topicList"><h5><a href="'.$categoryPermalink.'" style="text-decoration: none;">';
        						printf(__('%s', 'enterprise'), $parent['cat_name']);
        						echo '</a></h5>'.PHP_EOL;
								echo '</li>'.PHP_EOL;	
							}
						}
    
    					if($wb_ent_options['hascat']['ondemand']){
      						echo '<a href="'.$wb_ent_options['hascat']['ondemandurl'].'" style="text-decoration: none;"><li class="parent topicList" style="cursor: pointer;"><h5 style="margin: 0px;">';
      						printf(__('%s', 'enterprise'), $wb_ent_options['hascat']['ondemandtitle']);
      						echo '</h5></li></a>';
    					}
					?>
				</ul>
			</li>
		</ul>
	</div>
</div>
<?php	
}
?>