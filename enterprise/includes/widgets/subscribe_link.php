<?php
/*
  Filename:      subscribeCollapse.php
  Description:   collapsible subscribe box
  Author: 	   	Jullie Quijano
 */
?>
<style>
    #subscribe-link{
        padding: 0;
        margin-bottom: 20px;
        max-width: 300px;
    }
</style>
<div id="subscribe-link" class="wbc-sidemenu-style gradBox btnwb visible-desktop vtgreen marginleft" style="">
    
        <a href="/subscribe"><h5 id="subscribe-link-header" class="subs-h"><?= _e('Subscribe', 'enterprise') ?></h5></a>   
    
</div>
<div class="spacing"></div>
