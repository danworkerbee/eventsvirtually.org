<?php
/*
Filename:      subscribe.php                                                 
Description:   this include will allow users to subscribe to the site through rss or podcast
Author:        
Change Log:                                                                                                                                                                                                   
*/

?>
<div id="subscribe" class="gradBox">
	<style>
    #leftCol #subscribe .subsIconDiv {
          padding: 0px;
    }
    h4#subscribeHeader {
        /*background:url("/images/sidebarCollapseArrowDownGray.png") no-repeat scroll 255px 0 transparent;*/
       cursor:pointer;
       text-align:center;
       text-transform:uppercase;
       font-size:17.5px;
       color:#000;
    }
    #subscribe {       
       padding: 10px;
    }
    h4#subscribeHeader:hover {
       color: #FF0000;
    }
	</style>
	<h4><?php printf(__('Subscribe to %s', 'enterprise'), $wb_ent_options['channelname']); ?></h4>
	<div id="subscribeContent" class="subsIconDiv" style="margin-left: 30px; text-align: center; height: 0px; overflow: hidden; height: 90px;">
		<div style="float: left;">
			<a href="<?php echo $wb_ent_options['ituneslink'] ?>" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/images/sub_itunes.png" alt="iTunes" /></a><br/>
			<a href="<?php echo $wb_ent_options['ituneslink'] ?>" target="_blank"><?= _e('iTunes', 'enterprise') ?></a>
		</div>
		<div style="float: left;">
			<a href="<?php echo $wb_ent_options['emailsubslink'] ?>"><img width="60" height="60" src="<?php echo get_template_directory_uri() ?>/images/emailReg.png" alt="Email" /></a><br/>
			<a href="<?php echo $wb_ent_options['emailsubslink'] ?>"><?= _e('Email', 'enterprise') ?></a>
		</div>
		<div style="float: left;">
			<a href="<?php echo $wb_ent_options['rsslink'] ?>" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/images/sub_rss.png" alt="RSS" /></a><br/>
			<a href="<?php echo $wb_ent_options['rsslink'] ?>" target="_blank"><?= _e('RSS', 'enterprise') ?></a>
		</div>
		<?php /*
    <div style="float: left;margin:5px 10px;" >
		<a href="<?php echo youtube_channel ?>" target="_blank"><img src="<?php echo site_root_theme ?>images/sub_youtube.png" alt="Youtube" /></a><br/>
		<a href="<?php echo youtube_channel ?>" target="_blank">Youtube</a>
	</div>
	<div style="width:100%;clear:both; height: 1px;">
		&nbsp;
	</div>
	 */ ?>
</div>
</div>