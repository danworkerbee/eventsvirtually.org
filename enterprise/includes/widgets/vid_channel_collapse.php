<?php
/*
Filename:      vid_channel.php                                                 
Description:   will select the categories and their subcategories and display them
Author:        				
*/

//error_reporting(E_ALL);
//print_r($postCategories);
if (!isset($postChannels) || count($postChannels) <= 0) {
    $postChannels = wb_get_channels();
}

//print_r($postChannels);

?>
<style>
.topicList{
	list-style: none;	
}
.topicList a{
	color: #000;	
}
#channels h5{
	color: #000000;	
}
#channels .subs-h{
	color: #000;	
}
#channels a:hover, #channels h5:hover{
	color : #FF0000 ;
}
#vid_channel li.parent h5{
	text-align: left;
	background-position: right center !important	;
	margin-bottom: 0px;	
   padding: 5px 20px;
}
#vid_channel .subTopicList{
	text-align: left;
	/*background: url("http://mhedappv.wbtvserver.com//images/arrow.gif") no-repeat scroll 10px top transparent;*/
	list-style-type: none;
	/*margin-left: 40px !important;*/
}
#vid_channel .subTopicList a{ 
	font-weight: 600;
	/*margin-right: 70px;*/
}
#vid_channel li.parent{
	 padding: 0;
}
#vid_channel .subTopicList{
	background-position: 10px !important;
	padding: 3px;
}
#vid_channel li.parent{
	list-style-type: none !important;
}
#channels ul li {
    margin-left: 0px;
    width: 100%;
}
#channels ul {
    margin-top: 0px;
    margin-bottom: 0px;
}
#vid_channel {
    background: url("/wp-content/themes/enterprise/images/bg_box.gif") repeat-x scroll left top #FFFFFF;
    //border: 1px solid #EEEEEE;
    margin: 0;
    padding: 0;
    text-align: left;
    width: 300px;
}
</style>
       <script type="text/javascript">
      /*
      This script controls the styling and functionality of the category/topic box on the sidebar(includes/box/vid_channel.php)
      */
      (function( $ ) {
          <?php if(!$wb_ent_options['catlisttype']['collapse']){ ?>
         $('#channels li ul').hide();
          <?php } ?>
         <?php
         if($wb_ent_options['catlisttype'] == 'image'){
            $ulMargin = '85px';
            $arrowBgX = '5px';
            $arrowDownBgX = '4px';
            $arrowDownBgY = '4px';
         }
         else{
            $ulMargin = '15px';
            $arrowBgX = 'left';
            $arrowDownBgX = '10px';
            //$arrowDownBgY = 'top';
         }
         ?>
         
         //$('#channels li ul').css('margin-left', '<?php echo $ulMargin; ?>');
         
         //$("#channels h5").css('background','url("<?php echo get_site_url();?>/images/arrow.gif") <?php echo $arrowBgX; ?> center no-repeat');
         $("#channels .subCat h5").css('background','none');
         $("#channels h5").css('margin','0');
         
         //$("#channels h5").css('padding-left','25px');
         
         $('#channels h5').click(function() {
            if ($(this).next("#channels ul").is(":hidden")){
               //$(this).css('background','url("<?php echo get_site_url();?>/images/arrow-down.gif") <?php echo $arrowDownBgX.' '.$arrowDownBgY  ?>  no-repeat');
               $(this).css('background','url("/wp-content/themes/enterprise/images/bg_box.gif") repeat-x scroll left top #FFFFFF');
							 $(this).next("#channels ul").show();
						 }
            else{
               //$(this).css('background','url("<?php echo get_site_url();?>/images/arrow.gif") <?php echo $arrowBgX; ?> center no-repeat');
               $(this).css('background','url("/wp-content/themes/enterprise/images/bg_box.gif") repeat-x scroll left top #FFFFFF');
               //$(this).css('background','url("<?php echo get_site_url();?>/wp-content/themes/enterprise/images/bg_box.gif") <?php echo $arrowBgX; ?> center no-repeat');
							 $(this).next("#channels ul").hide();
						 }            
						 //$(this).next("#channels ul").slideToggle("fast");
         });
         
         $('#channels ul ul li').click(function() {
            //$('#pageContents').fadeOut('slow');
            $('#vid_list').slideDown("slow");
            $('#video_list').fadeOut("slow");
            
         });
         $('#channels ul li').hover(function() {
            $(this).css('cursor','pointer');
         });
         $('#channels ul ul li').hover(function() {
            //$(this).css({'cursor':'pointer', 'color':'#016891', 'padding-left':'8px',  'text-decoration':'none'});
         });
         $('#channels ul ul li').mouseout(function() {
            //$(this).css({'cursor':'arrow', 'color':'#000000', 'padding-left':'8px', 'text-decoration':'none'});
         });         

      })( jQuery );
      </script>
<?php

if($wb_ent_options['catlisttype'] == 'image'){
?>
<style>
#channels img{
	float: left;	
	display: block;
}

#channels h5 {
    display: block;
    margin: 0;
    width: 300px;
    font-size: 16px;
    font-weight: bold;   
    color:#362f25;
}
#channels h5:hover, #channels h5 a:hover, #vid_channel li.parent ul li:hover, #vid_channel li.parent ul li a:hover{
	color: #A19273;
}

#channels h5 a{
	padding: 0px;
}




#vid_channel li.parent {
	min-height: 30px;
}

#vid_channel li.parent ul{
    display: block;
    margin-left: 0px;
    margin-top: 0px;
}

#vid_channel li.parent ul li{
	list-style: none;	
	font-size: 12px;
	font-weight: bold;
	margin-bottom: 0px;
}

#vid_channel li.parent ul li .subCatCount{
	color: #E3852B;
}

</style>
<?php
}
//will check if there are posts for each category or subcategory
if(count($postChannels) > 0){
?>	
	<div id="vid_channel" class="marginleft visible-desktop btnwb btn-block">
	<!-- <h4>Browse by Category &amp; Topic</h4> -->
	<div id="channels">
	<ul>
	<li class="" style="padding-bottom: 0px; cursor: pointer; padding: 0px;" ><h5 class="subs-h" style="background: url('/wp-content/themes/enterprise/images/bg_box.gif') repeat-x scroll left top #FFFFFF;">Channel Library</h5>
		<ul style="display: block; " >
	<?php
		foreach ($postChannels as $parent) {
			//skip mheda-u cats
			/*if( $parent['cat_slug'] == 'mheda-u' ){			    
			    continue;    
			}*/
			$id = $parent['cat_id'];
			//if the subCat exists for this parent category, it means that subCat has posts associated with it
			if($parent['subCatNumRows'] > 0 && ( $parent['cat_name'] != "MHEDA University" && $parent['cat_id'] != 235 ) ){
				echo '<li class="parent topicList subCat">';
				if($wb_ent_options['catlisttype'] == 'image'){
					echo '<img src="'.get_site_url().'/images/thumbs/'.$parent['cat_slug'].'_cat.jpg" />';
				}
				echo '<h5>'.$parent['cat_name'].'</h5>'.PHP_EOL;
				echo '	<ul>'.PHP_EOL;
				foreach($parent['subCat'] as $currentSubCat){
					echo '<li class="subTopicList"><a href="'.get_site_url().'/category/'.$currentSubCat['subCat_slug'].'"  style="font-size: 11px; line-height: 20px; margin-left: 30px; display:block;"  >';
					echo $currentSubCat['subCat_name'];
					if($wb_ent_options['catlistcount']){
						/*echo ' <span class="subCatCount">('.$currentSubCat['subCat_count'];
						if($currentSubCat['subCat_count'] > 1){
							echo ' videos';
						}
						else{
							echo ' video';
						}
						echo ')</span>';
						*/
					}
					echo '</a></li>'.PHP_EOL;		
				}
				echo '	</ul>'.PHP_EOL.'</li>'.PHP_EOL;
			}
			else{ //if the subCat doesn't exist, it means the post is associated with the parent category. display parent category as a link
				echo '';
				if($wb_ent_options['catlisttype'] == 'image'){
					echo '<img src="'.get_site_url().'/images/thumbs/'.$parent['cat_slug'].'_cat.jpg" />';
				}

            $categoryPermalink = get_site_url().'/category/'.$parent['cat_slug'];
				echo '<a href="'.$categoryPermalink.'" style="text-decoration: none;"><li class="parent topicList"><h5>'.$parent['cat_name'].'</h5>'.PHP_EOL;
				echo '</li></a>'.PHP_EOL;	
			}
		}
    
    if($wb_ent_options['hascat']['ondemand']){
      echo '<a href="'.$wb_ent_options['hascat']['ondemandurl'].'" style="text-decoration: none;"><li class="parent topicList" style="cursor: pointer;"><h5 style="margin: 0px;">'.$wb_ent_options['hascat']['ondemandtitle'].'</h5></li></a>';
    }
	?>
      <?php /*              
      <li class="parent topicList subCat" style="cursor: pointer;"><h5 style="background: none repeat scroll 0% 0% transparent; margin: 0px;">On-Demand</h5>
	<ul style="display: none;">
<li class="subTopicList"><a style="font-size: 11px; line-height: 20px; margin-left: 30px; display:block;" href="/product/2014-asq-world-conference">World Conference</a></li>
<li class="subTopicList"><a style="font-size: 11px; line-height: 20px; margin-left: 30px; display:block;" href="/product/2014-international-team-excellence-award-quality-impact-sessions">ITEA</a></li>
	</ul>
</li> */
      ?>
		</ul>
		</li>

			</ul>
		</div>
</div>
<?php	
}
?>