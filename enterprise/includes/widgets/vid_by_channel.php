<?php
/*
Filename:      vid_by_channel.php                                                 
Description:   will select the channelists and their subchannelists and display them
Author:        				
*/

//error_reporting(E_ALL);
 
if (!isset($postChannels) || count($postChannels) <= 0) {
    $postChannels = wb_get_channels(0, true, 'id');  
}
//echo '<!-- $postChannels is '.print_r($postChannels, true).' -->';
$channelHeaderTitle = ($wb_ent_options['channels']['headertitle']) ? $wb_ent_options['channels']['headertitle'] : 'Channels';
?>
<style>
.topicList{
	list-style: none;	
}
.topicList a{
	color: #000;	
}
#channelists h5{
	color: #000000;	
}
#channelists .subs-h{
	color: #000;	
}
#channelists a:hover, #channelists h5:hover{
	//color : #FF0000 ;
}
#vid_by_channel li.parent h5{
	text-align: left;
	background-position: right center !important	;
	margin-bottom: 0px;	
   padding: 5px 20px;
}
#vid_by_channel .subTopicList{
	text-align: left;
	/*background: url("http://mhedappv.wbtvserver.com//images/arrow.gif") no-repeat scroll 10px top transparent;*/
	list-style-type: none;
	/*margin-left: 40px !important;*/
}
#vid_by_channel .subTopicList a{ 
	font-weight: 600;
	/*margin-right: 70px;*/
}
#vid_by_channel li.parent{
	 padding: 0;
}
#vid_by_channel .subTopicList{
	background-position: 10px !important;
	padding: 3px;
}
#vid_by_channel li.parent{
	list-style-type: none !important;
}
#channelists ul li {
    margin-left: 0px;
    width: 100%;
}
#channelists ul {
    margin-top: 0px;
    margin-bottom: 0px;
}
#vid_by_channel {
    background: url("/wp-content/themes/enterprise/images/bg_box.gif") repeat-x scroll left top #FFFFFF;
    //border: 1px solid #EEEEEE;
    margin: 0 auto;
    padding: 0;
    text-align: left;
    max-width: 300px;
}
#vid_by_channel .topicList a{
width: 100%;
margin: 0;
}

#side-nav .vtgreen .vtul li h5:hover{
color: #fff;
}
#side-nav #vid_by_channel .subTopicList{
    padding: 0px;
    
}
#side-nav #vid_by_channel .subTopicList a {
    font-weight: normal;
    padding: 0 0 0 40px;
    width: auto !important;
}
</style>
       <script type="text/javascript">
      /*
      This script controls the styling and functionality of the category/topic box on the sidebar(includes/box/vid_topic.php)
      */
      (function( $ ) {
          <?php if(!$wb_ent_options['catlisttype']['collapse']){ ?>
        $('#channelists li ul').hide();
          <?php } ?>
         <?php
         if($wb_ent_options['catlisttype'] == 'image'){
            $ulMargin = '85px';
            $arrowBgX = '5px';
            $arrowDownBgX = '4px';
            $arrowDownBgY = '4px';
         }
         else{
            $ulMargin = '15px';
            $arrowBgX = 'left';
            $arrowDownBgX = '10px';
            //$arrowDownBgY = 'top';
         }
         ?>
         
         //$('#channelists li ul').css('margin-left', '<?php echo $ulMargin; ?>');
         
         //$("#channelists h5").css('background','url("<?php echo get_site_url();?>/images/arrow.gif") <?php echo $arrowBgX; ?> center no-repeat');
         $("#channelists .subCat h5").css('background','none');
         $("#channelists h5").css('background','none');
         $("#channelists h5").css('margin','0');
         $('#channelists .subs-h').addClass("collapsed_boxarrow");
         
           $('#channelists .subs-h').click(function() {
            $(this).next("#channelists ul").slideToggle("fast");
             $('#channelists  .subs-h').toggleClass("collapsed_boxarrow expanded_boxarrow");
         });     
    
    
         //$("#channelists h5").css('padding-left','25px');
         
         $('#channelists .topicList  h5').click(function() {        
           $(this).next("#channelists .topicList ul").slideToggle("fast");           
           $(this).children('.iconwb').toggleClass("icon-plus icon-minus");
         });
         
         $('#channelists ul ul li').click(function() {
            //$('#pageContents').fadeOut('slow');
            $('#vid_list').slideDown("slow");
            $('#video_list').fadeOut("slow");
            
         });
         
         
         $('#channelists .topicList h5').mouseover(function(){
            $(this).css('color', '#ffffff');
            $(this).children('i').addClass('icon-white');
            $(this).children('a').css('color', '#ffffff');
         });
                  
         $('#channelists .topicList h5').mouseout(function(){
             $(this).css('color', '#000000');
             $(this).children('i').removeClass('icon-white');
             $(this).children('a').css('color', '#000000');
         });        
         
        $('#channelists ul li').hover(function() {
            $(this).css('cursor','pointer');
         });
         $('#channelists ul ul li').hover(function() {
            //$(this).css({'cursor':'pointer', 'color':'#016891', 'padding-left':'8px',  'text-decoration':'none'});
         });
         $('#channelists ul ul li').mouseout(function() {
            //$(this).css({'cursor':'arrow', 'color':'#000000', 'padding-left':'8px', 'text-decoration':'none'});
         });         

     })( jQuery );
      </script>
<?php

if($wb_ent_options['catlisttype'] == 'image'){
?>
<style>
#channelists img{
	float: left;	
	display: block;
}

#channelists h5 {
    display: block;
    margin: 0;
    width: 300px;
    font-size: 16px;
    font-weight: bold;   
    color:#362f25;
}
#channelists h5:hover, #channelists h5 a:hover, #vid_by_channel li.parent ul li:hover, #vid_by_channel li.parent ul li a:hover{
	color: #A19273;
}

#channelists h5 a{
	padding: 0px;
}

#vid_by_channel li.parent {
	min-height: 30px;
}

#vid_by_channel li.parent ul{
    display: block;
    margin-left: 0px;
    margin-top: 0px;
}

#vid_by_channel li.parent ul li{
	list-style: none;	
	font-size: 12px;
	font-weight: bold;
	margin-bottom: 0px;
}

#vid_by_channel li.parent ul li .subCatCount{
	color: #E3852B;
}

</style>
<?php
}
//will check if there are posts for each category or subcategory
if(count($postChannels) > 0){
?>	
	<div id="vid_by_channel" class="wbc-sidemenu-style vtgreen marginleft btnwb btn-block">
	<!-- <h4>Browse by Category &amp; Topic</h4> -->
	  <div id="channelists">
	    <ul>
	      <li class="" style="padding-bottom: 0px; cursor: pointer; padding: 0px;" >
                <h5 class="subs-h" style=""><?php echo $channelHeaderTitle; ?></h5>
		<ul style="display: block; " class="vtul">
                <?php
                $count = 0;
		foreach ($postChannels as $parent) {
			//skip mheda-u cats
			/*if( $parent['cat_slug'] == 'mheda-u' ){			    
			    continue;    
			}*/
			$id = $parent['cat_id'];
                        echo '<!-- $parent is '.print_r($parent, true).' -->';                 
 
                        $parentChannelInfo = wb_get_channel_info($id, 0);
      
                        if($parentChannelInfo['landingPageUrl']){
                        $landingUrl = $parentChannelInfo['landingPageUrl'];
                        }else{
                        $landingUrl = get_site_url().'/product-category/channel/'.$parent['cat_slug'];
                        }
                        $channelName = $parent['cat_name'];
                        if($parentChannelInfo['channelTopics'] != ''){
                             $channelName  = $parentChannelInfo['channelTopics'];
                        }
      /*
      echo '<li class="parent topicList subCat">';
      echo '<a href="'.$landingUrl.'"><h5>'.$channelName.'</h5></a>'.PHP_EOL;
			echo '</li>'.PHP_EOL;
      */
      
      //if the subCat exists for this parent category, it means that subCat has posts associated with it
			if($parent['subCatNumRows'] > 0){
				echo '<li class="parent topicList subCat">';
				if($wb_ent_options['catlisttype'] == 'image'){
					echo '<img src="'.get_site_url().'/images/thumbs/'.$parent['cat_slug'].'_cat.jpg" />';
				}?>
                        <h5 class=""><!--<a href="<?= $landingUrl ?>">--><?php printf(__('%s', 'enterprise'), $parent['cat_name']); ?><!--</a>--><i class="icon-plus iconwb" style="float:right;margin-top:5px;"></i></h5><?= PHP_EOL ?>
                        <?php
				//echo '<a href="'.get_site_url().'/product-category/channel/'.$parent['cat_slug'].'"><h5>'.$channelName.'</h5></a>'.PHP_EOL;
				echo '	<ul class="sub-topic-list">'.PHP_EOL;
				foreach($parent['subCat'] as $currentSubCat){
					$currentsubcat_count++;
					echo '<li class="subTopicList"><a href="'.get_site_url().'/product-category/channel/'.$currentSubCat['subCat_slug'].'"  style="line-height: 20px; display:block; width: 100%;"  >';
					echo '<h5>'.$currentSubCat['subCat_name'].'</h5>';					
					echo '</a></li>'.PHP_EOL;
                                        
				}
				if($currentsubcat_count > 1){
					echo '<li class="subTopicList" style="cursor: pointer;"><a href="'.$landingUrl.'" style="line-height: 20px; display:block;"><h5>View All</h5></a></li>';
				}
				$currentsubcat_count=0;
				echo '	</ul>'.PHP_EOL.'</li>'.PHP_EOL;
			}
			else{ //if the subCat doesn't exist, it means the post is associated with the parent category. display parent category as a link
				echo '';
				if($wb_ent_options['catlisttype'] == 'image'){
					echo '<img src="'.get_site_url().'/images/thumbs/'.$parent['cat_slug'].'_cat.jpg" />';
				}
                                 $categoryPermalink = get_site_url().'/product-category/channel/'.$parent['cat_slug'];
				echo '<a href="'.$categoryPermalink.'" style="text-decoration: none;"><li class="parent topicList"><h5>'.$parent['cat_name'].'</h5>'.PHP_EOL;
				echo '</li></a>'.PHP_EOL;
                        }

      $count++;
		}

	?>
  <!-- <li class="parent topicList subCat" style="cursor: pointer;"><h5 style="background: none repeat scroll 0% 0% transparent; margin: 0px;">On-Demand</h5> -->

</li>
		</ul>
		</li>

			</ul>
		</div>
</div>
<?php	
}
?>