<?php
/*
 * Change log:
 *    Nov 25, 2013   [Jullie]added code to display message if file is included
 */
  
?>
<div id="keyword_cloud" class="gradBox">
	<h4><?= _e('Keyword Cloud', 'enterprise') ?></h4>
	<div>
	<?php 
    if($wb_ent_options['haskeywordcloudlimit']){$wb_tagcloud_limit_val = $wb_ent_options['haskeywordcloudlimit'];} else {$wb_tagcloud_limit_val = 5;}
    wp_tag_cloud('smallest=8&largest=24&number='.$wb_tagcloud_limit_val); ?>
	</div>
</div>