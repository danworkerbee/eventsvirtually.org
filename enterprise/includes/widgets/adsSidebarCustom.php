<?php
/**
 * filename: adsSidebarCustom.php
 * description: will display custom ads on the sidebar; comment everything out if using naylor ads
 * author: Jullie Quijano
 * date created: 2014-04-23
 *
 */
global $postId, $pageId, $wb_ent_options, $majorAds, $categoryAds, $postAds, $sitewideAds, $excluded_banners;
$wb_ent_options 	= get_option('wb_ent_options');
$analytics 			= $wb_ent_options['analytics'];
$majorAds 			= array();
$categoryAds 		= array();
$postAds 			= array();
$sitewideAds		= array();
$pageAds			= array();
$excluded_banners 	= array('wide','leaderboardsw');


function wb_get_sitewide_banners(){
    global $wb_ent_options, $sitewideAds, $excluded_banners;
    for($i=1; $i<=4; $i++){
        $majorBanners = $wb_ent_options['banners']['major'.$i];
        if($majorBanners[status] == 'active' && ($majorBanners[image] != '' && $majorBanners[url] != '' || $majorBanners[customcode] != '') ){
            array_push($sitewideAds, $majorBanners);
        }
    }
    for($i=1; $i<=4; $i++){
        $minorBanners = $wb_ent_options['banners']['minor'.$i];
        if($minorBanners[status] == 'active' && ($minorBanners[image] != '' && $minorBanners[url] != '' || $minorBanners[customcode] != '') ){
            array_push($sitewideAds, $minorBanners);
        }
    }
}

function wb_get_category_banners(){
    global $postId, $wb_ent_options, $categoryAds, $excluded_banners;
    if (is_category()) {
        $category = get_category(get_query_var('cat'));
        $cat_id = $category->cat_ID;
        $category_banner_sequence = get_term_meta($cat_id,'wb_ad_banner_sequence',true);
        foreach ( $category_banner_sequence as $current_banner_key ){
            if ( !in_array( $current_banner_key, $excluded_banners ) ){
                $categoryBanner = get_term_meta($cat_id,$current_banner_key,true);
                array_push($categoryAds, $categoryBanner);
            }
        }
    }else{
        if ( get_post_type($postId) == "post" ){
            $wb_categories = wp_get_post_categories($postId);
            rsort($wb_categories);
            //check if post's category has specific banners
            foreach ( $wb_categories as $current_category ){
                $category_banner_sequence = get_term_meta($current_category,'wb_ad_banner_sequence',true);
                if ( $category_banner_sequence ){
                    foreach ( $category_banner_sequence as $current_banner_key ){
                        if ( !in_array( $current_banner_key, $excluded_banners ) ){
                            $categoryBanner = get_term_meta($current_category,$current_banner_key,true);
                            array_push($categoryAds, $categoryBanner);
                        }
                    }
                    if ( $categoryAds ){
                        break;
                    }
                }
            }
        }
    }
}

function wb_get_post_banners(){
    global $postId, $wb_ent_options, $postAds, $excluded_banners;
    if ( get_post_type($postId) == "post" ){
        $post_banner_sequence = get_post_meta($postId,'wb_ad_banner_sequence',true);
        if ( $post_banner_sequence ){
            foreach ( $post_banner_sequence as $current_banner_key ){
                if ( $current_banner_key == "leaderboardsw" ){
                    continue;
                }
                
                if ( !in_array( $current_banner_key, $excluded_banners ) ){
                    $postBanner = get_post_meta( $postId, $current_banner_key, true );
                    array_push($postAds, $postBanner);
                }
            }
        }
    }
}

function wb_get_page_banners(){
    global $postId, $wb_ent_options, $postAds, $excluded_banners;
    if ( get_post_type($postId) == "page" ){
        $post_banner_sequence = get_post_meta($postId,'wb_ad_banner_sequence',true);
        if ( $post_banner_sequence ){
            foreach ( $post_banner_sequence as $current_banner_key ){
                if ( $current_banner_key == "leaderboardsw" ){
                    continue;
                }
                
                if ( !in_array( $current_banner_key, $excluded_banners ) ){
                    $postBanner = get_post_meta( $postId, $current_banner_key, true );
                    array_push($postAds, $postBanner);
                }
            }
        }
    }
}


/*
 $upload_dir 		= wp_upload_dir();
 $wb_page_template 	= '';
 $wb_page_template 	= get_query_var('pagename');
 
 //Check if there are specific banners
 //Major
 $displayOnHome 	= get_post_meta($postId, 'wb_column_ad_display_on_home', true );
 $getMajor 		= get_post_meta($postId, 'wb_column_ad_enable_major', true );
 $getMinor 		= get_post_meta($postId, 'wb_column_ad_enable_minor', true );
 $getWide 		= get_post_meta($postId, 'wb_column_ad_enable_wide', true );
 
 
 //get video specific banners
 $videoSpecificBanner['enable'] 		= get_post_meta($postId, 'wb_column_ad_enable', true );
 $videoSpecificBanner['title'] 		= get_post_meta($postId, 'wb_column_ad_title', true );
 $videoSpecificBanner['url'] 		= get_post_meta($postId, 'wb_column_ad_url', true );
 $videoSpecificBanner['image'] 		= get_post_meta($postId, 'wb_column_ad_file', true );
 $videoSpecificBanner['orderNumber'] = get_post_meta($postId, 'wb_order_number', true );
 $videoSpecificBanner['customcode'] 	= get_post_meta($postId, 'wb_column_custom_code', true );
 
 
 $vsb['url'] = get_post_meta($postId, 'Column Ad URL', true );
 $bannerfile = get_post_meta($postId, 'Column Ad File', true );
 
 if($bannerfile){
 $bannerimg = wp_get_attachment_url( $bannerfile );
 $vsb['image'] = $bannerimg;
 }
 
 
 //If there are page specific major banners fetch it
 if($getPageMajor && !is_category() && !is_home() ){
 //echo "Page specific";
 //echo $pageId;
 for($i=1; $i<=4; $i++){
 $majorBanners = get_post_meta($pageId, 'major'.$i, true );
 //echo $postBanners[status]."status <br />";
 if($majorBanners[status] == 'active' && ($majorBanners[image] != '' && $majorBanners[url] != '' || $majorBanners[customcode] != '') ){
 array_push($majorAds, $majorBanners);
 }
 } // end of for loop
 }
 //If there are post specific major banners fetch it
 elseif($getMajor && (
 ( $displayOnHome && is_page_template( 'video-home.php' ) ) ||
 ( !$displayOnHome && !is_page_template( 'video-home.php' ) ||
 (!is_page_template( 'video-home.php' ) ) )
 ) && !is_category() && !is_home() ){
 //echo "Yes to Majors <br />";
 for($i=1; $i<=4; $i++){
 $majorBanners = get_post_meta($postId, 'major'.$i, true );
 //echo $postBanners[status]."status <br />";
 if($majorBanners[status] == 'active' && ($majorBanners[image] != '' && $majorBanners[url] != '' || $majorBanners[customcode] != '') ){
 array_push($majorAds, $majorBanners);
 }
 }// end of for loop
 }
 //Else get the sitewide banners
 else{
 //echo "else page sitewide";
 
 if(!empty($wb_ent_options[banners][bannersequence])){
 //print_r($wb_ent_options[banners][bannersequence]);
 $bannerSeq = explode(",", $wb_ent_options[banners][bannersequence]);
 foreach($bannerSeq as $key => $bninfo){
 $currentBanner =  $wb_ent_options[banners][$bninfo];
 $banSeqArr[$bninfo] = $currentBanner;
 }
 $usethisarray = $banSeqArr;
 foreach($usethisarray as $key => $bannerinfo){
 array_push($majorAds, $wb_ent_options['banners'][$key]);
 }
 }else{
 for($i=1; $i<=4; $i++){
 if( isset($wb_ent_options['banners']['major'.$i]) && (trim($wb_ent_options['banners']['major'.$i]['image']) != '' || trim($wb_ent_options['banners']['major'.$i]['customcode']) != '' ) ){
 array_push($majorAds, $wb_ent_options['banners']['major'.$i]);
 }
 }
 }
 //print_r($majorAds);
 
 }
 
 
 /*
 $minorAds = array();
 
 if( ($getMinor || $getMajor) && !is_category() && !is_home() ){
 $majorAds = array();
 //$usethisarray = $wb_ent_options['banners'];
 $usethisarray = array();
 $selPostIdSeq = get_post_meta($postId, 'wb_ad_banner_sequence');
 
 if(!empty($selPostIdSeq)){
 //print_r($wb_ent_options[banners][bannersequence]);
 //$bannerSeq = explode(",", $wb_ent_options[banners][bannersequence]);
 $bannerSeq = $selPostIdSeq;
 foreach($bannerSeq[0] as $key => $bninfo){
 if ( $bninfo == 'leaderboardsw'){
 continue;
 }
 $currentBanner =  $wb_ent_options[banners][$bninfo];
 //if ( $_SERVER['REMOTE_ADDR'] == '64.141.26.78' ){
 //	echo "<br />" . $bninfo;
 //}
 }
 $usethisarray = $banSeqArr;
 }
 
 foreach ($usethisarray as $key => $bannerinfo){
 if($key == 'lastupdate' || $key == 'leaderboardswurl' || $key == 'minor' || $key == 'wide' || $key == 'leaderboardsw'){
 continue;
 }
 $minorBanners = get_post_meta($postId, $key, true );
 if($minorBanners[status] == 'active' && ($minorBanners[image] != '' && $minorBanners[url] != '' || $minorBanners[customcode] != '') ){
 array_push($majorAds, $minorBanners);
 }
 }
 }
 elseif( $videoSpecificBanner['enable'] && trim($videoSpecificBanner['image']) != ''){
 $majorAds = array($videoSpecificBanner);
 }elseif($vsb['url'] != '' && $vsb['image'] != ''){
 $majorAds = array($vsb);
 }
 //Else get the sitewide banners
 else{
 $majorAds = array();
 if(!empty($wb_ent_options[banners][bannersequence])){
 $bannerSeq = $wb_ent_options[banners][bannersequence];
 
 foreach($bannerSeq as $key => $bninfo){
 if($key == 'lastupdate' || $key == 'leaderboardswurl' || $key == 'minor' || $key == 'wide' || $key == 'leaderboardsw'){
 continue;
 }
 $currentBanner =  $wb_ent_options[banners][$bninfo];
 $banSeqArr[$bninfo] = $currentBanner;
 }
 $usethisarray = $banSeqArr;
 
 foreach($usethisarray as $key => $bannerinfo){
 if($key == 'lastupdate' || $key == 'leaderboardswurl' || $key == 'minor' || $key == 'wide' || $key == 'leaderboardsw'){
 continue;
 }
 array_push($majorAds, $wb_ent_options['banners'][$key]);
 }
 }else{
 $count = 1;
 foreach ($wb_ent_options['banners'] as $key => $bannerinfo){
 if( isset($bannerinfo) && (trim($bannerinfo['image']) != '' || trim($bannerinfo['customcode']) != '' ) ){
 array_push($majorAds, $bannerinfo);
 $count++;
 }
 }
 }
 }
 
 */


$wb_current_ID = get_the_ID();
// Check if it's a page
if(  get_post_type($wb_current_ID) == "page" ){
    //check for page specific banners
    $pageId = get_the_ID();
    wb_get_page_banners();
    usort($postAds, "banner_sort");
    if ( $postAds ){
        $majorAds = $postAds;
    }
}elseif ( is_category() ){
    //check if there are specific category banners
    wb_get_category_banners();
    if ( $categoryAds ){
        $majorAds = $categoryAds;
    }
}else{
    //check for specific post banners
    wb_get_post_banners();
    if ( $postAds ){
        $majorAds = $postAds;
    }else{
        //check if there are banners assigned to category where this post belong
        wb_get_category_banners();
        if ( $categoryAds ){
            $majorAds = $categoryAds;
        }
    }
}

if ( !$majorAds ){
    //No banners assigned up to this point, sitewide banners will be loaded
    wb_get_sitewide_banners();
    $majorAds = $sitewideAds;
}

if ( isset($majorAds) && count($majorAds) > 0  ) {
    function cmp($a, $b){
        return strcmp($a["orderNumber"], $b["orderNumber"]);
    }
    
    ?>
    <div class="row-fluid">
        <ul class="thumbnails">
            <?php
            foreach ($majorAds as $majorAd) {
                if ( trim($majorAd['image']) != '' || trim($majorAd['customcode']) != '' ) {
                    if ($majorAd['target'] != ''){
                        $target = $majorAd['target'];
                    }
                    else{
                        $target = "_blank";
                    }
                    echo '
                <li class="span">';
                    if( trim($majorAd['customcode']) != '' ){
                        echo html_entity_decode($majorAd['customcode']);
                    }else{
                    	
                    	if (filter_var($majorAd['url'], FILTER_VALIDATE_EMAIL)) {
                    		$wb_banner_link = "mailto:" . $majorAd['url'];
                    	}else{
                    		$wb_banner_link = $majorAd['url'];
                    	}
                    	
                         echo '
                       <div class="sidebar-banner">
                         <script type="text/javascript">
                            ';
                        $imageFilenameTemp = explode('/', $majorAd['image']);
                        $imageFilename = $imageFilenameTemp[(count($imageFilenameTemp) - 1)];
                        foreach ($analytics as $key => $tracker) {
                            if (isset($majorAd['orderNumber']) && trim($majorAd['orderNumber']) != '') {
                                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner View\', \'' . $majorAd['orderNumber'] . '_300x250_' . $imageFilename . '\');';
                            } else {
                                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner View\', \'' . '300x250_' . $imageFilename . '\');';
                            }
                        }                 


                        echo '
                         </script>			
                         <a onclick="';
                        foreach ($analytics as $key => $tracker) {
                            if (isset($majorAd['orderNumber']) && trim($majorAd['orderNumber']) != '') {
                                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner Click\', \'' . $majorAd['orderNumber'] . '_300x250_' . $imageFilename . '\');';
                            } else {
                                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner Click\', \'' . '300x250_' . $imageFilename . '\');';
                            }
                        }
                        echo '" href="';
                        echo $wb_banner_link . '" target="'.$target;
                        echo '">';
                        $tempArray = explode('.', $majorAd['image']);
                        if (($tempArray[count($tempArray) - 1]) == 'swf') {
                            ?>
                            <object width="300" height="250">
                                <param name="movie" value="<?php echo $majorAd['image']; ?>">
                                <param name="wmode" value="transparent" />
                                <embed wmode="transparent" src="<?php echo $majorAd['image']; ?>" width="300" height="250">
                                </embed>
                            </object>	
                            <?php
                        } else {
                            echo '<img src="' . $majorAd['image'] . '" width="300" height="250" alt="' . $majorAd['title'] . '" />';
                        }

                        echo '</a>
                       </div>';                        
                    }
                    echo '
                </li>';
                }
            }
            ?>
        </ul>                
    </div><!-- END OF SPONSOR BANNERS -->   
    <?php
}

/*
if ( count($majorAds) <= 0 && isset($minorAds) && count($minorAds) > 0 && (trim($minorAds[0]['image']) != '' || trim($minorAds[0]['customcode']) != '' ) && $videoSpecificBanner['imgId'] == '' ){
    
	function cmp($a, $b){
		return strcmp($a["orderNumber"], $b["orderNumber"]);
	}
	usort($minorAds, "cmp");
	
	
	?>
<div class="row-fluid">
    <ul class="thumbnails">
        <?php        
            foreach ($minorAds as $minorAd) {
                if ( trim($minorAd['image']) != '' || trim($minorAd['customcode']) != '' ) {
                    if ($minorAd['target'] != ''){
                        $target = $minorAd['target'];
                    }
                    else{
                        $target = "_blank";
                    }
                    echo '
                <li class="span">';

                    if( trim($minorAd['customcode']) != '' ){
                        echo html_entity_decode($minorAd['customcode']);
                    }
                    else{
                    	
                    	if (filter_var($minorAd['url'], FILTER_VALIDATE_EMAIL)) {
                    		$wb_banner_link = "mailto:" . $minorAd['url'];
                    	}else{
                    		$wb_banner_link = $minorAd['url'];
                    	}
                    	
                        echo '
                       <div class="sidebar-banner">
                         <script type="text/javascript">
                            ';
                        $imageFilenameTemp = explode('/', $minorAd['image']);
                        $imageFilename = $imageFilenameTemp[(count($imageFilenameTemp) - 1)];
                        foreach ($analytics as $key => $tracker) {
                            if (isset($minorAd['orderNumber']) && trim($minorAd['orderNumber']) != '') {                            
                                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner View\', \'' . $minorAd['orderNumber'] . '_300x100_' . $imageFilename . '\');';
                            } else {                            
                                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner View\', \'' . '300x100_' . $imageFilename . '\');';
                            }
                        }
                        echo '
                         </script>			
                         <a onclick="';
                        foreach ($analytics as $key => $tracker) {
                            if (isset($minorAd['orderNumber']) && trim($minorAd['orderNumber']) != '') {                            
                                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner Click\', \'' . $minorAd['orderNumber'] . '_300x100_' . $imageFilename . '\');';
                            } else {                            
                                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner Click\', \'' . '300x100_' . $imageFilename . '\');';
                            }
                        }
                        echo '" href="';
                        echo $wb_banner_link . '" target="'.$target;
                        echo '">';
                        $tempArray = explode('.', $minorAd['image']);
                        if (($tempArray[count($tempArray) - 1]) == 'swf') {
                            ?>
                            <object width="300" height="100">
                                <param name="movie" value="<?php echo $minorAd['image']; ?>">
                                <param name="wmode" value="transparent" />
                                <embed wmode="transparent" src="<?php echo $minorAd['image']; ?>" width="300" height="100">
                                </embed>
                            </object>	
                            <?php
                        } else {
                            echo '<img src="' . $minorAd['image'] . '" width="300" height="100" alt="' . $minorAd['title'] . '" />';
                        }

                        echo '</a>
                       </div>';                        
                    }
                    

                    
                    echo '
                </li>';
                }
            }        
        ?>
    </ul>                
</div><!-- END OF SPONSOR BANNERS -->
<?php
}
*/
function banner_sort($a, $b){
	return strcmp($a["orderNumber"], $b["orderNumber"]);
}
?>