<?php
/*
  Filename:      subscribeCollapse.php
  Description:   collapsible subscribe box
  Author: 	   	Jullie Quijano
 */
?>
<div id="subscribe" class="wbc-sidemenu-style gradBox btnwb visible-desktop" style="">
    <script type="text/javascript">
        (function() {
            $("#subscribeHeader").addClass("collapsed_boxarrow");
            $('#subscribeHeader').click(function() {                
                if ($("#subscribeContent").is(":hidden")) {
                    //$("#subscribeContent").slideToggle("fast");
										//$("#subscribeContent img").slideDown("fast");
										$("#subscribeContent").show();
                    $("#subscribeHeader").toggleClass("collapsed_boxarrow expanded_boxarrow");
                    //$('h4#subscribeHeader').css('background', 'url("/images/sidebarCollapseArrowUpGray.png") no-repeat scroll 255px 0 transparent');
                } else {
                    //$("#subscribeContent").slideToggle("fast");
										//$("#subscribeContent img").slideUp("fast");
										$("#subscribeContent").hide();
                    //$('h4#subscribeHeader').css('background', 'url("/images/sidebarCollapseArrowDownGray.png") no-repeat scroll 255px 0 transparent');
                    $("#subscribeHeader").toggleClass("collapsed_boxarrow expanded_boxarrow");
                }
            });
        })(jQuery);
    </script>
    <h5 id="subscribeHeader" class="subs-h"><?= _e('Subscribe', 'enterprise') ?></h5>
    <div id="subscribeContent" class="subsIconDiv">
        
        <?php
        /*
        if( trim($wb_ent_options['emailsubslink']) != '' ){
            $emailsubslink = $wb_ent_options['emailsubslink'];
        }
        else{
            $emailsubslink = '/subcribe';
        }*/
        $count = 0;
       
        
        if( trim($wb_ent_options['rsslink']) != '' ){
            $rsslink = $wb_ent_options['rsslink'];
            $count++;
        }
        else{
            $rsslink = get_template_directory_uri().'/feed/mrss.php';
             $count++;
        }
        
        
        ?>
        <?php if( trim($wb_ent_options['emailsubslink']) != '' ){ 
            $emailsubslink = $wb_ent_options['emailsubslink'];
             $count++;?>
        <div>
            <a href="<?php echo $emailsubslink; ?>"><img width="60" height="60" src="<?php echo get_template_directory_uri() ?>/images/emailReg.png" alt="Email" /></a><br/>
            <a href="<?php echo $emailsubslink; ?>"><?= _e('Email', 'enterprise') ?></a>
        </div>
        <?php } ?>
        <div>
            <a href="<?php echo $rsslink; ?>" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/images/sub_rss.png" alt="RSS" /></a><br/>
            <a href="<?php echo $rsslink; ?>" target="_blank"><?= _e('RSS', 'enterprise') ?></a>
        </div>
        <?php
        if( trim($wb_ent_options['ituneslink']) != '' ){
            $count++;
        ?>
        <div>
            <a href="<?php echo $wb_ent_options['ituneslink'] ?>" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/images/sub_itunes.png" alt="iTunes" /></a><br/>
            <a href="<?php echo $wb_ent_options['ituneslink'] ?>" target="_blank"><?= _e('iTunes', 'enterprise') ?></a>
        </div>
        <?php
        }
        if($count == 1){            
          echo '<style>#subscribe #subscribeContent {margin-left: 80px;}</style>';  
        }
        ?>
        <?php /*
          <div style="float: left;margin:5px 10px;" >
          <a href="<?php echo youtube_channel ?>" target="_blank"><img src="<?php echo site_root_theme ?>images/sub_youtube.png" alt="Youtube" /></a><br/>
          <a href="<?php echo youtube_channel ?>" target="_blank">Youtube</a>
          </div>
          <div style="width:100%;clear:both; height: 1px;">
          &nbsp;
          </div>
         */ ?>
        <div style="width:100%;clear:both; height: 1px;">
          &nbsp;
          </div>
          <?php if ($wb_ent_options['hassubscribe']['about']) { ?>
         <div id="subsaboutText"  style="font-size: 11px;"><?php printf(__('%s', 'enterprise'), $wb_ent_options['channeldesc']); ?></div>	
         <?php } ?> 
        <style>
             #leftCol #subscribe .subsIconDiv {
            padding: 0px;
        }
        h4#subscribeHeader {
            /*background:url("/images/sidebarCollapseArrowDownGray.png") no-repeat scroll 255px 0 transparent;*/
            cursor:pointer;
            text-align:center;
            text-transform:uppercase;
            font-size:17.5px;
            color:#000;
            //line-height: 1.6em;
        }
        #leftCol #subscribe {
            /* margin-bottom:10px;*/
        }
        h4#subscribeHeader:hover {
            color: #FF0000;
        }
        #subscribeContent{
            margin-left: 20px; 
            text-align: center; 
            display: none; 
            //height: 0px; 
            overflow: hidden; 
            min-height: 90px;
        }
        #subscribeContent > div{
            float: left;
            <?php
            if( trim($wb_ent_options['ituneslink']) != '' && $count != 2) { ?>
                width: 80px;
            <?php            
            }
            else{ ?>
                width: 110px;
            <?php
            }
            ?>
        }
         #subscribeContent > #subsaboutText{
        width: 100%;
        text-align:left;
        line-height: 13px;
        margin-top: 10px;
        }
        
    </style>
      
    </div>
</div>
