<?php
/**
 * filename: videoSearchWidget.php
 * description: this will display the actual code for the player
 * author: Jullie Quijano
 * date created: 2014-04-16
 */

global $wb_ent_options, $postId, $privatePostsIds, $unlistedPostsIds,$current_lang;
        switch($current_lang){
        case 'en_US' :
        $catlibrary =  $wb_ent_options['videocats']['library'];
        break;
        case 'fr_FR' :
        $catlibrary = $wb_ent_options['videocats']['library-fr_FR'];
        break; 
        } 
//get categories
//get limit of videos per display
$numOfVidsLoad = ( intval($wb_ent_options['videolistinfo']['vidsperpagehome'] / 3) ) * 3;
if($numOfVidsLoad == ""){
    $numOfVidsLoad = 6;    
}
//$numOfVidsLoad = $wb_ent_options['videolistinfo']['vidsperpage'];
$categoryName = ($wb_ent_options['categorytopicname']['category']) ? $wb_ent_options['categorytopicname']['category'] : 'Category';
$topicName = ($wb_ent_options['categorytopicname']['topic']) ? $wb_ent_options['categorytopicname']['topic'] : 'Topic';
$enableApfSearch = $wb_ent_options['videosearchwidget']['enableApfSearch'];
if( $_SERVER['HTTPS'] == 'on' ){
   $protocol = "https";
}
else{
   $protocol = "http";
}
?>
<style>
    #mainContentVidList .hiddenVid{
        display: none;
    }
</style>   
<script type="text/javascript">
    var ver = new Date();
    $(function() {
        $('#prependedInput').keypress(function(e) {
            if (e.which == 13) {
                <?php if($enableApfSearch){ ?>
                var str = $('#prependedInput').val();
                var domain = '<?php echo $protocol.'://'.$wb_ent_options['channeldomain']; ?>';                
                var url = domain+"/?s=" + str;
                location.href = url;
                <?php }else{ ?>
                searchVideo();
                <?php } ?>
            }
        });
    });

    function displayCatVids(selectElement) {
        var categoryId = $(selectElement).val();
        

        if ($(selectElement).attr('id') == 'mainCatSelect') {
            $('#hiddenCat').val(categoryId);
        }
        else if ($(selectElement).attr('id') == 'subCategoryList') {
            $('#hiddenSubCat').val(categoryId);
        }
        $('#hiddenSearchWord').val('');
        $('#prependedInput').val('');

        $.ajax({
            type: "POST",
            url: "<?php echo get_template_directory_uri() ; ?>/resources/ajaxFunctions.php",
            data: "fx=getCatVids&currentPostId=<?php echo $postId; ?>&lang=<?php echo $current_lang; ?>&catId=" + categoryId,
            success: function(data) {

                var subData = eval('(' + data + ')');
                if (subData.success) {
                    $('#mainContentVidList').html('');
                    var videoCounter = 0;
                    $.each(subData.catVideos, function(key, value) {
                        //console.log("a = %o, b = %o", key, value);  
                        if (videoCounter < <?php echo $numOfVidsLoad; ?>) {
                            $('#mainContentVidList').append('<li class="span4"><div class="thumbnail"><a href="/' + value.postName + '"><img src="' + value.midThumb + '?v='+ver.getTime()+'" alt="' + value.title + '" /></a><a href="/' + value.postName + '"><h3>' + value.title + '</h3></a><p>' + value.desc + '</p><input type="hidden" value="'+value.postId+'" name="wb_ent_displayed_posts[]"></div></li>');
                            videoCounter++;
                        }
                        else {
                            $('#mainContentVidList').append('<li class="span4 hiddenVid"><div class="thumbnail"><a href="/' + value.postName + '"><img src="' + value.midThumb + '?v='+ver.getTime()+'" alt="' + value.title + '" /></a><a href="/' + value.postName + '"><h3>' + value.title + '</h3></a><p>' + value.desc + '</p><input type="hidden" value="'+value.postId+'" name="wb_ent_displayed_posts[]"></div></li>');
                        }                        

                    });

                    if (subData.totalVids > videoCounter) {
                        $('#loadVideoButton').show();
                    }
                    else {
                        $('#loadVideoButton').hide();
                    }
                }
                else {
                }
            }
        });
        $('#vidPage').val(1);
    }

    function displaySubCats(selectElement) {
        var categoryId = $(selectElement).val();
  // console.log("a = %o, b = %o", selectElement, categoryId);
        if( categoryId != 'default'){
            $.ajax({
                type: "POST",
                url: "<?php echo get_template_directory_uri() ; ?>/resources/ajaxFunctions.php",
                data: "fx=getSubCats&currentPostId=<?php echo $postId; ?>&catId=" + categoryId,
                success: function(data) {

                    var subData = eval('(' + data + ')');
                    if (subData.success) {
                        $('#subCategoryList').html('');
                        $('#subCategoryList').append('<option value="default"><?php echo $topicName ?></option>');                        

                        $.each(subData.subcats, function(key, value) {
                            $('#subCategoryList').append('<option value="' + value.term_id + '">' + value.name + '</option>');
                        });

                    }
                    else {
                        $('#subCategoryList').html('');
                        $('#subCategoryList').append('<option value="default"><?php echo $topicName; ?></option>');
                    }



                }
            });            
        }
        else{
               $('#subCategoryList').html('');
               $('#subCategoryList').append('<option value="default"><?php echo $topicName ?></option>');            
        }

        $('#vidPage').val(1);
    }

    function searchVideo() {
        $('#hiddenSearchWord').val($('#prependedInput').val());
        $('#lang').val('');
        $('#hiddenCat').val('');
        $('#hiddenSubCat').val('');


        $.ajax({
            type: "POST",
            url: "<?php echo get_template_directory_uri() ; ?>/resources/ajaxFunctions.php",
            data: "fx=getSearchVids&currentPostId=<?php echo $postId; ?>&searchWord=" + $('#prependedInput').val()+"&lang=<?php echo $current_lang ?>",
            success: function(data) {

                var subData = eval('(' + data + ')');
                if (subData.success) {
                    $('#mainContentVidList').html('');
                    var videoCounter = 0;
                    $.each(subData.searchVideos, function(key, value) {
                        //console.log("a = %o, b = %o", key, value);  
                        if (videoCounter < <?php echo $numOfVidsLoad; ?>) {
                            $('#mainContentVidList').append('<li class="span4"><div class="thumbnail"><a href="/' + value.postName + '"><img src="' + value.midThumb + '?v='+ver.getTime()+'" alt="' + value.title + '" /></a><a href="/' + value.postName + '"><h3>' + value.title + '</h3></a><p>' + value.desc + ' </p><input type="hidden" value="'+value.postId+'" name="wb_ent_displayed_posts[]"></div></li>');
                            videoCounter++;
                        }
                        
                    });
                    $('#subCategoryList').html('');
                    $('#subCategoryList').append('<option value="default"><?php echo $topicName; ?></option>');
                    $('#mainCatSelect[val="default"]').attr('selected', 'selected');
                    $('#mainCatSelect').val('default');

                    if (subData.totalVids > videoCounter) {
                        $('#loadVideoButton').show();
                    }
                    else {
                        $('#loadVideoButton').hide();
                    }

                }
            }
        });

        $('#vidPage').val(1);
    }

    function loadMoreVids() {
        var mode = 'all'; //can be cat or search
        var vidPage = $('#vidPage').val();

        var catForVid = <?php echo $catlibrary; ?>;

        if (parseInt(vidPage) == NaN) {
            vidPage = 1;
        }
        else {
            vidPage = parseInt(vidPage);
        }

        if ($.trim($('#hiddenSearchWord').val()) != '') {
            mode = 'search';
        }
        else if ($.trim($('#hiddenSubCat').val()) != '' && $.trim($('#hiddenSubCat').val()) != 'default') {
            mode = 'catVid';
            catForVid = $.trim($('#hiddenSubCat').val());
        }
        else if ($.trim($('#hiddenCat').val()) != '' && $.trim($('#hiddenCat').val()) != 'default') {
            mode = 'catVid';
            catForVid = $.trim($('#hiddenCat').val());
        }

        if (mode == 'search') {
            $.ajax({
                type: "POST",
                url: "<?php echo get_template_directory_uri() ; ?>/resources/ajaxFunctions.php",
                data: "fx=loadMoreVids&searchWord=" + $('#prependedInput').val() + "&vidPage=" + vidPage+"&"+jQuery('input[name="wb_ent_displayed_posts[]"]').serialize(),
                success: function(data) {

                    var subData = eval('(' + data + ')');
                    if (subData.success) {
                        videoCounter = 0;
                        $.each(subData.searchVideos, function(key, value) {
                            //console.log("a = %o, b = %o", key, value);  
                            if (videoCounter < <?php echo $numOfVidsLoad; ?>) {
                                $('#mainContentVidList').append('<li class="span4"><div class="thumbnail"><a href="/' + value.postName + '"><img src="' + value.midThumb + '?v='+ver.getTime()+'" alt="' + value.title + '" /></a><a href="/' + value.postName + '"><h3>' + value.title + '</h3></a><p>' + value.desc + '</p><input type="hidden" value="'+value.postId+'" name="wb_ent_displayed_posts[]"></div></li>');
                                videoCounter++;
                            }
                        });
                        $('#subCategoryList').html('');
                        $('#subCategoryList').append('<option value="default"><?php echo $topicName; ?></option>');
                        $('#mainCatSelect[val="default"]').attr('selected', 'selected');
                        $('#mainCatSelect').val('default');

                        if (subData.totalVids > <?php echo $numOfVidsLoad; ?>) {
                            $('#loadVideoButton').show();
                        }
                        else {
                            $('#loadVideoButton').hide();
                        }


                    }
                    else {
                        $('#loadVideoButton').hide();
                    }
                }
            });
            $('#vidPage').val(vidPage + 1);
        }
        else {
            //console.log("loadmore");
            $.ajax({
                type: 	"POST",
                url: 	"<?php echo get_template_directory_uri() ; ?>/resources/ajaxFunctions.php",
                data: 	"fx=loadMoreVids&categoryId=" + catForVid + "&vidPage=" + vidPage+"&"+jQuery('input[name="wb_ent_displayed_posts[]"]').serialize(),
                success: function(data) {
                	//console.log(data);
                    var subData = eval('(' + data + ')');
                    if (subData.success) {
                        videoCounter = 0;
                        $.each(subData.searchVideos, function(key, value) {
                            //console.log("a = %o, b = %o", key, value);  
                            if (videoCounter < <?php echo $numOfVidsLoad; ?>) {
                                $('#mainContentVidList').append('<li class="span4"><div class="thumbnail"><a href="/' + value.postName + '"><img src="' + value.midThumb + '?v='+ver.getTime()+'" alt="' + value.title + '" /></a><a href="/' + value.postName + '"><h3>' + value.title + '</h3></a><p>' + value.desc + '</p><input type="hidden" value="'+value.postId+'" name="wb_ent_displayed_posts[]"></div></li>');
                                videoCounter++;
                            }
                        });
                        //$('#subCategoryList').html('');
                        //$('#subCategoryList').append('<option value="default">TOPIC</option>');            
                        //$('#mainCatSelect[val="default"]').attr('selected', 'selected');
                        //$('#mainCatSelect').val('default');                

                        if (subData.totalVids > <?php echo $numOfVidsLoad; ?>) {
                            $('#loadVideoButton').show();
                        }
                        else {
                            $('#loadVideoButton').hide();
                        }
                        $(".ie8 #video-thumbnails [class*='span']:nth-child(3n+1)").css( "margin-left", "0" );


                    }
                    else {
                        $('#loadVideoButton').hide();
                    }
                }
            });
            $('#vidPage').val(vidPage + 1);
        }







    }
</script>
 <?php if($wb_ent_options['videosearchwidget']['enable']) { ?>
<div id="cat-nav" class="row-fluid">

    <!-- Single button -->
    <div class="span4 btn-block">
    <?php if (trim($wb_ent_options['videosearchwidget']['replacecategory'])) {
            echo html_entity_decode($wb_ent_options['videosearchwidget']['replacecategory']);
          } else {
    ?>
		<?php $searchwidget_category_name = strtoupper($wb_ent_options['categorytopicname']['category']); ?>
        <select class="selectpicker btn-block btnwb" title="<?php echo $categoryName; ?>" onchange="displaySubCats(this); displayCatVids(this);" id="mainCatSelect" name="mainCatSelect"> 
            <!--<option value="default"><?= _e('CATEGORY', 'enterprise') ?></option>-->
            <option value="default"><?php echo $categoryName ?></option>
            <?php
            if (!isset($postCategories) || count($postCategories) <= 0) {
                $postCategories = wb_get_categories();
            }
            foreach ($postCategories as $currentCat) {
                ?>
                <option value="<?php echo $currentCat['cat_id']; ?>"><?php printf(__('%s', 'enterprise'), $currentCat['cat_name']) ?></option>  
                <?php
            }
            ?>          
        </select>         
    <?php } ?>             
    </div>
    <div class="span4">
    <?php 
    
    if (trim($wb_ent_options['videosearchwidget']['replacetopic'])) {
            echo html_entity_decode($wb_ent_options['videosearchwidget']['replacetopic']);
          } else {
    ?>
		<?php $searchwidget_topic_name = strtoupper($wb_ent_options['categorytopicname']['topic']); ?>
        <select class="selectpicker btn-block btnwb" id="subCategoryList" title="<?php echo $topicName ?>" onchange="displayCatVids(this);">            
            <!--<option value="default"><?= _e('TOPIC', 'enterprise') ?></option>-->
						<option value="default"><?php echo $topicName ?></option>         
        </select>
    <?php }  ?>
    </div>

    <div class="span4">
    <?php
    if (trim($wb_ent_options['videosearchwidget']['replacesearch'])) {
            echo html_entity_decode($wb_ent_options['videosearchwidget']['replacesearch']);
          } else {
    ?>
        <div class="input-prepend btn-group">
            <span class="add-on"><?= _e('Search', 'enterprise') ?></span>
            <input class="span" id="prependedInput" type="text" placeholder="">
        </div> 
     <?php } ?>
    </div>
</div>
 <?php } ?>
<div style="clear:both;" class="spacing"></div>
<div id="video-thumbnails" class="row-fluid">
    <ul class="thumbnails" id="mainContentVidList">
        <?php
        switch($current_lang){
        case 'en_US' :
        $catlibrary =  $wb_ent_options['videocats']['library'];
        break;
        case 'fr_FR' :
        $catlibrary = $wb_ent_options['videocats']['library-fr_FR'];
        break; 
        }
        $result = wb_get_cat_vids($catlibrary, '', false, $wb_ent_options['videolistinfo']['vidsperpagehome']);
        //echo '$result is '.print_r($result, true);
        $allVideos = $result['posts'];
        $numRows = $result['numPosts'];

        $i = 0;
        foreach ($allVideos as $currentVideo) {            
            
            if( ( !is_user_logged_in() ) && (in_array( $post_id,  $privatePostsIds) || in_array( $post_id,  $unlistedPostsIds)  ) ){
                continue;
            }
            ?>
            <li class="span4">
                <div class="thumbnail">
                    <a href="<?php echo $currentVideo['postLink']; ?>"><img style="width: 240px; height: auto;" src="<?php echo $currentVideo['midThumb'].'?v='.time(); ?>" alt="<?php echo $currentVideo['title']; ?>" /></a>
                    <a href="<?php echo $currentVideo['postLink']; ?>"><h3><?php echo wb_format_string( $currentVideo['title'], false, true, $wb_ent_options['videolistinfo']['titlelimit'], '...'); ?></h3></a>
                    <p><?php echo wb_format_string( $currentVideo['desc'], false, true, $wb_ent_options['videolistinfo']['desclimit'], '... <a href="'.$currentVideo['postLink'].'"><span class="link">[more]</span></a>'); ?></p>
                    <input type="hidden" value="<?php echo $currentVideo['postId']; ?>" name="wb_ent_displayed_posts[]">
                </div>
            </li>              
            <?php
            $i++;
            
            if ($i >= $numOfVidsLoad) {
                break;
            }
        }

        if ($i < $numRows ) {
            $displayButton = '';
        }
        else{
            $displayButton = ' style="display: none; "';
        }
        ?>            



    </ul>
            <!-- START OF button (Load more videos) -->        
            <div class="container-narrow">
                <div class="jumbotron">
                    <input type="hidden" name="hiddenSearchWord" id="hiddenSearchWord" />
                    <input type="hidden" name="hiddenCat" id="hiddenCat" />
                    <input type="hidden" name="hiddenSubCat" id="hiddenSubCat" />
                    <input type="hidden" name="vidPage" id="vidPage" value="1" />
                    <input type="hidden" name="lang" id="lang" value="<?php echo $current_lang; ?>" />
                    <button <?php echo $displayButton; ?> class="btn btn-large btn-success" type="button" onclick="loadMoreVids();" id="loadVideoButton" name="loadVideoButton"><?= _e('Load more', 'enterprise') ?></button>

                </div>
            </div>      
</div>