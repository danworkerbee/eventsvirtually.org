<?php
/*
Filename:      postLogo.php                                                 
Description:	this will display a postLogo when the plugin is activated   
Author:        Jullie Quijano
Change Log:
   Nov 25, 2013   [Jullie]added code to display message if file is included
	February 28, 2012	[Jullie]added values for img alt; 
	July 21, 2011  [Jullie]created file                                                                                                                                                                                                            
*/
//will determine if file is included
if($_SERVER['REMOTE_ADDR'] == WORKERBEE_IP){
   echo '
   
<!-- START OF '. __FILE__ .' -->
   
   ';
}


		$enable_post_logo = get_post_custom_values('_enable_post_logo', $post_id);
		$post_logo_link 	= get_post_custom_values('_post_logo_link', $post_id);
		$post_logo_title 	= get_post_custom_values('_post_logo_title', $post_id);
		$post_logo_pos = get_post_custom_values('_post_logo_pos', $post_id);
		$post_logo_image 	= get_post_custom_values('_post_logo_image', $post_id);
		
		$enable_post_logo = is_array($enable_post_logo) ? $enable_post_logo[0]: $enable_post_logo;
		$post_logo_link = is_array($post_logo_link) ? $post_logo_link[0]: $post_logo_link;
		$post_logo_title = is_array($post_logo_title) ? $post_logo_title[0]: $post_logo_title;
		$post_logo_pos = is_array($post_logo_pos) ? $post_logo_pos[0]: $post_logo_pos;
		$post_logo_image = is_array($post_logo_image) ? $post_logo_image[0]: $post_logo_image;
		
		if(isset($post_logo_image) && trim($post_logo_image) != '' && $enable_post_logo == 'enabled' ){
			echo '<img src="'.urldecode($post_logo_image).'" class="descImage" title="'.$post_logo_title.'" alt="'.$post_logo_title.'" />';	
		}
		else{
			echo '<img src="'.POST_LOGO_IMG.'" class="descImage visible-desktop"  style="margin: 10px 0 0;" title="'.CLIENT_CHANNEL.'" alt="'.CLIENT_CHANNEL.'" />';                        
		}
      
      
      
//will determine if file is included
if($_SERVER['REMOTE_ADDR'] == WORKERBEE_IP){
   echo '
   
<!-- END OF '. __FILE__ .' -->
   
   ';
}		
	?>	