<?php
/**
 * filename: adsWideCustom.php
 * description: will display custom ads for the 468x60 banner below the video
 * author: Jullie Quijano
 * date created: 2014-04-23
 *
 */
global $postId, $wb_ent_options, $majorAdsW, $categoryAdsW, $postAdsW, $sitewideAdsW, $included_bannersW;
$wb_ent_options 		= get_option('wb_ent_options');
$analytics 				= $wb_ent_options['analytics'];
$majorAdsW				= array();
$categoryAdsW			= array();
$postAdsW				= array();
$sitewideAdsW			= array();
$included_bannersW		= array('wide');
$wide_banner_add		= array();


function wb_get_sitewide_bannersW(){
    global $wb_ent_options, $sitewideAdsW, $included_bannersW;
    
    $sitewideAdsW= $wb_ent_options['banners']['wide'];
    if($sitewideAdsW['status'] != "active"){
        $sitewideAdsW= array();
    }
}

function wb_get_category_bannersW(){
    global $postId, $wb_ent_options, $categoryAdsW, $included_bannersW;
    if (is_category()) {
        $category = get_category(get_query_var('cat'));
        $cat_id = $category->cat_ID;
        $category_banner_sequence = get_term_meta($cat_id,'wb_ad_banner_sequence',true);
        foreach ( $category_banner_sequence as $current_banner_key ){
            if ( in_array( $current_banner_key, $included_bannersW) ){
                $categoryBanner = get_term_meta($cat_id,$current_banner_key,true);
                array_push($categoryAdsW, $categoryBanner);
            }
        }
    }else{
        if ( get_post_type($postId) == "post" ){
            $wb_categories = wp_get_post_categories($postId);
            rsort($wb_categories);
            //check if post's category has specific banners
            foreach ( $wb_categories as $current_category ){
                $category_banner_sequence = get_term_meta($current_category,'wb_ad_banner_sequence',true);
                if ( $category_banner_sequence ){
                    foreach ( $category_banner_sequence as $current_banner_key ){
                        if ( in_array( $current_banner_key, $included_bannersW) ){
                            $categoryBanner = get_term_meta($current_category,$current_banner_key,true);
                            array_push($categoryAdsW, $categoryBanner);
                        }
                    }
                    if ( $categoryBanner ){
                        break;
                    }
                }
            }
        }
    }
}

function wb_get_post_bannersW(){
    global $postId, $wb_ent_options, $postAdsW, $included_bannersW;
    if ( get_post_type($postId) == "post" ){
        $post_banner_sequence = get_post_meta($postId,'wb_ad_banner_sequence',true);
        if ( $post_banner_sequence ){
            foreach ( $post_banner_sequence as $current_banner_key ){
                if ( in_array( $current_banner_key, $included_bannersW) ){
                    $postBanner = get_post_meta( $postId, $current_banner_key, true );
                    array_push($postAdsW, $postBanner);
                }
            }
        }
    }
}

$currentAdTitle = $wb_ent_options['channelname'] . " - Wide Banner";
wb_get_post_bannersW();

if ( $postAdsW && !is_category() ){
    $wide_banner_add = $postAdsW[0];
}else{
    wb_get_category_bannersW();
    if ( $categoryAdsW ){
        $wide_banner_add = $categoryAdsW[0];
    }else{
        wb_get_sitewide_bannersW();
        if ( $sitewideAdsW ){
            $wide_banner_add = $sitewideAdsW;
        }
    }
}

if ( $wide_banner_add){
    if ( $wide_banner_add['image'] != "" ){
        //Wide banner is image
        $imageFilenameTemp = explode('/', $wide_banner_add[0]['image']);
        $imageFilename = $imageFilenameTemp[ (count($imageFilenameTemp)- 1) ];
        echo '
		<div id="adActionDiv">
         <script type="text/javascript">
            ';
        foreach ($analytics as $key=>$tracker) {
            if( isset($currentAdOrderNumber) && trim($currentAdOrderNumber) != ''){
                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner View\', \'' . $currentAdOrderNumber . '_468x60_' . $imageFilename . '\');';
            } else {
                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner View\', \'' . '468x60_' . $imageFilename . '\');';
            }
        }
        echo '
         </script>
			<a target="'.$target.'" onclick="';
        foreach ($analytics as $key=>$tracker) {
            if( isset($currentAdOrderNumber) && trim($currentAdOrderNumber) != ''){
                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner Click\', \'' . $currentAdOrderNumber . '_468x60_' . $imageFilename . '\');';
            } else {
                echo 'ga(\'pageTracker' . $key . '.send\', \'event\', \'Channel\', \'Banner Click\', \'' . '468x60_' . $imageFilename . '\');';
            }
        }
        echo '"';
        echo ' href="'.$wide_banner_add['url'].'"';
        echo '>';
        
        $tempArray =  explode('.', $currentAdImage);
        if( ($tempArray[count($tempArray)-1]) == 'swf' ){
            ?>
					<object width="468" height="60">
						<param name="movie" value="<?php echo $currentAdImage; ?>">
						<param name="wmode" value="transparent" />
						<embed wmode="transparent" src="<?php echo $currentAdImage; ?>" width="468" height="60">
						</embed>
					</object>	
				<?php
				}
				else{
					echo '<img src="'.$wide_banner_add['image'].'" width="468" height="60" alt="'.$currentAdTitle.'" />';
				}	
				
				echo '</a>
			<div style="clear:both; width:100%">&nbsp;</div>
		</div>';
	}else{
		//Wide banner is custom code
		echo html_entity_decode($wide_banner_add['customcode']);
		?>
		<style>
			#video-post-page article footer div{
				margin: 0 auto 25px;
			}
		</style>
		<?php 
	}
}
?>