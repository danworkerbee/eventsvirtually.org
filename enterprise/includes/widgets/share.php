<?php
/**
 * filename: share.php
 * description: this will be display the embed code(if enabled) when a user is logged in; this will also display the sharethis bar when the sharetype is static
 * author: Jullie Quijano
 * date created: 2014-03-28
 */
?>
<div id="share" class="gradBox">
    <?php
// Get rid of dumb carriage returns! This is a line feed only zone.
    $printPostContent = urldecode(str_replace('%0D', '<br />', urlencode($video['desc'])));


//check for short code
    $postMediaId = $video['mediaId'];
    $postPublisher = $wb_ent_options['brightcoveinfo']['publisherid'];
    $postPlayer = $wb_ent_options['bcplayers']['embed'];


    if (preg_match('/\\[VIDEO(.*)videoid([ ]*)=([ ]*)"(.*)"(.*)"ceoshow"(.*)\\/\\]/i', $printPostContent, $matches) !== false && count($matches) > 0) {
        $postMediaId = $matches[4];
        $postPublisher = $wb_ent_options['vidshortcode']['pubId'];
        $postPlayer = $wb_ent_options['vidshortcode']['playerId'];
    } else if (trim($postMediaId) == '' && preg_match('/\\[VIDEO([ ]*)videoid([ ]*)=([ ]*)["](.*)["](.*)\\/\\]/i', $printPostContent, $matches) !== false && count($matches) > 0) {
        $postMediaId = $matches[4];
    }

    $playerEmbedCode = '&lt;!-- Start of ' . $clientChannel . ' Player --&gt;';
    //$playerEmbedCode .= '<div style="display:none"> </div> <script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script> <object id="myExperience' . $postMediaId . '" class="BrightcoveExperience"> <param name="bgcolor" value="#FFFFFF" /> <param name="width" value="480" /> <param name="height" value="270" /> <param name="playerID" value="' . $postPlayer . '" /> <param name="publisherID" value="' . $postPublisher . '"/> <param name="isVid" value="true" /> <param name="isUI" value="true" /> <param name="wmode" value="transparent" /> <param name="@videoPlayer" value="' . $postMediaId . '" /> </object>';
    $playerEmbedCode .= htmlentities('<meta charset="utf-8" /> <div id="playerDiv"> <div id="player"> <div style="display: block; position: relative; max-width: 100%;"> <div style="display: block; padding-top: 56.25%;"> <video id="WBplayer" data-video-id="'.$postMediaId.'" data-account="'.$wb_ent_options['brightcoveinfo']['publisherid'].'" data-player="' . $postPlayer . '" data-embed="default" class="video-js" controls="" style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;"></video> <script src="//players.brightcove.net/'.$wb_ent_options['brightcoveinfo']['publisherid'].'/'.$postPlayer.'_default/index.min.js"></script> </div> </div> </div> </div>');
    $playerEmbedCode .= '&lt;!-- End of ' . $clientChannel . ' Player --&gt;';      

    if ($wb_ent_options['sharetype'] == 'static') {
        ?>
        <div id="share-this-div">
            <div id="share-box"><?= _e('Share/email this video', 'enterprise') ?></div>	
            <div id="share-this-video">
                <span class="st_twitter_large"></span>
                <span class="st_facebook_large"></span>
                <span id="linkedIn_button" class="st_linkedin_large"></span>
    <?PHP /* <span class="st_stumbleupon_large"></span> */ ?>	
                <img style="margin-bottom: 2px; cursor: pointer;" src="/images/googleplus.png"									onclick="javascript:(function() {
                            var w = 480;
                            var h = 380;
                            var x = Number((window.screen.width - w) / 2);
                            var y = Number((window.screen.height - h) / 2);
                            window.open('https://plusone.google.com/_/+1/confirm?hl=en&url=' + encodeURIComponent(location.href) + '&title=' + encodeURIComponent(document.title), '', 'width=' + w + ',height=' + h + ',left=' + x + ',top=' + y + ',scrollbars=no');
                        })();" />
                <!-- adding qr code -->

                <!-- adding qr code -->
                <script type="text/javascript"> function getBitly() {
                        $.getJSON('http://api.bit.ly/v3/shorten?', {'format': 'json', 'login': 'videoqrcode', 'apiKey': 'R_4fe5b3cdf79e449b9aa43d63b9ae11ad', 'longUrl': location.href + '?utm_campaign=qrCode&utm_source=qrCode&utm_medium=share&utm_content=' + encodeURIComponent($('meta[property="og:title"]').attr('content')) + '&utm_name=' + encodeURIComponent($('meta[property="og:description"]').attr('content'))}, function(response) {
                            var w = 350;
                            var h = 350;
                            var x = Number((window.screen.width - w) / 2);
                            var y = Number((window.screen.height - h) / 2);
                            window.open(response.data.url + '.qrcode', '_blank', 'width=' + w + ',height=' + h + ',left=' + x + ',top=' + y + ',scrollbars=no');
                        });
                    }</script>
                <a href="javascript: getBitly();"> <span style="margin-left: 3px !important;" > <img src="<?php echo get_template_directory_uri(); ?>/images/qrIcon.jpg" title="QR Code" /> </span> </a>
                <!-- end qr code -->    

                <span class="st_email_large"></span>
                <span class="st_sharethis_large"></span>
            </div>	 
        </div>
        <iframe src="http://www.facebook.com/plugins/like.php?href=<?php echo $video['postLink']; ?>&amp;layout=standard&amp;show_faces=true&amp;width=655&amp;action=like&amp;font=segoe+ui&amp;colorscheme=light" scrolling="no" frameborder="0" style="margin:10px 0 0 0; border:none; overflow:hidden; height: 80px; width: 655px;" allowTransparency="true"></iframe>


<br clear="all" />  
    <?php
}
?>
    <!-- 
       This styling is for when only the permalink is visible(the embed is not displayed).
    -->
    
    <div id="permalink-embed-div">
<?php
$userInfo = wp_get_current_user();
$current_user_roles = $userInfo->roles;

if (is_user_logged_in() && ( in_array('editor', $current_user_roles) || in_array('administrator', $current_user_roles) ) ) {    
    ?>
        <style type="text/css">
        #share{
            width: 100%;
            margin-bottom: 0px;
        }
        #share{
                margin-bottom: 30px !important;
            }
        #share-this-div{
            width: 300px;
            display: inline-block;
            zoom: 1;
            *display: inline;
        }
        #share-box{
            margin-bottom: 5px;
        }
        #share-this-video{
            display: inline-block;
            width: 300px;			
        }
        #permalink-embed-div{
            display: inline-block;
            max-width: 600px;
            vertical-align: top;
            margin-left: 10px;		
            zoom: 1;
            *display: inline;
        }
        #permalink-embed-div label{
            width: 100%;
            margin-top: 0px;
        }	
        #permalink-embed-div input{
            width: 200px;
            margin-right: 5px;
        }		
        #permalink-embed-div textarea{
            width: 250px;
            height: 100px;
            margin-right: 5px;
            border: solid 1px #a3a3a3;
            overflow: hidden;
        }			
        #permalink-embed-div object{
            vertical-align:	sub;			
        }		
        *+#permalink-embed-div object{
            vertical-align: text-top;
        }
    </style>
            <script type="text/javascript">
                $(function() {
                    $('#permalink-embed-div textarea').click(function() {
                        this.select();
                    });
                });
            </script>
            <div class="labelInputDiv" > 
                <label for="permalinkBox"><?php _e( 'Permalink', 'enterprise' ); ?></label>
                <textarea class="text" id="permalinkBox"><? the_permalink(); ?></textarea>
                <!-- <input type="text" class="text" name="shareLink" value="<? the_permalink(); ?>" id="permalinkBox" /> -->
            </div>
            <div class="labelInputDiv">
                <label for="embedCodeBox"><?php _e( 'Embed Code', 'enterprise' ); ?></label>
                <textarea class="text" id="permalinkBox"><?= $playerEmbedCode; ?></textarea>
                <!-- <input type="text" class="text" name="shareEmbed" value="<?= @$playerEmbedCode ?>" id="embedCodeBox" /> -->
            </div>
            <style>
                #permalink-embed-div input{
                    width: 145px;	
                }
                #permalink-embed-div label{
                    width: 145px;	
                    display: inline;
                    margin-bottom: 0px;
                }
                #permalink-embed-div .labelInputDiv{
                    width: 250px;
                    float: left;
                    margin-right: 49px;
                }
            </style>

            <?php
        }        
?>

    </div>


</div>
