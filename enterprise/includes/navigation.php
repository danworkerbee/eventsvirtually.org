<?php
/**
 * filename: navigation.php
 * description: 
 * author: Jullie Quijano
 * date created: 2014-03-25
 * 
 */
global $wb_ent_options;
//Back to Main Site link 
$mainsitelink = ($wb_ent_options['mainsitelink']) ? $wb_ent_options['mainsitelink'] : '/';
      ?>
      <!-- start of #page : ios responsive header -->
      <div id="page">
         <!-- TOP Header Back to the main site Link -->
         <div id="top-header" class="hidden-desktop" style="border: 0px solid black;">
            <div class="navbar-inverse">
               <div class="navbar-inner">
                  <div class="">
                     <a class="" href="<?php echo $mainsitelink; ?>"><i class="icon-arrow-left icon-white"></i> <?php _e( 'Back to the main site', 'enterprise' ); ?></a>
                     <!-- <img src="" style="float:right;">  If the client has small icon-->
                  </div>
               </div>
            </div>
         </div>
         <!-- End of top header -->
         <!-- Middle header that contains the client name/logo -->
         <div id="header" class="hidden-desktop">
             <a href="#menu-left"></a><a href="<?php echo $mainsitelink; ?>"><img src="<?php echo $wb_ent_options[customstyle][headerlogo]; ?>"></a>
            <!-- <h2><?php /*echo $wb_ent_options['channelname']; */ ?></h2> -->
            <!-- <a href="#menu-right" class="browseby right" >Browse by</a> -->
         </div>
         <!-- start of Subscription button -->
         <?php
        if ($wb_ent_options['hassubscribe']) {
            ?>
         
         <div class="container-fluid" id="subscribe-container">
            <div class="row-fluid">
                        
               <div class="jumbotron hidden-desktop" id="subscribe-button">
                  <div class="btn btn-large" type="button">
                      <?php _e( 'Subscribe', 'enterprise' ); ?>
                      <?php
                      if( trim($wb_ent_options['ituneslink']) != '' ){
                      ?>
                      <a href="<?php echo $wb_ent_options['ituneslink']; ?>"> 
                      <img src="<?php echo get_template_directory_uri(); ?>/images/sub_itunes_mini.png" /></a>                      
                      <?php
                      }
                      ?>
                      <?php if( trim($wb_ent_options['emailsubslink']) != '' ){ ?>
                      <a href="<?php echo $wb_ent_options['emailsubslink']; ?>"> <img src="<?php echo get_template_directory_uri(); ?>/images/sub_emailReg_mini.png" /></a>
                      <?php } ?>
                      
                      <a href="<?php echo $wb_ent_options['rsslink']; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/sub_rss_mini.png" /></a>
                  </div>
               </div>
            </div>
         </div>
         
        <?php } ?>
         <!--  end of Subscription button -->
         <!-- start of #menu-left : The ios menu dropdown of the browseby -->
         <nav id="menu-left" class="">
         <ul class="nav">
            <div class="mm-search">
               <form role="search" method="get" id="searchform" action="/">
                  <input type="text" value="Search Videos" onblur="if (this.value === '') { this.value = 'Search Videos'; }" onfocus="if (this.value === 'Search Videos') { this.value = ''; }" maxlength="200" name="s" id=""/>
               </form>
            </div>
            <?php if($wb_ent_options[hastvheaderlink]) {?>
                          <li><a href='/'  style="text-transform:none;"><?php printf( __('%s', 'enterprise'), $wb_ent_options[channelname]); ?></a></li> 
            <?php }  
                  if($wb_ent_options[hasclientlink]) {?>
                          <li><a href='<?php echo $wb_ent_options['clientsiteurl'] ?>'  style="text-transform:none;"><?php printf( __('%s', 'enterprise'), $wb_ent_options[clientname]); ?></a></li> 
            <?php } ?>
            <li>
                <a href='<?php echo esc_url( home_url( '/' ) ); ?>' <?php if ($current_page == 'home') echo ' class="current"'; ?>><?php _e( 'Latest Video', 'enterprise' ); ?></a>
            </li>
            <?php if(!$wb_ent_options['wbtvnavigation']['hideBrowse']){ ?>
            <li class="dropdown"><span class="mm-subtitle"><a href="#mm-m0-p1" class="dropdown-toggle mm-fullsubopen" data-toggle="dropdown"><?php _e( 'Browse', 'enterprise' ); ?></a></span>
                <?php
                include get_template_directory() . '/includes/browsebymenu.php';
                ?>                            
            </li>
            <?php } 
            $naviSelected = $wb_ent_options[wbtvnavigation][custnavigation];
            //echo "This is navi : ".$naviSelected;
            if($naviSelected == 'default'){ $navilinks = 'default'; }
              else if($naviSelected == 'categoryNavigation'){ $navilinks = 'categoryNavigation'; }
              else if($naviSelected = 'customNavigation' && $wb_ent_options[wbtvnavigation][links] != ""){ $navilinks = 'customNavigation'; }
            else{ $navilinks = 'default'; } 
            
            if($navilinks == 'default') {
            	/*
            ?>
            <li>
                <a href='<?php echo esc_url( home_url( '/' ) ); ?>' <?php if ($current_page == 'home') echo ' class="current"'; ?>><?php _e( 'Latest Video', 'enterprise' ); ?></a>
            </li>
            <?php
            if( trim($wb_ent_options['notification']['contact']['url']) != '' ){
                $contactUrl = trim($wb_ent_options['notification']['contact']['url']);
            }
            else{
                $contactUrl = get_site_url().'/contact';
            }
            ?>
            <li><a href='<?php echo $contactUrl; ?>'<?php if ($current_page == 'contact') echo ' class="current"'; ?>>Contact Us</a></li>
            <?php         
            /*
            if (HAS_CVIDEOS) {                
            ?>
            <!--<li class="dir" id="cVideos-Dir"> -->
            <li class="dropdown" id="com-vid"><span class="mm-subtitle"><a href="#mm-m0-p7" class="dropdown-toggle mm-fullsubopen" data-toggle="dropdown">Community Videos</a></span>
            <a href='<?php echo get_site_url().'/' ?>community-videos' > Community Videos </a>
            <ul class="dropdown-menu">
               <li class="dropdown-submenu">
                   <?php                            
                   echo '<a class="square" id="browse-cvideos" href="' . get_site_url().'/' . 'category/community-videos"';                            
                   if (count($cVideoCat) <= 0) {                                
                       echo ' class="noArrowNav" ';                                                   
                   }                            
                   echo '>Browse Videos </a>
                   <ul class="dropdown-menu">
                       '; 
                   foreach ($cVideoCat as $currentCVideoCat) { 
                      if ($currentCVideoCat['count'] >= 1) {
                          echo '
                      <li class=""><a class="square cVideoCat" href="' . get_site_url().'/' . 'category/' . $currentCVideoCat['slug'] . '">' . ucwords($currentCVideoCat['cat_name']) . '</a></li>';
                      }                       
                   } 
                   ?>
               </ul>
               </li>
               <li class="square dir"><a class="noArrowNav" style="font-size: 100%; " href='<?php echo get_site_url().'/' . cvideo_form_link ?>' > Submit Videos </a></li>
            </ul>
            </li>
            }
             */             
            if( $wb_ent_options['associations']['sponsorpages'] ){            ?>             
            <li>
                <a href='<?php echo get_site_url(); ?>/sponsor-us'
                    <?php 
                    if ( ($current_page == $wb_ent_options['associations']['name'].'-banner-solutions') || 
                         ($current_page == $wb_ent_options['associations']['name'].'-video-solutions') || 
                            ($current_page ==  $wb_ent_options['associations']['name'].'-video-production') || 
                            ($current_page ==  $wb_ent_options['associations']['name'].'-sponsor-contact')){
                        echo ' class="current"';
                    }?>>
                    <?php _e( 'Sponsor', 'enterprise' ); ?> <?php echo $wb_ent_options['clientname']; ?>
                </a>
            </li>
            <?php                    
            }        
            ?>
            <li id="wb-responsive-nav-viewing-tips">
                <a href='<?php echo get_site_url().'/'; ?>viewing-tips'<?php if ($current_page == 'viewing-tips') echo ' class="current"'; ?>>Viewing Tips</a>
            </li>
            <li>
            	<a href='<?php echo $contactUrl; ?>'<?php if ($current_page == 'contact') echo ' class="current"'; ?>><?php _e( 'Contact Us', 'enterprise' ); ?></a>
            </li>                        
            
            <?php
            //if($_SERVER['REMOTE_ADDR'] ==  '24.79.152.107' || $_SERVER['REMOTE_ADDR'] ==  '24.79.137.158'){   
            if ($wb_ent_options['channels']['librarybox']) {
            if (!isset($postChannels) || count($postChannels) <= 0) {
                  $postChannels = wb_get_channels(0, true, 'id');  
            }            
          
            ?>
                    <li class="dropdown dropdown2">
                    
                    <span class="mm-subtitle"><a href="#mm-m0-p5" class="dropdown-toggle mm-fullsubopen" data-toggle="dropdown"><?php _e( 'Browse by Channels', 'enterprise' ); ?></a></span>
                     <!-- <a data-toggle="dropdown" class="mm-subopen mm-fullsubopen" href="javascript:void(0);">Channels</a>-->
                      <ul class="dropdown-menu">                      
                      <?php  $count = 0;
                      foreach ($postChannels as $parent) {
                      //if($parent['cat_id'] == '7'){continue;}
                       $parentChannelInfo = wb_get_channel_info($parent['cat_id'], 0);
      
                      if($parentChannelInfo['landingPageUrl']){
                      $landingUrl = $parentChannelInfo['landingPageUrl'];
                      }else{
                      $landingUrl = get_site_url().'/product-category/channel/'.$parent['cat_slug'];
                      }
                      $channelName = $parent['cat_name'];
                      if($channelName == 'ACPA 2015 Convention Channel'){
                      continue;
                      }
      if($parentChannelInfo['channelTopics'] != ''){
      
           $channelName  = $parentChannelInfo['channelTopics'];
      }
                      ?>
                       
                      <li><a class="noArrowNav" href="<?= $landingUrl ?>"><?= $channelName ?></a></li>                      
              
                      <?php 
                      $count++;
                      } ?>
                      </ul>
                    </li>
                    <?php } ?>
            <?php } else if($navilinks == 'categoryNavigation'){
              if (!isset($postCategories) || count($postCategories) <= 0) {
                $postCategories = wb_get_categories();
              }               
              foreach ($postCategories as $currentCat) { ?>
                <li class="">
                <a href="<?= get_site_url().'/category/' . $currentCat['cat_slug'] ?>"><?php printf( __('%s', 'enterprise'), $currentCat['cat_name']); ?></a>
                </li>
                <?php
              } 
            } else if($navilinks == 'customNavigation'){
              echo html_entity_decode($wb_ent_options['wbtvnavigation']['links']);
            } ?>
         </ul>
         </nav>
         <!-- end of #menu-left : The ios menu dropdown of the browseby -->         
      </div>
      <!-- End of Page-->
      <!-- end of #page : ios responsive header -->
      <style>
      .dropdown-submenu .noArrowNav:after{
            border-style: none;
      }
      </style>

      <!--[if IE]>
         <style>
         ul.dropdown, ul.dropdown li, ul.dropdown ul{
            margin-top: 1px;
         }  
         </style>
      <![endif]-->   
      <!-- The visible menu bar on desktop -->
      <div class="container-fluid" id="<?php echo $wb_ent_options[customstyle][contentwrapper]; ?>">
          <?php if(!$wb_ent_options[wbtvnavigation][disabled]){ ?>
          <div class="navbar visible-desktop" id="wb-nav">
              <div class="navbar-inner"  style="">           
                  <div class="nav-collapse collapse" id="navinavi">
                    <?php if(!$wb_ent_options[multilang]) { ?>
                      <ul class="nav">
	                      <li>
	                            <a href='<?php echo esc_url( home_url( '/all-videos' ) ); ?>'  <?php if ($current_page == 'home') echo ' class="current"'; ?>   >
	                                <?php _e( 'All Videos', 'enterprise' ); ?>
	                            </a>
	                        </li>
                          <?php if($wb_ent_options[hastvheaderlink]) {?>
                          <li><a href='/' style="text-transform:none;"><?php printf( __('%s', 'enterprise'), $wb_ent_options[channelname]); ?></a></li> 
                          <?php } 
                          if(!$wb_ent_options['wbtvnavigation']['hideBrowse']){ ?>
                          <li class="dropdown">
                              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><?php _e( 'Browse', 'enterprise' ); ?> <b class="caret"></b></a>
                              <?php
                              include get_template_directory() . '/includes/browsebymenu.php';
                              ?>

                        </li>
<?php } ?>
                        <?php
                        $naviSelected = $wb_ent_options[wbtvnavigation][custnavigation];
                        if($naviSelected == 'default'){
                        $navilinks = 'default';
                        }
                        else if($naviSelected == 'categoryNavigation'){
                        $navilinks = 'categoryNavigation';
                        }
                        else if($naviSelected = 'customNavigation' && $wb_ent_options[wbtvnavigation][links] != ""){
                        $navilinks = 'customNavigation';
                        }
                        else{
                        $navilinks = 'default';
                        } 
                        
                        
                        if($navilinks == 'default') { 
                        /*
                        ?>

                        <li>
                            <a href='<?php echo esc_url( home_url( '/' ) ); ?>'  <?php if ($current_page == 'home') echo ' class="current"'; ?>   >
                                ?>
                                <?php _e( 'Latest Video', 'enterprise' ); ?>
                            </a>
                        </li>
                        <?php
                        */
                        if( trim($wb_ent_options['notification']['contact']['url']) != '' ){
                            $contactUrl = trim($wb_ent_options['notification']['contact']['url']);
                        }
                        else{
                            $contactUrl = get_site_url().'/contact';
                        }
                        ?>
                        <li><a href='<?php echo $contactUrl; ?>'<?php if ($current_page == 'contact') echo ' class="current"'; ?>><?php _e( 'Contact Us', 'enterprise' ); ?></a></li>                        


                        <?php
                        /*
                        if (HAS_CVIDEOS) {
                            ?>
                            <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Community Videos <b class="caret"></b></a>
                                <?php     <!-- <a href='<?php //echo get_site_url().'/' ?>community-videos'  > Community Videos </a>-->           ?>

                                <ul class="dropdown-menu">
                                    <li class="dropdown-submenu">
                                        <?php
                                        echo '

                         <a class="square" id="browse-cvideos" href="' . get_site_url().'/' . 'category/community-videos"';

                                        if (count($cVideoCat) <= 0) {
                                            echo ' class="noArrowNav" ';
                                        }
                                        echo '> Browse Videos </a>
                      <ul class="dropdown-menu">';
                                        foreach ($cVideoCat as $currentCVideoCat) {
                                            if ($currentCVideoCat['count'] >= 1) {
                                                echo '<li class=""><a class="square cVideoCat" href="' . get_site_url().'/' . 'category/' . $currentCVideoCat['slug'] . '">' . ucwords($currentCVideoCat['cat_name']) . '</a></li>';
                                            }
                                        }
                                        ?> 

                                </ul>
                            </li>
                            <li class="square dir" ><a  class="noArrowNav"  style="font-size: 100%; " href='<?php echo get_site_url().'/' . CVIDEO_FORM_LINK ?>' > Submit Videos </a></li>
                        </ul>
                        </li>

                        <?

                        }                         
                         */

                        if($wb_ent_options['associations']['sponsorpages']){            ?>             
                        <li>
                            <a href='<?php echo get_site_url(); ?>/sponsor-us'
                                <?php 
                                if ( ($current_page == $wb_ent_options['associations']['name'].'-banner-solutions') || 
                                     ($current_page == $wb_ent_options['associations']['name'].'-video-solutions') || 
                                        ($current_page ==  $wb_ent_options['associations']['name'].'-video-production') || 
                                        ($current_page ==  $wb_ent_options['associations']['name'].'-sponsor-contact')){
                                    echo ' class="current"';
                                }?>>
                                <?php printf(__('Sponsor %s', 'enterprise'), $wb_ent_options['clientname']) ?>                                
                            </a>
                        </li>
                        <?php
                    }
                    ?>

                    <li id="wb-nav-viewing-tips"><a href='<?php echo get_site_url().'/'; ?>viewing-tips'<?php if ($current_page == 'viewing-tips') echo ' class="current"'; ?>><?php _e( 'Viewing Tips', 'enterprise' ); ?></a></li>        
                        <?php
                        // if($_SERVER['REMOTE_ADDR'] ==  '24.79.152.107' || $_SERVER['REMOTE_ADDR'] ==  '24.79.137.158'){   
            if ($wb_ent_options['channels']['librarybox']) {
                        if (!isset($postChannels) || count($postChannels) <= 0) {
                  $postChannels = wb_get_channels(0, true, 'id');  
            }           
            ?>
                    <li class="dropdown">
                      <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0);">Browse by Channels  <b class="caret"></b></a>
                      <ul class="dropdown-menu">                      
                      <?php  
                      $count = 0;
                      foreach ($postChannels as $parent) {
                      //if($parent['cat_id'] == '7'){continue;}
                      $parentChannelInfo = wb_get_channel_info($parent['cat_id'], 0);
      
                      if($parentChannelInfo['landingPageUrl']){
                      $landingUrl = $parentChannelInfo['landingPageUrl'];
                      }else{
                      $landingUrl = get_site_url().'/product-category/channel/'.$parent['cat_slug'];
                      }
                      $channelName = $parent['cat_name'];
                      if($channelName == 'ACPA 2015 Convention Channel'){
                      //continue;
                      }
                      if($parentChannelInfo['channelTopics'] != ''){
      
                      $channelName  = $parentChannelInfo['channelTopics'];
                      }
                      ?>  
                      <li><a class="noArrowNav" href="<?= $landingUrl ?>"><?= $channelName ?></a></li>
                      <?php
                      $count++;
                      } ?>
                      </ul>
                    </li>
                    <?php  
                     }
                    ?> 
                        <?php } else if($navilinks == 'categoryNavigation'){               
                if (!isset($postCategories) || count($postCategories) <= 0) {
                    $postCategories = wb_get_categories();
                }
               
               foreach ($postCategories as $currentCat) { ?>
               <li class="">
               <a href="<?= get_site_url().'/category/' . $currentCat['cat_slug']; ?>"><?php printf( __('%s', 'enterprise'), $currentCat['cat_name']); ?></a></li>
               <?php } ?>
                
                        <?php } else if($navilinks == 'customNavigation'){
                           echo html_entity_decode($wb_ent_options['wbtvnavigation']['links']);
                        }?>
                    </ul>
                    <?php } else{
                    bones_main_nav(); // Adjust using Menus in Wordpress Admin                     
                    } ?>
                </div>
                <?php if(!$wb_ent_options['hassearch']['disabled']) { ?> 
                    <script type="text/javascript">
        				jQuery(document).ready(function() {            
          					// Dynamic search input
          					jQuery( "#searchbtnshow" ).click(function() {            
            					jQuery( "#s" ).show();                                               
            					jQuery( "#s" ).focus();
            					jQuery( "#s" ).blur(function() {
                 					jQuery( "#searchbtnshow" ).prop('type', 'submit');
            					});             
                        	});
        				});

        				function submit_search(){
           					if( $.trim($('#s').val()) == "" || $.trim($('#s').val()) == 'START TYPING...' ){
              					return false;
           					}
        				}
    				</script>
     				<script>
      					$(function() {         
       						$('#s').bind("keyup change keypress", function(e) {
         						if( $('#s').val() != ""){
           							$('#searchsubmit').removeAttr('disabled');
         						} else {
           							$('#searchsubmit').attr('disabled', true);   
         						}
         						if(e.keyCode == 13 && $('#s').val()!="" ){
            						$('#searchformnav').submit();
         						}
       						});
      					});     
                    </script>
                  <div id="searchDiv">
                    <form role="search" method="get" id="searchformnav" action="/" onsubmit="return submit_search();">
                        <div class="input-append">
                            <input class="appendedInputButton" id="s" name="s" type="text" placeholder="START TYPING..." style="display:none;">
                            <i class="wbicon-search2" id="wb-advance-search-icon-nav"></i>
                           <!-- <button id="searchbtnshow" class="btn btn-custom" type="button"><i class="icon-search icon-white"></i></button> -->
                        </div>
                    </form>
                </div> 
                    <?php /*
                  <div id="searchDiv">
                    <form role="search" method="get" id="searchform" action="/" >
                       <div>
                          <input type="text" value="<?= _e('Search Videos', 'enterprise') ?>" onblur="if(this.value==='') {this.value='Search Videos';}" onfocus="if(this.value==='Search Videos') {this.value='';}" maxlength="200" name="s" id="s" /><input type="submit" id="searchsubmit" value="Search" />
                       </div>
                    </form> 
                    <script>
                      $('#s').bind("keyup change keypress", function(e) {
                        if( $('#s').val() != ""){
                          $('#searchsubmit').removeAttr('disabled');
                        } else {
                          $('#searchsubmit').attr('disabled', true);   
                        }
                        if(e.keyCode == 8){
                          if( $('#s').val()==""){
                            $('#searchsubmit').attr('disabled', true);   
                          }
                        }
                      });
                    </script>
                </div>
                 * 
                 */?>
               <?php } ?>
            </div>
        </div>
          <?php } ?>
        <!-- The visible menu bar on desktop -->
        <!-- End of Navigation Bar -->
