<?php
/**
 * filename: page.php
 * description: this will be the default template to be used for the theme
 * author: Jullie Quijano
 * date created: 2014-03-25
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: Default
 */
get_header();
if (have_posts()) : while (have_posts()) : the_post(); 
?>
<div id="wb_ent_content" class="clearfix row-fluid">
	<div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
    <div id="viewing-tips">
        <h1><?php the_title(); ?></h1>
    		<?php the_content(); ?>
    		
        <?php
        include ( get_template_directory() . '/includes/widgets/adsWideCustom.php');
        ?>
    
    		<?php endwhile; else: ?>
    		<p>&nbsp;</p>
    		<?php endif; ?>
      
    </div>
  </div>
<?php 
get_sidebar(); 
get_footer(); 
?>