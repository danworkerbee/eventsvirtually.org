<?php
/*
Filename:      sponsor-template-videoSolutions.php                                                 
Description:   will display the Video Solutions(page 1) part of the Sposonr Template for Naylor    
Author: 	      Jullie Quijano                       
Change Log:
   November 26, 2013 [Jullie]added code to display message if file is included
	August 29, 2012 	[Jullie] Created the file                                                                                                                                                                                                              
*/ 

/**
 * @package WordPress
 * @subpackage wp-bootstrap
 */
/*
Template Name: Sponsor Template Video Solutions v.6
*/

//error_reporting(E_ALL);
//echo '	lib ';


// Sitewide Variables
if (!defined('CHANNEL_SITE')) include get_template_directory_uri().'/library/includes/config.php';

get_header(); 


//will determine if file is included
if($_SERVER['REMOTE_ADDR'] == WORKERBEE_IP){
   echo '
   
<!-- START OF '. __FILE__ .' -->
   
   ';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/naylorsponsor.css" />
<style>
#wb_ent_content #centerMessage{
   width: 575px;
   height: 50px;
   margin: 25px auto 0;
   background-color: #d9e5ee;
   color: #243c5f;
   font-size: 18px;
   clear: both;
   font-weight: bold;
   padding: 25px 47px;
}

#wb_ent_content #vidEverywhere{
   float: right; 
   margin: 45px 0 10px;
}

#wb_ent_content #embedDivMain{
   width: 630px;
   height: 250px;
   margin: 50px auto 55px;
}

#wb_ent_content .embedDiv{
   width: 300px;
   float: left;
   font-size: 15px;
}
</style>
<div id="wb_ent_content" class="clearfix row-fluid">

    <div id="wb_ent_main" class="span12 clearfix" role="main" style="border: 0px solid black;">
        <div id="sectionNav">
         <ul>
         	<li><a href="/naylor-banner-solutions" ><?= _e('Banner Solutions', 'enterprise') ?></a></li>
          <li><a href="/naylor-video-solutions" class="currentPage"><?= _e('Video Solutions', 'enterprise') ?></a></li>
          <li><a href="/naylor-video-production" ><?= _e('Video Production', 'enterprise') ?></a></li>
          <li><a href="/sponsor-contact"><?= _e('Request Information', 'enterprise') ?></a></li>
         </ul>
      </div>      
      
      <h1><?= _e('Video Content Marketing Solutions', 'enterprise') ?></h1>
      <div class="section" id="videoContent" style="margin: 5px; ">
         
         <?php /*
          <!-- Start of Association Adviser Embed Widget code -->        
         <div id="naylorATV357216s" style="float: left; width: 300px;">        
         <script type="text/javascript" src='http://tv.associationadviser.com/sponsor-template-embed/embed-popup-square.js' type='text/javascript'%3E%3C/script%3E"));></script>
         </div><!-- End of Association Adviser Embed Widget -->
        */?> 
         <!-- Start of Association Adviser Embed Widget code -->        
         <div id="naylorATV357216s" style="float: left; width: 300px;">           

         <script type="text/javascript" src="<?= get_template_directory_uri() ?>/library/sponsor-template-embed/embed-popup-square.php">
           
         </script>
        
         <script type="text/javascript">
        $(document).ready(function () {
           $("#watchNowDiv").nextAll().remove();
        });
    </script>
         </div>
                     
         <div id="vs-msg" style="float: left; width: 500px;  margin-right: 70px; margin-top: 5px;">
         <p style="font-size: 23px; line-height: 1.6em; color: #222;"><?= _e('Achieve powerful video messaging and maximum reach and engagement. Click on the play button', 'enterprise') ?> <img src="/wp-content/themes/enterprise/images/play_button_small.png" title="play button" /> <?= _e('to watch a video highlighting the video services for marketers available from your association and Naylor.', 'enterprise') ?></p>
         </div>
         
         <div>
         
        
         </div>
      </div>        
      
       <div style="clear:both;" >
      </div>
      <div class="section" id="benefitsDiv">
         <h2><?= _e('Benefits to Marketers', 'enterprise') ?></h2>
         <p><span class="bold"><?= _e('Sight, Sound & Motion:', 'enterprise') ?></span> <?= _e('Use video to tell your story and engage your target market.', 'enterprise') ?>

         <p><span class="bold"><?= _e('Authentic Brand Alignment:', 'enterprise') ?></span> <?php printf(__('Tell your story on %s, in social media and outbound marketing with our ', 'enterprise'), $wb_ent_options['clientname']); ?><em><?= _e('Video Everywhere!', 'enterprise') ?></em> <?= _e('Platform. Your branded video provides immediate awareness, authenticity, and trust to your video message.', 'enterprise') ?></p>
         
         <p><span class="bold"><?= _e('Maximize Reach:', 'enterprise') ?></span> <?= _e('We make it simple and effective to place to your video everywhere your target market spends their time. Your video can be placed in a multitude of mediums, from your website, to outbound marketing materials, emails, social media, and QR codes in print ads and brochures.', 'enterprise') ?></p>
         
         <p><span class="bold"><?= _e('Convert Visitors into Leads, and Leads into Sales:', 'enterprise') ?><?= _e('All video platforms include an embed code, and two click-through banners (IAB Standard: 120 x 140 pixels, 300 x 250 pixels and 468 x 60 pixels) to make it easier for the viewer to learn more, register and buy.', 'enterprise') ?></span> </p>        
      </div>
      
      <div class="section" id="vidEverywhere">
         <h4><?= _e('Our <em>Video Everywhere!</em> Player', 'enterprise'); ?></h4>
         <img src="<?php echo get_template_directory_uri(); ?>/images/sponsorTempVidEverywhere1.jpg" />
      </div>    
     
      <div style="clear:both;" >
      </div>
      <div class="section" id="butdivs">
      
      	 <a class="btn btn-primary" style="padding:20px 24px;text-decoration: none; font-size: 16px;" href="/naylor-banner-solutions"><?= _e('Our Banner Solutions', 'enterprise') ?></a>
      	 
         <a class="btn btn-primary" style="padding:20px 24px;text-decoration: none; font-size: 16px;" href="/naylor-video-production"><?= _e('Learn more about our Video Production Process', 'enterprise') ?></a>
         
         <a class="btn btn-primary" style="padding:20px 24px;text-decoration: none; font-size: 16px;" href="/sponsor-contact"><?= _e('Request More Information Here', 'enterprise') ?></a>
      </div>
    </div>
</div>
<?php 
//will determine if file is included
if($_SERVER['REMOTE_ADDR'] == WORKERBEE_IP){
   echo '
   
<!-- END OF '. __FILE__ .' -->
   
   ';
}


get_footer(); 

?>