<?php
/**
 * filename: subscribeMc.php
 * description: this will be the template used for the Mailchimp Subscribe Form
 * author: Jullie Quijano
 * date created: 2014-05-02
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: Mailchimp Subscribe Template
 */
global $wb_ent_options;
get_header();
if (have_posts()) : while (have_posts()) : the_post();
        ?>
        <div id="wb_ent_content" class="clearfix row-fluid">
            <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
                <div id="viewing-tips">
                    <h1 class="pagetitle">Subscribe to our video programming, and be notified when a new video is released.</h1>
                    <?php
                    if (trim($wb_ent_options['clientprivacyurl']) != '' || trim($wb_ent_options['clienttermsurl']) != '') {
                        ?>
                        <p style="color: #535353; font-size: 12px;"><?php printf(__('View our <a href="%s" title="Terms of Use">Terms of Use</a> and <a href="%s" title="Privacy Policy">Privacy Policy</a>.', 'enterprise'),$wb_ent_options['clienttermsurl'], trim($wb_ent_options['clientprivacyurl']));  ?></p>
                        <?php
                    }
                    if (trim($wb_ent_options['mailchimpform'])) {
                        echo html_entity_decode($wb_ent_options['mailchimpform'.$curlang]);
                    }
                    ?>
                    <br />
                    <?php the_content(); ?>
                <?php endwhile;
            endif;
            ?>
        </div>
    </div>
    <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<?php
get_sidebar();
get_footer();
?>
