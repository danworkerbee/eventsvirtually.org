<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;800;900&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400;500;600;700;900&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="/css/icomoon.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="/wp-content/themes/enterprise/css/font-awesome/fontawesome.min.css" type="text/css" />
<link type="text/css" rel="stylesheet" href="/wp-content/themes/enterprise/css/font-awesome/all.css" type="text/css" />
<div id="hub-header-wrapper">
	<div id="hub-top-header">
		<ul id="hub-upper-nav">
			<li><a href="#" class="active">Register</a></li>
			<li><a href="#">Event Schedule</a></li>
			<li><a href="#">ATP Media Center</a></li>
			<li><a href="#">ATP Home</a></li>
		</ul>
	</div>
	<div id="hub-header-below">
		<img src="/wp-content/uploads/2020/08/home-main-1.png" id="hub-image-bellow" />
	</div>
</div>
<style>
	#hub-header-wrapper{
		max-width: 1208px;
	    margin: 0px auto;
	    padding-right: 20px;
	    padding-left: 20px;
	    width: 100%;
	}
	#hub-image-bellow{
		-webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 0 60%);
  		clip-path: polygon(0 0, 100% 0, 100% 100%, 0 60%);
	}
	#wb_ent_content{
		margin-top: -70px;
	}
	#hub-title{
		font-family: 'Montserrat', sans-serif;
	    font-style: normal;
	    font-weight: 700;
	    font-size: 32px;
	    color: #F1592A;
	    max-width: 590px;
	}
	#hub-page-content p{
		font-style: normal;
	    font-weight: normal;
	    font-size: 16px;
	    line-height: 137.69%;
	    font-family: 'Lato', sans-serif;
	}	
	#hub-upper-nav {
  		list-style-type: none;
  		margin: 0;
		padding: 0;
		overflow: hidden;
		background-color: #fff;
	}
	#hub-upper-nav li{
		float: right;
	}	
	#hub-upper-nav li a {
  		display: block;
	  	text-align: center;
	  	padding: 14px 16px;
	  	text-decoration: none;
	  	font-family: Open Sans;
		font-style: normal;
		font-weight: normal;
		font-size: 18px;
		color: #F1592A;
	}
	#hub-upper-nav li a:hover,
	#hub-upper-nav li a.active {
  		background-color: #F1592A;
  		color: white;
	}
    @media (max-width: 979px){
    	#wb_ent_content{
			margin-top: 0px;
		}
    }
    #header a img{
		content:url("/wp-content/uploads/2020/08/home-main-1.png");
		max-width: 400px;
		width: 100%;
	}
	button.wb-ent-aana-exhibitor-showcase-btn,
	button.wb-ent-aana-purchase-pass-btn {
	    background-color: #F1592A;
	    color: #fff;
	    text-shadow: none;
	    background-image: none;
	    background-repeat: no-repeat;
	    border: 1px solid #F1592A;
	    border-radius: 0px;
	    font-weight: 600;
	    padding: 10px 29px;
	    box-shadow: none;
	    width: 240px;
	}button.wb-ent-aana-exhibitor-showcase-btn:hover,
	button.wb-ent-aana-purchase-pass-btn:hover{
		background-color: #F1592A;
	    color: #fff;
	    text-shadow: none;
	    background-image: none;
	    background-repeat: no-repeat;
	    border: 1px solid #F1592A;
	}
	.wb-ent-sched-btn-group #wb-hub-thank-purchase,
	button.wb-ent-aana-click-here-txt-link{
		font-family: Open Sans;
	    font-style: normal;
	    font-weight: bold;
	    font-size: 14px;
	    line-height: 137.69%;
	    display: flex;
	    align-items: center;
	    text-align: center;
	    color: #F1592A;
	}
	button.wb-ent-aana-click-here-txt-link{
		margin-left: 15px;
	}
	#aana-hub div .wb-ent-sched-btn-group a{
	 	display: inline-flex;
	}
	p.wb-ent-aana-hub-date{
		background: #78297D;
	}
	.wb-ent-date-event ul .wb-hub-session div.wb-ent-aana-hub-time-tracks{
		margin: 0px;
		text-align: center;
	}
	@media (max-width: 575px){
    	#schedule-area .wb-ent-date-event ul .wb-hub-session{
    		padding: 10px 0; 
    	}
    }
	.wb-hub-session .wb-ent-aana-hub-time-tracks .wb-ent-aana-hub-time{
		max-width: 256px!important;
    	width: 100% !important;
	}
	.wb-hub-session-title-link .wb-ent-aana-hub-event,
	.modal-body .wb-ent-aanaapf-header-title-modal{
		font-family: 'Montserrat', sans-serif;
		font-style: normal;
		font-weight: bold;
		font-size: 16px;
		line-height: 22px;
		color: #F1592A;
	}
	.wb-ent-sched-btn-group {
	    padding-bottom: 1.2rem;
	}
	.wb-ent-aana-hub-event-time{
		font-size: 12px;
   		font-weight: 700;
    	padding-left: 0.2rem;
    	font-family: 'Montserrat',sans-serif;
    	display: block;
	}
	.wb-ent-aana-hub-event-content{
		font-size: 14px;
	    font-weight: 400;
	    padding-left: 0.2rem;
	    font-family: 'Montserrat',sans-serif;
	    display: block;
	    padding-top: 5px;
	}
	.wb-ent-aanaapf-header-date-tracks-modal h2{
		font-size: 12px;
	    font-weight: 700;
	    font-family: 'Montserrat',sans-serif;
	}
	.wb-ent-aanaapf-hub-session-description-modal{
		font-size: 14px;
	    font-weight: 400;
	    font-family: 'Montserrat',sans-serif;
	}
	.modal-body .wb-ent-aana-modal-login-btn {
	    background-color: #F1592A;
	    border-color: #F1592A;
	    color: #fff;
	    text-shadow: none;
	    background-image: none;
	    background-repeat: unset;
	    border-radius: 0px;
	    box-shadow: none;
	    padding: 10px 30px;
	    font-size: 14px;
	    font-weight: 700;
	    margin-top: 1rem;
	    margin-bottom: 5px;
	}
	.modal-body .wb-ent-aana-modal-login-btn:hover{
	    background-color: #F1592A;
	    border-color: #F1592A;
	    color: #fff;
	    text-shadow: none;
	    background-image: none;
	    background-repeat: unset;
	    border-radius: 0px;
	    box-shadow: none;
	    padding: 10px 30px;
	    font-size: 14px;
	    font-weight: 700;
	    margin-top: 1rem;
	    margin-bottom: 5px;
	}
</style>