<?php
/**
 * filename: wb-sso-logout-impexium.php
 * description: This template will serve as a front-end logout
 * author: WB Team
 * date created: 2018-06-15
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: SSO Logout Page (Impexium)
 */

$impexium_url = '';
$member_user_role = '';
$wb_home_url  = get_site_url();

if( is_user_logged_in() || $wb_channels_is_user_logged_in ){
	$wb_user_data = get_userdata( get_current_user_id() );
	wp_logout();
	if ( in_array( $member_user_role, $wb_user_data->roles  )) {
		$wb_home_url = $impexium_url . "/account/logout.aspx?ReturnUrl=" . $wb_home_url;
	}
}
	wp_redirect( $wb_home_url );
	exit;
?>
