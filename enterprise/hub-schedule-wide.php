<?php
/**
 * filename: hub-splash.php
 * description: This will be the template to use for the AANA Hub Splash page
 * author: Jullie Quijano
 * date created: 2020-06-14
 *
 *
 * @package WordPress
 * @subpackage Enterprise
 
 * Template Name: Hub - Schedule Wide Page
 */
define('DONOTCACHEPAGE', true);
global $postId, $wb_ent_options, $current_user;

$wb_page_id 		= get_the_ID();
$wb_page_title 		= get_the_title( $wb_page_id );
$wb_page_content	= get_post( $wb_page_id );
$wb_content_output 	= apply_filters( 'the_content', $wb_page_content->post_content );
$wb_all_meta_data 	= get_metadata("post", $wb_page_id);
$wb_hub_category 	= $wb_all_meta_data["wb_live_session_category"][0];
$wb_hub_main_category = get_option('wb_vh_live_events_category');
$wb_hub_default_speaker_image = get_option('wb_vh_default_speaker_image');
$wb_hub_post_sessions = wb_get_virtual_conference_posts( $wb_hub_category );

//get_header('hub');
get_header("hub");
if ($wb_ent_options['sharetype'] == 'floating' && $wb_ent_options['hasshare'] && function_exists(sharebar)) {
	if ( !in_category( $wb_ent_options['videocats']['howto'] )){
		if (  !in_category($wb_ent_options['videocats']['members']) ){
			sharebar();
		}
	}
}

$banner_image = get_the_post_thumbnail_url($postId, 'full');

?>
<div id="wb_ent_content" class="clearfix row-fluid" style="padding: 0;">
    <div id="wb_ent_main" class="span8 clearfix" role="main" style="width: 100% !important;">
    	<?php if ( $wb_all_meta_data["wb_live_hub_banner_upload_url"][0] != "" ){?>
		<div class="wb-ent-event-hub-banner">
			<a href="<?php echo urldecode($wb_all_meta_data["wb_live_hub_banner_link"][0]); ?>">
				<img src="<?php echo urldecode($wb_all_meta_data["wb_live_hub_banner_upload_url"][0]); ?>" alt="ANNA Splash Screen Hub" />
			</a>
        </div>
		<?php } ?>
		<h1 id="hub-title"><?php echo $wb_page_title; ?></h1>
		<div id="hub-page-content">
			<?php echo $wb_content_output; ?>
		</div>	
        <div id="aana-hub">
        
        <!-- START HTML CODE HERE -->
            <div class="clearfix row-fluid">
            	<?php if ( $wb_all_meta_data["wb_live_hub_show_buttons"][0] == "yes" ){ ?>
				<div class="wb-ent-sched-btn-group">
					<?php if ( $wb_all_meta_data["wb_live_hub_left_button_text"][0] ){ ?>
					<a href="<?php echo urldecode($wb_all_meta_data["wb_live_hub_left_button_link"][0]); ?>"><button type="button" class="btn btn-primary wb-ent-aana-exhibitor-showcase-btn"><?php echo $wb_all_meta_data["wb_live_hub_left_button_text"][0]; ?></button></a>
					<?php } ?>
					<?php if ( !is_user_logged_in() ){ ?>
					<a href="<?php echo urldecode($wb_all_meta_data["wb_live_hub_center_button_link"][0]); ?>"><button type="button" class="btn btn-primary wb-ent-aana-purchase-pass-btn"><?php echo $wb_all_meta_data["wb_live_hub_center_button_text"][0]; ?></button></a>
					<a href="<?php echo urldecode($wb_all_meta_data["wb_live_hub_right_button_link"][0]); ?>"><button type="button" class="btn btn-primary wb-ent-aana-click-here-txt-link"><?php echo $wb_all_meta_data["wb_live_hub_right_button_text"][0]; ?></button></a>
					<?php 
            			}else{ 
							?>
								<p id="wb-hub-thank-purchase"><?php echo $wb_all_meta_data["wb_live_hub_welcome_text"][0]; ?></p>
					<?php } ?>
				</div>
				<?php } ?>
				
				<?php if ( $wb_all_meta_data["wb_live_hub_show_track"][0] == "yes" ){ 
					$wb_session_tracks =  wb_get_session_track( $wb_hub_category );
					//echo "<pre>";
					//print_r($wb_session_tracks);
					//echo "</pre>";
					//echo (count($wb_session_tracks));
				?>
				<div class="wb-ent-session-tracks-group">
					<p class="wb-ent-session-txt">SESSION TRACKS</p>
					<div class="wb-ent-sessions-label">
						<ul>
							<li><span><i class="far fa-square wb-ent-tracks-all"></i> <label>All Tracks</label></span></li>
							<?php 
							$wb_track_style = "";
							$wb_track_ctr = 2;
							foreach ( $wb_session_tracks as $wb_current_track ){
								?>
								<li><span><i class="fas fa-square wb-ent-tracks-<?php echo $wb_current_track->slug; ?>"></i><label><?php echo $wb_current_track->name; ?></label></span></li>
								<?php
								$wb_track_style .= "i.wb-ent-tracks-".$wb_current_track->slug."{color:".$wb_current_track->color.";}";
								$wb_track_style_filter .= '.SumoSelect > .optWrapper > .options > li.opt:nth-child('.$wb_track_ctr.') { color: '.$wb_current_track->color.' !important;}';
								$wb_track_ctr++;
							}
							?>
						</ul>
						<?php echo "<style>".$wb_track_style.$wb_track_style_filter."</style>"; ?>
					</div>
				</div>
				<?php } ?>
				
				<?php 
				if ( $wb_all_meta_data["wb_live_hub_show_filter_bar"][0] == "yes" ){
				?>
				<div class="wb-ent-aana-filter-search">
					<form>
						<div class="wb-ent-aana-filter-search-area">
							<div class="form-group mb-0">
								<label for="" class="wb-ent-aana-filter-search-form-label">Filter <span>by Session</span> Date:</label>
								<select name="session-date" <?php if ( $wb_all_meta_data["wb_live_hub_multi_select_filter"][0] == "yes" ) echo ' multiple="multiple"'; ?> class="form-control wb-ent-aana-filter-search-box SlectBox" onclick="console.log($(this).val())" onchange="console.log('change is firing')" id="wb-session-date" >	
									<option value="all-dates">All Dates</option>
									<?php 
										foreach ($wb_hub_post_sessions["dates"] as $wb_current_session_date ){
											if ( $wb_current_session_date['type'] == "sponsor" ){
												$wb_option_text = $wb_current_session_date['name'];
											}elseif( $wb_current_session_date['type'] == "lms" ){
											    $wb_option_text = $wb_current_session_date['name'];
											}else{
												$wb_option_text = date('l, F d, Y',strtotime($wb_current_session_date['name']));
											}
											?>
												<option value="<?php echo $wb_current_session_date['value']; ?>"><?echo $wb_option_text; ?></option>
											<?php 
										} 
								 	?>
									<!-- <option>Saturday, August 15, 2020</option> 
									<option>SPONSOR COMPANY NAME</option>
									<option>On-Demand Learning</option>-->
								</select>
							</div>
							
							<div class="form-group mb-0">
								<label for="" class="wb-ent-aana-filter-search-form-label">Filter <span>by Session</span> Track:</label>
								<select name="session-track" <?php if ( $wb_all_meta_data["wb_live_hub_multi_select_filter"][0] == "yes" ) echo ' multiple="multiple"'; ?> class="form-control wb-ent-aana-filter-search-box SlectBox wb-ent-aana-track-i" onclick="console.log($(this).val())" onchange="console.log('change is firing')" id="wb-session-track">
									<option value="all-tracks">All Tracks</option>
									<?php foreach ( $wb_session_tracks as $wb_current_track ){ ?>
									<option value="<?php echo $wb_current_track->slug; ?>"><?php echo $wb_current_track->name; ?></option>
									<?php } ?>
								</select>
							</div>

							<div class="form-group mb-0">
								<label for="" class="wb-ent-aana-filter-search-form-label">&nbsp;</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<div class="input-group-text" class="wb-ent-aana-hub-search-btn"><i class="fas fa-search"></i></div>
									</div>
									<input type="text" class="form-control wb-ent-aana-hub-search-field" aria-label="Type to filter the list" id="wb-session-search">
								</div>
							</div>
						</div>

						<div class="wb-ent-aana-filter-search-btn-txt-area">
							<div class="wb-ent-aana-filter-search-col-area">
								<a href="<?php 
											if(filter_var( urldecode($wb_all_meta_data["wb_live_hub_need_assistance_link"][0]), FILTER_VALIDATE_URL)){
												echo urldecode($wb_all_meta_data["wb_live_hub_need_assistance_link"][0]);
											}elseif(filter_var( urldecode($wb_all_meta_data["wb_live_hub_need_assistance_link"][0]), FILTER_VALIDATE_EMAIL)){
												echo "mailto:".urldecode($wb_all_meta_data["wb_live_hub_need_assistance_link"][0])."?subject=".$wb_page_title." - SUPPORT";
											}else{
												echo "#";
											}
								?>" class="btn btn-primary btn-lg wb-ent-aana-filter-need-assistance-txt" role="button">
									<?php echo $wb_all_meta_data["wb_live_hub_need_assistance_text"][0]; ?>
								</a>
							</div>

							<div class="wb-ent-aana-filter-search-col-area">
								&nbsp;
							</div>

							<div class="wb-ent-aana-filter-search-col-area">
								<input type="button" class="btn btn-primary btn-lg wb-ent-aana-filter-search-search-btn" id="wb-search-session" value="Search">
								<input type="button" class="btn btn-primary btn-lg wb-ent-aana-filter-search-clear-refresh-btn" id="wb-clear-refresh-session" value="Clear/Refresh">
							</div>
						</div>
					</form>
				</div>
				<?php } //show filter bar ?>
				<div class="wb-ent-events-schedule-area" id="schedule-area">
				
					<?php 
						//echo "<pre>";
						//print_r ( $wb_hub_post_sessions["posts"] );
						//echo "</pre>";
						$wb_date = "";
						$wb_target_link = "";
						if ( $wb_hub_post_sessions["posts"] ){
							foreach ( $wb_hub_post_sessions["posts"] as $key => $wb_current_session ){
								if ( $wb_current_session->wb_virtual_conference_time_display == "sponsor" ){
									$wb_session_time_display = $wb_current_session->wb_virtual_sponsor;
									$wb_session_display_class = ' wb-session-hide';
									$wb_sponsor_logo = ' <img src="' . $wb_current_session->wb_virtual_sponsor_logo . '" style="height: 26px;" />';
								}elseif( $wb_current_session->wb_virtual_conference_default_media == 'lms' ){
								    $wb_session_time_display =  $wb_current_session->wb_virtual_conference_on_demand_text . " Learning";
								    $wb_session_display_class = "";
								    $wb_sponsor_logo = "";
								}else{
									$wb_session_time_display =  date('l, F d, Y',strtotime($wb_current_session->wb_virtual_conference_date));
									$wb_session_display_class = "";
									$wb_sponsor_logo = "";
								}
								if ( $wb_date != $wb_current_session->wb_virtual_conference_date && $key == 0 ){
									?>
									<div class="wb-ent-date-event" id="wb-date-<?php echo $wb_current_session->wb_virtual_conference_date; ?>">
										<p class="wb-ent-aana-hub-date"><?php echo $wb_session_time_display; ?> <span class="wb-ent-aana-hub-sponsor-logo"><?php echo $wb_sponsor_logo;?></span></p>
										<ul>
									<?php 
								}elseif ( $wb_date != $wb_current_session->wb_virtual_conference_date ){
								?>	
										</ul>
									</div>
									<div class="wb-ent-date-event" id="wb-date-<?php echo $wb_current_session->wb_virtual_conference_date; ?>">
										<p class="wb-ent-aana-hub-date"><?php echo $wb_session_time_display; ?> <span class="wb-ent-aana-hub-sponsor-logo"><?php echo $wb_sponsor_logo;?></span></p>
										<ul>
								<?php 								
								}
								?>
											<li class="wb-hub-session <?php 
											
											$wb_hub_session_link = "";
											if ( $wb_current_session->wb_virtual_conference_default_media == "redirect-link" ){
												$wb_hub_session_link = ' href="' . $wb_current_session->wb_virtual_conference_redirect_link .'"';
											}else{
												$wb_hub_session_link = ' href="#" data-toggle="modal" data-target="#wb-ent-aana-hub-show-sessions-'. $wb_current_session->ID .'"';
											}
											
											foreach ( $wb_current_session->wb_virtual_conference_category as $wb_current_session_category ){
												if ( $wb_current_session_category->term_id == $wb_hub_category || $wb_hub_main_category == $wb_current_session_category->term_id )
													continue;
												echo " wb-hub-session-".$wb_current_session_category->slug;
											}
											?> wb-session-date-<?php 
											if ( $wb_current_session->wb_virtual_conference_time_display == 'image' ){
												$wb_session_image = "";
												if ( $wb_current_session->wb_virtual_conference_hub_image_url != "" ){
													$wb_session_image = urldecode($wb_current_session->wb_virtual_conference_hub_image_url);
												}else{
													$wb_session_image = urldecode($wb_current_session->wb_virtual_conference_thumb);
												}
												$wb_session_track_name_display = '<a '.$wb_hub_session_link.'><img src="'.$wb_session_image.'" class="hub-session-list-image"></a>';
											}elseif ( $wb_current_session->wb_virtual_conference_time_display == 'sponsor' ){
											    echo "wb-sponsor-".$wb_current_session->wb_virtual_conference_date;
											    $wb_session_track_name_display = "";
											}elseif ( $wb_current_session->wb_virtual_conference_default_media == 'lms' ){
											    echo "wb-lms-".$wb_current_session->wb_virtual_conference_date;
											    $wb_session_track_name_display = $wb_current_session->wb_virtual_conference_on_demand_text;
											}else{
											      echo $wb_current_session->wb_virtual_conference_date;
											      $wb_session_track_name_display = date('h:i:A',strtotime($wb_current_session->wb_virtual_conference_from));//$wb_current_session->wb_virtual_conference_time_zone;
											      if ( $wb_current_session->wb_virtual_conference_time_display != "from" ){
											      	$wb_session_track_name_display .= ' - ' .  date('h:i:A',strtotime($wb_current_session->wb_virtual_conference_to)); // . " " . $wb_current_session->wb_virtual_conference_time_zone;
											      }
											      $wb_session_track_name_display .= " " . $wb_current_session->wb_virtual_conference_time_zone;
											} ?>">
												<div class="wb-ent-aana-hub-time-tracks">
													<span class="wb-ent-aana-hub-time <?php echo $wb_session_display_class; ?>"><?php echo $wb_session_track_name_display; ?></span>
													<p class="wb-ent-aana-hub-track-name">
														<?php 
														if ( $wb_current_session->wb_virtual_conference_default_media == "redirect-link" ){
															?>
															<a href="<?php echo $wb_current_session->wb_virtual_conference_redirect_link; ?>" target="_blank" class="wb-hub-session-title-link" >
															<?php 
														}else{
															?>
															<a href="#" data-toggle="modal" data-target="#wb-ent-aana-hub-show-sessions-<?php echo $wb_current_session->ID;?>" class="wb-hub-session-title-link" >
															<?php 
														}
														if ( $wb_all_meta_data["wb_live_hub_show_track"][0] == "yes" ){ 
															if ( count($wb_session_tracks) > (count( $wb_current_session->wb_virtual_conference_category ) - 2)){
																foreach ( $wb_current_session->wb_virtual_conference_category as $wb_current_session_category ){
																	if ( $wb_current_session_category->term_id == $wb_hub_category || $wb_hub_main_category == $wb_current_session_category->term_id )
																		continue;
																		?>
																	<i class="fas fa-square wb-ent-tracks-<?php echo $wb_current_session_category->slug; ?>"></i>
																<?php
																}
															}else{
																?>
																	<i class="far fa-square " ></i>
																<?php
															}
														}	
														?>
														<span class="wb-ent-aana-hub-event"><?php echo $wb_current_session->post_title; ?></span></a>
														<span class="wb-ent-aana-hub-event-time"><?php echo $wb_session_time_display . " | " . date('h:i:A',strtotime($wb_current_session->wb_virtual_conference_from)) . " - " .  date('h:i:A',strtotime($wb_current_session->wb_virtual_conference_to)) . " " . $wb_current_session->wb_virtual_conference_time_zone; ?></span>
														<span class="wb-ent-aana-hub-event-content"><?php echo $wb_current_session->post_content; ?></span>
													</p>
												</div> 
											</li>
								<?php 
									$wb_date = $wb_current_session->wb_virtual_conference_date;
								}
					   			?>
										</ul>
								<?php 
							} // $wb_hub_post_sessions["posts"]
								?>
								</div>
						<?php 
						foreach ( $wb_hub_post_sessions["posts"] as $key => $wb_current_session ){
							if ( $wb_current_session->wb_virtual_conference_default_media == "redirect-link" )
								continue;
						?>		
						<div class="modal fade" id="wb-ent-aana-hub-show-sessions-<?php echo $wb_current_session->ID;?>" tabindex="-1" role="dialog" aria-labelledby="wb-ent-aana-hub-show-sessions-label" aria-hidden="true" style="display: none;">
							<div class="modal-dialog">
								<div class="modal-content wb-modal-content">
									<div class="modal-body">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<h1 class="wb-ent-aanaapf-header-title-modal"><?php echo $wb_current_session->post_title; ?></h1>
										<div class="wb-ent-aanaapf-header-date-tracks-modal">
											<h2><?php 
												$wb_session_track_name_display = date('l, F d, Y',strtotime($wb_current_session->wb_virtual_conference_date)) . " | " . date('h:i:A',strtotime($wb_current_session->wb_virtual_conference_from));//$wb_current_session->wb_virtual_conference_time_zone;
												if ( $wb_current_session->wb_virtual_conference_time_display != "from" ){
													$wb_session_track_name_display .= ' - ' .  date('h:i:A',strtotime($wb_current_session->wb_virtual_conference_to)); // . " " . $wb_current_session->wb_virtual_conference_time_zone;
												}
												$wb_session_track_name_display .= " " . $wb_current_session->wb_virtual_conference_time_zone;
												echo $wb_session_track_name_display;
											//echo date('l, F d, Y',strtotime($wb_current_session->wb_virtual_conference_date)) . " | " . date('h:i:A',strtotime($wb_current_session->wb_virtual_conference_from)) . ' | ' .  date('h:i:A',strtotime($wb_current_session->wb_virtual_conference_to))  . " " . $wb_current_session->wb_virtual_conference_time_zone; ?>
											</h2>
											<!--<h2>Saturday, August 15 | 7:15 AM - 8:15 AM PST | 10:15 AM - 11:15 AM EST</h2>  -->
											<?php 
											if ( $wb_all_meta_data["wb_live_hub_show_track"][0] == "yes" ){ 
											?>
											<div class="wb-ent-aanaapf-header-tracks-modal">
												<ul>
													<?php 
													if ( count($wb_session_tracks) > (count( $wb_current_session->wb_virtual_conference_category ) - 2)){
														foreach ( $wb_current_session->wb_virtual_conference_category as $wb_current_session_category ){
															if ( $wb_current_session_category->term_id == $wb_hub_category || $wb_hub_main_category == $wb_current_session_category->term_id )
																continue;
														?>	
															<li><p><i class="fas fa-square wb-ent-tracks-<?php echo $wb_current_session_category->slug; ?>"></i> <span><?php echo $wb_current_session_category->name; ?></span></p></li>
														<?php
														}
													}else{
														?>
															<i class="far fa-square " ></i> <span>All Tracks</span>
														<?php
													}
													?>
												</ul>
											</div>
											<?php 
											}
											?>
										</div>
										<?php 
										$wb_virtual_speaker_array = json_decode($wb_current_session->wb_virtual_conference_speaker);
										if ( $wb_virtual_speaker_array ){
											?>
											<h2 class="wb-ent-aanaapf-speaker-txt-modal">Speaker(s)</h2>
											<?php 
											foreach ( $wb_virtual_speaker_array as $wb_current_virtual_speaker ){
												
												if ( trim($wb_current_virtual_speaker->image) == '' && trim($wb_current_virtual_speaker->speaker) == '' && trim($wb_current_virtual_speaker->position) == '' && trim($wb_current_virtual_speaker->work) == '' && trim($wb_current_virtual_speaker->work_address) == ''){
													continue;
												}
												if ( trim($wb_current_virtual_speaker->image) == "" )	{
													$wb_speaker_image = $wb_hub_default_speaker_image;
												}else{
													$wb_speaker_image = urldecode($wb_current_virtual_speaker->image);
												}
										?>
										<div class="wb-ent-aanaapf-speaker-info-area-modal">
											<img src="<?php echo $wb_speaker_image; ?>" alt="Speaker Profile Picture" />
											<div class="wb-ent-aanaapf-speaker-info-modal">
												<?php if ( trim($wb_current_virtual_speaker->speaker) != "" ){?>
												<p class="wb-ent-aanaapf-speaker-name-modal"><?php echo $wb_current_virtual_speaker->speaker; ?></p>
												<?php } ?>
												<?php if ( trim($wb_current_virtual_speaker->position) != "" ){?>
												<p class="wb-ent-aanaapf-speaker-data-modal"><?php echo $wb_current_virtual_speaker->position; ?></p>
												<?php } ?>
												<?php if ( trim($wb_current_virtual_speaker->work) != "" ){?>
												<p class="wb-ent-aanaapf-speaker-data-modal"><?php echo $wb_current_virtual_speaker->work; ?></p>
												<?php } ?>
												<?php if ( trim($wb_current_virtual_speaker->work_address) != "" ){?>
												<p class="wb-ent-aanaapf-speaker-data-modal"><?php echo $wb_current_virtual_speaker->work_address; ?></p>
												<?php } ?>
												<?php if ( trim($wb_current_virtual_speaker->slide) != "" ){?>
												<a href="<?php echo $wb_current_virtual_speaker->slide; ?>" target="_blank" class="btn btn-primary btn-sm wb-ent-aana-slides-btn" role="button">Slides</a>
												<?php } ?>
											</div>
										</div>
										<?php 
											}
										}
										?>
										<p class="wb-ent-aanaapf-hub-session-description-modal"><?php echo wpautop($wb_current_session->post_content); ?></p>
										<?php 
										if ( $wb_current_session->wb_virtual_conference_login_required == "no" || is_user_logged_in() ){
										?>
											<a href="<?php echo "/".$wb_current_session->post_name; ?>" class="btn btn-primary btn-lg wb-ent-aana-modal-login-btn" role="button">View Session</a>
										<?php 
										}else{
										?>
											<a href="<?php echo $wb_current_session->wb_virtual_login_link; ?>" class="btn btn-primary btn-lg wb-ent-aana-modal-login-btn" role="button">Register</a>
										<?php 
										} 
										?>
									</div>
								</div>
							</div>
						</div>
								
						<?php 
						}
						?>	
				</div>
			</div>
        	<!-- END HTML HERE -->
        </div>
	<!--</div>  end #main -->
	<!-- SumoSelect -->
	<script type="text/javascript">
		$(document).ready(function () {
			/*window.Search = $('.search-box').SumoSelect({ csvDispCount: 3, search: true, searchText:'Enter here.' });*/
			$('.wb-ent-aana-filter-search-box').SumoSelect({ csvDispCount: 3, search: true, searchText:'Enter here.' });
		});
	</script>

    <link href="<?php echo WB_VH_DIR_URL . 'assets/css/lightbox-simple-style.css'; ?>" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo WB_VH_DIR_URL . 'assets/js/aana-hub.js'; ?>"></script>
	<link type="text/css" rel="stylesheet" href="<?php echo WB_VH_DIR_URL . 'assets/css/aana-hub.css';?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo WB_VH_DIR_URL . 'assets/css/fontawesome-free-5.13.1-web/css/all.css' ?>" />
	<!-- css.sumoselect plugin -->
	<link type="text/css" rel="stylesheet" href="<?php echo WB_VH_DIR_URL . 'assets/css/sumoselect.css'; ?>" />
	<!-- jQuery.sumoselect plugin -->
	<script type="text/javascript" src="<?php echo WB_VH_DIR_URL . 'assets/js/jquery.sumoselect.min.js'; ?>"></script>

<?php //get_sidebar(); // sidebar 1  ?>

</div> <!-- end content -->
<script type="text/javascript">
	$(document).ready(function () {
		//console.log("load..");
		$('#wb-search-session').off().on('click', function(e) {
			//console.log("search!");
			var wb_date_search = [];
            var wb_session = [];
            var wb_search = $('#wb-session-search').val();
            $('.wb-hub-session').show();
            $('.wb-ent-date-event').show();
            
		    $('#wb-session-date option:selected').each(function(i) {
			    if ( $(this).val() != 'all-dates'){
            		wb_date_search.push($(this).val());
			    }
            });
            
            $('#wb-session-track option:selected').each(function(i) {
            	if ( $(this).val() != 'all-tracks'){
            		wb_session.push($(this).val());
            	}
            });
            
		    $('.wb-hub-session').hide();
		    $('.wb-hub-session').removeClass('wb-first-session');
		    
            $('.wb-hub-session').each(function() {
        		if ( wb_date_search.length > 0 ){
            		for (var i = 0; i < wb_date_search.length; i++) {
                		if ( $(this).hasClass( 'wb-session-date-'+wb_date_search[i] ) ){
                			if ( wb_session.length > 0 ){
                				for (var i = 0; i < wb_session.length; i++) {
                        			if ( $(this).hasClass( 'wb-hub-session-'+wb_session[i] ) ){
                            			if ( wb_search != '' ){
                            				var wb_current_post = $(this);
                            				var wb_current_title = wb_current_post.contents().find( '.wb-ent-aana-hub-event' ).text().toLowerCase();
                            				//console.log(wb_current_title);
                            				var wb_compare = wb_current_title.search( wb_search.toLowerCase() );
                            				if( wb_compare != -1 ){
                            					$(this).show();
                                			}
                            			}else{
                            				$(this).show();
                            			}
                            		}
                                };
                			}else{
                				if ( wb_search != '' ){
                    				var wb_current_post = $(this);
                    				var wb_current_title = wb_current_post.contents().find( '.wb-ent-aana-hub-event' ).text().toLowerCase();
                    				//console.log(wb_current_title);
                    				var wb_compare = wb_current_title.search( wb_search.toLowerCase() );
                    				if( wb_compare != -1 ){
                    					$(this).show();
                        			}
                    			}else{
                    				$(this).show();
                    			}
                			}
                		}
                     };
            	}else if ( wb_session.length > 0 ){
    					for (var i = 0; i < wb_session.length; i++) {
                			if ( $(this).hasClass( 'wb-hub-session-'+wb_session[i] ) ){
                				if ( wb_search != '' ){
                					var wb_current_post = $(this);
                    				var wb_current_title = wb_current_post.contents().find( '.wb-ent-aana-hub-event' ).text().toLowerCase();
                    				//console.log(wb_current_title);
                    				var wb_compare = wb_current_title.search( wb_search.toLowerCase() );
                    				if( wb_compare != -1 ){
                    					$(this).show();
                        			}
                				}else{
                					$(this).show();
                				}
                    		}
                        };
    		   	}else if ( wb_search != '' ){
    		   		var wb_current_post = $(this);
    				var wb_current_title = wb_current_post.contents().find( '.wb-ent-aana-hub-event' ).text().toLowerCase();
    				//console.log(wb_current_title);
    				var wb_compare = wb_current_title.search( wb_search.toLowerCase() );
    				if( wb_compare != -1 ){
    					$(this).show();
        			}
	           	}else{
	           		$(this).show();
	           	}
            });

            var session_found = false;
			var current_id = '';
			var wb_sib = [];
            $('.wb-ent-date-event').each(function() {
            	wb_sib.push( this.id );
            });

            if (wb_sib.length !== 0){
				for (var i = 0; i < wb_sib.length; i++) {
					session_found = false;
					var session_visible = $('#'+wb_sib[i]+' ul > li:visible').length;
					if ( session_visible < 1 ){
						$('#'+wb_sib[i]).hide();
					}else{
						//console.log('with content ' + wb_sib[i]);
						$('#'+wb_sib[i]+' ul li:visible').eq(0).addClass( 'wb-first-session' );
						//console.log('=------------------------------=');
					}
				}
			}
		    //console.log("done!");
		});
		<?php 
			if ( $wb_all_meta_data["wb_live_hub_show_search_sidebar"][0] == 'no' ){
				echo " $('#searchDiv').remove();";
			}
			if ( $wb_all_meta_data["wb_live_hub_show_video_library_sidebar"][0] == 'no' ){
				echo " $('#side-nav #vid_topic').remove();";
			}
			if ( $wb_all_meta_data["wb_live_hub_show_side_banner"][0] == 'no' ){
				echo " $('#side-nav .sidebar-banner').remove();";
			}
			if ( $wb_all_meta_data["wb_live_hub_show_podcast_library_sidebar"][0] == 'no' ){
				echo " $('#side-nav #podcast_topic').remove();";
			}
			if ( $wb_all_meta_data["wb_live_hub_show_subscribe_sidebar"][0] == 'no' ){
				echo " $('#side-nav #subscribe-link').remove();";
			}
			if ( $wb_all_meta_data["wb_live_hub_show_wb_nav"][0] == 'no' ){
				echo " $('#wb-nav').remove();";
			}
		?>
	});



	$('#wb-clear-refresh-session').off().on('click', function(e) {
    	$(".wb-ent-date-event").show();
		$(".wb-hub-session").show();

        $('#wb-session-date option:selected').each(function(i) {
        	$('#wb-session-date')[0].sumo.unSelectItem($(this).val());
        });

        $('#wb-session-track option:selected').each(function(i) {
        	$('#wb-session-track')[0].sumo.unSelectItem($(this).val());
        });
		$('#wb-session-search').val('');
        $('#wb-session-date')[0].sumo.unSelectAll();
        $('#wb-session-track')[0].sumo.unSelectAll();
    });











 		/*
      	This script controls the styling and functionality of the category/topic box on the sidebar(includes/box/vid_topic.php)
      	*/

      	<?php if(!$wb_ent_options['catlisttype']['collapse']){ ?>
      		$('#categories li ul').hide();
			$('#categories .subs-h').addClass("collapsed_boxarrow");
        <?php }else{ ?>
			$('#categories li ul li ul').hide();
		  	$('#categories .subs-h').addClass("expanded_boxarrow");
		<?php }
        	if($wb_ent_options['catlisttype'] == 'image'){
            	$ulMargin = '85px';
            	$arrowBgX = '5px';
            	$arrowDownBgX = '4px';
            	$arrowDownBgY = '4px';
         	}else{
            	$ulMargin = '15px';
            	$arrowBgX = 'left';
            	$arrowDownBgX = '10px';
            	//$arrowDownBgY = 'top';
         	}
        ?>
        /*
         	$('#categories ul ul li').click(function() {
            	$('#vid_list').slideDown(200);
            	$('#video_list').fadeOut(200);
         	});

         	$('#categories .topicList h5').mouseover(function(){
            	$(this).css('color', '#ffffff');
            	$(this).children('i').addClass('icon-white');
            	$(this).children('a').css('color', '#ffffff');
            	$(this).children('i').css('color', '#ffffff');
         	});

         	$('#categories .topicList h5').mouseout(function(){
            	$(this).css('color', '#000000');
             	$(this).children('i').removeClass('icon-white');
             	$(this).children('a').css('color', '#000000');
         	});

         	$('#categories ul li').hover(function() {
            	$(this).css('cursor','pointer');
         	});
         	$('#categories ul ul li').hover(function() {
            	//$(this).css({'cursor':'pointer', 'color':'#016891', 'padding-left':'8px',  'text-decoration':'none'});
         	});
         	$('#categories ul ul li').mouseout(function() {
            	//$(this).css({'cursor':'arrow', 'color':'#000000', 'padding-left':'8px', 'text-decoration':'none'});
         	});

         	*/
      	
	</script>    
<?php
get_footer();
?>