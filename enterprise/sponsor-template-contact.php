<?php
/*

  Filename:      sponsor-template-videoSolutions.php
  Description:   will display the Video Solutions(page 1) part of the Sposonr Template for Naylor
  Author: 	      Jullie Quijano
  Change Log:
  November 26, 2013 [Jullie]added code to display message if file is included
 * August 29, 2012 	[Jullie] Created the file                                                                                                                                                                                                              

 * @package WordPress
 * @subpackage Enterprise APF
 */
/*
  Template Name: Sponsor Template Contact Form v.6
 */

//error_reporting(E_ALL);
//echo '	lib ';
// Sitewide Variables

if (!defined('CHANNEL_SITE'))
    include get_template_directory_uri() . '/library/includes/config.php';

get_header();
//include 'includes/navigation.php';
//will determine if file is included
if ($_SERVER['REMOTE_ADDR'] == WORKERBEE_IP) {

    echo '<!-- START OF ' . __FILE__ . ' -->';
}
global $wb_ent_options;
$wb_ent_options = get_option('wb_ent_options');
//$sponsorContact = 'Dan Stevens <dan.stevens@workerbee.tv>, Aj Batac <webmaster@workerbee.tv>, Laura Taylor <letaylor@naylor.com>' ; //old contact infor
//$sponsorContact = 'Dan Stevens <dan.stevens@workerbee.tv>, Aj Batac <webmaster@workerbee.tv>, PamD@naylor.com' ;
$sponsorContact = 'Jullie Quijano<jullie.quijano@workerbee.tv>, Lourice Capili<lourice.capili@workerbee.tv>';
//$sponsorContact = 'AJ Batac <webmaster@workerbee.tv>, Cpopper <cpopper@naylor.com>, Dan Stevens <dan.stevens@workerbee.tv>, Dana P. <dplotke@naylor.com>';
$sponsorSubject = CLIENT_CHANNEL . ' | Sponsorship Information Request';
$max = CONFORM_UPLOADMAX;
$uploadMax = CONFORM_SIZEMAX;
$error = 0;
$errormsg = '';

$linkID = time();
$linkID .= (mt_rand(1, 9999999999)); // Random # between 1 and 9,999,999,999


if (isset($_POST['cSubmit'])) {

    require_once('resources/recaptcha/recaptchalib.php');
    /* old recaptcha 
    $privatekey = $wb_ent_options['recaptchapriv'];
    $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
    if (!$resp->is_valid) {
        $error = 1;
        $errormsg .= "The reCAPTCHA wasn't entered correctly. Please try it again.<br />" .
                "<!--(reCAPTCHA said: " . $resp->error . ")-->";
    }*/ 
     $curlString = 'https://www.google.com/recaptcha/api/siteverify?secret='.$wb_ent_options['recaptchapriv'].'&response='.$_POST['g-recaptcha-response'];

     $ch = curl_init();
     $timeout = 0; // set to zero for no timeout
     curl_setopt($ch, CURLOPT_URL, $curlString);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
     $file_contents = curl_exec($ch);
     echo '<!-- $file_contents is '.print_r($file_contents, true).' -->';
     curl_close($ch);
     $returndata = json_decode($file_contents);
     if( !$returndata->success ){
        $error = 1;
        $errormsg .= "The reCAPTCHA wasn't entered correctly. Please try it again.<br />" .
           "<!--(reCAPTCHA said: " . $resp->error . ")-->";
    } 

    function sendEmails($c, $clientChannel, $contactURL, $to, $subject) {
        $from = stripslashes($c['firstName']['value']) . ' <' . $c['email']['value'] . '>';
        $headers = '';
        $headers .= 'From: ' . $from . "\r\n";
        $message = "First Name: " . stripslashes($c['firstName']['value']);
        $message .= "\nLast Name: " . stripslashes($c['lastName']['value']);
        $message .= "\nTitle: " . stripslashes($c['title']['value']);
        $message .=" \nAssociation: " . stripslashes($c['association']['value']);
        $message .= "\nEmail: " . stripslashes($c['email']['value']);
        $message .= "\nPhone: " . stripslashes($c['phone']['value']);
        $message .= "\nComments: " . stripslashes($c['comments']['value']);

        mail($to, $subject, $message, $headers);
    }

    $linkID = $_POST['linkID'];
    $c['firstName'] = array('value' => $_POST['firstName'], 'name' => 'First Name');
    $c['lastName'] = array('value' => $_POST['lastName'], 'name' => 'Last Name');
    $c['title'] = array('value' => $_POST['title'], 'name' => 'Title');
    $c['association'] = array('value' => $_POST['association'], 'name' => 'Association');
    $c['email'] = array('value' => $_POST['email'], 'name' => 'Email Address');
    $c['phone'] = array('value' => $_POST['phone'], 'name' => 'Phone Number');
    $c['comments'] = array('value' => $_POST['comments'], 'name' => 'Comments');

    foreach ($c as $item) {
        //$len = strlen($item['value']);
        switch ($item['name']) {
            /*
              case 'comments':
              switch($item['value']){
              case '':
              $errormsg .= 'Please enter a value for '.$item['name'].'.<br />';
              $error=1;
              break;
              default: break;
              }
              break; */
            default:
                switch ($item['value']) {
                    case '':
                        if ($item['name'] == 'Title') {
                            break;
                        }
                        if ($item['name'] == 'Comments') {
                            break;
                        }
                        $errormsg .= 'Please enter a value for ' . $item['name'] . '.<br />';
                        $error = 1;
                        break;
                    default: break;
                }
                break;
        }
    }

    switch ($c['email']['value']) {
        case '': break;
        case (!preg_match("/^[a-z0-9_-][a-z0-9._-]+@([a-z0-9][a-z0-9-]*\.)+[a-z]{2,6}$/i", $c['email']['value'])):
            $errormsg .= 'Sorry, the email address you entered is not valid.<br />';
            $error = 1;
            break;
        default: break;
    }

    switch ($error) {
        case 0: sendEmails($c, $wb_ent_options['channelname'], '', $wb_ent_options['notification']['adform']['recipient'], $wb_ent_options['notification']['adform']['subject']);
            
            break;
        default: break;
    }
}
?>

<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/naylorsponsor.css" />

<style>
    #contactPar {float: left; /*width: 396px;*/}
    #contactPar ol {
        text-align: left;
        margin: 20px 40px;
        font-size: 15px;
    }

    #contactPar ol li {
        list-style: decimal outside;
        margin: 3px 0;
    }
    #videoContent {clear: both;}

    #contactPar {
        float: left; /*width: 550px;*/
        margin-bottom: 75px !important;
    }

    #apfScreenshot {
        float: right;
        margin: 30px 0 10px;
    }

    #wb_ent_content #centerMessage {
        width: 575px;
        height: 50px;
        margin: 25px auto 0;
        background-color: #d9e5ee;
        color: #243c5f;
        font-size: 18px;
        clear: both;
        font-weight: bold;
        padding: 25px 47px;
    }

    #wb_ent_content #vidEverywhere {
        float: left;
        margin: 24px 0 10px 5px;
    }

    #wb_ent_content #embedDivMain {
        width: 630px;
        height: 250px;
        margin: 50px auto 55px;
    }

    #wb_ent_content .embedDiv {
        width: 300px;
        float: left;
        font-size: 15px;
    }

    fieldset {
        border: 0 none;
        margin: 0 40px; /*width: 500px;*/
    }

    #contactPar fieldset {
        padding: 5px;
        border: 1px solid #AAAAAA;
        border-radius: 4px;
    }

    #contactForm input.styled, #contactForm textarea, #contactForm select {
        border: 1px solid #777777;
        border-radius: 4px;
        margin: 5px 0;
        min-height: 20px;
        padding: 2px; /*width: 290px;*/
    }

    #contactForm input[type="submit"], #contactForm input.submit {
        //background: none repeat scroll 0 0 #454545;
        border: medium none;
        color: #FFFFFF;
        cursor: pointer;
        font-size: 18px;
        outline: medium none;
        padding: 10px 15px;
        position: relative;
        text-align: center;
        width: auto;
        margin-top: 10px;
        border-radius: 4px;
    }

    .errors {
        background: none repeat scroll 0 0 #FFEEEE;
        border: 1px solid #FF0000;
        color: #FF0000;
        margin: 20px auto;
        padding: 10px 30px;
        width: 335px;
    }

    #contactForm label {
        font-size: 15px;
        font-weight: bold;
        
    }
</style>

<div id="wb_ent_content" class="clearfix row-fluid">
    <div id="wb_ent_main" class="span12 clearfix" role="main" style="border: 0px solid black;">
        <div id="sectionNav">
            <ul>
                <li><a href="/naylor-banner-solutions"><?= _e('Banner Solutions', 'enterprise') ?></a></li>
                <li><a href="/naylor-video-solutions" ><?= _e('Video Solutions', 'enterprise') ?></a></li>
                <li><a href="/naylor-video-production"><?= _e('Video Production', 'enterprise') ?></a></li>
                <li><a href="/sponsor-contact" class="currentPage"><?= _e('Request Information', 'enterprise') ?></a></li>                
            </ul>
        </div>      
        <div class="container-fluid" style="clear:both;">
            <!--<h1>Schedule a Meeting</h1>-->
        </div>


        <div class="span7" id="contactPar">
            <p><?= _e('Engage your market with video services from Naylor! We are proud to offer you full-service video content marketing solutions to:', 'enterprise') ?></p>
            <ol>
                <li><?= _e('Leverage your existing videos, or', 'enterprise') ?></li>
                <li><?= _e('Create and distribute new video content to engage your target market, from creating the concept, to filming, editing, and online distribution.', 'enterprise') ?></li>
            </ol>
            <p style="margin-bottom: 0px;"><?= _e('Fill out the form to receive more information or schedule a call on how you can use content marketing to effectively engage your audience and capture new clients.', 'enterprise') ?></p>

            <form name="contactForm" id="contactForm" action="<?= $_SERVER['REQUEST_URI'] ?>" enctype="multipart/form-data" method="post">

                <fieldset class="span10" style="padding:10px;">

<?php
if ($error != 0) {
    echo '<div class="errors">' . $errormsg . '<br /></div>';
} else if (isset($_POST['cSubmit'])) {
    echo '<h3>Thank you. We have received your information and will get back to you as soon as possible.</h3>';
}
?>
                    <div<?php if (isset($_POST['cSubmit']) && $error == 0) echo ' style="display:none"'; ?>>
                        <label for="firstName"><?= _e('First Name', 'enterprise') ?> *</label>
                        <input class="styled" type="text" name="firstName" id="firstName" value="<?php echo $_POST['firstName']; ?>" /><br />              

                        <label for="lastName"><?= _e('Last Name', 'enterprise') ?> *</label>
                        <input class="styled" type="text" name="lastName" id="lastName" value="<?php echo $_POST['lastName']; ?>" /><br />              

                        <label for="association"><?= _e('Company', 'enterprise') ?> *</label>
                        <input class="styled" type="text" name="association" id="association" value="<?php echo $_POST['association']; ?>" /><br />

                        <label for="title"><?= _e('Title', 'enterprise') ?></label>
                        <input class="styled" type="text" name="title" id="title" value="<?php echo $_POST['title']; ?>" /><br />

                        <label for="email"><?= _e('Email', 'enterprise') ?> *</label>
                        <input class="styled" type="text" name="email" id="email" value="<?php echo $_POST['email']; ?>" /><br />

                        <label for="phone"><?= _e('Phone', 'enterprise') ?> *</label>
                        <input class="styled" type="text" name="phone" id="phone" value="<?php echo $_POST['phone']; ?>" /><br />        

                        <label for="comments" style=""><?= _e('Comments/Questions', 'enterprise') ?></label><br />
                        <textarea rows="5" name="comments" id="comments" style=""><?php echo $_POST['comments']; ?></textarea><br />             

                        <span style="font-size: 13px;"><?= _e('All fields with asterisk (*) are required.', 'enterprise') ?></span>
                        <br />
                        <br />     

<?php
// require_once('resources/recaptcha/recaptchalib.php');
// $publickey = $wb_ent_options['recaptchapub']; // you got this from the signup page
// echo recaptcha_get_html($publickey);

?>
<div class="g-recaptcha" data-sitekey="<?php echo $wb_ent_options['recaptchapub']; ?>"></div> 

                        <input type="submit" name="cSubmit" id="cSubmit" value="Submit" class="btn btn-info" />
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="section span4" id="vidEverywhere">         
            <img src="<?php echo get_template_directory_uri(); ?>/images/sponsorTempVidEverywhere.jpg" />
        </div>    
        <div style="clear:both"></div>

        <div class="section" style="">

            <a class="btn btn-primary" style="padding:20px 24px;text-decoration: none; font-size: 16px;" href="/naylor-banner-solutions"><?= _e('Our Banner Solutions', 'enterprise') ?></a>

            <a class="btn btn-primary" style="padding:20px 24px;text-decoration: none; font-size: 16px;" href="/naylor-video-solutions"><?= _e('Our Video Solutions', 'enterprise') ?></a>

            <a class="btn btn-primary" style="padding:20px 24px;text-decoration: none; font-size: 16px;" href="/naylor-video-production">

                <span><?= _e('Learn more about our Video Production Process', 'enterprise') ?></span></a>

        </div> 

    </div>

</div>


<?php
//will determine if file is included

if ($_SERVER['REMOTE_ADDR'] == WORKERBEE_IP) {

    echo '<!-- END OF ' . __FILE__ . ' -->';
}
get_footer();
?>