<?php
/**
 * filename: single.php
 * description: this will include the proper template depending on what kind of post is to be displayed
 * author: Jullie Quijano
 * date created: 2014-03-27
*/

global $postId, $wb_ent_options, $wpdb;
$postId = $wp_query->post->ID; 
$post_name = $wp_query->post->post_name;
$currentCast = wb_get_most_recent($wb_ent_options['videocats']['webcast']);
$featureVideo = wb_get_most_recent($wb_ent_options['videocats']['feature']);
$mostRecentVideo = wb_get_most_recent($wb_ent_options['videocats']['library']);

$unlistedCat = $wb_ent_options['videocats']['unlisted'];
$privateCat = $wb_ent_options['videocats']['private'];
$membersOnlyCat = $wb_ent_options['videocats']['members'];

global $video, $unlistedPostsIds, $privatePostsIds;
$video = wb_get_post_details($postId);

if($wb_ent_options['channelformat'] == 'playlist'){
    include(get_template_directory() . '/video-playlist.php');
}
else if( ( !is_user_logged_in() ) && (in_array( $postId,  $privatePostsIds)  ) ){
    include(get_template_directory() . '/404.php');
}
else if( has_category( $membersOnlyCat, $postId ) ){
  if( !is_user_logged_in() ){
    //include(get_stylesheet_directory() . '/wb-sso-login-page.php');
    include(get_stylesheet_directory() . '/wb-sso-content-landing-page.php');
  }
  else{
    //get user roles
    $user_info = get_userdata(get_current_user_id());
    if( in_array('administrator', $user_info->roles) || in_array('editor', $user_info->roles) ||
        (in_array($wb_ent_options['roles']['members'], $user_info->roles) && has_category( $membersOnlyCat, $postId )) ){
      include(get_stylesheet_directory() . '/video-page.php');
    }
    else{
      //include(get_stylesheet_directory() . '/wb-sso-login-page.php');
      include(get_stylesheet_directory() . '/wb-sso-content-landing-page.php');
    } 
  }  
}
else if( is_array($video) ){
    include(get_template_directory() . '/video-page.php');
}
?>
