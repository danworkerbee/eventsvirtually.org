<?php
/**
 * filename: userImport.php
 * description: this will be the default template to be used for the theme
 * author: Jullie Quijano
 * date created: 2014-03-25
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: User Import Page
 */
$wb_ent_options = get_option('wb_ent_options');

//print_r($wb_ent_options);

$ipRestrictionArray = explode(',', $wb_ent_options['workerbeeip'] );

$allowedIps = array();
foreach($ipRestrictionArray as $currentIp){
    if(trim($currentIp) != '' ){
        $allowedIps[] = trim($currentIp);
    }
    
}
 
$userIp = trim($_SERVER['REMOTE_ADDR']);
//echo $userIp;
$allowedIps[] = $userIp;

if( !in_array($userIp, $allowedIps) || trim($wb_ent_options['workerbeeip']) == '' )
{
echo 'Please check back later. Thanks.';
exit;
}  
/*
 * Instructions:
1. open the xls file and replace all commas with "|||". This will make manipulating information easier
2. rename the headings for each of the the column to the following text. if additional user_meta need to be saved, replace the heading with the meta_key for that user meta. for example, adding a title for the user, use wb_user_title for the column heading. 
  DEFAULT ARGUMENTS
    'ID', 
    'user_pass',
    'user_login',
    'user_nicename',
    'user_url',
    'user_email',
    'display_name',
    'nickname',
    'first_name',
    'last_name',
    'description',
    'rich_editing',
    'user_registered',
    'role',
    'jabber',
    'aim',
    'yim',

    WOOCOMMERCE ARGUMENTS
    billing_first_name
    billing_last_name
    billing_email
    billing_company
    billing_address_1
    billing_address_2
    billing_city
    billing_state
    billing_postcode
    billing_country
    billing_phone
 3. If countries are going to be added for the user, make sure that the value of the country inside a cell corresponds to the values inside the $countries_name array below, if not, update the values as needed. Countries to watch out for would be United States, United Kingdom, Republic of Korea, Taiwan, Russia, Cote d'Ivoire. See https://lists.mysql.com/mysql/192008 for new list.
 4. Do a special find and replace to look for empty cells
    a. Select all the cells inside the sheet
    b. Do a find and replace. Leave the Find what field blank and enter the string "-empty-" in the Replace with field. This will fill the cells with the specified string.
 5. Save the file as a CSV file.
 6. Upload CSV file to inside public_html
 7. Modify the userImport.php file
    a. set the $filePath to the name of the test csv file
    b. set the $userRole to whatever userrole you would like to add the users to
    c. set the $userRoleName to the proper name for this role
    d. set the $defaultPassword to whatever password you would like to use for all users
    e. set the $updateRole to true ONLY if you want to change the role of an existing user
 8. On the WordPress backend, create a new page and set the template to User Import Page
 9. Open the page to test
 10. Go to the All Users page to see if the role was created and if each user has the correct information
 11. Once the users have been successfully imported, clear the values for the reuired variables below and delete the file inside public_html and delete the page you just created
  NOTE: If there are a lot of users, create a test version of the CSV file first that only contains about 20 or so entries. Also, make sure to split the entries into multiple 
  sheets so there's a maximum 1000 entries each sheet so that it will not use up too much resources on the server.
 
 DELETING USERS
 to delete users, you can use the delete_users_from_role($userRole) function. 
 
 * 
 */

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*REQUIRED Variables*/
$filePath = "afs-registration-08262020.csv";//"ANFPMemberList_20200501_import.csv"; //ANFPMemberList_Sept042019_import.csv
$userRole = "afs-hub-2020";
$userRoleName = "ATP 2020 Global Summit";
$defaultPassword = 'atp2020globalsummit!';
$updateRole = false;
$userImportReport_file_name = "afs-registration-08262020-import.csv"; // "userImportReport20200601_import.csv";
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$defaultWpUserArgs = array(
   'ID', 
   'user_pass',
   'user_login',
   'user_nicename',
   'user_url',
   'user_email',
   'display_name',
   'nickname',
   'first_name',
   'last_name',
   'description',
   'rich_editing',
   'user_registered',
   'role',
   'jabber',
   'aim',
   'yim',
   );

if( trim($filePath) == '' ){
  exit;
}
//comment the next line when importing new user
echo "Starting....<br />";
exit();
if ( ! isset( $wp_roles ) )
    $wp_roles = new WP_Roles();

$customer = $wp_roles->get_role('customer');
$wp_roles->add_role($userRole, $userRoleName, $customer->capabilities);

$fileContent = file_get_contents($filePath);

print_r (explode("\r\n",$fileContent));

$rows = explode("\r\n",$fileContent);

// echo $fileContent;
// var_dump($fileContent);
//echo "\r\n".'$rows is '.print_r($rows, true);

//echo "\r\n".'$rows[0] is '.print_r($rows[0], true);

//the first row will indicate the fields to fill out
$fieldTitlesTemp = explode(",", $rows[0]);

//echo "\r\n".'$fieldTitlesTemp is '.print_r($fieldTitlesTemp, true);

$fieldTitles = array();
$col = 0;
$fieldCounter = 0;
$array_for_file = array();
foreach($fieldTitlesTemp as $currentFieldTitle){
    if( trim($currentFieldTitle) != '' ){
        $fieldTitles[$fieldCounter]['col'] = $col;
        $fieldTitles[$fieldCounter]['name'] = trim($currentFieldTitle);
    }
    $fieldCounter++;
    $col++;
}

//echo "\r\n".'$fieldTitles is '.print_r($fieldTitles, true);
$new_user = 0;
$allUsers = array();
$numRows = count($rows);
for($i = 1; $i< $numRows; $i++){
    $currentUserArgs = array();
    if( trim($rows[$i]) == '' ){
        continue;
    }
    //echo "\r\n".'$rows[$i] is '.print_r($rows[$i], true);
    
    $currentRowValues = explode(",", $rows[$i]);
    
    //echo "\r\n".'$currentRowValues is '.print_r($currentRowValues, true);
    
    foreach($fieldTitles as $currentField){        
        if( trim($currentRowValues[$currentField['col']] ) == '-empty-'){
          continue;
        }
        
        if( trim($currentField['name']) == 'country' || trim($currentField['name']) == 'countries' || strpos($currentField['name'], 'ountry') > 0){
          $currentRowValues[$currentField['col']] = get_country_code($currentRowValues[$currentField['col']]);
        }

        if( in_array($currentField['name'], $defaultWpUserArgs) ){
        	//echo "<br />in ".$currentField['name'];
            $currentUserArgs['userInfo'][$currentField['name']] = str_replace('|||', ',', $currentRowValues[$currentField['col']] );
        }else{
            $currentUserArgs['userInfo'][$currentField['name']] = str_replace('|||', ',', $currentRowValues[$currentField['col']] );
        }
        
    }
    
    //echo "<br />email " . $currentUserArgs['userInfo']['user_email'] . "<br />";
    //echo "<pre>";
    //print_r($currentUserArgs);
    //echo "</pre>";
    //if the user does not have their email address on this list, add to array to print to txt file
    //echo $currentUserArgs['userInfo']['user_email'];
    if( !isset($currentUserArgs['userInfo']['user_email']) || trim($currentUserArgs['userInfo']['user_email']) == '' ){
    	echo "<br />".'email is blank' . $filePath;
      $array_for_file[] = $currentRowValues;
      continue;
    }
    
    //echo "\r\n".'$currentUserArgs after foreach is '.print_r($currentUserArgs, true);
    
    if( !isset($currentUserArgs['userInfo']['user_login']) || trim($currentUserArgs['userInfo']['user_login']) == '' ){
        $currentUserArgs['userInfo']['user_login'] = $currentUserArgs['userInfo']['user_email'];
    }
    
    if( !isset($currentUserArgs['userInfo']['role']) || trim($currentUserArgs['userInfo']['role']) == '' ){
        $currentUserArgs['userInfo']['role'] = $userRole;
    }    
    
    if( !isset($currentUserArgs['userInfo']['user_pass']) || trim($currentUserArgs['userInfo']['user_pass']) == '' ){
        $currentUserArgs['userInfo']['user_pass'] = $defaultPassword;
    }      
    
    //echo "\r\n".'$currentUserArgs after ifs is '.print_r($currentUserArgs, true);
                
    $allUsers[] = $currentUserArgs;
    
    $currenUserId = wp_insert_user( $currentUserArgs['userInfo'] );
    
    //echo "<br />".'$currenUserId is '.print_r($currenUserId, true);
    
    if( is_numeric($currenUserId) ){
    	echo "<br /> New - " . $currenUserId . " - " . $currentUserArgs['userInfo']['user_email'];
    	$new_user++;
        foreach($currentUserArgs['userMeta'] as $currentMetaName => $currentMetaValue){
            update_user_meta( $currenUserId, $currentMetaName, $currentMetaValue );
        }
    }
    elseif( $updateRole ){
      $existingUserId = email_exists( $currentUserArgs['userInfo']['user_email'] );
      echo "\r\n".'$existingUserId is '.$existingUserId;
      if( is_numeric($existingUserId) ) { //check if user exists
        //update to make sure the user role is correct
        $u = new WP_User( $existingUserId );
        $current_user_roles = $u->roles;
        echo "\r\n".'$current_user_roles is '.print_r($current_user_roles, true);
           if(!in_array('editor', $current_user_roles)  && !in_array('administrator', $current_user_roles) && !in_array('admin', $current_user_roles)){             
            $update_role_results = wp_update_user( array( 'ID' => $existingUserId, 'role' => $userRole ) );       
            echo "\r\n".'$update_role_results is '.print_r($update_role_results, true);
         }
      }

    }else{
    	$existingUserId = email_exists( $currentUserArgs['userInfo']['user_email'] );
    	echo "<br />" . $existingUserId . " - " .$currentUserArgs['userInfo']['user_email'];
    }
    
    
    
}

if( count($array_for_file) > 0 ){
  //echo "\r\n".'$array_for_file is '.print_r($array_for_file, true);
  $file = fopen($userImportReport_file_name,"a");

  foreach ($array_for_file as $line){
    fputcsv($file,$line);
  }

  fclose($file);
}

echo "<br />Done! " . $new_user . " new user(s)";

function get_country_code($string){

  $countries_with_code = array(
    "�land Islands"=>"AX",
    "Afghanistan"=>"AF",
    "Albania"=>"AL",
    "Algeria"=>"DZ",
    "American Samoa"=>"AS",
    "Andorra"=>"AD",
    "Angola"=>"AO",
    "Anguilla"=>"AI",
    "Antarctica"=>"AQ",
    "Antigua and Barbuda"=>"AG",
    "Argentina"=>"AR",
    "Armenia"=>"AM",
    "Aruba"=>"AW",
    "Australia"=>"AU",
    "Austria"=>"AT",
    "Azerbaijan"=>"AZ",
    "Bahamas"=>"BS",
    "Bahrain"=>"BH",
    "Bangladesh"=>"BD",
    "Barbados"=>"BB",
    "Belarus"=>"BY",
    "Belau"=>"PW",
    "Belgium"=>"BE",
    "Belize"=>"BZ",
    "Benin"=>"BJ",
    "Bermuda"=>"BM",
    "Bhutan"=>"BT",
    "Bolivia"=>"BO",
    "Bonaire, Saint Eustatius and Saba"=>"BQ",
    "Bosnia and Herzegovina"=>"BA",
    "Botswana"=>"BW",
    "Bouvet Island"=>"BV",
    "Brazil"=>"BR",
    "British Indian Ocean Territory"=>"IO",
    "British Virgin Islands"=>"VG",
    "Brunei"=>"BN",
    "Bulgaria"=>"BG",
    "Burkina Faso"=>"BF",
    "Burundi"=>"BI",
    "Cambodia"=>"KH",
    "Cameroon"=>"CM",
    "Canada"=>"CA",
    "Cape Verde"=>"CV",
    "Cayman Islands"=>"KY",
    "Central African Republic"=>"CF",
    "Chad"=>"TD",
    "Chile"=>"CL",
    "China"=>"CN",
    "Christmas Island"=>"CX",
    "Cocos (Keeling) Islands"=>"CC",
    "Colombia"=>"CO",
    "Comoros"=>"KM",
    "Congo (Brazzaville)"=>"CG",
    "Congo (Kinshasa)"=>"CD",
    "Cook Islands"=>"CK",
    "Costa Rica"=>"CR",
		"Cote D\'Ivoire" => "CI",
    "Croatia"=>"HR",
    "Cuba"=>"CU",
    "Cura�ao"=>"CW",
    "Cyprus"=>"CY",
    "Czech Republic"=>"CZ",
    "Denmark"=>"DK",
    "Djibouti"=>"DJ",
    "Dominica"=>"DM",
    "Dominican Republic"=>"DO",
    "Ecuador"=>"EC",
    "Egypt"=>"EG",
    "El Salvador"=>"SV",
    "Equatorial Guinea"=>"GQ",
    "Eritrea"=>"ER",
    "Estonia"=>"EE",
    "Ethiopia"=>"ET",
    "Falkland Islands"=>"FK",
    "Faroe Islands"=>"FO",
    "Fiji"=>"FJ",
    "Finland"=>"FI",
    "France"=>"FR",
    "French Guiana"=>"GF",
    "French Polynesia"=>"PF",
    "French Southern Territories"=>"TF",
    "Gabon"=>"GA",
    "Gambia"=>"GM",
    "Georgia"=>"GE",
    "Germany"=>"DE",
    "Ghana"=>"GH",
    "Gibraltar"=>"GI",
    "Greece"=>"GR",
    "Greenland"=>"GL",
    "Grenada"=>"GD",
    "Guadeloupe"=>"GP",
    "Guam"=>"GU",
    "Guatemala"=>"GT",
    "Guernsey"=>"GG",
    "Guinea"=>"GN",
    "Guinea-Bissau"=>"GW",
    "Guyana"=>"GY",
    "Haiti"=>"HT",
    "Heard Island and McDonald Islands"=>"HM",
    "Honduras"=>"HN",
    "Hong Kong"=>"HK",
    "Hungary"=>"HU",
    "Iceland"=>"IS",
    "India"=>"IN",
    "Indonesia"=>"ID",
    "Iran"=>"IR",
    "Iraq"=>"IQ",
    "Ireland"=>"IE",
    "Isle of Man"=>"IM",
    "Israel"=>"IL",
    "Italy"=>"IT",
    "Ivory Coast"=>"CI",
    "Jamaica"=>"JM",
    "Japan"=>"JP",
    "Jersey"=>"JE",
    "Jordan"=>"JO",
    "Kazakhstan"=>"KZ",
    "Kenya"=>"KE",
    "Kiribati"=>"KI",
    "Kuwait"=>"KW",
    "Kyrgyzstan"=>"KG",
    "Laos"=>"LA",
    "Latvia"=>"LV",
    "Lebanon"=>"LB",
    "Lesotho"=>"LS",
    "Liberia"=>"LR",
    "Libya"=>"LY",
    "Liechtenstein"=>"LI",
    "Lithuania"=>"LT",
    "Luxembourg"=>"LU",
    "Macao S.A.R., China"=>"MO",
    "Macedonia"=>"MK",
    "Madagascar"=>"MG",
    "Malawi"=>"MW",
    "Malaysia"=>"MY",
    "Maldives"=>"MV",
    "Mali"=>"ML",
    "Malta"=>"MT",
    "Marshall Islands"=>"MH",
    "Martinique"=>"MQ",
    "Mauritania"=>"MR",
    "Mauritius"=>"MU",
    "Mayotte"=>"YT",
    "Mexico"=>"MX",
    "Micronesia"=>"FM",
    "Moldova"=>"MD",
    "Monaco"=>"MC",
    "Mongolia"=>"MN",
    "Montenegro"=>"ME",
    "Montserrat"=>"MS",
    "Morocco"=>"MA",
    "Mozambique"=>"MZ",
    "Myanmar"=>"MM",
    "Namibia"=>"NA",
    "Nauru"=>"NR",
    "Nepal"=>"NP",
    "Netherlands"=>"NL",
    "New Caledonia"=>"NC",
    "New Zealand"=>"NZ",
    "Nicaragua"=>"NI",
    "Niger"=>"NE",
    "Nigeria"=>"NG",
    "Niue"=>"NU",
    "Norfolk Island"=>"NF",
    "North Korea"=>"KP",
    "Northern Mariana Islands"=>"MP",
    "Norway"=>"NO",
    "Oman"=>"OM",
    "Pakistan"=>"PK",
    "Palestinian Territory"=>"PS",
    "Panama"=>"PA",
    "Papua New Guinea"=>"PG",
    "Paraguay"=>"PY",
    "Peru"=>"PE",
    "Philippines"=>"PH",
    "Pitcairn"=>"PN",
    "Poland"=>"PL",
    "Portugal"=>"PT",
    "Puerto Rico"=>"PR",
    "Qatar"=>"QA",
    "Reunion"=>"RE",
    "Romania"=>"RO",
    "Russia"=>"RU",
    "Rwanda"=>"RW",
    "S�o Tom� and Pr�ncipe"=>"ST",
    "Saint Barth�lemy"=>"BL",
    "Saint Helena"=>"SH",
    "Saint Kitts and Nevis"=>"KN",
    "Saint Lucia"=>"LC",
    "Saint Martin (Dutch part)"=>"SX",
    "Saint Martin (French part)"=>"MF",
    "Saint Pierre and Miquelon"=>"PM",
    "Saint Vincent and the Grenadines"=>"VC",
    "Samoa"=>"WS",
    "San Marino"=>"SM",
    "Saudi Arabia"=>"SA",
    "Senegal"=>"SN",
    "Serbia"=>"RS",
    "Seychelles"=>"SC",
    "Sierra Leone"=>"SL",
    "Singapore"=>"SG",
    "Slovakia"=>"SK",
    "Slovenia"=>"SI",
    "Solomon Islands"=>"SB",
    "Somalia"=>"SO",
    "South Africa"=>"ZA",
    "South Georgia/Sandwich Islands"=>"GS",
    "South Korea"=>"KR",
    "South Sudan"=>"SS",
    "Spain"=>"ES",
    "Sri Lanka"=>"LK",
    "Sudan"=>"SD",
    "Suriname"=>"SR",
    "Svalbard and Jan Mayen"=>"SJ",
    "Swaziland"=>"SZ",
    "Sweden"=>"SE",
    "Switzerland"=>"CH",
    "Syria"=>"SY",
    "Taiwan"=>"TW",
    "Tajikistan"=>"TJ",
    "Tanzania"=>"TZ",
    "Thailand"=>"TH",
    "Timor-Leste"=>"TL",
    "Togo"=>"TG",
    "Tokelau"=>"TK",
    "Tonga"=>"TO",
    "Trinidad and Tobago"=>"TT",
    "Tunisia"=>"TN",
    "Turkey"=>"TR",
    "Turkmenistan"=>"TM",
    "Turks and Caicos Islands"=>"TC",
    "Tuvalu"=>"TV",
    "Uganda"=>"UG",
    "Ukraine"=>"UA",
    "United Arab Emirates"=>"AE",
    "United Kingdom (UK)"=>"GB",
    "United States (US)"=>"US",
    "United States (US) Minor Outlying Islands"=>"UM",
    "United States (US) Virgin Islands"=>"VI",
    "Uruguay"=>"UY",
    "Uzbekistan"=>"UZ",
    "Vanuatu"=>"VU",
    "Vatican"=>"VA",
    "Venezuela"=>"VE",
    "Vietnam"=>"VN",
    "Wallis and Futuna"=>"WF",
    "Western Sahara"=>"EH",
    "Yemen"=>"YE",
    "Zambia"=>"ZM",
    "Zimbabwe"=>"ZW"
  );

  $countries_name = array(
    "�land Islands",
    "Afghanistan",
    "Albania",
    "Algeria",
    "American Samoa",
    "Andorra",
    "Angola",
    "Anguilla",
    "Antarctica",
    "Antigua and Barbuda",
    "Argentina",
    "Armenia",
    "Aruba",
    "Australia",
    "Austria",
    "Azerbaijan",
    "Bahamas",
    "Bahrain",
    "Bangladesh",
    "Barbados",
    "Belarus",
    "Belau",
    "Belgium",
    "Belize",
    "Benin",
    "Bermuda",
    "Bhutan",
    "Bolivia",
    "Bonaire, Saint Eustatius and Saba",
    "Bosnia and Herzegovina",
    "Botswana",
    "Bouvet Island",
    "Brazil",
    "British Indian Ocean Territory",
    "British Virgin Islands",
    "Brunei",
    "Bulgaria",
    "Burkina Faso",
    "Burundi",
    "Cambodia",
    "Cameroon",
    "Canada",
    "Cape Verde",
    "Cayman Islands",
    "Central African Republic",
    "Chad",
    "Chile",
    "China",
    "Christmas Island",
    "Cocos (Keeling) Islands",
    "Colombia",
    "Comoros",
    "Congo (Brazzaville)",
    "Congo (Kinshasa)",
    "Cook Islands",
    "Costa Rica",
		"Cote D\'Ivoire",
    "Croatia",
    "Cuba",
    "Cura�ao",
    "Cyprus",
    "Czech Republic",
    "Denmark",
    "Djibouti",
    "Dominica",
    "Dominican Republic",
    "Ecuador",
    "Egypt",
    "El Salvador",
    "Equatorial Guinea",
    "Eritrea",
    "Estonia",
    "Ethiopia",
    "Falkland Islands",
    "Faroe Islands",
    "Fiji",
    "Finland",
    "France",
    "French Guiana",
    "French Polynesia",
    "French Southern Territories",
    "Gabon",
    "Gambia",
    "Georgia",
    "Germany",
    "Ghana",
    "Gibraltar",
    "Greece",
    "Greenland",
    "Grenada",
    "Guadeloupe",
    "Guam",
    "Guatemala",
    "Guernsey",
    "Guinea",
    "Guinea-Bissau",
    "Guyana",
    "Haiti",
    "Heard Island and McDonald Islands",
    "Honduras",
    "Hong Kong",
    "Hungary",
    "Iceland",
    "India",
    "Indonesia",
    "Iran",
    "Iraq",
    "Ireland",
    "Isle of Man",
    "Israel",
    "Italy",
    "Ivory Coast",
    "Jamaica",
    "Japan",
    "Jersey",
    "Jordan",
    "Kazakhstan",
    "Kenya",
    "Kiribati",
    "Kuwait",
    "Kyrgyzstan",
    "Laos",
    "Latvia",
    "Lebanon",
    "Lesotho",
    "Liberia",
    "Libya",
    "Liechtenstein",
    "Lithuania",
    "Luxembourg",
    "Macao S.A.R., China",
    "Macedonia",
    "Madagascar",
    "Malawi",
    "Malaysia",
    "Maldives",
    "Mali",
    "Malta",
    "Marshall Islands",
    "Martinique",
    "Mauritania",
    "Mauritius",
    "Mayotte",
    "Mexico",
    "Micronesia",
    "Moldova",
    "Monaco",
    "Mongolia",
    "Montenegro",
    "Montserrat",
    "Morocco",
    "Mozambique",
    "Myanmar",
    "Namibia",
    "Nauru",
    "Nepal",
    "Netherlands",
    "New Caledonia",
    "New Zealand",
    "Nicaragua",
    "Niger",
    "Nigeria",
    "Niue",
    "Norfolk Island",
    "North Korea",
    "Northern Mariana Islands",
    "Norway",
    "Oman",
    "Pakistan",
    "Palestinian Territory",
    "Panama",
    "Papua New Guinea",
    "Paraguay",
    "Peru",
    "Philippines",
    "Pitcairn",
    "Poland",
    "Portugal",
    "Puerto Rico",
    "Qatar",
    "Reunion",
    "Romania",
    "Russia",
    "Rwanda",
    "S�o Tom� and Pr�ncipe",
    "Saint Barth�lemy",
    "Saint Helena",
    "Saint Kitts and Nevis",
    "Saint Lucia",
    "Saint Martin (Dutch part)",
    "Saint Martin (French part)",
    "Saint Pierre and Miquelon",
    "Saint Vincent and the Grenadines",
    "Samoa",
    "San Marino",
    "Saudi Arabia",
    "Senegal",
    "Serbia",
    "Seychelles",
    "Sierra Leone",
    "Singapore",
    "Slovakia",
    "Slovenia",
    "Solomon Islands",
    "Somalia",
    "South Africa",
    "South Georgia/Sandwich Islands",
    "South Korea",
    "South Sudan",
    "Spain",
    "Sri Lanka",
    "Sudan",
    "Suriname",
    "Svalbard and Jan Mayen",
    "Swaziland",
    "Sweden",
    "Switzerland",
    "Syria",
    "Taiwan",
    "Tajikistan",
    "Tanzania",
    "Thailand",
    "Timor-Leste",
    "Togo",
    "Tokelau",
    "Tonga",
    "Trinidad and Tobago",
    "Tunisia",
    "Turkey",
    "Turkmenistan",
    "Turks and Caicos Islands",
    "Tuvalu",
    "Uganda",
    "Ukraine",
    "United Arab Emirates",
    "United Kingdom (UK)",
    "United States (US)",
    "United States (US) Minor Outlying Islands",
    "United States (US) Virgin Islands",
    "Uruguay",
    "Uzbekistan",
    "Vanuatu",
    "Vatican",
    "Venezuela",
    "Vietnam",
    "Wallis and Futuna",
    "Western Sahara",
    "Yemen",
    "Zambia",
    "Zimbabwe"
  );

  $countries_code = array(
    "AX",
    "AF",
    "AL",
    "DZ",
    "AS",
    "AD",
    "AO",
    "AI",
    "AQ",
    "AG",
    "AR",
    "AM",
    "AW",
    "AU",
    "AT",
    "AZ",
    "BS",
    "BH",
    "BD",
    "BB",
    "BY",
    "PW",
    "BE",
    "BZ",
    "BJ",
    "BM",
    "BT",
    "BO",
    "BQ",
    "BA",
    "BW",
    "BV",
    "BR",
    "IO",
    "VG",
    "BN",
    "BG",
    "BF",
    "BI",
    "KH",
    "CM",
    "CA",
    "CV",
    "KY",
    "CF",
    "TD",
    "CL",
    "CN",
    "CX",
    "CC",
    "CO",
    "KM",
    "CG",
    "CD",
    "CK",
    "CR",
		"CI",
    "HR",
    "CU",
    "CW",
    "CY",
    "CZ",
    "DK",
    "DJ",
    "DM",
    "DO",
    "EC",
    "EG",
    "SV",
    "GQ",
    "ER",
    "EE",
    "ET",
    "FK",
    "FO",
    "FJ",
    "FI",
    "FR",
    "GF",
    "PF",
    "TF",
    "GA",
    "GM",
    "GE",
    "DE",
    "GH",
    "GI",
    "GR",
    "GL",
    "GD",
    "GP",
    "GU",
    "GT",
    "GG",
    "GN",
    "GW",
    "GY",
    "HT",
    "HM",
    "HN",
    "HK",
    "HU",
    "IS",
    "IN",
    "ID",
    "IR",
    "IQ",
    "IE",
    "IM",
    "IL",
    "IT",
    "CI",
    "JM",
    "JP",
    "JE",
    "JO",
    "KZ",
    "KE",
    "KI",
    "KW",
    "KG",
    "LA",
    "LV",
    "LB",
    "LS",
    "LR",
    "LY",
    "LI",
    "LT",
    "LU",
    "MO",
    "MK",
    "MG",
    "MW",
    "MY",
    "MV",
    "ML",
    "MT",
    "MH",
    "MQ",
    "MR",
    "MU",
    "YT",
    "MX",
    "FM",
    "MD",
    "MC",
    "MN",
    "ME",
    "MS",
    "MA",
    "MZ",
    "MM",
    "NA",
    "NR",
    "NP",
    "NL",
    "NC",
    "NZ",
    "NI",
    "NE",
    "NG",
    "NU",
    "NF",
    "KP",
    "MP",
    "NO",
    "OM",
    "PK",
    "PS",
    "PA",
    "PG",
    "PY",
    "PE",
    "PH",
    "PN",
    "PL",
    "PT",
    "PR",
    "QA",
    "RE",
    "RO",
    "RU",
    "RW",
    "ST",
    "BL",
    "SH",
    "KN",
    "LC",
    "SX",
    "MF",
    "PM",
    "VC",
    "WS",
    "SM",
    "SA",
    "SN",
    "RS",
    "SC",
    "SL",
    "SG",
    "SK",
    "SI",
    "SB",
    "SO",
    "ZA",
    "GS",
    "KR",
    "SS",
    "ES",
    "LK",
    "SD",
    "SR",
    "SJ",
    "SZ",
    "SE",
    "CH",
    "SY",
    "TW",
    "TJ",
    "TZ",
    "TH",
    "TL",
    "TG",
    "TK",
    "TO",
    "TT",
    "TN",
    "TR",
    "TM",
    "TC",
    "TV",
    "UG",
    "UA",
    "AE",
    "GB",
    "US",
    "UM",
    "VI",
    "UY",
    "UZ",
    "VU",
    "VA",
    "VE",
    "VN",
    "WF",
    "EH",
    "YE",
    "ZM",
    "ZW"
  );

  return str_replace($countries_name, $countries_code, $string);
}

function delete_users_from_role($user_role){
  require_once(ABSPATH.'wp-admin/includes/user.php' );
  $args1 = array(
   'role' => $user_role,
   'orderby' => 'user_nicename',
   'order' => 'ASC'
  );

  $cscmp_user_role = get_users($args1);
  echo '<ul>';
  $count_empty_email = 0;
   foreach ($cscmp_user_role as $user) {
   echo '<li>' . $user->ID .'--'.$user->display_name.'['.$user->user_email . ']'.print_r($user->caps).'first name: '.$user->first_name.'last name: '.$user->last_name;
    $result = count($user->caps );

    $current_user_caps = $user->caps; 
    echo "result : $result <br />";
    if ($current_user_caps[$user_role] == 1) {
         echo "Got Irix";
         if($result <= 1){
            echo "<br /> only 1 role <br />";
            wp_delete_user( $user->ID );
            echo "<br /> user deleted";

            $userRoles = new WP_User( $user->ID );
            //$current_user_caps['customer'] = 1;
            $userRoles->add_cap( 'customer' );
            $userRoles->remove_cap( $user_role );  
            //echo "<br />should delete user <br />";

         }else{
            echo "should remove cyber user role";
            $current_user_caps[$user_role] = 0;
            $userRoles = new WP_User( $user->ID );
            $userRoles->remove_cap( $user_role );          
         }
         //$user->remove_cap( 'cybersec_user_role');
     } 

    print_r($user->caps);

     echo "</li>";
   }


  echo '</ul>';
   echo "empty : email : $count_empty_email";
  exit; 

}

function display_emails_for_role($user_role){
  require_once(ABSPATH.'wp-admin/includes/user.php' );
  $args1 = array(
   'role' => $user_role,
   'orderby' => 'email',
   'order' => 'ASC',
   'fields' => array('user_email')
  );

  $cscmp_user_role = get_users($args1);
  //echo "\r\n". '<!-- $cscmp_user_role is '.print_r($cscmp_user_role, true).' -->';
  
  foreach($cscmp_user_role as $current_user){
    echo "\r\n".$current_user->user_email;
  }
  
}
?>