<?php

/*

  Filename:      sponsor-template-videoSolutions.php
  Description:   will display the Video Solutions(page 1) part of the Sposonr Template for Naylor
  Author: 	      Jullie Quijano
  Change Log:
 * November 26, 2013 [Jullie]added code to display message if file is included
 * August 29, 2012 	[Jullie] Created the file                                                                                                                                                                                                              
 */

/**
 * @package WordPress
 * @subpackage Enterprise APF 
 * Template Name: Sponsor Template Banner Solutions v.6
 */
//error_reporting(E_ALL);
//echo '	lib ';
// Sitewide Variables
if (!defined('CHANNEL_SITE'))
    include get_template_directory_uri() . '/library/includes/config.php';
get_header();

//will determine if file is included
if ($_SERVER['REMOTE_ADDR'] == WORKERBEE_IP) {
    echo '<!-- START OF ' . __FILE__ . ' -->';
}
?>

<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/naylorsponsor.css" />

<div id="wb_ent_content" class="clearfix row-fluid">
    <div id="wb_ent_main" class="span12 clearfix" role="main" style="border: 0px solid black;">
        <div id="sectionNav">
            <ul>
                <li><a href="/naylor-banner-solutions" class="currentPage"><?= _e('Banner Solutions', 'enterprise') ?></a></li>
                <li><a href="/naylor-video-solutions" ><?= _e('Video Solutions', 'enterprise') ?></a></li>
                <li><a href="/naylor-video-production"><?= _e('Video Production', 'enterprise') ?></a></li>
                <li><a href="/sponsor-contact"><?= _e('Request Information', 'enterprise') ?></a></li>
            </ul>
        </div>

        <h1>Banner Sponsorship for <?php echo $wb_ent_options['clientname']; ?></h1>
        <div class="section" id="bannerSoln">
            <ul class="listUl">
                <li><strong><?= _e('High visibility', 'enterprise') ?></strong> <?= _e('and', 'enterprise') ?> <strong><?= _e('prime location', 'enterprise') ?></strong> <?= _e('during all Association video plays', 'enterprise') ?></li>
                <li><?= _e('Your', 'enterprise') ?> <strong><?= _e('branded', 'enterprise') ?></strong> <?= _e('and', 'enterprise') ?> <strong><?= _e('action oriented banner', 'enterprise') ?></strong> <?= _e('is featured every time the Association\'s videos are played', 'enterprise') ?></li>
                <li><?= _e('Videos keep the audience on the site longer, making your banner more visible with click-thru rates up to', 'enterprise') ?> <strong><?= _e('20x higher', 'enterprise') ?></strong> <?= _e('than regular web advertising', 'enterprise') ?></li>  
                <li><?= _e('Opportunity to change your banner and URL landing page monthly', 'enterprise') ?></li> 
            </ul>         
        </div>
        <div class="section" id="apfScreenshot">
            <img src="<?php echo get_template_directory_uri(); ?>/images/sponsorTempApfScreenShot.jpg" style="padding-top: 7px;" />
        </div>

        <br />
        <div class="section" style="">
            <a class="btn btn-primary" style="padding:20px 24px;text-decoration: none; font-size: 16px;" href="/naylor-video-solutions"><?= _e('Our Video Solutions', 'enterprise') ?></a>
            <a class="btn btn-primary" style="padding:20px 24px; text-decoration: none; font-size: 16px;" href="/naylor-video-production">
                <span><?= _e('Learn more about our Video Production Process', 'enterprise') ?></span></a>
            <a class="btn btn-primary" style="padding:20px 24px;text-decoration: none; font-size: 16px;" href="/sponsor-contact"><?= _e('Request More Information Here', 'enterprise') ?></a>
        </div>  
    </div>
</div>
<?php
//will determine if file is included
if ($_SERVER['REMOTE_ADDR'] == WORKERBEE_IP) {
    echo '<!-- END OF ' . __FILE__ . ' -->';
}

get_footer();
?>