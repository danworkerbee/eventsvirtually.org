<?php
/**
 * filename: subscribeMc.php
 * description: this will be the template used for the Mailchimp Subscribe Form
 * author: Jullie Quijano
 * date created: 2014-05-02
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: Subscribe Template Mailchimp Step 2
 */
global $wb_ent_options;
get_header();
if (have_posts()) : while (have_posts()) : the_post();
        ?>
<style type="text/css">
    /*
    #subscribe-mailchimp .error{
        background-color: #6b0505;
        color: #fff;
        font-weight: bold;
        margin: 0 0 1em;
        padding: 5px 10px;
        z-index: 1;
    }
    #subscribe-mailchimp .checked{
        background-color: #fff;
        color: #fff;
        font-weight: bold;
        margin: 0 0 1em;
        padding: 5px 10px;
        z-index: 1;
    }*/
    #subscribe-mailchimp .error{
        color: red;
        display: inline;
        font-size: small;
        border: 2px solid red;
    }
    
    #subscribe-mailchimp .checked{
       color: #fff;
       border: none;
    }
    #subscribe-mailchimp .span6 .error{
        
    }
    #subscribe-mailchimp .error{
        
    }
    span.required{
        color: #c60;
        font-size: 200%;
    }
    #subscribe-mailchimp .wb-lbl-inline{
        display: inline;
    }#subscribe-mailchimp .span6{
        margin: 0;
    }
    @media only screen and (max-width: 450px){
    #subscribe-mailchimp fieldset > div{
      margin: 30px 0 20px;
    }
    }
    
</style>
        <div id="wb_ent_content" class="clearfix row-fluid">
            <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
                <div id="viewing-tips">
                    <!--<h1><?php //the_title(); ?></h1>-->
                    <?php 
                    /*
                    if (trim($wb_ent_options['mailchimpform'])) {
                        echo html_entity_decode($wb_ent_options['mailchimpform'.$curlang]);
                    }*/
                    if(isset($_COOKIE['wb_sub_email'])) {
                       $email = $_COOKIE['wb_sub_email']; 
                      }else{
                          wp_redirect("subscribe");
                      }
                    ?>
                    <!-- PHP form with jQuery validation -->
                    <div id="error-div" class="alert" style="display:none;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <div class="error-message"></div>
                    </div>
                    <form id="subscribe-mailchimp" method="post">
                        <fieldset>
                            <h2>Please tell us a little bit about yourself so we can align our content with your interests</h2>
                            
                             <input type="hidden" id="email" name="email" value="<?php echo $email; ?>" required />
                            
                            <div class="mcgroup">
                            <label for="membership" class="wb-lbl-inline">Are you a member or non-member?<span class="required">*</span></label>
                               <select id="membership" name="membership" required>
                                 <option value="MEMBER">Member</option>
                                 <option value="NON-MEMBER">Non-member</option>                            
                               </select>
                            </div>
                          
                         <div class="mcgroupsec span6">
                          <label for="optionsRadios" class="parent">What is your industry sector? <span class="required">*</span></label>
                          <label class="radio">
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="Operator">
                            Operator
                          </label>
                          <label class="radio">
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="Manufacturer">
                            Manufacturer
                          </label>
                          <label class="radio">
                            <input type="radio" name="optionsRadios" id="optionsRadios3" value="Supplier">
                            Supplier
                          </label>
                          <label class="radio">
                            <input type="radio" name="optionsRadios" id="optionsRadios4" value="Distributor">
                            Distributor
                          </label>
                          <label class="radio">
                            <input type="radio" name="optionsRadios" id="optionsRadios5" value="Associate member">
                            Associate Member
                          </label>
                          <label class="radio">
                            <input type="radio" name="optionsRadios" id="optionsRadios6" value="Other"> 
                            Other <input type="text" id="other_ra_text" name="other_ra_text" />
                          </label>
                          <span id="error-sector"></span>
                          </div> <!-- end of radio buttons -->
                          
                          <div class="mcgroupint span6">
                              <label for="interestGroup" class="parent">What are your interests? <span class="required">*</span></label>
                              <label class="checkbox ">
                                <input type="checkbox" name="interestGroup[]" id="inlineCheckbox1" value="Culinary"> Culinary
                              </label>
                              <label class="checkbox ">
                                <input type="checkbox" name="interestGroup[]" id="inlineCheckbox2" value="Insights"> Insights
                              </label>
                              <label class="checkbox ">
                                <input type="checkbox" name="interestGroup[]" id="inlineCheckbox3" value="Marketing"> Marketing
                              </label>
                              <label class="checkbox ">
                                <input type="checkbox" name="interestGroup[]" id="inlineCheckbox4" value="Supply Chain / Sales"> Supply Chain / Sales
                              </label>
                              <label class="checkbox ">
                                <input type="checkbox" name="interestGroup[]" id="inlineCheckbox5" value="Leadership"> Leadership
                              </label>
                              <label class="checkbox ">
                                <input type="checkbox" name="interestGroup[]" id="inlineCheckbox6" value="Other"> Other <input id="other_ch_text" type="text" name="other_ch_text" />
                              </label>
                              
                              <span id="error-interests"></span>
                          </div>
                          <div class="mcgroup clear">
                              <label class="wb-lbl-inline parent">What is your job title?</label>
                              <input type="text" name="jobtitle" required /><span class="required">*</span>
                          </div>
                          <button type="submit" name="submit" class="btn">Subscribe</button>
                        </fieldset>
                    </form>
                    <br />
                     <script type='text/javascript' src='http://cdn.jsdelivr.net/jquery.validation/1.14.0/jquery.validate.min.js'></script>                    
                     
      <script type='text/javascript'>
          $().ready(function() {
               
             checkOtherRadio(); 
             checkOtherCheckbox();
          });
          function checkOtherRadio(){
              var radioOption = $("input:radio[name='optionsRadios']:checked").val();
               if(radioOption === 'Other' &&  $( "#other_ra_text").val() == ""){                  
                    $( "#other_ra_text").addClass("error required");                   
               }else if(radioOption === 'Other' &&  $( "#other_ra_text").val() != ""){
                   $( "#other_ra_text").removeClass("error");                   
               }else{
                   $( "#other_ra_text").removeClass("error required");                   
               }
               
          }
          function checkOtherCheckbox(){
                  var values = new Array();
                  $.each($("input[name='interestGroup[]']:checked"), function() {
                  values.push($(this).val());
        /*          
        if($(this).val() === "Other" && $('#other_ch_text').val() == ""){
                      $( "#other_ch_text").addClass("error required");
                  }else if($(this).val() === "Other" && $('#other_ch_text').val() != ""){
                      $( "#other_ch_text").removeClass("error required");
                  }*/
                  
                  // or you can do something to the actual checked checkboxes by working directly with  'this'
                  // something like $(this).hide() (only something useful, probably) :P
                });
                if($.inArray( "Other", values )  > -1){
                    if($('#other_ch_text').val() == ""){
                    $( "#other_ch_text").addClass("error required");
                    }else{
                    $( "#other_ch_text").removeClass("required");    
                    }
                }else{
                    $( "#other_ch_text").removeClass("error required");
//                    /alert(values);
                }
                
          }
          
        $( ":radio" ).click( checkOtherRadio );
        $( ":checkbox" ).click( checkOtherCheckbox );
        function valid_email_address(email)
		{
		    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
		    return pattern.test(email);
		}
          function checkOtherValues(){
        alert("submit");     
        alert($("input:radio[name='optionsRadios']:checked").val());
        var values = new Array();
        $.each($("input[name='interestGroup[]']:checked"), function() {
          values.push($(this).val());
          // or you can do something to the actual checked checkboxes by working directly with  'this'
          // something like $(this).hide() (only something useful, probably) :P
        });
        alert(values);
        //alert($("input:checkbox[name='interestGroup']:checked").val());
              
          }
          //$.validator.messages.required = "";
          $("#subscribe-mailchimp").validate({
            invalidHandler: function(event, validator) {
                        // 'this' refers to the form
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                         var message = errors == 1
                            ? '<i class="icon-remove-sign"></i> To proceed, please check the highlighted field.'
                            : '<i class="icon-remove-sign"></i> To proceed, please check the highlighted fields.';
                          $("div.error-message").html(message);
                          $("#error-div").addClass('alert-danger');                           
                          $("#error-div").show();
                        } else {
                          $("#error-div").hide();
                        }
                      },		
            rules: {				
                    optionsRadios: "required",
                    'interestGroup[]' : "required",
                    jobtitle: "required"
                    },
                        messages: {
                            optionsRadios: "Please select an industry sector",
                            'interestGroup[]' : "Please select at least one"
                        },
            // the errorPlacement has to take the table layout into account
            errorPlacement: function(error, element) {                    
                    if (element.is(":radio"))
                     //$(element).closest( "form" ).find( "label[for='optionsRadios']" ).append( error );
                     error.appendTo('#error-sector');         
                    else if (element.is(":checkbox"))
                      //$(element).closest( "form" ).find( "label[for='interestGroup']" ).append( error );                       
                          error.appendTo("#error-interests");
                    else {
                    //$(element).addClass('error');
                    }
        

            },                        
            // specifying a submitHandler prevents the default submit, good for the demo
            submitHandler: function() {
                    //alert("submitted!");
                    //checkOtherValues();
                    //form.submit();
                    if (!valid_email_address($("#email").val()))
		        {
		            $("div.error-message").html('Please make sure you enter a valid email address.');
		        }
		        else
		        {
		            
		            //$("div.error-message").html("<span style='color:green;'>Almost done, please check your email address to confirmation.</span>");
		            $.ajax({
		                url: '<?php echo get_template_directory_uri() ; ?>/checkSubscribe.php', 
		                data: $('#subscribe-mailchimp').serialize(),
		                type: 'POST',
		                success: function(data) {
		                    var subData = eval('(' + data + ')');
                                    if (subData.success) 
		                    {
		                        $("#email").val("");
                            $("#error-div").addClass('alert-success');
                            $("#error-div").removeClass('alert-danger');
                            $("#error-div").show();
                            $("#subscribe-mailchimp").hide();
		                        $("div.error-message").html('<span style="color:green;">Almost done. Please check your email and confirm your subscription.</span>');
		                        
		                    }
		                    else
		                    {	
                                       $("#error-div").removeClass('alert-success alert-danger');
                                       $("div.error-message").html(subData.message);  
                                       $("#error-div").show();
		                    }
		                }
		            });
		        }		 
		        return false;
            },
            // set this class to error-labels to indicate valid fields
            success: function(label) {
                    // set &nbsp; as text for IE
                    label.removeClass("error");
            },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass(errorClass).removeClass(validClass);
                    $(element.form).prev("label")
                      .addClass(errorClass);
                  },
                  unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass(errorClass).addClass(validClass);
                    $(element.form).prev("label")
                      .removeClass(errorClass);
                  }
                /*
            highlight: function(element, errorClass) {
                    $(element).parent().next().find("." + errorClass).removeClass("checked");
            }*/
        });
                
                    </script>
                    <?php the_content(); ?>


                <?php endwhile;
            endif;
            ?>
        </div>
    </div>
    
    
<?php /*<script type='text/javascript' src='http://cdn.jsdelivr.net/jquery.validation/1.14.0/jquery.validate.min.js'></script>*/ ?>

<?php
get_sidebar();
get_footer();
?>