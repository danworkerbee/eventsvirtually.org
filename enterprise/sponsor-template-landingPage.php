<?php
/*
  Filename:      sponsor-template-videoSolutions.php
  Description:   will display the Video Solutions(page 1) part of the Sposonr Template for Naylor
  Author: 	      Jullie Quijano
  Change Log:
  November 26, 2013 [Jullie]added code to display message if file is included
  August 29, 2012 	[Jullie] Created the file
 
 * @package WordPress
 * @subpackage Enterprise APF
 * Template Name: Sponsor Template Landing Page v.6
 */

//error_reporting(E_ALL);
//echo '	lib ';
// Sitewide Variables

if (!defined('CHANNEL_SITE'))
    include get_template_directory_uri() . '/library/includes/config.php';
get_header();
//will determine if file is included
if ($_SERVER['REMOTE_ADDR'] == WORKERBEE_IP) {
    echo '<!-- START OF ' . __FILE__ . ' -->';
}

if ($wb_ent_options['associations']['community'] != '')
    $community = $wb_ent_options['associations']['community'];
else{
    $community = 'quality';
}
?>


<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/naylorsponsor.css" />

<div id="wb_ent_content" class="clearfix row-fluid">
    <div id="wb_ent_main" class="span12 clearfix" role="main" style="border: 0px solid black;">
        <div class="row-fluid span9 " >
            <p><?php printf(__('%s was designed to allow our editors, sponsors, thought leaders and advertisers all have an engaging platform for reaching, educating and engaging our members and the greater %s community. If you are interested, please click on the tabs below to read and view a video on our various solutions that will enhance your communication, branding and content marketing capabilities.', 'enterprise'), $wb_ent_options['channelname'], $community); ?> </p>
        </div>



        <div class="span3">
            <style>
                img.centerImageLogo { display: block; padding-left: 0px; }
            </style>

            <div class="">
                <img class="centerImageLogo"  src="<?php echo $wb_ent_options['loginlogo']; ?>" title="<?php echo $wb_ent_options['loginlogo']; ?>'s LOGO" />
            </div>
        </div>

        <div class="clear" ></div>
        <div class="row-fluid">
            <p>
<?php printf(__('%s\'s Media Sales partner, Naylor LLC, is just a click away-schedule a call today to learn more about these new and exciting solutions.', 'enterprise'), $wb_ent_options['channelname']) ?>
            </p>

        </div>

        <div class="clear" ></div>

        <div class="span12">
            <div id="sectionNav" style="margin: 0 auto;" >
                <ul>
                <li><a href="/naylor-banner-solutions"><?= _e('Banner Solutions', 'enterprise') ?></a></li>
                <li><a href="/naylor-video-solutions" ><?= _e('Video Solutions', 'enterprise') ?></a></li>
                <li><a href="/naylor-video-production"><?= _e('Video Production', 'enterprise') ?></a></li>
                <li><a href="/sponsor-contact"><?= _e('Request Information', 'enterprise') ?></a></li>
                </ul>
            </div>

            <div class="section" id="videoContent" style="">
            </div>  

        </div>

    </div> <!-- end of #wb_ent_main -->  

</div> <!--end of content-->



<?php
//will determine if file is included

if ($_SERVER['REMOTE_ADDR'] == WORKERBEE_IP) {

    echo '<!-- END OF ' . __FILE__ . ' -->';
}


get_footer();
?>