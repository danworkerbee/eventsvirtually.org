<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;800;900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="/wp-content/themes/enterprise/css/icomoon.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link type="text/css" rel="stylesheet" href="/wp-content/themes/enterprise/css/font-awesome/fontawesome.min.css" type="text/css" />
<link type="text/css" rel="stylesheet" href="/wp-content/themes/enterprise/css/font-awesome/all.css" type="text/css" />
<link href="https://fonts.googleapis.com/css2?family=Oswald:wght@400;600;700&display=swap" rel="stylesheet">
<div id="hub-header-wrapper hub-header-wrapper=pm">
	<div id="wb-client-header-content">
		<div id="fms-head-left">
			<a href="/" title="Home" rel="home" class="navbar-brand">
                <img src="<?php echo "/wp-content/uploads/2020/09/pharmacists-manitoba-logo.png";?>" style="height: 60px">
			</a>
		</div>
		<div id="hub-head-right">
			<a href="/" rel="home">
				<div class="" id="back-to-sc-site-button" style="font-size: 9.5pt;"><i style="padding-right:10px;" class="fas fa-chevron-left"></i>Back to pharmacistsmb.ca</div>
			</a>
			<!-- <a href="/" rel="home">
				<div class="" id="back-to-main-site-button"><i style="padding-right:10px;" class="fas fa-chevron-left"></i>Back</div>
			</a> -->
		</div>
	</div>
</div>