<?php
/**
 * filename: category.php
 * description: this will be the template to be used to display categories
 * author: Jullie Quijano
 * date created: 2014-03-25
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: Category
 */
global $wb_ent_options, $privatePostsIds, $unlistedPostsIds, $moretext, $carousel_term_meta;
//get_header();
$carousel_term_meta = "";
if ( is_category()){
	$wb_cat_id 			= get_query_var('cat');
	$carousel_term_meta = get_term_meta( $wb_cat_id, "wb_carousel_image", true );
}
if ( $wb_cat_id == $wb_ent_options['videocats']['howto'] ){
	include("how-to-category.php");
}elseif( trim($carousel_term_meta) != "" ){
	include("carousel-category-template.php");
}else{
	include("default-category-template.php");
}