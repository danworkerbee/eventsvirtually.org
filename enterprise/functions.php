<?php
/**
 * filename: functions.php
 * description: this will include all functions needed for the APF
 * author: Jullie Quijano
 * date created: 2014-03-20
 */
/**
 * @package WordPress
 * @subpackage Enterprise
 */
/*
 * WP Bootstrap Functions
 */

// Get Bones Core Up & Running!
require_once('library/bones.php');            // core functions (don't remove)
require_once('library/plugins.php');          // plugins & extra functions (optional)
// Options panel
require_once('library/options-panel.php');

// Shortcodes
require_once('library/shortcodes.php');

$args = array(
   'sort_order' => 'ASC',
   'sort_column' => 'post_title',
   'hierarchical' => 1,
   'exclude' => '',
   'include' => '',
   'meta_key' => '',
   'meta_value' => '',
   'authors' => '',
   'child_of' => 0,
   'parent' => 0,
   'exclude_tree' => '',
   'number' => '',
   'offset' => 0,
   'post_type' => 'page',
   'post_status' => 'publish'
);         
$allPages = get_pages($args); 

$pageCounter = 0;
foreach($allPages as $currentPage){
    //echo '$currentPage is '.print_r($currentPage, true);
    $currentArgs = array(
        'sort_order' => 'ASC',
        'sort_column' => 'post_title',
        'hierarchical' => 1,
        'exclude' => '',
        'include' => '',
        'meta_key' => '',
        'meta_value' => '',
        'authors' => '',
        'child_of' => 0,
        'parent' => $currentPage->ID,
        'exclude_tree' => '',
        'number' => '',
        'offset' => 0,
        'post_type' => 'page',
        'post_status' => 'publish'
     );       
    //echo '$currentArgs is '.print_r($currentArgs, true);
    $allChildren = get_pages($currentArgs);

    //echo '$allChildren is '.print_r($allChildren, true);

    if( count($allChildren) > 0 ){
        $allPages[$pageCounter]->children = $allChildren;
    }            

    $pageCounter++;

}

/*
// Admin Functions (commented out by default)
// require_once('library/admin.php');         // custom admin functions
// Custom Backend Footer
add_filter('admin_footer_text', 'bones_custom_admin_footer');

function bones_custom_admin_footer() {
    echo '<span id="footer-thankyou">Developed by <a href="http://320press.com" target="_blank">320press</a></span>. Built using <a href="http://themble.com/bones" target="_blank">Bones</a>.';
}
 * 


// adding it to the admin area
add_filter('admin_footer_text', 'bones_custom_admin_footer');
 */

// Set content width
if (!isset($content_width))
    $content_width = 580;

/**
 * Creates a nicely formatted and more specific title element text
 * for output in head of document, based on current view.
 *
 * @param string $title Default title text for current view.
 * @param string $sep   Optional separator.
 *
 * @return string Filtered title.
 *
 * @note may be called from http://example.com/wp-activate.php?key=xxx where the plugins are not loaded.
 */
function bones_filter_title($title, $sep) {
    global $paged, $page;

    if (is_feed()) {
        return $title;
    }

    // Add the site name.
    $title .= get_bloginfo('name');

    // Add the site description for the home/front page.
    $site_description = get_bloginfo('description', 'display');
    if ($site_description && ( is_home() || is_front_page() )) {
        $title = "$title $sep $site_description";
    }

    // Add a page number if necessary.
    if ($paged >= 2 || $page >= 2) {
        $title = "$title $sep " . sprintf(__('Page %s', 'bonestheme'), max($paged, $page));
    }

    return $title;
}

add_filter('wp_title', 'bones_filter_title', 10, 2);

add_action('after_theme_setup', 'enterprise_theme_setup');
function enterprise_theme_setup(){
    load_theme_textdomain('enterprise', get_template_directory().'/languages');
}

/* * *********** THUMBNAIL SIZE OPTIONS ************ */

// Thumbnail sizes
add_image_size('wpbs-featured', 638, 300, true);
add_image_size('wpbs-featured-home', 970, 311, true);
add_image_size('wpbs-featured-carousel', 970, 400, true);

/*
  to add more sizes, simply copy a line from above
  and change the dimensions & name. As long as you
  upload a "featured image" as large as the biggest
  set width or height, all the other sizes will be
  auto-cropped.

  To call a different size, simply change the text
  inside the thumbnail function.

  For example, to call the 300 x 300 sized image,
  we would use the function:
  <?php the_post_thumbnail( 'bones-thumb-300' ); ?>
  for the 600 x 100 image:
  <?php the_post_thumbnail( 'bones-thumb-600' ); ?>

  You can change the names and dimensions to whatever
  you like. Enjoy!
 */

/* * *********** ACTIVE SIDEBARS ******************* */

// Sidebars & Widgetizes Areas
if (!function_exists('bones_register_sidebars')) {

    function bones_register_sidebars() {
        register_sidebar(array(
           'id' => 'wb_ent_sidebar1',
           'name' => 'Main Sidebar',
           'description' => 'Used on every page BUT the homepage page template.',
           'before_widget' => '<div id="%1$s" class="widget %2$s">',
           'after_widget' => '</div>',
           'before_title' => '<h4 class="widgettitle">',
           'after_title' => '</h4>',
        ));

        register_sidebar(array(
           'id' => 'sidebar2',
           'name' => 'Homepage Sidebar',
           'description' => 'Used only on the homepage page template.',
           'before_widget' => '<div id="%1$s" class="widget %2$s">',
           'after_widget' => '</div>',
           'before_title' => '<h4 class="widgettitle">',
           'after_title' => '</h4>',
        ));

        register_sidebar(array(
           'id' => 'footer1',
           'name' => 'Footer 1',
           'before_widget' => '<div id="%1$s" class="widget span4 %2$s">',
           'after_widget' => '</div>',
           'before_title' => '<h4 class="widgettitle">',
           'after_title' => '</h4>',
        ));

        register_sidebar(array(
           'id' => 'footer2',
           'name' => 'Footer 2',
           'before_widget' => '<div id="%1$s" class="widget span4 %2$s">',
           'after_widget' => '</div>',
           'before_title' => '<h4 class="widgettitle">',
           'after_title' => '</h4>',
        ));

        register_sidebar(array(
           'id' => 'footer3',
           'name' => 'Footer 3',
           'before_widget' => '<div id="%1$s" class="widget span4 %2$s">',
           'after_widget' => '</div>',
           'before_title' => '<h4 class="widgettitle">',
           'after_title' => '</h4>',
        ));


        /*
          to add more sidebars or widgetized areas, just copy
          and edit the above sidebar code. In order to call
          your new sidebar just use the following code:

          Just change the name to whatever your new
          sidebar's id is, for example:

          To call the sidebar in your template, you can just copy
          the sidebar.php file and rename it to your sidebar's name.
          So using the above example, it would be:
          sidebar-sidebar2.php

         */
    }

} // don't remove this bracket!

/* * *********** COMMENT LAYOUT ******************** */

// Comment Layout
function bones_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    ?>
    <li <?php comment_class(); ?>>
        <article id="comment-<?php comment_ID(); ?>" class="clearfix">
            <div class="comment-author vcard row-fluid clearfix">
                <div class="avatar span3">
                    <?php echo get_avatar($comment, $size = '75'); ?>
                </div>
                <div class="span9 comment-text">
                    <?php printf('<h4>%s</h4>', get_comment_author_link()) ?>
                    <?php edit_comment_link(__('Edit', 'bonestheme'), '<span class="edit-comment btn btn-small btn-info"><i class="icon-white icon-pencil"></i>', '</span>') ?>

                    <?php if ($comment->comment_approved == '0') : ?>
                        <div class="alert-message success">
                            <p><?php _e('Your comment is awaiting moderation.', 'bonestheme') ?></p>
                        </div>
                    <?php endif; ?>

                    <?php comment_text() ?>

                    <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>"><?php comment_time('F jS, Y'); ?> </a></time>

                    <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
            </div>
        </article>
        <!-- </li> is added by wordpress automatically -->
        <?php
    }

// don't remove this bracket!
// Display trackbacks/pings callback function
    function list_pings($comment, $args, $depth) {
        $GLOBALS['comment'] = $comment;
        ?>
    <li id="comment-<?php comment_ID(); ?>"><i class="icon icon-share-alt"></i>&nbsp;<?php comment_author_link(); ?>
        <?php
    }

// Only display comments in comment count (which isn't currently displayed in wp-bootstrap, but i'm putting this in now so i don't forget to later)
    add_filter('get_comments_number', 'comment_count', 0);

    function comment_count($count) {
        if (!is_admin()) {
            global $id;
            $comments_by_type = separate_comments(get_comments('status=approve&post_id=' . $id));
            return count($comments_by_type['comment']);
        } else {
            return $count;
        }
    }

    /*     * *********** SEARCH FORM LAYOUT **************** */

// Search Form
    function bones_wpsearch($form) {
        $form = '<form role="search" method="get" id="searchform" action="' . home_url('/') . '" >
  <label class="screen-reader-text" for="s">' . __('Search for:', 'bonestheme') . '</label>
  <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Search the Site..." />
  <input type="submit" id="searchsubmit" value="' . esc_attr__('Search', 'bonestheme') . '" />
  </form>';
        return $form;
    }

// don't remove this bracket!

    /*     * **************** password protected post form **** */

    add_filter('the_password_form', 'custom_password_form');

    function custom_password_form() {
        global $post;
        $label = 'pwbox-' . ( empty($post->ID) ? rand() : $post->ID );
        $o = '<div class="clearfix"><form class="protected-post-form" action="' . get_option('siteurl') . '/wp-login.php?action=postpass" method="post">
   ' . '<p>' . __("This post is password protected. To view it please enter your password below:", 'bonestheme') . '</p>' . '
   <label for="' . $label . '">' . __("Password:", 'bonestheme') . ' </label><div class="input-append"><input name="post_password" id="' . $label . '" type="password" size="20" /><input type="submit" name="Submit" class="btn btn-primary" value="' . esc_attr__("Submit", 'bonestheme') . '" /></div>
   </form></div>
   ';
        return $o;
    }

    /*     * ********* update standard wp tag cloud widget so it looks better *********** */

    add_filter('widget_tag_cloud_args', 'my_widget_tag_cloud_args');

    function my_widget_tag_cloud_args($args) {
        $args['number'] = 20; // show less tags
        $args['largest'] = 9.75; // make largest and smallest the same - i don't like the varying font-size look
        $args['smallest'] = 9.75;
        $args['unit'] = 'px';
        return $args;
    }

// filter tag cloud output so that it can be styled by CSS
    function add_tag_class($taglinks) {
        $tags = explode('</a>', $taglinks);
        $regex = "#(.*tag-link[-])(.*)(' title.*)#e";
        $term_slug = "(get_tag($2) ? get_tag($2)->slug : get_category($2)->slug)";

        foreach ($tags as $tag) {
            $tagn[] = preg_replace($regex, "('$1$2 label tag-'.$term_slug.'$3')", $tag);
        }

        $taglinks = implode('</a>', $tagn);

        return $taglinks;
    }

    add_action('wp_tag_cloud', 'add_tag_class');

    add_filter('wp_tag_cloud', 'wp_tag_cloud_filter', 10, 2);

    function wp_tag_cloud_filter($return, $args) {
        return '<div id="tag-cloud">' . $return . '</div>';
    }

// Enable shortcodes in widgets
    add_filter('widget_text', 'do_shortcode');

// Disable jump in 'read more' link
    function remove_more_jump_link($link) {
        $offset = strpos($link, '#more-');
        if ($offset) {
            $end = strpos($link, '"', $offset);
        }
        if ($end) {
            $link = substr_replace($link, '', $offset, $end - $offset);
        }
        return $link;
    }

    add_filter('the_content_more_link', 'remove_more_jump_link');

// Remove height/width attributes on images so they can be responsive
    add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10);
    add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10);

    function remove_thumbnail_dimensions($html) {
        $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
        return $html;
    }

// Add the Meta Box to the homepage template
    function add_homepage_meta_box() {
        global $post;

        // Only add homepage meta box if template being used is the homepage template
        // $post_id = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post_ID']) ? $_POST['post_ID'] : "");
        $post_id = $post->ID;
        $template_file = get_post_meta($post_id, '_wp_page_template', TRUE);

        if ($template_file == 'page-homepage.php') {
            add_meta_box(
               'homepage_meta_box', // $id  
               'Optional Homepage Tagline', // $title  
               'show_homepage_meta_box', // $callback  
               'page', // $page  
               'normal', // $context  
               'high'); // $priority  
        }
    }

    add_action('add_meta_boxes', 'add_homepage_meta_box');

// Field Array  
    $prefix = 'custom_';
    $custom_meta_fields = array(
       array(
          'label' => 'Homepage tagline area',
          'desc' => 'Displayed underneath page title. Only used on homepage template. HTML can be used.',
          'id' => $prefix . 'tagline',
          'type' => 'textarea'
       )
    );

// The Homepage Meta Box Callback  
    function show_homepage_meta_box() {
        global $custom_meta_fields, $post;

        // Use nonce for verification
        wp_nonce_field(basename(__FILE__), 'wpbs_nonce');

        // Begin the field table and loop
        echo '<table class="form-table">';

        foreach ($custom_meta_fields as $field) {
            // get value of this field if it exists for this post  
            $meta = get_post_meta($post->ID, $field['id'], true);
            // begin a table row with  
            echo '<tr> 
              <th><label for="' . $field['id'] . '">' . $field['label'] . '</label></th> 
              <td>';
            switch ($field['type']) {
                // text  
                case 'text':
                    echo '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="60" /> 
                          <br /><span class="description">' . $field['desc'] . '</span>';
                    break;

                // textarea  
                case 'textarea':
                    echo '<textarea name="' . $field['id'] . '" id="' . $field['id'] . '" cols="80" rows="4">' . $meta . '</textarea> 
                          <br /><span class="description">' . $field['desc'] . '</span>';
                    break;
            } //end switch  
            echo '</td></tr>';
        } // end foreach  
        echo '</table>'; // end table  
    }

// Save the Data  
    function save_homepage_meta($post_id) {

        global $custom_meta_fields;

        // verify nonce  
        if (!isset($_POST['wpbs_nonce']) || !wp_verify_nonce($_POST['wpbs_nonce'], basename(__FILE__)))
            return $post_id;

        // check autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return $post_id;

        // check permissions
        if ('page' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id))
                return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
        }

        // loop through fields and save the data  
        foreach ($custom_meta_fields as $field) {
            $old = get_post_meta($post_id, $field['id'], true);
            $new = $_POST[$field['id']];

            if ($new && $new != $old) {
                update_post_meta($post_id, $field['id'], $new);
            } elseif ('' == $new && $old) {
                delete_post_meta($post_id, $field['id'], $old);
            }
        } // end foreach
    }

    add_action('save_post', 'save_homepage_meta');

// Add thumbnail class to thumbnail links
    function add_class_attachment_link($html) {
        $postid = get_the_ID();
        $html = str_replace('<a', '<a class="thumbnail"', $html);
        return $html;
    }

    add_filter('wp_get_attachment_link', 'add_class_attachment_link', 10, 1);

// Add lead class to first paragraph
    function first_paragraph($content) {
        global $post;

        // if we're on the homepage, don't add the lead class to the first paragraph of text
        if (is_page_template('page-homepage.php'))
            return $content;
        else
            return preg_replace('/<p([^>]+)?>/', '<p$1 class="lead">', $content, 1);
    }

    add_filter('the_content', 'first_paragraph');

// Menu output mods
    /* Bootstrap_Walker for Wordpress 
     * Author: George Huger, Illuminati Karate, Inc 
     * More Info: http://illuminatikarate.com/blog/bootstrap-walker-for-wordpress 
     * 
     * Formats a Wordpress menu to be used as a Bootstrap dropdown menu (http://getbootstrap.com). 
     * 
     * Specifically, it makes these changes to the normal Wordpress menu output to support Bootstrap: 
     * 
     *        - adds a 'dropdown' class to level-0 <li>'s which contain a dropdown 
     *         - adds a 'dropdown-submenu' class to level-1 <li>'s which contain a dropdown 
     *         - adds the 'dropdown-menu' class to level-1 and level-2 <ul>'s 
     * 
     * Supports menus up to 3 levels deep. 
     *  
     */
    class Bootstrap_Walker extends Walker_Nav_Menu {
        /* Start of the <ul> 
         * 
         * Note on $depth: Counterintuitively, $depth here means the "depth right before we start this menu".  
         *                   So basically add one to what you'd expect it to be 
         */

        function start_lvl(&$output, $depth = 0, $args = array()) {
            $tabs = str_repeat("\t", $depth);
            // If we are about to start the first submenu, we need to give it a dropdown-menu class 
            if ($depth == 0 || $depth == 1) { //really, level-1 or level-2, because $depth is misleading here (see note above) 
                $output .= "\n{$tabs}<ul class=\"dropdown-menu\">\n";
            } else {
                $output .= "\n{$tabs}<ul>\n";
            }
            return;
        }

        /* End of the <ul> 
         * 
         * Note on $depth: Counterintuitively, $depth here means the "depth right before we start this menu".  
         *                   So basically add one to what you'd expect it to be 
         */

        function end_lvl(&$output, $depth = 0, $args = array()) {
            if ($depth == 0) { // This is actually the end of the level-1 submenu ($depth is misleading here too!) 
                // we don't have anything special for Bootstrap, so we'll just leave an HTML comment for now 
                $output .= '<!--.dropdown-->';
            }
            $tabs = str_repeat("\t", $depth);
            $output .= "\n{$tabs}</ul>\n";
            return;
        }

        /* Output the <li> and the containing <a> 
         * Note: $depth is "correct" at this level 
         */

        function start_el(&$output, $item, $depth = 0, $args = array(), $current_object_id = 0) {
            global $wp_query;
            $indent = ( $depth ) ? str_repeat("\t", $depth) : '';
            $class_names = $value = '';
            $classes = empty($item->classes) ? array() : (array) $item->classes;

            /* If this item has a dropdown menu, add the 'dropdown' class for Bootstrap */
            if ($item->hasChildren) {
                $classes[] = 'dropdown';
                // level-1 menus also need the 'dropdown-submenu' class 
                if ($depth == 1) {
                    $classes[] = 'dropdown-submenu';
                }
            }

            /* This is the stock Wordpress code that builds the <li> with all of its attributes */
            $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item));
            $class_names = ' class="' . esc_attr($class_names) . '"';
            $output .= $indent . '<li id="menu-item-' . $item->ID . '"' . $value . $class_names . '>';
            $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
            $attributes .=!empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
            $attributes .=!empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
            $attributes .=!empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';
            $item_output = $args->before;

            /* If this item has a dropdown menu, make clicking on this link toggle it */
            if ($item->hasChildren && $depth == 0) {
                $item_output .= '<a' . $attributes . ' class="dropdown-toggle" data-toggle="dropdown">';
            } else {
                $item_output .= '<a' . $attributes . '>';
            }

            $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;

            /* Output the actual caret for the user to click on to toggle the menu */
            if ($item->hasChildren && $depth == 0) {
                $item_output .= '<b class="caret"></b></a>';
            } else {
                $item_output .= '</a>';
            }

            $item_output .= $args->after;
            $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
            return;
        }

        /* Close the <li> 
         * Note: the <a> is already closed 
         * Note 2: $depth is "correct" at this level 
         */

        function end_el(&$output, $item, $depth = 0, $args = array()) {
            $output .= '</li>';
            return;
        }

        /* Add a 'hasChildren' property to the item 
         * Code from: http://wordpress.org/support/topic/how-do-i-know-if-a-menu-item-has-children-or-is-a-leaf#post-3139633  
         */

        function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
            // check whether this item has children, and set $item->hasChildren accordingly 
            $element->hasChildren = isset($children_elements[$element->ID]) && !empty($children_elements[$element->ID]);

            // continue with normal behavior 
            return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
        }

    }

    add_editor_style('editor-style.css');

// Add Twitter Bootstrap's standard 'active' class name to the active nav link item
    add_filter('nav_menu_css_class', 'add_active_class', 10, 2);

    function add_active_class($classes, $item) {
        if ($item->menu_item_parent == 0 && in_array('current-menu-item', $classes)) {
            $classes[] = "active";
        }

        return $classes;
    }

// enqueue styles
    if (!function_exists("theme_styles")) {

        function theme_styles() {
            // This is the compiled css file from LESS - this means you compile the LESS file locally and put it in the appropriate directory if you want to make any changes to the master bootstrap.css.
            wp_register_style('bootstrap', get_template_directory_uri() . '/library/css/bootstrap.css', array(), '1.0', 'all');
            wp_register_style('bootstrap-responsive', get_template_directory_uri() . '/library/css/responsive.css', array('bootstrap'), '1.0', 'all');
            wp_register_style('wp-bootstrap', get_stylesheet_uri(), array('bootstrap-responsive'), '1.0', 'all');

            wp_enqueue_style('bootstrap');
            wp_enqueue_style('bootstrap-responsive');
            wp_enqueue_style('wp-bootstrap');
        }

    }
    add_action('wp_enqueue_scripts', 'theme_styles');

// enqueue javascript
    if (!function_exists("theme_js")) {

        function theme_js() {

            wp_register_script('bootstrap', get_template_directory_uri() . '/library/js/bootstrap.min.js', array('jquery'), '1.2');

            wp_register_script('wpbs-scripts', get_template_directory_uri() . '/library/js/scripts.js', array('jquery'), '1.2');

            wp_register_script('modernizr', get_template_directory_uri() . '/library/js/modernizr.full.min.js', array('jquery'), '1.2');

            wp_enqueue_script('bootstrap');
            wp_enqueue_script('wpbs-scripts');
            wp_enqueue_script('modernizr');
        }

    }
    add_action('wp_enqueue_scripts', 'theme_js');

// Get theme options
    function get_wpbs_theme_options() {
        $theme_options_styles = '';

        $heading_typography = of_get_option('heading_typography');
        if ($heading_typography['face'] != 'Default') {
            $theme_options_styles .= '
        h1, h2, h3, h4, h5, h6{ 
          font-family: ' . $heading_typography['face'] . '; 
          font-weight: ' . $heading_typography['style'] . '; 
          color: ' . $heading_typography['color'] . '; 
        }';
        }

        $main_body_typography = of_get_option('main_body_typography');
        if ($main_body_typography['face'] != 'Default') {
            $theme_options_styles .= '
        body{ 
          font-family: ' . $main_body_typography['face'] . '; 
          font-weight: ' . $main_body_typography['style'] . '; 
          color: ' . $main_body_typography['color'] . '; 
        }';
        }

        $link_color = of_get_option('link_color');
        if ($link_color) {
            $theme_options_styles .= '
        a{ 
          color: ' . $link_color . '; 
        }';
        }

        $link_hover_color = of_get_option('link_hover_color');
        if ($link_hover_color) {
            $theme_options_styles .= '
        a:hover{ 
          color: ' . $link_hover_color . '; 
        }';
        }

        $link_active_color = of_get_option('link_active_color');
        if ($link_active_color) {
            $theme_options_styles .= '
        a:active{ 
          color: ' . $link_active_color . '; 
        }';
        }

        $topbar_position = of_get_option('nav_position');
        if ($topbar_position == 'scroll') {
            $theme_options_styles .= '
        .navbar{ 
          position: static; 
        }
        body{
          padding-top: 0;
        }
        #content {
          padding-top: 27px;
        }
        '
            ;
        }

        $topbar_bg_color = of_get_option('top_nav_bg_color');
        $use_gradient = of_get_option('showhidden_gradient');

        if ($topbar_bg_color && !$use_gradient) {
            $theme_options_styles .= '
        .navbar-inner, .navbar .fill { 
          background-color: ' . $topbar_bg_color . ';
          background-image: none;
        }';
        }

        if ($use_gradient) {
            $topbar_bottom_gradient_color = of_get_option('top_nav_bottom_gradient_color');

            $theme_options_styles .= '
        .navbar-inner, .navbar .fill {
          background-image: -khtml-gradient(linear, left top, left bottom, from(' . $topbar_bg_color . '), to(' . $topbar_bottom_gradient_color . '));
          background-image: -moz-linear-gradient(top, ' . $topbar_bg_color . ', ' . $topbar_bottom_gradient_color . ');
          background-image: -ms-linear-gradient(top, ' . $topbar_bg_color . ', ' . $topbar_bottom_gradient_color . ');
          background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, ' . $topbar_bg_color . '), color-stop(100%, ' . $topbar_bottom_gradient_color . '));
          background-image: -webkit-linear-gradient(top, ' . $topbar_bg_color . ', ' . $topbar_bottom_gradient_color . '2);
          background-image: -o-linear-gradient(top, ' . $topbar_bg_color . ', ' . $topbar_bottom_gradient_color . ');
          background-image: linear-gradient(top, ' . $topbar_bg_color . ', ' . $topbar_bottom_gradient_color . ');
          filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'' . $topbar_bg_color . '\', endColorstr=\'' . $topbar_bottom_gradient_color . '2\', GradientType=0);
        }';
        } else {
            
        }

        $topbar_link_color = of_get_option('top_nav_link_color');
        if ($topbar_link_color) {
            $theme_options_styles .= '
        .navbar .nav li a { 
          color: ' . $topbar_link_color . ';
        }';
        }

        $topbar_link_hover_color = of_get_option('top_nav_link_hover_color');
        if ($topbar_link_hover_color) {
            $theme_options_styles .= '
        .navbar .nav li a:hover { 
          color: ' . $topbar_link_hover_color . ';
        }';
        }

        $topbar_dropdown_hover_bg_color = of_get_option('top_nav_dropdown_hover_bg');
        if ($topbar_dropdown_hover_bg_color) {
            $theme_options_styles .= '
          .dropdown-menu li > a:hover, .dropdown-menu .active > a, .dropdown-menu .active > a:hover {
            background-color: ' . $topbar_dropdown_hover_bg_color . ';
          }
        ';
        }

        $topbar_dropdown_item_color = of_get_option('top_nav_dropdown_item');
        if ($topbar_dropdown_item_color) {
            $theme_options_styles .= '
          .dropdown-menu a{
            color: ' . $topbar_dropdown_item_color . ' !important;
          }
        ';
        }

        $hero_unit_bg_color = of_get_option('hero_unit_bg_color');
        if ($hero_unit_bg_color) {
            $theme_options_styles .= '
        .hero-unit { 
          background-color: ' . $hero_unit_bg_color . ';
        }';
        }

        $suppress_comments_message = of_get_option('suppress_comments_message');
        if ($suppress_comments_message) {
            $theme_options_styles .= '
        #main article {
          border-bottom: none;
        }';
        }

        $additional_css = of_get_option('wpbs_css');
        if ($additional_css) {
            $theme_options_styles .= $additional_css;
        }

        if ($theme_options_styles) {
            echo '<style>'
            . $theme_options_styles . '
        </style>';
        }

        $bootstrap_theme = of_get_option('wpbs_theme');
        $use_theme = of_get_option('showhidden_themes');

        if ($bootstrap_theme && $use_theme) {
            if ($bootstrap_theme == 'default') {
                
            } else {
                echo '<link rel="stylesheet" href="' . get_template_directory_uri() . '/admin/themes/' . $bootstrap_theme . '.css">';
            }
        }
    }

// end get_wpbs_theme_options function


    /*
     * WBTV Functions
     */

    global $wb_ent_options, $wpdb, $current_lang;
    $wb_ent_options = get_option('wb_ent_options');
    
    $passwordProtectedCondition = " AND p.post_password   = '' ";
    if( $wb_ent_options['poststodisplay']['passwordprotected'] ){
        $passwordProtectedCondition = '';
    }
   
    $current_lang = get_locale();
    switch($current_lang){
        case 'en_US' :
        $catlibrary =  $wb_ent_options['videocats']['library']; // English Library
        $curlang = ''; // used to select the library EN when en_US
        break;
        case 'fr_FR' :
        $catlibrary = $wb_ent_options['videocats']['library-fr_FR']; // French Library
        $curlang = '-'.$current_lang; // used to select the library FR when fr_FR
        $moretext = 'Plus'; // for the desctiption [plus]
        $lesstext = 'Moins';
        break; 
        }
        
    //get unlisted posts
    $unlistedCat = $wb_ent_options['videocats']['unlisted'];  
    $unlistedPosts = $wpdb->get_results($wpdb->prepare(
          "SELECT p.ID, p.post_title, p.post_name, p.post_content, p.guid
        FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
        WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
        AND tt.term_id=t.term_id
        AND tr.object_id=p.ID
        AND p.post_status='publish'
        AND p.post_type='post'
        AND t.term_id='%s'
        GROUP BY p.ID
        ORDER BY p.post_date DESC", $unlistedCat
       ), ARRAY_A);

    $unlistedPostsIds = array(); //will contain the IDs of the bundles
    foreach($unlistedPosts as $currentPost){
        $unlistedPostsIds[] = $currentPost['ID'];
    }    

    //get private posts
    $privateCat = $wb_ent_options['videocats']['private'];  
    $privatePosts = $wpdb->get_results($wpdb->prepare(
          "SELECT p.ID, p.post_title, p.post_name, p.post_content, p.guid
        FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
        WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
        AND tt.term_id=t.term_id
        AND tr.object_id=p.ID
        AND p.post_status='publish'
        AND p.post_type='post'
        AND t.term_id='%s'
        GROUP BY p.ID
        ORDER BY p.post_date DESC", $privateCat
       ), ARRAY_A);

    $privatePostsIds = array(); //will contain the IDs of the bundles
    foreach($privatePosts as $currentPost){
        $privatePostsIds[] = $currentPost['ID'];
    }   
  
    /*
     * $string -- (string) whatever the string is; required
     * $hasHTML -- (true/false) will indicate if we'll allow html
     * $listToCsv -- will transform <li> to comma separated values
     * $maxChar -- (int) maximum number of characters; set to -1 to have no limit
     * $end -- (string) what will be added to the end of the truncated string
     */
	if (!function_exists('wb_format_string')) {
    function wb_format_string($string, $hasHTML = false, $listToCsv = false, $maxChar = -1, $end = '... ') {

        //list to csv
        if ($listToCsv && preg_match("/<[^<]+>/", $string, $m) != 0) {
            $string = preg_replace("'</li[^>]*?>.*?<li>'si", ", ", $string);
            //$string = strip_tags($string, "<a><img>"); //nyssa 
            $string = strip_tags($string, "<a><sup><i>");
        }

        //strip html first
        if (!$hasHTML) {
            //$string = strip_tags($string,"<img>");//nyssa
            $string = strip_tags($string, "<sup>");
        }

        //truncate
        if ($maxChar > 0) {
            $encoding = 'UTF-8';
            $string = trim($string);

            if ((mb_strlen($string, $encoding)) <= $maxChar) {
                return $string;
            }

            $cutPos = $maxChar;
            $boundaryPos = mb_strrpos(mb_substr($string, 0, mb_strpos($string, ' ', $cutPos)), ' ');
            return mb_substr($string, 0, $boundaryPos === false ? $cutPos : $boundaryPos) . $end;
            //return mb_substr($string, 0, $cutPos) . $end;
        }

        return $string;
    }
  }

    if (!function_exists('more_less')) {
        

        function more_less($desc_id, $key_id, $desc_lines = '3', $key_lines = '1', $color = '#999', $font_size = '10px') {
           
             global $current_lang;
         $moreText =  "more";
        $lessText = "less";
        if($current_lang == "fr_FR"){
           $moreText = "Plus";
           $lessText = "Moins"; 
        }
            echo '
    <script type="text/javascript">
    descriptionMoreDisplayed = false;
    $(window).load(function(){
       if( ! descriptionMoreDisplayed ){
          $("#' . $desc_id . '").jTruncate({  
            length: 200,  
            minTrail: 0,  
            moreText: "['.$moreText.']",  
            lessText: "['.$lessText.']",  
            ellipsisText: "... "
          });       
          descriptionMoreDisplayed = true;    
       }
    });
    </script>';
        }

    }

    /*
     * will get the most recent post for given category $categoryId 
     */

	if (!function_exists('wb_get_most_recent')) {
		function wb_get_most_recent($categoryId = 0) {
			global $wb_ent_options, $wpdb, $passwordProtectedCondition;
			if ($categoryId != 0) {
				if ($wb_ent_options['vidshortcode']['enabled']) {
					$result = $wpdb->get_results($wpdb->prepare(
							"SELECT p.ID
							FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
							WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
							AND tt.term_id=t.term_id
							AND tr.object_id=p.ID
							AND p.post_status='publish'
							$passwordProtectedCondition
							AND p.post_type IN ('post', 'wb_audio')
							AND t.term_id IN(%s)
							GROUP BY p.ID
							ORDER BY p.post_date DESC LIMIT 1", $categoryId
							));
				} else {
					$result = $wpdb->get_results($wpdb->prepare(
							"SELECT p.ID
							FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media b
							WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
							AND tt.term_id=t.term_id
							AND tr.object_id=p.ID
							AND p.ID=b.post_id
							$passwordProtectedCondition
							AND p.post_status='publish'
							AND p.post_type IN ('post', 'wb_audio')
							AND t.term_id IN(%s)
							GROUP BY p.ID
							ORDER BY p.post_date DESC LIMIT 1", $categoryId
							));
				}
				
				$num_rows = $wpdb->num_rows;
				
				if ($num_rows > 0) {
					return $result[0]->ID;
				} else {
					return false;
				}
			}
		}
	}

	if (!function_exists('wb_get_post_details')) {
            function wb_get_post_details($videoPostId = 0) {
    		if ($videoPostId != 0) {
                    global $wb_ent_options, $wpdb;
                        //echo 'debug: $videoPostId is '.$videoPostId;
    			//echo 'debug: $wb_ent_options is '.print_r($wb_ent_options, true);
    			
    			if ($wb_ent_options['vidshortcode']['enabled']) {
                            //echo 'debug: videoshortcode is '.$wb_ent_options['vidshortcode']['enabled'];
                            if (isset($_GET['preview']) && $_GET['preview'] == "true") {
    					$result = $wpdb->get_results($wpdb->prepare(
    							"SELECT p.post_title, p.post_name, p.post_content
							FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
							WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
							AND tt.term_id=t.term_id
							AND tr.object_id=p.ID
							AND (p.post_status='publish'
							OR p.post_status='private'
							OR p.post_status='future'
							OR p.post_status='draft')
							AND p.ID='%s'
							ORDER BY p.post_date DESC LIMIT 1", $videoPostId), ARRAY_A);
    				} else {
    					$result = $wpdb->get_results($wpdb->prepare(
    							"SELECT p.post_title, p.post_name, p.post_content
							FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
							WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
							AND tt.term_id=t.term_id
							AND tr.object_id=p.ID
							AND (p.post_status='publish'
							OR p.post_status='private'
							OR p.post_status='future')
							AND p.ID='%s'
							ORDER BY p.post_date DESC LIMIT 1", $videoPostId), ARRAY_A);
    				}
    			} else {
    				
    				if (isset($_GET['preview']) && $_GET['preview'] == "true") {
    					$result = $wpdb->get_results($wpdb->prepare(
    							"SELECT p.post_title, p.post_name, p.post_content, m.media_id, m.media_thumb
							FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media m
							WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
							AND tt.term_id=t.term_id
							AND tr.object_id=p.ID
							AND p.ID=m.post_id
							AND (p.post_status='publish'
							OR p.post_status='private'
							OR p.post_status='future'
							OR p.post_status='draft')
							AND p.ID='%s'
							ORDER BY p.post_date DESC LIMIT 1", $videoPostId), ARRAY_A);
    				} else {
    					$result = $wpdb->get_results($wpdb->prepare(
    							"SELECT p.post_title, p.post_name, p.post_content, m.media_id, m.media_thumb
							FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media m
							WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
							AND tt.term_id=t.term_id
							AND tr.object_id=p.ID
							AND p.ID=m.post_id
							AND (p.post_status='publish'
							OR p.post_status='private'
							OR p.post_status='future')
							AND p.ID='%s'
							ORDER BY p.post_date DESC LIMIT 1", $videoPostId), ARRAY_A);
    				}
    			}
    			
    			
    			foreach ($result as $row) {
    				$video['postId'] = $videoPostId;
    				$video['postName'] = $row['post_name'];
    				$video['postLink'] = get_site_url() . '/' . $row['post_name'];
    				$video['title'] = $row['post_title'];
    				$video['desc'] = $row['post_content'];
    				$video['mediaId'] = $row['media_id'];
    				
    				
    				$video['smlThumb'] = $row['media_thumb'];
    				
    				break; // only show the first one
    			}
    			
    			$curlString = '';
    			if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)mode(.*)' . $wb_ent_options['vidshortcode']['name'] . '(.*)\/\]/', $video['desc'], $match) >= 1) {
    				//echo 'inside first if';
    				//echo 'match is '.print_r($match, true);
    				$video['mediaId'] = $match[3][0];
    				$video['desc'] = removeShortCode($wb_ent_options['vidshortcode']['tag'],$video['desc']);
    			} else if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)\/\]/', $video['desc'], $match) >= 1) {
    				//echo 'inside second if';
    				//echo 'match is '.print_r($match, true);
    				$video['mediaId'] = $match[3][0];
    				$video['desc'] = removeShortCode($wb_ent_options['vidshortcode']['tag'],$video['desc']);
    			} else {
    				//echo 'inside third if';
    				$video['mediaId'] = $row['media_id'];
    			}
    			
    			$getBrightcoveInfo = false;
    			$attachment_id = get_post_thumbnail_id($videoPostId);
    			if( is_numeric($attachment_id) ){
    				$video['smlThumb'] = $video['midThumb'] = get_post_meta($videoPostId, 'wb_ppv_prod_img_thumb', true);
    				$video['largeThumb'] = get_post_meta($videoPostId, 'wb_ppv_prod_img_main', true);
    				$video['videoStill'] = get_post_meta($videoPostId, 'wb_ppv_prod_video_still', true);
    			}
    			else{
    				$video['smlThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $video['postId'] . '_thumb.jpg';
    				$ch = curl_init();
    				$timeout = 0; // set to zero for no timeout
    				curl_setopt($ch, CURLOPT_URL, $video['smlThumb']);
    				curl_setopt($ch, CURLOPT_HEADER, 1);
    				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    				$file_contents = curl_exec($ch);
    				list($headers, $response) = explode("\r\n\r\n", $file_contents, 2);
    				$headers = explode("\n", $headers);
    				
    				curl_close($ch);
    				$returndata = json_decode($file_contents);
    				
    				if( strpos($headers[0], '200 OK') <= 0 ){
    					$video['smlThumb'] = $wb_ent_options['defaultsmlthumb'];
    				}
    				
    				$video['midThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $video['postId'] . '_midThumb.jpg';
    				$ch = curl_init();
    				$timeout = 0; // set to zero for no timeout
    				curl_setopt($ch, CURLOPT_URL, $video['midThumb']);
    				curl_setopt($ch, CURLOPT_HEADER, 1);
    				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    				$file_contents = curl_exec($ch);
    				list($headers, $response) = explode("\r\n\r\n", $file_contents, 2);
    				$headers = explode("\n", $headers);
    				
    				curl_close($ch);
    				$returndata = json_decode($file_contents);
    				
    				if( strpos($headers[0], '200 OK') <= 0 ){
    					$video['midThumb'] = $wb_ent_options['defaultmidThumb'];
    				}
    				
    				$video['largeThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $video['postId'] . '_largeThumb.jpg';
    				
    				$ch = curl_init();
    				$timeout = 0; // set to zero for no timeout
    				curl_setopt($ch, CURLOPT_URL, $video['largeThumb']);
    				curl_setopt($ch, CURLOPT_HEADER, 1);
    				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    				$file_contents = curl_exec($ch);
    				list($headers, $response) = explode("\r\n\r\n", $file_contents, 2);
    				$headers = explode("\n", $headers);
    				
    				curl_close($ch);
    				$returndata = json_decode($file_contents);
    				
    				
    				if( strpos($headers[0], '200 OK') <= 0 ){
    					$video['largeThumb'] = $wb_ent_options['defaultlrgthumb'];
    				}
    				
    				//Video Still
    				$video['videoStill'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/stills/' . $video['postId'] . '_still.jpg';
    				
    				$ch = curl_init();
    				$timeout = 0; // set to zero for no timeout
    				curl_setopt($ch, CURLOPT_URL, $video['videoStill']);
    				curl_setopt($ch, CURLOPT_HEADER, 1);
    				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    				$file_contents = curl_exec($ch);
    				list($headers, $response) = explode("\r\n\r\n", $file_contents, 2);
    				$headers = explode("\n", $headers);
    				
    				curl_close($ch);
    				$returndata = json_decode($file_contents);
    				
    				
    				if( strpos($headers[0], '200 OK') <= 0 ){
    					$video['videoStill'] = $wb_ent_options['defaultstill'];
    				}
    			}
    			
    			
    			
    			if ($wb_ent_options['updatethumbs']) {
    				
    				
    				//check if the midsize and the large thumbs are available
    				$headers = get_headers($wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $video['postId'] . '_midThumb.jpg');
    				if (preg_match('/^HTTP\/\d\.\d\s+(200|301|302)/', $headers[0])) {
    					$video['midThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $video['postId'] . '_midThumb.jpg';
    				} else {
    					$getBrightcoveInfo = true;
    				}
    				
    				
    				
    				//check if the midsize and the large thumbs are available
    				$headers = get_headers($wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $video['postId'] . '_largeThumb.jpg');
    				if (preg_match('/^HTTP\/\d\.\d\s+(200|301|302)/', $headers[0])) {
    					$video['largeThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $video['postId'] . '_largeThumb.jpg';
    				} else {
    					$getBrightcoveInfo = true;
    				}
    				
    				
    				
    				//check if the video displayed is through a shortcode
    				if ($video && (trim($video['smlThumb']) == '' || trim($video['mediaId']) == '' || $wb_ent_options['updatethumbs'] || $getBrightcoveInfo )) {
    					
    					if (trim($curlString) == '' && $wb_ent_options['vidshortcode']['enabled']) {
    						$video['mediaId'] = '';
    						$video['smlThumb'] = $wb_ent_options['defaultsmlthumb'];
    					} else {
    						$video['smlThumb'] = wb_get_video_thumbnail_url($video['mediaId']);
    					}
    					
    					$updateResult = $wpdb->query($wpdb->prepare(
    							"UPDATE wb_media
							SET media_thumb='%s'
							WHERE media_id='%s'", $video['smlThumb'], $video['mediaId']
    							));
    					
    					$videoStill = wb_get_video_still_url($video['mediaId']);
    					$video['videoStill'] = $videoStill;
    					
    					if (trim($videoStill) == '' || is_null($videoStill) || trim($video['mediaId']) == '' || is_null($video['mediaId'])) {
    						$videoStill = $wb_ent_options['defaultstill'];
    					}
    					
    					
    					if ($getBrightcoveInfo || $wb_ent_options['updatethumbs']) {
    						$prewd = getcwd();
    						chdir(realpath(dirname(__FILE__)));
    						
    						
    						
    						
    						if (!class_exists('SimpleImage'))
    							require_once('resources/SimpleImage.php');
    							
    							//include the S3 class
    							if (!class_exists('S3'))
    								require_once('resources/S3.php');
    								
    								$s3 = new S3($wb_ent_options['amazons3info']['accesskey'], $wb_ent_options['amazons3info']['secretkey']);
    								
    								
    								
    								
    								copy($videoStill, '../../../images/stills/' . $video['postId'] . '_still.jpg');
    								$image = new SimpleImage();
    								$image->load('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/stills/' . $video['postId'] . '_still.jpg');
    								
    								$image->resize(375, 211);
    								$image->save('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $video['postId'] . '_largeThumb.jpg', IMAGETYPE_JPEG, 100);
    								
    								
    								
    								$largeThumbFileName = 'images/thumbs/' . $video['postId'] . '_largeThumb.jpg';
    								$filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $video['postId'] . '_largeThumb.jpg';
    								
    								if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $largeThumbFileName, S3::ACL_PUBLIC_READ)) {
    									//echo "debug: We successfully uploaded your file.<br />";
    								} else {
    									//echo "debug: Something went wrong while uploading your file... sorry.<br />";
    								}
    								
    								$video['largeThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $video['postId'] . '_largeThumb.jpg';
    								
    								
    								$image->resize(240, 135);
    								$image->save('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $video['postId'] . '_midThumb.jpg', IMAGETYPE_JPEG, 100);
    								
    								$midThumbFileName = 'images/thumbs/' . $video['postId'] . '_midThumb.jpg';
    								$filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $video['postId'] . '_midThumb.jpg';
    								
    								if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $midThumbFileName, S3::ACL_PUBLIC_READ)) {
    									//echo "debug: We successfully uploaded your file.<br />";
    								} else {
    									//echo "debug: Something went wrong while uploading your file... sorry.<br />";
    								}
    								
    								$video['midThumb'] = $wb_ent_options['amazons3info']['channelbucketurl'] . '/images/thumbs/' . $video['postId'] . '_midThumb.jpg';
    								
    								chdir($prewd);
    					}
    				}
    			}
    			
    			
    			
    			if (isset($video)) {
    				$video['tags'] = get_the_tags($video['postId']);
    				
    				$taglistArray = array();
    				foreach ($video['tags'] as $currentTag) {
    					$taglistArray[] = $currentTag->name;
    				}
    				
    				$video['taglist'] = implode(', ', $taglistArray);
    				
    				return $video;
    			}
    			return null;
    		}
    	}
    }
	
	if (!function_exists('wb_get_page_details')) {
	  function wb_get_page_details($videoPostId = 0) {
		  //if the post being displayed is on for previewing
		  if (isset($_GET['preview']) && $_GET['preview'] == "true") {
			  $result = $wpdb->get_results($wpdb->prepare(
					"SELECT p.post_title, p.post_name, p.post_content 
			  FROM wp_posts p
			  WHERE (p.post_status='publish'
			  OR p.post_status='private' 
			  OR p.post_status='future'
			  OR p.post_status='draft')
			  AND p.ID='%s'
			  ORDER BY p.post_date DESC LIMIT 1", $videoPostId));
		  } else {
			  $result = $wpdb->get_results($wpdb->prepare(
					"SELECT p.post_title, p.post_name, p.post_content 
			  FROM wp_posts p
			  WHERE (p.post_status='publish'
			  OR p.post_status='private' 
			  OR p.post_status='future')
			  AND p.ID='%s'        
			  ORDER BY p.post_date DESC LIMIT 1", $videoPostId));
		  }


		  foreach ($result as $row) {
			  $video['postId'] = $videoPostId;
			  $video['postName'] = $row['post_name'];
			  $video['postLink'] = get_site_url() . '/' . $row['post_name'];
			  $video['title'] = $row['post_title'];
			  $video['desc'] = $row['post_content'];
			  $video['mediaId'] = $row['media_id'];
			  $video['smlThumb'] = $row['media_thumb'];

			  break; // only show the first one   
		  }

		  if (isset($video)) {
			  $video['tags'] = get_the_tags($video['postId']);
			  return $video;
		  }
		  return null;
	  }
	}
    /*
     * $excludePostId -- will be excluded on the list returned
     * $paging -- will determine if the list will be paged or not
     * Returns an array
     *  postIds -- an array of post Ids on the list
     *  posts -- an array of post with all its details
     *  numPosts -- total number of posts on the list
     */
	
	if (!function_exists('wb_get_archive_vids')) {
		function wb_get_archive_vids($excludePostId = 0, $paging = false, $limitCount = -1) {
			global $wb_ent_options, $wbArraySearch, $wbArrayReplace, $wpdb, $current_lang, $catlibrary, $passwordProtectedCondition, $privatePostsIds, $unlistedPostsIds;
			$archiveVideo = array(
					postIds => array(),
					posts => array(),
					numPosts => 0
			);
			
			$exludedPostString = '';
			if ($excludePostId != 0) {
				$exludedPostString = 'AND p.ID <> ' . $excludePostId;
			}
			
			//echo 'debug: $exludedPostString is '.$exludedPostString;
			//get total number of pages
			if ($wb_ent_options['vidshortcode']['enabled']) {
				$maxPageResult = $wpdb->get_results($wpdb->prepare(
						"SELECT p.ID
						FROM wp_posts p
						WHERE p.post_type IN ('post', 'wb_audio')
						AND p.post_status='publish'
						$passwordProtectedCondition
						$exludedPostString
						GROUP BY p.ID
						ORDER BY p.post_date DESC
						"), ARRAY_A);
			} else {
				$maxPageResult = $wpdb->get_results($wpdb->prepare(
						"SELECT p.ID
						FROM wp_posts p, wb_media b, wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t
						WHERE b.post_id=p.ID
						AND tt.term_taxonomy_id = tr.term_taxonomy_id
						AND tt.term_id=t.term_id
						AND tr.object_id=p.ID
						AND p.post_type IN ('post', 'wb_audio')
						AND p.post_status='publish'
						$passwordProtectedCondition
						AND t.term_id IN(%s, ".$wb_ent_options['videocats']['podcast'].")
						$exludedPostString
						GROUP BY p.ID
						ORDER BY p.post_date DESC
						", $catlibrary), ARRAY_A);
			}
			
			//echo 'debug: $maxPageResult is '.print_r($maxPageResult, true);
			
			$archiveVideo['numPosts'] = $maxPageNumRows = $wpdb->num_rows;
			
			//echo 'debug: numPosts is '.$archiveVideo['numPosts'];
			
			
			$limitString = '';
			if ($paging) {
				$limit = $wb_ent_options['videolistinfo']['vidsperpage'];
				$max_pages = ceil($maxPageNumRows / $limit);
				
				if (!isset($_GET['vidPage']) || ($_GET['vidPage']) == 1) {
					$start = 0;
					$vidPage = 1;
				} else {
					$vidPage = $_GET['vidPage'];
					$start = (($vidPage - 1) * $limit);
				}
				
				$limitString = sprintf(" LIMIT %s, %s ", $start, $limit);
			} else {
				$limit = $maxPageNumRows;
			}
			
			//echo 'debug: $limitString is '.$limitString;
			
			if ($wb_ent_options['vidshortcode']['enabled']) {
				$result = $wpdb->get_results(
						"SELECT p.ID
						FROM wp_posts p
						WHERE p.post_type IN ('post', 'wb_audio')
						AND p.post_status='publish'
						$passwordProtectedCondition
						$exludedPostString
						GROUP BY p.ID
						ORDER BY p.post_date DESC
						$limitString
						", ARRAY_A);
			} else {
				$result = $wpdb->get_results($wpdb->prepare(
						"SELECT p.ID
						FROM wp_posts p, wb_media b, wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t
						WHERE b.post_id=p.ID
						AND tt.term_taxonomy_id = tr.term_taxonomy_id
						AND tt.term_id=t.term_id
						AND tr.object_id=p.ID
						AND p.post_type IN ('post', 'wb_audio')
						AND p.post_status='publish'
						$passwordProtectedCondition
						AND t.term_id IN(%s, ".$wb_ent_options['videocats']['podcast'].")
						$exludedPostString
						GROUP BY p.ID
						ORDER BY p.post_date DESC
						$limitString
						", $catlibrary), ARRAY_A);
			}
			
			//echo 'debug: $result is '.print_r($result, true);
			
			$i = 0;
			$more = false;
			
			foreach ($result as $currentPost) {
				//echo 'debug: $col is '.print_r($col, true);
				if ($i >= $limit || ($limitCount > 0 && $i >= $limitCount) ) {
					$more = true;
					break;
				}
				
				if ((!is_user_logged_in() ) && ( in_array($currentPost['ID'], $privatePostsIds) || in_array($currentPost['ID'], $unlistedPostsIds) )) {
					$i--;
					continue;
				}
				
				$archiveVideo['postIds'][] = $currentPost['ID'];
				
				$archiveVideo['posts'][] = $currentVideo = wb_get_post_details($currentPost['ID']);
				
				$i++;
			}
			$archiveVideo['numPosts'] = $i+1;
			
			return $archiveVideo;
		}
	}

    /*
     * $currentPostId -- will be the post we're getting related vids for
     * Returns an array
     *  postIds -- an array of post Ids on the list
     *  posts -- an array of post with all its details
     *  numPosts -- total number of posts on the list
     */

	if (!function_exists('wb_get_related_vids')) {
		function wb_get_related_vids($currentPostId = 0, $limitCount = -1) {
			global $wb_ent_options, $wbArraySearch, $wbArrayReplace, $wpdb, $passwordProtectedCondition, $privatePostsIds, $unlistedPostsIds;
			
			if ($currentPostId == 0) {
				return;
			}
			
			$relatedVideo = array(
					postIds => array(),
					posts => array(),
					numPosts => 0
			);
			
			// Grab tags for current video
			$postTags = $wpdb->get_results($wpdb->prepare(
			"SELECT t.term_id, t.name
			FROM wp_posts p, wp_terms t, wp_term_relationships tr, wp_term_taxonomy tt
			WHERE t.term_id=tt.term_id
			AND tt.term_taxonomy_id=tr.term_taxonomy_id
			AND tr.object_id=p.ID
			$passwordProtectedCondition
			AND (tt.taxonomy='post_tag'
			OR tt.taxonomy='category')
			AND t.term_id <> %s
			AND t.term_id <> %s
			AND t.term_id <> %s
			AND p.ID=%s", $wb_ent_options['videocats']['webcast'], $wb_ent_options['videocats']['feature'], $wb_ent_options['videocats']['library'], $currentPostId), ARRAY_A);
			
			//echo 'debug: $postTags is '.print_r($postTags, true);
			
			$tag = array();
			foreach ($postTags as $row) {
				$tag[$row['term_id']] = $row['term_id'];
			}
			//} if statement to include tv guide and cvideos
			//echo 'debug: tag is '.print_r($tag, true);
			//echo 'debug: condition is '.( isset($tag) && is_array($tag) && count($tag) > 0 );
			//get videos for each
			if (isset($tag) && is_array($tag) && count($tag) > 0) {
				
				//echo 'debug: inside condition';
				
				foreach ($tag as $t) {
					//echo 'debug: inside foreach';
					
					if ($wb_ent_options['vidshortcode']['enabled']) {
						//echo 'debug: inside if shortcode enabled';
						$result = $wpdb->get_results($wpdb->prepare(
						"SELECT p.ID, p.post_name, p.post_date, p.post_title, p.post_content
						FROM wp_posts p, wp_terms t, wp_term_relationships tr, wp_term_taxonomy tt
						WHERE t.term_id=tt.term_id
						AND tt.term_taxonomy_id=tr.term_taxonomy_id
						AND tr.object_id=p.ID
						$passwordProtectedCondition
						AND (tt.taxonomy='post_tag'
						OR tt.taxonomy='category')
						AND p.ID <> %s
						AND p.post_type='post'
						AND p.post_status='publish'
						AND t.term_id = %s
						GROUP BY p.ID
						ORDER BY p.post_date DESC", $currentPostId, $t), ARRAY_A);
						
						$relatedVideo['numPosts'] = $wpdb->num_rows;
					} else {
						
						//echo 'debug: inside if shortcode disabled';
						
						$result = $wpdb->get_results($wpdb->prepare(
						"SELECT p.ID, p.post_name, b.media_id, p.post_date, p.post_title, p.post_content, b.media_thumb
						FROM wp_posts p, wp_terms t, wp_term_relationships tr, wp_term_taxonomy tt, wb_media b
						WHERE t.term_id=tt.term_id
						AND tt.term_taxonomy_id=tr.term_taxonomy_id
						AND tr.object_id=p.ID
						AND (tt.taxonomy='post_tag'
						OR tt.taxonomy='category')
						AND p.ID=b.post_id
						AND p.ID <> %s
						AND p.post_type IN ('post', 'wb_audio')
						AND p.post_status='publish'
						$passwordProtectedCondition
						AND t.term_id IN(%s, ".$wb_ent_options['videocats']['podcast'].")
						GROUP BY p.ID
						ORDER BY p.ID DESC", $currentPostId, $t), ARRAY_A);
						
						$relatedVideo['numPosts'] = $wpdb->num_rows;
						
						//echo 'debug: $result is '.print_r($result, true);
					}
				}
				
				$i = 0;
				foreach ($result as $currentPost) {
					if ((!is_user_logged_in() ) && ( in_array($currentPost['ID'], $privatePostsIds) || in_array($currentPost['ID'], $unlistedPostsIds) )) {
						$i--;
						continue;
					}
					
					if($limitCount > 0 && $i >= $limitCount) {
						break;
					}
					
					$relatedVideo['postIds'][] = $currentPost['ID'];
					
					$relatedVideo['posts'][] = $currentVideo = wb_get_post_details($currentPost['ID']);
					
					$i++;
				}
				$relatedVideo['numPosts'] = $i+1;
				
				
				//echo 'debug: numPosts is '.$relatedVideo['numPosts'];
				//echo 'debug: $relatedVideo is '.print_r($relatedVideo, true);
				
				return $relatedVideo;
			}
		}
	}

	if (!function_exists('wb_get_cat_vids')) {
		function wb_get_cat_vids($categoryId = 0, $conditionString = '', $paging = false, $limitCount = -1) {
			global $wb_ent_options, $wpdb, $privatePostsIds, $unlistedPostsIds, $passwordProtectedCondition;
			$catVideo = array();
			if ($categoryId != 0) {
				
				if ($wb_ent_options['vidshortcode']['enabled']) {
					//echo 'debug: inside if shortcode enabled';
					$result = $wpdb->get_results($wpdb->prepare(
					"SELECT p.ID, p.post_name, p.post_date, p.post_title, p.post_content
					FROM wp_posts p, wp_terms t, wp_term_relationships tr, wp_term_taxonomy tt
					WHERE t.term_id=tt.term_id
					AND tt.term_taxonomy_id=tr.term_taxonomy_id
					AND tr.object_id=p.ID
					$passwordProtectedCondition
					AND (tt.taxonomy='post_tag'
					OR tt.taxonomy='category')
					AND p.post_type='post'
					AND p.post_status='publish'
					AND t.term_id = %s
					$conditionString
					GROUP BY p.ID
					ORDER BY p.post_date DESC", $categoryId), ARRAY_A);
					
					$catVideo['numPosts'] = $maxPageNumRows = $wpdb->num_rows;
				} else {
					$result = $wpdb->get_results($wpdb->prepare(
							"SELECT p.ID, p.post_name, p.post_date, p.post_title, p.post_content, b.media_id, tr.object_id, b.media_thumb
																FROM wp_posts p, wb_media b, wp_term_relationships tr, wp_term_taxonomy tt, wp_terms t
																WHERE p.post_type IN ('post', 'wb_audio')
																AND p.post_status='publish'
																AND p.ID = b.post_id
																AND tr.object_id = p.ID
																AND tt.taxonomy IN( 'post_tag', 'category' )
																AND tr.object_id = p.ID
																AND tt.term_taxonomy_id = tr.term_taxonomy_id
																AND t.term_id = tt.term_id
																AND t.term_id IN(%s, ".$wb_ent_options['videocats']['podcast'].")
							$conditionString
							GROUP BY p.id
							ORDER BY p.post_date DESC", $categoryId), ARRAY_A);
							
							$catVideo['numPosts'] = $maxPageNumRows = $wpdb->num_rows;
				}
				$limitString = '';
				if ($paging) {
					//echo 'debug: inside paging';
					$limit = $wb_ent_options['videolistinfo']['vidsperpage'];
					//echo 'debug: $limit is '.$limit;
					
					$max_pages = ceil($maxPageNumRows / $limit);
					//echo 'debug: $max_pages is '.$max_pages;
					
					if (!isset($_GET['vidPage']) || ($_GET['vidPage']) == 1) {
						$start = 0;
						$vidPage = 1;
					} else {
						$vidPage = $_GET['vidPage'];
						$start = (($vidPage - 1) * $limit);
					}
					
					$limitString = sprintf(" LIMIT %s, %s ", $start, $limit);
					//echo 'debug: $limitString is '.$limitString;
					
					if ($wb_ent_options['vidshortcode']['enabled']) {
						////echo 'debug: inside if shortcode enabled';
						$result = $wpdb->get_results($wpdb->prepare(
						"SELECT p.ID, p.post_name, p.post_date, p.post_title, p.post_content
						FROM wp_posts p, wp_terms t, wp_term_relationships tr, wp_term_taxonomy tt
						WHERE t.term_id=tt.term_id
						AND tt.term_taxonomy_id=tr.term_taxonomy_id
						AND tr.object_id=p.ID
						$passwordProtectedCondition
						AND (tt.taxonomy='post_tag'
						OR tt.taxonomy='category')
						AND p.post_type='post'
						AND p.post_status='publish'
						AND t.term_id = %s
						$conditionString
						GROUP BY p.ID
						ORDER BY p.post_date DESC
						$limitString", $categoryId), ARRAY_A);
						
						$catVideo['numPosts'] = $maxPageNumRows = $wpdb->num_rows;
					} else {
						
						////echo 'debug: inside if shortcode disabled';
						
						$result = $wpdb->get_results($wpdb->prepare(
						"SELECT p.ID, p.post_name, p.post_date, p.post_title, p.post_content, b.media_id, tr.object_id, b.media_thumb
						FROM wp_posts p, wb_media b, wp_term_relationships tr, wp_term_taxonomy tt, wp_terms t
						WHERE p.post_type IN ('post', 'wb_audio')
						AND p.post_status='publish'
						AND p.ID = b.post_id
						AND tr.object_id = p.ID
						AND tt.taxonomy IN( 'post_tag', 'category' )
						AND tr.object_id = p.ID
						$passwordProtectedCondition
						AND tt.term_taxonomy_id = tr.term_taxonomy_id
						AND t.term_id = tt.term_id
						AND t.term_id IN(%s, ".$wb_ent_options['videocats']['podcast'].")
						$conditionString
						GROUP BY p.id
						ORDER BY p.id DESC
						$limitString", $categoryId), ARRAY_A);
						
						$catVideo['numPosts'] = $maxPageNumRows = $wpdb->num_rows;
					}
					
					//echo 'debug: $result is '.print_r($result, true);
				} else {
					$limit = $maxPageNumRows;
					$max_pages = $maxPageNumRows;
				}
				
				
				
				$i = 0;
				$more = false;
				
				foreach ($result as $currentPost) {
					if ($i >= $limit) {
						$more = true;
						break;
					}
					
					if($limitCount > 0 && $i >= $limitCount) {
						break;
					}
					
					if ( (!is_user_logged_in() ) && ( in_array($currentPost['ID'], $privatePostsIds) || in_array($currentPost['ID'], $unlistedPostsIds) )) {
						$i--;
						continue;
					}
					
					$catVideo['postIds'][] = $currentPost['ID'];
					
					$catVideo['posts'][] = $currentVideo = wb_get_post_details($currentPost['ID']);
					
					$i++;
				}
				//echo 'debug: $catVideo is '.print_r($catVideo, true);
				$catVideo['max_pages'] = $max_pages;
				return $catVideo;
			}
		}
	}

	if (!function_exists('wb_get_popular_vids')) {
		function wb_get_popular_vids($conditionString = '', $paging = false, $limitCount = -1) {
			global $wb_ent_options, $wpdb, $catlibrary, $passwordProtectedCondition,$privatePostsIds,$unlistedPostsIds;
			$popularVideo = array();
			
			if ($wb_ent_options['vidshortcode']['enabled']) {
				//echo 'debug: inside if shortcode enabled';
				$result = $wpdb->get_results($wpdb->prepare(
				"SELECT ID, post_title, post_content, post_name
				FROM wb_views_total v, wp_posts p
				WHERE p.ID=v.post_id
				AND p.post_status = 'publish'
				AND p.post_type IN ('post', 'wb_audio')
				$passwordProtectedCondition
				$conditionString
				GROUP BY p.ID
				ORDER BY views DESC
				"), ARRAY_A);
				
				$popularVideo['numPosts'] = $maxPageNumRows = $wpdb->num_rows;
			} else {
				
				//echo 'debug: inside if shortcode disabled';
				
				$result = $wpdb->get_results($wpdb->prepare(
				"SELECT ID, post_title, post_content, post_name, media_thumb, media_id
				FROM wb_views_total v, wp_posts p, wb_media m, wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t
				WHERE p.ID=v.post_id
				AND tt.term_taxonomy_id = tr.term_taxonomy_id
				AND tt.term_id=t.term_id
				AND tr.object_id=p.ID
				AND p.ID=m.post_id
				AND p.post_status = 'publish'
				AND p.post_type IN ('post', 'wb_audio')
				$passwordProtectedCondition
				AND t.term_id IN(%s, ".$wb_ent_options['videocats']['podcast'].")
				
				$conditionString
				GROUP BY p.ID
				ORDER BY views DESC",
				$catlibrary
				), ARRAY_A);
				
				$popularVideo['numPosts'] = $maxPageNumRows = $wpdb->num_rows;
				
			}
			
			//echo 'debug: $result is '.print_r($result, true);
			
			$limitString = '';
			if ($paging) {
				//echo 'debug: inside paging';
				$limit = $wb_ent_options['videolistinfo']['vidsperpage'];
				//echo 'debug: $limit is '.$limit;
				
				$max_pages = ceil($maxPageNumRows / $limit);
				//echo 'debug: $max_pages is '.$max_pages;
				
				if (!isset($_GET['vidPage']) || ($_GET['vidPage']) == 1) {
					$start = 0;
					$vidPage = 1;
				} else {
					$vidPage = $_GET['vidPage'];
					$start = (($vidPage - 1) * $limit);
				}
				
				$limitString = sprintf(" LIMIT %s, %s ", $start, $limit);
				//echo 'debug: $limitString is '.$limitString;
				
				if ($wb_ent_options['vidshortcode']['enabled']) {
					////echo 'debug: inside if shortcode enabled';
					$result = $wpdb->get_results($wpdb->prepare(
					"SELECT ID, post_title, post_content, post_name
					FROM wb_views_total v, wp_posts p
					WHERE p.ID=v.post_id
					AND p.post_status = 'publish'
					AND p.post_type='post'
					$passwordProtectedCondition
					$conditionString
					GROUP BY p.ID
					ORDER BY views DESC
					$limitString"), ARRAY_A);
					
					$popularVideo['numPosts'] = $maxPageNumRows = $wpdb->num_rows;
				} else {
					
					////echo 'debug: inside if shortcode disabled';
					
					$result = $wpdb->get_results($wpdb->prepare(
					"SELECT ID, post_title, post_content, post_name, media_thumb, media_id
					FROM wb_views_total v, wp_posts p, wb_media m
					WHERE p.ID=v.post_id
					AND p.ID=m.post_id
					AND p.post_status = 'publish'
					AND p.post_type='post'
					$passwordProtectedCondition
					$conditionString
					GROUP BY p.ID
					ORDER BY views DESC
					$limitString"), ARRAY_A);
					
					$popularVideo['numPosts'] = $maxPageNumRows = $wpdb->num_rows;
					
				}
				
				//echo 'debug: $result is '.print_r($result, true);
			} else {
				$limit = $wb_ent_options[videolistwidget][videocount];
				$max_pages = 1;
			}
			
			$i = 0;
			$more = false;
			
			foreach ($result as $currentPost) {
				if ($i >= $limit || ($limitCount > 0 && $i >= $limitCount) ) {
					$more = true;
					break;
				}
				
				if ((!is_user_logged_in() ) && ( in_array($currentPost['ID'], $privatePostsIds) || in_array($currentPost['ID'], $unlistedPostsIds) )) {
					$i--;
					continue;
				}
				
				$popularVideo['postIds'][] = $currentPost['ID'];
				
				$popularVideo['posts'][] = $currentVideo = wb_get_post_details($currentPost['ID']);
				
				$i++;
			}
			//echo 'debug: $popularVideo is '.print_r($popularVideo, true);
			$popularVideo['max_pages'] = $max_pages;
			$popularVideo['numPosts'] = $i+1;
			return $popularVideo;
		}
	}

    /*
     * arguments:
     *  $parentCat -- if set to zero, will get ALL the categories added;
     *  $recursive -- if it will get all the subcategories for each and all categories(m
     */
	if (!function_exists('wb_get_categories')) {
	  function wb_get_categories($parentCat = 0, $recursive = true) {
		  global $wb_ent_options, $wpdb, $current_lang;
		  $postCategories = array();

		  if( $current_lang != 'en_US' && $current_lang != 'en_CA' && $current_lang){
		  $cur_lang =  '-'.$current_lang;
		  }else{
		  $cur_lang = '';
		  }

		  if ($parentCat == 0) {
			  $parentCat = $wb_ent_options['videocats']['library'.$cur_lang];
		  }

		  $excludeArray = array();

		  if ($wb_ent_options['videocats']['private'] != 0) {
			  $excludeArray[] = $wb_ent_options['videocats']['private'];
		  }

		  if ($wb_ent_options['videocats']['unlisted'] != 0) {
			  $excludeArray[] = $wb_ent_options['videocats']['unlisted'];
		  }

		  $excludeString = implode(',', $excludeArray);

		  if($wb_ent_options['catlistorder']['options'] == 'id'){
			$args = array(
			   'type' => 'post',
			   'parent' => $parentCat,
			   'orderby' => 'id',
			   'order' => 'ASC',
			   'hide_empty' => 1,
			   'hierarchical' => 0,
			   'exclude' => $excludeString,
			   'include' => '',
			   'number' => '',
			   'taxonomy' => 'category',
			   'pad_counts' => false
			);
		  } else {
			$args = array(
			   'type' => 'post',
			   'parent' => $parentCat,
			   'orderby' => 'name',
			   'order' => 'ASC',
			   'hide_empty' => 1,
			   'hierarchical' => 0,
			   'exclude' => $excludeString,
			   'include' => '',
			   'number' => '',
			   'taxonomy' => 'category',
			   'pad_counts' => false
			);
		  }

		  $categories = get_categories($args);
		  $i = 0;
		  foreach ($categories as $currentCategory) {
			  $postCategories[$i]['cat_id'] = $currentCategory->term_id;
			  $postCategories[$i]['cat_name'] = $currentCategory->name;
			  $postCategories[$i]['cat_slug'] = $currentCategory->slug;


			  if ($recursive) {
				  $args2 = array(
					 'type' => 'post',
					 'parent' => $currentCategory->term_id,
					 'orderby' => 'name',
					 'order' => 'ASC',
					 'hide_empty' => 1,
					 'hierarchical' => 0,
					 'exclude' => $wb_ent_options['videocats']['private'] . ',' . $wb_ent_options['videocats']['unlisted'],
					 'include' => '',
					 'number' => '',
					 'taxonomy' => 'category',
					 'pad_counts' => false
				  );

				  $subCategories = get_categories($args2);
				  $postCategories[$i]['subCatNumRows'] = count($subCategories);

				  foreach ($subCategories as $currentSubCat) {
					  $postCategories[$i]['subCat'][] = array(
						 'subCat_id' => $currentSubCat->term_id,
						 'subCat_name' => $currentSubCat->name,
						 'subCat_slug' => $currentSubCat->slug,
						 'subCat_count' => $currentSubCat->count
					  );
				  }
			  }
			  $i++;
		  }

		  return (array) $postCategories;
	  }
	}
    
    if (!function_exists('wb_get_channels')) {
	  function wb_get_channels($parentCat = 0, $recursive = true) {
		  global $wb_ent_options, $wpdb, $current_lang, $wb_channels_options;
		  $postCategories = array();
		  $wb_channels_options = get_option('wb_channels_options');

		  if($current_lang != 'en_US' && $current_lang != 'en_CA' && $current_lang){
		  $cur_lang =  '-'.$current_lang;
		  }else{
		  $cur_lang = '';
		  }

		  if ($parentCat == 0) {
			  $parentCat = $wb_ent_options['channels']['library'.$cur_lang];
		  }

		  if ($parentCat == 0) {
			  $parentCat = $wb_channels_options['channelsId'];
		  }				

		  $excludeArray = array();

		  if ($wb_ent_options['videocats']['private'] != 0) {
			  $excludeArray[] = $wb_ent_options['videocats']['private'];
		  }

		  if ($wb_ent_options['videocats']['unlisted'] != 0) {
			  $excludeArray[] = $wb_ent_options['videocats']['unlisted'];
		  }

		  $excludeString = implode(',', $excludeArray);

		  if($wb_ent_options['catlistorder']['options'] == 'id'){
			  $args = array(
					'orderby'           => 'id', 
					'order'             => 'ASC',
					'hide_empty'        => true, 
					'exclude'           => $excludeString,
					'exclude_tree'      => array(), 
					'include'           => array(),
					'number'            => '', 
					'fields'            => 'all', 
					'slug'              => '',
					'parent'            => $parentCat,
					'hierarchical'      => false, 
					'childless'         => false,
					'get'               => '', 
					'name__like'        => '',
					'description__like' => '',
					'pad_counts'        => false, 
					'offset'            => '', 
					'search'            => '', 
					'cache_domain'      => 'core'
			  );           
		  } else {
			  $args = array(
					'orderby'           => 'name', 
					'order'             => 'ASC',
					'hide_empty'        => true, 
					'exclude'           => $excludeString,
					'exclude_tree'      => array(), 
					'include'           => array(),
					'number'            => '', 
					'fields'            => 'all', 
					'slug'              => '',
					'parent'            => $parentCat,
					'hierarchical'      => false, 
					'childless'         => false,
					'get'               => '', 
					'name__like'        => '',
					'description__like' => '',
					'pad_counts'        => false, 
					'offset'            => '', 
					'search'            => '', 
					'cache_domain'      => 'core'
			  );
		  }

		  $taxonomies = array( 
				'channel'    
		  );        
		  $categories = get_terms($taxonomies, $args);

		  $i = 0;
		  foreach ($categories as $currentCategory) {           
			  $postCategories[$i]['cat_id'] = $currentCategory->term_id;
			  $postCategories[$i]['cat_name'] = $currentCategory->name;
			  $postCategories[$i]['cat_slug'] = $currentCategory->slug;


			  if ($recursive) {                                
				 $args2 = array(
								'orderby'           => 'name', 
								'order'             => 'ASC',
								'hide_empty'        => true, 
								'exclude'           => $wb_ent_options['videocats']['private'] . ',' . $wb_ent_options['videocats']['unlisted'],
								'exclude_tree'      => array(), 
								'include'           => array(),
								'number'            => '', 
								'fields'            => 'all', 
								'slug'              => '',
								'parent'            => $currentCategory->term_id,
								'hierarchical'      => false, 
								'child_of'          => null,
								'childless'         => false,
								'get'               => '', 
								'name__like'        => '',
								'description__like' => '',
								'pad_counts'        => false, 
								'offset'            => '', 
								'search'            => '', 
								'cache_domain'      => 'core'
					   );                

				  $subCategories = get_terms($taxonomies,$args2);
				  $postCategories[$i]['subCatNumRows'] = count($subCategories);               
				  foreach ($subCategories as $currentSubCat) {
					  $postCategories[$i]['subCat'][] = array(
						 'subCat_id' => $currentSubCat->term_id,
						 'subCat_name' => $currentSubCat->name,
						 'subCat_slug' => $currentSubCat->slug,
						 'subCat_count' => $currentSubCat->count
					  );
				  }
			  }
			  $i++;
		  }

		  return (array) $postCategories;
	  }
	}
    
	if (!function_exists('wb_get_channel_info')) {
	  function wb_get_channel_info($channelId = 0, $recursive = true){
		global $wb_ent_options, $wpdb, $current_lang;
		$channelInfoArray = array();
		if ($channelId != 0) {    
		 $channelInfoArray = $wpdb->get_row( $wpdb->prepare( 
	  "
		SELECT * FROM wb_channels_info WHERE productCategoryId = %d AND status = 1
	  ", 
		  $channelId

	  ) );
	  }
	   return (array) $channelInfoArray;

	  }// end of wb_get_channel_info function
	}

	if (!function_exists('mobile_device_detect')) {
	  function mobile_device_detect($iphone = true, $ipad = true, $android = true, $opera = true, $blackberry = true, $palm = true, $windows = true, $mobileredirect = false, $desktopredirect = false) {

		  $mobile_browser = false; // set mobile browser as false till we can prove otherwise
		  $user_agent = $_SERVER['HTTP_USER_AGENT']; // get the user agent value - this should be cleaned to ensure no nefarious input gets executed
		  $accept = $_SERVER['HTTP_ACCEPT']; // get the content accept value - this should be cleaned to ensure no nefarious input gets executed

		  switch (true) { // using a switch against the following statements which could return true is more efficient than the previous method of using if statements
			  case (preg_match('/ipad/i', $user_agent)); // we find the word ipad in the user agent
				  $mobile_browser = $ipad; // mobile browser is either true or false depending on the setting of ipad when calling the function
				  $status = 'Apple iPad';
				  if (substr($ipad, 0, 4) == 'http') { // does the value of ipad resemble a url
					  $mobileredirect = $ipad; // set the mobile redirect url to the url value stored in the ipad value
				  } // ends the if for ipad being a url
				  break; // break out and skip the rest if we've had a match on the ipad // this goes before the iphone to catch it else it would return on the iphone instead

			  case (preg_match('/ipod/i', $user_agent) || preg_match('/iphone/i', $user_agent)); // we find the words iphone or ipod in the user agent
				  $mobile_browser = $iphone; // mobile browser is either true or false depending on the setting of iphone when calling the function
				  $status = 'Apple';
				  if (substr($iphone, 0, 4) == 'http') { // does the value of iphone resemble a url
					  $mobileredirect = $iphone; // set the mobile redirect url to the url value stored in the iphone value
				  } // ends the if for iphone being a url
				  break; // break out and skip the rest if we've had a match on the iphone or ipod

			  case (preg_match('/android/i', $user_agent));  // we find android in the user agent
				  $mobile_browser = $android; // mobile browser is either true or false depending on the setting of android when calling the function
				  $status = 'Android';
				  if (substr($android, 0, 4) == 'http') { // does the value of android resemble a url
					  $mobileredirect = $android; // set the mobile redirect url to the url value stored in the android value
				  } // ends the if for android being a url
				  break; // break out and skip the rest if we've had a match on android

			  case (preg_match('/opera mini/i', $user_agent)); // we find opera mini in the user agent
				  $mobile_browser = $opera; // mobile browser is either true or false depending on the setting of opera when calling the function
				  $status = 'Opera';
				  if (substr($opera, 0, 4) == 'http') { // does the value of opera resemble a rul
					  $mobileredirect = $opera; // set the mobile redirect url to the url value stored in the opera value
				  } // ends the if for opera being a url
				  break; // break out and skip the rest if we've had a match on opera

			  case (preg_match('/blackberry/i', $user_agent)); // we find blackberry in the user agent
				  $mobile_browser = $blackberry; // mobile browser is either true or false depending on the setting of blackberry when calling the function
				  $status = 'Blackberry';
				  if (substr($blackberry, 0, 4) == 'http') { // does the value of blackberry resemble a rul
					  $mobileredirect = $blackberry; // set the mobile redirect url to the url value stored in the blackberry value
				  } // ends the if for blackberry being a url
				  break; // break out and skip the rest if we've had a match on blackberry

			  case (preg_match('/(pre\/|palm os|palm|hiptop|avantgo|plucker|xiino|blazer|elaine)/i', $user_agent)); // we find palm os in the user agent - the i at the end makes it case insensitive
				  $mobile_browser = $palm; // mobile browser is either true or false depending on the setting of palm when calling the function
				  $status = 'Palm';
				  if (substr($palm, 0, 4) == 'http') { // does the value of palm resemble a rul
					  $mobileredirect = $palm; // set the mobile redirect url to the url value stored in the palm value
				  } // ends the if for palm being a url
				  break; // break out and skip the rest if we've had a match on palm os

			  case (preg_match('/(iris|3g_t|windows ce|opera mobi|windows ce; smartphone;|windows ce; iemobile)/i', $user_agent)); // we find windows mobile in the user agent - the i at the end makes it case insensitive
				  $mobile_browser = $windows; // mobile browser is either true or false depending on the setting of windows when calling the function
				  $status = 'Windows Smartphone';
				  if (substr($windows, 0, 4) == 'http') { // does the value of windows resemble a rul
					  $mobileredirect = $windows; // set the mobile redirect url to the url value stored in the windows value
				  } // ends the if for windows being a url
				  break; // break out and skip the rest if we've had a match on windows

			  case (preg_match('/(mini 9.5|vx1000|lge |m800|e860|u940|ux840|compal|wireless| mobi|ahong|lg380|lgku|lgu900|lg210|lg47|lg920|lg840|lg370|sam-r|mg50|s55|g83|t66|vx400|mk99|d615|d763|el370|sl900|mp500|samu3|samu4|vx10|xda_|samu5|samu6|samu7|samu9|a615|b832|m881|s920|n210|s700|c-810|_h797|mob-x|sk16d|848b|mowser|s580|r800|471x|v120|rim8|c500foma:|160x|x160|480x|x640|t503|w839|i250|sprint|w398samr810|m5252|c7100|mt126|x225|s5330|s820|htil-g1|fly v71|s302|-x113|novarra|k610i|-three|8325rc|8352rc|sanyo|vx54|c888|nx250|n120|mtk |c5588|s710|t880|c5005|i;458x|p404i|s210|c5100|teleca|s940|c500|s590|foma|samsu|vx8|vx9|a1000|_mms|myx|a700|gu1100|bc831|e300|ems100|me701|me702m-three|sd588|s800|8325rc|ac831|mw200|brew |d88|htc\/|htc_touch|355x|m50|km100|d736|p-9521|telco|sl74|ktouch|m4u\/|me702|8325rc|kddi|phone|lg |sonyericsson|samsung|240x|x320|vx10|nokia|sony cmd|motorola|up.browser|up.link|mmp|symbian|smartphone|midp|wap|vodafone|o2|pocket|kindle|mobile|psp|treo)/i', $user_agent)); // check if any of the values listed create a match on the user agent - these are some of the most common terms used in agents to identify them as being mobile devices - the i at the end makes it case insensitive
				  $mobile_browser = true; // set mobile browser to true
				  $status = 'Mobile matched on piped preg_match';
				  break; // break out and skip the rest if we've preg_match on the user agent returned true

			  case ((strpos($accept, 'text/vnd.wap.wml') > 0) || (strpos($accept, 'application/vnd.wap.xhtml+xml') > 0)); // is the device showing signs of support for text/vnd.wap.wml or application/vnd.wap.xhtml+xml
				  $mobile_browser = true; // set mobile browser to true
				  $status = 'Mobile matched on content accept header';
				  break; // break out and skip the rest if we've had a match on the content accept headers

			  case (isset($_SERVER['HTTP_X_WAP_PROFILE']) || isset($_SERVER['HTTP_PROFILE'])); // is the device giving us a HTTP_X_WAP_PROFILE or HTTP_PROFILE header - only mobile devices would do this
				  $mobile_browser = true; // set mobile browser to true
				  $status = 'Mobile matched on profile headers being set';
				  break; // break out and skip the final step if we've had a return true on the mobile specfic headers

			  case (in_array(strtolower(substr($user_agent, 0, 4)), array('1207' => '1207', '3gso' => '3gso', '4thp' => '4thp', '501i' => '501i', '502i' => '502i', '503i' => '503i', '504i' => '504i', '505i' => '505i', '506i' => '506i', '6310' => '6310', '6590' => '6590', '770s' => '770s', '802s' => '802s', 'a wa' => 'a wa', 'acer' => 'acer', 'acs-' => 'acs-', 'airn' => 'airn', 'alav' => 'alav', 'asus' => 'asus', 'attw' => 'attw', 'au-m' => 'au-m', 'aur ' => 'aur ', 'aus ' => 'aus ', 'abac' => 'abac', 'acoo' => 'acoo', 'aiko' => 'aiko', 'alco' => 'alco', 'alca' => 'alca', 'amoi' => 'amoi', 'anex' => 'anex', 'anny' => 'anny', 'anyw' => 'anyw', 'aptu' => 'aptu', 'arch' => 'arch', 'argo' => 'argo', 'bell' => 'bell', 'bird' => 'bird', 'bw-n' => 'bw-n', 'bw-u' => 'bw-u', 'beck' => 'beck', 'benq' => 'benq', 'bilb' => 'bilb', 'blac' => 'blac', 'c55/' => 'c55/', 'cdm-' => 'cdm-', 'chtm' => 'chtm', 'capi' => 'capi', 'cond' => 'cond', 'craw' => 'craw', 'dall' => 'dall', 'dbte' => 'dbte', 'dc-s' => 'dc-s', 'dica' => 'dica', 'ds-d' => 'ds-d', 'ds12' => 'ds12', 'dait' => 'dait', 'devi' => 'devi', 'dmob' => 'dmob', 'doco' => 'doco', 'dopo' => 'dopo', 'el49' => 'el49', 'erk0' => 'erk0', 'esl8' => 'esl8', 'ez40' => 'ez40', 'ez60' => 'ez60', 'ez70' => 'ez70', 'ezos' => 'ezos', 'ezze' => 'ezze', 'elai' => 'elai', 'emul' => 'emul', 'eric' => 'eric', 'ezwa' => 'ezwa', 'fake' => 'fake', 'fly-' => 'fly-', 'fly_' => 'fly_', 'g-mo' => 'g-mo', 'g1 u' => 'g1 u', 'g560' => 'g560', 'gf-5' => 'gf-5', 'grun' => 'grun', 'gene' => 'gene', 'go.w' => 'go.w', 'good' => 'good', 'grad' => 'grad', 'hcit' => 'hcit', 'hd-m' => 'hd-m', 'hd-p' => 'hd-p', 'hd-t' => 'hd-t', 'hei-' => 'hei-', 'hp i' => 'hp i', 'hpip' => 'hpip', 'hs-c' => 'hs-c', 'htc ' => 'htc ', 'htc-' => 'htc-', 'htca' => 'htca', 'htcg' => 'htcg', 'htcp' => 'htcp', 'htcs' => 'htcs', 'htct' => 'htct', 'htc_' => 'htc_', 'haie' => 'haie', 'hita' => 'hita', 'huaw' => 'huaw', 'hutc' => 'hutc', 'i-20' => 'i-20', 'i-go' => 'i-go', 'i-ma' => 'i-ma', 'i230' => 'i230', 'iac' => 'iac', 'iac-' => 'iac-', 'iac/' => 'iac/', 'ig01' => 'ig01', 'im1k' => 'im1k', 'inno' => 'inno', 'iris' => 'iris', 'jata' => 'jata', 'java' => 'java', 'kddi' => 'kddi', 'kgt' => 'kgt', 'kgt/' => 'kgt/', 'kpt ' => 'kpt ', 'kwc-' => 'kwc-', 'klon' => 'klon', 'lexi' => 'lexi', 'lg g' => 'lg g', 'lg-a' => 'lg-a', 'lg-b' => 'lg-b', 'lg-c' => 'lg-c', 'lg-d' => 'lg-d', 'lg-f' => 'lg-f', 'lg-g' => 'lg-g', 'lg-k' => 'lg-k', 'lg-l' => 'lg-l', 'lg-m' => 'lg-m', 'lg-o' => 'lg-o', 'lg-p' => 'lg-p', 'lg-s' => 'lg-s', 'lg-t' => 'lg-t', 'lg-u' => 'lg-u', 'lg-w' => 'lg-w', 'lg/k' => 'lg/k', 'lg/l' => 'lg/l', 'lg/u' => 'lg/u', 'lg50' => 'lg50', 'lg54' => 'lg54', 'lge-' => 'lge-', 'lge/' => 'lge/', 'lynx' => 'lynx', 'leno' => 'leno', 'm1-w' => 'm1-w', 'm3ga' => 'm3ga', 'm50/' => 'm50/', 'maui' => 'maui', 'mc01' => 'mc01', 'mc21' => 'mc21', 'mcca' => 'mcca', 'medi' => 'medi', 'meri' => 'meri', 'mio8' => 'mio8', 'mioa' => 'mioa', 'mo01' => 'mo01', 'mo02' => 'mo02', 'mode' => 'mode', 'modo' => 'modo', 'mot ' => 'mot ', 'mot-' => 'mot-', 'mt50' => 'mt50', 'mtp1' => 'mtp1', 'mtv ' => 'mtv ', 'mate' => 'mate', 'maxo' => 'maxo', 'merc' => 'merc', 'mits' => 'mits', 'mobi' => 'mobi', 'motv' => 'motv', 'mozz' => 'mozz', 'n100' => 'n100', 'n101' => 'n101', 'n102' => 'n102', 'n202' => 'n202', 'n203' => 'n203', 'n300' => 'n300', 'n302' => 'n302', 'n500' => 'n500', 'n502' => 'n502', 'n505' => 'n505', 'n700' => 'n700', 'n701' => 'n701', 'n710' => 'n710', 'nec-' => 'nec-', 'nem-' => 'nem-', 'newg' => 'newg', 'neon' => 'neon', 'netf' => 'netf', 'noki' => 'noki', 'nzph' => 'nzph', 'o2 x' => 'o2 x', 'o2-x' => 'o2-x', 'opwv' => 'opwv', 'owg1' => 'owg1', 'opti' => 'opti', 'oran' => 'oran', 'p800' => 'p800', 'pand' => 'pand', 'pg-1' => 'pg-1', 'pg-2' => 'pg-2', 'pg-3' => 'pg-3', 'pg-6' => 'pg-6', 'pg-8' => 'pg-8', 'pg-c' => 'pg-c', 'pg13' => 'pg13', 'phil' => 'phil', 'pn-2' => 'pn-2', 'pt-g' => 'pt-g', 'palm' => 'palm', 'pana' => 'pana', 'pire' => 'pire', 'pock' => 'pock', 'pose' => 'pose', 'psio' => 'psio', 'qa-a' => 'qa-a', 'qc-2' => 'qc-2', 'qc-3' => 'qc-3', 'qc-5' => 'qc-5', 'qc-7' => 'qc-7', 'qc07' => 'qc07', 'qc12' => 'qc12', 'qc21' => 'qc21', 'qc32' => 'qc32', 'qc60' => 'qc60', 'qci-' => 'qci-', 'qwap' => 'qwap', 'qtek' => 'qtek', 'r380' => 'r380', 'r600' => 'r600', 'raks' => 'raks', 'rim9' => 'rim9', 'rove' => 'rove', 's55/' => 's55/', 'sage' => 'sage', 'sams' => 'sams', 'sc01' => 'sc01', 'sch-' => 'sch-', 'scp-' => 'scp-', 'sdk/' => 'sdk/', 'se47' => 'se47', 'sec-' => 'sec-', 'sec0' => 'sec0', 'sec1' => 'sec1', 'semc' => 'semc', 'sgh-' => 'sgh-', 'shar' => 'shar', 'sie-' => 'sie-', 'sk-0' => 'sk-0', 'sl45' => 'sl45', 'slid' => 'slid', 'smb3' => 'smb3', 'smt5' => 'smt5', 'sp01' => 'sp01', 'sph-' => 'sph-', 'spv ' => 'spv ', 'spv-' => 'spv-', 'sy01' => 'sy01', 'samm' => 'samm', 'sany' => 'sany', 'sava' => 'sava', 'scoo' => 'scoo', 'send' => 'send', 'siem' => 'siem', 'smar' => 'smar', 'smit' => 'smit', 'soft' => 'soft', 'sony' => 'sony', 't-mo' => 't-mo', 't218' => 't218', 't250' => 't250', 't600' => 't600', 't610' => 't610', 't618' => 't618', 'tcl-' => 'tcl-', 'tdg-' => 'tdg-', 'telm' => 'telm', 'tim-' => 'tim-', 'ts70' => 'ts70', 'tsm-' => 'tsm-', 'tsm3' => 'tsm3', 'tsm5' => 'tsm5', 'tx-9' => 'tx-9', 'tagt' => 'tagt', 'talk' => 'talk', 'teli' => 'teli', 'topl' => 'topl', 'hiba' => 'hiba', 'up.b' => 'up.b', 'upg1' => 'upg1', 'utst' => 'utst', 'v400' => 'v400', 'v750' => 'v750', 'veri' => 'veri', 'vk-v' => 'vk-v', 'vk40' => 'vk40', 'vk50' => 'vk50', 'vk52' => 'vk52', 'vk53' => 'vk53', 'vm40' => 'vm40', 'vx98' => 'vx98', 'virg' => 'virg', 'vite' => 'vite', 'voda' => 'voda', 'vulc' => 'vulc', 'w3c ' => 'w3c ', 'w3c-' => 'w3c-', 'wapj' => 'wapj', 'wapp' => 'wapp', 'wapu' => 'wapu', 'wapm' => 'wapm', 'wig ' => 'wig ', 'wapi' => 'wapi', 'wapr' => 'wapr', 'wapv' => 'wapv', 'wapy' => 'wapy', 'wapa' => 'wapa', 'waps' => 'waps', 'wapt' => 'wapt', 'winc' => 'winc', 'winw' => 'winw', 'wonu' => 'wonu', 'x700' => 'x700', 'xda2' => 'xda2', 'xdag' => 'xdag', 'yas-' => 'yas-', 'your' => 'your', 'zte-' => 'zte-', 'zeto' => 'zeto', 'acs-' => 'acs-', 'alav' => 'alav', 'alca' => 'alca', 'amoi' => 'amoi', 'aste' => 'aste', 'audi' => 'audi', 'avan' => 'avan', 'benq' => 'benq', 'bird' => 'bird', 'blac' => 'blac', 'blaz' => 'blaz', 'brew' => 'brew', 'brvw' => 'brvw', 'bumb' => 'bumb', 'ccwa' => 'ccwa', 'cell' => 'cell', 'cldc' => 'cldc', 'cmd-' => 'cmd-', 'dang' => 'dang', 'doco' => 'doco', 'eml2' => 'eml2', 'eric' => 'eric', 'fetc' => 'fetc', 'hipt' => 'hipt', 'http' => 'http', 'ibro' => 'ibro', 'idea' => 'idea', 'ikom' => 'ikom', 'inno' => 'inno', 'ipaq' => 'ipaq', 'jbro' => 'jbro', 'jemu' => 'jemu', 'java' => 'java', 'jigs' => 'jigs', 'kddi' => 'kddi', 'keji' => 'keji', 'kyoc' => 'kyoc', 'kyok' => 'kyok', 'leno' => 'leno', 'lg-c' => 'lg-c', 'lg-d' => 'lg-d', 'lg-g' => 'lg-g', 'lge-' => 'lge-', 'libw' => 'libw', 'm-cr' => 'm-cr', 'maui' => 'maui', 'maxo' => 'maxo', 'midp' => 'midp', 'mits' => 'mits', 'mmef' => 'mmef', 'mobi' => 'mobi', 'mot-' => 'mot-', 'moto' => 'moto', 'mwbp' => 'mwbp', 'mywa' => 'mywa', 'nec-' => 'nec-', 'newt' => 'newt', 'nok6' => 'nok6', 'noki' => 'noki', 'o2im' => 'o2im', 'opwv' => 'opwv', 'palm' => 'palm', 'pana' => 'pana', 'pant' => 'pant', 'pdxg' => 'pdxg', 'phil' => 'phil', 'play' => 'play', 'pluc' => 'pluc', 'port' => 'port', 'prox' => 'prox', 'qtek' => 'qtek', 'qwap' => 'qwap', 'rozo' => 'rozo', 'sage' => 'sage', 'sama' => 'sama', 'sams' => 'sams', 'sany' => 'sany', 'sch-' => 'sch-', 'sec-' => 'sec-', 'send' => 'send', 'seri' => 'seri', 'sgh-' => 'sgh-', 'shar' => 'shar', 'sie-' => 'sie-', 'siem' => 'siem', 'smal' => 'smal', 'smar' => 'smar', 'sony' => 'sony', 'sph-' => 'sph-', 'symb' => 'symb', 't-mo' => 't-mo', 'teli' => 'teli', 'tim-' => 'tim-', 'tosh' => 'tosh', 'treo' => 'treo', 'tsm-' => 'tsm-', 'upg1' => 'upg1', 'upsi' => 'upsi', 'vk-v' => 'vk-v', 'voda' => 'voda', 'vx52' => 'vx52', 'vx53' => 'vx53', 'vx60' => 'vx60', 'vx61' => 'vx61', 'vx70' => 'vx70', 'vx80' => 'vx80', 'vx81' => 'vx81', 'vx83' => 'vx83', 'vx85' => 'vx85', 'wap-' => 'wap-', 'wapa' => 'wapa', 'wapi' => 'wapi', 'wapp' => 'wapp', 'wapr' => 'wapr', 'webc' => 'webc', 'whit' => 'whit', 'winw' => 'winw', 'wmlb' => 'wmlb', 'xda-' => 'xda-',))); // check against a list of trimmed user agents to see if we find a match
				  $mobile_browser = true; // set mobile browser to true
				  $status = 'Mobile matched on in_array';
				  break; // break even though it's the last statement in the switch so there's nothing to break away from but it seems better to include it than exclude it

			  default;
				  $mobile_browser = false; // set mobile browser to false
				  $status = 'Desktop / full capability browser';
				  break; // break even though it's the last statement in the switch so there's nothing to break away from but it seems better to include it than exclude it
		  } // ends the switch
		  // tell adaptation services (transcoders and proxies) to not alter the content based on user agent as it's already being managed by this script, some of them suck though and will disregard this....
  // header('Cache-Control: no-transform'); // http://mobiforge.com/developing/story/setting-http-headers-advise-transcoding-proxies
  // header('Vary: User-Agent, Accept'); // http://mobiforge.com/developing/story/setting-http-headers-advise-transcoding-proxies
		  // if redirect (either the value of the mobile or desktop redirect depending on the value of $mobile_browser) is true redirect else we return the status of $mobile_browser
		  if ($redirect = ($mobile_browser == true) ? $mobileredirect : $desktopredirect) {
			  header('Location: ' . $redirect); // redirect to the right url for this device
			  exit;
		  } else {
  // a couple of folkas have asked about the status - that's there to help you debug and understand what the script is doing
			  if ($mobile_browser == '') {
				  return $mobile_browser; // will return either true or false
			  } else {
				  return array($mobile_browser, $status); // is a mobile so we are returning an array ['0'] is true ['1'] is the $status value
			  }
		  }
	  }
	}

// ends function mobile_device_detect
//Brightcove Curl 
    if (!function_exists('brightcove_curl')) {

        function brightcove_curl($video_id) {
            /*
            global $wb_ent_options;
            $ch = curl_init();
            $timeout = 0; // set to zero for no timeout
            curl_setopt($ch, CURLOPT_URL, 'http://wbapi.brightcove.com/services/library?command=find_video_by_id&video_id=' . $video_id . '&video_fields=id,name,startDate,publishedDate,length,shortDescription,longDescription,thumbnailURL,renditions,FLVURL&media_delivery=http&token=' . $wb_ent_options['brightcoveinfo']['readurltoken']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $file_contents = curl_exec($ch);
            curl_close($ch);
            $returndata = json_decode($file_contents);
            return $returndata;
            */
        }

    }

//End of Brightcove Curl
	if (!function_exists('wb_count_page_views')) {
	  function wb_count_page_views($permalink, $postId) {
		  global $wpdb;

		  $ipAddress = $_SERVER['REMOTE_ADDR'];
		  $browser = $_SERVER['HTTP_USER_AGENT'];
		  $viewSql = $wpdb->query($wpdb->prepare("INSERT INTO wb_views_log ( 
			  permalink,
			  post_id,
			  ip_address,
			  browser
			  )VALUES(
			  '%s',
			  %s,
			  '%s',
			  '%s'
			  )
		", $permalink, $postId, $ipAddress, $browser));
		  //echo "view log";
	  }
	}

    if (!function_exists('wb_enterprise_init')) {

        function wb_enterprise_init() {
            global $wpdb;
            
            $wb_ent_options = get_option('wb_ent_options');
            $oldConfigOptions = array();

            //checking older Calypso versions
            $configPath = $_SERVER['DOCUMENT_ROOT'] . 'includes/config.php';
            if (file_exists($configPath)) {
                include '/includes/config.php';
            }
            
            //checking older Calypso versions
            $bctokensPath = $_SERVER['DOCUMENT_ROOT'] . 'includes/brightcove-tokens.php';
            if (file_exists($bctokensPath)) {
                include '/includes/brightcove-tokens.php';
            }            
            
            if( $wb_ent_options ) {
                $oldConfigOptions = $wb_ent_options;
            }
            else{
                 if (defined('WORKERBEE_IP')) {
                    $oldConfigOptions['workerbeeip'] = WORKERBEE_IP;
                } else {$oldConfigOptions['workerbeeip'] = '24.77.245.56';}

                if (defined('CLIENT_NAME')) {
                    $oldConfigOptions['clientname'] = CLIENT_NAME;
                } else {$oldConfigOptions['clientname'] = 'Workerbee';}
                if (defined('CLIENT_CHANNEL')) {
                    $oldConfigOptions['channelname'] = CLIENT_CHANNEL;
                } else {$oldConfigOptions['channelname'] = 'Workerbee';}
                if (defined('CHANNEL_DESC')) {
                    $oldConfigOptions['channeldesc'] = CHANNEL_DESC;
                } else {$oldConfigOptions['channeldesc'] = 'This is going to be the template for wordpress installs.';}
                if (defined('HAS_HOME') && HAS_HOME) {
                    $oldConfigOptions['hashome'] = true;
                } else {$oldConfigOptions['hashome'] = true;}
                if (defined('HOME_LINK') && HOME_LINK) {
                    $oldConfigOptions['homelink'] = HOME_LINK;
                } else {$oldConfigOptions['homelink'] = 'home';}
                if (defined('CHANNEL_SITE')) {
                    $oldConfigOptions['channeldomain'] = CHANNEL_SITE;
                } else {$oldConfigOptions['channeldomain'] = 'responsive.workerbeetv.com';}
                if (defined('DEFAULT_STILL')) {
                    $oldConfigOptions['defaultstill'] = DEFAULT_STILL;
                } else {$oldConfigOptions['defaultstill'] = $oldConfigOptions['channeldomain'].'/wp-content/themes/enterprise/images/defaultVideoStill.jpg';}
                if (defined('DEFAULT_THUMB')) {
                    $oldConfigOptions['defaultsmlthumb'] = DEFAULT_THUMB;
                } else {$oldConfigOptions['defaultsmlthumb'] = $oldConfigOptions['channeldomain'].'/wp-content/themes/enterprise/images/default-thumb.jpg';}
                $oldConfigOptions['defaultlrgthumb'] = $oldConfigOptions['channeldomain'].'/wp-content/themes/enterprise/images/defaultLargeThumb.jpg';
                $oldConfigOptions['defaultmidthumb'] = $oldConfigOptions['channeldomain'].'/wp-content/themes/enterprise/images/defaultMidThumb.jpg';
                if (defined('CHANNEL_EMAIL')) {
                    $oldConfigOptions['channelemail'] = CHANNEL_EMAIL;
                } else {$oldConfigOptions['channelemail'] = 'webmaster@template.workerbeetv.com';}
                if (defined('LOGIN_LOGO')) {
                    $oldConfigOptions['loginlogo'] = LOGIN_LOGO;
                } else {$oldConfigOptions['loginlogo'] = $oldConfigOptions['channeldomain'].'/wp-content/themes/enterprise/images/fpma_logo-new.png';}
                if (defined('HAS_COMMENTS') && HAS_COMMENTS) {
                    $oldConfigOptions['hascomment'] = true;
                } else {$oldConfigOptions['hascomment'] = true;}

                $oldConfigOptions['commenttype'] = 'disqus';

                if (defined('HAS_SHARE') && HAS_SHARE) {
                    $oldConfigOptions['hasshare'] = true;
                } else {$oldConfigOptions['hasshare'] = true;}
                if (defined('FLOATING_SHARE') && FLOATING_SHARE) {
                    $oldConfigOptions['sharetype'] = 'floating';
                } else if (defined('FLOATING_SHARE')) {
                    $oldConfigOptions['sharetype'] = 'static';
                } else {$oldConfigOptions['sharetype'] = 'floating';}

                if (defined('SHARE_EMBED') && SHARE_EMBED) {
                    $oldConfigOptions['showembed'] = true;
                } else {$oldConfigOptions['showembed'] = true;}
                if (defined('HAS_DOWNLOAD') && HAS_DOWNLOAD) {
                    $oldConfigOptions['hasdownload'] = true;
                } else {$oldConfigOptions['hasdownload'] = true;}
                if (defined('HAS_SUBSCRIBE') && HAS_SUBSCRIBE) {
                    $oldConfigOptions['hassubscribe'] = true;
                } else {$oldConfigOptions['hassubscribe'] = true;}
                $oldConfigOptions['mailchimpform'] = '';
                
                if (defined('SUBSCRIBE_COLLAPSE') && SUBSCRIBE_COLLAPSE) {
                    $oldConfigOptions['subscribetype'] = 'collapse';
                } else if (defined('SUBSCRIBE_COLLAPSE')) {
                    $oldConfigOptions['subscribetype'] = 'static';
                } else {$oldConfigOptions['subscribetype'] = 'collapse';}
                if (defined('HAS_KEYWORDCLOUD') && HAS_KEYWORDCLOUD) {
                    $oldConfigOptions['haskeywordcloud'] = true;
                } else {$oldConfigOptions['haskeywordcloud'] = true;}
                if (defined('HAS_POLL') && HAS_POLL) {
                    $oldConfigOptions['haspoll'] = true;
                } else {$oldConfigOptions['haspoll'] = true;}
                if (defined('HAS_TOPIC_LIST') && HAS_TOPIC_LIST) {
                    $oldConfigOptions['hascatlist'] = true;
                } else {$oldConfigOptions['hascatlist'] = true;}
                
                        if (defined('HAS_TOPIC_LIST') && defined('TOPIC_COLLAPSE') && TOPIC_COLLAPSE) {
                    $oldConfigOptions['catlisttype'] = 'collapse';
                } else if (defined('HAS_TOPIC_LIST') && defined('TOPIC_COLLAPSE') && HAS_TOPIC_LIST_IMG) {
                    $oldConfigOptions['catlisttype'] = 'image';
                } else if (defined('HAS_TOPIC_LIST')) {
                    $oldConfigOptions['catlisttype'] = 'static';
                } else {$oldConfigOptions['catlisttype'] = 'collapse';}
                        
                if (defined('HAS_TOPIC_COUNT') && HAS_TOPIC_COUNT) {
                    $oldConfigOptions['catlistcount'] = true;
                } else {$oldConfigOptions['catlistcount'] = true;}

                if (defined('HAS_CAT_VIDEOS_WIDGET') && HAS_CAT_VIDEOS_WIDGET) {
                    $oldConfigOptions['hasrotatewidget'] = true;
                } else {$oldConfigOptions['hasrotatewidget'] = false;}
                
                        if (defined('HAS_CAT_VIDEOS_WIDGET') && HAS_CAT_VIDEOS_WIDGET) {
                    $oldConfigOptions['rotatewidgetcatarray'] = array();
                }
                if (defined('HAS_ABOUT') && HAS_ABOUT) {
                    $oldConfigOptions['hasabout'] = true;
                } else {$oldConfigOptions['hasabout'] = true;}
                if (defined('HAS_ARCHIVE_WIDGET') && HAS_ARCHIVE_WIDGET) {
                    $oldConfigOptions['hasarchivewidget'] = true;
                } else {$oldConfigOptions['hasarchivewidget'] = true;}
                if (defined('CPANEL_USER')) {
                    $oldConfigOptions['serveruser'] = CPANEL_USER;
                } else {$oldConfigOptions['serveruser'] = 'template';}
                if (defined('KLOXO_FOLDER')) {
                    $oldConfigOptions['publicdir'] = KLOXO_FOLDER;
                } else {$oldConfigOptions['publicdir'] = 'responsive.workerbeetv.com';}

                if (defined('HAS_POST_LOGO') && HAS_POST_LOGO) {
                    $oldConfigOptions['haspostlogo'] = true;
                    if (defined('POST_LOGO_IMG')) {
                        $oldConfigOptions['postlogoinfo']['image'] = POST_LOGO_IMG;
                        $oldConfigOptions['postlogoinfo']['width'] = 107;
                        $oldConfigOptions['postlogoinfo']['height'] = 99;
                        $oldConfigOptions['postlogoinfo']['align'] = 'left';
                        $oldConfigOptions['postlogoinfo']['style'] = '';
                    } else {
                                    $oldConfigOptions['postlogoinfo']['image'] = '';
                        $oldConfigOptions['postlogoinfo']['width'] = '';
                        $oldConfigOptions['postlogoinfo']['height'] = '';
                        $oldConfigOptions['postlogoinfo']['align'] = '';
                        $oldConfigOptions['postlogoinfo']['style'] = '';}
                } else {
                           $oldConfigOptions['haspostlogo'] = false;
                        }

                if (defined('HAS_CVIDEOS') && HAS_CVIDEOS) {
                    $oldConfigOptions['hascvideo'] = true;

                    if (defined('CAT_CVIDEOS')) {
                        $oldConfigOptions['cvideoinfo']['cat'] = CAT_CVIDEOS;
                    } else {$oldConfigOptions['cvideoinfo']['cat'] = '17';}

                    if (defined('CVIDEO_AUTHOR') && CVIDEO_AUTHOR) {
                        $oldConfigOptions['cvideoinfo']['author'] = CVIDEO_AUTHOR;
                    } else {$oldConfigOptions['cvideoinfo']['author'] = '2';}

                    if (defined('CVIDEOS_DEBUG') && CVIDEOS_DEBUG) {
                        $oldConfigOptions['cvideoinfo']['debug'] = true;
                    } else {$oldConfigOptions['cvideoinfo']['debug'] = false;}
                    if (defined('CVIDEO_FORM_LINK')) {
                        $oldConfigOptions['cvideoinfo']['formurl'] = CVIDEO_FORM_LINK;
                    } else {$oldConfigOptions['cvideoinfo']['formurl'] = 'community-video-form';}
                    if (defined('CVIDEO_CONFIRM_LINK')) {
                        $oldConfigOptions['cvideoinfo']['confirmpage'] = CVIDEO_CONFIRM_LINK;
                    } else {$oldConfigOptions['cvideoinfo']['confirmpage'] = 'community-video-confirmation';}

                    if (defined('MAX_FILE_SIZE')) {
                        $oldConfigOptions['cvideoinfo']['maxfilesize'] = MAX_FILE_SIZE;
                    } else {$oldConfigOptions['cvideoinfo']['maxfilesize'] = '510027336';}

                    if (defined('MAX_VIDEO_UPLOADS')) {
                        $oldConfigOptions['cvideoinfo']['uploadlimit'] = MAX_VIDEO_UPLOADS;
                    } else {$oldConfigOptions['cvideoinfo']['uploadlimit'] = '-1';}

                    if (defined('MAX_VIDEO_CONTACT')) {
                        $oldConfigOptions['cvideoinfo']['maxvidnotify'] = MAX_VIDEO_CONTACT;
                    } else {$oldConfigOptions['cvideoinfo']['maxvidnotify'] = 'webmaster@template.wbtvserver.com';}

                    if (defined('CVIDEO_LABEL_TEXT')) {
                        $oldConfigOptions['cvideoinfo']['vidlabeltext'] = CVIDEO_LABEL_TEXT;
                    } else {$oldConfigOptions['cvideoinfo']['vidlabeltext'] = 'Community Video';}

                    if (defined('CVIDEO_SUBCATS') && CVIDEO_SUBCATS) {
                        $oldConfigOptions['cvideoinfo']['hassubcats'] = true;
                    } else {$oldConfigOptions['cvideoinfo']['hassubcats'] = true;}
                } else {$oldConfigOptions['hascvideo'] = false;}
                        
                if (defined('PLAYER_CVIDEO')) {
                    $oldConfigOptions['bcplayers']['cvideo'] = PLAYER_CVIDEO;
                } else {$oldConfigOptions['bcplayers']['cvideo'] = '1894468836001';}


                if (defined('DISPLAY_RELATED') && DISPLAY_RELATED) {
                    $oldConfigOptions['videolistwidget']['hasrelated'] = true;
                } else {$oldConfigOptions['videolistwidget']['hasrelated'] = true;}

                if (defined('DISPLAY_ARCHIVE') && DISPLAY_ARCHIVE) {
                    $oldConfigOptions['videolistwidget']['hasarchive'] = true;
                } else {$oldConfigOptions['videolistwidget']['hasarchive'] = true;}

                if (defined('DISPLAY_GUIDE') && DISPLAY_GUIDE) {
                    $oldConfigOptions['videolistwidget']['hastvguide'] = true;
                } else {$oldConfigOptions['videolistwidget']['hastvguide'] = false;}

                if (defined('DISPLAY_POPULAR') && DISPLAY_POPULAR) {
                    $oldConfigOptions['videolistwidget']['haspopular'] = true;
                } else {$oldConfigOptions['videolistwidget']['haspopular'] = true;}

                if (defined('DISPLAY_SEP')) {
                    $oldConfigOptions['videolistwidget']['displaymode'] = DISPLAY_SEP;
                } else {$oldConfigOptions['videolistwidget']['displaymode'] = 'tabbed';}

                if (defined('LIST_CAT') && LIST_CAT != 0) {
                    $oldConfigOptions['videolistinfo']['videocats'] = array(LIST_CAT);
                } else {$oldConfigOptions['videolistinfo']['videocats'] = array();}

                if (defined('STATIC_LIST') && STATIC_LIST) {
                    $oldConfigOptions['videolistinfo']['usestatic'] = true;
                } else {$oldConfigOptions['videolistinfo']['usestatic'] = false;}

                if (defined('VIDS_PER_PAGE') && VIDS_PER_PAGE) {
                    $oldConfigOptions['videolistinfo']['vidsperpage'] = 10;
                } else {$oldConfigOptions['videolistinfo']['vidsperpage'] = 10;}

                if (defined('UPDATE_VIDEO_THUMBS') && UPDATE_VIDEO_THUMBS) {
                    $oldConfigOptions['updatethumbs'] = true;
                }$oldConfigOptions['updatethumbs'] = false;



                if (defined('FEED_RSS')) {
                    $oldConfigOptions['rsslink'] = FEED_RSS;
                }$oldConfigOptions['rsslink'] = $oldConfigOptions['channeldomain'].'/wp-content/themes/enterprise/feed/mrss.php';

                if (defined('EMAIL_SUBSCRIBE')) {
                    $oldConfigOptions['emailsubslink'] = EMAIL_SUBSCRIBE;
                } else {$oldConfigOptions['emailsubslink'] = '/subscribe';}

                if (defined('FEED_ITUNES')) {
                    $oldConfigOptions['ituneslink'] = FEED_ITUNES;
                } else {$oldConfigOptions['ituneslink'] = '';}

                if (defined('ITUNES_IMAGE')) {
                    $oldConfigOptions['itunesimage'] = ITUNES_IMAGE;
                } else {$oldConfigOptions['itunesimage'] = $oldConfigOptions['channeldomain'].'/wp-content/themes/enterprise/images/wrkbee-icon.png';}

                if (defined('DISQUS_SHORTNAME')) {
                    $oldConfigOptions['disqussname'] = DISQUS_SHORTNAME;
                } else {$oldConfigOptions['disqussname'] = 'officeweeklytv';}

                if (defined('SHARETHIS_PUB_ID')) {
                    $oldConfigOptions['sharethispubid'] = SHARETHIS_PUB_ID;
                } else {$oldConfigOptions['sharethispubid'] = 'f5322edb-8d5b-4c6f-8d6a-7376002f5ca6';}

                if (defined('RECAPTCHA_PRIV')) {
                    $oldConfigOptions['recaptchapriv'] = RECAPTCHA_PRIV;
                } else {$oldConfigOptions['recaptchapriv'] = '6LeF_AsAAAAAACKwJHGsV9f8LRvfy_00eYnJA6Q2';}

                if (defined('RECAPTCHA_PUB')) {
                    $oldConfigOptions['recaptchapub'] = RECAPTCHA_PUB;
                } else {$oldConfigOptions['recaptchapub'] = '6LeF_AsAAAAAAFSLOe1jX3EzES_ArLxoY1viQ9VF';}

                if (defined('FB_APP_ID')) {
                    $oldConfigOptions['fbappid'] = FB_APP_ID;
                } else {$oldConfigOptions['fbappid'] = '190725671020161';}

                global $analytics;
                if (is_array($analytics) && count($analytics) > 0) {
                    $oldConfigOptions['analytics'] = $analytics;
                } else {
                           $oldConfigOptions['analytics']['name']='Workerbee';
                           $oldConfigOptions['analytics']['code']='UA-15100124-1';
                        }

                if (defined('AMAZON_S3_BUCKET')) {
                    $oldConfigOptions['amazons3info']['channelbucket'] = AMAZON_S3_BUCKET;
                } else {$oldConfigOptions['amazons3info']['channelbucket'] = 'workerbeewebteam';}

                if (defined('AMAZON_S3_URL')) {
                    $oldConfigOptions['amazons3info']['channelbucketurl'] = AMAZON_S3_URL;
                } else {$oldConfigOptions['amazons3info']['channelbucketurl'] = 'http://workerbeewebteam.s3.amazonaws.com';}

                if (defined('AWS_ACCESS_KEY')) {
                    $oldConfigOptions['amazons3info']['accesskey'] = AWS_ACCESS_KEY;
                } else {$oldConfigOptions['amazons3info']['accesskey'] = 'AKIAJBSQCWDEHEF44B3Q';}

                if (defined('AWS_SECRET_KEY')) {
                    $oldConfigOptions['amazons3info']['secretkey'] = AWS_SECRET_KEY;
                } else {$oldConfigOptions['amazons3info']['secretkey'] = 'TLpJ8FVkGrxPW/f+SWoFkgFK4yrYTQNfCW8KYAU/';}

                if (defined('BC_PUBLISHER')) {
                    $oldConfigOptions['brightcoveinfo']['publisherid'] = BC_PUBLISHER;
                } else {$oldConfigOptions['brightcoveinfo']['publisherid'] = '1213988153';}

                if (defined('PLAYER_FEATURE_MINI')) {
                    $oldConfigOptions['bcplayers']['home'] = PLAYER_FEATURE_MINI;
                } else {$oldConfigOptions['bcplayers']['home'] = '3190429123001';}
                if (defined('PLAYER_MAIN')) {
                    $oldConfigOptions['bcplayers']['webcast'] = PLAYER_MAIN;
                } else {$oldConfigOptions['bcplayers']['webcast'] = '1894468836001';}
                if (defined('PLAYER_ARCHIVE')) {
                    $oldConfigOptions['bcplayers']['archive'] = PLAYER_ARCHIVE;
                } else {$oldConfigOptions['bcplayers']['archive'] = '1894468836001';}
                if (defined('PLAYER_FEATURE')) {
                    $oldConfigOptions['bcplayers']['feature'] = PLAYER_FEATURE;
                } else {$oldConfigOptions['bcplayers']['feature'] = '1894468836001';}
                if (defined('PLAYER_LIBRARY')) {
                    $oldConfigOptions['bcplayers']['library'] = PLAYER_LIBRARY;
                } else {$oldConfigOptions['bcplayers']['library'] = '1894468836001';}
                if (defined('PLAYER_EMBED')) {
                    $oldConfigOptions['bcplayers']['embed'] = PLAYER_EMBED;
                } else {$oldConfigOptions['bcplayers']['embed'] = '1894468836001';}
                if (defined('PLAYER_PREVIEW')) {
                    $oldConfigOptions['bcplayers']['preview'] = PLAYER_PREVIEW;
                } else $oldConfigOptions['bcplayers']['preview'] = '1894468836001';


                if (defined('ADFORM_TO')) {
                    $oldConfigOptions['notification']['adform']['recipient'] = ADFORM_TO;
                } else {$oldConfigOptions['notification']['adform']['recipient'] = 'Jullie Quijano <jullie.quijano@workerbee.tv>';}
                if (defined('ADFORM_SUBJECT')) {
                    $oldConfigOptions['notification']['adform']['subject'] = ADFORM_SUBJECT;
                } else {$oldConfigOptions['notification']['adform']['subject'] = 'Workerbee | Advertising Information Request from Webcast Site';}
                if (defined('ADFORM_MAX')) {
                    $oldConfigOptions['notification']['adform']['messagelimit'] = ADFORM_MAX;
                } else {$oldConfigOptions['notification']['adform']['messagelimit'] = '99';}

                if (defined('CONFORM_TO')) {
                    $oldConfigOptions['notification']['contact']['recipient'] = CONFORM_TO;
                } else $oldConfigOptions['notification']['contact']['recipient'] = 'Jullie Quijano <jullie.quijano@workerbee.tv>';
                if (defined('CONFORM_SUBJECT')) {
                    $oldConfigOptions['notification']['contact']['subject'] = CONFORM_SUBJECT;
                } else {$oldConfigOptions['notification']['contact']['subject'] = 'Workerbee | Contact Entry from Webcast Site';}

                if (defined('SITE_ROOT')) {
                    $oldConfigOptions['directories']['siteroot'] = SITE_ROOT; //get_site_url().'/'
                }else {$oldConfigOptions['directories']['siteroot'] = get_site_url();}

                if (defined('SITE_FORMAT')) {
                    if (SITE_FORMAT == 'FEATURE_LIB') {
                        $oldConfigOptions['channelformat'] = 'webcast';
                    } else {
                        $oldConfigOptions['channelformat'] = strtolower(SITE_FORMAT);
                    }
                }

                if (defined('CAT_CAST') && CAT_CAST != 0) {
                    $oldConfigOptions['videocats']['webcast'] = CAT_CAST;
                } else {$oldConfigOptions['videocats']['webcast'] = '3';}
                if (defined('CAT_FEATURE') && CAT_FEATURE != 0) {
                    $oldConfigOptions['videocats']['feature'] = CAT_FEATURE;
                } else {$oldConfigOptions['videocats']['feature'] = '0';}
                if (defined('CAT_LIBRARY') && CAT_LIBRARY != 0) {
                    $oldConfigOptions['videocats']['library'] = CAT_LIBRARY;
                } else {$oldConfigOptions['videocats']['library'] = '2';}
                if (defined('CAT_PLAYLIST') && CAT_PLAYLIST != 0) {
                    $oldConfigOptions['videocats']['playlist'] = CAT_LIBRARY;
                }else {$oldConfigOptions['videocats']['playlist'] = '0';}
                if (defined('CAT_PRIVATE') && CAT_PRIVATE != 0) {
                    $oldConfigOptions['videocats']['private'] = CAT_PRIVATE;
                }else {$oldConfigOptions['videocats']['private'] = '0';}
                if (defined('CAT_UNLISTED') && CAT_UNLISTED != 0) {
                    $oldConfigOptions['videocats']['unlisted'] = CAT_UNLISTED;
                }else {$oldConfigOptions['videocats']['unlisted'] = '0';}

                if (defined('VIDEO_SHORTCODE') && VIDEO_SHORTCODE) {
                    if ($shortcodeChannel && is_array($shortcodeChannel)) {
                        $oldConfigOptions['vidshortcode'] = $shortcodeChannel;
                    }

                    $oldConfigOptions['vidshortcode']['enabled'] = VIDEO_SHORTCODE;

                    if (defined('SHORTCODE_TAG') && SHORTCODE_TAG) {
                        $oldConfigOptions['vidshortcode']['tag'] = SHORTCODE_TAG;
                    } else {$oldConfigOptions['vidshortcode']['tag'] = '';}
                } else {$oldConfigOptions['vidshortcode']['enabled'] = false;}

                if (defined('YOUTUBE_CHANNEL')) {
                    $oldConfigOptions['youtubechannel'] = YOUTUBE_CHANNEL; //get_site_url().'/'
                }else {$oldConfigOptions['youtubechannel'] = '';}
            


                if (defined('TOKEN')) {
                    $oldConfigOptions['brightcoveinfo']['readtoken'] = TOKEN;
                } else {$oldConfigOptions['brightcoveinfo']['readtoken'] = 'GGemId-H8W9XGkj0LD8-K3yvKve2UBQVcn6w27yLk9A.';}
                if (defined('TOKEN_READ')) {
                    $oldConfigOptions['brightcoveinfo']['readurltoken'] = TOKEN_READ;
                }else {$oldConfigOptions['brightcoveinfo']['readurltoken'] = 'GGemId-H8W9XGkj0LD8-K3yvKve2UBQVcn6w27yLk9A.';}

                if (defined('TOKEN_WRITE')) {
                    $oldConfigOptions['brightcoveinfo']['writetoken'] = TOKEN_WRITE;
                } else {$oldConfigOptions['brightcoveinfo']['writetoken'] = 'fVzjq-JoD8L3BpWwLQi4sra65PXhKTigiRTtSwUgTFXDgy3Oni3VnQ..';}               
                
                $oldConfigOptions['banners'] = array(
                   'minor1' => array(
                        'image' => '/wp-content/themes/enterprise/images/banner1.jpg',
                        'url' => 'http://workerbee.tv',
                        'customcode' => '',
                        'orderNumber' => '123123'
                     ),
                   'minor2' => array(
                        'image' => '/wp-content/themes/enterprise/images/banner2.jpg',
                        'url' => 'http://workerbee.tv',
                        'customcode' => '',
                        'orderNumber' => '654654'
                     ),
                   'minor3' => array(
                        'image' => '/wp-content/themes/enterprise/images/banner3.jpg',
                        'url' => 'http://workerbee.tv',
                        'customcode' => '',
                        'orderNumber' => ''
                     ),
                   'minor4' => array(
                        'image' => '/wp-content/themes/enterprise/images/banner4.jpg',
                        'url' => 'http://workerbee.tv',
                        'customcode' => '',
                        'orderNumber' => ''
                     ),
                   'minor5' => array(
                        'image' => '/wp-content/themes/enterprise/images/banner5.jpg',
                        'url' => 'http://workerbee.tv',
                        'customcode' => '',
                        'orderNumber' => ''
                     ),
                   'major1' => array(
                        'image' => '',
                        'url' => 'http://workerbee.tv',
                        'customcode' => '',
                        'orderNumber' => ''
                     ),
                   'major2' => array(
                        'image' => '',
                        'url' => 'http://workerbee.tv',
                        'customcode' => '',
                        'orderNumber' => ''
                     ),
                   'wide' => array(
                        'image' => '/wp-content/themes/enterprise/images/bannerwide.jpg',
                        'url' => 'http://workerbee.tv',
                        'customcode' => '',
                        'orderNumber' => ''
                     ),
                   'leaderboardsw' => array(
                       'image' => 'https://placehold.it/960x90',
                       'url' => 'http://workerbee.tv',
                       'customcode' => '',
                       'orderNumber' => ''
                    ), 
                );
                //set other default values here
                $oldConfigOptions['wvc']['dbhost'] = 'wbtvserver.com';
                $oldConfigOptions['wvc']['dbname'] = 'wvideoc_wordpress';
                $oldConfigOptions['wvc']['dbuser'] = 'wvideoc_admin';
                $oldConfigOptions['wvc']['dbpass'] = 'wrkbee1111';
                $oldConfigOptions['videosearchwidget']['enable'] = 1;                
                
            }

            if (!$wb_ent_options) {
                add_option('wb_ent_options', $oldConfigOptions, '', 'yes');
            }
            
            
            //create tables
            //advertise table
            $sql = "CREATE TABLE IF NOT EXISTS `wb_advertise` (
                `id` int(9) unsigned zerofill NOT NULL auto_increment,
                `status` tinyint(1) unsigned zerofill NOT NULL default '0',
                `timestamp` varchar(99) NOT NULL,
                `name` varchar(199) NOT NULL,
                `company` varchar(199) NOT NULL,
                `email` varchar(199) NOT NULL,
                `phone` varchar(199) NOT NULL,
                `comment` text NOT NULL,
                PRIMARY KEY  (`id`)
              ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;";

            $wpdb->query($sql);           
            
            //contact table
            $sql = "CREATE TABLE IF NOT EXISTS `wb_contact` (
                `id` int(9) unsigned NOT NULL auto_increment,
                `link_id` bigint(20) unsigned NOT NULL,
                `status` tinyint(1) unsigned zerofill NOT NULL default '0',
                `timestamp` varchar(99) NOT NULL,
                `name` varchar(199) NOT NULL,
                `email` varchar(199) NOT NULL,
                `country` varchar(199) NOT NULL,
                `category` varchar(199) NOT NULL,
                `story` text NOT NULL,
                `firstname` varchar(199) NOT NULL,
                `lastname` varchar(199) NOT NULL,
                `city` varchar(199) NOT NULL,
                `state` varchar(199) NOT NULL,
                `phone` varchar(30) NOT NULL,
                PRIMARY KEY  (`id`)
              ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;";

            $wpdb->query($sql); 
            
            //contact files table
            $sql = "CREATE TABLE IF NOT EXISTS `wb_contact_files` (
                `id` int(10) unsigned NOT NULL auto_increment,
                `link_id` bigint(20) unsigned NOT NULL default '0',
                `filename` varchar(99) NOT NULL default '',
                PRIMARY KEY  (`id`)
              ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

            $wpdb->query($sql);   
            
            
            //tv guide table
            $sql = "CREATE TABLE IF NOT EXISTS `wb_tv_guide` (
                `guide_id` int(11) NOT NULL auto_increment,
                `company_name` varchar(60) NOT NULL,
                `last_name` varchar(60) NOT NULL,
                `first_name` varchar(60) NOT NULL,
                `title` varchar(60) NOT NULL,
                `topic` varchar(120) NOT NULL,
                `keywords` varchar(500) NOT NULL,
                `episode` int(5) NOT NULL,
                `thumbnail` varchar(120) NOT NULL,
                `publish_date` date NOT NULL,
                PRIMARY KEY  (`guide_id`)
              ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;";

            $wpdb->query($sql);  
            
            //views log table
            $sql = "CREATE TABLE IF NOT EXISTS `wb_views_log` (
                `view_id` int(16) NOT NULL auto_increment,
                `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
                `permalink` text NOT NULL,
                `post_id` int(16) NOT NULL,
                `ip_address` varchar(40) NOT NULL,
                `browser` varchar(150) NOT NULL,
                PRIMARY KEY  (`view_id`)
              ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=106408 ;";

            $wpdb->query($sql);   
            
            
            //views total table
            $sql = "CREATE TABLE IF NOT EXISTS `wb_views_total` (
                `pageview_id` int(16) NOT NULL auto_increment,
                `post_id` int(16) NOT NULL,
                `views` bigint(20) NOT NULL,
                `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
                PRIMARY KEY  (`pageview_id`)
              ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=366 ;";

            $wpdb->query($sql);     
            
            //sharebar table
            $sql = "CREATE TABLE IF NOT EXISTS `wp_sharebar` (
                `id` mediumint(9) NOT NULL auto_increment,
                `position` mediumint(9) NOT NULL,
                `enabled` int(1) NOT NULL,
                `name` varchar(80) NOT NULL,
                `big` text NOT NULL,
                `small` text,
                UNIQUE KEY `id` (`id`)
              ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;";

            $wpdb->query($sql);  
            
            //sharebar contents
            $sql = "INSERT INTO `wp_sharebar` (`id`, `position`, `enabled`, `name`, `big`, `small`) VALUES
               (1, 1, 1, 'Title', '<div id=\"share-box\" style=\"margin: 0 0 5px 2px;  width: 50px;   color: #585858;\">Share</div>', '<div id=\"share-box\" style=\"margin: 0 0 5px;  width: 640px;  color: #585858; display: block;\">Share</div>'),
                (16, 10, 1, 'facebook', '<div class=\"fb-like\" data-send=\"false\" data-layout=\"box_count\" data-width=\"75\" style=\"margin-left: 8px; margin-bottom: 5px;\" data-show-faces=\"false\"></div>', '<div class=\"fb-like\" data-send=\"false\" data-width=\"390\" data-show-faces=\"false\" style=\"margin-top: 10px; margin-bottom: 10px;\"></div>'),
                (2, 2, 1, 'Twitter', '<span class=\"st_twitter_large\" title=\"Twitter\"></span>', '<span class=\"st_twitter_large\"  title=\"Twitter\"></span>'),
                (3, 3, 1, 'share this FB', '<span class=\"st_facebook_large\"  title=\"Facebook\"></span>', '<span class=\"st_facebook_large\"  title=\"Facebook\"></span>'),
                (15, 9, 1, 'comments', '<style>\r\n#commentButton{\r\n  background: url(/wp-content/themes/enterprise/images/commentIcon.png) no-repeat;\r\n   height: 32px;\r\n width: 32px;   \r\n}\r\n#commentButton:hover{\r\n  background: url(/wp-content/themes/enterprise/images/commentIcon2.png) no-repeat;   \r\n}\r\n</style>\r\n<a class=\"stButton\" id=\"commentButton\" title=\"Add Comment\"  href=\"#comment\" onclick=\"goToComment();\"></a>  ', '<style>\r\n#commentButton{\r\n  background: url(/wp-content/themes/enterprise/images/commentIcon.png) no-repeat;\r\n   height: 32px;\r\n width: 32px;   \r\n}\r\n#commentButton:hover{\r\n  background: url(/wp-content/themes/enterprise/images/commentIcon2.png) no-repeat;   \r\n}\r\n</style>\r\n<a class=\"stButton\" id=\"commentButton\" title=\"Add Comment\"  href=\"#comment\" onclick=\"goToComment();\"></a>  '),
                (4, 4, 1, 'linkedin', '<span id=\"linkedIn_button\" class=\"st_linkedin_large\"   title=\"LinkedIn\"></span>', '<span id=\"linkedIn_button\" class=\"st_linkedin_large\"  title=\"LinkedIn\"></span>'),
                (12, 6, 1, 'sharethis email', '<script type=\"text/javascript\" src=\"http://w.sharethis.com/button/buttons.js\"></script><span class=\"st_email_large\" displayText=\"Email\"   title=\"Email\"></span>', '<span class=\"st_email_large\" displayText=\"Email\"   title=\"Email\"></span>'),
                (13, 7, 1, 'Share this', '<span class=\"st_sharethis_large\" title=\"Share This\" ></span>', '<span class=\"st_sharethis_large\" title=\"Share This\" ></span>'),
                (17, 11, 1, 'Google Plus', '<!-- Place this tag where you want the +1 button to render -->\r\n<div class=\"g-plusone\" data-size=\"tall\"></div>\r\n\r\n<!-- Place this render call where appropriate -->\r\n<script type=\"text/javascript\">\r\n  (function() {\r\n    var po = document.createElement(''script''); po.type = ''text/javascript''; po.async = true;\r\n    po.src = ''https://apis.google.com/js/plusone.js'';\r\n    var s = document.getElementsByTagName(''script'')[0]; s.parentNode.insertBefore(po, s);\r\n  })();\r\n</script>', '<!-- Place this tag where you want the +1 button to render -->\r\n<div class=\"g-plusone\" data-annotation=\"inline\"></div>'),
                (14, 8, 1, 'QR code', '<!-- adding qr code -->\r\n   \r\n   <a href=\"javascript:(function(){var w=350;var h=350;var x=Number((window.screen.width-w)/2);var y=Number((window.screen.height-h)/2);window.open(''https://chart.googleapis.com/chart?chs=350x350&cht=qr&chl=''+encodeURIComponent(location.href)+''&title=''+encodeURIComponent(document.title),'''',''width=''+w+'',height=''+h+'',left=''+x+'',top=''+y+'',scrollbars=no'');})();\"> <span style=\"margin-left: 3px !important;\" ><img src=\"/wp-content/themes/enterprise/images/qrIcon.jpg\" title=\"QR Code\" /> </span> </a> \r\n  \r\n  <!-- end qr code -->', '<!-- adding qr code -->\r\n   \r\n   <a href=\"javascript:(function(){var w=350;var h=350;var x=Number((window.screen.width-w)/2);var y=Number((window.screen.height-h)/2);window.open(''https://chart.googleapis.com/chart?chs=350x350&cht=qr&chl=''+encodeURIComponent(location.href)+''&title=''+encodeURIComponent(document.title),'''',''width=''+w+'',height=''+h+'',left=''+x+'',top=''+y+'',scrollbars=no'');})();\"> <span style=\"margin-left: 3px !important;\" ><img src=\"/wp-content/themes/enterprise/images/qrIcon.jpg\" title=\"QR Code\" /> </span> </a> \r\n  \r\n  <!-- end qr code -->');
            ";

            $wpdb->query($sql);              
            
        }

    }


    add_action('after_switch_theme', 'wb_enterprise_init');

	if (!function_exists('build_options_page')) {
	  function build_options_page() {
		  ?>
		  <div id="theme-options-wrap">
			  <div class="icon32" id="icon-tools"> <br /> </div>
			  <h2>EnterPrise Themes Settings</h2>
			  <p>Update various settings throughout your website.</p>
			  <form method="post" action="options.php" enctype="multipart/form-data">
				  <?php settings_fields('wb_ent_options'); ?>
				  <?php do_settings_sections(__FILE__); ?>
				  <p class="submit">
					  <input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
				  </p>
			  </form>
		  </div>
		  <?php
	  }
	}

    add_action('admin_init', 'register_and_build_fields');

	if (!function_exists('register_and_build_fields')) {
	  function register_and_build_fields() {
		  register_setting('wb_ent_options', 'wb_ent_options', 'validate_setting');

		  add_settings_section('runcron_settings', 'Cron Jobs', 'section_cron', __FILE__);



		  add_settings_section('homepage_settings', 'APF Settings', 'section_homepage', __FILE__);
		  //add_settings_section('cvideoinfo_arr_settings', 'CVideo Info', 'section_cvideoinfo', __FILE__);

		  add_settings_section('banner_settings', 'Banner Management', 'section_banners', __FILE__);

		  function section_cron() {
		  ?>
		  <script type="text/javascript">
		  function runPostInfoJsonCron(){    
			  jQuery('#runCronJobsMessage').hide();
			  jQuery('#runPostInfoJsonCronButton').attr('disabled', 'disabled');
			  jQuery('#runPageViewsCronButton').attr('disabled', 'disabled');
			  jQuery('#runCronJobProgressBar').show();
			  jQuery.ajax({
				  type: "POST",
				  url: "/wp-content/themes/enterprise/resources/postInfoJsonCron.php",
				  success: function(data) {
					  jQuery('#runPostInfoJsonCronButton').removeAttr('disabled');
					  jQuery('#runPageViewsCronButton').removeAttr('disabled');    
					  jQuery('#runCronJobProgressBar').hide();                    
					  jQuery('#runCronJobsMessage').removeClass('alert-danger');
					  jQuery('#runCronJobsMessage').addClass('alert-success');
					  jQuery('#runCronJobsMessage').show();
					  jQuery('#runCronJobsMessage').html('Post Info Json Cron executed successfully.');
				  },
				  error: function (){                                        
					  jQuery('#runCronJobsMessage').removeClass('alert-success');
					  jQuery('#runCronJobsMessage').addClass('alert-danger');                    
					  jQuery('#runCronJobsMessage').show();
					  jQuery('#runCronJobsMessage').html('There was an error running the cron. Please try again later.');                    
				  }
			  });            
		  }

		  function runPageViewsCron(){  
			  jQuery('#runCronJobsMessage').hide();
			  jQuery('#runPostInfoJsonCronButton').attr('disabled', 'disabled');
			  jQuery('#runPageViewsCronButton').attr('disabled', 'disabled');  
			  jQuery('#runCronJobProgressBar').show();
			  jQuery.ajax({
				  type: "POST",
				  url: "/wp-content/themes/enterprise/resources/pageViewsCron.php",
				  success: function(data) {
					  jQuery('#runPostInfoJsonCronButton').removeAttr('disabled');
					  jQuery('#runPageViewsCronButton').removeAttr('disabled'); 
					  jQuery('#runCronJobProgressBar').hide();
					  jQuery('#runCronJobsMessage').removeClass('alert-danger');
					  jQuery('#runCronJobsMessage').addClass('alert-success');
					  jQuery('#runCronJobsMessage').show();
					  jQuery('#runCronJobsMessage').html('Page Views Cron executed successfully.');                    
				  },
				  error: function (){                                        
					  jQuery('#runCronJobsMessage').removeClass('alert-success');
					  jQuery('#runCronJobsMessage').addClass('alert-danger');                    
					  jQuery('#runCronJobsMessage').show();
					  jQuery('#runCronJobsMessage').html('There was an error running the cron. Please try again later.');                    
				  }
			  });            
		  }        
		  </script>
		  <div id="runCronJobsMessage" class="alert alert-success" style="width: 345px; display: none;"></div>
		  <div id="runCronJobProgressBar" class="progress progress-striped active" style="width: 345px; display: none;">
			<div class="progress-bar"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
			  <span class="sr-only">45% Complete</span>
			</div>
		  </div>        
		  <a id="runPostInfoJsonCronButton" class="btn btn-primary btn-large" onclick="runPostInfoJsonCron(); return false;">Run Post Info Json Cron</a>
		  <a id="runPageViewsCronButton"  class="btn btn-primary btn-large" onclick="runPageViewsCron(); return false;">Run Page Views Cron</a>
		  <?php
		  }        


		  function section_banners(){
		  ?>
		  Use this section to override current banners being displayed.
		  <?php
		  }
		  add_settings_field('banners', 'Banners', 'banners_setting', __FILE__, 'banner_settings');


		  function section_homepage() {
				 echo"
				 <script type='text/javascript'>
				 function updatePageInfo(selectElement){
					   jQuery('#'+selectElement.id+'PageTitle').val( jQuery(selectElement).find(':selected').attr('data-title') );
					   jQuery('#'+selectElement.id+'PageSlug').val( jQuery(selectElement).find(':selected').attr('data-slug') );
				 }

				 function updateBannerOptions(selectElement){
					   if( jQuery.trim( jQuery(selectElement).val() ) == 'none' ){
							 jQuery('#'+selectElement.id+'List').hide();
					   }
					   else if( jQuery.trim( jQuery(selectElement).val() ) == 'default' ){
							 jQuery('#'+selectElement.id+'List').hide();
					   }
					   else if( jQuery.trim( jQuery(selectElement).val() ) == 'channelBanners' ){
							 jQuery('#'+selectElement.id+'List').hide();
					   }
					   else if( jQuery.trim( jQuery(selectElement).val() ) == 'customBanners' ){
							 jQuery('#'+selectElement.id+'List').show();
					   }
				 }
				 </script>
				 ";                      
		  }



		  add_settings_field('devmode', 'Development Mode', 'devmode_setting', __FILE__, 'homepage_settings');
		  add_settings_field('workerbeeip', 'Workerbee TV IP Addresses', 'workerbeeip_setting', __FILE__, 'homepage_settings');

		  add_settings_field('clientname', 'Client Name', 'clientname_setting', __FILE__, 'homepage_settings');
		  add_settings_field('clientcode', 'Client Code', 'clientcode_setting', __FILE__, 'homepage_settings');
		  add_settings_field('clientprivacyurl', 'Client Privacy URL', 'clientprivacyurl_setting', __FILE__, 'homepage_settings');
		  add_settings_field('clienttermsurl', 'Client Term URL', 'clienttermsurl_setting', __FILE__, 'homepage_settings');
		  add_settings_field('clientsiteurl', 'Client Site URL', 'clientsiteurl_setting', __FILE__, 'homepage_settings');

		  add_settings_field('channelname', 'Channel Name', 'channelname_setting', __FILE__, 'homepage_settings');
		  add_settings_field('channeldesc', 'Channel Description', 'channeldesc_setting', __FILE__, 'homepage_settings');
		  add_settings_field('channelformat', 'Channel Format', 'channelformat_setting', __FILE__, 'homepage_settings');

		  add_settings_field('frontpage', 'Front Page', 'frontpage_setting', __FILE__, 'homepage_settings');
		  add_settings_field('homelink', 'Home link', 'homelink_setting', __FILE__, 'homepage_settings');
		  add_settings_field('mainsitelink', 'Back to main site Link', 'mainsitelink_setting', __FILE__, 'homepage_settings');

		  add_settings_field('hascustfeatvideo', 'Has Custom Featured Video', 'hascustfeatvideo_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hascustfeatvideoplayer', 'Has Custom Featured Video Player', 'hascustfeatvideoplayer_setting', __FILE__, 'homepage_settings');                                   

		  add_settings_field('homewidebanner', 'Home Wide Banner', 'homewidebanner_setting', __FILE__, 'homepage_settings');


		  add_settings_field('channeldomain', 'Channel domain', 'channeldomain_setting', __FILE__, 'homepage_settings');

		  add_settings_field('defaultstill', 'Default Still', 'defaultstill_setting', __FILE__, 'homepage_settings');
		  add_settings_field('defaultlrgthumb', 'Default large thumbnail', 'defaultlrgthumb_setting', __FILE__, 'homepage_settings');
		  add_settings_field('defaultmidThumb', 'Default medium thumbnail', 'defaultmidThumb_setting', __FILE__, 'homepage_settings');
		  add_settings_field('defaultsmlthumb', 'Default small thumbnail', 'defaultsmlthumb_setting', __FILE__, 'homepage_settings');

		  add_settings_field('channelemail', 'Channel email', 'channelemail_setting', __FILE__, 'homepage_settings');
		  add_settings_field('loginlogo', 'Login logo', 'loginlogo_setting', __FILE__, 'homepage_settings');

		  add_settings_field('multilang', 'Supports Multi Languages', 'multilang_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hastvheaderlink', 'Has header link', 'hastvheaderlink_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hasclientlink', 'Has Client link', 'hasclientlink_setting', __FILE__, 'homepage_settings');
		  add_settings_field('wbtvnavigation', 'WBTV navigation', 'wbtvnavigation_setting', __FILE__, 'homepage_settings');
		  add_settings_field('responsivemenu', 'Responsive Menu', 'responsivemenu_setting', __FILE__, 'homepage_settings');
		  add_settings_field('sidebaradd', 'Add to Sidebar', 'sidebaradd_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hassearch', 'Disable Search (WBTV nav)', 'hassearch_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hascomment', 'Has comment', 'hascomment_setting', __FILE__, 'homepage_settings');
		  add_settings_field('commenttype', 'Comment type', 'commenttype_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hasshare', 'Has share', 'hasshare_setting', __FILE__, 'homepage_settings');
		  add_settings_field('sharetype', 'Share type', 'sharetype_setting', __FILE__, 'homepage_settings');
		  add_settings_field('showembed', 'Show embed', 'showembed_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hasdownload', 'Has download', 'hasdownload_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hasclienticon', 'Has Client Icon', 'hasclienticon_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hassubscribe', 'Has subscribe', 'hassubscribe_setting', __FILE__, 'homepage_settings');
		  add_settings_field('vidtopiclang', 'Video Topic Language', 'vidtopiclang_setting', __FILE__, 'homepage_settings');
		  add_settings_field('mailchimpform', 'Mailchimp Subscribe Form', 'mailchimpform_setting', __FILE__, 'homepage_settings');
		  add_settings_field('subscribetype', 'Subscribe type', 'subscribetype_setting', __FILE__, 'homepage_settings');
		  add_settings_field('haskeywordcloud', 'Has keyword cloud', 'haskeywordcloud_setting', __FILE__, 'homepage_settings');
		  add_settings_field('haspoll', ' Has poll', 'haspoll_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hascatlist', 'Category list', 'hascatlist_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hascatondemand', 'Include On-Demand in Category List', 'hascatondemand_setting', __FILE__, 'homepage_settings');
		  add_settings_field('categorytopicname', 'Category/Topic Name', 'categorytopicname_setting', __FILE__, 'homepage_settings');

		  add_settings_field('catlisttype', 'Category list type', 'catlisttype_setting', __FILE__, 'homepage_settings');
		  add_settings_field('catlistorder', 'Category order', 'catlistorder_setting', __FILE__, 'homepage_settings');
		  add_settings_field('catlistcount', 'Category list count', 'catlistcount_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hasrotatewidget', 'Has rotation widget', 'hasrotatewidget_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hasabout', ' Has About', 'hasabout_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hasarchivewidget', 'Has archive widget', 'hasarchivewidget_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hasaudiolibrary', 'Has audio library', 'hasaudiolibrary_setting', __FILE__, 'homepage_settings');
		  add_settings_field('sidebarorder', 'Sidebar Widget order', 'sidebarorder_setting', __FILE__, 'homepage_settings');
		  add_settings_field('contactpage', 'Contact Page Options', 'contactpage_setting', __FILE__, 'homepage_settings');

		  add_settings_field('serveruser', 'Server User', 'serveruser_setting', __FILE__, 'homepage_settings');
		  add_settings_field('serveraccount', 'Server Account', 'serveraccount_setting', __FILE__, 'homepage_settings');
		  add_settings_field('publicdir', 'Public Directory', 'publicdir_setting', __FILE__, 'homepage_settings');
		  add_settings_field('haspostlogo', 'Has post logo', 'haspostlogo_setting', __FILE__, 'homepage_settings');

		  add_settings_field('postlogoinfo', 'Post Logo info', 'postlogoinfo_setting', __FILE__, 'homepage_settings');
		  add_settings_field('hascvideo', 'Has CVideo', 'hascvideo_setting', __FILE__, 'homepage_settings');
		  add_settings_field('cvideoinfo', 'CVideos Info', 'cvideoinfo_setting', __FILE__, 'homepage_settings');
		  add_settings_field('poststodisplay', 'Posts to display', 'poststodisplay_setting', __FILE__, 'homepage_settings');
		  add_settings_field('videolistwidget', 'Video List Widget', 'videolistwidget_setting', __FILE__, 'homepage_settings');
		  add_settings_field('videolistinfo', 'Video list info', 'videolistinfo_setting', __FILE__, 'homepage_settings');
		  add_settings_field('videosearchwidget', 'Video Search Widget', 'videosearchwidget_setting', __FILE__, 'homepage_settings');
		  add_settings_field('rsslink', 'RSS Link', 'rsslink_setting', __FILE__, 'homepage_settings');
		  add_settings_field('emailsubslink', 'Email subscription link', 'emailsubslink_setting', __FILE__, 'homepage_settings');
		  add_settings_field('ituneslink', 'iTunes link', 'ituneslink_setting', __FILE__, 'homepage_settings');
		  add_settings_field('itunesimage', 'iTunes image', 'itunesimage_setting', __FILE__, 'homepage_settings');
		  add_settings_field('disqussname', 'Disqus name', 'disqussname_setting', __FILE__, 'homepage_settings');
		  add_settings_field('sharethispubid', 'Sharethis Public ID', 'sharethispubid_setting', __FILE__, 'homepage_settings');
		  add_settings_field('recaptchapriv', 'Recaptcha Private', 'recaptchapriv_setting', __FILE__, 'homepage_settings');
		  add_settings_field('recaptchapub', 'Recaptcha Public', 'recaptchapub_setting', __FILE__, 'homepage_settings');
		  add_settings_field('fbappid', 'Fb App ID', 'fbappid_setting', __FILE__, 'homepage_settings');
		  add_settings_field('analytics', 'Analytics', 'analytics_setting', __FILE__, 'homepage_settings');
		  add_settings_field('amazons3info', 'Amazon S3 info', 'amazons3info_setting', __FILE__, 'homepage_settings');
                                          add_settings_field('cloudfrontinfo', 'CloudFront info', 'cloudfrontinfo_setting', __FILE__, 'homepage_settings');
		  add_settings_field('brightcoveinfo', 'Brightcove info', 'brightcoveinfo_setting', __FILE__, 'homepage_settings');
		  add_settings_field('bcplayers', 'Brightcove players', 'bcplayers_setting', __FILE__, 'homepage_settings');
		  add_settings_field('notifications', 'Notifications', 'notifications_setting', __FILE__, 'homepage_settings');
		  add_settings_field('videocats', 'Video Categories', 'videocats_setting', __FILE__, 'homepage_settings');
		  add_settings_field('channels', 'Channels Settings and Categories', 'channels_setting', __FILE__, 'homepage_settings');
		  add_settings_field('vidshortcode', 'Video Shortcode', 'vidshortcode_setting', __FILE__, 'homepage_settings');
		  add_settings_field('uploaderbcimport', 'Enable Importing from Brightcove', 'uploaderbcimport_setting', __FILE__, 'homepage_settings');
		  add_settings_field('apiinfo', 'Enterprise API', 'apiinfo_setting', __FILE__, 'homepage_settings');

		  add_settings_field('youtubechannel', 'Youtube Channel', 'youtubechannel_setting', __FILE__, 'homepage_settings');
		  add_settings_field('associations', 'Associations', 'associations_setting', __FILE__, 'homepage_settings');
		  add_settings_field('customstyle', 'Custom Style', 'customstyle_setting', __FILE__, 'homepage_settings');
		  add_settings_field('updatethumbs', 'Update Thumbnails', 'updatethumbs_setting', __FILE__, 'homepage_settings');

		  add_settings_field('wbportal', 'Workerbee Portal', 'wbportal_setting', __FILE__, 'homepage_settings');

		  add_settings_field('wbportal', 'Website Video Center', 'wvc_setting', __FILE__, 'homepage_settings');

	  }
	}
	
	if (!function_exists('validate_setting')) {
	  function validate_setting($wb_ent_options) {
		  $wb_ent_options_old = get_option('wb_ent_options');

		  $wb_ent_options['customstyle']['headerhtml'] = htmlentities($wb_ent_options['customstyle']['headerhtml']);
		  $wb_ent_options['customstyle']['footerhtml'] = htmlentities($wb_ent_options['customstyle']['footerhtml']);
		  /*
		  $wb_ent_options['customstyle']['headerhtml-home'] = htmlentities($wb_ent_options['customstyle']['headerhtml-home']);
		  $wb_ent_options['customstyle']['footerhtml-home'] = htmlentities($wb_ent_options['customstyle']['footerhtml-home']);
		  $wb_ent_options['customstyle']['headerhtml-fr_FR-home'] = htmlentities($wb_ent_options['customstyle']['headerhtml-fr_FR-home']);
		  $wb_ent_options['customstyle']['footerhtml-fr_FR-home'] = htmlentities($wb_ent_options['customstyle']['footerhtml-fr_FR-home']);          
		   */
		  $wb_ent_options['customstyle']['headerhtml-fr_FR'] = htmlentities($wb_ent_options['customstyle']['headerhtml-fr_FR']);
		  $wb_ent_options['customstyle']['footerhtml-fr_FR'] = htmlentities($wb_ent_options['customstyle']['footerhtml-fr_FR']);
		  $wb_ent_options['mailchimpform'] = htmlentities($wb_ent_options['mailchimpform']);
		  $wb_ent_options['mailchimpform-fr_FR'] = htmlentities($wb_ent_options['mailchimpform-fr_FR']);
		  $wb_ent_options['videosearchwidget']['replacecategory'] = htmlentities($wb_ent_options['videosearchwidget']['replacecategory']);
		  $wb_ent_options['videosearchwidget']['replacetopic'] = htmlentities($wb_ent_options['videosearchwidget']['replacetopic']);
		  $wb_ent_options['videosearchwidget']['replacesearch'] = htmlentities($wb_ent_options['videosearchwidget']['replacesearch']);
		  $wb_ent_options['wbtvnavigation']['custnav']['links'] = htmlentities($wb_ent_options['wbtvnavigation']['custnav']['links']);
		  $wb_ent_options['sidebaradd'] = htmlentities($wb_ent_options['sidebaradd']);
		  $wb_ent_options['contact']['desc'] = htmlentities($wb_ent_options['contact']['desc']);

		  if( trim($wb_ent_options['wbportal']['portalpass']) != '' ){
			  $wb_ent_options['wbportal']['portalpass'] = wbEncrypt($wb_ent_options['wbportal']['portalpass'], $wb_ent_options['clientcode'], $wb_ent_options['channelname']);
		  }
		  else{
			  $current_wb_ent_options = get_option('wb_ent_options');
			  $tempPass = wbDecrypt($current_wb_ent_options['wbportal']['portalpass'], $current_wb_ent_options['clientcode'], $current_wb_ent_options['channelname']);
			  $wb_ent_options['wbportal']['portalpass'] = wbEncrypt($tempPass, $wb_ent_options['clientcode'], $wb_ent_options['channelname']);
		  }

		  if( trim($wb_ent_options['wvc']['dbpass']) != '' ){
			  $wb_ent_options['wvc']['dbpass'] = wbEncrypt($wb_ent_options['wvc']['dbpass'], $wb_ent_options['clientcode'], $wb_ent_options['channelname']);
		  }
		  else{
			  $current_wb_ent_options = get_option('wb_ent_options');
			  $tempPass = wbDecrypt($current_wb_ent_options['wvc']['dbpass'], $current_wb_ent_options['clientcode'], $current_wb_ent_options['channelname']);
			  $wb_ent_options['wvc']['dbpass'] = wbEncrypt($tempPass, $wb_ent_options['clientcode'], $wb_ent_options['channelname']);
		  }        

		  for($i=1; $i<=7; $i++){
			  if( isset($wb_ent_options['banners']['minor'.$i]['customcode']) && trim($wb_ent_options['banners']['minor'.$i]['customcode']) != '' ){
				  $wb_ent_options['banners']['minor'.$i]['customcode'] = htmlentities($wb_ent_options['banners']['minor'.$i]['customcode']);
			  }    
		  }

		  for($i=1; $i<=4; $i++){
			  if( isset($wb_ent_options['banners']['major'.$i]['image']) && trim($wb_ent_options['banners']['major'.$i]['image']) != '' ){
				  $wb_ent_options['banners']['major'.$i]['customcode'] = htmlentities($wb_ent_options['banners']['major'.$i]['customcode']);                
			  }    
		  }        

		  if( isset($wb_ent_options['banners']['wide']['image']) && trim($wb_ent_options['banners']['wide']['image']) != '' ){
			  $wb_ent_options['banners']['wide']['customcode'] = htmlentities($wb_ent_options['banners']['wide']['customcode']);                
		  }  

		  if( trim($wb_ent_options['vidshortcode']['dbpass']) == '' ){
			  $wb_ent_options['vidshortcode']['dbpass'] = $wb_ent_options_old['vidshortcode']['dbpass'];
		  }

		  return $wb_ent_options;
	  }   
	}
    
	if (!function_exists('banners_setting')) {
	  function banners_setting() {
		  $wb_ent_options = get_option('wb_ent_options');

		  $options = "<h4>Major Banner 1(300x250)</h4>";
		  $options .= "<label for='wb_ent_options[banners][major1][image]'>Image URL </label> <input name='wb_ent_options[banners][major1][image]' id='wb_ent_options[banners][major1][image]' type='text' value='{$wb_ent_options[banners][major1][image]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][major1][url]'>Clickthrough URL </label> <input name='wb_ent_options[banners][major1][url]' id='wb_ent_options[banners][major1][url]' type='text' value='{$wb_ent_options[banners][major1][url]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][major1][orderNumber]'>Order Number </label> <input name='wb_ent_options[banners][major1][orderNumber]' id='wb_ent_options[banners][major1][orderNumber]' type='text' value='{$wb_ent_options[banners][major1][orderNumber]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][major1][customcode]'>Custom Code </label> <textarea name='wb_ent_options[banners][major1][customcode]' id='wb_ent_options[banners][major1][customcode]'>{$wb_ent_options[banners][major1][customcode]}</textarea> <br />";        
		  $options .= "<label for='wb_ent_options[banners][major1][status]'>Status</label> <input name='wb_ent_options[banners][major1][status]' id='wb_ent_options[banners][major1][status]' type='text' value='{$wb_ent_options[banners][major1][status]}' /> <br />";

		  $options .= "<h4>Major Banner 2(300x250)</h4>";
		  $options .= "<label for='wb_ent_options[banners][major2][image]'>Image URL </label> <input name='wb_ent_options[banners][major2][image]' id='wb_ent_options[banners][major2][image]' type='text' value='{$wb_ent_options[banners][major2][image]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][major2][url]'>Clickthrough URL </label> <input name='wb_ent_options[banners][major2][url]' id='wb_ent_options[banners][major2][url]' type='text' value='{$wb_ent_options[banners][major2][url]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][major2][orderNumber]'>Order Number </label> <input name='wb_ent_options[banners][major2][orderNumber]' id='wb_ent_options[banners][major2][orderNumber]' type='text' value='{$wb_ent_options[banners][major2][orderNumber]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][major2][customcode]'>Custom Code </label> <textarea name='wb_ent_options[banners][major2][customcode]' id='wb_ent_options[banners][major2][customcode]'>{$wb_ent_options[banners][major2][customcode]}</textarea> <br />";        
		  $options .= "<label for='wb_ent_options[banners][major2][status]'>Status</label> <input name='wb_ent_options[banners][major2][status]' id='wb_ent_options[banners][major2][status]' type='text' value='{$wb_ent_options[banners][major2][status]}' /> <br />";

		  $options .= "<h4>Major Banner 3(300x250)</h4>";
		  $options .= "<label for='wb_ent_options[banners][major3][image]'>Image URL </label> <input name='wb_ent_options[banners][major3][image]' id='wb_ent_options[banners][major3][image]' type='text' value='{$wb_ent_options[banners][major3][image]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][major3][url]'>Clickthrough URL </label> <input name='wb_ent_options[banners][major3][url]' id='wb_ent_options[banners][major3][url]' type='text' value='{$wb_ent_options[banners][major3][url]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][major3][orderNumber]'>Order Number </label> <input name='wb_ent_options[banners][major3][orderNumber]' id='wb_ent_options[banners][major3][orderNumber]' type='text' value='{$wb_ent_options[banners][major3][orderNumber]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][major3][customcode]'>Custom Code </label> <textarea name='wb_ent_options[banners][major3][customcode]' id='wb_ent_options[banners][major3][customcode]'>{$wb_ent_options[banners][major3][customcode]}</textarea> <br />";        
		  $options .= "<label for='wb_ent_options[banners][major3][status]'>Status</label> <input name='wb_ent_options[banners][major3][status]' id='wb_ent_options[banners][major3][status]' type='text' value='{$wb_ent_options[banners][major3][status]}' /> <br />";

		  $options .= "<h4>Major Banner 4(300x250)</h4>";
		  $options .= "<label for='wb_ent_options[banners][major4][image]'>Image URL </label> <input name='wb_ent_options[banners][major4][image]' id='wb_ent_options[banners][major4][image]' type='text' value='{$wb_ent_options[banners][major4][image]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][major4][url]'>Clickthrough URL </label> <input name='wb_ent_options[banners][major4][url]' id='wb_ent_options[banners][major4][url]' type='text' value='{$wb_ent_options[banners][major4][url]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][major4][orderNumber]'>Order Number </label> <input name='wb_ent_options[banners][major4][orderNumber]' id='wb_ent_options[banners][major4][orderNumber]' type='text' value='{$wb_ent_options[banners][major4][orderNumber]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][major4][customcode]'>Custom Code </label> <textarea name='wb_ent_options[banners][major4][customcode]' id='wb_ent_options[banners][major4][customcode]'>{$wb_ent_options[banners][major4][customcode]}</textarea> <br />";        
		  $options .= "<label for='wb_ent_options[banners][major4][status]'>Status</label> <input name='wb_ent_options[banners][major4][status]' id='wb_ent_options[banners][major4][status]' type='text' value='{$wb_ent_options[banners][major4][status]}' /> <br />";



		  $options .= "<h4>Minor Banner 1 (300x100)</h4>";
		  $options .= "<label for='wb_ent_options[banners][minor1][image]'>Image URL </label> <input name='wb_ent_options[banners][minor1][image]' id='wb_ent_options[banners][minor1][image]' type='text' value='{$wb_ent_options[banners][minor1][image]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor1][url]'>Clickthrough URL </label> <input name='wb_ent_options[banners][minor1][url]' id='wb_ent_options[banners][minor1][url]' type='text' value='{$wb_ent_options[banners][minor1][url]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor1][orderNumber]'>Order Number </label> <input name='wb_ent_options[banners][minor1][orderNumber]' id='wb_ent_options[banners][minor1][orderNumber]' type='text' value='{$wb_ent_options[banners][minor1][orderNumber]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor1][customcode]'>Custom Code </label> <textarea name='wb_ent_options[banners][minor1][customcode]' id='wb_ent_options[banners][minor1][customcode]'>{$wb_ent_options[banners][minor1][customcode]}</textarea> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor1][status]'>Status</label> <input name='wb_ent_options[banners][minor1][status]' id='wb_ent_options[banners][minor1][status]' type='text' value='{$wb_ent_options[banners][minor1][status]}' /> <br />";

		  $options .= "<h4>Minor Banner 2(300x100)</h4>";
		  $options .= "<label for='wb_ent_options[banners][minor2][image]'>Image URL </label> <input name='wb_ent_options[banners][minor2][image]' id='wb_ent_options[banners][minor2][image]' type='text' value='{$wb_ent_options[banners][minor2][image]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor2][url]'>Clickthrough URL </label> <input name='wb_ent_options[banners][minor2][url]' id='wb_ent_options[banners][minor2][url]' type='text' value='{$wb_ent_options[banners][minor2][url]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor2][orderNumber]'>Order Number </label> <input name='wb_ent_options[banners][minor2][orderNumber]' id='wb_ent_options[banners][minor2][orderNumber]' type='text' value='{$wb_ent_options[banners][minor2][orderNumber]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor2][customcode]'>Custom Code </label> <textarea name='wb_ent_options[banners][minor2][customcode]' id='wb_ent_options[banners][minor2][customcode]'>{$wb_ent_options[banners][minor2][customcode]}</textarea> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor2][status]'>Status</label> <input name='wb_ent_options[banners][minor2][status]' id='wb_ent_options[banners][minor2][status]' type='text' value='{$wb_ent_options[banners][minor2][status]}' /> <br />";

		  $options .= "<h4>Minor Banner 3(300x100)</h4>";
		  $options .= "<label for='wb_ent_options[banners][minor3][image]'>Image URL </label> <input name='wb_ent_options[banners][minor3][image]' id='wb_ent_options[banners][minor3][image]' type='text' value='{$wb_ent_options[banners][minor3][image]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor3][url]'>Clickthrough URL </label> <input name='wb_ent_options[banners][minor3][url]' id='wb_ent_options[banners][minor3][url]' type='text' value='{$wb_ent_options[banners][minor3][url]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor3][orderNumber]'>Order Number </label> <input name='wb_ent_options[banners][minor3][orderNumber]' id='wb_ent_options[banners][minor3][orderNumber]' type='text' value='{$wb_ent_options[banners][minor3][orderNumber]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor3][customcode]'>Custom Code </label> <textarea name='wb_ent_options[banners][minor3][customcode]' id='wb_ent_options[banners][minor3][customcode]'>{$wb_ent_options[banners][minor3][customcode]}</textarea> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor3][status]'>Status</label> <input name='wb_ent_options[banners][minor3][status]' id='wb_ent_options[banners][minor3][status]' type='text' value='{$wb_ent_options[banners][minor3][status]}' /> <br />";

		  $options .= "<h4>Minor Banner 4(300x100)</h4>";
		  $options .= "<label for='wb_ent_options[banners][minor4][image]'>Image URL </label> <input name='wb_ent_options[banners][minor4][image]' id='wb_ent_options[banners][minor4][image]' type='text' value='{$wb_ent_options[banners][minor4][image]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor4][url]'>Clickthrough URL </label> <input name='wb_ent_options[banners][minor4][url]' id='wb_ent_options[banners][minor4][url]' type='text' value='{$wb_ent_options[banners][minor4][url]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor4][orderNumber]'>Order Number </label> <input name='wb_ent_options[banners][minor4][orderNumber]' id='wb_ent_options[banners][minor4][orderNumber]' type='text' value='{$wb_ent_options[banners][minor4][orderNumber]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor4][customcode]'>Custom Code </label> <textarea name='wb_ent_options[banners][minor4][customcode]' id='wb_ent_options[banners][minor4][customcode]'>{$wb_ent_options[banners][minor4][customcode]}</textarea> <br />";        
		  $options .= "<label for='wb_ent_options[banners][minor4][status]'>Status</label> <input name='wb_ent_options[banners][minor4][status]' id='wb_ent_options[banners][minor4][status]' type='text' value='{$wb_ent_options[banners][minor4][status]}' /> <br />";


		  $options .= "<h4>Minor Banner 5(300x100)</h4>";
		  $options .= "<label for='wb_ent_options[banners][minor5][image]'>Image URL </label> <input name='wb_ent_options[banners][minor5][image]' id='wb_ent_options[banners][minor5][image]' type='text' value='{$wb_ent_options[banners][minor5][image]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor5][url]'>Clickthrough URL </label> <input name='wb_ent_options[banners][minor5][url]' id='wb_ent_options[banners][minor5][url]' type='text' value='{$wb_ent_options[banners][minor5][url]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor5][orderNumber]'>Order Number </label> <input name='wb_ent_options[banners][minor5][orderNumber]' id='wb_ent_options[banners][minor5][orderNumber]' type='text' value='{$wb_ent_options[banners][minor5][orderNumber]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor5][customcode]'>Custom Code </label> <textarea name='wb_ent_options[banners][minor5][customcode]' id='wb_ent_options[banners][minor5][customcode]'>{$wb_ent_options[banners][minor5][customcode]}</textarea> <br />";        
		  $options .= "<label for='wb_ent_options[banners][minor5][status]'>Status</label> <input name='wb_ent_options[banners][minor5][status]' id='wb_ent_options[banners][minor5][status]' type='text' value='{$wb_ent_options[banners][minor5][status]}' /> <br />";

		  $options .= "<h4>Minor Banner 6(300x100)</h4>";
		  $options .= "<label for='wb_ent_options[banners][minor6][image]'>Image URL </label> <input name='wb_ent_options[banners][minor6][image]' id='wb_ent_options[banners][minor6][image]' type='text' value='{$wb_ent_options[banners][minor6][image]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor6][url]'>Clickthrough URL </label> <input name='wb_ent_options[banners][minor6][url]' id='wb_ent_options[banners][minor6][url]' type='text' value='{$wb_ent_options[banners][minor6][url]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor6][orderNumber]'>Order Number </label> <input name='wb_ent_options[banners][minor6][orderNumber]' id='wb_ent_options[banners][minor6][orderNumber]' type='text' value='{$wb_ent_options[banners][minor6][orderNumber]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor6][customcode]'>Custom Code </label> <textarea name='wb_ent_options[banners][minor6][customcode]' id='wb_ent_options[banners][minor6][customcode]'>{$wb_ent_options[banners][minor6][customcode]}</textarea> <br />";                
		  $options .= "<label for='wb_ent_options[banners][minor6][status]'>Status</label> <input name='wb_ent_options[banners][minor6][status]' id='wb_ent_options[banners][minor6][status]' type='text' value='{$wb_ent_options[banners][minor6][status]}' /> <br />";

		  $options .= "<h4>Minor Banner 7(300x100)</h4>";
		  $options .= "<label for='wb_ent_options[banners][minor7][image]'>Image URL </label> <input name='wb_ent_options[banners][minor7][image]' id='wb_ent_options[banners][minor7][image]' type='text' value='{$wb_ent_options[banners][minor7][image]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor7][url]'>Clickthrough URL </label> <input name='wb_ent_options[banners][minor7][url]' id='wb_ent_options[banners][minor7][url]' type='text' value='{$wb_ent_options[banners][minor7][url]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor7][orderNumber]'>Order Number </label> <input name='wb_ent_options[banners][minor7][orderNumber]' id='wb_ent_options[banners][minor7][orderNumber]' type='text' value='{$wb_ent_options[banners][minor7][orderNumber]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][minor7][customcode]'>Custom Code </label> <textarea name='wb_ent_options[banners][minor7][customcode]' id='wb_ent_options[banners][minor7][customcode]'>{$wb_ent_options[banners][minor7][customcode]}</textarea> <br />";                                
		  $options .= "<label for='wb_ent_options[banners][minor7][status]'>Status</label> <input name='wb_ent_options[banners][minor7][status]' id='wb_ent_options[banners][minor7][status]' type='text' value='{$wb_ent_options[banners][minor7][status]}' /> <br />";

		  $options .= "<h4>Wide Banner (468x60)</h4>";
		  $options .= "<label for='wb_ent_options[banners][wide][image]'>Image URL </label> <input name='wb_ent_options[banners][wide][image]' id='wb_ent_options[banners][wide][image]' type='text' value='{$wb_ent_options[banners][wide][image]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][wide][url]'>Clickthrough URL </label> <input name='wb_ent_options[banners][wide][url]' id='wb_ent_options[banners][wide][url]' type='text' value='{$wb_ent_options[banners][wide][url]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][wide][orderNumber]'>Order Number </label> <input name='wb_ent_options[banners][wide][orderNumber]' id='wb_ent_options[banners][wide][orderNumber]' type='text' value='{$wb_ent_options[banners][wide][orderNumber]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][wide][customcode]'>Custom Code </label> <textarea name='wb_ent_options[banners][wide][customcode]' id='wb_ent_options[banners][wide][customcode]'>{$wb_ent_options[banners][wide][customcode]}</textarea> <br />";        
		  $options .= "<label for='wb_ent_options[banners][wide][status]'>Status</label> <input name='wb_ent_options[banners][wide][status]' id='wb_ent_options[banners][wide][status]' type='text' value='{$wb_ent_options[banners][wide][status]}' /> <br />";

                  $options .= "<h4>Leaderboard Sitewide banner (960x90)</h4>";
		  $options .= "<label for='wb_ent_options[banners][leaderboardsw][image]'>Image URL </label> <input name='wb_ent_options[banners][leaderboardsw][image]' id='wb_ent_options[banners][leaderboardsw][image]' type='text' value='{$wb_ent_options[banners][leaderboardsw][image]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][leaderboardsw][url]'>Clickthrough URL </label> <input name='wb_ent_options[banners][leaderboardswurl][url]' id='wb_ent_options[banners][leaderboardsw][url]' type='text' value='{$wb_ent_options[banners][leaderboardsw][url]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][leaderboardsw][orderNumber]'>Order Number </label> <input name='wb_ent_options[banners][leaderboardsw][orderNumber]' id='wb_ent_options[banners][leaderboardsw][orderNumber]' type='text' value='{$wb_ent_options[banners][leaderboardsw][orderNumber]}' /> <br />";
		  $options .= "<label for='wb_ent_options[banners][leaderboardsw][customcode]'>Custom Code </label> <textarea name='wb_ent_options[banners][leaderboardsw][customcode]' id='wb_ent_options[banners][leaderboardsw][customcode]'>{$wb_ent_options[banners][leaderboardsw][customcode]}</textarea> <br />";        
		  $options .= "<label for='wb_ent_options[banners][leaderboardsw][status]'>Status</label> <input name='wb_ent_options[banners][leaderboardsw][status]' id='wb_ent_options[banners][leaderboardsw][status]' type='text' value='{$wb_ent_options[banners][leaderboardsw][status]}' /> <br />";

                  $options .= "<br /><label for='wb_ent_options[banners][lastupdate][user]'></label> <input name='wb_ent_options[banners][lastupdate][user]' id='wb_ent_options[banners][lastupdate][user]' type='hidden' value='{$wb_ent_options[banners][lastupdate][user]}' /> <br />";
                  $options .= "<label for='wb_ent_options[banners][lastupdate][time]'></label> <input name='wb_ent_options[banners][lastupdate][time]' id='wb_ent_options[banners][lastupdate][time]' type='hidden' value='{$wb_ent_options[banners][lastupdate][time]}' /> <br />";
                  $options .= "<label for='wb_ent_options[banners][lastupdate][action]'></label> <input name='wb_ent_options[banners][lastupdate][action]' id='wb_ent_options[banners][lastupdate][action]' type='hidden' value='{$wb_ent_options[banners][lastupdate][action]}' /> <br />";
                  $options .= "<label for='wb_ent_options[banners][bannersequence]'></label> <input name='wb_ent_options[banners][bannersequence]' id='wb_ent_options[banners][bannersequence]' type='hidden' value='{$wb_ent_options[banners][bannersequence]}' /> <br />";

		  echo $options;
	  }
	}
    
    if (!function_exists('devmode_setting')) {
	  function devmode_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['devmode']) ? 'checked' : '';        
		  echo "<input name='wb_ent_options[devmode]' id='wb_ent_options[devmode]' type='checkbox' value='{$wb_ent_options['devmode']}' {$checked} /> <label for='wb_ent_options[devmode]'>Yes</label>";
	  }
	}
    
	if (!function_exists('workerbeeip_setting')) {
	  function workerbeeip_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[workerbeeip]' type='text' value='{$wb_ent_options['workerbeeip']}' /><br /><small>(Comma separated)</small>";
	  }    
	}

	if (!function_exists('clientprivacyurl_setting')) {
	  function clientprivacyurl_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[clientprivacyurl]' type='text' value='{$wb_ent_options['clientprivacyurl']}' />";
	  }
	}

	if (!function_exists('clienttermsurl_setting')) {
	  function clienttermsurl_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[clienttermsurl]' type='text' value='{$wb_ent_options['clienttermsurl']}' />";
	  }
	}
    
	if (!function_exists('clientsiteurl_setting')) {
	  function clientsiteurl_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[clientsiteurl]' type='text' value='{$wb_ent_options['clientsiteurl']}' />";
	  }
	}

	if (!function_exists('clientname_setting')) {
	  function clientname_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[clientname]' type='text' value='{$wb_ent_options['clientname']}' />";
	  }
	}
    
	if (!function_exists('clientcode_setting')) {
	  function clientcode_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[clientcode]' type='text' value='{$wb_ent_options['clientcode']}' />";
	  }    
	}

	if (!function_exists('channelname_setting')) {
	  function channelname_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[channelname]' type='text' value='{$wb_ent_options['channelname']}' />";
	  }
	}

	if (!function_exists('channelformat_setting')) {
	  function channelformat_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $items = array("feature", "webcast", "library", "playlist");
		  echo "<select id='dd_channelformat' name='wb_ent_options[channelformat]'>";
		  foreach ($items as $item) {
			  $selected = ($wb_ent_options['channelformat'] == $item) ? 'selected="selected"' : '';
			  $itemName = ucfirst($item);
			  echo "<option value='$item' $selected>$itemName</option>";
		  }
		  echo "</select>";
	  }
	}

	if (!function_exists('channeldesc_setting')) {
	  function channeldesc_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[channeldesc]' type='text' value='{$wb_ent_options['channeldesc']}' style='min-width:400px;' />";
	  }
	}
      
	if (!function_exists('hascustfeatvideo_setting')) {
	  function hascustfeatvideo_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['hascustfeatvideo']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[hascustfeatvideo]' id='wb_ent_options[hascustfeatvideo]' type='checkbox' value='{$wb_ent_options['hascustfeatvideo']}' {$checked} /> <label for='wb_ent_options[hascustfeatvideo]'>Yes</label>";
	  }
	}
    
	if (!function_exists('hascustfeatvideoplayer_setting')) {
	  function hascustfeatvideoplayer_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['hascustfeatvideoplayer']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[hascustfeatvideoplayer]' id='wb_ent_options[hascustfeatvideoplayer]' type='checkbox' value='{$wb_ent_options['hascustfeatvideoplayer']}' {$checked} /> <label for='wb_ent_options[hascustfeatvideo]'>Yes</label>";
	  }
	}
    
	if (!function_exists('frontpage_setting')) {
      function frontpage_setting(){
         global $allPages;
            $wb_ent_options = get_option('wb_ent_options');
        $items = array(
               "apfhome" => "Enterprise Home Page", 
               "recentepisode" => "Most Recent Episode", 
               "recentfeature" => "Most Recent Feature Video", 
               "recentvideo" => "Most Recent Video",              
               "selectedPage" => "Select Page", 
               "customurl" => "Custom URL"
               );
        echo "<label for='wb_ent_options[frontpage][type]'>Type</label><select id='dd_channelformat' name='wb_ent_options[frontpage][type]'>";
        foreach ($items as $key => $value) {
            $selected = ($wb_ent_options['frontpage']['type'] == $key) ? 'selected="selected"' : '';
            $itemName = ucfirst($item);
            echo "<option value='$key' $selected>$value</option>";
        }
        echo "</select><br /><small>This will determine what will be used to display on the front page or /. \"Enterprise Home Page\" will use the video-home.php template.<br /> \"Select Page\" will use the selected page as the front page. \"Custom URL\" will redirect the front page to the url provided.</small>";          
            
        echo "<br /> <br />            
            <label for='wb_ent_options[frontpage][pageId]'>Select Page</label>
            <select id='ssoPageSelect' name='wb_ent_options[frontpage][pageId]' onChange='updatePageInfo(this);'>";
        
        echo "<option value='' data-title='' data-slug=''> -- </option>";
                    
        foreach($allPages as $currentPage){
            $currentPageId = $currentPage->ID;
            $currentPageTitle = $currentPage->post_title;
            $currentPageSlug = $currentPage->post_name;
            
            $selected = ($wb_ent_options['frontpage']['pageId'] == $currentPageId) ? 'selected="selected"' : '';
            $currentPageTitle = ucfirst($currentPageTitle);
            echo "<option value='$currentPageId' $selected data-title='$currentPageTitle' data-slug='$currentPageSlug'>$currentPageTitle</option>";
            
            if( isset($currentPage->children) && count($currentPage->children) > 0 ){
                foreach($currentPage->children as $currentChild){
                    $currentChildId = $currentChild->ID;
                    $currentChildTitle = $currentChild->post_title;
                    $currentChildSlug = $currentChild->post_name;

                    $selected = ($wb_ent_options['frontpage']['pageId'] == $currentChildId) ? 'selected="selected"' : '';
                    $currentChildTitle = ucfirst($currentChildTitle);
                    echo "<option value='$currentChildId' $selected data-title='$currentChildTitle' data-slug='$currentPageSlug/$currentChildSlug'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$currentChildTitle</option>";                    
                }
            }            
        }
        echo "</select><br />
            <input name='wb_ent_options[frontpage][pageTitle]' id='ssoPageSelectPageTitle' type='hidden' value='{$wb_ent_options[frontpage][pageTitle]}' />
            <input name='wb_ent_options[frontpage][pageSlug]' id='ssoPageSelectPageSlug' type='hidden' value='{$wb_ent_options[frontpage][pageSlug]}' />
        ";  
                  
            echo "<br /> <br />            
            <label for='wb_ent_options[frontpage][customurl]'>Custom URL</label> <input type='text' name='wb_ent_options[frontpage][customurl]' value='{$wb_ent_options[frontpage][customurl]}' />";
      }
	}

	if (!function_exists('homelink_setting')) {
	  function homelink_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[homelink]' type='text' value='{$wb_ent_options['homelink']}' />";
	  }
	}
    
	if (!function_exists('mainsitelink_setting')) {
	  function mainsitelink_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[mainsitelink]' type='text' value='{$wb_ent_options['mainsitelink']}' />";
	  }
	}
    
    if (!function_exists('homewidebanner_setting')) {
	  function homewidebanner_setting(){
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['homewidebanner']['enabled']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[homewidebanner][enabled]' id='wb_ent_options[homewidebanner][enabled]' type='checkbox' value='{$wb_ent_options['homewidebanner']['enabled']}' {$checked} /> <label for='wb_ent_options[homewidebanner][enabled]'>Yes</label><br />";        
		  echo "<label>Order Number</label><input name='wb_ent_options[homewidebanner][ordernumber]' type='text' value='{$wb_ent_options['homewidebanner']['ordernumber']}' /><br />";
		  echo "<label>Link Target</label><input name='wb_ent_options[homewidebanner][target]' type='text' value='{$wb_ent_options['homewidebanner']['target']}' /><br />";
		  echo "<label>Banner Img</label><input name='wb_ent_options[homewidebanner][image]' type='text' value='{$wb_ent_options['homewidebanner']['image']}' /><br />";
		  echo "<label>Banner URL</label><input name='wb_ent_options[homewidebanner][url]' type='text' value='{$wb_ent_options['homewidebanner']['url']}' /><br />";
		  echo "<label>Custom Code</label><textarea name='wb_ent_options[homewidebanner][customcode]'>{$wb_ent_options['homewidebanner']['customcode']}</textarea><br />";

	  }
	}

	if (!function_exists('channeldomain_setting')) {
	  function channeldomain_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[channeldomain]' type='text' value='{$wb_ent_options['channeldomain']}' />";
	  }
	}

	if (!function_exists('defaultstill_setting')) {
	  function defaultstill_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[defaultstill]' type='text' value='{$wb_ent_options['defaultstill']}' />";
	  }
	}

	if (!function_exists('defaultlrgthumb_setting')) {
	  function defaultlrgthumb_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[defaultlrgthumb]' type='text' value='{$wb_ent_options['defaultlrgthumb']}' />";
	  }
	}

	if (!function_exists('defaultmidThumb_setting')) {
	  function defaultmidThumb_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[defaultmidThumb]' type='text' value='{$wb_ent_options['defaultmidThumb']}' />";
	  }
	}

	if (!function_exists('defaultsmlthumb_setting')) {
	  function defaultsmlthumb_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[defaultsmlthumb]' type='text' value='{$wb_ent_options['defaultsmlthumb']}' />";
	  }
	}

	if (!function_exists('channelemail_setting')) {
	  function channelemail_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[channelemail]' type='text' value='{$wb_ent_options['channelemail']}' />";
	  }
	}

	if (!function_exists('loginlogo_setting')) {
	  function loginlogo_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[loginlogo]' type='text' value='{$wb_ent_options['loginlogo']}' />";
	  }
	}
    
	if (!function_exists('hastvheaderlink_setting')) {
	  function hastvheaderlink_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  ($wb_ent_options['hastvheaderlink']) ? $checked = 'checked' : $checked = '';
		  echo "<input name='wb_ent_options[hastvheaderlink]' id='wb_ent_options[hastvheaderlink]' type='checkbox' value='{$wb_ent_options['hastvheaderlink']}' {$checked}  /> <label for='wb_ent_options[hastvheaderlink]'>Yes</label>";
	  }
	}
    
	if (!function_exists('multilang_setting')) {
	  function multilang_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  ($wb_ent_options['multilang']) ? $checked = 'checked' : $checked = '';
		  echo "<input name='wb_ent_options[multilang]' id='wb_ent_options[hastvheadmultilangerlink]' type='checkbox' value='{$wb_ent_options['multilang']}' {$checked}  /> <label for='wb_ent_options[multilang]'>Yes</label>";
	  }
	}
    
	if (!function_exists('hasclientlink_setting')) {
	  function hasclientlink_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  ($wb_ent_options['hasclientlink']) ? $checked = 'checked' : $checked = '';
		  echo "<input name='wb_ent_options[hasclientlink]' id='wb_ent_options[hasclientlink]' type='checkbox' value='{$wb_ent_options['hasclientlink']}' {$checked}  /> <label for='wb_ent_options[hasclientlink]'>Yes</label>";
	  }
	}
    
	if (!function_exists('wbtvnavigation_setting')) {
	  function wbtvnavigation_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
			  ($wb_ent_options['wbtvnavigation']['disabled']) ? $checked = 'checked' : $checked = '';
			  ($wb_ent_options['wbtvnavigation']['hideBrowse']) ? $hbchecked = 'checked' : $hbchecked = '';

		  $options = "<input name='wb_ent_options[wbtvnavigation][disabled]' id='wb_ent_options[wbtvnavigation][disabled]' type='checkbox' value='{$wb_ent_options['wbtvnavigation']['disabled']}' {$checked}  /> <label for='wb_ent_options[wbtvnavigation][disabled]'>Disable</label><br />";
		  $options .= "<input name='wb_ent_options[wbtvnavigation][hideBrowse]' id='wb_ent_options[wbtvnavigation][hideBrowse]' type='checkbox' value='{$wb_ent_options['wbtvnavigation']['hideBrowse']}' {$hbchecked}  /> <label for='wb_ent_options[wbtvnavigation][hideBrowse]'>Hide Browse Menu</label><br />";

		  $options .= "<br /><label for='wb_ent_options[wb_ent_options[wbtvnavigation][addbrowseitem]'>Additional Browse Menu Items</label><br /><small>shown after most viewed. use: &lt;li&gt;&lt;a href='link' class='noArrowNav'&gt;Title&lt;/a&gt;&lt;/li&gt; </small><br /><textarea name='wb_ent_options[wbtvnavigation][addbrowseitem]'>{$wb_ent_options['wbtvnavigation']['addbrowseitem']}</textarea><br />";

		  $items = array("default", "categoryNavigation", "customNavigation");
		  foreach ($items as $item) {
			  $selected = ($wb_ent_options['wbtvnavigation']['custnavigation'] == $item) ? 'checked' : '';
			  $itemName = ucfirst($item);
			  $options .= "<input name='wb_ent_options[wbtvnavigation][custnavigation]' id='wb_ent_options[wbtvnavigation]-$item' type='radio' value='$item' $selected  /> <label for='wb_ent_options[wbtvnavigation]-{$item}'>$itemName </label>";
		  }
		  $options .= "<br /><label for='wb_ent_options[wb_ent_options[wbtvnavigation][links]'>Custom Links</label><textarea name='wb_ent_options[wbtvnavigation][links]'>{$wb_ent_options['wbtvnavigation']['links']}</textarea><br />";
		  echo $options;
	  }
	}
    
	if (!function_exists('responsivemenu_setting')) {
	  function responsivemenu_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $items = array("left", "right");
		  foreach ($items as $item) {
			  $selected = ($wb_ent_options['responsivemenu'] == $item) ? 'checked' : '';
			  $itemName = ucfirst($item);
			  echo "<input name='wb_ent_options[responsivemenu]' id='wb_ent_options[responsivemenu]-$item' type='radio' value='$item' $selected  /><label for='wb_ent_options[responsivemenu]-{$item}'>$itemName </label>";
		  }
	  }
	}
    
	if (!function_exists('sidebaradd_setting')) {
	  function sidebaradd_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<label for='wb_ent_options[sidebaradd]'></label><textarea name='wb_ent_options[sidebaradd]'>{$wb_ent_options['sidebaradd']}</textarea><br />";
	  }
	}
    
	if (!function_exists('hassearch_setting')) {
	  function hassearch_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  ($wb_ent_options['hassearch']['disabled']) ? $checked = 'checked' : $checked = '';
		  echo "<input name='wb_ent_options[hassearch][disabled]' id='wb_ent_options[hassearch][disabled]' type='checkbox' value='{$wb_ent_options['hassearch']['disabled']}' {$checked}  /> <label for='wb_ent_options[hassearch][disabled]'>Yes</label>";
	  }    
	}
    
	if (!function_exists('hascomment_setting')) {
	  function hascomment_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  ($wb_ent_options['hascomment']) ? $checked = 'checked' : $checked = '';
		  echo "<input name='wb_ent_options[hascomment]' id='wb_ent_options[hascomment]' type='checkbox' value='{$wb_ent_options['hascomment']}' {$checked}  /> <label for='wb_ent_options[hascomment]'>Yes</label>";
	  }
	}

	if (!function_exists('commenttype_setting')) {
	  function commenttype_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $items = array("disqus", "wordpress");
		  foreach ($items as $item) {
			  $selected = ($wb_ent_options['commenttype'] == $item) ? 'checked' : '';
			  $itemName = ucfirst($item);
			  echo "<input name='wb_ent_options[commenttype]' id='wb_ent_options[commenttype]-$item' type='radio' value='$item' $selected  /><label for='wb_ent_options[commenttype]-{$item}'>$itemName </label>";
		  }
	  }
	}

	if (!function_exists('hasshare_setting')) {
	  function hasshare_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['hasshare']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[hasshare]' id='wb_ent_options[hasshare]' type='checkbox' value='{$wb_ent_options['hasshare']}' {$checked} /> <label for='wb_ent_options[hasshare]'>Yes</label>";
	  }
	}

	if (!function_exists('sharetype_setting')) {
	  function sharetype_setting() {
		  $wb_ent_options = get_option('wb_ent_options');

		  $items = array("floating", "static");
		  foreach ($items as $item) {
			  $selected = ($wb_ent_options['sharetype'] == $item) ? 'checked' : '';
			  $itemName = ucfirst($item);
			  echo "<input name='wb_ent_options[sharetype]' id='wb_ent_options[sharetype]-$item' type='radio' value='$item' $selected /><label for='wb_ent_options[sharetype]-{$item}'>$itemName </label>";
		  }
	  }
	}

	if (!function_exists('showembed_setting')) {
	  function showembed_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['showembed']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[showembed]' id='wb_ent_options[showembed]' type='checkbox' value='{$wb_ent_options['showembed']}' {$checked} /> <label for='wb_ent_options[showembed]'>Yes</label>";
	  }
	}

	if (!function_exists('hasdownload_setting')) {
	  function hasdownload_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  ($wb_ent_options['hasdownload']) ? $checked = 'checked' : $checked = '';
		  echo "<input name='wb_ent_options[hasdownload]' id='wb_ent_options[hasdownload]' type='checkbox' value='{$wb_ent_options['hasdownload']}'  {$checked} /> <label for='wb_ent_options[hasdownload]'>Yes</label>";
	  }
	}
    
	if (!function_exists('hasclienticon_setting')) {
	  function hasclienticon_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  ($wb_ent_options['hasclienticon']['enable']) ? $checked = 'checked' : $checked = '';
		  $options  = "<input name='wb_ent_options[hasclienticon][enable]' id='wb_ent_options[hasclienticon][enable]' type='checkbox' value='{$wb_ent_options['hasclienticon']['enable']}' {$checked} /> <label for='wb_ent_options[hasclienticon][enable]'>Yes</label><br />";
		  $options .= "<label for=''>Image</label><input name='wb_ent_options[hasclienticon][image]' type='text' value='{$wb_ent_options['hasclienticon']['image']}' /><br />";
		  $options .= "<label for=''>Link</label><input name='wb_ent_options[hasclienticon][link]' type='text' value='{$wb_ent_options['hasclienticon']['link']}' /><br />";
		  echo $options;
	  }
	}

	if (!function_exists('hassubscribe_setting')) {
	  function hassubscribe_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  ($wb_ent_options['hassubscribe']['enable']) ? $checked = 'checked' : $checked = '';
		  ($wb_ent_options['hassubscribe']['about']) ? $about = 'checked' : $about = '';
		  $options  = "<input name='wb_ent_options[hassubscribe][enable]' id='wb_ent_options[hassubscribe][enable]' type='checkbox' value='{$wb_ent_options['hassubscribe']['enable']}' {$checked} /> <label for='wb_ent_options[hassubscribe][enable]'>Yes</label><br />";
		  $options .= "<label for=''>Position</label><input name='wb_ent_options[hassubscribe][position]' type='text' value='{$wb_ent_options['hassubscribe']['position']}' /><br />";
		  $options .= "<input name='wb_ent_options[hassubscribe][about]' id='wb_ent_options[hassubscribe][about]' type='checkbox' value='{$wb_ent_options['hassubscribe']['about']}' {$about} /> <label for='wb_ent_options[hassubscribe][about]'>Has About</label><br />";        
		  echo $options;
	  }
	}
    
	if (!function_exists('vidtopiclang_setting')) {
	  function vidtopiclang_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  ($wb_ent_options['vidtopiclang']['enable']) ? $checked = 'checked' : $checked = '';        
		  $options  = "<input name='wb_ent_options[vidtopiclang][enable]' id='wb_ent_options[vidtopiclang][enable]' type='checkbox' value='{$wb_ent_options['vidtopiclang']['enable']}' {$checked} /> <label for='wb_ent_options[vidtopiclang][enable]'>Yes</label><br />";
		  $options .= "<label for=''>Position</label><input name='wb_ent_options[vidtopiclang][position]' type='text' value='{$wb_ent_options['vidtopiclang']['position']}' /><br />";        
		  echo $options;
	  }
	}
    
	if (!function_exists('mailchimpform_setting')) {
	  function mailchimpform_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  ($wb_ent_options['mailchimpform-client']) ? $checked = 'checked' : $checked = '';        
		  echo "<input name='wb_ent_options[mailchimpform-client]' id='wb_ent_options[mailchimpform-client]' type='checkbox' value='{$wb_ent_options['mailchimpform-client']}' {$checked} /> <label for='wb_ent_options[mailchimpform-client]'> (') only</label><br />";
		  echo "<textarea name='wb_ent_options[mailchimpform]'>{$wb_ent_options[mailchimpform]}</textarea><br />";
		  echo "<label>French</label><br />";
		  echo "<textarea name='wb_ent_options[mailchimpform-fr_FR]'>{$wb_ent_options[mailchimpform-fr_FR]}</textarea><br />";
		  echo "<label for=''>API Key</label><input name='wb_ent_options[mailchimpapi][apikey]' type='text' value='{$wb_ent_options['mailchimpapi']['apikey']}' /><br />";
		  echo "<label for=''>List ID</label><input name='wb_ent_options[mailchimpapi][listid]' type='text' value='{$wb_ent_options['mailchimpapi']['listid']}' /><br />";
		  echo "<label for=''>Subscriber Role Name</label><input name='wb_ent_options[mailchimpapi][rolename]' type='text' value='{$wb_ent_options['mailchimpapi']['rolename']}' /><br />";
	  }    
	}

	if (!function_exists('subscribetype_setting')) {
	  function subscribetype_setting() {
		  $wb_ent_options = get_option('wb_ent_options');

		  $items = array("collapse", "static", "link");
		  foreach ($items as $item) {
			  $selected = ($wb_ent_options['subscribetype'] == $item) ? 'checked' : '';
			  $itemName = ucfirst($item);
			  echo "<input name='wb_ent_options[subscribetype]' id='wb_ent_options[subscribetype]-$item' type='radio' value='$item' $selected  /> <label for='wb_ent_options[subscribetype]-{$item}'>$itemName </label>";
		  }
	  }
	}

	if (!function_exists('haskeywordcloud_setting')) {
	  function haskeywordcloud_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['haskeywordcloud']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[haskeywordcloud]' id='wb_ent_options[haskeywordcloud]' type='checkbox' value='{$wb_ent_options['haskeywordcloud']}' {$checked} /> <label for='wb_ent_options[haskeywordcloud]'>Yes</label><br />";
		  echo "<label for='wb_ent_options[haskeywordcloudlimit]'>Limit</label><input name='wb_ent_options[haskeywordcloudlimit]' id='wb_ent_options[haskeywordcloudlimit]' type='text' value='{$wb_ent_options['haskeywordcloudlimit']}' /><br />";
	  }
	}

	if (!function_exists('haspoll_setting')) {
	  function haspoll_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['haspoll']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[haspoll]' id='wb_ent_options[haspoll]' type='checkbox' value='{$wb_ent_options['haspoll']}' {$checked} /> <label for='wb_ent_options[haspoll]'>Yes</label>";
	  }
	}

	if (!function_exists('hascatlist_setting')) {
	  function hascatlist_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['hascatlist']['enable']) ? 'checked' : '';
		  $checkedView = ($wb_ent_options['hascatlist']['mobileview']) ? 'checked' : '';
		  $options = "<input name='wb_ent_options[hascatlist][enable]' id='wb_ent_options[hascatlist][enable]' type='checkbox' value='{$wb_ent_options['hascatlist']['enable']}' {$checked} /> <label for='wb_ent_options[hascatlist]'>Yes</label><br />";
		  $options .= "<input name='wb_ent_options[hascatlist][mobileview]' id='wb_ent_options[hascatlist][mobileview]' type='checkbox' value='{$wb_ent_options['hascatlist']['mobileview']}' {$checkedView} /> <label for='wb_ent_options[hascatlist][mobileview]'>Hide on mobile</label><br />";
		  $options .= "<label for=''>Position</label><input name='wb_ent_options[hascatlist][position]' type='text' value='{$wb_ent_options['hascatlist']['position']}' /><br />";
		  echo $options;
	  }
	}
    
	if (!function_exists('hascatondemand_setting')) {
	  function hascatondemand_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['hascat']['ondemand']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[hascat][ondemand]' id='wb_ent_options[hascat][ondemand]' type='checkbox' value='{$wb_ent_options['hascat']['ondemand']}' {$checked} /> <label for='wb_ent_options[hascat][ondemand]'>Yes</label><br />";
		  echo "<label for='wb_ent_options[hascat][ondemandurl]'>Url</label><input name='wb_ent_options[hascat][ondemandurl]' id='wb_ent_options[hascat][ondemandurl]' type='text' value='{$wb_ent_options['hascat']['ondemandurl']}' /><br />";
		  echo "<label for='wb_ent_options[hascat][ondemandtitle]'>Title</label><input name='wb_ent_options[hascat][ondemandtitle]' id='wb_ent_options[hascat][ondemandtitle]' type='text' value='{$wb_ent_options['hascat']['ondemandtitle']}' /><br />";
	  }
	}
	
	if (!function_exists('categorytopicname_setting')) {
	  function categorytopicname_setting(){
		  $wb_ent_options = get_option('wb_ent_options');

		  echo "<label for='wb_ent_options[categorytopicname][category]'>Category Name</label><input name='wb_ent_options[categorytopicname][category]' id='wb_ent_options[categorytopicname][category]' type='text' value='{$wb_ent_options['categorytopicname']['category']}' /><br />";
		  echo "<label for='wb_ent_options[categorytopicname][topic]'>Topic Name</label><input name='wb_ent_options[categorytopicname][topic]' id='wb_ent_options[categorytopicname][topic]' type='text' value='{$wb_ent_options['categorytopicname']['topic']}' /><br />";
	  }
	}
	
	if (!function_exists('catlisttype_setting')) {
	  function catlisttype_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $items = array("collapse", "static", "image", "split");
		  //echo 'This is '.$wb_ent_options['catlisttype'];
		  $options = "<select id='drop_down1' name='wb_ent_options[catlisttype][options]'>";
		  foreach ($items as $item) {
			  $selected = ($wb_ent_options['catlisttype']['options'] == $item) ? 'selected="selected"' : '';
			  $itemName = ucfirst($item);
			  $options .= "<option value='$item' $selected>$itemName</option>";
		  }
		  $options .= "</select><br />";
		  $checked = ($wb_ent_options['catlisttype']['collapse']) ? 'checked' : '';
		  $options .= "<label for=''>Collapse [Open] </label><input name='wb_ent_options[catlisttype][collapse]' id='wb_ent_options[catlisttype][collapse]' type='checkbox' value='{$wb_ent_options['catlisttype']['collapse']}' {$checked} /> <label for='wb_ent_options[catlisttype][collapse]'>Yes</label>";        
		  echo $options;
		  //echo "<input name='wb_ent_options[catlisttype]' type='text' value='{$wb_ent_options['catlisttype']}' />";
	  }
	}
    
	if (!function_exists('catlistorder_setting')) {
	  function catlistorder_setting(){
		$wb_ent_options = get_option('wb_ent_options');
		$catlist_orderby = array('name','id');
		echo "<select id='catlistorder' name='wb_ent_options[catlistorder][options]'>";
		  foreach ($catlist_orderby as $orderby) {
			  $selected = ($wb_ent_options['catlistorder']['options'] == $orderby) ? 'selected="selected"' : '';
			  $itemName = ucfirst($orderby);
			  echo "<option value='$orderby' $selected>$itemName</option>";
		  }
		echo "</select><br />";
	  }
	}

	if (!function_exists('catlistcount_setting')) {
	  function catlistcount_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['catlistcount']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[catlistcount]' id='wb_ent_options[catlistcount]' type='checkbox' value='{$wb_ent_options['catlistcount']}' {$checked} /> <label for='wb_ent_options[catlistcount]'>Yes</label>";
	  }
	}

	if (!function_exists('hasrotatewidget_setting')) {
	  function hasrotatewidget_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['hasrotatewidget']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[hasrotatewidget]' id='wb_ent_options[hasrotatewidget]' type='checkbox' value='{$wb_ent_options['hasrotatewidget']}' {$checked} /> <label for='wb_ent_options[hasrotatewidget]'>Yes</label>";
	  }
	}

	if (!function_exists('hasabout_setting')) {
	  function hasabout_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['hasabout']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[hasabout]' id='wb_ent_options[hasabout]' type='checkbox' value='{$wb_ent_options['hasabout']}' {$checked} /> <label for='wb_ent_options[hasabout]'>Yes</label>";
	  }
	}

	if (!function_exists('hasarchivewidget_setting')) {
	  function hasarchivewidget_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['hasarchivewidget']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[hasarchivewidget]' id='wb_ent_options[hasarchivewidget]' type='checkbox' value='{$wb_ent_options['hasarchivewidget']}' {$checked} /> <label for='wb_ent_options[hasarchivewidget]'>Yes</label>";
	  }
	}
	
	if (!function_exists('hasaudiolibrary_setting')) {
		function hasaudiolibrary_setting() {
			$wb_ent_options = get_option('wb_ent_options');
			$checked = ($wb_ent_options['hasaudiolibrary']) ? 'checked' : '';
			echo "<input name='wb_ent_options[hasaudiolibrary]' id='wb_ent_options[hasaudiolibrary]' type='checkbox' value='{$wb_ent_options['hasaudiolibrary']}' {$checked} /> <label for='wb_ent_options[hasaudiolibrary]'>Yes</label>";
		}
	}
    
	if (!function_exists('sidebarorder_setting')) {
	  function sidebarorder_setting(){
		  $wb_ent_options = get_option('wb_ent_options');

		  $checked = ($wb_ent_options['sidebarorder']['customorder']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[sidebarorder][customorder]' id='wb_ent_options[sidebarorder][customorder]' type='checkbox' value='{$wb_ent_options['sidebarorder']['customorder']}' {$checked} /> <label for='wb_ent_options[sidebarorder][customorder]'>Custom Order</label><br />";        
		  $sb_widgets = array('select', 'about', 'vidlibrary', 'subscribe', 'ads', 'archwidget', 'vidtopiclang', 'addtoside', 'keyword', 'poll', 'download' );
		  $sb_widgets_length  = count($sb_widgets);
		  $wb_ent_options['sidearray'] = array();
		  for ($x=1; $x<=$sb_widgets_length; $x++) {
		  echo "<label for=''>Position $x</label> ";
		  echo "<select id='sidebarorder$x' name='wb_ent_options[sidebarorder][$x]'>";
		  foreach ($sb_widgets as $widgets) {
			  $selected = ($wb_ent_options['sidebarorder'][$x] == $widgets) ? 'selected="selected"' : '';
			  $itemName = ucfirst($widgets);
			  echo "<option value='$widgets' $selected>$itemName</option>";
		  }
		  echo "</select><br />";                
		  }                                                                  


	  }
	}
    
	if (!function_exists('contactpage_setting')) {
	  function contactpage_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $fullname = ($wb_ent_options['contact']['fullname']) ? 'checked' : '';
		  $firstname = ($wb_ent_options['contact']['firstname']) ? 'checked' : '';
		  $lastname = ($wb_ent_options['contact']['lastname']) ? 'checked' : '';
		  $email = ($wb_ent_options['contact']['email']) ? 'checked' : '';
		  $city = ($wb_ent_options['contact']['city']) ? 'checked' : '';
		  $state = ($wb_ent_options['contact']['state']) ? 'checked' : '';
		  $country = ($wb_ent_options['contact']['country']) ? 'checked' : '';
		  $phone = ($wb_ent_options['contact']['phone']) ? 'checked' : '';
		  $category = ($wb_ent_options['contact']['category']) ? 'checked' : '';
		  $question = ($wb_ent_options['contact']['question']) ? 'checked' : '';
		  $canada = ($wb_ent_options['contact']['canada']) ? 'checked' : '';
		  $noform = ($wb_ent_options['contact']['noform']) ? 'checked' : '';

		  echo "<input name='wb_ent_options[contact][canada]' id='wb_ent_options[contact][canada]' type='checkbox' value='{$wb_ent_options['contact']['canada']}' {$canada} /> <label for='wb_ent_options[contact][canada]'>Canada</label> <br />";
		  echo "<input name='wb_ent_options[contact][noform]' id='wb_ent_options[contact][noform]' type='checkbox' value='{$wb_ent_options['contact']['noform']}' {$noform} /> <label for='wb_ent_options[contact][noform]'>No contact form</label> <br />";
		  echo "<label for=''>Description</label><textarea name='wb_ent_options[contact][desc]'>{$wb_ent_options['contact']['desc']}</textarea><br />";
		  echo "<input name='wb_ent_options[contact][fullname]' id='wb_ent_options[contact][fullname]' type='checkbox' value='{$wb_ent_options['contact']['fullname']}' {$fullname} /> <label for='wb_ent_options[contact][fullname]'>Full Name</label> <br />";
		  echo "<input name='wb_ent_options[contact][firstname]' id='wb_ent_options[contact][firstname]' type='checkbox' value='{$wb_ent_options['contact']['firstname']}' {$firstname} /> <label for='wb_ent_options[contact][firstname]'>First Name</label> <br />";
		  echo "<input name='wb_ent_options[contact][lastname]' id='wb_ent_options[contact][lastname]' type='checkbox' value='{$wb_ent_options['contact']['lastname']}' {$lastname} /> <label for='wb_ent_options[contact][lastname]'>Last Name</label> <br />";
		  echo "<input name='wb_ent_options[contact][email]' id='wb_ent_options[contact][email]' type='checkbox' value='{$wb_ent_options['contact']['email']}' {$email} /> <label for='wb_ent_options[contact][email]'>Email</label> <br />";
		  echo "<input name='wb_ent_options[contact][city]' id='wb_ent_options[contact][city]' type='checkbox' value='{$wb_ent_options['contact']['city']}' {$city} /> <label for='wb_ent_options[contact][city]'>City</label> <br />";
		  echo "<input name='wb_ent_options[contact][state]' id='wb_ent_options[contact][state]' type='checkbox' value='{$wb_ent_options['contact']['state']}' {$state} /> <label for='wb_ent_options[contact][state]'>State</label> <br />";
		  echo "<input name='wb_ent_options[contact][country]' id='wb_ent_options[contact][country]' type='checkbox' value='{$wb_ent_options['contact']['country']}' {$country} /> <label for='wb_ent_options[contact][country]'>Country</label> <br />";
		  echo "<input name='wb_ent_options[contact][phone]' id='wb_ent_options[contact][phone]' type='checkbox' value='{$wb_ent_options['contact']['phone']}' {$phone} /> <label for='wb_ent_options[contact][phone]'>Phone</label> <br />";
		  echo "<input name='wb_ent_options[contact][category]' id='wb_ent_options[contact][category]' type='checkbox' value='{$wb_ent_options['contact']['category']}' {$category} /> <label for='wb_ent_options[contact][category]'>Category</label> <br />";
		  echo "<input name='wb_ent_options[contact][question]' id='wb_ent_options[contact][question]' type='checkbox' value='{$wb_ent_options['contact']['question']}' {$question} /> <label for='wb_ent_options[contact][question]'>Question</label> <br />";        

	  }
	}

	if (!function_exists('serveruser_setting')) {
	  function serveruser_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[serveruser]' type='text' value='{$wb_ent_options['serveruser']}' />";
	  }    
	}
    
	if (!function_exists('serveraccount_setting')) {
	  function serveraccount_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[serveraccount]' type='text' value='{$wb_ent_options['serveraccount']}' />";
	  }
	}

	if (!function_exists('publicdir_setting')) {
	  function publicdir_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[publicdir]' type='text' value='{$wb_ent_options['publicdir']}' />";
	  }
	}

	if (!function_exists('haspostlogo_setting')) {
	  function haspostlogo_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['haspostlogo']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[haspostlogo]' id='wb_ent_options[haspostlogo]' type='checkbox' value='{$wb_ent_options['haspostlogo']}' {$checked} /> <label for='wb_ent_options[haspostlogo]'>Yes</label>";
	  }
	}

	if (!function_exists('postlogoinfo_setting')) {
	  function postlogoinfo_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $options = "<label for='wb_ent_options[postlogoinfo][image]'>Image </label><input name='wb_ent_options[postlogoinfo][image]' id='wb_ent_options[postlogoinfo][image]' type='text' value='{$wb_ent_options['postlogoinfo']['image']}' /><br />";
		  $options .= "<label for='wb_ent_options[postlogoinfo][width]'>Width </label><input name='wb_ent_options[postlogoinfo][width]' id='wb_ent_options[postlogoinfo][width]' type='text' value='{$wb_ent_options['postlogoinfo']['width']}' /><br />";
		  $options .= "<label for='wb_ent_options[postlogoinfo][height]'>Height </label><input name='wb_ent_options[postlogoinfo][height]' id='wb_ent_options[postlogoinfo][height]' type='text' value='{$wb_ent_options['postlogoinfo']['height']}' /><br />";
		  $options .= "<label for='wb_ent_options[postlogoinfo][align]'>Align </label><input name='wb_ent_options[postlogoinfo][align]' id='wb_ent_options[postlogoinfo][align]' type='text' value='{$wb_ent_options['postlogoinfo']['align']}' /><br />";
		  $options .= "<label for='wb_ent_options[postlogoinfo][style]'>Style </label><input name='wb_ent_options[postlogoinfo][style]' id='wb_ent_options[postlogoinfo][style]' type='text' value='{$wb_ent_options['postlogoinfo']['style']}' /><br />";

		  echo $options;
	  }
	}

	if (!function_exists('hascvideo_setting')) {
	  function hascvideo_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['hascvideo']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[hascvideo]' id='wb_ent_options[hascvideo]' type='checkbox' value='{$wb_ent_options['hascvideo']}' {$checked} /> <label for='wb_ent_options[hascvideo]'>Yes</label>";
	  }
	}

	if (!function_exists('cvideoinfo_setting')) {
	  function cvideoinfo_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $options = "<label for='wb_ent_options[cvideoinfo][cat]'>Category Id: </label><input name='wb_ent_options[cvideoinfo][cat]' id='wb_ent_options[cvideoinfo][cat]' type='text' value='{$wb_ent_options['cvideoinfo']['cat']}' /> <br />";
		  $options .= "<label for='wb_ent_options[cvideoinfo][author]'>Author </label><input name='wb_ent_options[cvideoinfo][author]' id='wb_ent_options[cvideoinfo][author]' type='text' value='{$wb_ent_options['cvideoinfo']['author']}' /> <br />";
		  ($wb_ent_options['cvideoinfo']['debug']) ? $checked = 'checked' : $checked = '';
		  $options .= "<label for='wb_ent_options[cvideoinfo][debug]'>Debug </label><input name='wb_ent_options[cvideoinfo][debug]' id='wb_ent_options[cvideoinfo][debug]' type='checkbox' value='{$wb_ent_options['cvideoinfo']['debug']}' {$checked} /> <label for='wb_ent_options[cvideoinfo][debug]'>Yes</label> <br />";
		  $options .= "<label for='wb_ent_options[cvideoinfo][formurl]'>Form Url </label><input name='wb_ent_options[cvideoinfo][formurl]' id='wb_ent_options[cvideoinfo][formurl]' type='text' value='{$wb_ent_options['cvideoinfo']['formurl']}' /> <br />";
		  $options .= "<label for='wb_ent_options[cvideoinfo][confirmpage]'>Confirm Page </label><input name='wb_ent_options[cvideoinfo][confirmpage]' id='wb_ent_options[cvideoinfo][confirmpage]' type='text' value='{$wb_ent_options['cvideoinfo']['confirmpage']}' /> <br />";
		  $options .= "<label for='wb_ent_options[cvideoinfo][maxfilesize]'>Max File Size </label><input name='wb_ent_options[cvideoinfo][maxfilesize]' id='wb_ent_options[cvideoinfo][maxfilesize]' type='number' value='{$wb_ent_options['cvideoinfo']['maxfilesize']}' /> <br />";
		  $options .= "<label for='wb_ent_options[cvideoinfo][uploadlimit]'>Upload Limit </label><input name='wb_ent_options[cvideoinfo][uploadlimit]' id='wb_ent_options[cvideoinfo][uploadlimit]' type='number' value='{$wb_ent_options['cvideoinfo']['uploadlimit']}' /> <br />";
		  $options .= "<label for='wb_ent_options[cvideoinfo][maxvidnotify]'>Max Vid Notify </label><input name='wb_ent_options[cvideoinfo][maxvidnotify]' id='wb_ent_options[cvideoinfo][maxvidnotify]' type='text' value='{$wb_ent_options['cvideoinfo']['maxvidnotify']}' /> <br />";
		  $options .= "<label for='wb_ent_options[cvideoinfo][vidlabeltext]'>Video Label </label><input name='wb_ent_options[cvideoinfo][vidlabeltext]' id='wb_ent_options[cvideoinfo][vidlabeltext]' type='text' value='{$wb_ent_options['cvideoinfo']['vidlabeltext']}' /> <br />";
		  ($wb_ent_options['cvideoinfo']['hassubcats']) ? $checked = 'checked' : $checked = '';
		  $options .= "<label for='wb_ent_options[cvideoinfo][hassubcats]'>Sub Category </label><input name='wb_ent_options[cvideoinfo][hassubcats]' id='wb_ent_options[cvideoinfo][hassubcats]' type='checkbox' value='{$wb_ent_options['cvideoinfo']['hassubcats']}' {$checked} /> <label for='wb_ent_options[cvideoinfo][hassubcats]'>Yes</label>";
		  echo $options;
	  }
	}
    
	if (!function_exists('poststodisplay_setting')) {
	  function poststodisplay_setting(){
		  $wb_ent_options = get_option('wb_ent_options');
		  $passwordChecked = ($wb_ent_options['poststodisplay']['passwordprotected']) ? 'checked' : '';
		  //$privatePostsChecked = ($wb_ent_options['poststodisplay']['private']) ? 'checked' : '';

		  echo "<input name='wb_ent_options[poststodisplay][passwordprotected]' id='wb_ent_options[poststodisplay][passwordprotected]' type='checkbox' value='{$wb_ent_options['poststodisplay']['passwordprotected']}' {$passwordChecked} /> <label for='wb_ent_options[poststodisplay][passwordprotected]'>Password Protected Posts</label> <br />";
		  //echo "<input name='wb_ent_options[poststodisplay][private]' id='wb_ent_options[poststodisplay][private]' type='checkbox' value='{$wb_ent_options['poststodisplay']['private']}' {$privatePostsChecked} /> <label for='wb_ent_options[poststodisplay][private]'>Private Posts</label> <br />";

	  }
	}

	if (!function_exists('videolistwidget_setting')) {
	  function videolistwidget_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['videolistwidget']['hasrelated']) ? 'checked' : '';
		  $options = "<label for='wb_ent_options[videolistwidget][hasrelated]'>Related Vids</label><input name='wb_ent_options[videolistwidget][hasrelated]' id='wb_ent_options[videolistwidget][hasrelated]' type='checkbox' value='{$wb_ent_options['videolistwidget']['hasrelated']}' {$checked} /> <label for='wb_ent_options[videolistwidget][hasrelated]'>Yes </label><br />";
		  $checked = ($wb_ent_options['videolistwidget']['hasarchive']) ? 'checked' : '';
		  $options .= "<label for='wb_ent_options[videolistwidget][hasarchive]'>Archive Vids</label><input name='wb_ent_options[videolistwidget][hasarchive]' id='wb_ent_options[videolistwidget][hasarchive]' type='checkbox' value='{$wb_ent_options['videolistwidget']['hasarchive']}'  {$checked} /> <label for='wb_ent_options[videolistwidget][hasarchive]'>Yes </label><br />";
		  $checked = ($wb_ent_options['videolistwidget']['hastvguide']) ? 'checked' : '';
		  $options .= "<label for='wb_ent_options[videolistwidget][hastvguide]'>TV Guide </label><input name='wb_ent_options[videolistwidget][hastvguide]' id='wb_ent_options[videolistwidget][hastvguide]' type='checkbox' value='{$wb_ent_options['videolistwidget']['hastvguide']}' {$checked}  /> <label for='wb_ent_options[videolistwidget][hastvguide]'>Yes </label><br />";
		  $checked = ($wb_ent_options['videolistwidget']['haspopular']) ? 'checked' : '';
		  $options .= "<label for='wb_ent_options[videolistwidget][haspopular]'>Popular Vids</label><input name='wb_ent_options[videolistwidget][haspopular]' id='wb_ent_options[videolistwidget][haspopular]' type='checkbox' value='{$wb_ent_options['videolistwidget']['hasarchive']}' {$checked} /> <label for='wb_ent_options[videolistwidget][haspopular]'>Yes </label><br />";

		  $options .= "<label>Display mode </label>";
		  $items = array("separate", "tabbed");
		  foreach ($items as $item) {
			  $selected = ($wb_ent_options['videolistwidget']['displaymode'] == $item) ? 'checked' : '';
			  $itemName = ucfirst($item);
			  $options .= "<input name='wb_ent_options[videolistwidget][displaymode]' id='wb_ent_options[videolistwidget][displaymode]-$item' type='radio' value='$item' $selected /><label for='wb_ent_options[videolistwidget][displaymode]-$item'>$itemName </label>";
		  }
		  $options .= "<br /><label for='wb_ent_options[videolistwidget][videocats]'>Video Categories </label><input name='wb_ent_options[videolistwidget][videocats]' id='wb_ent_options[videolistwidget][videocats]' type='text' value='{$wb_ent_options['videolistwidget']['videocats']}' /> <br />";
		  $options .= "<br /><label for='wb_ent_options[videolistwidget][videocount]'>Number of Videos To Display </label><input name='wb_ent_options[videolistwidget][videocount]' id='wb_ent_options[videolistwidget][videocount]' type='text' value='{$wb_ent_options['videolistwidget']['videocount']}' /> <br />";

		  echo $options;
	  }
	}

	if (!function_exists('videolistinfo_setting')) {
	  function videolistinfo_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['videolistinfo']['usestatic']) ? 'checked' : '';
		  $options = "<label for='wb_ent_options[videolistinfo][usestatic]'>Static </label><input name='wb_ent_options[videolistinfo][usestatic]' id='wb_ent_options[videolistinfo][usestatic]' type='checkbox' type='checkbox'{$wb_ent_options['videolistinfo']['usestatic']}' {$checked} /><label for='wb_ent_options[videolistinfo][usestatic]'> Yes </label><br />";
		  $options .= "<label for='wb_ent_options[videolistinfo][vidsperpage]'>Vids per Page </label><input name='wb_ent_options[videolistinfo][vidsperpage]' type='number' id='wb_ent_options[videolistinfo][vidsperpage]' value='{$wb_ent_options['videolistinfo']['vidsperpage']}' /><br />";
		  $options .= "<label for='wb_ent_options[videolistinfo][vidsperpagehome]'>Vids per Page on Home </label><input name='wb_ent_options[videolistinfo][vidsperpagehome]' type='number' id='wb_ent_options[videolistinfo][vidsperpagehome]' value='{$wb_ent_options['videolistinfo']['vidsperpagehome']}' /><br />";
		  $options .= "<label for='wb_ent_options[videolistinfo][keywordlimit]'>Keyword Limit </label><input name='wb_ent_options[videolistinfo][keywordlimit]' type='number' id='wb_ent_options[videolistinfo][keywordlimit]' value='{$wb_ent_options['videolistinfo']['keywordlimit']}' /><br />";
		  $options .= "<label for='wb_ent_options[videolistinfo][desclimit]'>Description Limit </label><input name='wb_ent_options[videolistinfo][desclimit]' type='number' id='wb_ent_options[videolistinfo][desclimit]' value='{$wb_ent_options['videolistinfo']['desclimit']}' /><br />";
		  $options .= "<label for='wb_ent_options[videolistinfo][titlelimit]'>Title Limit </label><input name='wb_ent_options[videolistinfo][titlelimit]' type='number' id='wb_ent_options[videolistinfo][titlelimit]' value='{$wb_ent_options['videolistinfo']['titlelimit']}' /><br />";
		  $options .= "<label for='wb_ent_options[videolistinfo][vidlistlimit]'>Video List limit </label><input name='wb_ent_options[videolistinfo][vidlistlimit]' type='number' id='wb_ent_options[videolistinfo][vidlistlimit]' value='{$wb_ent_options['videolistinfo']['vidlistlimit']}' /><br />";
		  echo $options;
	  }
	}
    
	if (!function_exists('videosearchwidget_setting')) {
	  function videosearchwidget_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['videosearchwidget']['enable']) ? 'checked' : '';
		  $checkedApfSearch = ($wb_ent_options['videosearchwidget']['enableApfSearch']) ? 'checked' : '';

		  $options  = "<label for='wb_ent_options[videosearchwidget][enable]'>Enable </label><input name='wb_ent_options[videosearchwidget][enable]' id='wb_ent_options[videosearchwidget][enable]' type='checkbox' value='{$wb_ent_options['videosearchwidget']['enable']}' {$checked} /><br />";
		  $options  .= "<label for='wb_ent_options[videosearchwidget][enableApfSearch]'>Enable Apf Search</label><input name='wb_ent_options[videosearchwidget][enableApfSearch]' id='wb_ent_options[videosearchwidget][enableApfSearch]' type='checkbox' value='{$wb_ent_options['videosearchwidget']['enableApfSearch']}' {$checkedApfSearch} /><br />";
		  $options .= "<label for='wb_ent_options[videosearchwidget][replacecategory]'>Replace Category</label><textarea name='wb_ent_options[videosearchwidget][replacecategory]'>{$wb_ent_options['videosearchwidget']['replacecategory']}</textarea><br />";
		  $options .= "<label for='wb_ent_options[videosearchwidget][replacetopic]'>Replace Topic</label><textarea name='wb_ent_options[videosearchwidget][replacetopic]'>{$wb_ent_options['videosearchwidget']['replacetopic']}</textarea><br />";
		  $options .= "<label for='wb_ent_options[videosearchwidget][replacesearch]'>Replace Search</label><textarea name='wb_ent_options[videosearchwidget][replacesearch]'>{$wb_ent_options['videosearchwidget']['replacesearch']}</textarea><br />";
		  echo $options;
	  }
	}

	if (!function_exists('rsslink_setting')) {
	  function rsslink_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[rsslink]' type='text' value='{$wb_ent_options['rsslink']}' />";
	  }
	}

	if (!function_exists('emailsubslink_setting')) {
	  function emailsubslink_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[emailsubslink]' type='text' value='{$wb_ent_options['emailsubslink']}' />";
	  }
	}

	if (!function_exists('ituneslink_setting')) {
	  function ituneslink_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[ituneslink]' type='text' value='{$wb_ent_options['ituneslink']}' />";
	  }
	}

	if (!function_exists('itunesimage_setting')) {
	  function itunesimage_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<label> EN: </label>";
		  echo "<input name='wb_ent_options[itunesimage]' type='text' value='{$wb_ent_options['itunesimage']}' /><br />";
		  echo "<label> FR: </label>";
		  echo "<input name='wb_ent_options[itunesimage-fr_FR]' type='text' value='{$wb_ent_options['itunesimage-fr_FR']}' /><br />";

	  }
	}

	if (!function_exists('disqussname_setting')) {
	  function disqussname_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[disqussname]' type='text' value='{$wb_ent_options['disqussname']}' />";
	  }
	}

	if (!function_exists('sharethispubid_setting')) {
	  function sharethispubid_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[sharethispubid]' type='text' value='{$wb_ent_options['sharethispubid']}' />";
	  }
	}

	if (!function_exists('recaptchapriv_setting')) {
	  function recaptchapriv_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[recaptchapriv]' type='text' value='{$wb_ent_options['recaptchapriv']}' />";
	  }
	}

	if (!function_exists('recaptchapub_setting')) {
	  function recaptchapub_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[recaptchapub]' type='text' value='{$wb_ent_options['recaptchapub']}' />";
	  }
	}

	if (!function_exists('fbappid_setting')) {
	  function fbappid_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[fbappid]' type='text' value='{$wb_ent_options['fbappid']}' />";
	  }
	}

	if (!function_exists('analytics_setting')) {
	  function analytics_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  //print_r($wb_ent_options[analytics]);
		  $options = "<label for='wb_ent_options[analytics][1][name]'>Name </label><input name='wb_ent_options[analytics][1][name]' id='wb_ent_options[analytics][1][name]' type='text' value='{$wb_ent_options['analytics'][1]['name']}' /><br />";
		  $options .= "<label for='wb_ent_options[analytics][1][code]'>Code </label><input name='wb_ent_options[analytics][1][code]' id='wb_ent_options[analytics][1][code]' type='text' value='{$wb_ent_options['analytics'][1]['code']}' /><br />";
		  $options .= "<label for='wb_ent_options[analytics][2][name]'>Name </label><input name='wb_ent_options[analytics][2][name]' id='wb_ent_options[analytics][2][name]' type='text' value='{$wb_ent_options['analytics'][2]['name']}' /><br />";
		  $options .= "<label for='wb_ent_options[analytics][2][code]'>Code </label><input name='wb_ent_options[analytics][2][code]' id='wb_ent_options[analytics][2][code]' type='text' value='{$wb_ent_options['analytics'][2]['code']}' /><br />";        
		  $options .= "<label for='wb_ent_options[analytics][3][name]'>Name </label><input name='wb_ent_options[analytics][3][name]' id='wb_ent_options[analytics][3][name]' type='text' value='{$wb_ent_options['analytics'][3]['name']}' /><br />";
		  $options .= "<label for='wb_ent_options[analytics][3][code]'>Code </label><input name='wb_ent_options[analytics][3][code]' id='wb_ent_options[analytics][3][code]' type='text' value='{$wb_ent_options['analytics'][3]['code']}' />";        		
		  echo $options;
	  }
	}

	if (!function_exists('amazons3info_setting')) {
	  function amazons3info_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  //print_r($wb_ent_options[amazons3info]);
		  $options = "<label for='wb_ent_options[amazons3info][channelbucket]'>Channel Bucket</label><input name='wb_ent_options[amazons3info][channelbucket]' id='wb_ent_options[amazons3info][channelbucket]' type='text' value='{$wb_ent_options['amazons3info']['channelbucket']}' /><br />";
		  $options .= "<label for='wb_ent_options[amazons3info][channelbucketurl]'>Channel Bucket URL</label><input name='wb_ent_options[amazons3info][channelbucketurl]' id='wb_ent_options[amazons3info][channelbucketurl]' type='text' value='{$wb_ent_options['amazons3info']['channelbucketurl']}' /><br />";
		  $options .= "<label for='wb_ent_options[amazons3info][accesskey]'>Access Key</label><input name='wb_ent_options[amazons3info][accesskey]' type='text' id='wb_ent_options[amazons3info][accesskey]' value='{$wb_ent_options['amazons3info']['accesskey']}' /><br />";
		  $options .= "<label for='wb_ent_options[amazons3info][secretkey]'>Secret Key</label><input name='wb_ent_options[amazons3info][secretkey]' type='text' id='wb_ent_options[amazons3info][secretkey]' value='{$wb_ent_options['amazons3info']['secretkey']}' /><br />";

		  echo $options;
	  }
	}
        
                    if (!function_exists('cloudfrontinfo_setting')) {
		function cloudfrontinfo_setting() {
			$wb_ent_options = get_option('wb_ent_options');
			$options = "<label for='wb_ent_options[cloudfrontinfo][domainname]'>Domain Name</label><input name='wb_ent_options[cloudfrontinfo][domainname]' id='wb_ent_options[cloudfrontinfo][domainname]' type='text' value='{$wb_ent_options['cloudfrontinfo']['domainname']}' /><br />";
			$options .= "<label for='wb_ent_options[cloudfrontinfo][distributionid]'>Distribution ID</label><input name='wb_ent_options[cloudfrontinfo][distributionid]' id='wb_ent_options[cloudfrontinfo][distributionid]' type='text' value='{$wb_ent_options['cloudfrontinfo']['distributionid']}' /><br />";
			echo $options;
		}
	}

	if (!function_exists('brightcoveinfo_setting')) {
	  function brightcoveinfo_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $options = "<label for='wb_ent_options[brightcoveinfo][publisherid]'>Publisher ID </label><input name='wb_ent_options[brightcoveinfo][publisherid]' id='wb_ent_options[brightcoveinfo][publisherid]' type='text' value='{$wb_ent_options['brightcoveinfo']['publisherid']}' /><br />";
		  $options .= "<label for='wb_ent_options[brightcoveinfo][readtoken]'>Read Token </label><input name='wb_ent_options[brightcoveinfo][readtoken]' id='wb_ent_options[brightcoveinfo][readtoken]' type='text' value='{$wb_ent_options['brightcoveinfo']['readtoken']}' /><br />";
		  $options .= "<label for='wb_ent_options[brightcoveinfo][readurltoken]'>Read URL Token </label><input name='wb_ent_options[brightcoveinfo][readurltoken]' id='wb_ent_options[brightcoveinfo][readurltoken]' type='text' value='{$wb_ent_options['brightcoveinfo']['readurltoken']}' /><br />";
		  $options .= "<label for='wb_ent_options[brightcoveinfo][writetoken]'>Write Token </label><input name='wb_ent_options[brightcoveinfo][writetoken]' id='wb_ent_options[brightcoveinfo][writetoken]' type='text' value='{$wb_ent_options['brightcoveinfo']['writetoken']}' /><br />";
		  echo $options;
	  }
	}

	if (!function_exists('bcplayers_setting')) {
	  function bcplayers_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $options = "<label for='wb_ent_options[bcplayers][home]'>Home </label><input name='wb_ent_options[bcplayers][home]' id='wb_ent_options[bcplayers][home]' type='text' value='{$wb_ent_options['bcplayers']['home']}' /><br />";
		  $options .= "<label for='wb_ent_options[bcplayers][webcast]'>Webcast </label><input name='wb_ent_options[bcplayers][webcast]' id='wb_ent_options[bcplayers][webcast]' type='text' value='{$wb_ent_options['bcplayers']['webcast']}' /><br />";
		  $options .= "<label for='wb_ent_options[bcplayers][archive]'>Archive </label><input name='wb_ent_options[bcplayers][archive]' id='wb_ent_options[bcplayers][archive]' type='text' value='{$wb_ent_options['bcplayers']['archive']}' /><br />";
		  $options .= "<label for='wb_ent_options[bcplayers][feature]'>Feature </label><input name='wb_ent_options[bcplayers][feature]' id='wb_ent_options[bcplayers][feature]' type='text' value='{$wb_ent_options['bcplayers']['feature']}' /><br />";
		  $options .= "<label for='wb_ent_options[bcplayers][embed]'>Embed </label><input name='wb_ent_options[bcplayers][embed]' id='wb_ent_options[bcplayers][embed]' type='text' value='{$wb_ent_options['bcplayers']['embed']}' /><br />";
		  $options .= "<label for='wb_ent_options[bcplayers][preview]'>Preview</label><input name='wb_ent_options[bcplayers][preview]' id='wb_ent_options[bcplayers][preview]' type='text' value='{$wb_ent_options['bcplayers']['preview']}' /><br />";
		  $options .= "<label for='wb_ent_options[bcplayers][library]'>Library</label><input name='wb_ent_options[bcplayers][library]' id='wb_ent_options[bcplayers][library]' type='text' value='{$wb_ent_options['bcplayers']['library']}' /><br />";
		  $options .= "<label for='wb_ent_options[bcplayers][cvideo]'>CVideo</label><input name='wb_ent_options[bcplayers][cvideo]' id='wb_ent_options[bcplayers][cvideo]' type='text' value='{$wb_ent_options['bcplayers']['cvideo']}' /><br />";
		  $options .= "<label for='wb_ent_options[bcplayers][invidad]'>In-Video Ad</label><input name='wb_ent_options[bcplayers][invidad]' id='wb_ent_options[bcplayers][invidad]' type='text' value='{$wb_ent_options['bcplayers']['invidad']}' /><br />";
		  echo $options;
	  }
	}

	if (!function_exists('notifications_setting')) {
	  function notifications_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<p>Adform</p>";
		  $options = "<label for='wb_ent_options[notification][adform][recipient]'>Recipient</label><input name='wb_ent_options[notification][adform][recipient]' id='wb_ent_options[notification][adform][recipient]' type='text' value='{$wb_ent_options['notification']['adform']['recipient']}' /> <br />";
		  $options .= "<label for='wb_ent_options[notification][adform][subject]'>Subject</label><input name='wb_ent_options[notification][adform][subject]' id='wb_ent_options[notification][adform][subject]' type='text' value='{$wb_ent_options['notification']['adform']['subject']}' /> <br />";
		  $options .= "<label for='wb_ent_options[notification][adform][messagelimit]'>Message Limit</label><input name='wb_ent_options[notification][adform][messagelimit]' id='wb_ent_options[notification][adform][messagelimit]' type='text' value='{$wb_ent_options['notification']['adform']['messagelimit']}' /> <br />";
		  $options .= "<p>Contact</p>";
		  $options .= "<label for='wb_ent_options[notification][contact][recipient]'>Recipient</label><input name='wb_ent_options[notification][contact][recipient]' id='wb_ent_options[notification][contact][recipient]' type='text' value='{$wb_ent_options['notification']['contact']['recipient']}' /> <br />";
		  $options .= "<label for='wb_ent_options[notification][contact][subject]'>Subject</label><input name='wb_ent_options[notification][contact][subject]' id='wb_ent_options[notification][contact][subject]' type='text' value='{$wb_ent_options['notification']['contact']['subject']}' /> <br />";
		  $options .= "<label for='wb_ent_options[notification][contact][url]'>Client Contact Page</label><input name='wb_ent_options[notification][contact][url]' id='wb_ent_options[notification][contact][url]' type='text' value='{$wb_ent_options['notification']['contact']['url']}' /> <br />";

		  echo $options;
	  }
	}

	if (!function_exists('videocats_setting')) {
	  function videocats_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $options = "<label for='wb_ent_options[videocats][webcast]'>Webcast </label><input name='wb_ent_options[videocats][webcast]' id='wb_ent_options[videocats][webcast]' type='text' value='{$wb_ent_options['videocats']['webcast']}' /> <br />";
		  $options .= "<label for='wb_ent_options[videocats][feature]'>Feature </label><input name='wb_ent_options[videocats][feature]' id='wb_ent_options[videocats][feature]' type='text' value='{$wb_ent_options['videocats']['feature']}' /> <br />";
		  $options .= "<label for='wb_ent_options[videocats][library]'>Library </label><input name='wb_ent_options[videocats][library]' id='wb_ent_options[videocats][library]' type='text' value='{$wb_ent_options['videocats']['library']}' /> <br />";
		  $options .= "<label for='wb_ent_options[videocats][playlist]'>Playlist </label><input name='wb_ent_options[videocats][playlist]' id='wb_ent_options[videocats][playlist]' type='text' value='{$wb_ent_options['videocats']['playlist']}' /> <br />";
		  $options .= "<label for='wb_ent_options[videocats][private]'>Private list</label><input name='wb_ent_options[videocats][private]' id='wb_ent_options[videocats][private]' type='text' value='{$wb_ent_options['videocats']['private']}' /> <br />";
		  $options .= "<label for='wb_ent_options[videocats][unlisted]'>Unlisted </label><input name='wb_ent_options[videocats][unlisted]' id='wb_ent_options[videocats][unlisted]' type='text' value='{$wb_ent_options['videocats']['unlisted']}' /> <br />";
          $options .= "<label for='wb_ent_options[videocats][howto]'>How-to </label><input name='wb_ent_options[videocats][howto]' id='wb_ent_options[videocats][howto]' type='text' value='{$wb_ent_options['videocats']['howto']}' /> <br />";
          $options .= "<label for='wb_ent_options[videocats][podcast]'>Podcast </label><input name='wb_ent_options[videocats][podcast]' id='wb_ent_options[videocats][podcast]' type='text' value='{$wb_ent_options['videocats']['podcast']}' /> <br />";
          $options .= "<label for='wb_ent_options[videocats][coming]'>Coming Soon </label><input name='wb_ent_options[videocats][coming]' id='wb_ent_options[videocats][coming]' type='text' value='{$wb_ent_options['videocats']['coming']}' /> <br />";
          $options .= "<label for='wb_ent_options[videocats][cayl]'>CAYL</label><input name='wb_ent_options[videocats][cayl]' id='wb_ent_options[videocats][cayl]' type='text' value='{$wb_ent_options['videocats']['cayl']}' /> ";
          $options .= "<label for='wb_ent_options[videocats][cayl_user]'>CAYL User Role </label><input name='wb_ent_options[videocats][cayl_user]' id='wb_ent_options[videocats][cayl_user]' type='text' value='{$wb_ent_options['videocats']['cayl_user']}' /> <br />";
          
          $options .= "<label for='wb_ent_options[videocats][members]'>Members Only</label><input name='wb_ent_options[videocats][members]' id='wb_ent_options[videocats][members]' type='text' value='{$wb_ent_options['videocats']['members']}' /> ";
          $options .= "<label for='wb_ent_options[roles][members]'>Members User Role Name</label><input name='wb_ent_options[roles][members]' id='wb_ent_options[roles][members]' type='text' value='{$wb_ent_options['roles']['members']}' /> <br /><br />";

		  $options .= "<label>French Post Categories</label><br />";        
		  $options .= "<label for='wb_ent_options[videocats][webcast-fr_FR]'>Webcast French</label><input name='wb_ent_options[videocats][webcast-fr_FR]' id='wb_ent_options[videocats][webcast-fr_FR]' type='text' value='{$wb_ent_options['videocats']['webcast-fr_FR']}' /> <br />";
		  $options .= "<label for='wb_ent_options[videocats][feature-fr_FR]'>Feature French</label><input name='wb_ent_options[videocats][feature-fr_FR]' id='wb_ent_options[videocats][feature-fr_FR]' type='text' value='{$wb_ent_options['videocats']['feature-fr_FR']}' /> <br />";
		  $options .= "<label for='wb_ent_options[videocats][library-fr_FR]'>Library French</label><input name='wb_ent_options[videocats][library-fr_FR]' id='wb_ent_options[videocats][library-fr_FR]' type='text' value='{$wb_ent_options['videocats']['library-fr_FR']}' /> <br />";
		  $options .= "<label for='wb_ent_options[videocats][playlist-fr_FR]'>Playlist French</label><input name='wb_ent_options[videocats][playlist-fr_FR]' id='wb_ent_options[videocats][playlist-fr_FR]' type='text' value='{$wb_ent_options['videocats']['playlist-fr_FR']}' /> <br />";
		  $options .= "<label for='wb_ent_options[videocats][private-fr_FR]'>Private list French</label><input name='wb_ent_options[videocats][private-fr_FR]' id='wb_ent_options[videocats][private-fr_FR]' type='text' value='{$wb_ent_options['videocats']['private-fr_FR']}' /> <br />";
		  $options .= "<label for='wb_ent_options[videocats][unlisted-fr_FR]'>Unlisted French</label><input name='wb_ent_options[videocats][unlisted-fr_FR]' id='wb_ent_options[videocats][unlisted-fr_FR]' type='text' value='{$wb_ent_options['videocats']['unlisted-fr_FR']}' /> <br />";

		  echo $options;
	  }
	}
    
	if (!function_exists('channels_setting')) {
	  function channels_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['channels']['librarybox']) ? 'checked' : '';
		  $options  = "<label for='wb_ent_options[channels][librarybox]'>Channel Library Box </label><input name='wb_ent_options[channels][librarybox]' id='wb_ent_options[channels][librarybox]' type='checkbox' value='{$wb_ent_options['channels']['librarybox']}' {$checked} /> <br />";
		  $options .= "<label for='wb_ent_options[channels][library]'>Channel Library</label><input name='wb_ent_options[channels][library]' id='wb_ent_options[channels][library]' type='text' value='{$wb_ent_options['channels']['library']}' /> <br />";        
		  $options .= "<label for='wb_ent_options[channels][headertitle]'>Channel Header Title</label><input name='wb_ent_options[channels][headertitle]' id='wb_ent_options[channels][headertitle]' type='text' value='{$wb_ent_options['channels']['headertitle']}' /> <br />";        

		  echo $options;
	  }
	}
	
	if (!function_exists('uploaderbcimport_setting')) {
	  function uploaderbcimport_setting(){
        $wb_ent_options = get_option('wb_ent_options');
        $checked = ($wb_ent_options['uploaderbcimport']['enabled']) ? 'checked' : '';			
				$options = "<label for='wb_ent_options[uploaderbcimport][enabled]'>Enabled</label><input name='wb_ent_options[uploaderbcimport][enabled]' id='wb_ent_options[uploaderbcimport][enabled]' type='checkbox' value='{$wb_ent_options['uploaderbcimport']['enabled']}' {$checked} /> <label for='wb_ent_options[uploaderbcimport][enabled]'>Yes </label><br />";
				
				echo $options;
	  }
	}

	if (!function_exists('vidshortcode_setting')) {
	  function vidshortcode_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['vidshortcode']['enabled']) ? 'checked' : '';
		  $options = "<label for='wb_ent_options[vidshortcode][enabled]'>Enabled</label><input name='wb_ent_options[vidshortcode][enabled]' id='wb_ent_options[vidshortcode][enabled]' type='checkbox' value='{$wb_ent_options['vidshortcode']['enabled']}' {$checked} /> <label for='wb_ent_options[vidshortcode][enabled]'>Yes </label><br />";
		  //$options  = "<label for='wb_ent_options[vidshortcode][enabled]'>Enabled</label><input name='wb_ent_options[vidshortcode][enabled]' id='wb_ent_options[vidshortcode][enabled]' type='text' value='{$wb_ent_options['vidshortcode']['enabled']}' /> <br />";
		  $options .= "<label for='wb_ent_options[vidshortcode][tag]'>Tag</label><input name='wb_ent_options[vidshortcode][tag]' id='wb_ent_options[vidshortcode][tag]' type='text' value='{$wb_ent_options['vidshortcode']['tag']}' /> <br />";
		  $options .= "<label for='wb_ent_options[vidshortcode][name]'>Name</label><input name='wb_ent_options[vidshortcode][name]' id='wb_ent_options[vidshortcode][name]' type='text' value='{$wb_ent_options['vidshortcode']['name']}' /> <br />";
		  $options .= "<label for='wb_ent_options[vidshortcode][pubId]'>Pub ID</label><input name='wb_ent_options[vidshortcode][pubId]' id='wb_ent_options[vidshortcode][pubId]' type='text' value='{$wb_ent_options['vidshortcode']['pubId']}' /> <br />";
		  $options .= "<label for='wb_ent_options[vidshortcode][playerId]'>Player ID</label><input name='wb_ent_options[vidshortcode][playerId]' id='wb_ent_options[vidshortcode][playerId]' type='text' value='{$wb_ent_options['vidshortcode']['playerId']}' /> <br />";
		  $options .= "<label for='wb_ent_options[vidshortcode][token]'>Token</label><input name='wb_ent_options[vidshortcode][token]' id='wb_ent_options[vidshortcode][token]' type='text' value='{$wb_ent_options['vidshortcode']['token']}' /> <br />";
		  $options .= "<label for='wb_ent_options[vidshortcode][dbhost]'>DB Host</label><input name='wb_ent_options[vidshortcode][dbhost]' id='wb_ent_options[vidshortcode][dbhost]' type='text' value='{$wb_ent_options['vidshortcode']['dbhost']}' /> <br />";
		  $options .= "<label for='wb_ent_options[vidshortcode][dbname]'>DB Name</label><input name='wb_ent_options[vidshortcode][dbname]' id='wb_ent_options[vidshortcode][dbname]' type='text' value='{$wb_ent_options['vidshortcode']['dbname']}' /> <br />";
		  $options .= "<label for='wb_ent_options[vidshortcode][dbuser]'>DB User</label><input name='wb_ent_options[vidshortcode][dbuser]' id='wb_ent_options[vidshortcode][dbuser]' type='text' value='{$wb_ent_options['vidshortcode']['dbuser']}' /> <br />";
		  $options .= "<label for='wb_ent_options[vidshortcode][dbpass]'>DB Pass</label><input name='wb_ent_options[vidshortcode][dbpass]' id='wb_ent_options[vidshortcode][dbpass]' type='text' value='' /> <br />";

		  echo $options;
	  }
	}
    
	if (!function_exists('apiinfo_setting')) {
	  function apiinfo_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['apiinfo']['displayall']) ? 'checked' : '';


		  $options = "<label for='wb_ent_options[apiinfo][displayall]'>Display All Public Posts</label> <input name='wb_ent_options[apiinfo][displayall]' id='wb_ent_options[apiinfo][displayall]' type='checkbox' value='{$wb_ent_options['apiinfo']['displayall']}' {$checked} /> <label for='wb_ent_options[apiinfo][displayall]'>Yes </label><br />";        
		  $options .= "<label for='wb_ent_options[apiinfo][apikey]'>Client API Key</label> <input name='wb_ent_options[apiinfo][apikey]' id='wb_ent_options[apiinfo][apikey]' type='text' value='{$wb_ent_options['apiinfo']['apikey']}' /> <br />";

		  echo $options;
	  }  
	}
    
    
	if (!function_exists('youtubechannel_setting')) {
	  function youtubechannel_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  echo "<input name='wb_ent_options[youtubechannel]' type='text' value='{$wb_ent_options['youtubechannel']}' />";
	  }
	}

	if (!function_exists('associations_setting')) {
	  function associations_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['associations']['sponsorpages']) ? 'checked' : '';
		  $options = "<label for=''>Name </label><input name='wb_ent_options[associations][name]' type='text' value='{$wb_ent_options['associations']['name']}' /><br />";
		  $options .= "<label for='wb_ent_options[associations][sponsorpages]'>Sponsor Pages </label><input name='wb_ent_options[associations][sponsorpages]' id='wb_ent_options[associations][sponsorpages]' type='checkbox' value='{$wb_ent_options['associations']['sponsorpages']}' {$checked} /><br />";
		  $checked2 = ($wb_ent_options['associations']['naylor']) ? 'checked' : '';
		  $options .= "<label for='wb_ent_options[associations][naylor]'>Naylor </label><input name='wb_ent_options[associations][naylor]' id='wb_ent_options[associations][naylor]' type='checkbox' value='{$wb_ent_options['associations']['naylor']}' {$checked2} /><br />";
		  $options .= "<label for=''>Community </label><input name='wb_ent_options[associations][community]' type='text' value='{$wb_ent_options['associations']['community']}' /><br />";
		  echo $options;
	  }
	}

	if (!function_exists('customstyle_setting')) {
	  function customstyle_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $options = "<label for=''>Favicon</label><input name='wb_ent_options[customstyle][favicon]' type='text' value='{$wb_ent_options['customstyle']['favicon']}' /><br />";
		  $options .= "<label for=''>Custom CSS</label><input name='wb_ent_options[customstyle][customcss]' type='text' value='{$wb_ent_options['customstyle']['customcss']}' /><br />";
		  $options .= "<label for=''>Custom IE8 CSS</label><input name='wb_ent_options[customstyle][customie8css]' type='text' value='{$wb_ent_options['customstyle']['customie8css']}' /><br />";
		  $options .= "<label for=''>Custom JS</label><input name='wb_ent_options[customstyle][customjs]' type='text' value='{$wb_ent_options['customstyle']['customjs']}' /><br />";
		  $options .= "<label for=''>Header ID</label><input name='wb_ent_options[customstyle][headerid]' type='text' value='{$wb_ent_options['customstyle']['headerid']}' /><br />";
		  $options .= "<label for=''>Header Container</label><input name='wb_ent_options[customstyle][headercontainer]' type='text' value='{$wb_ent_options['customstyle']['headercontainer']}' /><br />";
		  $options .= "<label for=''>Header HTML</label><textarea name='wb_ent_options[customstyle][headerhtml]'>{$wb_ent_options['customstyle']['headerhtml']}</textarea><br />";
		  $options .= "<label for=''>Header Logo</label><input name='wb_ent_options[customstyle][headerlogo]' type='text' value='{$wb_ent_options['customstyle']['headerlogo']}' /><br />";
		  $options .= "<label for=''>Workerbee Content Wrapper ID</label><input name='wb_ent_options[customstyle][contentwrapper]' type='text' value='{$wb_ent_options['customstyle']['contentwrapper']}' /><br />";
		  $options .= "<label for=''>Footer HTML</label><textarea name='wb_ent_options[customstyle][footerhtml]'>{$wb_ent_options['customstyle']['footerhtml']}</textarea><br />";
		  /*
		  $options .= "<label for=''>Header HTML Home</label><textarea name='wb_ent_options[customstyle][headerhtml-home]'>{$wb_ent_options['customstyle']['headerhtml-home']}</textarea><br />";
		  $options .= "<label for=''>Footer HTML Home</label><textarea name='wb_ent_options[customstyle][footerhtml-home]'>{$wb_ent_options['customstyle']['footerhtml-home']}</textarea><br />";        
		  $options .= "<label for=''>Header HTML FR Home</label><textarea name='wb_ent_options[customstyle][headerhtml-fr_FR-home]'>{$wb_ent_options['customstyle']['headerhtml-fr_FR-home']}</textarea><br />";
		  $options .= "<label for=''>Footer HTML FR Home</label><textarea name='wb_ent_options[customstyle][footerhtml-fr_FR-home]'>{$wb_ent_options['customstyle']['footerhtml-fr_FR-home']}</textarea><br />";          
		   */
		  $options .= "<label for=''>Header HTML FR</label><textarea name='wb_ent_options[customstyle][headerhtml-fr_FR]'>{$wb_ent_options['customstyle']['headerhtml-fr_FR']}</textarea><br />";
		  $options .= "<label for=''>Footer HTML FR</label><textarea name='wb_ent_options[customstyle][footerhtml-fr_FR]'>{$wb_ent_options['customstyle']['footerhtml-fr_FR']}</textarea><br />";
		  //$options .= "<label></label><input name='wb_ent_options[customstyle][]' type='text' value='{$wb_ent_options['customstyle']['']}' /><br />";
		  echo $options;
	  }
	}

	if (!function_exists('updatethumbs_setting')) {
	  function updatethumbs_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $checked = ($wb_ent_options['updatethumbs']) ? 'checked' : '';
		  echo "<input name='wb_ent_options[updatethumbs]' id='wb_ent_options[updatethumbs]' type='checkbox' value='{$wb_ent_options['updatethumbs']}' {$checked} /> <label for='wb_ent_options[updatethumbs]'>Yes</label>";
	  }
	}

	if (!function_exists('wbportal_setting')) {
	  function wbportal_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $options = "<label for=''>Host</label><input name='wb_ent_options[wbportal][portalhost]' type='text' value='{$wb_ent_options['wbportal']['portalhost']}' /><br />";
		  $options .= "<label for=''>Database Name</label><input name='wb_ent_options[wbportal][portaldbname]' type='text' value='{$wb_ent_options['wbportal']['portaldbname']}' /><br />";
		  $options .= "<label for=''>Database User</label><input name='wb_ent_options[wbportal][portaluser]' type='text' value='{$wb_ent_options['wbportal']['portaluser']}' /><br />";
		  $options .= "<label for=''>Database Password</label><input name='wb_ent_options[wbportal][portalpass]' type='password' value='' /><br /><br />";
		  $options .= "<label for=''>Portal Client ID</label><input name='wb_ent_options[wbportal][portalclientid]' type='text' value='{$wb_ent_options['wbportal']['portalclientid']}' /><br />";
		  $options .= "<label for=''>Portal User Type</label><input name='wb_ent_options[wbportal][portalusertype]' type='text' value='{$wb_ent_options['wbportal']['portalusertype']}' /><br />";

		  echo $options;
	  }   
	}
    
	if (!function_exists('wvc_setting')) {
	  function wvc_setting() {
		  $wb_ent_options = get_option('wb_ent_options');
		  $options = "<label for=''>Host</label><input name='wb_ent_options[wvc][dbhost]' type='text' value='{$wb_ent_options['wvc']['dbhost']}' /><br />";
		  $options .= "<label for=''>Database Name</label><input name='wb_ent_options[wvc][dbname]' type='text' value='{$wb_ent_options['wvc']['dbname']}' /><br />";
		  $options .= "<label for=''>Database User</label><input name='wb_ent_options[wvc][dbuser]' type='text' value='{$wb_ent_options['wvc']['dbuser']}' /><br />";
		  $options .= "<label for=''>Database Password</label><input name='wb_ent_options[wvc][dbpass]' type='password' value='' /><br /><br />";        

		  echo $options;
	  }     
	}
    

    add_action('admin_menu', 'wb_ent_options_page');

	if (!function_exists('wb_ent_options_page')) {
	  function wb_ent_options_page() {
		  $enterprise_apf_settings = add_options_page('Enterprise APF Settings', 'Theme Settings', 'administrator', __FILE__, 'build_options_page');
		  add_action( "admin_head-{$enterprise_apf_settings}", 'enterprise_apf_head_script' );
	  }
	}
    
	if (!function_exists('enterprise_apf_head_script')) {
	  function enterprise_apf_head_script() { ?>

	  <script type="text/javascript">
		  jQuery(document).ready( function($) {
			  jQuery('#theme-options-wrap input[type="checkbox"]').change(function() {
				 if( this.checked ){
					this.value = true;
				 }
				 else{
					this.value = false;
				 }
			  });
		  });

	  </script>

	  <?php }  
	}

	if (!function_exists('getCurrentAPFPage')) {
	  function getCurrentAPFPage() {
		  $current_page0 = explode('?', $_SERVER['REQUEST_URI']);
		  $current_page = explode('/', $current_page0[0]);
		  return $current_page[1];
	  }
	}

//Making jQuery Google API
	if (!function_exists('modify_jquery')) {
	  function modify_jquery() {
		  if (!is_admin()) {
			  // comment out the next two lines to load the local copy of jQuery
			  wp_deregister_script('jquery');
			  wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js', false, '1.8.1');
			  wp_enqueue_script('jquery');
		  }
	  }
	}

    add_action('init', 'modify_jquery');
/*
//function to change the H3 from custome field template in the admin panel
    function modify_title_custome_template_field_plugin() {
        global $parent_file;


        If (is_plugin_active('custom-field-template/custom-field-template.php')) {
            if (is_admin() && $parent_file == 'edit.php') {
                echo '
         <script type="text/javascript">
            
            //http://codex.wordpress.org/Function_Reference/wp_enqueue_script
            jQuery(document).ready(function(){
               jQuery("#cftdiv h3").html("Banner Management");
            });
                     
         </script>
         
         ';
            }
        }
    }

    add_filter('admin_head', 'modify_title_custome_template_field_plugin');
*/

    add_action('load-themes.php', 'Init_theme');
	if (!function_exists('Init_theme')) {
	  function Init_theme() {
		  global $pagenow;

		  if ('themes.php' == $pagenow && isset($_GET['activated'])) { // Test if theme is activate
			  // What to do when theme is activate
  // Lists of the default pages and its template
			  $defaultPages = array(
				 /* 0 => array(
				   'post_title' => '404',
				   'ID' => '',
				   'post_type' => 'page',
				   'post_status' => 'publish',
				   'page_template' => 'contact.php'
				   ),
				   1 => array(
				   'post_title' => 'Viewing Tips',
				   'ID' => '',
				   'post_type' => 'page',
				   'post_status' => 'publish',
				   'page_template' => 'viewing-tips.php'
				   ), */
				 0 => array(
					'post_title' => 'All Episodes',
					'ID' => '',
					'post_name' => 'all-episodes',
					'post_type' => 'page',
					'post_status' => 'publish',
					'page_template' => 'episodes.php'
				 ),
				 1 => array(
					'post_title' => 'All Videos',
					'ID' => '',
					'post_name' => 'all-videos',
					'post_type' => 'page',
					'post_status' => 'publish',
					'page_template' => 'allvideos.php'
				 ),
				 2 => array(
					'post_title' => 'Contact Us',
					'ID' => '',
					'post_name' => 'contact',
					'post_type' => 'page',
					'post_status' => 'publish',
					'page_template' => 'contact.php'
				 ),
				 3 => array(
					'post_title' => 'Home',
					'ID' => '',
					'post_name' => 'home',
					'post_type' => 'page',
					'post_status' => 'publish',
					'page_template' => 'video-home.php'
				 ),
				 4 => array(
					'post_title' => 'Most Recent Videos',
					'ID' => '',
					'post_name' => 'most-recent-videos',
					'post_type' => 'page',
					'post_status' => 'publish',
					'page_template' => 'allvideos.php'
				 ),
				 5 => array(
					'post_title' => 'Most Viewed Videos',
					'ID' => '',
					'post_name' => 'most-viewed-videos',
					'post_type' => 'page',
					'post_status' => 'publish',
					'page_template' => 'most-viewed.php'
				 ),
				 6 => array(
					'post_title' => 'Naylor Banner Solutions',
					'ID' => '',
					'post_name' => 'naylor-banner-solutions',
					'post_type' => 'page',
					'post_status' => 'publish',
					'page_template' => 'sponsor-template-bannerSolutions.php'
				 ),
				 7 => array(
					'post_title' => 'Naylor Video Production',
					'ID' => '',
					'post_name' => 'naylor-video-production',
					'post_type' => 'page',
					'post_status' => 'publish',
					'page_template' => 'sponsor-template-videoProd.php'
				 ),
				 8 => array(
					'post_title' => 'Naylor Video Solutions',
					'ID' => '',
					'post_name' => 'naylor-video-solutions',
					'post_type' => 'page',
					'post_status' => 'publish',
					'page_template' => 'sponsor-template-videoSolutions.php'
				 ),
				 9 => array(
					'post_title' => 'Schedule a Meeting',
					'ID' => '',
					'post_name' => 'sponsor-contact',
					'post_type' => 'page',
					'post_status' => 'publish',
					'page_template' => 'sponsor-template-contact.php'
				 ),
				 10 => array(
					'post_title' => 'Sponsor Us',
					'ID' => '',
					'post_name' => 'sponsor-us',
					'post_type' => 'page',
					'post_status' => 'publish',
					'page_template' => 'sponsor-template-landingPage.php'
				 ),
				 11 => array(
					'post_title' => 'Subscribe',
					'ID' => '',
					'post_name' => 'subscribe',
					'post_type' => 'page',
					'post_status' => 'publish',
					'page_template' => 'subscribeMc.php'
				 ),               
				 12 => array(
					'post_title' => 'Viewing Tips',
					'ID' => '',
					'post_name' => 'viewing-tips',
					'post_type' => 'page',
					'post_status' => 'publish',
					'page_template' => 'viewing-tips.php'
				 ),                             

			  );




  //Check if the default pages exists
			  foreach ($defaultPages as $defaultpage) {

				  //check if page existed
				  $pagefound = pagefind($defaultpage[post_title]);

				  //If Page exists
				  if (isset($pagefound)) {

					  //if page exists and contains the right template 
					  if (get_page_template_slug($pagefound->ID) == $defaultpage[page_template]) {
						  continue;
						  /* echo get_page_template_slug($pagefound->ID);
							echo ' - Same template.<br />'; */
					  }
					  //if page exists but doesn't have the right template
					  else {
						  //echo 'Not the same <br />';
						  //get the page ID to update the template
						  $defaultpage[ID] = $pagefound->ID;
						  //update the page with the right template
						  wp_insert_post($defaultpage);
					  }
				  }
				  //If page hasn't been created yet
				  else {
					  //echo 'Page is not yet created';
					  //create the page from the default pages array
					  wp_insert_post($defaultpage);
				  }
			  } // end of foreach
		  } // end of if theme is activated
  // end of function pagefind
	  }
	}

// End if Init_theme
    /**
     * 
     * @param type $pagetitle
     * @return type array
     * 
     * Checks if the page exists and return its info
     */
	if (!function_exists('pagefind')) {
	  function pagefind($pagetitle) {
		  $pages = get_pages();
		  foreach ($pages as $page) {
			  //echo $page->post_title;
			  if ($page->post_title == $pagetitle) {
				  echo $page->post_title . '<br />';
				  return $page;
			  }
		  } // end of for each
	  }
	}

//removes feeds
    remove_action('wp_head', 'feed_links_extra', 3); // This is the main code that removes unwanted RSS Feeds
    remove_action('wp_head', 'feed_links', 2); // Removes Post and Comment Feeds
    
    add_action('after_setup_theme', 'remove_post_format', 15);
	if (!function_exists('remove_post_format')) {
	  function remove_post_format() {
		  remove_theme_support('post-formats');
	  }
	}



//change logo
add_filter("login_headerurl","tiw_site_url");
/*function to return the current site url */
if (!function_exists('tiw_site_url')) {
  function tiw_site_url($url){
	 return get_bloginfo('siteurl');//return the current wp blog url
  }
}

add_filter("login_headertitle","tiw_login_header_title");
 
/*function to return the description of the current blog*/
if (!function_exists('tiw_login_header_title')) {
  function tiw_login_header_title($message){
	 return get_bloginfo('description'); /*return the description of current blog */
  }
}

add_action("login_head", "my_login_head");
if (!function_exists('my_login_head')) {
  function my_login_head() {
	  global $wb_ent_options;
	 echo "
	 <style>
	 body.login #login h1 a {
		background: url('".$wb_ent_options['loginlogo']."') no-repeat scroll center top transparent;
		height: 160px;
		width: 320px;
	 }
	 </style>
	 ";
  }  
}
/*
if (!function_exists('autoSelectLibrary')) {
  function autoSelectLibrary($postId) {
	  global $wb_ent_options;    

	  if( get_post_type($postId) != 'post' ){
		 return;
	  }

	  $libCat = intval($wb_ent_options['videocats']['library']);
	  $privateCat = intval($wb_ent_options['videocats']['private']);

	  $currentPostTerms = wp_get_post_terms($postId, 'category', array("fields" => "all"));

	  //echo '$currentPostTerms is '.print_r($currentPostTerms, true);

	  $postCategories = array();
	  foreach($currentPostTerms as $currentTerm){
		  if( $currentTerm->term_id == $wb_ent_options['videocats']['library'] ){
			  continue;
		  }
		  $postCategories[] = $currentTerm->term_id;
	  }

	  //echo '$postCategories is '.print_r($postCategories, true);

	  //get all children of library
	  $allLibraryChildren = get_term_children( $libCat, 'category' );

	  $libraryChildrenSelected = array();
	  if( count($postCategories) > 0 && count($allLibraryChildren) > 0 ){
		$libraryChildrenSelected = array_intersect($postCategories, $allLibraryChildren);   
	  }



	  $currentPostChannelCat = wp_get_post_terms($postId, 'channel', array("fields" => "all"));

	  //echo '$currentPostChannelCat is '.print_r($currentPostChannelCat, true);

	  $postChannelCategories = array();
	  foreach($currentPostChannelCat as $currentCatTerm){
		  $postChannelCategories[] = $currentCatTerm->term_id;
	  }    

	  if( count($currentPostChannelCat) > 0 && count($postChannelCategories) > 0 && count($libraryChildrenSelected) <= 0){
		   wp_delete_object_term_relationships($postId, 'category');
		   wp_set_post_terms($postId, $privateCat, 'category', true);
	  }
	  elseif(
		( 
		   !( 
			  trim($wb_ent_options['videocats']['private']) != '' && 
			  intval($wb_ent_options['videocats']['private']) != 0 && 
			  in_array($wb_ent_options['videocats']['private'], $postCategories)
		   ) &&
		   !( 
			  trim($wb_ent_options['videocats']['unlisted']) != '' && 
			  intval($wb_ent_options['videocats']['unlisted']) && 
			  in_array($wb_ent_options['videocats']['unlisted'], $postCategories)
		   )

		)   && 
		(
		   count($postCategories) <= 0 || count($libraryChildrenSelected) > 0
		) 

	  ){
		 wp_set_post_terms($postId, $libCat, 'category', true);     
	  }


  }
}
add_action('save_post', 'autoSelectLibrary');
*/
if (!function_exists('wbEncrypt')) {
  function wbEncrypt($string, $newClientCode = null, $newChannelName = null) {
	  /*
	 $wb_ent_options = get_option('wb_ent_options');

	 echo '<br />$userDisplayedClientCode is '.$userDisplayedClientCode;
	 echo '<br />$userDisplayedClientName is '.$userDisplayedClientName;  

	 $displayedClientCode = ( !is_null($newClientCode) && trim($newClientCode) != '' ) ?  $newClientCode : $wb_ent_options['clientcode'];
	 $displayedChannelName = ( !is_null($newChannelName) && trim($newChannelName) != '' ) ?  $newChannelName : $wb_ent_options['channelName'];

	 echo '<br />$displayedClientCode is '.$displayedClientCode;
	 echo '<br />$displayedChannelName is '.$displayedChannelName;
	 echo '<br />$string is '.$string;

	 $clientSalt = $displayedClientCode.$displayedChannelName;

	 echo '$clientSalt is '.$clientSalt;

	 $encryptedString = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($clientSalt), $string, MCRYPT_MODE_CBC, md5(md5($clientSalt))));
	 */
	  $encryptedString = $string;
	 return $encryptedString;
  }
}

if (!function_exists('wbDecrypt')) {
  function wbDecrypt($string, $newClientCode = null, $newChannelName = null) {
	  /*
	  $wb_ent_options = get_option('wb_ent_options');
	 //header('Content-Type: text/html; charset=iso-8859-1');

	 echo '<br />$newClientCode is '.$newClientCode;
	 echo '<br />$newChannelName is '.$newChannelName;  

	 $displayedClientCode = ( !is_null($newClientCode) && trim($newClientCode) != '' ) ?  $newClientCode : $wb_ent_options['clientcode'];
	 $displayedChannelName = ( !is_null($newChannelName) && trim($newChannelName) != '' ) ?  $newChannelName : $wb_ent_options['channelName'];

	 echo '<br />$displayedClientCode is '.$displayedClientCode;
	 echo '<br />$displayedChannelName is '.$displayedChannelName;
	 echo '<br />$string is '.$string;

	 $clientSalt = $displayedClientCode.$displayedChannelName;
	 echo '$clientSalt is '.$clientSalt.'<br />';
	 $decryptedString = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($clientSalt), base64_decode($string), MCRYPT_MODE_CBC, md5(md5($clientSalt))), "\0");

	 */
	  $decryptedString = $string;
	 return $decryptedString;

  }
}

/**
This function will check a string for shortcodes and remove it.
$codeTag -- this is the tag for the specific shortcode. for ex. in the shortcode [VIDEO videoid="1178440566001" mode="ceoshow" /], the $codeTag will be "VIDEO"
$string -- this will be the string that will be checked for shortcodes
*/
if (!function_exists('removeShortCode')) {
   function removeShortCode($codeTag, $string){
      return preg_replace("/\[".$codeTag." (.*)\/\]/i", "", $string);
      
   }
}

/**
Shortcode function for inserting a brightcove video
*/
if (!function_exists('brightcove_shortcode')) {
   function brightcove_shortcode($atts, $content = null) {
       global $wb_ent_options;
      extract(shortcode_atts(array(
            'videoid'  => '',
            'mode' => 'default',
            'publisher' => '',
            'player' => '',   
            'width'  => '640',
            'height' => '360'                   
            ), $atts));
      
      if( trim($publisher) == '' && trim($player) == '' ){
          if( $mode == 'default' ){
               $publisher = $wb_ent_options['brightcoveinfo']['publisherid'];
               $player = $wb_ent_options['bcplayers']['library'];              
          }
          elseif( trim($mode) != '' ){
               $publisher = $wb_ent_options['vidshortcode']['pubId'];
               $player = $wb_ent_options['vidshortcode']['playerId'];                  
          }

      }
      
     $output = '
<!-- start of player -->
<style type="text/css">

    .outer-container {
        position: relative;
        padding-bottom: 56.25%;         
        height: 0;
        overflow: hidden;
    }
    .BrightcoveExperience {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    video {
        max-width: 100%;
        height: auto;
    }
    .outer-container  iframe,  
    .outer-container  object,  
    .outer-container  embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
</style>
<div id="playerDiv">

    <div id="player">
        <div style="display:none">

        </div>

        <script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>
        <script src="http://admin.brightcove.com/js/APIModules_all.js" type="text/javascript"></script> 
        <div id="container'.$videoid.'" class="outer-container">
            <object id="myExperience'.$videoid.'" class="BrightcoveExperience" style="width:100%;">
                <param name="bgcolor" value="#FFFFFF" />
                <param name="width" value="'.$width.'" />      
                <param name="height" value="'.$height.'" />                
                <param name="playerID" value="'.$player.'" />
                <param name="publisherID" value="'.$publisher.'"/>
                <param name="isVid" value="true" />
                <param name="isUI" value="true" />
                <param name="wmode" value="transparent" />
                <param name="includeAPI" value="true" />
                <param name="templateLoadHandler" value="onTemplateLoad" />
                <param name="templateReadyHandler" value="onTemplateReady" />
                <param name="@videoPlayer" value="'.$videoid.'" />
            </object>
        </div>
        <!--[if gt IE 9]>
        <script type="text/javascript">brightcove.createExperiences();</script>
        <![endif]-->';
        
        $mobile = mobile_device_detect();


        if (is_array($mobile)) {
            $device = $mobile['0'];


            if ($mobile) {
                $output .= '
                <script type="text/javascript">
                    var player,
                            APIModules,
                            videoPlayer,
                            experienceModule;

                    function onTemplateLoad(experienceID) {
                        console.log("EVENT: onTemplateLoad");
                        player = brightcove.api.getExperience(experienceID);
                        APIModules = brightcove.api.modules.APIModules;
                    }

                    function onTemplateReady(evt) {
                        console.log("EVENT.onTemplateReady");
                        videoPlayer = player.getModule(APIModules.VIDEO_PLAYER);
                        experienceModule = player.getModule(APIModules.EXPERIENCE);
                        videoPlayer.play();

                        videoPlayer.getCurrentRendition(function(renditionDTO) {
                            var newPercentage = (renditionDTO.frameHeight / renditionDTO.frameWidth) * 100;
                            newPercentage = newPercentage + "%";
                            console.log("Video Width = " + renditionDTO.frameWidth + " and Height = " + renditionDTO.frameHeight);
                            console.log("New Percentage = " + newPercentage);
                            document.getElementById("container1").style.paddingBottom = newPercentage;
                            var evt = document.createEvent(\'UIEvents\');
                            evt.initUIEvent(\'resize\', true, false, 0);
                            window.dispatchevent(evt);
                        });
                    }

                    window.onresize = function(evt) {
                        console.log("in resize function");
                        var playerDivWidth = $(\'#vid-post .outer-container\').width();
                        var playerDivHeight = ((playerDivWidth / 16) * 9);
                        var resizeWidth = document.getElementById("myExperience'.$videoid.'").offsetWidth,
                                resizeHeight = document.getElementById("myExperience'.$videoid.'").offsetHeight;
        //experienceModule.setSize(resizeWidth, resizeHeight);
                        experienceModule.setSize(playerDivWidth, playerDivHeight);
                    }
                </script>';
                
            } // end of if($mobile)
        } // end of is array($mobile)        
$output .= '
    </div>

</div>
<!-- end of player -->   ';      
         return $output;
   }
}

add_shortcode($wb_ent_options['vidshortcode']['tag'], 'brightcove_shortcode');


add_action( 'save_post', 'get_mobile_form_info' );

if (!function_exists('get_mobile_form_info')) {
  function get_mobile_form_info( $post_id ) {

	 global $wpdb;


	 // Bail if we're doing an auto save  
	  if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;


	 $onoffswitch = $_POST['onoffswitch'];
	 $feature = $_POST['feature'];

	 if($onoffswitch == 'on'){
		$onoffswitch = 1;
	 }else{
		$onoffswitch = 0;
	 }

	 if($feature == 'on'){
		$feature = 1;
	 }else{
		$feature = 0;
	 }

	 $values = array('pushMobile'=>$onoffswitch,'makeFeature'=>$feature);
	 $values = json_encode($values);

	 update_post_meta($post_id, 'mobile_option_RSS', $values);



  }
}

if( $wb_ent_options['apiinfo']['displayall'] != 'true' ){
    add_action( 'add_meta_boxes', 'cd_meta_box_add' );     
}


if (!function_exists('cd_meta_box_add')) {
  function cd_meta_box_add(){  
	 add_meta_box( 'push-to-mobile-meta-box', 'Push to mobile', 'meta_box_form_mobile', 'post', 'side', 'high' );  
  }  
}
   
if (!function_exists('meta_box_form_mobile')) {
	function meta_box_form_mobile(){ 


	   global $wpdb;

	   global $post;
	   $post_id = $post->ID;



	   //check if mobile ON
	   $mobileOn = '' ;
	   $makeFeature = '';
	   $pushMobilePosts = $wpdb->get_results( 
		  "
		  SELECT post_id, meta_value 
		  FROM $wpdb->postmeta
		  WHERE post_id = $post_id AND meta_key = 'mobile_option_RSS' ORDER BY meta_id  DESC 

		  ");

	   //check just the first value
	   foreach ( $pushMobilePosts as $postInfo ) {

			 $jsonValues = json_decode($postInfo->meta_value);


			 if($jsonValues->pushMobile == 1){

				$mobileOn = 1;

			 } //end mobile true


			 if($jsonValues->makeFeature == 1){

				$makeFeature = 1;

			 } //end mobile true



			 break;

	   } //end for each





	   echo '



	   <style>
	   .onoffswitch {
		  position: relative; width: 90px;
		  -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
	   }
	   .onoffswitch-checkbox {
		  display: none;
	   }
	   .onoffswitch-label {
		  display: block; overflow: hidden; cursor: pointer;
		  border: 2px solid #999999; border-radius: 20px;
	   }
	   .onoffswitch-inner {
		  width: 200%; margin-left: -100%;
		  -moz-transition: margin 0.3s ease-in 0s; -webkit-transition: margin 0.3s ease-in 0s;
		  -o-transition: margin 0.3s ease-in 0s; transition: margin 0.3s ease-in 0s;
	   }
		   .onoffswitch-inner:before, .onoffswitch-inner:after {
		   float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
		   font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
		   -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box;
	   }
	   .onoffswitch-inner:before {
		  content: "ON";
		  padding-left: 10px;
		  background-color: #2FCCFF; color: #FFFFFF;
	   }
	   .onoffswitch-inner:after {
		  content: "OFF";
		  padding-right: 10px;
		  background-color: #EEEEEE; color: #999999;
		  text-align: right;
	   }
	   .onoffswitch-switch {
			 width: 18px; margin: 6px;
			 background: #FFFFFF;
			 border: 2px solid #999999; border-radius: 20px;
			 position: absolute; top: 0; bottom: 0; right: 56px;
			 -moz-transition: all 0.3s ease-in 0s; -webkit-transition: all 0.3s ease-in 0s;
			 -o-transition: all 0.3s ease-in 0s; transition: all 0.3s ease-in 0s;
	   }
	   .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
			 margin-left: 0;
	   }
	   .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
		  right: 0px;
	   }
		#myonoffswitch.onoffswitch-checkbox {
			display: none;
		}
	   </style>


	   ';

	   //if less then IE8 
	   if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])){

			 echo '   
				<div id="wrapperButtons" style="width: 300px; height: 30px; padding: 10px;" >    

			 <div  style="width: 90px; float: left; margin-left: 12px; ">   
				  <div >
				   <label> <input type="checkbox" name="onoffswitch" id="myonoffswitch" ';


				if($mobileOn == 1){
				   echo 'checked';
				}    

		   echo ' > ON/OF F</labe>

				 </div> 
			  </div>
			 ';




		   }else{

			  //if not IE or IE 9 +

		  echo '   

		  <div id="wrapperButtons" style="width: 300px; height: 30px; padding: 10px;" >    

			 <div  style="width: 90px; float: left; margin-left: 12px; ">   
				  <div class="onoffswitch" >
					<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" ';


				if($mobileOn == 1){
				   echo 'checked';
				}    

		   echo '>
					<label class="onoffswitch-label" for="myonoffswitch">
					<div class="onoffswitch-inner"></div>
					<div class="onoffswitch-switch"></div>
					</label>
				 </div> 
			  </div>

			 ';

		  }  //end checking If IE8 -


		  echo ' 

		  <div style="width: 200px; margin-top: 6px ; margin-left: 116px; ">
			   <label for="feature"><input  id="feature" name="feature" type="checkbox"  ';

		  if($makeFeature == 1){
				echo 'checked';
			 }       


	   echo '>
			   Make featured</label>
		  </div>

	   </div>


	   ';




	}
}

add_action( 'save_post', 'save_post_specific_banners' );


if (!function_exists('save_post_specific_banners')) {
  function save_post_specific_banners( $post_id ) {

	 global $wpdb;


	 /*
	  * We need to verify this came from our screen and with proper authorization,
	  * because the save_post action can be triggered at other times.
	  */

	 // Check if our nonce is set.
	 if ( ! isset( $_POST['post_specific_banners_box_nonce'] ) ) {
		return;
	 }

	 // Verify that the nonce is valid.
	 if ( ! wp_verify_nonce( $_POST['post_specific_banners_box_nonce'], 'post_specific_banners_box' ) ) {
		return;
	 }

	 // If this is an autosave, our form has not been submitted, so we don't want to do anything.
	 if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	 }

	 // Check the user's permissions.
	 if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
		   return;
		}

	 } else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
		   return;
		}
	 }



	 // Make sure that it is set.
	 if ( isset( $_POST['wb_column_ad_file'] ) ) {
		  // Sanitize user input.
		  $wb_column_ad_file = sanitize_text_field( $_POST['wb_column_ad_file'] );             
	 }

	 if ( isset( $_POST['wb_column_ad_title'] ) ) {
		  // Sanitize user input.
		  $wb_column_ad_title = sanitize_text_field( $_POST['wb_column_ad_title'] );              
	 }   

	 if ( isset( $_POST['wb_column_ad_url'] ) ) {
		  // Sanitize user input.
		  $wb_column_ad_url = sanitize_text_field( $_POST['wb_column_ad_url'] );      
	 }     

	 if ( isset( $_POST['wb_order_number'] ) ) {
		  // Sanitize user input.
		  $wb_order_number = sanitize_text_field( $_POST['wb_order_number'] );     
	 }   

	 if ( isset( $_POST['wb_column_custom_code'] ) ) {
		  // Sanitize user input.
		  $wb_column_custom_code = ( $_POST['wb_column_custom_code'] );   
	 }     

	 if ( isset( $_POST['wb_column_ad_display_on_home'] ) ) {
		  // Sanitize user input.
		  $wb_column_ad_display_on_home = ( $_POST['wb_column_ad_display_on_home'] );  
	 }     

	 if ( isset( $_POST['wb_wide_ad_file'] ) ) {
		  // Sanitize user input.
		  $wb_wide_ad_file = sanitize_text_field( $_POST['wb_wide_ad_file'] );     
	 } 

	 if ( isset( $_POST['wb_wide_ad_title'] ) ) {
		  // Sanitize user input.
		  $wb_wide_ad_title = sanitize_text_field( $_POST['wb_wide_ad_title'] );      
	 }    

	 if ( isset( $_POST['wb_wide_ad_url'] ) ) {
		  // Sanitize user input.
		  $wb_wide_ad_url = sanitize_text_field( $_POST['wb_wide_ad_url'] );    
	 } 

	 if ( isset( $_POST['wb_wide_custom_code'] ) ) {
		  // Sanitize user input.
		  $wb_wide_custom_code = ( $_POST['wb_wide_custom_code'] );    
	 }

	 if ( isset( $_POST['wb_wide_ad_display_on_home'] ) ) {
		  // Sanitize user input.
		  $wb_wide_ad_display_on_home = ( $_POST['wb_wide_ad_display_on_home'] );  
	 }    

	  //Update the post meta
	  // Make sure that it is set.
	  if ( isset( $_POST['columnAd'] ) ) {
	 // Sanitize user input.
	 $wb_column_ad_enable = sanitize_text_field( $_POST['columnAd'] );
	 // Update the meta field in the database.
	 update_post_meta( $post_id, 'wb_column_ad_enable_major', $wb_column_ad_enable );
	 update_post_meta( $post_id, 'wb_column_ad_display_on_home', $wb_column_ad_display_on_home );       


	  if (!empty($wb_column_custom_code)){
	  $poddata = Array(
	  'image' => '',
	  'url' => '',
	  'target' => '',
	  'title' => '',
	  'orderNumber' => $wb_order_number,
	  'customcode' => stripslashes($wb_column_custom_code),
	  'status' => 'active',
	  'displayOnHome' => $wb_column_ad_display_on_home

	  );
	  //echo "$key array ----- ";
	  //print_r($poddata);
	  update_post_meta($post_id, 'major1', $poddata);    
	  unset($poddata);    
	 }
	 else if(!empty($wb_column_ad_file) && !empty($wb_column_ad_url)){
	  $title = explode("/", $wb_column_ad_file);    
	  $poddata = Array(
	  'image' => $wb_column_ad_file,
	  'url' => $wb_column_ad_url,
	  'target' => '_blank',
	  'title' => $title[3],
	  'orderNumber' => $wb_order_number,
	  'customcode' => '',
	  'status' => 'active',
	  'displayOnHome' => $wb_column_ad_display_on_home
	  );
	  //echo "$key array ----- ";
	  //print_r($poddata);
	  update_post_meta($post_id, 'major1', $poddata);    
	  unset($poddata);      
	 }
	 }
         else{
            $poddata = Array(
            'image' => '',
            'url' => '',
            'target' => '_blank',
            'title' => '',
            'orderNumber' => '',
            'customcode' => '',
            'status' => 'deleted',
            'displayOnHome' => ''
            );
            //echo "$key array ----- ";
            //print_r($poddata);
            update_post_meta($post_id, 'major1', $poddata);   
        }


	 // Make sure that it is set.
   if ( isset( $_POST['bannerAd'] ) ) {
	  // Sanitize user input.
	  $wb_wide_ad_enable = sanitize_text_field( $_POST['bannerAd'] );
	  update_post_meta( $post_id, 'wb_column_ad_enable_wide', $wb_wide_ad_enable ); 
	  update_post_meta( $post_id, 'wb_wide_ad_display_on_home', $wb_wide_ad_display_on_home );

	  if (!empty($wb_wide_custom_code)){
	  $poddata = Array(
	  'image' => '',
	  'url' => '',
	  'target' => '',
	  'title' => '',
	  'orderNumber' => $wb_order_number,
	  'customcode' => stripslashes($wb_wide_custom_code),
	  'status' => 'active',
	  'displayOnHome' => $wb_wide_ad_display_on_home

	  );
	  //echo "$key array ----- ";
	  //print_r($poddata);
	  update_post_meta($post_id, 'wide', $poddata);    

	 }
	 else if(!empty($wb_wide_ad_file) && !empty($wb_wide_ad_url)){
	  $title = explode("/", $wb_wide_ad_file);    
	  $poddata = Array(
	  'image' => $wb_wide_ad_file,
	  'url' => $wb_wide_ad_url,
	  'target' => '_blank',
	  'title' => $title[3],
	  'orderNumber' => $wb_order_number,
	  'customcode' => '',
	  'status' => 'active',
	  'displayOnHome' => $wb_wide_ad_display_on_home
	  );
	  //echo "$key array ----- ";
	  //print_r($poddata);
	  update_post_meta($post_id, 'wide', $poddata);    

	 }   
	  unset($poddata); 
	}else{
          $poddata = Array(
          'image' => '',
          'url' => '',
          'target' => '_blank',
          'title' => '',
          'orderNumber' => '',
          'customcode' => '',
          'status' => 'deleted',
          'displayOnHome' => ''
          );
            //echo "$key array ----- ";
            //print_r($poddata);
          update_post_meta($post_id, 'wide', $poddata);   
       }

  }
}

/*
add_action( 'add_meta_boxes', 'post_specific_banners_box_add' );     



if (!function_exists('post_specific_banners_box_add')) {
  function post_specific_banners_box_add(){  
	 add_meta_box( 'post_specific_banners', 'Banner Manager', 'post_specific_banners_box', 'post', 'normal', 'high' );  
  }
}

if (!function_exists('post_specific_banners_box')) {
	function post_specific_banners_box($post){      
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'post_specific_banners_box', 'post_specific_banners_box_nonce' );
		$upload_dir = wp_upload_dir();

		$minorBanners = 0;
		$majorBanners = 0;
		$wideBanner = 0;
		$countAll = 0;
		$postbanner = get_post_meta($post->ID, 'major1');

		   foreach ($postbanner as $keySel => $postbannerinfo){
			   if($postbannerinfo[status] == 'active'){
			  $wb_column_ad_title = $postbannerinfo[title];
			  $wb_column_ad_file =  $postbannerinfo[image];          
			  $wb_column_ad_url =  $postbannerinfo[url];
			  $wb_order_number = $postbannerinfo[orderNumber];
			  $wb_column_custom_code = $postbannerinfo[customcode];
			  $wb_column_ad_display_on_home = $postbannerinfo[displayOnHome];
			   }
			   else{
			  $wb_column_ad_title = "";
			  $wb_column_ad_file =  "";
			  $wb_column_ad_url =  "";
			  $wb_order_number = "";
			  $wb_column_custom_code = "";
			  $wb_column_ad_display_on_home = "";
			   }

		   }

		echo '
		<script type="text/javascript">
			jQuery(document).ready( function($) {
				jQuery(\'#columnAd, #bannerAd, #columnAdTable input[type="checkbox"], #bannerAdTable input[type="checkbox"]\').change(function() {
				   if( this.checked ){
					  this.value = true;
				   }
				   else{
					  this.value = false;
				   }
				});
			});
		</script>    
		<script type="text/javascript">
		(function (a) {
			if (window.filepicker) {
				return
			}
			var b = a.createElement("script");
			b.type = "text/javascript";
			b.async = !0;
			b.src = ("https:" === a.location.protocol ? "https:" : "http:") + "//api.filepicker.io/v1/filepicker.js";
			var c = a.getElementsByTagName("script")[0];
			c.parentNode.insertBefore(b, c);
			var d = {};
			d._queue = [];
			var e = "pick,pickMultiple,pickAndStore,read,write,writeUrl,export,convert,store,storeUrl,remove,stat,setKey,constructWidget,makeDropPane".split(",");
			var f = function (a, b) {
				return function () {
					b.push([a, arguments])
				}
			};
			for (var g = 0; g < e.length; g++) {
				d[e[g]] = f(e[g], d._queue)
			}
			window.filepicker = d
		})(document);

		var apiKey = \'A0XL2VDAScq3G8FCdhVH8z\';
		var full_img_url ="";

		filepicker.setKey(apiKey);

		function wb_banner_upload(key) {

			filepicker.setKey(apiKey);
			filepicker.pickAndStore({
				mimetype: "image/*",
				folders: true
			}, {
				location: "S3",
				container: "wb.banner.manager",
				access: \'public\'
			}, function (InkBlobs) {
				console.log(JSON.stringify(InkBlobs));
				var metadata = (JSON.stringify(InkBlobs));
				//alert(key);   
				var img = new Image();
				img.src = InkBlobs[0].url;
				var imagename = InkBlobs[0].filename;
				full_img_url = "http://" + InkBlobs[0].container + ".s3.amazonaws.com/" + InkBlobs[0].key;
				//var n = key.substr(0, 4); 
				//If key contains certain word : validate image sizes
				//alert(full_img_url);

				img.onload = function () {
					if (key.search("mino") > -1) {
						if (this.width == 300 && this.height == 100) {                                                
							document.getElementById("wb_column_ad_file").value = full_img_url;
							document.getElementById(key + "_file_container").innerHTML = imagename+" has been successfully uploaded.";
							document.getElementById(key + "_image_display").src = full_img_url;
							document.getElementById(key + "_image_display").style.display = "block";
						}
						else {
							document.getElementById(key + "_file_container").innerHTML = "<strong style=\'color: #f00;\'>Image must be 300x100.</strong>";
						}
					}
					else if (key.search("majo") > -1) {
						if (this.width == 300 && this.height == 250) {
							document.getElementById("wb_column_ad_file").value = full_img_url;
							document.getElementById(key + "_file_container").innerHTML = imagename+" has been successfully uploaded.";
							document.getElementById(key + "_image_display").src = full_img_url;
							document.getElementById(key + "_image_display").style.display = "block";                        
						}
						else {
							document.getElementById(key + "_file_container").innerHTML = "<strong style=\'color: #f00;\'>Image must be 300x250.</strong>";
						}
					}
					else if (key.search("wide") > -1) {
						if (this.width == 468 && this.height == 60) {
							document.getElementById("wb_wide_ad_file").value = full_img_url;
							document.getElementById(key + "_file_container").innerHTML = imagename+" has been successfully uploaded.";
							document.getElementById(key + "_image_display").src = full_img_url;
							document.getElementById(key + "_image_display").style.display = "block";                        
						}
						else {
							document.getElementById(key + "_file_container").innerHTML = "<strong style=\'color: #f00;\'>Image must be 468x60.</strong>";
						}
					}            
				}

			});

		}
		function showDivForAd(element){
			var currentCheckbox = element;
			if( jQuery(currentCheckbox).prop( "checked" ) ){
				jQuery(currentCheckbox).val(1);
				jQuery("#"+currentCheckbox.id+"Table").show();
			}
			else{
				jQuery(currentCheckbox).val(0);
				jQuery("#"+currentCheckbox.id+"Table").hide();
			}        
		}
		</script>
		<div class="post-meta-box-wrapper">
		<h4>
			<label for="wb_order_number">Order Number</label>
			<input type="text" id="wb_order_number" name="wb_order_number" value="' . esc_attr( $wb_order_number ) . '"/><br />
		</h4>    ';

		$displayColumnAdBox = ' style="display: none;" ';
		$checkedColumnAdBox = '';
		if( trim( $wb_column_ad_file ) != '' || trim( $wb_column_custom_code ) != '' ){
		  $displayColumnAdBox = '';
		  $checkedColumnAdBox = ' checked="checked" ';   
		}

		if( $wb_column_ad_display_on_home ){
		   $wb_column_ad_display_on_home_checked = ' checked="checked" ';
		}


		echo '
		<h3 id="columnAdHeader"><em><input type="checkbox" name="columnAd" id="columnAd" onchange="showDivForAd(this);" '.$checkedColumnAdBox.'/><label for="columnAd">Column Ad</label></em></h3>
		<table id="columnAdTable" '.$displayColumnAdBox.'>
			<tr>
				<th><label for="wb_column_ad_display_on_home">Display on Home Page</label></th>
				<td><input type="checkbox" style="width: 20px;" id="wb_column_ad_display_on_home" name="wb_column_ad_display_on_home" '.$wb_column_ad_display_on_home_checked.' value="' . ( $wb_column_ad_display_on_home ) . '" /></td>
			</tr>
			<tr>
				<th><label for="wb_column_ad_title">Ad title</label></th>
				<td><input type="text" id="wb_column_ad_title" name="wb_column_ad_title" value="' . esc_attr( $wb_column_ad_title ) . '" /></td>
			</tr>
			<tr>
				<th><label for="wb_column_ad_file">Upload File</label> </th>
				<td><div class="upload_wrapper">
				  <div class="upload_btn_wrapper">
					<input type="button" onclick="return wb_banner_upload(this.id);" value="Browse files" name="major1" class="css_btn_upload input_bn btn btn-default" id="major1">
				  </div>  
				   <span id="major1_file_container"></span> 
					';

					if( trim( $wb_column_ad_file ) != '' ){
						echo '<img id="major1_image_display" src="'.esc_attr( $wb_column_ad_file ).'" /></br >';
					}
					else{
						echo '<img id="major1_image_display" src="" style="display: none;"/></br >';
					}

		echo '
					<input type="hidden" id="wb_column_ad_file" name="wb_column_ad_file" value="' . esc_attr( $wb_column_ad_file ) . '"  />
				</div>
				</td>
			</tr>
			<tr>
				<th><label for="wb_column_ad_url">Ad URL</label> </th>
				<td><input type="text" id="wb_column_ad_url" name="wb_column_ad_url" value="' . esc_attr( $wb_column_ad_url ) . '"  /></td>
			</tr>
			<tr>
				<th><label for="wb_column_custom_code">Custom Code</label> </th>
				<td><textarea id="wb_column_custom_code" name="wb_column_custom_code" size="100%">' . esc_attr( $wb_column_custom_code ) . '</textarea><br />       </td>
			</tr>            
		</table>
		   ';       


		$postwidebanner = get_post_meta($post->ID, 'wide');

		   foreach ($postwidebanner as $keySel => $postwidebannerinfo){
			  if($postwidebannerinfo[status] == "active"){

			  $wb_wide_ad_title =  $postwidebannerinfo[title]; 
			  $wb_wide_ad_file =  $postwidebannerinfo[image];          
			  $wb_wide_ad_url =  $postwidebannerinfo[url];
			  $wb_order_number = $postwidebannerinfo[orderNumber];
			  $wb_wide_custom_code = $postwidebannerinfo[customcode];
			  $wb_wide_ad_display_on_home = $postbannerinfo[displayOnHome];
			  }else{
			  $wb_wide_ad_title =  ""; 
			  $wb_wide_ad_file =  "";          
			  $wb_wide_ad_url =  "";
			  $wb_order_number = "";
			  $wb_wide_custom_code  = "";
			  $wb_wide_ad_display_on_home = "";
			  }
		   }

		$displayWideAdBox = ' style="display: none;" ';
		$checkedWideAdBox = '';
		if( trim( $wb_wide_ad_file ) != '' || trim( $wb_wide_custom_code ) != '' ){
		  $displayWideAdBox = '';   
		   $checkedWideAdBox = ' checked="checked" ';   
		}       

		if( $wb_wide_ad_display_on_home ){
		   $wb_wide_ad_display_on_home_checked = ' checked="checked" ';
		}

		echo '    
		<h3 id="bannerAdHeader"><em><input type="checkbox" name="bannerAd" id="bannerAd" onchange="showDivForAd(this);" '.$checkedWideAdBox.'/><label for="bannerAd">Banner</label></em></h3>

		<table id="bannerAdTable" '.$displayWideAdBox.'>
			<tr>
				<th><label for="wb_wide_ad_display_on_home">Display on Home Page</label></th>
				<td><input type="checkbox" style="width: 20px;" id="wb_wide_ad_display_on_home" name="wb_wide_ad_display_on_home" '.$wb_wide_ad_display_on_home_checked.' value="' . ( $wb_wide_ad_display_on_home ) . '" /></td>
			</tr>    
			<tr>
				<th><label for="wb_wide_ad_title">Ad title</label></th>
				<td><input type="text" id="wb_wide_ad_title" name="wb_wide_ad_title" value="' . esc_attr( $wb_wide_ad_title ) . '" /></td>
			</tr>
			<tr>
				<th><label for="wb_wide_ad_file">Upload File</label> </th>
				<td><div class="upload_wrapper">
				<div class="upload_btn_wrapper">
					<input type="button" onclick="return wb_banner_upload(this.id);" value="Browse files" name="wide1" class="css_btn_upload input_bn" id="wide1">
				 </div>
					<span id="wide1_file_container"></span>
					';

					if( trim( $wb_wide_ad_file ) != '' ){
						echo '<img id="wide1_image_display" src="'.esc_attr( $wb_wide_ad_file ).'" /></br >';
					}
					else{
						echo '<img id="wide1_image_display" src="" style="display: none;"/></br >';
					}

		echo '                    
					<input type="hidden" id="wb_wide_ad_file" name="wb_wide_ad_file" value="' . esc_attr( $wb_wide_ad_file ) . '"  />
				</div></td>
			</tr>
			<tr>
				<th><label for="wb_wide_ad_url">Ad URL</label> </th>
				<td><input type="text" id="wb_wide_ad_url" name="wb_wide_ad_url" value="' . esc_attr( $wb_wide_ad_url ) . '"  /></td>
			</tr>
			<tr>
				<th><label for="wb_wide_custom_code">Custom Code</label> </th>
				<td><textarea id="wb_wide_custom_code" name="wb_wide_custom_code" size="100%">' . esc_attr( $wb_wide_custom_code ) . '</textarea><br />       </td>
			</tr>            
		</table></div>
		   ';           

	}
}
*/

/**
 * Adds the meta box stylesheet when appropriate
 */
if (!function_exists('prfx_admin_styles')) {
  function prfx_admin_styles(){
	  global $typenow;
	  if( $typenow == 'post' ) {
		  wp_enqueue_style( 'prfx_meta_box_styles', get_template_directory_uri() . '/meta-box-styles.css' );
	  }
  }
}
add_action( 'admin_print_styles', 'prfx_admin_styles' );

//Add some style to the theme settings for readability
add_action( 'admin_enqueue_scripts', 'load_admin_style' );

if (!function_exists('load_admin_style')) {
  function load_admin_style() {
  wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/css/theme-settings-style.css');
  }
}


/*
add_action( 'save_post', 'run_post_info_cron' );
add_action( 'delete_post', 'run_post_info_cron' );
if (!function_exists('run_post_info_cron')) {
  function run_post_info_cron($post_id){
	  $currentPost = get_post($post_id);   
	  $wb_ent_options = get_option('wb_ent_options');

	  if($currentPost->post_type == 'post'){
		  exec("/usr/local/bin/php -q /home/".$wb_ent_options['serveruser']."/".$wb_ent_options['publicdir']."/wp-content/themes/enterprise/resources/postInfoJsonCron.php  > /dev/null &");
	  }
  }
}
*/
define('WP_DEBUG', false);

$site_url = parse_url( get_site_url() );
if( $site_url['scheme'] != 'https'){
  $_SERVER['HTTPS'] = false;
}

function remove_quick_edit( $actions ) {
	unset($actions['inline hide-if-no-js']);
	return $actions;
}
//Remove comment in the condition to enable quick edit function for specific user capabilities
//if ( !current_user_can('administrator') ) {
	add_filter('post_row_actions','remove_quick_edit', 10, 1);
//}

//Custom CSS to hide edit in both top and bottom bulk list function
add_action('admin_head', 'wb_custom_css');
function wb_custom_css() {
	echo '<style>
		    #bulk-action-selector-top .hide-if-no-js,
		    #bulk-action-selector-bottom .hide-if-no-js{
			display: none;
		    }
		  </style>';
}
add_filter('upload_mimes','restrict_mime');

function restrict_mime($mimes) {

    $mimes = array(
	// Image formats
	'jpg|jpeg|jpe'                 => 'image/jpeg',
	'gif'                          => 'image/gif',
	'png'                          => 'image/png',
	'bmp'                          => 'image/bmp',
	'tif|tiff'                     => 'image/tiff',
	'ico'                          => 'image/x-icon',
	// Text formats
	'txt|asc|c|cc|h'               => 'text/plain',
	'csv'                          => 'text/csv',
	'tsv'                          => 'text/tab-separated-values',
	'ics'                          => 'text/calendar',
	'rtx'                          => 'text/richtext',
	'css'                          => 'text/css',
	'htm|html'                     => 'text/html',
	// Misc application formats
	'rtf'                          => 'application/rtf',	
	'pdf'                          => 'application/pdf',	
	'class'                        => 'application/java',
	'tar'                          => 'application/x-tar',
	'zip'                          => 'application/zip',
	'gz|gzip'                      => 'application/x-gzip',
	'rar'                          => 'application/rar',
	'7z'                           => 'application/x-7z-compressed',	
    //'mp3'                       => 'audio/mpeg3',
	'mp3'                       => 'audio/mpeg',
    //'mp3'                       => 'application/octet-stream',	
	
	// Video formats
    'wmv'                          => 'video/x-ms-wmv',
    'avi'                          => 'video/avi',
    'mpeg|mpg|mpe'                 => 'video/mpeg',
    'mp4|m4v'                      => 'video/mp4',
    //'mkv'  
	
	// MS Office formats
	'doc'                          => 'application/msword',
	'pot|pps|ppt'                  => 'application/vnd.ms-powerpoint',
	'wri'                          => 'application/vnd.ms-write',
	'xla|xls|xlt|xlw'              => 'application/vnd.ms-excel',
	'mdb'                          => 'application/vnd.ms-access',
	'mpp'                          => 'application/vnd.ms-project',
	'docx'                         => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
	'docm'                         => 'application/vnd.ms-word.document.macroEnabled.12',
	'dotx'                         => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
	'dotm'                         => 'application/vnd.ms-word.template.macroEnabled.12',
	'xlsx'                         => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
	'xlsm'                         => 'application/vnd.ms-excel.sheet.macroEnabled.12',
	'xlsb'                         => 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
	'xltx'                         => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
	'xltm'                         => 'application/vnd.ms-excel.template.macroEnabled.12',
	'xlam'                         => 'application/vnd.ms-excel.addin.macroEnabled.12',
	'pptx'                         => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
	'pptm'                         => 'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
	'ppsx'                         => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
	'ppsm'                         => 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
	'potx'                         => 'application/vnd.openxmlformats-officedocument.presentationml.template',
	'potm'                         => 'application/vnd.ms-powerpoint.template.macroEnabled.12',
	'ppam'                         => 'application/vnd.ms-powerpoint.addin.macroEnabled.12',
	'sldx'                         => 'application/vnd.openxmlformats-officedocument.presentationml.slide',
	'sldm'                         => 'application/vnd.ms-powerpoint.slide.macroEnabled.12',
	'onetoc|onetoc2|onetmp|onepkg' => 'application/onenote',
	
	// OpenOffice formats
	'odt'                          => 'application/vnd.oasis.opendocument.text',
	'odp'                          => 'application/vnd.oasis.opendocument.presentation',
	'ods'                          => 'application/vnd.oasis.opendocument.spreadsheet',
	'odg'                          => 'application/vnd.oasis.opendocument.graphics',
	'odc'                          => 'application/vnd.oasis.opendocument.chart',
	'odb'                          => 'application/vnd.oasis.opendocument.database',
	'odf'                          => 'application/vnd.oasis.opendocument.formula',
	
	// WordPerfect formats
	'wp|wpd'                       => 'application/wordperfect',
	
	// iWork formats
	'key'                          => 'application/vnd.apple.keynote',
	'numbers'                      => 'application/vnd.apple.numbers',
	'pages'                        => 'application/vnd.apple.pages',
    );

    return $mimes;
}

function myplugin_tinymce_buttons( $buttons ) {	
	$remove = array( 'wp_more', 'spellchecker', 'dfw', 'wp_adv'  );        
	return array_diff( $buttons, $remove );
 }
add_filter( 'mce_buttons', 'myplugin_tinymce_buttons' );
add_filter( 'mce_buttons_2', 'myplugin_tinymce_buttons' );
remove_action('init', 'poll_tinymce_addbuttons');

function wpse_edit_footer() {
    add_filter( 'admin_footer_text', 'wpse_edit_text', 11 );
    add_filter( 'update_footer', '' );
}

function wpse_edit_text($content) {
    return "";
}

add_action( 'admin_init', 'wpse_edit_footer' );

function custom_manage_posts( $columns ) {
  unset($columns['custom-fields']);
  return $columns;
}

function custom_manage_posts_init() {
  add_filter( 'manage_posts_columns' , 'custom_manage_posts' );
}
add_action( 'admin_init' , 'custom_manage_posts_init' );

function wb_mail_name( $email ){
  $wb_ent_options = get_option('wb_ent_options');
  return $wb_ent_options['channelname']; // new email name from sender.
}
add_filter( 'wp_mail_from_name', 'wb_mail_name' );



/*
 * This are updates for Brightcove API
 *
 */
global $bc_account_id;
$bc_account_id = $wb_ent_options['brightcoveinfo']['publisherid'];
// Generate Authentication Token //
if (!function_exists('wb_get_auth_token_version_4')) {
	function wb_get_auth_token_version_4(){
		global $bc_account_id;
		$data 		= array();
		$id 		= "559779e8e4b072e9641b841d"; //from settings
		$client_id     	= "07aa7b60-2e1c-4775-8bc5-e3f5220d2d8d"; //API From settings
		$client_secret 	= "6a8-bE1KqdzoAbxupjTiDpEriPI4lZHUDS2mxDfU697lWuCRJNRJlJchL-ZbUCsPmC3lj-r8uram6vpPKldFmg"; //API From settings
		$auth_string   	= "{$client_id}:{$client_secret}";
		$request       	= "https://oauth.brightcove.com/v4/access_token?grant_type=client_credentials";
		$ch            	= curl_init($request);
		curl_setopt_array($ch, array(
				CURLOPT_POST           => TRUE,
				CURLOPT_RETURNTRANSFER => TRUE,
				CURLOPT_SSL_VERIFYPEER => FALSE,
				CURLOPT_USERPWD        => $auth_string,
				CURLOPT_HTTPHEADER     => array('Content-type: application/x-www-form-urlencoded'),
				CURLOPT_POSTFIELDS => $data
		));
		$response = curl_exec($ch);
		curl_close($ch);
		// Check for errors
		if ($response === FALSE) {
			die(curl_error($ch));
		}
		// Decode the response
		$responseData = json_decode($response, TRUE);
		$access_token = $responseData["access_token"];
		return $access_token;
	}
}

//Return poster or video still 640 360
if (!function_exists('wb_get_video_still_url')) {
	function wb_get_video_still_url( $video_id ){
		global $bc_account_id;
		
		$access_token = wb_get_auth_token_version_4();
		
		$url = "https://cms.api.brightcove.com/v1/accounts/$bc_account_id/videos/$video_id/images";
		$header = array();
		$access = 'Authorization: Bearer ' . $access_token;
		$header[] = 'Content-Type: application/json';
		$header[] = $access;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response= curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		if ( $status != "404" ){
			$response_array = json_decode($response);
			$wb_https_image_link = "";
			
			foreach ( $response_array->poster->sources as $wb_current_image_source ){
				$wb_find_word = "https";
				$wb_pos = substr($wb_current_image_source->src, 0, 5);
				if ( $wb_pos == "https" ){
					$wb_https_image_link = $wb_current_image_source->src;
				}
				
			}
			
			if ( $wb_https_image_link != "" ){
				return $wb_https_image_link;
			}else{
				return $response_array->poster->src;
			}
			
		}else{
			return "";
		}
	}
}

if (!function_exists('wb_get_video_thumbnail_url')) {
	function wb_get_video_thumbnail_url( $video_id ){
		global $bc_account_id;
		
		$access_token = wb_get_auth_token_version_4();
		
		$url = "https://cms.api.brightcove.com/v1/accounts/$bc_account_id/videos/$video_id/images";
		$header = array();
		$access = 'Authorization: Bearer ' . $access_token;
		$header[] = 'Content-Type: application/json';
		$header[] = $access;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response= curl_exec($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		if ( $status != "404" ){
			$response_array = json_decode($response);
			$wb_https_image_link = "";
			
			foreach ( $response_array->thumbnail->sources as $wb_current_image_source ){
				$wb_find_word = "https";
				$wb_pos = substr($wb_current_image_source->src, 0, 5);
				if ( $wb_pos == "https" ){
					$wb_https_image_link = $wb_current_image_source->src;
				}
				
			}
			
			if ( $wb_https_image_link != "" ){
				return $wb_https_image_link;
			}else{
				return $response_array->thumbnail->src;
			}
		}else{
			return "";
		}
	}
}

if (!function_exists('wb_get_video_duration_seconds')) {
	function wb_get_video_duration_seconds( $video_id ){
		global $bc_account_id;
		
		$access_token = wb_get_auth_token_version_4();
		
		$url = "https://cms.api.brightcove.com/v1/accounts/". $bc_account_id ."/videos/".$video_id;
		$header = array();
		$access = 'Authorization: Bearer ' . $access_token;
		$header[] = 'Content-Type: application/json';
		$header[] = $access;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response= curl_exec($ch);
		$curl_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		$wb_status = json_decode($response);
		if( $curl_status == "404"){
			return "0";
		} else {
			$wb_status_value = $wb_status->duration;
			if( $wb_status_value ){
				return  floor($wb_status_value/ 1000);
			} else {
				return "0";
			}
		}
	}
}

if (!function_exists('wb_get_video_duration_milliseconds')) {
	function wb_get_video_duration_milliseconds( $video_id ){
		global $bc_account_id;
		
		$access_token = wb_get_auth_token_version_4();
		
		$url = "https://cms.api.brightcove.com/v1/accounts/". $bc_account_id ."/videos/".$video_id;
		$header = array();
		$access = 'Authorization: Bearer ' . $access_token;
		$header[] = 'Content-Type: application/json';
		$header[] = $access;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response= curl_exec($ch);
		$curl_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		$wb_status = json_decode($response);
		if( $curl_status == "404"){
			return "0";
		} else {
			$wb_status_value = $wb_status->duration;
			if( $wb_status_value ){
				return  $wb_status_value;
			} else {
				return "0";
			}
		}
	}
}


//returns highest size video source in array (mp4, size)

if (!function_exists('wb_get_video_source_mp4')) {
	function wb_get_video_source_mp4( $video_id ){
		global $bc_account_id;
		
		$access_token = wb_get_auth_token_version_4();
		
		$url = "https://cms.api.brightcove.com/v1/accounts/$bc_account_id/videos/$video_id/sources";
		$header = array();
		$access = 'Authorization: Bearer ' . $access_token;
		$header[] = 'Content-Type: application/json';
		$header[] = $access;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		$curl_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		if( $curl_status == "404"){
			return "";
		}else{
			$wb_response_array = json_decode($response);
			$wb_video_size = 0;
			$wb_video_src = "";
			
			foreach ( $wb_response_array as $current_video_source ){
				if ( $current_video_source->src !="" && $current_video_source->size > $wb_video_size && strrpos( $current_video_source->src,'https' ) !== false ){
					$wb_video_src = $current_video_source->src;
					$wb_video_size = $current_video_source->size;
				}
			}
			
			$response_array = array(
					'mp4'=>$wb_video_src,
					'size'=>$wb_video_size
			);
			
			return $response_array;
		}
	}
}

if (!function_exists('wb_get_video_sources_mp4')) {
	function wb_get_video_sources_mp4( $video_id ){
		global $bc_account_id;
		
		$access_token = wb_get_auth_token_version_4();
		
		$url = "https://cms.api.brightcove.com/v1/accounts/$bc_account_id/videos/$video_id/sources";
		$header = array();
		$access = 'Authorization: Bearer ' . $access_token;
		$header[] = 'Content-Type: application/json';
		$header[] = $access;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		$curl_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		if( $curl_status == "404"){
			return "";
		}else{
			return $response;
		}
	}
}


if (!function_exists('wb_get_video_cuepoints')) {
	function wb_get_video_cuepoints( $video_id ){
		GLOBAL $bc_account_id;
		
		$access_token = wb_get_auth_token_version_4();
		
		$url = "https://cms.api.brightcove.com/v1/accounts/$bc_account_id/videos/$video_id";
		$header = array();
		$access = 'Authorization: Bearer ' . $access_token;
		$header[] = 'Content-Type: application/json';
		$header[] = $access;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response= curl_exec($ch);
		curl_close($ch);
		
		$wb_return_cuepoint = json_decode($response);
		$wb_return_cuepoint_arr = $wb_return_cuepoint->cue_points;
		
		return json_decode($wb_return_cuepoint_arr);
	}
}

if (!function_exists('wb_remove_url_arguments')) {
	function wb_remove_url_arguments($url, $which_argument=false){
		return preg_replace( '/'. ($which_argument ? '(\&|)'.$which_argument.'(\=(.*?)((?=&(?!amp\;))|$)|(.*?)\b)' : '(\?.*)').'/i' , '', $url);
	}
}
/*
 * This are updates for Brightcove API
 *
 */

/* Disable Wordpress REST API */
add_filter( 'rest_authentication_errors', 'wp_snippet_disable_rest_api' );
function wp_snippet_disable_rest_api( $access ) {
	return new WP_Error( 'rest_disabled', __('The WordPress REST API has been disabled.'), array( 'status' => rest_authorization_required_code()));
}

if ( ! function_exists( 'wb_invalidate_cloudfront_file' ) ) {
	function wb_invalidate_cloudfront_file( $wb_cloudfront_file ) {
		return;
		global $wb_ent_options;
		$access_key 	= $wb_ent_options['amazons3info']['accesskey']; //AWS_ACCESS_KEY
		$secret_key 	= $wb_ent_options['amazons3info']['secretkey']; //AWS_SECRET_KEY
		$distribution 	= $wb_ent_options['cloudfrontinfo']['distributionid']; //DISTRIBUTION_ID;
		$epoch 			= date('U');
                                        if ( $distribution != '' ){
		//$wb_invalidate_file = '/videos/images/stills/1401_still.jpg'; //This is the file wifth path to be removed
		
		$xml = <<<EOD
					<InvalidationBatch>
						<Path>$wb_cloudfront_file</Path>
    					<CallerReference>{$distribution}{$epoch}</CallerReference>
					</InvalidationBatch>
EOD;
		
		$len = strlen($xml);
		$date = gmdate('D, d M Y G:i:s T');
		$sig = base64_encode(
				hash_hmac('sha1', $date, $secret_key, true)
				);
		$msg = "POST /2010-11-01/distribution/{$distribution}/invalidation HTTP/1.0\r\n";
		$msg .= "Host: cloudfront.amazonaws.com\r\n";
		$msg .= "Date: {$date}\r\n";
		$msg .= "Content-Type: text/xml; charset=UTF-8\r\n";
		$msg .= "Authorization: AWS {$access_key}:{$sig}\r\n";
		$msg .= "Content-Length: {$len}\r\n\r\n";
		$msg .= $xml;
		$fp = fsockopen('ssl://cloudfront.amazonaws.com', 443,
				$errno, $errstr, 30
				);
		if (!$fp) {
			die("Connection failed: {$errno} {$errstr}\n");
		}
		fwrite($fp, $msg);
		$resp = '';
		while(! feof($fp)) {
			$resp .= fgets($fp, 1024);
		}
		fclose($fp);
		return $resp;
	}
        }
}

//Updates 2020-08
function hide_update_notice_to_all_but_admin_users()
{
	if (!current_user_can('update_core')) {
		remove_action( 'admin_notices', 'update_nag', 3 );
	}
}
add_action( 'admin_head', 'hide_update_notice_to_all_but_admin_users', 1 );

add_action('admin_head', 'wb_my_custom_css');
function wb_my_custom_css() {
	echo '<style>
		    div#wp-content-editor-tools {
			   background-color: #FFFFFF;
		    }
           /*
			#bulk-action-selector-top .hide-if-no-js,
		    #bulk-action-selector-bottom .hide-if-no-js,
			.gadwp div#post-body #postbox-container-1{
			   display: none;
		    }
           */
			td.undeletable,th#undeletable{
				text-align: center;
			}
		  </style>';
}

/* Undeletable categories*/
add_action( 'delete_term_taxonomy', 'wpse_70758_del_tax', 10, 1 );
add_action( 'edit_term_taxonomies', 'wpse_70758_del_child_tax', 10, 1 );
add_filter( 'manage_edit-category_columns', 'wpse_70758_cat_edit_columns' );
add_filter( 'manage_category_custom_column', 'wpse_70758_cat_custom_columns', 10, 3 );


global $undeletable, $wb_how_to_videos_slug;
$undeletable = [];

$wb_slug = get_category( $wb_ent_options['videocats']['members'] ); //members only category
if( !$wb_slug->slug == "" )
	array_push($undeletable, $wb_slug->slug);
	
$wb_slug = get_category( $wb_ent_options['videocats']['webcast'] );
if( !$wb_slug->slug == "" )
	array_push($undeletable, $wb_slug->slug);
		
$wb_slug = get_category( $wb_ent_options['videocats']['library'] );
if( !$wb_slug->slug == "" )
	array_push($undeletable, $wb_slug->slug);
			
$wb_slug = get_category( $wb_ent_options['videocats']['unlisted'] );
if( !$wb_slug->slug == "" )
	array_push($undeletable, $wb_slug->slug);
				
$wb_slug = get_category( $wb_ent_options['videocats']['feature'] );
if( !$wb_slug->slug == "" )
	array_push($undeletable, $wb_slug->slug);
					
$wb_slug = get_category( $wb_ent_options['videocats']['playlist'] );
if( !$wb_slug->slug == "" )
	array_push($undeletable, $wb_slug->slug);
						
$wb_slug = get_category( $wb_ent_options['videocats']['private'] );
if( !$wb_slug->slug == "" )
	array_push($undeletable, $wb_slug->slug);
							
$wb_slug = get_category( $wb_ent_options['videocats']['howto'] );//How to
if( !$wb_slug->slug == "" )
	array_push($undeletable, $wb_slug->slug);

$wb_slug = get_category( $wb_ent_options['videocats']['cayl'] );
if( !$wb_slug->slug == "" )
	array_push($undeletable, $wb_slug->slug);
							
$wb_slug = get_category( $wb_ent_options['videocats']['podcast'] );
if( !$wb_slug->slug == "" )
	array_push($undeletable, $wb_slug->slug);
								
function wpse_70758_del_tax( $tt_id ){
	global $undeletable;
	$term = get_term_by( 'id', $tt_id, 'category' );
	if( in_array( $term->slug, $undeletable ) )
		wp_die( 'cant delete' );
}
								
function wpse_70758_del_child_tax( $arr_ids ){
	global $undeletable;
	foreach( $arr_ids as $id ){
		$term   = get_term_by( 'id', $id, 'category' );
		$parent = get_term_by( 'id', $term->parent, 'category' );
		if( in_array( $parent->slug, $undeletable ) )
			wp_die( 'cant delete' );
	}
}
								
function wpse_70758_cat_edit_columns( $columns ){
	//$columns['tt_id'] = 'ID';
	$columns['undeletable'] = 'Undeletable';
	return $columns;
}
								
function wpse_70758_cat_custom_columns( $value, $name, $tt_id ){
	if( 'tt_id' == $name )
		echo $tt_id;
									
	global $undeletable;
	$term = get_term_by( 'id', $tt_id, 'category' );
	if( 'undeletable' == $name && in_array( $term->slug, $undeletable ) )
		echo '<span style="color:#f00;font-size:5em;line-height:.5em;text-align:center;">&#149;</span>';
}
/* Undeletable categories*/

/* disable pagination for specific category */
$wb_slug = get_category( $wb_ent_options['videocats']['howto'] );//How to
if( !$wb_slug->slug == "" ){
	$wb_how_to_videos_slug = $wb_slug->slug;
}

add_action( 'pre_get_posts', 'wb_disable_pagination' );
function wb_disable_pagination( $query )
{
	global $wb_how_to_videos_slug;
	if ( $wb_how_to_videos_slug != "" ){
		if ( is_category( $wb_how_to_videos_slug ) )
			$query->set( 'posts_per_page', '-1' );
	}
}
/* disable pagination for specific category */

/* Custom admin menu entry */
add_action( 'admin_menu' , 'wb_custom_admin_menu' );
function wb_custom_admin_menu() {
	global $menu, $wb_ent_options;
	if ( $wb_ent_options['videocats']['howto'] != "" ){
		$wb_how_to_videos_category_link = get_category_link( $wb_ent_options['videocats']['howto'] );
		$menu[9999] = array( __('<div id="wb-external-link-to-how-to-videos">How-To Videos</div>'), 'edit_private_posts', $wb_how_to_videos_category_link, '', 'open-if-no-js menu-top', '', 'dashicons-editor-help' );
	}
}
/* Custom admin menu entry */
/* Custom admin menu entry pointing outside */
add_action( 'admin_footer', 'make_external_link_blank' );
function make_external_link_blank(){
	?>
    <script type="text/javascript">
    	jQuery(document).ready(function($) {
    		$('#wb-external-link-to-how-to-videos').parent().parent().attr('target','_blank');
   		});
    </script>
<?php
}
/* Custom admin menu entry pointing outside */

// Function to change email address
function wpb_sender_email( $original_email_address ) {
	global $wb_ent_options;
	if ( $wb_ent_options['channelemail'] != "" ){
		return $wb_ent_options['channelemail'];
	}else{
		return 'Webmaster@workerbee.tv';
	}
}

// Function to change sender name
function wpb_sender_name( $original_email_from ) {
	global $wb_ent_options;
	if ( $wb_ent_options['channelname'] != "" ){
		return $wb_ent_options['channelname'] . " Webmaster";
	}else{
		return 'Webmaster';
	}
}

// Hooking up our functions to WordPress filters
add_filter( 'wp_mail_from', 'wpb_sender_email' );
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );

function wb_category_carousel_category_field( $term ){
	$term_id = $term->term_id;
	$term_meta = get_term_meta( $term_id,"wb_carousel_image", true );
	?>
    <tr class="form-field">
        <th scope="row">
            <label for="term_meta[carousel_image]"><?php echo _e('Carousel Image URL') ?></label>
            <td>
            	<input type="url" name="term_meta[carousel_image]" id="term_meta[carousel_image]" value="<?php echo $term_meta; ?>">
            </td>
        </th>
    </tr>
<?php
} 

add_action( 'category_edit_form_fields', 'wb_category_carousel_category_field' ); 

function wb_save_tax_meta( $term_id ){
	if ( isset( $_POST['term_meta'] ) ) {
		$term_meta = array();
		$term_meta['carousel_image'] = isset ( $_POST['term_meta']['carousel_image'] ) ? sanitize_text_field( $_POST['term_meta']['carousel_image'] ) : '';
		update_term_meta($term_id, "wb_carousel_image", sanitize_text_field($_POST['term_meta']['carousel_image']));
	}
} // save_tax_meta
add_action( 'edited_category', 'wb_save_tax_meta', 10, 2 ); 

//Add wb_audio type to search list
if (!function_exists('add_my_post_types_to_query')) {
	function add_my_post_types_to_query( $query ) {
		if ( !is_admin() && $query->is_main_query() )
			$query->set( 'post_type', array( 'post', 'wb_audio', 'page' ) );
			return $query;
	}
}
add_action( 'pre_get_posts', 'add_my_post_types_to_query' );

/*
function do_favicon() {
	global $wb_ent_options;
	do_action( 'do_faviconico' );
	wp_redirect( get_site_icon_url( 32, admin_url( $wb_ent_options['customstyle']['favicon']) ) );
	exit;
}
*/
?>