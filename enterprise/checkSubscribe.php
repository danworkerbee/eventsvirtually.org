<?php
/**
 * filename: checkSubscribe.php
 * description: This will validate the entered values from user and send it to mailchimp
 * author: Lourice Capili
 * date created: 2015-06-17
 * 
 */
error_reporting(0);
$prewd = getcwd();
chdir(realpath(dirname(__FILE__)));

if (!defined(DB_NAME) && !defined(SHORTINIT)) {
    define('SHORTINIT', true);
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php' );
}
global $wb_ent_options;
$wb_ent_options = wb_get_option('wb_ent_options');
$api_key = $wb_ent_options['mailchimpapi']['apikey'];
$list_id = $wb_ent_options['mailchimpapi']['listid'];

function cleanHTMLVariable($var){
	return strip_tags(trim($var));
}
function wb_get_option($optionName = '') {
    if (trim($optionName) != '') {
        if (function_exists(get_option)) {
            return get_option($optionName);
        } else {
            try {
                $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

                $getWpOptions = $dbh->prepare("
                    SELECT option_value 
                    FROM `wp_options` 
                    WHERE option_name = ?           
                 ");

                $getWpOptions->bindParam(1, $optionName);

                $getWpOptions->execute();
                $getWpOptionsNumrows = $getWpOptions->rowCount();
                $getWpOptionsResult = $getWpOptions->fetch();

                echo '$getWpOptionsResult is ' . print_r($getWpOptionsResult, true);

                $getWpOptions = null;
                $dbh = null;
            } catch (PDOException $e) {
                echo "Error!: Could not connect to DB";
            }
        }
    }
}
require('Mailchimp.php');
//$Mailchimp = new Mailchimp( $api_key );
//$Mailchimp_Lists = new Mailchimp_Lists( $Mailchimp );
    $membership = cleanHTMLVariable($_POST['membership']);
    $industrySector = cleanHTMLVariable($_POST['optionsRadios']);
    $industrySectorOther = "";
    if($industrySector == "Other"){
    $industrySectorOther = cleanHTMLVariable($_POST['other_ra_text']);
    }
    $interestsArr = $_POST['interestGroup'];
    $interestsOther = cleanHTMLVariable($_POST['other_ch_text']);
    $jobTitle = cleanHTMLVariable($_POST['jobtitle']);    
    /*
    print_r($interestsArr);
    echo $interestsOther;
    echo "Print values <br />";
    echo "email address : <br />";
    echo "membership: $membership <br />";
    echo "Inudstry Sector: $industrySector <br />";
    echo "Interests: $interests <br />";
    echo "Job title: $jobTitle <br />";
 */
$email =  array( 'email' => htmlentities($_POST['email']) );
$email_type = 'html';
$double_optin = TRUE;       
$merge_vars = array(
                    'MEMBERSHIP' => $membership,
                    'INDSECTOR' => $industrySector,
                    'OINDSECTOR' => $industrySectorOther,
                    'OINTEREST' => $interestsOther,
                    'JOBTITLE' => $jobTitle,
                    'groupings'=>array(
                              array('id'=>53, 'groups'=> $interestsArr)
                             )
                    
                     );


$api = new Mailchimp($api_key);
$response = array();
try{
$api->lists->subscribe($list_id, array( 'email' => htmlentities($_POST['email']) ), $merge_vars, $email_type, $double_optin);
if(isset($_COOKIE['wb_sub_email'])) {
    unset($_COOKIE['wb_sub_email']); 
    setcookie('wb_sub_email', '', time() - 3600, '/');
}
$response['success'] = true;
}catch(Mailchimp_Error $e){
//echo $e->getMessage();
$response['success'] = false;
$response['message'] = $e->getMessage();
}

echo json_encode($response);
    unset($response);
    exit;
?>