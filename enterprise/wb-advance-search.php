<script>
    jQuery(function($wb){
        $wb("#wb-advance-text-search").keyup(function(){
            var wb_post_ctr = 0;
            $wb('#wb-advance-search-novideo').css('display', 'none');
            if( $wb("#wb-advance-text-search").val().length > 0 ){
                var wb_search_string = $wb("#wb-advance-text-search").val();
                $wb("#wb-channel-content > li").each(function(){
                    var wb_current_post = $wb(this);
                    var wb_current_title = wb_current_post.contents().find( ".search_title_unlock" ).text().toLowerCase();
                    var wb_compare = wb_current_title.search( wb_search_string.toLowerCase() );
                    if( wb_compare != -1 ){
                        $wb(this).css('display', 'inline-block');
                        wb_post_ctr++;
                    }else{
                        $wb(this).css('display', 'none');
                    }
                });
                if(	wb_post_ctr == 0 ){
                    $wb('#wb-advance-search-novideo').css('display', 'block');
                }else{
                    $wb('#wb-advance-search-novideo').css('display', 'none');
                }
            }else{
                $wb(".wb-subpage-block-wrapper").css('display', 'inline-block');
            }
        });
    });
</script>
<link rel="stylesheet" href="/css/icomoon.css">
<style>
    #wb-advance-text-search::-webkit-input-placeholder{color:#44779F!important}#wb-advance-text-search::-moz-placeholder{color:#44779F!important}#wb-advance-text-search:-ms-input-placeholder{color:#44779F!important}#wb-advance-text-search:-moz-placeholder{color:#44779F!important}#wb-advance-text-search::placeholder{color:#44779F!important}#wb-advance-text-search{width:80%;border-radius:15px;height:25px;margin-left:3%;padding-left:12px;font-size:17px}#wb-advance-search-icon{right:34px;position:relative;font-size:16px;top:-2px}#wb-advance-search-box{top:50%;left:0;z-index:2000;text-align:center;width:100%;margin-bottom: 30px;}@media screen and (max-width:600px){i#wb-advance-search-icon{display:inline-block!important}#wb-advance-text-search{margin-left:5%;font-size:12px}}@media screen and (max-width:481px){#wb-advance-search-box{top:60%}}@media screen and (max-width:400px){i#wb-advance-search-icon{display:none!important}}
</style>
<div id="wb-advance-search-box">
    <input type="text"  id="wb-advance-text-search" autocomplete="off" placeholder="Search a help topic or browse the videos below" />
    <i class="wbicon-search2" id="wb-advance-search-icon"></i>
</div>
