<?php
/**
 * filename: videopage.php
 * description: this will be the template used when there is a video
 * author: Jullie Quijano
 * date created: 2014-03-27
*/

global $postId;
$wb_post_type = get_post_type($postId);
get_header();
if ($wb_ent_options['sharetype'] == 'floating' && $wb_ent_options['hasshare'] && function_exists(sharebar)) {
    sharebar();
}
?>
<div id="wb_ent_content" class="clearfix row-fluid">

    <div id="wb_ent_main" class="span8 clearfix" role="main">

        <div id="video-post-page">
            <?php
            if (have_posts()) : while (have_posts()) : the_post();
            if(post_password_required( $postId ) && !is_user_logged_in() && !current_user_can('upload_files')){  ?>
              <h2><?php echo $video['title']; ?><?php edit_post_link('[Edit Video]', '', '', $video['postId']); ?></h2>
              <?php the_content(); 
            }else { 
                  $shortcodeCount = 0;
                  $videoDesc = get_the_content();
                  if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)\/\]/', $videoDesc, $match) >= 1) {
                     $shortcodeCount = count($match[0]);
                  }              
                    ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                        <header>

                            <?php //the_post_thumbnail('wpbs-featured'); ?>

                            <div class="">	
                                <h1 class="visible-desktop">
                                    <?php
                                    if ( in_category($wb_ent_options['cvideoinfo']['cat']) && (trim($wb_ent_options['cvideoinfo']['vidlabeltext']) != '')) {
                                        ?>
                                        <span class="cVidPrefixMain" style=""><?php echo $wb_ent_options['cvideoinfo']['vidlabeltext']; ?></span>
                                        <?php
                                    }
                                    ?>
                                    <?php //echo $video['title']; ?> <?php edit_post_link('[Edit Video]', '', '', $video['postId']); ?>
                                </h1>
                            </div>



                        </header> <!-- end article header -->

                        <section class="post_content clearfix" itemprop="articleBody">

                            <!-- Video Post -->

                            <div class="row-fluid" id="vid-post">
                                <ul class="">
                                    <?php                                    
                                    if( isset($wb_ent_options['vidshortcode']['enabled']) && ($wb_ent_options['vidshortcode']['enabled']) ){
                                    ?>
                                    <?php if ($wb_ent_options['haspostlogo']) { ?>
                                        <li class="span2 desc-position">
                                           <?php include ( get_template_directory() .'/includes/widgets/postLogo.php');?>
                                        </li>
                                    <?php } ?>                                         
                                    <li class="<?php
                                    if ($wb_ent_options['haspostlogo']) {
                                        echo 'span10';
                                    } else {
                                        echo 'span12';
                                    }
                                    ?> desc-position">
                                        <h2><?php echo $video['title']; ?></h2>
                                        <?php the_content(); ?>
                                    </li>
                                    <?php
                                    }
                                    else{
                                     ?>
																		 <?php //moving this\\ 
																		 /*
                                    <li class="span12">
                                        <div class="">
                                            <?php
                                            // PLAYER
                                              include ( get_template_directory() .'/includes/widgets/player.php');
                                            ?>
                                        </div>
                                    </li> */ ?>
                                    <?php if ($wb_ent_options['haspostlogo']) { ?>
                                        <li class="span2 desc-position">
                                           <?php include ( get_template_directory() .'/includes/widgets/postLogo.php');?>
                                        </li>
                                    <?php } ?>                   

                                    <li class="<?php
                                    if ($wb_ent_options['haspostlogo']) {
                                        echo 'span10';
                                    } else {
                                        echo 'span12';
                                    }
                                    ?> desc-position">
                                        <h2><?php echo $video['title']; ?></h2>
                                        <p id="video-desc-inner"><?php the_content(); ?></p>                    

                                        <?php
										/*
                                        if(strlen($video['desc']) > $wb_ent_options['videolistinfo']['desclimit']){ 
                                        //more_less('video-desc-inner', '', '1', '1');
                                        }
										*/
                                        ?>

                                    </li>
																		<li class="span12">
                                        <div class="">
                                            <?php
                                              // PLAYER
                                            if ( $wb_post_type == 'wb_audio' ){
                                            	$wb_audio_file = urldecode(get_post_meta($postId,'wb_podcast_file_url',true));
                                            	include ( get_template_directory() .'/includes/widgets/wb-new-podcast-player.php');
                                            	/*
												<audio controls style="width: 100%;">
													<source src="<?php echo $wb_audio_file; ?>" type="audio/mpeg">
												</audio>
												*/
											}else{
												include ( get_template_directory() .'/includes/widgets/player.php');
											}
                                            ?>
                                        </div>
                                    </li>
                                    <?php  
                                    }
                                    ?>
                                </ul>
                            </div>
                            <!-- End of Video Post -->
                        </section> <!-- end article section -->
                        <footer>

                            <?php 
                            $getWide = get_post_meta($postId, 'wb_column_ad_enable_wide', true );
                            $siteWide = $wb_ent_options['banners']['wide'];
                            if($getWide || $siteWide[status] == 'active'){
                                    include ( get_template_directory() . '/includes/widgets/adsWideCustom.php');
                            }
                            if ( is_array($video['tags']) && count($video['tags']) > 0) {
                                ?>
                                <p class="tags">
                                    <span class="tags-title"><?= _e('Tags:', 'enterprise') ?></span>                    
                                    <?php
                                    foreach ($video['tags'] as $currentTag) {
                                        ?>
                                        <a rel="tag" href="/keyword/<?php echo $currentTag->slug; ?>" class="label"><?php printf(__('%s', 'enterprise'), $currentTag->name); ?></a>
                                        <?php
                                    }
                                    ?>
                                </p> 
                                <?php
                            }
                            ?> 

                        </footer> <!-- end article footer -->

                    </article> <!-- end article -->
                    <?php
                    //count video views
                    $permalink = $video['postLink'];
                    wb_count_page_views($permalink, $postId);                   
                                                           
                    /*moving this before tags 7-08
										if($getWide || $siteWide[status] == 'active'){
											include ( get_template_directory() . '/includes/widgets/adsWideCustom.php');
                    } */
            }//end of else if password protected
                    ?>

                <?php endwhile; ?>			

            <?php else : ?>

                <article id="post-not-found">
                    <header>
                        <h1><?php _e("Not Found", "enterprise"); ?></h1>
                    </header>
                    <section class="post_content">
                        <p><?php _e("Sorry, but the requested resource was not found on this site.", "enterprise"); ?></p>
                    </section>
                    <footer>
                    </footer>
                </article>

<?php endif; ?>
                     <?php
                    /*
                    if (HAS_ATTACHMENTS) {
                        include ( get_template_directory() .'/includes/widgets/attachments.php');
                    }
                     
                     
                    // Advertisements
                    include ( get_template_directory(__FILE__) .'/library/includes/ads/ads_wide.php');


                    if (CENTER_BANNERS > 0) {
                        include ( get_template_directory(__FILE__) .'/library/includes/ads/ads_center_minor.php');
                    }
                     * 
                     */
                  if( !post_password_required( $postId ) || (is_user_logged_in() && current_user_can('upload_files')) ){
                    if ( $wb_ent_options['hasshare'] ) {                        
                        if( $wb_ent_options['sharetype'] == 'floating' && function_exists(sharebar_horizontal)){
                            sharebar_horizontal();
                            echo '<br clear="all" />';
                            
                        }                        
                        if( (is_user_logged_in() && current_user_can('upload_files')) ){
                        	include ( get_template_directory() .'/includes/widgets/wb-new-share.php');
                        }     
                        
                    }
                    if ($wb_ent_options['hasdownload'] && is_user_logged_in() && $shortcodeCount <= 1 ) {
                        //include ( get_template_directory() .'/includes/widgets/download.php');
                    }
                    ?>
                    <?php
                    // Components
                    if ($wb_ent_options['hascomment']) {
                        include ( get_template_directory() .'/includes/widgets/comments.php');
                    }
                    
                  }
                    ?> 

        </div> <!-- end #video-post-page -->

        <?php 
        if( $shortcodeCount <= 1 && ( !post_password_required( $postId ) || (is_user_logged_in() && current_user_can('upload_files')) ) ){
            include ( get_template_directory() .'/includes/widgets/videolistwidget.php');
        } 
        ?>
    </div> <!-- end #main -->


<?php get_sidebar(); // sidebar 1  ?>

</div> <!-- end content -->

<?php
get_footer();
?>
