	$(function() {
    	$('.embed-box').click(function() {
        	this.select();
        });
    });

	$( "#wb-select-embed-download" ).change(function() {
		var wb_download = $(this).val();
		$('.wb-download-div').css('display','none');
		switch (wb_download) { 
			case 'wb-permalink': 
				$('#wb-post-permalink').css('display','block');
				break;
			case 'wb-large-embed': 
				$('#wb-post-large-embed-code').css('display','block');
				break;
			case 'wb-medium-embed': 
				$('#wb-post-medium-embed-code').css('display','block');
				break;		
			case 'wb-small-embed': 
				$('#wb-post-small-embed-code').css('display','block');
				break;
			case 'wb-iframe-embed': 
				$('#wb-post-iframe-embed-code').css('display','block');
				break;
			case 'wb-high-video': 
				$('#wb-post-high-download').css('display','block');
				break;
			case 'wb-medium-video': 
				$('#wb-post-medium-download').css('display','block');
				break;
			case 'wb-low-video': 
				$('#wb-post-low-download').css('display','block');
				break;
			default:
				$('.wb-download-div').css('display','none');
		}
	});

	$(function() {
    	$('.wb-copy-embed-button').click(function() {
        	var current_id = $(this).attr('id');
        	
        	switch (current_id) { 
				case 'wb-copy-permalink': 
					$('#wb-permalinkBox').select();
					document.execCommand("copy");
					var tooltip = document.getElementById("wb-permalink-tooltip");
					tooltip.innerHTML = "Permalink copied!";
					$('#wb-check-image-pop-permalink').css("display", "inline-block");
					$('#wb-check-image-pop-permalink').fadeOut( 1600 );
					break;
				case 'wb-copy-large-embed': 
					$('#wb-large-embed-code-box').select();
					document.execCommand("copy");
					var tooltip = document.getElementById("wb-large-embed-code-tooltip");
					tooltip.innerHTML = "Embed code copied!";
					$('#wb-check-image-pop-large-embed').css("display", "inline-block");
					$('#wb-check-image-pop-large-embed').fadeOut( 1600 );
					break;
				case 'wb-copy-medium-embed': 
					$('#wb-medium-embed-code-box').select();
					document.execCommand("copy");
					var tooltip = document.getElementById("wb-medium-embed-code-tooltip");
					tooltip.innerHTML = "Embed code copied!";
					$('#wb-check-image-pop-medium-embed').css("display", "inline-block");
					$('#wb-check-image-pop-medium-embed').fadeOut( 1600 );
					break;		
				case 'wb-copy-low-embed': 
					$('#wb-small-embed-code-box').select();
					document.execCommand("copy");
					var tooltip = document.getElementById("wb-small-embed-code-tooltip");
					tooltip.innerHTML = "Embed code copied!";
					$('#wb-check-image-pop-small-embed').css("display", "inline-block");
					$('#wb-check-image-pop-small-embed').fadeOut( 1600 );
					break;
				case 'wb-copy-iframe-embed': 
					$('#wb-iframe-embed-code-box').select();
					document.execCommand("copy");
					var tooltip = document.getElementById("wb-iframe-embed-code-tooltip");
					tooltip.innerHTML = "Embed code copied!";
					$('#wb-check-image-pop-iframe-embed').css("display", "inline-block");
					$('#wb-check-image-pop-iframe-embed').fadeOut( 1600 );
					break;
				default:
					console.log("wb default");
			}
			
        });
    });

	$( ".wb-copy-embed-button" ).mouseout(function() {
		$( ".tooltiptext" ).text( "Copy to clipboard" );
	});