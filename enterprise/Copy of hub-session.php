<?php
/**
 * filename: hub-splash.php
 * description: This will be the template to use for the AANA Hub Splash page
 * author: Jullie Quijano
 * date created: 2020-06-14
 *
 *
 * @package WordPress
 * @subpackage Enterprise
 
 * Template Name: Hub - Session Page
 */
define('DONOTCACHEPAGE', true);
global $postId, $wb_ent_options, $video, $wpdb, $current_user;
if ( $postId == '' )
	$postId = get_the_ID();
$wb_virtual_post 			= wb_virtual_post_get($postId);
$banner_image 				= get_the_post_thumbnail_url($postId, 'full');
$wb_hub_page_id 			= '';
$wb_vh_live_events_category	= get_option('wb_vh_live_events_category');
$wb_session_track			= array();
$wb_attachments 			= new Attachments( 'attachments', $postId);
$wb_download_link			= '';
$wb_lms_url 				= "https://".WB_LMS_API_DOMAIN."/wp-json/wb-lms-aana-api/v1/lms-login";
//date_default_timezone_set('America/Los_Angeles');
$wb_lms_access				= true;
// Get only the first attachement
if( $wb_attachments->exist() ) :
	while( $attachment = $wb_attachments->get() ) :
		$wb_download_link = $wb_attachments->url();
		break;
	endwhile;
endif;
// Get only the first attachement

get_header();
if ($wb_ent_options['sharetype'] == 'floating' && $wb_ent_options['hasshare'] && function_exists(sharebar)) {
	if ( !in_category( $wb_ent_options['videocats']['howto'] )){
		if (  !in_category($wb_ent_options['videocats']['members']) ){
			sharebar();
		}
	}
}

foreach ( $wb_virtual_post['post_category'] as $wb_current_cat ){
	$wb_cat_info = wb_get_track_info($wb_current_cat);
	$wb_vpage_args = array(
			'posts_per_page'   	=> -1,
			'post_type'        	=> 'page',
			'meta_query' => array(
					array(
							'key'     => 'wb_live_session_category',
							'value'   =>  $wb_cat_info['term_id'],
							
					),
					array(
							'key'     => 'wb_virtual_conference_page_type',
							'value'   =>  'hub',
							
					),
			),
	);
	$wb_vpage_query = new WP_Query($wb_vpage_args);
	if ( $wb_vpage_query->post ){
		$wb_hub_page_id = $wb_vpage_query->post->ID;
	}elseif ( $wb_cat_info['color'] != '' ){
		$wb_track_style .= "i.wb-ent-tracks-".$wb_cat_info['slug']."{color:".$wb_cat_info['color'].";}";
		$wb_session_track[] = array( 	'slug' => $wb_cat_info['slug'],
										'name' => 	$wb_cat_info['name']
									);
	}
	
}

if ( $wb_hub_page_id != '' ){
	$wb_all_meta_data 	= get_metadata("post", $wb_hub_page_id);
}

if ( !is_user_logged_in() && $wb_virtual_post['wb_virtual_conference_login_required'] === "yes" ){
	wp_redirect('/?p='.$wb_hub_page_id);
	exit();
}

echo "<style>".$wb_track_style."</style>";
if( $wb_attachments->exist() ) :
	while( $attachment = $wb_attachments->get() ) :
		$wb_download_link = $wb_attachments->url();
		break;
	endwhile;
endif;

$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$wb_now =  date("Y-m-d");
$calendar_date_from = date('F d, Y',strtotime($wb_virtual_post["wb_virtual_conference_date"])) . " " . date('H:i',strtotime($wb_virtual_post['wb_virtual_conference_from']));
$calendar_date_to = date('F d, Y',strtotime($wb_virtual_post["wb_virtual_conference_date"])) . " " . date('H:i',strtotime($wb_virtual_post['wb_virtual_conference_to']));

if ( $wb_virtual_post['wb_virtual_calender_description'] == "" ){
	$calender_content = str_replace('"','',$wb_virtual_post->post_content);
}else{
	$calender_content = $wb_virtual_post['wb_virtual_calender_description'];
}

$calender_content = str_replace('&','and',$calender_content);
$calender_content = strip_tags($calender_content);
$calender_content = preg_replace('~[\r\n]+~', '', $calender_content);

if ( $wb_virtual_post['wb_virtual_conference_time_zone'] == "PT" ){
	$wb_calendar_timezone = "America/Los_Angeles";
	$calendar_date_from = $calendar_date_from . " GMT-07:00";
	$calendar_date_to = $calendar_date_to . " GMT-07:00";
	$convert_time_zone = "ET";
	
	$day_from = date('H:i',strtotime($wb_virtual_post['wb_virtual_conference_from']));
	$convert_time_from = date("H:i", strtotime('+3 hours', strtotime($day_from)));
	$day_to = date('H:i',strtotime($wb_virtual_post['wb_virtual_conference_to']));
	$convert_time_to = date("H:i", strtotime('+3 hours', strtotime($day_to)));
	
	$wb_pt_time_from = date("H:i", strtotime('+0 hours', strtotime($day_from)));
	$wb_pt_time_to = date("H:i", strtotime('+0 hours', strtotime($day_to)));
	$wb_et_time_from = date("H:i", strtotime('+3 hours', strtotime($day_from)));
	$wb_et_time_to = date("H:i", strtotime('+3 hours', strtotime($day_to)));
	
	$wb_mt_time_from = date("H:i", strtotime('+1 hours', strtotime($day_from)));
	$wb_mt_time_to = date("H:i", strtotime('+1 hours', strtotime($day_to)));
	$wb_ct_time_from = date("H:i", strtotime('+2 hours', strtotime($day_from)));
	$wb_ct_time_to = date("H:i", strtotime('+2 hours', strtotime($day_to)));
	
}elseif ($wb_virtual_post['wb_virtual_conference_time_zone'] == "ET" ){
	$calendar_date_from = $calendar_date_from . " GMT-04:00";
	$calendar_date_to = $calendar_date_to . " GMT-04:00";
	$wb_calendar_timezone = "America/New_York";
	$convert_time_zone = "PT";
	$day_from = date('H:i',strtotime($wb_current_page_time));
	$convert_time_from = date("H:i", strtotime('-3 hours', strtotime($day_from)));
	$day_to = date('H:i',strtotime($wb_virtual_post['wb_virtual_conference_to']));
	$convert_time_to = date("H:i", strtotime('-3 hours', strtotime($day_to)));
	
	$wb_pt_time_from = date("H:i", strtotime('-3 hours', strtotime($day_from)));
	$wb_pt_time_to = date("H:i", strtotime('-3 hours', strtotime($day_to)));
	$wb_et_time_from = date("H:i", strtotime('+0 hours', strtotime($day_from)));
	$wb_et_time_to = date("H:i", strtotime('+0 hours', strtotime($day_to)));
	
	$wb_mt_time_from = date("H:i", strtotime('-2 hours', strtotime($day_from)));
	$wb_mt_time_to = date("H:i", strtotime('-2 hours', strtotime($day_to)));
	$wb_ct_time_from = date("H:i", strtotime('-1 hours', strtotime($day_from)));
	$wb_ct_time_to = date("H:i", strtotime('-1 hours', strtotime($day_to)));
}
elseif ($wb_virtual_post['wb_virtual_conference_time_zone'] == "CT" ){
	$calendar_date_from = $calendar_date_from . " GMT-05:00";
	$calendar_date_to = $calendar_date_to . " GMT-05:00";
	$wb_calendar_timezone = "America/Chicago";
	$convert_time_zone = "PT";
	$day_from = date('H:i',strtotime($wb_virtual_post['wb_virtual_conference_from']));
	$convert_time_from = date("H:i", strtotime('-3 hours', strtotime($day_from)));
	$day_to = date('H:i',strtotime($wb_virtual_post['wb_virtual_conference_to']));
	$convert_time_to = date("H:i", strtotime('-3 hours', strtotime($day_to)));
	
	$wb_pt_time_from = date("H:i", strtotime('-2 hours', strtotime($day_from)));
	$wb_pt_time_to = date("H:i", strtotime('-2 hours', strtotime($day_to)));
	$wb_et_time_from = date("H:i", strtotime('+1 hours', strtotime($day_from)));
	$wb_et_time_to = date("H:i", strtotime('+1 hours', strtotime($day_to)));
	
	$wb_mt_time_from = date("H:i", strtotime('-1 hours', strtotime($day_from)));
	$wb_mt_time_to = date("H:i", strtotime('-1 hours', strtotime($day_to)));
	$wb_ct_time_from = date("H:i", strtotime('+0 hours', strtotime($day_from)));
	$wb_ct_time_to = date("H:i", strtotime('+0 hours', strtotime($day_to)));
	
}elseif ($wb_virtual_post['wb_virtual_conference_time_zone'] == "MT" ){
	$calendar_date_from = $calendar_date_from . " GMT-06:00";
	$calendar_date_to = $calendar_date_to . " GMT-06:00";
	$wb_calendar_timezone = "America/Phoenix";
	$convert_time_zone = "PT";
	$day_from = date('H:i',strtotime($wb_virtual_post['wb_virtual_conference_from']));
	$convert_time_from = date("H:i", strtotime('-3 hours', strtotime($day_from)));
	$day_to = date('H:i',strtotime($wb_virtual_post['wb_virtual_conference_to']));
	$convert_time_to = date("H:i", strtotime('-3 hours', strtotime($day_to)));
	
	$wb_pt_time_from = date("H:i", strtotime('-1 hours', strtotime($day_from)));
	$wb_pt_time_to = date("H:i", strtotime('-1 hours', strtotime($day_to)));
	$wb_et_time_from = date("H:i", strtotime('+2 hours', strtotime($day_from)));
	$wb_et_time_to = date("H:i", strtotime('+2 hours', strtotime($day_to)));
	
	$wb_mt_time_from = date("H:i", strtotime('+0 hours', strtotime($day_from)));
	$wb_mt_time_to = date("H:i", strtotime('+0 hours', strtotime($day_to)));
	$wb_ct_time_from = date("H:i", strtotime('+1 hours', strtotime($day_from)));
	$wb_ct_time_to = date("H:i", strtotime('+1 hours', strtotime($day_to)));
}

$wb_new_calendar_date_to = date('m/d/Y',strtotime($wb_virtual_post["wb_virtual_conference_date"])) . date(" h:i A",strtotime($wb_virtual_post['wb_virtual_conference_to']));
$wb_new_calendar_date_from = date('m/d/Y',strtotime($wb_virtual_post["wb_virtual_conference_date"])) . date(" h:i A",strtotime($wb_virtual_post['wb_virtual_conference_from']));

if ( $wb_now != $wb_virtual_post["wb_virtual_conference_date"] ){
	echo '<meta http-equiv="refresh" content="600">';
}

$wb_zoom_display = "display: none;";

if ( $wb_now == $wb_virtual_post["wb_virtual_conference_date"] && $wb_virtual_post['wb_virtual_conference_default_media'] == "zoom" ){
	$wb_disabled_button = "";
	$wb_zoom_display = "";
}else{
	$wb_disabled_button = " isDisabled";
	//urldecode($wb_virtual_post['wb_virtual_conference_link']) = "#";
}

//echo "<pre>";
//print_r($wb_virtual_post);
//print_r($attachments);
//echo "</pre>";
//echo $wb_now . " - " . $wb_virtual_post['wb_virtual_conference_date'];
?>
<script type="text/javascript" src="<?php echo WB_VH_DIR_URL . 'assets/calendar/ouical.js'; ?>"></script>
<link type="text/css" rel="stylesheet" href="<?php echo WB_VH_DIR_URL . 'assets/css/aana-hub.css';?>" />
<link rel="stylesheet" href="<?php echo WB_VH_DIR_URL . 'assets/calendar/main.css';?>">
<div id="wb_ent_content" class="clearfix row-fluid">
    <div id="wb_ent_main" class="span8 clearfix" role="main">

        <div id="aana-hub">
        <!-- START HTML CODE HERE -->
            <div class="clearfix row-fluid">

				<div class="wb-ent-event-hub-banner">
					<a href="<?php echo urldecode($wb_all_meta_data["wb_live_hub_banner_link"][0]); ?>">
						<img src="<?php echo urldecode($wb_all_meta_data["wb_live_hub_banner_upload_url"][0]); ?>" alt="ANNA Splash Screen Hub" />
					</a>
				</div>
				<?php 
					//Custom login codes for AANA
					$wb_aana_member_id 		= get_user_meta( $current_user->ID, 'wb_ent_aana_person_id', true);
					$wb_aana_member_token 	= get_user_meta( $current_user->ID, 'wb_aana_user_token', true);
					
					if ( trim($wb_virtual_post['wb_virtual_conference_product_id']) != "" ){
						//check if product is available for current user
						$wb_product_available = wb_aana_user_bought_product($wb_aana_member_id, trim($wb_virtual_post['wb_virtual_conference_product_id']));
						if ( !$wb_product_available ){
							$wb_lms_access = false;
							//wp_redirect('/?p='.$wb_hub_page_id);
							//exit();
						}
					}
					
					if ( $wb_lms_access || current_user_can( 'edit_posts' ) ){
						//if logged-in and LMS
						if ( $wb_virtual_post['wb_virtual_conference_default_media'] === "lms" && $wb_virtual_post['wb_virtual_conference_quiz_link'] != "" ){
							if ( $current_user->data->user_email != "" && $wb_aana_member_id != "" && $wb_aana_member_token != "" && $wb_virtual_post['wb_virtual_conference_quiz_link'] != "" ){
								$wb_lms_url .= "?person_id=".$wb_aana_member_id."&email=".urlencode($current_user->data->user_email)."&token=".$wb_aana_member_token."&post_id=".$wb_virtual_post['wb_virtual_conference_quiz_link'];
								//wp_redirect( $wb_lms_url );
								//exit();					
							}
						}
						//Custom login codes
					?>
					<?php 
						if ( $wb_virtual_post['wb_virtual_conference_default_media'] == "pre-recorded" && $wb_now === $wb_virtual_post['wb_virtual_conference_date'] ){
							if ( $wb_virtual_post['wb_virtual_conference_live_status'] == "live" && $wb_virtual_post['wb_ppv_prod_video_id'] != ""){
							?>
		                    	<div class="alert alert-error" role="alert">
									 If you're having issues, please clear your browser's cache then refresh the page.  Still having issues? <a href="<?php 
												if(filter_var( urldecode($wb_all_meta_data["wb_live_hub_need_assistance_link"][0]), FILTER_VALIDATE_URL)){
													echo urldecode($wb_all_meta_data["wb_live_hub_need_assistance_link"][0]);
												}elseif(filter_var( urldecode($wb_all_meta_data["wb_live_hub_need_assistance_link"][0]), FILTER_VALIDATE_EMAIL)){
													echo "mailto:".urldecode($wb_all_meta_data["wb_live_hub_need_assistance_link"][0])."?subject=".$wb_virtual_post['post_title']." - HUB Session SUPPORT";
												}else{
													echo "#";
												}?>">Contact Us</a>
								</div>
		                    <?php 
		                	}elseif( $wb_virtual_post['wb_virtual_conference_live_status'] == "coming-soon" && $wb_now === $wb_virtual_post['wb_virtual_conference_date'] ){
								echo '<meta http-equiv="refresh" content="300">';
							?>
		                    	<div class="alert alert-warning" role="alert">
									 Thank you for joining us! The live stream will begin shortly.
								</div>
		                    <?php 
							}elseif( $wb_virtual_post['wb_virtual_conference_live_status'] == "ended" && $wb_now === $wb_virtual_post['wb_virtual_conference_date'] ){
							?>
								<div class="alert alert-info" role="alert">
	                        			The live stream has ended, thank you!
	                        	</div>
								<?php 
							}
						}
					?>
					<h1 class="wb-ent-aanaapf-header-title"><?php echo $wb_virtual_post['post_title'] ; ?></h1>
	
					<div class="wb-ent-aanaapf-header-date-tracks">
						<h2><?php
						$wb_session_track_name_display = date('l, F d, Y',strtotime($wb_virtual_post["wb_virtual_conference_date"])) . " | " . date('h:i:A',strtotime($wb_virtual_post['wb_virtual_conference_from']));//$wb_current_session->wb_virtual_conference_time_zone;
						if ( $wb_virtual_post['wb_virtual_conference_time_display'] != "from" ){
							$wb_session_track_name_display .= ' - ' .  date('h:i:A',strtotime($wb_virtual_post['wb_virtual_conference_to'])); // . " " . $wb_current_session->wb_virtual_conference_time_zone;
						}
						$wb_session_track_name_display .= " " . $wb_virtual_post['wb_virtual_conference_time_zone'];
						echo $wb_session_track_name_display;
						//echo date('l, F d – ',strtotime($wb_virtual_post["wb_virtual_conference_date"])) . date('H:i A',strtotime($wb_virtual_post['wb_virtual_conference_from'])) . " - " . date('H:i A',strtotime($wb_virtual_post['wb_virtual_conference_to'])) . " " . $wb_virtual_post['wb_virtual_conference_time_zone']; 
						?><!-- Saturday, August 15 – 7:15 AM - 8:15 AM --></h2>
						<div class="wb-ent-aanaapf-header-tracks">
							<ul>
							<?php 
							foreach ( $wb_session_track as $wb_current_cat ){
							?>
								<li><p><i class="fas fa-square wb-ent-tracks-<?php echo $wb_current_cat['slug']; ?>"></i> <span><?php echo $wb_current_cat['name']; ?></span></p></li>	
							<?php 
							}
							?>	
							<!-- 
								<li><p><i class="fas fa-square wb-ent-tracks-clinical"></i> <span>Clinical Practice</span></p></li>
								<li><p><i class="fas fa-square wb-ent-tracks-business"></i> <span>Business of Anesthesia</span></p></li>
								<li><p><i class="fas fa-square wb-ent-tracks-pediatrics"></i> <span>Pediatrics and Obstetrics</span></p></li>
								<li><p><i class="fas fa-square wb-ent-tracks-individual"></i> <span>Individual Registration</span></p></li>
								<li><p><i class="fas fa-square wb-ent-tracks-special"></i> <span>Special Interest</span></p></li>
							 -->
							</ul>
						</div>
					</div>
					
					<?php 
					if (  $wb_virtual_post['wb_virtual_conference_live_status'] != "live"  ){
					
					?>
					<div class="wb-ent-aanaapf-btn-group">
						<?php 
						if ( $wb_virtual_post['wb_virtual_conference_link'] != ""){ ?>
							<a href="<?php 
							if ( $wb_now === $wb_virtual_post['wb_virtual_conference_date'] ){
								echo urldecode($wb_virtual_post['wb_virtual_conference_link']);
							}else{
								echo "#";
							}?>" class="
							<?php 
							if ( $wb_now != $wb_virtual_post['wb_virtual_conference_date'] ){
								echo " isDisabled";
							}
							?>" target="_blank"><button type="button" class="btn btn-primary wb-ent-aanaapf-hub-session-btn">Launch Session</button></a>
						<?php 
						}
						?>
						
						
						<?php 
						if ( trim($wb_download_link) != "" ){
						?>
							<a href="<?php echo $wb_download_link; ?>" target="_blank" class="<?php if ( $wb_download_link == "" ) echo " isDisabled"; ?>"><button type="button" class="btn btn-primary wb-ent-aanaapf-hub-session-btn">Download Materials</button></a>
							<?php 
						}
						?>
						
						
						<?php 
						if ( $wb_virtual_post['wb_virtual_conference_add_to_calendar'] != "no" ){
	                            ?>
	                            	<!-- <div class="new-cal"></div> -->
	                            	<script type="text/javascript" src="https://addevent.com/libs/atc/1.6.1/atc.min.js" async defer></script>
									<div title="Add to Calendar" class="addeventatc">
									    Add to Calendar
									    <span class="start"><?php echo $wb_new_calendar_date_from;?></span>
									    <span class="end"><?php echo $wb_new_calendar_date_to;?></span>
									    <span class="timezone"><?php echo $wb_calendar_timezone; ?></span>
									    <span class="title"><?php echo $wb_virtual_post['post_title']; ?></span>
									    <span class="description"><?php echo strip_tags($wb_virtual_post['post_content']); ?></span>
									    <span class="location"><?php echo $actual_link; ?></span>
									</div>
	                            	
	                            	
	                            	
	                            <?php 
	                     	} 
	                    ?>
						<!--  <a href="#"><button type="button" class="btn btn-primary wb-ent-aanaapf-hub-session-btn">Add to Calendar</button></a>  -->
						<?php 
						if ( trim($wb_virtual_post['wb_virtual_conference_quiz_link']) != "" ){
							$wb_lms_url = "#";
							$wb_quiz_button = " isDisabled";
							if( $wb_virtual_post['wb_virtual_conference_quiz_link'] != "" ){
							//AANA link to quiz
								if ( $current_user->data->user_email != "" && $wb_aana_member_id != "" && $wb_aana_member_token != "" && trim($wb_virtual_post['wb_virtual_conference_quiz_link']) != "" && $wb_now >= $wb_virtual_post['wb_virtual_conference_date']){
								    $wb_lms_url = 'https://'.WB_LMS_API_DOMAIN."/home?person_id=".$wb_aana_member_id."&email=".urlencode($current_user->data->user_email)."&token=".$wb_aana_member_token."&post_id=".$wb_virtual_post['wb_virtual_conference_quiz_link'];
								    $wb_quiz_button = "";
								}
							}
						?>
							<a href="<?php echo $wb_lms_url; ?>" class="<?php echo $wb_quiz_button; ?>"><button type="button" class="btn btn-primary wb-ent-aanaapf-hub-session-btn">Earn CE</button></a>
						<?php 
						}
						?>
						
						<a href="<?php echo "/?p=".$wb_hub_page_id; ?>"><button type="button" class="btn btn-primary wb-ent-aanaapf-hub-session-btn">Return to Hub</button></a>
					</div>
					<?php 
					}
					?>
					
					
					<?php 
						$video['mediaId'] = $wb_virtual_post['wb_ppv_prod_video_id'];
						if ( $wb_virtual_post['wb_virtual_conference_default_media'] == "pre-recorded" && $wb_now === $wb_virtual_post['wb_virtual_conference_date'] ){
							$video['mediaId'] = $wb_virtual_post['wb_ppv_prod_video_id'];
							if ( $wb_virtual_post['wb_virtual_conference_live_status'] == "live" && $wb_virtual_post['wb_ppv_prod_video_id'] != ""){
							   	include ( get_template_directory() .'/includes/widgets/player.php');
							   	?>
							   	
							   	<?php 
							   	  	if ( $wb_all_meta_data['wb_live_hub_buttons_below_player'][0] == "yes" ){
							   	?>
							   	<div class="wb-ent-aanaapf-btn-group" id="wb-hub-player-below-buttons" >
									<a href="<?php echo urldecode($wb_all_meta_data['wb_live_hub_player_left_button_link'][0]); ?>" class="<?php echo urldecode($wb_all_meta_data['wb_live_hub_player_left_button_text'][0]); ?>"><button type="button" class="btn btn-primary wb-ent-aanaapf-hub-session-btn wb-hub-player-below-buttons"><i class="fa fa-arrow-left" aria-hidden="true"></i> Return to AANA 2020 Congress Events</button></a>
									<a href="<?php echo urldecode($wb_all_meta_data['wb_live_hub_player_right_button_link'][0]);?>" class="<?php echo urldecode($wb_all_meta_data['wb_live_hub_player_right_button_text'][0]);?>"><button type="button" class="btn btn-primary wb-ent-aanaapf-hub-session-btn wb-hub-player-below-buttons">Visit the Exhibitor Showcase</button></a>
								</div>
								
								<?php 
							   	}
								?>
								
								<?php 
							}elseif( $wb_virtual_post['wb_virtual_conference_live_status'] == "coming-soon" && $wb_now === $wb_virtual_post['wb_virtual_conference_date'] ){
								echo '<meta http-equiv="refresh" content="300">';
							?>
		                    	<img alt="" src="<?php echo $wb_virtual_post['wb_ppv_prod_video_still']; ?>" style="width: 100%; margin-bottom: 5px;">
								<p style="margin-top: 0px; text-align: center;"><strong>Sessions will be broadcast live to this page</strong></p>
		                    <?php 
							}elseif( $wb_virtual_post['wb_virtual_conference_live_status'] == "ended" && $wb_now === $wb_virtual_post['wb_virtual_conference_date'] ){
							?>
				            	<img alt="" src="<?php echo $wb_virtual_post['wb_ppv_prod_video_still']; ?>" style="width: 100%; margin-bottom: 5px;">
							<?php 
							}else{
								
							}
						}
						if( $wb_virtual_post['wb_virtual_conference_live_status'] == "on-demand" && $wb_virtual_post['wb_ppv_prod_video_id'] != "" ){
							include ( get_template_directory() .'/includes/widgets/player.php');
						}
						
						if ( $wb_virtual_post['wb_virtual_io_embed'] != "" && $wb_now == $wb_virtual_post["wb_virtual_conference_date"] && is_user_logged_in() && $wb_virtual_post['wb_virtual_conference_live_status'] == "live" ){
							?>
	                	<div id="wb-hub-poll-wrapper">
		               	<?php 
		                      echo stripslashes(urldecode($wb_virtual_post['wb_virtual_io_embed']));
		                ?>
		                	</div> 
		                <?php 
	                   }
					?>
					
					
					
					<?php 
					if ( $wb_virtual_post['wb_virtual_conference_speaker'] != "" ){
						$wb_speakers = json_decode( $wb_virtual_post['wb_virtual_conference_speaker'] , true);
						if ( $wb_speakers ){
					?>
					<h2 class="wb-ent-aanaapf-speaker-txt">Speaker(s)</h2>
					<?php 
							foreach ( $wb_speakers as $wb_current_speaker ){
					?>
					<div class="wb-ent-aanaapf-speaker-info-area">
						<img src="<?php echo urldecode($wb_current_speaker['image']); ?>" alt="Speaker Profile Picture" style="max-height: 126px;"/>
						<div class="wb-ent-aanaapf-speaker-info">
							<?php if ( $wb_current_speaker['speaker'] != '' ){ ?>
							<p class="wb-ent-aanaapf-speaker-name"><?php echo $wb_current_speaker['speaker']; ?></p>
							<?php } ?>
							<?php if ( $wb_current_speaker['position'] != '' ){ ?>
							<p class="wb-ent-aanaapf-speaker-data"><?php echo $wb_current_speaker['position']; ?></p>
							<?php } ?>
							<?php if ( $wb_current_speaker['work'] != '' ){ ?>
							<p class="wb-ent-aanaapf-speaker-data"><?php echo $wb_current_speaker['work']; ?></p>
							<?php } ?>
							<?php if ( $wb_current_speaker['work_address'] != '' ){ ?>
							<p class="wb-ent-aanaapf-speaker-data"><?php echo $wb_current_speaker['work_address']; ?></p>
							<?php } ?>
							<?php if ( trim($wb_current_speaker['slide']) != '' ){ ?>
	    						<a href="<?php echo urldecode($wb_current_speaker['slide']); ?>" target="_blank" class="btn btn-primary btn-sm wb-ent-aana-slides-btn" role="button">Slides</a>
	    					<?php } ?>
						</div>
					</div>
					<?php 
							}
						}
					}
					?>
					
					
					<p class="wb-ent-aanaapf-hub-session-description"><?php echo wpautop($wb_virtual_post['post_content']); ?></p>
					
					
					
					
					
					
					
					<?php 
					include ( get_template_directory() . '/includes/widgets/adsWideCustom.php');
					
					
                //include ( get_template_directory() . '/includes/widgets/adsWideCustom.php');
					}else{//if $wb_lms_access
						?>
						<div class="alert alert-warning" role="alert" style="text-align: center; font-size: 20px;">
							You don't have access to this session. Click <a href="<?php echo "/?p=".$wb_hub_page_id; ?>" style="text-decoration: underline;">here</a> to go back to HUB page.
						</div>
					<?php 
					//echo "no access";	
					}
				?>
            </div>            
        
        <!-- END HTML HERE -->
        </div>
    </div> <!-- end #main -->

    <link href="/wp-includes/css/lightbox-simple-style.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/aana-hub.js"></script>
    <!-- <link type="text/css" rel="stylesheet" href="<?php //echo get_stylesheet_directory_uri(); ?>/css/aana-hub.css" /> -->
	<link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/fontawesome-free-5.13.1-web/css/all.css" />

<?php get_sidebar('hub'); // sidebar 1  ?>

</div> <!-- end content -->
<?php if ( $wb_virtual_post['wb_virtual_conference_add_to_calendar'] != "no" ){ ?>
        <script>
        	var myCalendar = createCalendar({
                options: {
                  class: 'my-class',
                  id: 'my-id'                               // You need to pass an ID. If you don't, one will be generated for you.
                },
                data: {
                  title: '<?php echo str_replace("'","\'",$wb_virtual_post->post_title); ?>',     // Event title
                  start: new Date('<?php echo $calendar_date_from; ?>'),   // Event start date
                  //duration: 120,                            // Event duration (IN MINUTES)
                  end: new Date('<?php echo $calendar_date_to; ?>'),     // You can also choose to set an end time.
                                                            // If an end time is set, this will take precedence over duration
                  address: '<?php echo $actual_link; ?>',
                  description: '<?php echo str_replace("'","",$calender_content); ?>',
                  //timezone: "<?php echo $wb_calendar_timezone; ?>"    
                }
              })
        
              $( document ).ready(function() {
            	  document.querySelector('.new-cal').appendChild(myCalendar);
              });
        </script>
<?php } ?>
<script type="text/javascript">
 		/*
      	This script controls the styling and functionality of the category/topic box on the sidebar(includes/box/vid_topic.php)
      	*/
/*
      	$( document ).ready(function() {
      	    //console.log( "ready bee" );
      	});

      
          //console.log("workerbee");
      	<?php if(!$wb_ent_options['catlisttype']['collapse']){ ?>
      		$('#categories li ul').hide();
			$('#categories .subs-h').addClass("collapsed_boxarrow");
        <?php }else{ ?>
			$('#categories li ul li ul').hide();
		  	$('#categories .subs-h').addClass("expanded_boxarrow");
		<?php }
        	if($wb_ent_options['catlisttype'] == 'image'){
            	$ulMargin = '85px';
            	$arrowBgX = '5px';
            	$arrowDownBgX = '4px';
            	$arrowDownBgY = '4px';
         	}else{
            	$ulMargin = '15px';
            	$arrowBgX = 'left';
            	$arrowDownBgX = '10px';
            	//$arrowDownBgY = 'top';
         	}
        ?>
        	$("#categories .subCat h5").css('background','none');
         	$("#categories h5").css('margin','0');
         	//$('#categories .subs-h').addClass("collapsed_boxarrow");

         	$('#categories .subs-h').click(function() {
	    		if ($(this).next("ul").is(":hidden")){
					$(this).next("ul").slideDown(200);
				}else{
					$(this).next("ul").slideUp(200);
				}
	            //$(this).next("#categories ul").slideToggle("fast");
	            $('#categories  .subs-h').toggleClass("collapsed_boxarrow expanded_boxarrow");
         	});

         	$('#categories .topicList h5').click(function() {
				if ($(this).next("#categories .topicList ul").is(":hidden")){
					$(this).next("#categories .topicList ul").slideDown(200);
				}else{
					$(this).next("#categories .topicList ul").slideUp(200);
				}
              	//$(this).next("#categories .topicList ul").slideToggle("fast");
              	$(this).children('.iconwb').toggleClass("icon-plus icon-minus");
            });
        	/*
        	$('#categories .topicList h5 a').click(function() {
         	alert("clicked a");
         	});
         	*/
         	/*
         	$('#categories ul ul li').click(function() {
            	$('#vid_list').slideDown(200);
            	$('#video_list').fadeOut(200);
         	});

         	$('#categories .topicList h5').mouseover(function(){
            	$(this).css('color', '#ffffff');
            	$(this).children('i').addClass('icon-white');
            	$(this).children('a').css('color', '#ffffff');
            	$(this).children('i').css('color', '#ffffff');
         	});

         	$('#categories .topicList h5').mouseout(function(){
            	$(this).css('color', '#000000');
             	$(this).children('i').removeClass('icon-white');
             	$(this).children('a').css('color', '#000000');
         	});

         	$('#categories ul li').hover(function() {
            	$(this).css('cursor','pointer');
         	});
         	$('#categories ul ul li').hover(function() {
            	//$(this).css({'cursor':'pointer', 'color':'#016891', 'padding-left':'8px',  'text-decoration':'none'});
         	});
         	$('#categories ul ul li').mouseout(function() {
            	//$(this).css({'cursor':'arrow', 'color':'#000000', 'padding-left':'8px', 'text-decoration':'none'});
         	});
      */	
	</script>    
	<style>
	.add-to-calendar-checkbox{
	        background-color: #009daa;
	        border: 1px solid #009daa;
	        margin-left: 0;
	        padding-left: 1.4rem;
	        padding-right: 1.4rem;
	        margin-right: 0;
	        color: #fff;
	        padding: 10px 20px;
	        border-radius: 10px;
	        font-weight: 600;
	        margin-right: 0.8rem;
	    }
	    .add-to-calendar-checkbox:checked ~ a {
	        margin-bottom: 10px;
	    }
	    .add-to-calendar-checkbox:hover,
	    .wb-hub-on-air-digital-launch-session-btn:hover{
	    	cursor: pointer;
	    	background-color: #01aebc;
    		border: 1px solid #01aebc;
    		color: #fff;
	    }
	    #my-id {
		    font-size: 14px;
		    position: relative;
		    background: white;
		}
		.new-cal{
			max-width: 200px;
			display: block;
		}
		@media (max-width: 1024px){
			.new-cal {
			    max-width: 200px;
			    display: block;
			    position: relative;
			    margin-top: 13px;
			    vertical-align: -webkit-baseline-middle;
			    margin-right: 6px;
			}
		}
		.isDisabled {
			  color: #fff;
			  cursor: not-allowed;
			  opacity: 0.5;
			  text-decoration: none;
			  pointer-events: none;
		}
		#playerDiv {
		    clear: both;
		    margin: 10px 0 10px 0;
		}
		div.alert {
		    padding: 14px 35px 14px 14px;
		}
		#wb-hub-poll-wrapper{
			margin-bottom: 20px;
			margin-top: 20px;
		}
		#wb-hub-player-below-buttons{
			width: 90%;
			margin: 0 auto;
			text-align: center;
		}
		#wb-hub-player-below-buttons a {
			display: inline-grid;
		    max-width: 350px;
		    width: 100%;
		}
		#adActionDiv {
		    max-width: 468px;
		    height: 60px;
		    margin: 25px auto 25px auto;
		}
		#div-gpt-ad-1588677728893-0{
			margin: 30px auto;
		}
		.addeventatc{
			border-radius: 8px;
		    margin-right: 0.8em;
		    background-color: rgb(0, 157, 170);
		    padding: 12px 21px;
		    color: rgb(255, 255, 255) !important;
		    vertical-align: bottom;
		    text-align: center;
    line-height: 16px;
		}
		.addeventatc_icon{
			display: none;
		}
		.addeventatc_dropdown .copyx{
			display: none;
		}
		.add-to-calendar-checkbox:hover, .wb-hub-on-air-digital-launch-session-btn:hover, .addeventatc:hover {
		    cursor: pointer;
		    background-color: #01aebc;
		    border: 1px solid #01aebc; 
		    color: #fff;
		}
	</style>
<?php
get_footer();
?>