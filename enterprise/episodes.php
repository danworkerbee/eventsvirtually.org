<?php
/**
 * filename: episodes.php
 * description: this will be the template to be used to display episodes
 * author: Jullie Quijano
 * date created: 2014-04-15
 *
 *
 * @package WordPress
 * @subpackage Enterprise
 
 * Template Name: Episodes
 */
global $wb_ent_options, $current_lang, $moretext;

get_header();

$episodeSearchYear = 0;
$conditionString = '';
if (isset($_GET['epY']) && trim($_GET['epY']) != '' && is_numeric($_GET['epY'])) {
	$episodeSearchYear = $_GET['epY'];
	$conditionString = sprintf("AND p.post_date >= '%s-01-01' AND p.post_date < '%s-01-01'", $episodeSearchYear, ($episodeSearchYear+1) );
}
$moretext = 'more';
switch($current_lang){
	case 'en_US' :
		$catlibrary =  $wb_ent_options['videocats']['library'];
		break;
	case 'fr_FR' :
		$catlibrary = $wb_ent_options['videocats']['library-fr_FR'];
		$moretext = 'plus';
		break;
}
$result = wb_get_cat_vids($catlibrary, $conditionString, true);

$episodeVideo = $result['posts'];
$max_pages = $result['max_pages'];
$vidPage = sanitize_text_field($_GET['vidPage']);

$videoDescLimit = $wb_ent_options['videolistinfo']['desclimit'];
?>
<div id="wb_ent_content" class="clearfix row-fluid">
    <?php
    if( count($episodeVideo) > 0 ){
    ?>
    <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
        <div id="all-videos" class="video-list-li">  
            <div class="page-header"><h1 class="pagetitle"><?php
                if ($episodeSearchYear != 0) {
                    ?>
                    <?php echo $episodeSearchYear; ?> <?= _e('Episodes', 'enterprise') ?>
                    <?php
                } else {
                    ?>
                    <?= _e('Most Recent Episodes', 'enterprise') ?>
                    <?php
                }
                ?></h1></div>
            <div class="row-fluid">
                <ul class="thumbnails">                  
                    <?php
                    
                    foreach ($episodeVideo as $video) {
                        $image = '<img src="' . $video['smlThumb'] . '" alt="' . $video['title'] . '" />';
                        if(strpos($video['desc'], '<img ') !== false){
                              $postContent = wb_format_string($video['desc'], false, false, $videoDescLimit, '');
                            } else {
                              //original
                              $postContent = wb_format_string($video['desc'], false, false, $videoDescLimit, '... <a href="' . get_permalink() . '"><span class="more-link">'.$moretext.'</span></a>');
                            }
                        ?>
                        <li class="span12 wb-subpage-block-wrapper">
                            <div class="thumbnail no-style span4">
                                    <a href="/<?php echo $video['postName']; ?>"> <?php echo $image; ?> </a>
                            </div>
                            <div class="span7">
                                    <a href="/<?php echo $video['postName']; ?>"><h3><?php echo wb_format_string($video['title'], false, true, $wb_ent_options['videolistinfo']['titlelimit'], '...'); ?></h3></a>
                                    <p><?php echo $postContent; ?></p>
                                    <?php
                                    if (count($video['tags']) > 0 && is_array($video['tags'])) {                                        
                                        ?>
                                    <p class="tags">
                                    <span class="tags-title"><?php _e('Tags:', 'enterprise') ?></span>
                                    <?php
                                    /*11.27.2014*/
                                    $count = 0;
                                    $tagTotal = count($video['tags']);
                                    
                                    if($wb_ent_options['videolistinfo']['keywordlimit']){$wb_keyword_limit = $wb_ent_options['videolistinfo']['keywordlimit'];} 
                                      else {$wb_keyword_limit = 0;}
                                    foreach ($video['tags'] as $currentTag) {
                                        $count++;                                        
                                        if($count <= $wb_keyword_limit && $wb_keyword_limit != 0){
                                        ?>
                                        <a rel="tag" href="/keyword/<?php echo $currentTag->slug; ?>" class="label"><?php echo $currentTag->name; ?><?php if($tagTotal > 1 && $count < $tagTotal){echo ',';}?></a>
                                        <?php
                                        } else if($wb_keyword_limit == 0){
                                        ?>
                                        <a rel="tag" href="/keyword/<?php echo $currentTag->slug; ?>" class="label"><?php echo $currentTag->name; ?><?php if($tagTotal > 1 && $count != $tagTotal){echo ',';}?></a>
                                        <?php
                                        }
                                        
                                    }
                                    $count = 0;
                                    /*end 11.27.2014*/
                                    /*
                                    foreach ($video['tags'] as $currentTag) {
                                    ?>
                                        <a rel="tag" href="/keyword/<?php echo $currentTag->slug; ?>" class="label"><?php echo $currentTag->name; ?></a>
                                        <?php
                                        }*/
                                        ?>

                                    </p> 
                                        <?php
                                    }
                                    ?>
                                </div>
                        </li> 
                        <?php } ?>
                </ul>
            </div>
        </div> <!-- END of all videos -->
     
    

        <?php
        if ($max_pages > 1) {
            ?>         
            <div class="row-fluid">
                <div class="pageNav span12">
                    <div id="paginationLeft" class="span4">
                        <?php
                        if (intval($vidPage) > 1) {
                            ?>
                            <a href="?vidPage=<?= intval($vidPage) - 1; ?>">&laquo; <?= _e('Previous', 'enterprise') ?></a>
                            <?php
                        }
                        ?>                    
                    </div>
                    <div id="paginationMid" class="span4">Page <?= ($vidPage != 0 ? $vidPage : 1); ?> <?= _e('of', 'enterprise') ?> <?= $max_pages ?></div>
                    <div id="paginationRight" class="span4">
                        <?php
                        if (intval($vidPage) < $max_pages) {
                            ?>
                            <a href="?vidPage=<?= intval(($vidPage != 0 ? $vidPage : 1)) + 1; ?>"><?= _e('Next', 'enterprise') ?> &raquo;</a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    
    </div>
    <?php
    }
    else{
    ?>
    <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
        <div id="all-videos" class="video-list-li">      
            <h1 class="center"><?= _e('No videos found.', 'enterprise') ?></h1>
            <p><?= _e('Try a different search or', 'enterprise') ?>:</p>

            <?php
            $query = $wpdb->query("SELECT post_name FROM wp_posts
                WHERE post_type='post'
                AND post_status='publish'
                ORDER BY RAND()
                LIMIT 1");

            foreach ($query as $row) {
                $videoname = $row['post_name'];
            }
            ?>
            <div id="linkList404">
                <p>&raquo; <a href="<?php echo get_site_url().'/' . $videoname ?>"><?= _e('Watch a Random Video', 'enterprise') ?></a></p>
                <p>&raquo; <a href="<?php echo get_site_url().'/' ?>"><?= _e('Go Back to Library', 'enterprise') ?></a></p>
                <p>&raquo; <a href="<?php echo get_site_url().'/' ?>viewing-tips"><?= _e('Read Viewing Tips', 'enterprise') ?></a></p>
                <p>&raquo; <a href="<?php echo get_site_url().'/' ?>contact"><?= _e('Contact Us', 'enterprise') ?></a></p>
            </div>            

            <p>&nbsp;</p>    
        </div>
    </div>
    <?php
        get_sidebar();
    }
    ?>
    <?php
    get_sidebar(); // sidebar 1 
    ?>
    <?php
    get_footer();
    ?>