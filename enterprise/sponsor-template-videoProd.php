<?php

/*
  Filename:      sponsor-template-videoSolutions.php
  Description:   will display the Video Solutions(page 1) part of the Sposonr Template for Naylor
  Author: 	      Jullie Quijano
  Change Log:
  November 26, 2013 [Jullie]added code to display message if file is included
  August 29, 2012 	[Jullie] Created the file
 */

/**
 * @package WordPress
 * @subpackage Enterprise APF
 * Template Name: Sponsor Template Video Production v.6
 */
//error_reporting(E_ALL);
//echo '	lib ';
// Sitewide Variables
if (!defined('CHANNEL_SITE'))
    include get_template_directory_uri() . '/library/includes/config.php';
get_header();



//will determine if file is included
if ($_SERVER['REMOTE_ADDR'] == WORKERBEE_IP) {
    echo '<!-- START OF ' . __FILE__ . ' -->';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/naylorsponsor.css" />

<style> 

    #videoTopics{
        /*width: 636px;*/
        float: left;
    }

    #videoCreation{
        clear: both;
    }

    #apfScreenshot{
        float: right; 
        margin: 30px 0 10px;
    }

    #v #centerMessage{
        width: 575px;
        height: 50px;
        margin: 25px auto 0;
        background-color: #d9e5ee;
        color: #243c5f;
        font-size: 18px;
        clear: both;
        font-weight: bold;
        padding: 25px 47px;
    }

    #wb_ent_content #vidEverywhere{
        float: right; 
        margin: 45px 0 10px;
    }

    #wb_ent_content #embedDivMain{
        width: 630px;
        height: 250px;
        margin: 50px auto 55px;
    }

    #wb_ent_content .embedDiv{
        width: 300px;
        float: left;
        font-size: 15px;
    }

    #wb_ent_content #buttonsDiv{
        /*height: 55px;*/
        margin: 0 auto;
        overflow: auto;
        width: 900px;
    }

    #buttonsDiv a{
        font-weight: bold;
        background: url("/images/sponsorTempButtonBg.png") no-repeat scroll 0 0 transparent;
        color: #FFFFFF;
        display: block;
        float: left;
        font-size: 15px;
        height: 19px;
        margin: 0 6px 0 0;
        padding: 19px 0;
        width: 294px;
    }

    #buttonsDiv a:hover{
        background: url('/images/sponsorTempButtonBg.png') no-repeat 0 -58px;
        text-decoration: none;
    }

    #buttonsDiv a:active{
        background: url('/images/sponsorTempButtonBg.png') no-repeat 0 -116px;
    }

    hr{
        color: #3c9bff;
        width: 920px; 
        border: 0 none;
        border-top: 4px solid;
        margin: 10px 0;
    }





    #wb_ent_content #playerBefore{
        float: left;
        height: 200px;
        /*width: 250px;*/
        color: #22547d;
        font-size: 18px;
        font-weight: bold;
        /*margin-bottom: 40px;*/
        text-align: left;
    }

    #wb_ent_content #playerAfter{
        float: left;
        height: 360px;
        /*width: 572px;*/
        color: #22547d;
        font-size: 18px;
        font-weight: bold;
        /*margin-bottom: 40px;*/
        text-align: left;
    }

    #apfScreenshot {
        float: right;
        margin: 60px 0 10px !important;
    }



    #videoTopics{
        margin-top: 70px !important;
    }



    #wb_ent_content .playersDiv div, #wb_ent_content .playersDiv object{
        margin-top: 5px;
    }

    #wb_ent_content #playerBefore.playersDiv{
        float: left;
    }

    #wb_ent_content #playerAfter.playersDiv{
        float: left;
    }





    #wb_ent_content #tableDiv ul li{
        margin: 5px 0;
    }

    #tableDiv{
        /*width: 923px;   */
        /*margin-bottom: 55px !important;*/
        clear: both;
    }

    #tableDiv table{
        text-align: left;
        font-size: 15px;
        /*border: 2px solid #cacaca;*/   
    }

    #tableDiv table ul{
        margin-left: 20px;
    }

    #tableDiv table ul.clear li{
        list-style: none;
    }

    #tableDiv table ul.clear li li{
        list-style: disc outside;
        margin-left: 10px;
    }

    #tableDiv tr th{
        font-size: 23px;
        font-weight: bold;
        vertical-align: middle;
        color: #193a5d;
    }

    #tableDiv tr th{
        padding: 23px 18px;
        /*border: 2px solid #193a5d;*/
        font-size: 23px;      
    }

    #tableDiv tr td{
        padding: 23px 0;
        /*border: 2px solid #cacaca;*/
    }

    #tableDiv tr td div{    
        padding-left: 30px;
        /*width: 610px;*/
    }

    #tableDiv tr.evenRow{
        background-color: #cae4ff;
    }

    #tableDiv table thead th{
        padding: 9px 15px;
        color: #fff;
        font-size: 24px;
    }

    #tableDiv table thead tr{
        /*background: url('/images/sponsorTempTableHead.png') repeat-x #3c9bff;*/
        background: #3c9bff;      
    }

    #tableDiv table thead tr th{
        text-align: center;
    }

    #wb_ent_content span.bold{
        font-weight: bold;
        color: #2a3f50;
    }

    #wb_ent_content #tableDiv tr td span{   
        color: #00356e;
    }
</style>
<div id="wb_ent_content" class="clearfix row-fluid">

    <div id="wb_ent_main" class="" role="main" style="border: 0px solid black;">
        <div id="sectionNav">
            <ul>
                <li><a href="/naylor-banner-solutions" ><?= _e('Banner Solutions', 'enterprise') ?></a></li>
                <li><a href="/naylor-video-solutions" ><?= _e('Video Solutions', 'enterprise') ?></a></li>
                <li><a href="/naylor-video-production" class="currentPage"><?= _e('Video Production', 'enterprise') ?></a></li>
                <li><a href="/sponsor-contact"><?= _e('Request Information', 'enterprise') ?></a></li>
            </ul>
        </div>      


        <div class="section span12" id="videoCreation">
            <h2><?= _e('Use Our Professional Video Creation Capabilities, or Provide Your Own', 'enterprise') ?></h2>
            <p><?= _e('We understand video and how it can build your brand and your business. That\'s why we\'ve been so successful in supporting our clients with creative
                video production, whether it is for education, or promotion. We capture your video at your desired location, from your corporate office to a tradeshow booth.
                Our expert production team will produce your video to align with your brand, and package it for marketing on your association website, banner ads,
                social media and email, including our two pop-up clickable banners.', 'enterprise') ?></p>
            <div class="playersDiv" id="playerBefore" style="margin: 0;">
                <p><?= _e('Raw footage we receive', 'enterprise') ?></p>
                <!-- Start of Brightcove Player -->

                <div style="display:none">

                </div>

                <!--
                By use of this code snippet, I agree to the Brightcove Publisher T and C 
                found at https://accounts.brightcove.com/en/terms-and-conditions/. 
                -->
                <?php
                if( $_SERVER['HTTPS'] == 'on' ){
                ?>
                <script language="JavaScript" type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>
                <?php
                }
                else{
                ?>
                <script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>
                <?php                   
                }
                ?>
                
                <script type="text/javascript">brightcove.createExperiences();</script>
                <object id="myExperience1709824262001" class="BrightcoveExperience">
                    <param name="bgcolor" value="#FFFFFF" />

                    <param name="width" value="250" />
                    <param name="height" value="200" />
                    <param name="playerID" value="1814867457001" />
                    <param name="playerKey" value="AQ~~,AAABZ9IPLEk~,hzlB2f0fLk9gFbF7MYsp4EHWJaD1jOw4" />
                    <param name="isVid" value="true" />
                    <param name="isUI" value="true" />
                    <param name="dynamicStreaming" value="true" />
                    <?php
                    if( $_SERVER['HTTPS'] == 'on' ){
                        echo '<param name="secureConnections" value="true" />';
                        echo '<param name="secureHTMLConnections" value="true" />';
                    }
                    ?>                    

                    <param name="@videoPlayer" value="1709824262001" />
                </object>

                <!-- 
                This script tag will cause the Brightcove Players defined above it to be created as soon
                as the line is read by the browser. If you wish to have the player instantiated only after
                the rest of the HTML is processed and the page load is complete, remove the line.
                -->
                <!-- End of Brightcove Player -->

            </div>

            <div style="float: left; margin: 90px 0 0px 10px; height: 200px;" class="">
                <img src="<?php echo get_template_directory_uri(); ?>/images/sponsorTempBecomes1.png" alt="Becomes" />
            </div>

            <div class="playersDiv" id="playerAfter">
                <p><?= _e('Finished video ready for distribution', 'enterprise') ?></p>
                <!-- Start of Brightcove Player -->

                <div style="display:none">            
                </div>

                <!--
                By use of this code snippet, I agree to the Brightcove Publisher T and C 
                found at https://accounts.brightcove.com/en/terms-and-conditions/. 
                -->

                <?php
                if( $_SERVER['HTTPS'] == 'on' ){
                ?>
                <script language="JavaScript" type="text/javascript" src="https://sadmin.brightcove.com/js/BrightcoveExperiences.js"></script>
                <?php
                }
                else{
                ?>
                <script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>
                <?php                   
                }
                ?>

                <object id="myExperience1709824266001" class="BrightcoveExperience">
                    <param name="bgcolor" value="#FFFFFF" />     
                    <param name="width" value="572" />
                    <param name="height" value="360" />
                    <param name="playerID" value="1866533037001" />
                    <param name="playerKey" value="AQ~~,AAABZ9IPLEk~,hzlB2f0fLk-3kE98M6-EuwEhRT-ZNPN0" />
                    <param name="isVid" value="true" />
                    <param name="isUI" value="true" />
                    <param name="dynamicStreaming" value="true" />
                    <param name="@videoPlayer" value="1709824266001" />
                    <?php
                    if( $_SERVER['HTTPS'] == 'on' ){
                        echo '<param name="secureConnections" value="true" />';
                        echo '<param name="secureHTMLConnections" value="true" />';
                    }
                    ?>                      

                </object>

                <!-- 
                This script tag will cause the Brightcove Players defined above it to be created as soon
                as the line is read by the browser. If you wish to have the player instantiated only after
                the rest of the HTML is processed and the page load is complete, remove the line.
                -->
                <!-- End of Brightcove Player -->         
            </div>
        </div>    

        <div class="section span6" id="videoTopics" >
            <h2><?= _e('Great Video Topics That Work', 'enterprise') ?></h2>
            <ul class="noStyle">        
                <li><span class="bold"><?= _e('1) Customer Success Stories and Testimonials', 'enterprise') ?></span> - <?= _e('Captivate your audience with stories from your customers.', 'enterprise') ?></li>
                <li><span class="bold"><?= _e('2) "How To" Video Series', 'enterprise') ?></span> <?= _e('', 'enterprise') ?>- <?= _e('Share tutorials on how to use your products or services.', 'enterprise') ?></li>
                <li><span class="bold"><?= _e('3) Thought Leadership and Education Videos', 'enterprise') ?></span> - <?= _e('Be seen as an expert in your field and provide value added insights relevant to your industry and customer base.', 'enterprise') ?>  </li>
                <li><span class="bold"><?= _e('4) Tips and Techniques Videos', 'enterprise') ?></span> - <?= _e('Useful tips that demonstrate your knowledge and service capabilities.', 'enterprise') ?> </li>              
            </ul> 
            <p><a style="text-decoration: none;" href="/naylor-video-solutions" class="btn btn-primary"><?= _e('Learn about the benefits of Content Marketing', 'enterprise') ?></a></p>

        </div>
        <div class="section span4" id="apfScreenshot">
            <img src="<?php echo get_template_directory_uri(); ?>/images/sponsorTempVideoScreen.jpg" style="padding-top: 7px;" />
        </div>

        <div class="" style="border: 0px solid red;">
            <div class="section" id="tableDiv" style="border: 0px solid green;">
                <table class="table" style="border: 0px solid blue;">
                    <thead class="">
                        <tr>
                            <th colspan="2"><?= _e('Video Content Marketing Process', 'enterprise') ?></th>
                        </tr>
                    </thead>
                    <tbody class="">
                        <tr class="oddRow">
                            <th>
                                <?= _e('Planning Process', 'enterprise') ?>
                            </th>
                            <td>
                                <div>
                                    <span><strong><?= _e('Script Development', 'enterprise') ?></strong></span>
                                    <ul>
                                        <li><?= _e('We define objectives, messaging, calls to action, and your measurements of success', 'enterprise') ?></li>
                                        <li><?= _e('We review your content and outline the key messages to deliver', 'enterprise') ?></li>
                                        <li><?= _e('Script is approved by you', 'enterprise') ?></li>
                                    </ul>
                                    <span><strong><?= _e('Storyboard Creation', 'enterprise') ?></strong></span>
                                    <ul>
                                        <li><?= _e('Imagery Selection', 'enterprise') ?></li>                        
                                    </ul>
                                    <span><strong><?= _e('Video Planning', 'enterprise') ?></strong></span>
                                    <ul>
                                        <li><?= _e('Defines graphics, music, tone, people, voice over and on-screen imagery required', 'enterprise') ?></li>
                                    </ul>            
                                </div>
                            </td>
                        </tr>
                        <tr class="evenRow">
                            <th>
                                <?= _e('Film Production and Execution', 'enterprise') ?>
                            </th>
                            <td>
                                <div>
                                    <span><strong><?= _e('Filming', 'enterprise') ?></strong></span>	
                                    <ul>
                                        <li><?= _e('We send a professional crew to film at your desired destination (if required)', 'enterprise') ?></li>                       
                                    </ul>
                                    <span><strong><?= _e('Editing', 'enterprise') ?></strong></span>
                                    <ul>
                                        <li><?= _e('Following the approved script, the first draft of the video is created to meet the objectives and goals', 'enterprise') ?></li>
                                    </ul>
                                    <span><strong><?= _e('Video Approval', 'enterprise') ?></strong></span>
                                    <ul>
                                        <li><?= _e('Online draft of your video is presented for edits and approval', 'enterprise') ?></li>
                                    </ul>
                                </div>
                            </td>
                        </tr> 
                        <tr class="oddRow">
                            <th>
                                <?= _e('Video Distribution & Outbound Marketing', 'enterprise') ?>
                            </th>
                            <td>
                                <div>
                                    <span><strong><?= _e('Leverage our', 'enterprise') ?> <em>"Video Everywhere!"</em> Player for:</strong></span>
                                    <ul>
                                        <li><span><strong><?= _e('Inbound Marketing', 'enterprise') ?></strong></span>
                                            <ul>
                                                <li><?= _e('Embed your video on your website', 'enterprise') ?></li>
                                                <li><?= _e('Click-thru conversion banner', 'enterprise') ?></li>
                                            </ul>                           
                                        </li>
                                        <li><span><strong><?= _e('Outbound Marketing', 'enterprise') ?></strong></span>
                                            <ul>
                                                <li><?= _e('Receive a custom embed code that produces a click banner that pops and plays your branded video on websites, e-newsletter, paid media', 'enterprise') ?></li>
                                                <li><?= _e('Use our sharing functionality to post your video on social networks, or our video email capability', 'enterprise') ?></li>
                                                <li><?= _e('QR Code to provide access to your video', 'enterprise') ?></li>
                                            </ul>                        
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr class="evenRow">
                            <th>
                                <?= _e('Success Measurement', 'enterprise') ?>
                            </th>
                            <td>
                                <div>
                                    <span><strong><?= _e('Integration to your Google Analytics allows for:', 'enterprise') ?></strong></span>
                                    <ul>
                                        <li><?= _e('Video views and impressions', 'enterprise') ?></li>
                                        <li><?= _e('Banner clicks', 'enterprise') ?></li>
                                    </ul>
                                    <span><strong><?= _e('Lead generation and Lead Nurturing capability', 'enterprise') ?></strong></span>
                                </div>
                            </td>
                        </tr>                
                    </tbody>
                </table>
                <br />
            </div>

            <div class="section" style="" >
                <a class="btn btn-primary" style="padding:20px 24px;text-decoration: none; font-size: 16px;" href="/naylor-banner-solutions"><?= _e('Our Banner Solutions', 'enterprise') ?></a>
                <a class="btn btn-primary" style="padding:20px 24px;text-decoration: none; font-size: 16px;" href="/naylor-video-solutions"><?= _e('Our Video Solutions', 'enterprise') ?></a>
                <a class="btn btn-primary" style="padding:20px 24px;text-decoration: none; font-size: 16px;" href="/sponsor-contact"><?= _e('Request More Information Here', 'enterprise') ?></a>
            </div>
        </div>
    </div>
</div>

<?php

//will determine if file is included
if ($_SERVER['REMOTE_ADDR'] == WORKERBEE_IP) {
    echo '<!-- END OF ' . __FILE__ . ' -->';
}
get_footer();
?>