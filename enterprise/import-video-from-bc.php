<?php
/**
 * filename: import-video-from-bc.php
 * description: this will grab the information from the wb_ent_option and based on the variable below will automatically get videos from the brightcove account and create them as posts
 * author: Jullie Quijano
 * date created: 2016-01-05
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: Import Videos From Brightcove Page
 */
global $wb_ent_options;
if( !defined('DB_USER') ){
    include $_SERVER['DOCUMENT_ROOT'].'/wp-config.php';
}

include( ABSPATH . 'wp-admin/includes/image.php' );

error_reporting(0);
$wb_ent_options = get_option('wb_ent_options');
$video_info = null;


//make sure this script is only run inside the office
$ipRestrictionArray = explode(',', $wb_ent_options['workerbeeip'] );
$allowedIps = array();
foreach($ipRestrictionArray as $currentIp){
    if(trim($currentIp) != '' ){
        $allowedIps[] = trim($currentIp);
    }
    
}
$userIp = trim($_SERVER['REMOTE_ADDR']);
if( (!in_array($userIp, $allowedIps)) || trim($wb_ent_options['workerbeeip']) == '' )
{
header( 'location: /');
exit;
}

$current_token = $wb_ent_options['brightcoveinfo']['readurltoken'];	
//$current_token = '_l_0lDWiVNohyR4DQeZu6Nx9t4XVmvP6ldDKJu2UufA.';	


$member_only_password = 'AICPA2016!';
$category_info_array = array(
	'library' => $wb_ent_options['videocats']['library'],
	'unlisted' => $wb_ent_options['videocats']['unlisted'],
	'private' => $wb_ent_options['videocats']['private'],
	'membersOnly' => 253,
	'uncategorized' => 617,
	'private' => $wb_ent_options['videocats']['private'],
);

//only process after submission
if( isset($_POST['processQuery']) && trim($_POST['processQuery']) == 'Submit'){		
	//$videoId = ( isset($_POST['videoId']) && trim($_POST['videoId']) != '' ) ? trim($_POST['videoId']) : ( (isset($_GET['videoId']) && trim($_GET['videoId']) != '') ? trim($_GET['videoId']) : '' );
	//$curlString = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id=' . $videoId . '&video_fields=name,length,shortDescription,longDescription,tags,videoStillURL,customFields&media_delivery=http&token='.$wb_ent_options['brightcoveinfo']['readurltoken'];	
	$post_status = sanitize_text_field($_POST['postStatus']);
	$video_limit = sanitize_text_field($_POST['videoLimit']);
	$page_size = sanitize_text_field($_POST['pageSize']);
	$page_number = sanitize_text_field($_POST['pageNumber']);
	$script_action = sanitize_text_field($_POST['scriptAction']);
	
	if( !is_numeric($video_limit) ){
		$video_limit = -1;
	}
	
	if( !is_numeric($page_size) ){
		$page_size = 100;
	}
	
	if( !is_numeric($page_number) ){
		$page_number = 0;
	}	
	
	if( $video_limit == -1 || $video_limit > $page_size ){
		//initial query
		$query_args = array();
		$query_args['command'] = 'search_videos';
		$query_args['get_item_count'] = 'true';
		$query_args['page_size'] = $page_size;
		$query_args['video_fields'] = 'id';
		$query_args['page_number'] = $page_number;
		$query_args['token'] = $current_token;			

		$initial_query_string = http_build_query($query_args);

		$initial_curl_string = 'http://api.brightcove.com/services/library?'.$initial_query_string;	
		
		$ch = curl_init();
		$timeout = 0; // set to zero for no timeout
		curl_setopt($ch, CURLOPT_URL, $initial_curl_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$initial_file_contents = curl_exec($ch);
		curl_close($ch);
		
		$initial_video_info = json_decode($initial_file_contents, true);
		$debugArray[] = "\r\n".'<br />$initial_video_info is '.print_r($initial_video_info, true);
		
		$debugArray[] = "\r\n".'<br />count is '.$initial_video_info['total_count'].'-'.($initial_video_info['total_count'] > 0).'-';
		
		if( $initial_video_info['total_count'] > 0 && $initial_video_info['total_count'] > $page_size ){
			if( $video_limit == -1 ){
				$total_pages = ceil($initial_video_info['total_count'] / $page_size);	
			}
			else{
				$total_pages = ceil($video_limit / $page_size);	
			}
				
			$debugArray[] = "\r\n".'<br />$total_pages is '.$total_pages;
			
			$video_info = array();
			
			for( $i=0; $i < $total_pages; $i++ ){
				$query_args = array();
				$query_args['command'] = 'search_videos';
				$query_args['get_item_count'] = 'true';
				$query_args['page_size'] = $page_size;
				$query_args['video_fields'] = 'id,name,shortDescription,longDescription,tags,videoStillURL,customFields';
				$query_args['page_number'] = $i;
				$query_args['token'] = $current_token;		

				$query_string = http_build_query($query_args);

				$curlString = 'http://api.brightcove.com/services/library?'.$query_string;

				$debugArray[] = "\r\n".'<br />$curlString is '.$curlString;

				$ch = curl_init();
				$timeout = 0; // set to zero for no timeout
				curl_setopt($ch, CURLOPT_URL, $curlString);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
				$file_contents = curl_exec($ch);
				curl_close($ch);

				$video_info_temp = json_decode($file_contents, true);	
				$remainder = ($video_limit % $page_size);
				if($i == ($total_pages-1) && $remainder != 0 ){
					$video_items = array_slice($video_info_temp['items'], 0, $remainder);
				}
				else{
					$video_items = $video_info_temp['items'];
				}
				
				$video_info = array_merge($video_info, $video_items);
			}
		}		
	}
	else{
		$query_args = array();
		$query_args['command'] = 'search_videos';
		$query_args['get_item_count'] = 'true';
		$query_args['page_size'] = $video_limit;
		$query_args['video_fields'] = 'id,name,shortDescription,longDescription,tags,customFields,videoStillURL';
		$query_args['page_number'] = $page_number;
		$query_args['token'] = $current_token;		

		$query_string = http_build_query($query_args);

		$curlString = 'http://api.brightcove.com/services/library?'.$query_string;

		$debugArray[] = "\r\n".'<br />$curlString is '.$curlString;

		$ch = curl_init();
		$timeout = 0; // set to zero for no timeout
		curl_setopt($ch, CURLOPT_URL, $curlString);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$file_contents = curl_exec($ch);
		curl_close($ch);

		$video_info = json_decode($file_contents, true)['items'];				
	}
		
	$debugArray[] = "\r\n".'<br />$video_info is '.print_r($video_info, true);
	$debugArray[] = "\r\n".'<br />count $video_info is '.(count($video_info));
	
	//process each video from brightcove
	if( count($video_info) > 0 ){
		foreach($video_info as $current_video){
			$post_password = '';
			$post_status = sanitize_text_field($_POST['postStatus']);
			$debugArray[] = "\r\n".'<br />$current_video is '.print_r($current_video, true);
			//check if media id already exists on the media
			$wb_media_entry_status = $wpdb->get_results( 
				$wpdb->prepare( '
					SELECT status 
					FROM wb_media m, wp_posts p
					WHERE p.post_id
					AND m.media_id = ?
				',$current_video['id']), ARRAY_A
				);
			$debugArray[] = "\r\n".'<br />$wb_media_entry_status is '.print_r($wb_media_entry_status, true);
			
			if( $wpdb->num_rows <= 0 ){
				$debugArray[] = "\r\n".'<br />creating new post';
				$video_title = $current_video['name'];			

				if( trim($current_video['longDescription']) != '' ){
					$video_desc = $current_video['longDescription'];  
				}
				elseif( trim($current_video['shortDescription']) != '' ){
					$video_desc = $current_video['shortDescription'];
				}
				else{
					$video_desc = '';
				}		

				$video_tags = $current_video['tags'];				
				$video_cats = array();
				
				//check custom fields
				$debugArray[] = "\r\n".'<br />check custom fields';
				if( is_array($current_video['customFields']) && count($current_video['customFields']) > 0 ){
										
					if( !isset($current_video['customFields']['publishtoworld']) || (isset($current_video['customFields']['publishtoworld']) && strtolower(trim($current_video['customFields']['publishtoworld'])) != 'yes') ){
						array_push($video_cats, $category_info_array['private']);
						$post_status = 'private';
						$debugArray[] = "\r\n".'<br />publishtoworld is not yes';
					}

					
					if( isset($current_video['customFields']['membership']) && strtolower(trim($current_video['customFields']['membership'])) == 'members only' ){
						array_push($video_cats, $category_info_array['membersOnly']);
						$post_password = $member_only_password;
						$debugArray[] = "\r\n".'<br />membership is members only';
					}
					
					$debugArray[] = "\r\n".'<br />$category_info_array1 is '.print_r($category_info_array, true);
					
					if( isset($current_video['customFields']['category']) && trim($current_video['customFields']['category']) != '' ){	
						$current_cat_name = trim($current_video['customFields']['category']);
						$debugArray[] = "\r\n".'<br />this has cat -'.($current_cat_name).'-';
						//get current cat from list
						$current_cat = array_search( $current_cat_name, $category_info_array);
						//$debugArray[] = "\r\n".'<br />$current_cat1 is '.print_r($current_cat, true);
						
						//cat does not exists
						if( $current_cat === false ){
							$debugArray[] = "\r\n".'<br />$current_cat is false';
							
							$debugArray[] = "\r\n".'<br />$current_cat_name is '.$current_cat_name;
							
							//check from database first
							$current_cat_id = get_term_by( 'name', $current_cat_name, 'category', ARRAY_A );
							
							//term does not exist
							if( $current_cat_id === false ){
								$cat_arr = array(																
									'parent' => $category_info_array['library'],
									);														

								//insert category
								$current_cat_id = wp_insert_term( $current_cat_name, 'category', $cat_arr );								
							}
							else{
								$current_cat_id = $current_cat_id['term_id'];
							}

							$debugArray[] = "\r\n".'<br />$current_cat_id is '.$current_cat_id;
							if( $current_cat_id != 0 ){
								array_push($video_cats, $current_cat_id);
								$category_info_array[$current_cat_id] = $current_cat_name;
								$current_cat = $current_cat_id;
							}
						}
						else{
							array_push($video_cats, $current_cat);
						}						
						$debugArray[] = "\r\n".'<br />$current_cat2 is '.$current_cat;
						
						if( isset($current_video['customFields']['subcategory']) && trim($current_video['customFields']['subcategory']) != '' ){		
							$current_subcat_name = trim($current_video['customFields']['subcategory']);
							$debugArray[] = "\r\n".'<br />this has subcat';
							//get current cat from list
							$current_subcat = array_search($current_subcat_name, $category_info_array);
							$debugArray[] = "\r\n".'<br />$current_subcat is '.$current_subcat;
							//cat does not exists
							if( $current_subcat === false ){
								//check from database first
								$current_subcat_id = get_term_by( 'name', $current_subcat_name, 'category', ARRAY_A );								
								
								if( $current_subcat_id === false ){
									$debugArray[] = "\r\n".'<br />creating current_subcat';
									$subcat_arr = array(																	
										'parent' => $current_cat,
										);														
									//insert category

									$current_subcat_id = wp_insert_term( $current_subcat_name, 'category', $subcat_arr );									
								}
								else{
									$current_subcat_id = $current_subcat_id['term_id'];
								}

								$debugArray[] = "\r\n".'<br />$current_subcat_id is '.$current_subcat_id;
								if( $current_subcat_id != 0 ){
									array_push($video_cats, $current_subcat_id);
									$category_info_array[$current_subcat_id] = $current_subcat_name;
								}
							}
							else{
								array_push($video_cats, $current_subcat);
							}						
						}						
						
					}
					else{
						array_push($video_cats, $category_info_array['uncategorized']);
					}
				}				
				else{
					$debugArray[] = "\r\n".'<br />no custom fields';
					array_push($video_cats, $category_info_array['private']);
					$post_status = 'private';
				}
				$debugArray[] = "\r\n".'<br />$category_info_array is '.print_r($category_info_array, true);
				$debugArray[] = "\r\n".'<br />$video_cats is '.print_r($video_cats, true);
				
				
				$post = array(
					'post_title' => $video_title,
					'post_content' => $video_desc,
					'post_category' => $video_cats,
					'tags_input' => $video_tags,
					'post_status' => $post_status,
					'post_password' => $post_password
				);
				
				$debugArray[] = "\r\n".'<br />$post is '.print_r($post, true);
				
				$debugArray[] = "\r\n".'<br />inserting post';
				
				if( $script_action == 'createPost' ){
					$current_post_id = wp_insert_post( $post );
				}
				else{
					$current_post_id = false;
				}
				
				$debugArray[] = "\r\n".'<br />$current_post_id is '.$current_post_id;
				
				if( $current_post_id !== false ){
					$debugArray[] = "\r\n".'<br />inserting media id';
					$videoStill = $current_video['videoStillURL'];
					$debugArray[] = "\r\n".'<br />$videoStill is '.$videoStill;				
					
					if( trim($videoStill) != '' && !has_post_thumbnail( $current_post_id )){				
							
							$videoStillDir = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/stills/';
							if (!file_exists($videoStillDir)) {
								mkdir($videoStillDir);
							}								
							echo "\r\n" . '<br />$videoStillDir is ' . $videoStillDir;
							if (trim($videoStill) == '') {
								$parsed = parse_url($wb_ent_options['defaultstill']);
								if (empty($parsed['scheme'])) {
									$videoStill = 'http://' . $wb_ent_options['defaultstill'];
								} else {
									$videoStill = $wb_ent_options['defaultstill'];
								}
							}
							$destinationFile = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/wp-content/uploads/stills/' . $current_post_id . '_still.jpg';							;
							echo "\r\n" . '<br />$destinationFile is ' . $destinationFile;
							
							$updated_post_meta = update_post_meta($current_post_id, 'wb_ppv_prod_video_still', '/images/stills/' . $current_post_id . '_still.jpg');
							echo "\r\n" . '<br />$updated_post_meta is ' . $updated_post_meta;
							
							echo "\r\n" . '<br />copying $videoStill ' . $videoStill;
							if (!copy($videoStill, $destinationFile)) {
								echo "\r\n" . '<br />error copying $videoStill ' . $videoStill;
							}

							$videoStillDir2 = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/wp-content/uploads/stills/';
							if (!file_exists($videoStillDir2)) {
								mkdir($videoStillDir2);
							}
							echo "\r\n" . '<br />$videoStillDir2 is ' . $videoStillDir2;
							
							$destinationFile2 = $videoStillDir2 . $current_post_id . '_still.jpg';
							echo "\r\n" . '<br />$destinationFile2 is ' . $destinationFile2;
							
							echo "\r\n" . '<br />copying $videoStill on $destinationFile2 ';
							if (!copy($videoStill, $destinationFile2)) {
								echo "\r\n" . '<br />error copying $videoStill on $destinationFile2 ';
							}


							$wp_filetype = wp_check_filetype(basename($destinationFile2), null);
							echo "\r\n" . '<br />$wp_filetype ' . print_r($wp_filetype, true);
							
							$attachment = array(
								'post_mime_type' => $wp_filetype['type'],
								'post_title' => preg_replace('/\.[^.]+$/', '', basename($destinationFile2)),
								'post_content' => '',
								'post_status' => 'inherit'
							);
							echo "\r\n" . '<br />$attachment ' . print_r($attachment, true);
							
							$attach_id = wp_insert_attachment($attachment, $destinationFile2, $current_post_id);
							echo "\r\n" . '<br />$attach_id ' . $attach_id;
							
							error_reporting(E_ALL);
							
							$attach_data = wp_generate_attachment_metadata($attach_id, $destinationFile2);
							echo "\r\n" . '<br />$attach_data ';
							
							wp_update_attachment_metadata($attach_id, $attach_data);					
							$videoStillFileName = 'images/stills/' . $current_post_id . '_still.jpg';
							
							update_post_meta($current_post_id, '_thumbnail_id', $attach_id);

							if (!class_exists('S3')){
								echo "\r\n" . '<br />S3 does not exist ';
								require_once('resources/S3.php');
							}
								
							if (!class_exists('SimpleImage')){
								echo "\r\n" . '<br />SimpleImage does not exist ';
								require_once('resources/SimpleImage.php');
							}
								
							$s3 = new S3($wb_ent_options['amazons3info']['accesskey'], $wb_ent_options['amazons3info']['secretkey']);

							if ($s3->putObjectFile($destinationFile, $wb_ent_options['amazons3info']['channelbucket'], $videoStillFileName, S3::ACL_PUBLIC_READ)) {
								//echo "debug: We successfully uploaded your file.<br />";
							} else {
								//echo "debug: Something went wrong while uploading your file... sorry.<br />";
							}

							$updated_post_meta = update_post_meta($current_post_id, 'wb_ppv_prod_video_still', $wb_ent_options['amazons3info']['channelbucketurl'] . '/' . $videoStillFileName);

							$image = new SimpleImage();
							$image->load('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/wp-content/uploads/stills/' . $current_post_id . '_still.jpg');

							//	create large-sized thumb

							$image->resize(375, 211);
							$destinationFileLrgThumb = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_largeThumb.jpg';
							$image->save($destinationFileLrgThumb, IMAGETYPE_JPEG, 100, 0777);

							$largeThumbFileName = 'images/thumbs/' . $current_post_id . '_largeThumb.jpg';
							$filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_largeThumb.jpg';

							if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $largeThumbFileName, S3::ACL_PUBLIC_READ)) {
								//echo "debug: We successfully uploaded your file.<br />";
							} else {
								//echo "debug: Something went wrong while uploading your file... sorry.<br />";
							}

							$largeThumbFileName = null;


							//	create mid-sized thumb

							$image->load('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/wp-content/uploads/stills/' . $current_post_id . '_still.jpg');
							$image->resize(240, 135);
							$destinationFileMidThumb = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_midThumb.jpg';
							$image->save($destinationFileMidThumb, IMAGETYPE_JPEG, 100, 0777);

							$midThumbFileName = 'images/thumbs/' . $current_post_id . '_midThumb.jpg';
							$filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_midThumb.jpg';

							if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $midThumbFileName, S3::ACL_PUBLIC_READ)) {
								//echo "debug: We successfully uploaded your file.<br />";
							} else {
								//echo "debug: Something went wrong while uploading your file... sorry.<br />";
							}

							$midThumbFileName = null;
							$image->load('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/wp-content/uploads/stills/' . $current_post_id . '_still.jpg');
							$image->resize(160, 90);
							$destinationFileSlide = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/slides/' . $current_post_id . '_slide.jpg';
							$image->save($destinationFileSlide, IMAGETYPE_JPEG, 100, 0777);

							$slideFileName = 'images/slides/' . $current_post_id . '_slide.jpg';
							$filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/slides/' . $current_post_id . '_slide.jpg';

							if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $slideFileName, S3::ACL_PUBLIC_READ)) {
								//echo "debug: We successfully uploaded your file.<br />";
							} else {
								//echo "debug: Something went wrong while uploading your file... sorry.<br />";
							}


							//	create actual thumb

							$image->load('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/wp-content/uploads/stills/' . $current_post_id . '_still.jpg');
							$image->resize(96, 54);
							$destinationFileThumb = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_thumb.jpg';
							$image->save($destinationFileThumb, IMAGETYPE_JPEG, 100, 0777);

							$thumbFileName = 'images/thumbs/' . $current_post_id . '_thumb.jpg';
							$filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_thumb.jpg';

							if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $thumbFileName, S3::ACL_PUBLIC_READ)) {
								//echo "debug: We successfully uploaded your file.<br />";
							} else {
								//echo "debug: Something went wrong while uploading your file... sorry.<br />";
							}


							//	create wb-product-main-image

							$image->load('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/wp-content/uploads/stills/' . $current_post_id . '_still.jpg');
							$image->resize(544, 306);
							$destinationFileThumb = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_product_main_image.jpg';
							$image->save($destinationFileThumb, IMAGETYPE_JPEG, 100, 0777);

							$thumbFileName = 'images/thumbs/' . $current_post_id . '_product_main_image.jpg';
							$filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_product_main_image.jpg';

							if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $thumbFileName, S3::ACL_PUBLIC_READ)) {
								//echo "debug: We successfully uploaded your file.<br />";
							} else {
								//echo "debug: Something went wrong while uploading your file... sorry.<br />";
							}

							$updated_post_meta = update_post_meta($current_post_id, 'wb_ppv_prod_img_main', $wb_ent_options['amazons3info']['channelbucketurl'] . '/' . $thumbFileName);


							//	create wb-product-image-ppv

							$image->load('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/wp-content/uploads/stills/' . $current_post_id . '_still.jpg');
							$image->resize(453, 405);
							$destinationFileThumb = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_product_image_ppv.jpg';
							$image->save($destinationFileThumb, IMAGETYPE_JPEG, 100, 0777);

							$thumbFileName = 'images/thumbs/' . $current_post_id . '_product_image_ppv.jpg';
							$filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_product_image_ppv.jpg';

							if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $thumbFileName, S3::ACL_PUBLIC_READ)) {
								//echo "debug: We successfully uploaded your file.<br />";
							} else {
								//echo "debug: Something went wrong while uploading your file... sorry.<br />";
							}

							$updated_post_meta = update_post_meta($current_post_id, 'wb_ppv_prod_img_ppv', $wb_ent_options['amazons3info']['channelbucketurl'] . '/' . $thumbFileName);



							//	create wb-product-image-thumb

							$image->load('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/wp-content/uploads/stills/' . $current_post_id . '_still.jpg');
							$image->resize(288, 162);
							$destinationFileThumb = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_product_image_thumb.jpg';
							$image->save($destinationFileThumb, IMAGETYPE_JPEG, 100, 0777);

							$thumbFileName = 'images/thumbs/' . $current_post_id . '_product_image_thumb.jpg';
							$filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_product_image_thumb.jpg';

							if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $thumbFileName, S3::ACL_PUBLIC_READ)) {
								//echo "debug: We successfully uploaded your file.<br />";
							} else {
								//echo "debug: Something went wrong while uploading your file... sorry.<br />";
							}

							$updated_post_meta = update_post_meta($current_post_id, 'wb_ppv_prod_img_thumb', $wb_ent_options['amazons3info']['channelbucketurl'] . '/' . $thumbFileName);


							//	create wb-product-image-live

							$image->load('/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/wp-content/uploads/stills/' . $current_post_id . '_still.jpg');
							$image->resize(340, 220);
							$destinationFileThumb = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_product_image_live.jpg';
							$image->save($destinationFileThumb, IMAGETYPE_JPEG, 100, 0777);

							$thumbFileName = 'images/thumbs/' . $current_post_id . '_product_image_live.jpg';
							$filePath = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/images/thumbs/' . $current_post_id . '_product_image_live.jpg';

							if ($s3->putObjectFile($filePath, $wb_ent_options['amazons3info']['channelbucket'], $thumbFileName, S3::ACL_PUBLIC_READ)) {
								//echo "debug: We successfully uploaded your file.<br />";
							} else {
								//echo "debug: Something went wrong while uploading your file... sorry.<br />";
							}

							$updated_post_meta = update_post_meta($current_post_id, 'wb_ppv_prod_img_live', $wb_ent_options['amazons3info']['channelbucketurl'] . '/' . $thumbFileName);

					}
					else{
						echo "\r\n" . '<br />no $videoStill';
					}

				
					
					
					
					
					$wpdb->query( 
						$wpdb->prepare( 
							"
								INSERT INTO wb_media 
								(post_id, media_type, media_id, src, status, media_thumb, youtube) 
								VALUES 
								(%d, 'video', '%s', '', '2', '%s', '0');
							", $current_post_id, $current_video['id'], $videoStill )
					);					
				}
				
			}
			else{
				$debugArray[] = "\r\n".'<br />Post already exists';
			}
			
		}
		if( $script_action == 'displayOutput' ){
			echo implode(' 
				', $debugArray);
		}
	}
	
}
else{
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<title>Import Videos from Brightcove</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>		
	</head>
	<body>		
		<div class="row" style="max-width: 960px; margin: auto;">
			<h1>Import Videos from Brightcove</h1>
			<div class="col-xs-12 col-sm-6 col-md-8">
				<form method="POST" class="form-horizontal">
					<div class="form-group">
						<label for="videoLimit" class="col-sm-3 control-label">Video Limit</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="videoLimit" name="videoLimit">
						</div>						
					</div>	
					<div class="form-group">
						<label for="pageSize" class="col-sm-3 control-label">Page Size</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="pageSize" name="pageSize">
						</div>						
					</div>					
					<div class="form-group">
						<label for="pageNumber" class="col-sm-3 control-label">Page Number</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="pageNumber" name="pageNumber">
						</div>						
					</div>								
					<!--
					<div class="form-group">
						<label for="customField" class="col-sm-3 control-label">Custom Field</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="customField" name="customField">
						</div>						
					</div>					
					<div class="form-group">
						<label for="libraryCatId" class="col-sm-3 control-label">Library Category Id</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="libraryCatId" name="libraryCatId">
						</div>						
					</div>
					-->
					<div class="form-group">
						<label for="scriptAction" class="col-sm-3 control-label">Action</label>
						<div class="col-sm-9">							
							<select class="form-control" id="scriptAction" name="scriptAction">
								<option value="createPost">Create Post</option>
								<option value="displayOutput">Display Output</option>								
							</select>							
						</div>						
					</div>							
					<div class="form-group">
						<label for="postStatus" class="col-sm-3 control-label">Post Status</label>
						<div class="col-sm-9">
							<select class="form-control" id="postStatus" name="postStatus">
								<option value="draft">Draft</option>
								<option value="pending">Pending</option>
								<option value="publish">Published</option>
							</select>
						</div>						
					</div>					
					<div class="form-group">
						<label for="processQuery" class="col-sm-3 control-label"></label>
						<div class="col-sm-9">
							<input class="btn btn-primary" type="submit" name="processQuery" id="processQuery" value="Submit" />
						</div>
					</div>
				</form>				
			</div>
		</div>
	</body>
</html>
<?php
}
?>