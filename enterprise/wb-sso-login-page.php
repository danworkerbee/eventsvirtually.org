<?php
/**
 * filename: wb-sso-redirect-page.php
 * description: this template is used to display a login form when users try to 
 *    access Member Exclusive Content. When using this template, it is important
 *    to update the single.php so that when a post is available for Members only
 *    users will get redirected to this template to login.
 * author: Jullie Quijano
 * date created: 2018-01-03
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: SSO Login Page(Impak)
 */
global $wb_ent_options;

$workerbee_security_token = ''; //impak provided security token
$impak_client_host = ''; //url for the client example asca.impakadvance.com
$user_role_to_add = ''; //user role to create/use for the members example asca_members
$user_role_label = ''; //nice name for user roles example ASCA Members
$default_password = ''; //default password to use when creating new user
$redirectLink = '/?p='.$postId;

$error_message = array();
if( $_POST['login_button'] == 'Login'){
  if(trim($_POST['username']) == ''){
    $error_message[] = 'Please enter your username.';
  }
  else{
    $username = sanitize_text_field($_POST['username']);
  }
  
  if(trim($_POST['password']) == ''){
    $error_message[] = 'Please enter your password.';
  }
  else{
    $password = sanitize_text_field($_POST['password']);
  }
  
  if( count($error_message) <= 0 ){
    //authenticate user
    $curlstring = 'https://'.$impak_client_host.'/higher_logic_service/service.asmx/AuthenticateUser';
    $fields = array(
      'strSecurityKey' => $workerbee_security_token,
      'strUsername' => $username,   
      'strPassword' => $password, 
    );
    foreach ($fields as $key => $value) {
      $fields_string .= $key . '=' . $value . '&';
    }
    rtrim($fields_string, '&');

    $ch = curl_init();
    $timeout = 0; // set to zero for no timeout
    curl_setopt($ch, CURLOPT_URL, $curlstring);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    $user_info_values = curl_exec($ch);
    curl_close($ch);

    $xml_parser = xml_parser_create();
    xml_parse_into_struct($xml_parser, $user_info_values, $xml_member_info_parse_values, $index);
    xml_parser_free($xml_parser);

    //get required values
    $user_email_key = array_search('EMAIL', array_column($xml_member_info_parse_values, 'tag'));        
    $user_email = $xml_member_info_parse_values[$user_email_key]['value'];

    $user_first_name_key = array_search('FIRST_NAME', array_column($xml_member_info_parse_values, 'tag'));        
    $user_first_name = $xml_member_info_parse_values[$user_first_name_key]['value'];

    $user_last_name_key = array_search('LAST_NAME', array_column($xml_member_info_parse_values, 'tag'));        
    $user_last_name = $xml_member_info_parse_values[$user_last_name_key]['value'];


    /*
     * Other search
     * ADDRESS_LINE_1
     * ADDRESS_CITY
     * ADDRESS_STATE_CODE
     * ADDRESS_ZIP
     * NUMBER
     */

    if (trim($user_email) != '') {
      echo "\r\n".'email not blank';

      if (!function_exists('get_role')) {
        echo "\r\n".'function get_role does not exist';
      } else {
        echo "\r\n".'function get_role exists';
      }

      $getRoleValue = get_role($user_role_to_add);
      echo "\r\n".'$getRoleValue is ' . print_r($getRoleValue, true);

      if (!is_object($getRoleValue)) {
        echo "\r\n".'create new role';
        global $wp_roles;
        if (!isset($wp_roles)) {
          $wp_roles = new WP_Roles();
        }

        $custRole = $wp_roles->get_role('subscriber');
        echo "\r\n".'$custRole is ' . print_r($custRole, true);

        $wp_roles->add_role($user_role_to_add, $user_role_label, $custRole->capabilities);
      }

      $emailAddress = trim($user_email);

      $emailExists = email_exists($user_email);
      echo "\r\n".'$emailExists is ' . $emailExists;
      $usernameExists = username_exists($emailAddress);
      echo "\r\n".'$usernameExists is ' . $usernameExists;

      //check if user exists
      if ($emailExists || $usernameExists) {

        $userId = (is_numeric($emailExists)) ? $emailExists : ( (is_numeric($emailAddress)) ? $emailAddress : 0 );
        $user_info = get_userdata($userId);
        echo "\r\n".'$user_info is ' . print_r($user_info, true);

        wp_set_auth_cookie($userId, true);
      }
      else {
        $createNewUser = true;
        $userLoggedIn = false;
        echo "\r\n".'user does not exist';
      }

      if ($createNewUser) {
        echo "\r\n".'creating new user';

        $userdata = array(
            'user_login' => $emailAddress,
            'user_email' => $emailAddress,
            'first_name' => $user_first_name,
            'last_name' => $user_last_name,
            'role' => $user_role_to_add,
            'user_pass' => $default_password
        );
        echo "\r\n".'$userdata is ' . print_r($userdata, true);

        $user_id = wp_insert_user($userdata);
        echo "\r\n".'$user_id is ' . print_r($user_id, true);

        if (is_wp_error($user_id)) {
          $displayMessage = true;
          $messageText = 'Please enter a valid email address.';
          $userLoggedIn = false;
        } else {
          $creds = array();
          $creds['user_login'] = $emailAddress;
          $creds['user_password'] = $default_password;
          $creds['remember'] = true;
          echo "\r\n".'$creds last is ' . print_r($creds, true);

          $user = wp_signon($creds, true);
          echo "\r\n".'$user is ' . print_r($user, true);

          if (is_wp_error($user)) {
            $createNewUser = true;
            $userLoggedIn = false;
            $displayMessage = true;
            $messageText = 'Please enter a valid email address.';
            echo "\r\n".'wp_error ';
          } else {
            $userLoggedIn = true;
            echo "\r\n".'no error ';
          }
        }
      }

      if (is_user_logged_in()) {
        echo "\r\n".'user_is logged in ';
        if (trim($redirectLink) != '') {
        header('location: ' . $redirectLink);
        } else {
        header('location: /');
        }
      }
      else{
        echo "\r\n".'user_is NOT logged in ';
        if (trim($redirectLink) != '') {
        header('location: ' . $redirectLink);
        } else {
        header('location: /');
        }
      }
      exit;
    }
  }
}

get_header();
if (have_posts()) : while (have_posts()) : the_post();
?>
<div id="wb_ent_content" class="clearfix row-fluid">
    <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
        <div id="viewing-tips">
            <h1>Member Exclusive Content</h1>            
            <?php
            if( count($error_message) > 0 ){
            ?>
            <div class="alert alert-danger">
              <ul>
              <?php
              foreach($error_message as $current_message){
                echo '<li>'.$current_message.'</li>';
              }
              ?>
              </ul>
            </div>
            <?php
            }
            ?>
            <p>Please login to continue.</p>
            <form method="POST">   
              <div class="span4">              
                <label for="username">Username</label>
                <input type="text" name="username" required="required" />
              </div>
              <div class="span4">  
                <label for="password">Password</label>
                <input type="password" name="password" required="required" />
              </div>
              <div class="span4">  
                <label for="login_button">&nbsp;</label>
                <input type="submit" name="login_button" value="Login" class="btn"/>
              </div>
            </form>
            <?php
        endwhile;
    else:
        ?>
        <p>&nbsp;</p>
    <?php endif; ?>

        </div>
    </div>
<?php
get_sidebar();
?>
</div>  <!--  wb_ent_content end -->
<?php 
get_footer();
?>