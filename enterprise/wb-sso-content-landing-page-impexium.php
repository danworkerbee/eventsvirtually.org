<?php
/**
 * filename: wb-sso-content-landing-page-impexium.php
 * description: this template will be used to let the users know that the content
 *    is only available for members. It will show the video still and a link/banner
 *    to direct the users to login. To get to this page, the single.php should be 
 *    modified so that Members Only content will be redirected here if users are
 *    not logged in or not members
 * author: Jullie Quijano
 * date created: 2018-06-08
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: SSO Members Only Landing Page (Impexium)
 */


/*
 * HOW TO USE:
 * 1. Update $impexium_url with the impexium url for the client. Include http 
 *    but no trailing slash. Eg: 'http://asca.mpxstage.com/account'
 * 2. Update $sso_redirect_page value. This will be the permalink of the page 
 *    using the wb-sso-redirect-page-impexium.php template
 *    eg. 'https://videos.schoolcounselor.org/sso-redirect-page-impexium'
 * 3. Update styling as necessary
 */
global $postId;

$wb_home_url 		= get_site_url();
$impexium_url 		= '';
$sso_redirect_page 	= $wb_home_url . '/redirect?pid=' . $postId;  // the [/redirect] is the page for wb-sso-redirect-page-impexium.php
$sso_login_url 		= $impexium_url . '/account/login.aspx?ReturnUrl=';
$login_link 		= $sso_login_url.$sso_redirect_page;

global $postId;
get_header();
if ($wb_ent_options['sharetype'] == 'floating' && $wb_ent_options['hasshare'] && function_exists(sharebar)) {
	if ( !in_category( wb_how_to_videos_category ) ){
    	sharebar();
	}
}
?>
<style type="text/css">
#wb_ent_content #wb_ent_main #sso-login-button {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #5bb75b;
  background-image: -moz-linear-gradient(top, #62c462, #51a351);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#62c462), to(#51a351));
  background-image: -webkit-linear-gradient(top, #62c462, #51a351);
  background-image: -o-linear-gradient(top, #62c462, #51a351);
  background-image: linear-gradient(to bottom, #62c462, #51a351);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462', endColorstr='#ff51a351', GradientType=0);
  border-color: #51a351 #51a351 #387038;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  *background-color: #51a351;
  /* Darken IE7 buttons by default so they stand out more given they won't have borders */

  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
  text-align: center;
  display: block;
  margin: auto;
  font-size: 24px;
  padding: 20px;
}
#wb_ent_content #wb_ent_main #sso-login-button:hover, #wb_ent_content #wb_ent_main #sso-login-button:focus {
  color: #ffffff;
  background-position: 0 0;
}
#sso-login-link{
  color: #2a63a1;
  text-decoration: none;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 16px;
  font-weight: bold;
  text-decoration: underline;
}
</style>
<script>
	jQuery(function($wb){
		$wb("#hiddenP").hide();
		$wb("#contentmore").click(function(){
			$wb("#video-desc-inner").hide();
			$wb("#hiddenP").show();
		});
		$wb("#contentless").click(function(){
			$wb("#video-desc-inner").show();
			$wb("#hiddenP").hide();
		});
	});
</script>
<div id="wb_ent_content" class="clearfix row-fluid">

    <div id="wb_ent_main" class="span8 clearfix" role="main">

        <div id="video-post-page">
            <?php
            if (have_posts()) : while (have_posts()) : the_post();
            if(post_password_required( $postId ) && !is_user_logged_in() && !current_user_can('upload_files')){  ?>
              <h2><?php echo $video['title']; ?></h2>
              <?php the_content(); 
            }else { 
                  $shortcodeCount = 0;
                  $videoDesc = get_the_content();
                  if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)\/\]/', $videoDesc, $match) >= 1) {
                     $shortcodeCount = count($match[0]);
                  }              
                    ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
                        <section class="post_content clearfix" itemprop="articleBody">
                            <!-- Video Post -->
                            <div class="row-fluid" id="vid-post">
                                <ul class="">                                    																		               
                                    <li class="<?php
                                    if ($wb_ent_options['haspostlogo']) {
                                        echo 'span10';
                                    } else {
                                        echo 'span12';
                                    }
                                    ?> desc-position">
                                        <h2><?php echo $video['title']; ?></h2>
                                        <div id="video-desc-inner">
			                            	<?php 
			                                    $myContent 			= $video['desc'];
			                                    $wb_custom_length 		= 100;
			                                    $target_word 		= 50;
			                                    $word_count 		= str_word_count($myContent);
			                                    $wb_pieces 			= explode(" ", $myContent);
			                                    $wb_first_part 		= implode(" ", array_splice($wb_pieces, 0, $target_word));
			                                    //echo "<!-- word count "  .$word_count. "-->";
			                                    //if(!empty($myContent) && $wb_ent_options['videolistinfo']['desclimit'] > 0 && strlen($myContent) > $wb_custom_length ){
			                                    if( !empty($myContent) && $wb_ent_options['videolistinfo']['desclimit'] > 0 && $word_count > $target_word ){
			                                    	//$initialContent = substr($myContent, 0, $wb_ent_options['videolistinfo']['desclimit']);
			                                        $initialContent = substr($myContent, 0, $wb_custom_length);
			                                        $initialContent = $wb_first_part. "... <span id=\"contentmore\">more</span>";
			                                    }else{
			                                       	$initialContent = $myContent;
			                                    }
			                                    if(!empty($video['desc'])){
			                                    	//echo str_replace("&nbsp;","<br />",$video['desc']); //echo $video['desc'];
			                                        //echo strlen($myContent) . "limit is : " . $wb_ent_options['videolistinfo']['desclimit'];
			                                     	echo  $initialContent ;
			                                     }else{
			                                     	//if (strlen($wbCurrentPost->post_content) > 100){
			                                        //echo str_replace("&nbsp;","<br />",$wbCurrentPost->post_content);  //$wbCurrentPost->post_content;
			                                       	echo nl2br( $initialContent );
			                                        //}
			                                     }
			                             	?>                
				                       	 </div>
                                    	 <div id="hiddenP" style="display: none;">
	                                    	<?php 	
	                                    		echo nl2br( $myContent . " <span id=\"contentless\"> less</span>" );
	                                    	?>
	                                    </div>       

                                        <?php
										/*
                                        if(strlen($video['desc']) > $wb_ent_options['videolistinfo']['desclimit']){ 
                                        //more_less('video-desc-inner', '', '1', '1');
                                        }
										*/
                                        ?>
										<p>
										  <a href="<?php echo $login_link; ?>" id="sso-login-link">To watch this video, click here to log in to your <?php echo $wb_ent_options[channelname]; ?> membership account.</a>
										</p>
                                    </li>
																		<li class="span12">
                                        <div class="">
                                          <a href="<?php echo $login_link; ?>"><img class="" src="<?php echo $video['videoStill']; ?>" /></a>
                                        </div>
                                    </li>
                                   
                                </ul>
                            </div>
                            <!-- End of Video Post -->
                        </section> <!-- end article section -->
                        <footer>
                          <!--
                          <div class="video-wide-banner">
                             <script type="text/javascript">
                                 //ga('pageTracker1.send', 'event', 'Specialty Channel', 'Banner View', '70QptYZXTja9KnmBVxuG_Sargento-468x60.jpg');ga('pageTracker2.send', 'event', 'Specialty Channel', 'Banner View', '70QptYZXTja9KnmBVxuG_Sargento-468x60.jpg');
                             </script>                              
                                       <a onclick="/*ga('pageTracker1.send', 'event', 'Specialty Channel', 'Banner Click', '70QptYZXTja9KnmBVxuG_Sargento-468x60.jpg');ga('pageTracker2.send', 'event', 'Specialty Channel', 'Banner Click', '70QptYZXTja9KnmBVxuG_Sargento-468x60.jpg');*/" href="https://www.sargento.com/" target="_blank"><img src="https://s3.amazonaws.com/wb.banner.manager/70QptYZXTja9KnmBVxuG_Sargento-468x60.jpg" alt="wb.banner.manager" name="" border="0"></a>
                          </div>
                          -->
                          <button id="sso-login-button" class="btn" style="" onclick="location.href='<?php echo $login_link; ?>';">You need to log in to <?php echo $wb_ent_options[channelname]; ?> to watch this video.</button>
                        </footer> <!-- end article footer -->

                    </article> <!-- end article -->
                    <?php
                    //count video views
                    $permalink = $video['postLink'];
                    wb_count_page_views($permalink, $postId);                   
                                                           
                    /*moving this before tags 7-08
										if($getWide || $siteWide[status] == 'active'){
											include ( get_template_directory() . '/includes/widgets/adsWideCustom.php');
                    } */
            }//end of else if password protected
                    ?>

                <?php endwhile; ?>			

            <?php else : ?>

                <article id="post-not-found">
                    <header>
                        <h1><?php _e("Not Found", "enterprise"); ?></h1>
                    </header>
                    <section class="post_content">
                        <p><?php _e("Sorry, but the requested resource was not found on this site.", "enterprise"); ?></p>
                    </section>
                    <footer>
                    </footer>
                </article>

<?php endif; ?>
        </div> <!-- end #video-post-page -->
    </div> <!-- end #main -->


<?php get_sidebar(); // sidebar 1  ?>

</div> <!-- end content -->

<?php
get_footer();
?>
