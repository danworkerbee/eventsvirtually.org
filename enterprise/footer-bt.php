<?php
/**
 * filename: footer.php
 * description: this will be displayed on all pages
 * author: Jullie Quijano
 * date created: 2014-03-25
 * 
 */
global $wb_ent_options, $current_lang, $catlibrary, $curlang;

 ?>
</div> <!-- wb_ent_content end -->
<div class="row-fluid" id="footer_main">
   <?php
    $mobile = mobile_device_detect();
    if (!is_array($mobile)){ 
   ?>
    <footer role="contentinfo" class="span12" style="margin: 0 auto; float:none;"> 
        <?php
             
        if( $wb_ent_options['customstyle']['footerhtml'.$curlang] && trim($wb_ent_options['customstyle']['footerhtml'.$curlang]) != '' ){
            //echo html_entity_decode( $wb_ent_options['customstyle']['footerhtml'.$curlang] );
            include "wb-client-footer.php";
        }
        else{
        ?>
        <div id="copyright" style="margin: 0 auto 20px; float:none; text-align: center;">
            &copy; Copyright <?php echo $wb_ent_options['channelname']; ?> <?php echo date("Y"); ?> | All rights reserved | <a href="<?php echo get_site_url(); ?>" title="<?php echo $wb_ent_options['channelname']; ?>" target="_self"><?php echo $wb_ent_options['channelemail']; ?></a>
            <div style="margin:4px 0 0 0;">
                <a href="<?php echo $wb_ent_options['clientprivacyurl']; ?>">Privacy Policy</a> - <a href="<?php echo $wb_ent_options['clienttermsurl']; ?>">Terms and Conditions</a>
            </div>
        </div>        
        <?php
        }        
        ?>
    </footer> <!-- end footer -->
    <?php } ?>
</div>



<!--[if lt IE 7 ]>
        <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
        <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
<![endif]-->
<?php
wp_footer();
if (!$wb_ent_options['videolistinfo']['usestatic'] && $wb_ent_options['hasarchivewidget']) {
    ?>
            <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/resources/postInfo.json?v=<?php echo time(); ?>"></script>
    <?php
}
?>
</body>

</html>