<?php
/**
 * filename: sidebar.php
 * description: will display the different widgets on the sidebar depending on the theme options
 * author: Jullie Quijano
 * date created: 2014-03-25
 * 
 */
$wb_ent_options = get_option('wb_ent_options');
$current_page 	= getCurrentAPFPage();
?>

<div id="wb_ent_sidebar1" class="fluid-sidebar sidebar span4" role="complementary" style="border: 0px solid blue;">
    <!-- START OF #side-nav -->				
    <div id="side-nav" class="row-fluid" style="border: 0px solid red;"> 
    <!-- Start of new sidebar with order -->
    
    <?php //if(!empty($wb_ent_options['sidearray'])) {
    	if($wb_ent_options['sidebarorder']['customorder']){
     		for ($x=0; $x<10; $x++) {
    			$sidebaroder_arr[] = $wb_ent_options[sidebarorder][$x];    
    		}
    	}else{
     		$sidebaroder_arr = array('about', 'vidlibrary', 'vidchannel', 'audio library', 'subscribe', 'ads', 'archwidget', 'addtoside', 'vidtopiclang', 'keyword', 'poll', 'download' );
    	}
        
    	foreach ( $sidebaroder_arr as $key => $value ) {
    		switch ($value) {
        		case 'select':
        			break;
        		case 'about' : 
	         		if ($wb_ent_options['hasabout']) {          
	        			?>
	        			<!-- START of ABOUT -->
	                	<div id="about" class="visible-desktop side-bar-width marginleft">  
	                    	<h4 class="subs-h"><?php printf(__('About %s', 'enterprise'), $wb_ent_options['channelname']); ?></h4>
	                    	<div id="aboutText"  style="font-size: 11px;"><?php printf(__('%s', 'enterprise'), $wb_ent_options['channeldesc']); ?></div>		  		
	                	</div>
	                	<!-- END of About -->
	                	<div class="spacing"></div>  
	        			<?php
	        		}       
	            	break ;
        		case 'vidlibrary' :
            		if ($wb_ent_options['hascatlist']['enable']) {            
            			if ($wb_ent_options['catlisttype']['options'] == 'collapse') {
                			include ( get_template_directory() . '/includes/widgets/vid_topic_collapse.php');
            			}elseif($wb_ent_options['catlisttype']['options'] == 'split'){
              				include ( get_template_directory() . '/includes/widgets/vid_topic_split.php');
            			}else{
                			include ( get_template_directory() . '/includes/widgets/vid_topic.php');
            			}
            		?>                               
            			<!-- END OF Category List -->
            			<div class="spacing"></div>
            		<?php
        			}
            		break ;
        		case 'subscribe' :
            		if ( $wb_ent_options['hassubscribe']['enable'] ) {
            	    	if ($wb_ent_options['subscribetype'] == 'collapse') {
                			include ( get_template_directory() . '/includes/widgets/subscribeCollapse.php');
            			} elseif ( $wb_ent_options['subscribetype']  == 'link' ){
                			include ( get_template_directory() . '/includes/widgets/subscribe_link.php');
            			} else {
                			include ( get_template_directory() . '/includes/widgets/subscribe.php');
            			}
            		?>        
			            <!-- END OF Subscribe -->
			            <div class="clear"></div>
		            <?php
        			}
            		break ;
        		case 'ads' :
        			include ( get_template_directory() . '/includes/widgets/adsSidebarCustom.php');        
        			break;
        		case 'archwidget' :
        			if ($wb_ent_options['hasarchivewidget']) {
            			include ( get_template_directory() . '/includes/widgets/archiveWidget.php');
            		?>
            			<!-- END OF Archive Widget -->
            			<div class="clear"></div>
            		<?php
        			}
        			break;
        		case 'vidtopiclang' :
        			if ($wb_ent_options['vidtopiclang']['enable']) { 
         				include ( get_template_directory() . '/includes/widgets/vid_topic_lang.php');
         			}
         			?>
         				<div class="spacing"></div>
         			<?php 
         			break;
        		case 'vidchannel' :
        			if ($wb_ent_options['channels']['librarybox']) {
         				include ( get_template_directory() . '/includes/widgets/vid_by_channel.php');
        			}
        			?>
         				<div class="spacing"></div>
        			<?php 
        			break; 
         		case 'poll' :
         			if ($wb_ent_options['haspoll']) {
           				if (function_exists('get_poll') && !in_pollarchive()) {
                		?>
		                <style>
		                    .wp-polls{
		                        padding: 0 !important;               
		                    }
		                    .wp-polls .wp-polls-form .wp-polls-ans{
		                        padding: 0 !important;
		                    }     
		                    .wp-polls .wp-polls-form .wp-polls-ans .wp-polls-ul li{
		                        margin-bottom: 10px;
		                    }
		                    .wp-polls .wp-polls-form .wp-polls-ans p{
		                        margin-left: 0;
		                    }
		                    .wp-polls .wp-polls-ans .wp-polls-ul li{
		                        max-width: 220px;
		                    }
		                </style>
		                <div class="marginleft visible-desktop btnwb" id="polls-widget" style="cursor: auto;">
		                    <div>             
		                        <li>
		                            <h2><?php printf(__('%s Poll', 'enterprise'), $wb_ent_options['channelname']); ?></h2>
		                            <?php get_poll(); ?> 
		                        </li>                
		                        <?php
		                        //display_polls_archive_link();
		                        ?>
		                    </div>
		                </div>
                		<!-- END OF Poll -->
                		<div class="clear"></div>
                		<?php
            			}
        			}
         			break;
         		case 'keyword' :
         			if ($wb_ent_options['haskeywordcloud']) {
            			include ( get_template_directory() . '/includes/widgets/keyword_cloud.php');
            			?>
            			<!-- END OF Keyword Cloud -->
            			<div class="clear"></div>
            			<?php
        			} 
         			break;
         		case 'addtoside' :
         			if(trim($wb_ent_options['sidebaradd']) != ""){
        			?>
        			<div class="row-fluid">              
              			<ul class="thumbnails">
                			<li class="span sidebar-etc">
                			<?php  echo html_entity_decode($wb_ent_options['sidebaradd']);?>
              				</li>
              			</ul>
        			</div>
        
        			<?php        
        			}
 	        		break;
         		case 'download' :
         			if ($wb_ent_options['hasdownload']) {
            			include ( get_template_directory() . '/includes/widgets/download.php');
            			?>
            			<!-- END OF Download -->
            			<div class="clear"></div>
            			<?php
        			}
         			break;
         		case 'clienticon':
           			if($wb_ent_options['hasclienticon']['enable'] && $wb_ent_options['hasclienticon']['image']!= ''){
	           			?>
	              		<div class="" style="margin: 0 auto; max-width:300px;">
	              			<a href="/"><img src="<?php echo $wb_ent_options['hasclienticon']['image']; ?>" /></a>
	              		</div>
	              		<div class="spacing"></div>
	        			<?php 
           			} 
         			break;
       			case 'audio library':
					include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
					if ( is_plugin_active( 'workerbee-audio-posts/wb-audio-post.php' ) ){
						include ( get_template_directory() . '/includes/widgets/wb-audio-library.php');
					?>
						<!-- END OF Audio Library List -->
						<div class="spacing"></div>  
					<?php 
					}
					break;
     			} // end of switch
    		} // end of foreach
		?>
    	</div><!-- END OF #side-nav -->
	</div>
</div>