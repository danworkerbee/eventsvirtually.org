<?php
/**
 * filename: wb-sso-members-landing-page.php
 * description: this template will be used to let the users know that the content
 *    is only available for members. It will show the video still and a link/banner
 *    to direct the users to login.
 * author: Jullie Quijano
 * date created: 2018-01-03
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: SSO Members Only Landing Page
 */

$sso_login_url = ''; //this should include the urlendcoded address for the redirect page; example https://www.schoolcounselor.org/login.aspx?ReturnUrl=
$sso_redirect_page = ''; //this is the page created using the template wb-sso-redirect-page.php
$login_link = $sso_login_url.urlencode($sso_redirect_page.'?p=http://'.$wb_ent_options['channeldomain'].'/'.$wp_query->post->post_name);

global $postId;
get_header();
if ($wb_ent_options['sharetype'] == 'floating' && $wb_ent_options['hasshare'] && function_exists(sharebar)) {
	if ( !in_category( wb_how_to_videos_category ) ){
    	sharebar();
	}
}
?>
<script>
	jQuery(function($wb){
		$wb("#hiddenP").hide();
		$wb("#contentmore").click(function(){
			$wb("#video-desc-inner").hide();
			$wb("#hiddenP").show();
		});
		$wb("#contentless").click(function(){
			$wb("#video-desc-inner").show();
			$wb("#hiddenP").hide();
		});
	});
</script>
<div id="wb_ent_content" class="clearfix row-fluid">

    <div id="wb_ent_main" class="span8 clearfix" role="main">

        <div id="video-post-page">
            <?php
            if (have_posts()) : while (have_posts()) : the_post();
            if(post_password_required( $postId ) && !is_user_logged_in() && !current_user_can('upload_files')){  ?>
              <h2><?php echo $video['title']; ?></h2>
              <?php the_content(); 
            }else { 
                  $shortcodeCount = 0;
                  $videoDesc = get_the_content();
                  if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)\/\]/', $videoDesc, $match) >= 1) {
                     $shortcodeCount = count($match[0]);
                  }              
                    ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
                        <section class="post_content clearfix" itemprop="articleBody">
                            <!-- Video Post -->
                            <div class="row-fluid" id="vid-post">
                                <ul class="">                                    																		               
                                    <li class="<?php
                                    if ($wb_ent_options['haspostlogo']) {
                                        echo 'span10';
                                    } else {
                                        echo 'span12';
                                    }
                                    ?> desc-position">
                                        <h2><?php echo $video['title']; ?></h2>
                                        <div id="video-desc-inner">
			                            	<?php 
			                                	$myContent 			= $video['desc'];
			                                    $wb_custom_length 	= 100;
			                                    $target_word 		= 50;
			                                    $word_count 		= str_word_count($myContent);
			                                    $wb_pieces 			= explode(" ", $myContent);
			                                    $wb_first_part 		= implode(" ", array_splice($wb_pieces, 0, $target_word));
			                                    //echo "<!-- word count "  .$word_count. "-->";
			                                    //if(!empty($myContent) && $wb_ent_options['videolistinfo']['desclimit'] > 0 && strlen($myContent) > $wb_custom_length ){
			                                    if( !empty($myContent) && $wb_ent_options['videolistinfo']['desclimit'] > 0 && $word_count > $target_word ){
			                                    	//$initialContent = substr($myContent, 0, $wb_ent_options['videolistinfo']['desclimit']);
			                                        $initialContent = substr($myContent, 0, $wb_custom_length);
			                                        $initialContent = $wb_first_part. "... <span id=\"contentmore\">more</span>";
			                                    }else{
			                                       	$initialContent = $myContent;
			                                    }
			                                    if(!empty($video['desc'])){
			                                    	//echo str_replace("&nbsp;","<br />",$video['desc']); //echo $video['desc'];
			                                        //echo strlen($myContent) . "limit is : " . $wb_ent_options['videolistinfo']['desclimit'];
			                                     	echo  $initialContent ;
			                                     }else{
			                                     	//if (strlen($wbCurrentPost->post_content) > 100){
			                                        //echo str_replace("&nbsp;","<br />",$wbCurrentPost->post_content);  //$wbCurrentPost->post_content;
			                                       	echo nl2br( $initialContent );
			                                        //}
			                                     }
			                             	?>                
				                       	 </div>
                                    	 <div id="hiddenP" style="display: none;">
	                                    	<?php 	
	                                    		echo nl2br( $myContent . " <span id=\"contentless\"> less</span>" );
	                                    	?>
	                                    </div>       

                                        <?php
										/*
                                        if(strlen($video['desc']) > $wb_ent_options['videolistinfo']['desclimit']){ 
                                        //more_less('video-desc-inner', '', '1', '1');
                                        }
										*/
                                        ?>

                                    </li>
																		<li class="span12">
                                        <div class="">
                                          <img class="" src="<?php echo $video['videoStill']; ?>" />
                                        </div>
                                    </li>
                                   
                                </ul>
                            </div>
                            <!-- End of Video Post -->
                        </section> <!-- end article section -->
                        <footer>
                          <!--
                          <div class="video-wide-banner">
                             <script type="text/javascript">
                                 //ga('pageTracker1.send', 'event', 'Specialty Channel', 'Banner View', '70QptYZXTja9KnmBVxuG_Sargento-468x60.jpg');ga('pageTracker2.send', 'event', 'Specialty Channel', 'Banner View', '70QptYZXTja9KnmBVxuG_Sargento-468x60.jpg');
                             </script>                              
                                       <a onclick="/*ga('pageTracker1.send', 'event', 'Specialty Channel', 'Banner Click', '70QptYZXTja9KnmBVxuG_Sargento-468x60.jpg');ga('pageTracker2.send', 'event', 'Specialty Channel', 'Banner Click', '70QptYZXTja9KnmBVxuG_Sargento-468x60.jpg');*/" href="https://www.sargento.com/" target="_blank"><img src="https://s3.amazonaws.com/wb.banner.manager/70QptYZXTja9KnmBVxuG_Sargento-468x60.jpg" alt="wb.banner.manager" name="" border="0"></a>
                          </div>
                          -->
                          <button class="btn btn-large btn-primary" style="text-align: center;display: block;margin: auto;" onclick="location.href='<?php echo $login_link; ?>';">You need to sign in to ASCA to watch this video.</button>
                        </footer> <!-- end article footer -->

                    </article> <!-- end article -->
                    <?php
                    //count video views
                    $permalink = $video['postLink'];
                    wb_count_page_views($permalink, $postId);                   
                                                           
                    /*moving this before tags 7-08
										if($getWide || $siteWide[status] == 'active'){
											include ( get_template_directory() . '/includes/widgets/adsWideCustom.php');
                    } */
            }//end of else if password protected
                    ?>

                <?php endwhile; ?>			

            <?php else : ?>

                <article id="post-not-found">
                    <header>
                        <h1><?php _e("Not Found", "enterprise"); ?></h1>
                    </header>
                    <section class="post_content">
                        <p><?php _e("Sorry, but the requested resource was not found on this site.", "enterprise"); ?></p>
                    </section>
                    <footer>
                    </footer>
                </article>

<?php endif; ?>
        </div> <!-- end #video-post-page -->
    </div> <!-- end #main -->


<?php get_sidebar(); // sidebar 1  ?>

</div> <!-- end content -->

<?php
get_footer();
?>