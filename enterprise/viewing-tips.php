<?php
/**
 * filename: page.php
 * description: this will be the default template to be used for the theme
 * author: Jullie Quijano
 * date created: 2014-03-25
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: Viewing Tips
 */
global $wb_ent_options;
get_header();
if (have_posts()) : while (have_posts()) : the_post();
        ?>
        <div id="wb_ent_content" class="clearfix row-fluid">
            <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
                <div id="viewing-tips">
                    <h1 class="pagetitle"><?php printf(__('%s Viewing Tips', 'enterprise' ),$wb_ent_options['channelname']); ?> <?php //the_title(); ?></h1>

                    <div id="viewingTips">

                        
                        <p class="lineheight15em"><?php printf(__('%s uses HTML5 video player to ensure a high quality viewing experience. If you have a slower connection, we recommend that you hit play to start the player and watch the progress bar in the player; it shows the download progress. The player will download the streaming video into your computer\'s memory, however this could take some time depending on the speed of your connection, so please be patient. Try playing the video by hitting the play button, if it stops playing, please pause and wait for more video to download.', 'enterprise'), $wb_ent_options['channelname']);?></p>
                        <p class="lineheight15em"><?php _e('Please note: any other downloads or browsing while watching the cast could affect the video player performance. If you are downloading movies or music, consider pausing those downloads while you watch the video.', 'enterprise') ?></p>

                        <ul class="lineheight15em">
                            <li><?= _e('The video player will load and play automatically. If it takes longer than 10 seconds for the player to appear, you may retry refresh or you may have an Internet connection problem.', 'enterprise') ?></li>
                            <li><?= _e('Please adjust the volume on the Player volume control to your desired level.', 'enterprise') ?></li>
                            <li><?= _e('Please ensure the angle of your monitor is tilted correctly for ideal brightness and contrast.', 'enterprise') ?></li>
                            <li><?= _e('By clicking on a banner next to the web cast player, you will be taken directly to the relevant website.', 'enterprise') ?></li>
                            <li><?= _e('We recommend the following minimum system configuration:', 'enterprise') ?>
                                <ul>
                                    <li><?= _e('512kbps or higher broadband connection', 'enterprise') ?></li>
                                    <li><?= _e('1GHz or faster processor', 'enterprise') ?></li>
                                    <li><?= _e('512MB of RAM', 'enterprise') ?></li>
                                </ul>
                            </li>
                            <li><?= _e('On Internet Explorer, please make sure the "Compatibility Mode" is turned off or you might see a jumbled screen. To check, Go to Tools > Compatibility View Settings > at the bottom checkbox "Display all websites in Compatibility View" should be un-checked > click OK and then reload the page. See screenshot below.', 'enterprise') ?>
                            </li>
                        </ul>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/compatibilityViewSettings<?php echo $curlang; ?>.png" title="Compatibility View Settings" style="border: 6px solid #E2E2E2; margin-left: 30px; margin-bottom: 5px;" />


                        <h2><?= _e('Try Clearing the Cache to Increase Speed / Solve a Problem', 'enterprise') ?></h2>
                        <h4><?= _e('Windows', 'enterprise') ?></h4>
                        <h5><?= _e('Internet Explorer 10 or higher', 'enterprise') ?></h5>
                        <ol>
                            <li><?= _e('From the Tools Menu, select the Safety menu, click Delete Browsing History... .', 'enterprise') ?></li>
                            <li><?= _e('Check Temporary Internet files and History, and then click Delete.', 'enterprise') ?></li>
                        </ol>
                        <h5><?= _e('Chrome', 'enterprise') ?></h5>
                        <ol>
                            <li><?= _e('Click the wrench icon on the browser toolbar.', 'enterprise') ?></li>
                            <li><?= _e('Select Tools.', 'enterprise') ?></li>
                            <li><?= _e('Select Clear browsing data.', 'enterprise') ?></li>
                            <li><?= _e('In the dialog that appears, select the checkboxes for the types of information that you want to remove.', 'enterprise') ?></li>
                            <li><?= _e('Use the menu at the top to select the amount of data that you want to delete. Select beginning of time to delete everything.', 'enterprise') ?></li>
                            <li><?= _e('Click Clear browsing data.', 'enterprise') ?></li>
                        </ol>
                        <h5><?= _e('Firefox 11 or higher', 'enterprise') ?></h5>
                        <ol>
                            <li><?= _e('At the top of the Firefox window, click on the Firefox button (Tools menu in Windows XP) and then click Options.', 'enterprise') ?></li>
                            <li><?= _e('Select the Advanced panel.', 'enterprise') ?></li>
                            <li><?= _e('Click on the <strong>Network</strong> tab.', 'enterprise') ?></li>
                            <li><?= _e('In the <strong>Offline Storage</strong> section, click Clear Now.', 'enterprise') ?></li>
                            <li><?= _e('Click OK to close the Options window.', 'enterprise') ?></li>
                        </ol>
                        &nbsp;
                        <h4><?= _e('Mac OS X', 'enterprise') ?></h4>
                        <h5><?= _e('Safari', 'enterprise') ?></h5>
                        <ol class="lineheight15em">
                            <li><?= _e('From the Safari menu, select Empty Cache... .', 'enterprise') ?></li>
                            <li><?= _e('When prompted, click Empty to confirm that you want to empty the cache.', 'enterprise') ?></li>
                        </ol>
                        <h5><?= _e('Firefox 11 or higher', 'enterprise') ?></h5>
                        <ol>
                            <li><?= _e('On the menu bar, click on the Firefox menu and select Preferences....', 'enterprise') ?></li>
                            <li><?= _e('Select the Advanced panel.', 'enterprise') ?></li>
                            <li><?= _e('Click on the <strong>Network</strong> tab.', 'enterprise') ?></li>
                            <li><?= _e('In the <strong>Offline Storage</strong> section, click Clear Now.', 'enterprise') ?></li>
                            <li><?= _e('Close the Preferences window.', 'enterprise') ?></li>
                        </ol>
                        <h5><?= _e('Chrome', 'enterprise') ?></h5>
                        <ol>
                            <li><?= _e('Click the wrench icon on the browser toolbar.', 'enterprise') ?></li>
                            <li><?= _e('Select Tools.', 'enterprise') ?></li>
                            <li><?= _e('Select Clear browsing data.', 'enterprise') ?></li>
                            <li><?= _e('In the dialog that appears, select the checkboxes for the types of information that you want to remove.', 'enterprise') ?></li>
                            <li><?= _e('Use the menu at the top to select the amount of data that you want to delete. Select beginning of time to delete everything.', 'enterprise') ?></li>
                            <li><?= _e('Click Clear browsing data.', 'enterprise') ?></li>
                        </ol>

                        <h2><?= _e('Recommended Browsers for Optimum Performance', 'enterprise') ?></h2>

                        <style>

                            #performanceTable ul{
                                margin: 20px;
                            }

                        </style>

                        <div style="width: 602px; height: 289px; border: 4px solid #e2e2e2;" id="performanceTable" > 
                            <div style="width: 300px; float: left; border-right: 2px solid #e2e2e2;" >
                                <div style="height: 125px; border-bottom: 2px solid #e2e2e2; padding: 5px;" >
                                    <h5><?= _e('Windows 8 & up', 'enterprise') ?></h5>
                                    <ul>
                                        <li><?= _e('IE 10 & up', 'enterprise') ?></li>
                                        <li><?= _e('Firefox 12 & up', 'enterprise') ?></li>
                                        <li><?= _e('Chrome 40 & up', 'enterprise') ?></li>
                                    </ul>
                                </div>
                                <div style="height: 143px;  padding: 5px; ">
                                    <h5><?= _e('Mac OSX 10.8 & up', 'enterprise') ?></h5>
                                    <ul>
                                        <li><?= _e('Safari 6 & up', 'enterprise') ?></li>
                                        <li><?= _e('Firefox 11 & up', 'enterprise') ?></li>
                                        <li><?= _e('Chrome 26 & up', 'enterprise') ?></li>
                                    </ul>
                                </div>
                            </div>
                            <div style="width: 300px; float: right;  ">
                                <div style=" height: 80px; border-bottom: 2px solid #e2e2e2; padding: 5px;"  >
                                    <h5><?= _e('Android powered mobile devices', 'enterprise') ?></h5>   
                                    <ul>
                                        <li><?= _e('Android 4.2 & up', 'enterprise') ?></li>
                                    </ul>
                                </div>
                                <div style="height: 80px; border-bottom: 2px solid #e2e2e2; padding: 5px; "  >
                                    <h5><?= _e('iOS mobile devices', 'enterprise') ?></h5>
                                    <ul>
                                        <li><?= _e('iOS 8 & up', 'enterprise') ?></li>
                                    </ul>
                                </div>
                                <div style="height: 95px; padding: 5px; "  >
                                    <h5><?= _e('Microsoft Surface', 'enterprise') ?> </h5> 
                                    <ul>
                                        <li> <?= _e('Windows RT', 'enterprise') ?></li>
                                        <li> <?= _e('Windows Pro', 'enterprise') ?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>




                        <h5><?php printf(__('Once you have all this checked, please <a href="index.php">click here to watch %s</a>', 'enterprise'), $wb_ent_options['channelname']) ?></a>.</h5>
                    </div>




                <?php endwhile;
            else: ?>
                <p>&nbsp;</p>
<?php endif; ?>

        </div>
    </div>
    <?php
    get_sidebar();
    get_footer();
    ?>
