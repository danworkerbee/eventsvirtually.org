<?php
/**
 * filename: wb-sso-redirect-page-impexium.php
 * description: this template will be used for SSO with Impexium when users are sent 
 *    to the client's login page and redirected back to the platform. This template
 *    requires a token value $_GET['sso'] by Impexium as well the url 
 *    to redirect users to. When sending users to the client's login page, the 
 *    ReturnUrl needs to be urlencoded for the redirection to work properly.
 * author: Jullie Quijano
 * date created: 2018-06-07
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: SSO Redirect Page(Impexium)
 */


/*
 * HOW TO USE:
 * 1. Update value for ACCESS_END_POINT. The value for this is usually 
 *    https://public.impexium.com/Api/v1/WebApiUrl but to make sure, go to the 
 *    API documentation CLIENT.mpxstage.com/api/help and look for the URL being
 *    used.
 * 2. Update the APP_NAME, APP_KEY, APP_ID, APP_PASSWORD based on the 
 *    credentials provided by Impexium for the client's account.
 * 3. Make sure WorkerBee.TV has it's own Impexium account, something that will
 *    have enough permissions to query users. For this account, update the values
 *    for WB_USERNAME and WB_PASSWORD.
 * 4. Update $user_role_to_add with the role name to be used for new users signing
 *    in to the platform using SSO.
 * 5. Update $user_role_label with the label for the above user role.
 * 6. Create a $default_password that will be used as the password for newly added
 *    users.
 * 7. Update $redirectLink with the URL where users will be redirected to by 
 *    default if the returned value does not specify a post/page. This is usually
 *    set to '/home'
 * 8. To use additional information for the user, do a search on the file for 
 *    @todo and follow instructions. See documentation 
 *      http://asca.mpxstage.com/api/Help/Api/GET-api-v1-Individuals-FindBySsoToken-sso_updateTokenUsage_includeDetails
 *    for the return values to find the information you would like to grab from 
 *    the user profile. This information will be saved as user_meta inside the 
 *    platform.
 */

//values provided by Impexium
define("ACCESS_END_POINT", "http://public.impexium.com/Api/v1/WebApiUrl");

define("APP_NAME", "");
define("APP_KEY", "");
define("APP_ID", "");
define("APP_PASSWORD", "");

define("WB_USERNAME", "");
define("WB_PASSWORD", "");

$user_role_to_add = '';
$user_role_label = '';
$default_password = '';
$redirectLink = '/home';

$asca_login_status = false;
$_SESSION["asca_try"] = "failed";

$error_message = array();
//echo "\r\n".' $error_message is '.print_r($error_message, true);

if( trim($_GET['sso']) != '' ){
	$sso_val = sanitize_text_field($_GET['sso']);
	
	if ( $_GET['pid'] != "" ){
		$redirectLink = "/?p=" . sanitize_text_field($_GET['pid']);
	}
	  //echo "\r\n".' $redirectLink is '.$redirectLink;
	
		//Step 1 : Get ApiEndPoint and AccessToken
		//POST api/v1/WebApiUrl
		$data = array(
		'AppName' => APP_NAME,
		'AppKey' => APP_KEY,								  
		);
										 
		//echo "\r\n".' $data is '.print_r($data, true);										  							   

		$data = impexium_send_request(ACCESS_END_POINT, $data);
		//echo "\r\n".' $data2 is '.print_r($data, true);				
		
		$apiEndPoint = $data->uri;
		$accessToken = $data->accessToken;

		//Step 2: Get AppToken or UserToken or Both
		//POST api/v1/Signup/Authenticate
		$data = array(
		'AppId' =>APP_ID,
		'AppPassword' => APP_PASSWORD,
		'AppUserEmail' => WB_USERNAME,
		'AppUserPassword' => WB_PASSWORD,
		);
		//echo "\r\n".' $data3 is '.print_r($data, true);	
		
		$data = impexium_send_request($apiEndPoint, $data, array(
		'accesstoken: ' . $accessToken,
		));
		//echo "\r\n".' $data4 is '.print_r($data, true);	
		
		$appToken = $data->appToken;
		$baseUri = $data->uri;
		$userToken = $data->userToken;
		$userId = $data->userId;		

	$sso_end_point = '/Individuals/FindBySsoToken/'.$sso_val.'?updateTokenUsage=false&includeDetails=true';
	$data = impexium_send_request($baseUri.$sso_end_point, null, array(
		'usertoken: ' . $userToken,
		'apptoken:' . $appToken,
		));
	//echo "\r\n".' $data5 is '.print_r($data, true);		
	
	//get required values	
	$user_email = $data->dataList[0]->email;	
	$user_first_name = $data->dataList[0]->firstName;
	$user_last_name = $data->dataList[0]->lastName;	
	
	/*
	 * @todo grab additional information needed for each user and assign them to 
	 * respective variables.	
	 * 
	 * ADDRESS_LINE_1
	 * ADDRESS_CITY
	 * ADDRESS_STATE_CODE
	 * ADDRESS_ZIP
	 * NUMBER
	 * Membership
	 */
	
	  //echo "\r\n".' $user_email is '.$user_email;	
		//echo "\r\n".' $user_first_name is '.$user_first_name;	
		//echo "\r\n".' $user_last_name is '.$user_last_name;	
	  
    $_SESSION["asca_token"] = $appToken;

    if (trim($user_email) != '') {
      //echo "\r\n".'email not blank';

      if (!function_exists('get_role')) {
        //echo "\r\n".'function get_role does not exist';
      } else {
        //echo "\r\n".'function get_role exists';
      }

      $getRoleValue = get_role($user_role_to_add);
      //echo "\r\n".'$getRoleValue is ' . print_r($getRoleValue, true);

      if (!is_object($getRoleValue)) {
        //echo "\r\n".'create new role';
        global $wp_roles;
        if (!isset($wp_roles)) {
          $wp_roles = new WP_Roles();
        }

        $custRole = $wp_roles->get_role('subscriber');
        //echo "\r\n".'$custRole is ' . print_r($custRole, true);

        $wp_roles->add_role($user_role_to_add, $user_role_label, $custRole->capabilities);
      }

      $emailAddress = trim($user_email);

      $emailExists = email_exists($user_email);
      //echo "\r\n".'$emailExists is ' . $emailExists;
      $usernameExists = username_exists($emailAddress);
      //echo "\r\n".'$usernameExists is ' . $usernameExists;

      //check if user exists
      if ($emailExists || $usernameExists) {

        $userId = (is_numeric($emailExists)) ? $emailExists : ( (is_numeric($emailAddress)) ? $emailAddress : 0 );
        $user_info = get_userdata($userId);
        //echo "\r\n".'$user_info is ' . print_r($user_info, true);
        $asca_login_status = true;
        $_SESSION["asca_try"] = "success";
        wp_set_auth_cookie($userId, true);
      }
      else {
        $createNewUser = true;
        $userLoggedIn = false;
        //echo "\r\n".'user does not exist';
      }

      if ($createNewUser) {
        //echo "\r\n".'creating new user';

        $userdata = array(
            'user_login' => $emailAddress,
            'user_email' => $emailAddress,
            'first_name' => $user_first_name,
            'last_name' => $user_last_name,
            'role' => $user_role_to_add,
            'user_pass' => $default_password
        );
        //echo "\r\n".'$userdata is ' . print_r($userdata, true);

        $user_id = wp_insert_user($userdata);
        //echo "\r\n".'$user_id is ' . print_r($user_id, true);

        if (is_wp_error($user_id)) {
          $displayMessage = true;
          $messageText = 'Please enter a valid email address.';
          $userLoggedIn = false;
        } else {
          $creds = array();
          $creds['user_login'] = $emailAddress;
          $creds['user_password'] = $default_password;
          $creds['remember'] = true;
          //echo "\r\n".'$creds last is ' . print_r($creds, true);

          $user = wp_signon($creds, true);
          //echo "\r\n".'$user is ' . print_r($user, true);

          if (is_wp_error($user)) {
            $createNewUser = true;
            $userLoggedIn = false;
            $displayMessage = true;
            $messageText = 'Please enter a valid email address.';
            //echo "\r\n".'wp_error ';
          } else {
            $userLoggedIn = true;
            $asca_login_status = true;
            $_SESSION["asca_try"] = "success";
            //echo "\r\n".'no error ';
          }
        }
      }
			
			/*
			 * @todo add post meta for users for additional information needed
			 */

      if (is_user_logged_in()) {
      	$asca_login_status = true;
      	$_SESSION["asca_try"] = "success";
        //echo "\r\n".'user_is logged in ';
        if (trim($redirectLink) != '') {
        header('location: ' . $redirectLink);
        } else {
        header('location: /');
        }
      }
      else{
        //echo "\r\n".'user_is NOT logged in ';
        if (trim($redirectLink) != '') {
        header('location: ' . $redirectLink);
        } else {
        header('location: /');
        }
      }
      exit;
    }
}
?>
