<?php
/*
  Filename:      mrss.php
  Description:
  Author:        Jullie Quijano
 */
$prewd = getcwd();
chdir(realpath(dirname(__FILE__)));

error_reporting(E_ALL);

include ('../../../../wp-config.php');


//get theme options
try {
    $dbh = new PDO('mysql:host='.DB_HOST.';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

    $getThemeOptions = $dbh->prepare("SELECT option_value FROM wp_options WHERE option_name = 'wb_ent_options'");

    $getThemeOptions->execute();
    $getThemeOptionsNumrows = $getThemeOptions->rowCount();
    $getThemeOptionsResult = $getThemeOptions->fetch();

    $getThemeOptions = null;
    $dbh = null;
} catch (PDOException $e) {
    echo "Error!: Could not connect to DB";
}

$wb_ent_options = unserialize($getThemeOptionsResult['option_value']);

$strReplaceSearch = array(
   "¡", "¢", "£", "¤", "¥", "¦", "§", "¨", "©", "ª",
   "«", "¬", "®", "¯", "°", "±", "²", "³", "´",
   "µ", "¶", "·", "¸", "¹", "º", "»", "¼", "½", "¾",
   "¿", "×", "÷", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ",
   "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", "Ð",
   "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û",
   "Ü", "Ý", "Þ", "ß", "à", "á", "â", "ã", "ä", "å",
   "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï",
   "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú",
   "û", "ü", "ý", "þ", "ÿ", "  ", "\r", "\n", "\t", "\v",
   "“", "‘", "’", "“", "”", "…", "–", "—", "“", "”", "-", " ", "•", "€", "™", "&amp;", ""
);

$strReplaceChars = array(
   "&#161;", "&#162;", "&#163;", "&#164;", "&#165;", "&#166;", "&#167;", "&#168;", "&#169;", "&#170;",
   "&#171;", "&#172;", "&#174;", "&#175;", "&#176;", "&#177;", "&#178;", "&#179;", "&#180;",
   "&#181;", "&#182;", "&#183;", "&#184;", "&#185;", "&#186;", "&#187;", "&#188;", "&#189;", "&#190;",
   "&#191;", "&#215;", "&#247;", "&#192;", "&#193;", "&#194;", "&#195;", "&#196;", "&#197;", "&#198;",
   "&#199;", "&#200;", "&#201;", "&#202;", "&#203;", "&#204;", "&#205;", "&#206;", "&#207;", "&#208;",
   "&#209;", "&#210;", "&#211;", "&#212;", "&#213;", "&#214;", "&#216;", "&#217;", "&#218;", "&#219;",
   "&#220;", "&#221;", "&#222;", "&#223;", "&#224;", "&#225;", "&#226;", "&#227;", "&#228;", "&#229;",
   "&#230;", "&#231;", "&#232;", "&#233;", "&#234;", "&#235;", "&#236;", "&#237;", "&#238;", "&#239;",
   "&#240;", "&#241;", "&#242;", "&#243;", "&#244;", "&#245;", "&#246;", "&#248;", "&#249;", "&#250;",
   "&#251;", "&#252;", "&#253;", "&#254;", "&#255;", " ", " ", " ", " ", " ",
   "\"", "'", "'", "\"", "\"", "...", "-", "-", "\"", "\"", "-", " ", " ", "", "&#153;", "&#038;", ""
);

   //get private && unlisted videos
   $unlistedAndPrivateVids = array();

   try {
      $dbh = new PDO('mysql:host='.DB_HOST.';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
           
           
      $catUnlisted = isset($wb_ent_options['videocats']['unlisted']) ? $wb_ent_options['videocats']['unlisted'] : 0 ;
      $catPrivate = isset($wb_ent_options['videocats']['private']) ? $wb_ent_options['videocats']['private'] : 0 ;           
           
      if( $wb_ent_options['vidshortcode']['enabled'] ){
         $getPrivateUnlistedPosts = $dbh->prepare("
            SELECT p.ID, p.post_title, p.post_name, p.post_content 
               FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
               WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
               AND tt.term_id=t.term_id
               AND tr.object_id=p.ID         
               AND p.post_status='publish'
               AND ( t.term_id=?
               OR t.term_id=? )
               GROUP BY p.ID
               ORDER BY p.post_date DESC 
         ");
            
         $getPrivateUnlistedPosts->bindParam(1, $catUnlisted);
         $getPrivateUnlistedPosts->bindParam(2, $catPrivate);
      }
      else{
         $getPrivateUnlistedPosts = $dbh->prepare("
            SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content 
            FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media b
            WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
            AND tt.term_id=t.term_id
            AND tr.object_id=p.ID
            AND p.ID=b.post_id
            AND p.post_status='publish'
            AND ( t.term_id=?
            OR t.term_id=? )
            GROUP BY p.ID
            ORDER BY p.post_date DESC
         ");
         
         $getPrivateUnlistedPosts->bindParam(1, $catUnlisted);
         $getPrivateUnlistedPosts->bindParam(2, $catPrivate);
         
         
      }           
           
      $getPrivateUnlistedPosts->execute();      
      $getPrivateUnlistedPostsNumrows = $getPrivateUnlistedPosts->rowCount();
      $getPrivateUnlistedPostsResult = $getPrivateUnlistedPosts->fetchAll();
               
      $getPrivateUnlistedPosts= null;  
      $dbh = null; 
              
   }catch (PDOException $e) { 
      echo "Error!: Could not connect to DB";
   }       
   
   foreach ($getPrivateUnlistedPostsResult as $currentPrivateUnlistedPost) {
      $unlistedAndPrivateVids[] = $currentPrivateUnlistedPost['ID'];
   }

header('Content-type: application/rss+xml; charset=utf-8');
?>
<rss version='2.0' xmlns:media='http://search.yahoo.com/mrss/' xmlns:atom="http://www.w3.org/2005/Atom">

    <channel>
        
        <atom:link href="http://<?php echo $wb_ent_options['channeldomain']; ?>/wp-content/themes/enterprise/feed/mrss.php" rel="self" type="application/rss+xml" />
        <title><?php echo $wb_ent_options['channelname']; ?></title>

        <link><![CDATA[<?php echo get_site_url(); ?>]]></link>

        <description><![CDATA[<?php echo $wb_ent_options['channelname']; ?>]]></description> 

        <?php
        try {
            $dbh = new PDO('mysql:host='.DB_HOST.';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

            if ($wb_ent_options['vidshortcode']['enabled']) {
                $getPosts = $dbh->prepare("SELECT *
                    FROM wp_posts p
                    WHERE p.post_status = 'publish'
                    AND p.post_type='post'
                    GROUP BY p.ID
                    ORDER BY p.post_date DESC
                    LIMIT 20");                
            }
            else{
                $getPosts = $dbh->prepare("SELECT *
                    FROM wp_posts p, wb_media m
                    WHERE p.ID = m.post_id
                    AND p.post_status = 'publish'
                    AND p.post_type='post'
                    GROUP BY p.ID
                    ORDER BY p.post_date DESC
                    LIMIT 20");                
            }

            $getPosts->execute();
            $getPostsNumrows = $getPosts->rowCount();
            $getPostsResult = $getPosts->fetchAll();

            $getPosts = null;
            $dbh = null;
        } catch (PDOException $e) {
            echo "Error!: Could not connect to DB";
        }

        //echo '$getPostsResult is '.print_r($getPostsResult, true);

        foreach ($getPostsResult as $row) {

            if( in_array($row['ID'], $unlistedAndPrivateVids) ){
               continue;               
            }            

            $curlString = '';
            if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)mode(.*)' . $wb_ent_options['vidshortcode']['name'] . '(.*)\/\]/', $row['post_content'], $match) >= 1) {
                //echo 'inside first if';
                //echo 'match is '.print_r($match, true);
                $row['mediaId'] = $match[3][0];
                $row['post_content'] = removeShortCode($wb_ent_options['vidshortcode']['tag'],$row['post_content']);
                $curlString = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id=' . $row['mediaId'] . '&video_fields=id,name,startDate,publishedDate,length,shortDescription,longDescription,thumbnailURL,videoStillURL,renditions,FLVURL&media_delivery=http&token=' . $wb_ent_options['vidshortcode']['token'];
            } else if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)\/\]/', $row['post_content'], $match) >= 1) {
                //echo 'inside second if';
                //echo 'match is '.print_r($match, true);                
                $row['mediaId'] = $match[3][0];
                $row['post_content'] = removeShortCode($wb_ent_options['vidshortcode']['tag'],$row['post_content']);
                $curlString = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id=' . $row['mediaId'] . '&video_fields=id,name,startDate,publishedDate,length,shortDescription,longDescription,thumbnailURL,videoStillURL,renditions,FLVURL&media_delivery=http&token=' . $wb_ent_options['brightcoveinfo']['readurltoken'];
            } else {
                //echo 'inside third if';
                $row['mediaId'] = $row['media_id'];
                $curlString = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id=' . $row['mediaId'] . '&video_fields=id,name,startDate,publishedDate,length,shortDescription,longDescription,thumbnailURL,videoStillURL,renditions,FLVURL&media_delivery=http&token=' . $wb_ent_options['brightcoveinfo']['readurltoken'];
            }
            
            $mediaId = $row['media_id'];

            if ($getPostsNumrows > 0) {
                
                $ch = curl_init();
                $timeout = 0; // set to zero for no timeout
                curl_setopt($ch, CURLOPT_URL, $curlString);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                $file_contents = curl_exec($ch);
                curl_close($ch);
                $video = json_decode($file_contents);


                $seconds = $video->length;
            }


            $post_title = str_replace($strReplaceSearch, $strReplaceChars, $row['post_title']);
            $post_title = str_replace('&#153;', '&#8482;', $row['post_title'] );
            $post_title = strip_tags($post_title);

            $post_content = str_replace($strReplaceSearch, $strReplaceChars, $row['post_content']);
            $post_content = str_replace($strReplaceSearch, $strReplaceChars, $post_content);
            $post_content = strip_tags($post_content);            

            $permalink = get_permalink($row['ID']);
            ?>

            <item>



                <title><![CDATA[<?php echo $post_title; ?>]]></title>

                <link><![CDATA[<?php echo $permalink; ?>]]></link>




                <description><![CDATA[<?php echo $post_content; ?>]]></description>



                <guid isPermaLink='true'><![CDATA[<?php echo $permalink; ?>]]></guid>

                <media:content url='<?php echo $video->FLVURL; ?>' fileSize='<?php echo $video->renditions[0]->size; ?>' duration='<?php echo $seconds; ?>' type='video/x-sgi-movie' height='360' width='640' medium='video' isDefault='true'>

                    <media:title><![CDATA[<?php echo $post_title; ?>]]></media:title>

                    <media:description type='html'><![CDATA[<?php echo $post_content; ?>]]></media:description>




                    <media:thumbnail url='<?php echo $video->thumbnailURL; ?>'  height='67' width='120' />

                </media:content>



            </item>

    <?php
}
?>

    </channel>

</rss>
<?php

function removeShortCode($codeTag, $string) {
    return preg_replace("/\[" . $codeTag . " (.*)\/\]/i", "", $string);
}
?>