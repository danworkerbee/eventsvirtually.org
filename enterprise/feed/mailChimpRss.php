<?php
/*
Filename:      mrss.php                                                 
Description:   
Author:        Ronald Iraheta
Change Log:
	June 20, 2011  [Jullie]Changed the title, link and description to the channel constants; commented out the include 
 						bc-mapi line                                                                                                                                                                                                               
*/
//include('../includes/config.php');
include ('../../../../wp-config.php');
//include('../includes/config.php');
//include('../includes/brightcove-tokens.php');

//get theme options
try {
    $dbh = new PDO('mysql:host=localhost;dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

    $getThemeOptions = $dbh->prepare("SELECT option_value FROM wp_options WHERE option_name = 'wb_ent_options'");

    $getThemeOptions->execute();
    $getThemeOptionsNumrows = $getThemeOptions->rowCount();
    $getThemeOptionsResult = $getThemeOptions->fetch();

    $getThemeOptions = null;
    $dbh = null;
} catch (PDOException $e) {
    echo "Error!: Could not connect to DB";
}

$wb_ent_options = unserialize($getThemeOptionsResult['option_value']);



   $strReplaceSearch = array(
      "¡", "¢", "£", "¤", "¥", "¦", "§", "¨", "©", "ª", 
      "«", "¬", "®", "¯", "°", "±", "²", "³", "´", 
      "µ", "¶", "·", "¸", "¹", "º", "»", "¼", "½", "¾", 
      "¿", "×", "÷", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", 
      "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", "Ð", 
      "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", 
      "Ü", "Ý", "Þ", "ß", "à", "á", "â", "ã", "ä", "å", 
      "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", 
      "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", 
      "û", "ü", "ý", "þ", "ÿ", "  ", "\r", "\n", "\t", "\v",
      "“", "‘", "’", "“", "”", "…", "–", "—", "“", "”", "-", " ", "•", "€", "™", "&amp;", ""          
   );

   $strReplaceChars = array(
      "&#161;", "&#162;", "&#163;", "&#164;", "&#165;", "&#166;", "&#167;", "&#168;", "&#169;", "&#170;", 
      "&#171;", "&#172;", "&#174;", "&#175;", "&#176;", "&#177;", "&#178;", "&#179;", "&#180;", 
      "&#181;", "&#182;", "&#183;", "&#184;", "&#185;", "&#186;", "&#187;", "&#188;", "&#189;", "&#190;", 
      "&#191;", "&#215;", "&#247;", "&#192;", "&#193;", "&#194;", "&#195;", "&#196;", "&#197;", "&#198;", 
      "&#199;", "&#200;", "&#201;", "&#202;", "&#203;", "&#204;", "&#205;", "&#206;", "&#207;", "&#208;", 
      "&#209;", "&#210;", "&#211;", "&#212;", "&#213;", "&#214;", "&#216;", "&#217;", "&#218;", "&#219;", 
      "&#220;", "&#221;", "&#222;", "&#223;", "&#224;", "&#225;", "&#226;", "&#227;", "&#228;", "&#229;", 
      "&#230;", "&#231;", "&#232;", "&#233;", "&#234;", "&#235;", "&#236;", "&#237;", "&#238;", "&#239;", 
      "&#240;", "&#241;", "&#242;", "&#243;", "&#244;", "&#245;", "&#246;", "&#248;", "&#249;", "&#250;", 
      "&#251;", "&#252;", "&#253;", "&#254;", "&#255;", " ", " ", " ", " ", " ",
      "\"", "'", "'", "\"", "\"", "...", "-", "-", "\"", "\"", "-", " ", " ", "", "&#153;", "&#038;", ""          
   );	

header('Content-type: application/rss+xml; charset=utf-8');
?>
<rss version='2.0' xmlns:media='http://search.yahoo.com/mrss/' xmlns:atom="http://www.w3.org/2005/Atom">

<channel>
<atom:link href="http://<?php echo $wb_ent_options['channeldomain']; ?>/wp-content/themes/enterprise/feed/mailChimpRss.php" rel="self" type="application/rss+xml" />
<title><?php echo $wb_ent_options['channelname']; ?></title>

<link><![CDATA[<?php echo get_site_url(); ?>]]></link>

<description><![CDATA[<?php echo $wb_ent_options['channelname']; ?>]]></description> 

<?php

        try {
            $dbh = new PDO('mysql:host=localhost;dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

            if ($wb_ent_options['vidshortcode']['enabled']) {
                $getPosts = $dbh->prepare("SELECT *
                    FROM wp_posts p
                    WHERE p.post_status = 'publish'
                    AND p.post_type='post'
                    GROUP BY p.ID
                    ORDER BY p.post_date DESC
                    LIMIT 1");                
            }
            else{
                $getPosts = $dbh->prepare("SELECT *
                    FROM wp_posts p, wb_media m
                    WHERE p.ID = m.post_id
                    AND p.post_status = 'publish'
                    AND p.post_type='post'
                    GROUP BY p.ID
                    ORDER BY p.post_date DESC
                    LIMIT 1");                
            }


            $getPosts->execute();
            $getPostsNumrows = $getPosts->rowCount();
            $getPostsResult = $getPosts->fetchAll();

            $getPosts = null;
            $dbh = null;
        } catch (PDOException $e) {
            echo "Error!: Could not connect to DB";
        }


foreach ($getPostsResult as $row) {

$mediaId = $row['media_id'];


$countMedia = count($mediaId);

            $curlString = '';
            if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)mode(.*)' . $wb_ent_options['vidshortcode']['name'] . '(.*)\/\]/', $row['post_content'], $match) >= 1) {
                //echo 'inside first if';
                //echo 'match is '.print_r($match, true);
                $row['mediaId'] = $match[3][0];
                $row['post_content'] = removeShortCode($wb_ent_options['vidshortcode']['tag'],$row['post_content']);
                $curlString = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id=' . $row['mediaId'] . '&video_fields=id,name,startDate,publishedDate,length,shortDescription,longDescription,thumbnailURL,videoStillURL,renditions,FLVURL&media_delivery=http&token=' . $wb_ent_options['vidshortcode']['token'];
                $tokenToUse = $wb_ent_options['vidshortcode']['token'];
            } else if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)\/\]/', $row['post_content'], $match) >= 1) {
                //echo 'inside second if';
                //echo 'match is '.print_r($match, true);                
                $row['mediaId'] = $match[3][0];
                $row['post_content'] = removeShortCode($wb_ent_options['vidshortcode']['tag'],$row['post_content']);
                $curlString = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id=' . $row['mediaId'] . '&video_fields=id,name,startDate,publishedDate,length,shortDescription,longDescription,thumbnailURL,videoStillURL,renditions,FLVURL&media_delivery=http&token=' . $wb_ent_options['brightcoveinfo']['readurltoken'];
                $tokenToUse = $wb_ent_options['vidshortcode']['token'];
            } else {
                //echo 'inside third if';
                $row['mediaId'] = $row['media_id'];
                $curlString = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id=' . $row['mediaId'] . '&video_fields=id,name,startDate,publishedDate,length,shortDescription,longDescription,thumbnailURL,videoStillURL,renditions,FLVURL&media_delivery=http&token=' . $wb_ent_options['brightcoveinfo']['readurltoken'];
                $tokenToUse = $wb_ent_options['brightcoveinfo']['readurltoken'];
            }
            
            $mediaId = $row['media_id'];
            
            if ($getPostsNumrows > 0) {
                
                $ch = curl_init();
                $timeout = 0; // set to zero for no timeout
                curl_setopt($ch, CURLOPT_URL, $curlString);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                $file_contents = curl_exec($ch);
                curl_close($ch);
                $video = json_decode($file_contents);


                $seconds = $video->length;
            }

//print_r($video);
//echo '$wb_ent_options is '.print_r($wb_ent_options, true);

$imageLink = explode('?pubId', $video->videoStillURL );
echo '$imageLink is '.print_r($imageLink, true);

copy($video->videoStillURL, '../images/rssStills/'.$video->id.'_still.jpg');
$imageSize = filesize('/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/wp-content/themes/enterprise/images/rssStills/'.$video->id.'_still.jpg');



//momentary fix
$pos = strpos($downloadLink, 'http://brightcove04.brightcove.com/media/');
if($pos !== false){
	$downloadLink = '';
}

$post_title = str_replace('&trade;', '', $row['post_title'] );
$post_title = str_replace('&#153;', '&#8482;', $row['post_title'] );
$post_content = str_replace('&trade;', '', $row['post_content']) ;
$post_content = str_replace('&mdash;', '', $post_content );
$post_title = strip_tags($post_title);
//$post_title = limit($post_title,70);
$post_content = strip_tags($post_content);
//$post_content = limit($post_content,135);
//removeShortCode
$post_content = str_replace($strReplaceSearch, $strReplaceChars, removeShortCode('VIDEO', $post_content) );

$permalink = get_permalink( $row['ID'] ).'?utm_source=mailchimp&utm_medium=newsletter&utm_content='.urlencode($post_title).'&utm_name='.$wb_ent_options['channelname'].'Newsletter'.date('Y-m-d'); 
?>

<item>

	<?php	
	
		//getting the pubdate to RFC822
		$postId = $row['ID'] ;
		
		$sql2 = "SELECT DATE_FORMAT(post_date,'%a, %d %b %Y %T') AS rfcpubdate 
		FROM wp_posts 
		WHERE ID = $postId "
		;
	
		$result2 = mysql_query($sql2) ;
		
		$rfcpubdate = mysql_fetch_row($result2);
		
		foreach($rfcpubdate as $date){
		
			echo "<pubDate>$date CST</pubDate>" ;
		
		}
	?>


<title><![CDATA[<?php echo $post_title; ?>]]></title>

<link><![CDATA[<?php echo $permalink ; ?>]]></link>




<description><![CDATA[<?php echo $post_content ; ?>]]></description>



<guid isPermaLink='true'><![CDATA[<?php echo $permalink ; ?>]]></guid>

<media:content url='<?php echo ( trim($imageLink[0]) != '') ? $imageLink[0]: '' ; ?>' type='image/jpg' height='144' width='256' medium='image' >

<media:title><![CDATA[<?php echo $post_title; ?>]]></media:title>

<media:description type='html'><![CDATA[<?php echo $post_content; ?>]]></media:description>

</media:content>

<enclosure url="<?php echo ( trim($imageLink[0]) != '') ? $imageLink[0]: '' ; ?>" length="<?php echo $imageSize; ?>" type="image/jpg" />

</item>

<?php
}
?>

</channel>

</rss>
<?php


	function removeShortCode($codeTag, $string){
		return preg_replace("/\[".$codeTag." (.*)\/\]/i", "", $string);
		
	}


?>
