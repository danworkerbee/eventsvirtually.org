<?php
// Welcome to Carl's sample Brightcove iTunes feed

// Please use this at your own risk.
// This is just a sample to get you started, you can customize further as your requirements grow.

// The following is a list of requirements and conditions in order for this podcast feed to function properly;

// 	1) You must have a Pro or Enterprise level Brightcove Account.
// 	2) You will need to contact Brightcove Support to request an API READ Token with FLV access, if you don't have one already.
// 	3) Due to iTunes current restrictions, you must have a Progressive Download account, this will not work with Streaming accounts.
//	4) You must be uploading your content in H.264 with or without transcoding and renditions. FLVs will not function with this feed.
//	5) You need to create the following custom fields. They are in your account settings on the Dashboard;
//			1) itunesartist		(string)
//			2) itunessize		(string)
//			3) itunestags		(string)
//
//	6) You will have to manually or programmatically set the custom metadata values when you upload content.
//	7) You will need to publish this PHP file on a PHP 5 server.

//Database details
include($_SERVER['DOCUMENT_ROOT']."/wp-config.php");

	$DBhost = "localhost";
	$DBname = DB_NAME;
	$DBuser = DB_USER;
	$DBpass = DB_PASSWORD;

    try {
          $dbh = new PDO('mysql:host='.$DBhost.';dbname='.$DBname.'', $DBuser, $DBpass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));

          $getThemeOptions = $dbh->prepare("SELECT option_value FROM wp_options WHERE option_name = 'wb_ent_options'");

          $getThemeOptions->execute();      
          $getThemeOptionsNumrows = $getThemeOptions->rowCount();
          $getThemeOptionsResult = $getThemeOptions->fetch();

          $getThemeOptions= null;  
          $dbh = null; 

       }catch (PDOException $e) { 
          echo "Error!: Could not connect to DB";
       }          
   $wb_ent_options = unserialize($getThemeOptionsResult['option_value']);
   

// Please customize the variables below;

$title 		= $wb_ent_options['channelname'];						// This is the title of the podcast itself.
$link 		= 'http://'.$wb_ent_options['channeldomain'];				// This is a link to where the podcast can be found.
$description= $wb_ent_options['channeldesc'];			// This is a description of this iTunes Feed.
$lang 		= "en-us";									// This is the language you display for this podcast.
$copyright 	= "Copyright ".date("Y")." " . $wb_ent_options['channelname'];			// This is the copyright information.
$subtitle 	= "For more ".$wb_ent_options['channelname']." videos visit http://".$wb_ent_options['channeldomain'];							// This is the subtitle of the podcast.
$author 	= $wb_ent_options['channelname'];						// This is the author's name.
$summary 	= $wb_ent_options['channeldesc'];			// This is the summary for the podcast.
$ownername 	= $wb_ent_options['channelname'];						// This is the owner's name.
$owneremail = $wb_ent_options['channelemail'];				// This is the owner's email address.
$imageurl 	= $wb_ent_options['itunesimage'];			// This is the podcast thumbnail image url.
$category 	= "TV &amp; Film";							// This is the podcast category.
$explicit 	= "no";									// This is a yes or no boolean if the podcast is explicit.

//$token		= "1Zce3P8xWQwJ_Bq7YULdXDMEdbwLa8eUThsJtZ5FyBQ79wy59hM7Zg..";		// This is your Media API READ token with FLV Access.							// The ID of the playlist you wish to publish.

$itunesID = '0'; //change to specific categories that you DONT want to be added

/*==================================
Replaces special characters with non-special equivalents
==================================*/
function normalize_special_characters( $str )
{
	$str = stripIllegalCharacters($str);
	
   $strReplaceSearch = array(
      "¡", "¢", "£", "¤", "¥", "¦", "§", "¨", "©", "ª", 
      "«", "¬", "®", "¯", "°", "±", "²", "³", "´", 
      "µ", "¶", "·", "¸", "¹", "º", "»", "¼", "½", "¾", 
      "¿", "×", "÷", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", 
      "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", "Ð", 
      "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", 
      "Ü", "Ý", "Þ", "ß", "à", "á", "â", "ã", "ä", "å", 
      "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", 
      "ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", 
      "û", "ü", "ý", "þ", "ÿ", "  ", "\r", "\n", "\t", "\v",
      "“", "‘", "’", "“", "”", "…", "–", "—", "“", "”", "-", " ", "•", "€", "™"      
   );
   
   $strReplaceChars = array(
      "&iexcl;", "&cent;", "&pound;", "&curren;", "&yen;", "&brvbar;", "&sect;", "&uml;", "&copy;", "&ordf;", 
      "&laquo;", "&not;", "&reg;", "&macr;", "&deg;", "&plusmn;", "&sup2;", "&sup3;", "&acute;", 
      "&micro;", "&para;", "&middot;", "&cedil;", "&sup1;", "&ordm;", "&raquo;", "&frac14;", "&frac12;", "&frac34;", 
      "&iquest;", "&times;", "&divide;", "&Agrave;", "&Aacute;", "&Acirc;", "&Atilde;", "&Auml;", "&Aring;", "&AElig;", 
      "&Ccedil;", "&Egrave;", "&Eacute;", "&Ecirc;", "&Euml;", "&Igrave;", "&Iacute;", "&Icirc;", "&Iuml;", "&ETH;", 
      "&Ntilde;", "&Ograve;", "&Oacute;", "&Ocirc;", "&Otilde;", "&Ouml;", "&Oslash;", "&Ugrave;", "&Uacute;", "&Ucirc;", 
      "&Uuml;", "&Yacute;", "&THORN;", "&szlig;", "&agrave;", "&aacute;", "&acirc;", "&atilde;", "&auml;", "&aring;", 
      "&aelig;", "&ccedil;", "&egrave;", "&eacute;", "&ecirc;", "&euml;", "&igrave;", "&iacute;", "&icirc;", "&iuml;", 
      "&eth;", "&ntilde;", "&ograve;", "&oacute;", "&ocirc;", "&otilde;", "&ouml;", "&oslash;", "&ugrave;", "&uacute;", 
      "&ucirc;", "&uuml;", "&yacute;", "&thorn;", "&yuml;", " ", " ", " ", " ", " ",
      "\"", "'", "'", "\"", "\"", "...", "-", "-", "\"", "\"", "-", " ", " ", "", "&trade;"       
   );		
   
   $str = str_replace( $strReplaceSearch, $strReplaceChars, $str);
		
	
    # Quotes cleanup
    $str = ereg_replace( chr(ord("`")), "'", $str );        # `
    $str = ereg_replace( chr(ord("´")), "'", $str );        # ´
    $str = ereg_replace( chr(ord("„")), ",", $str );        # „
    $str = ereg_replace( chr(ord("`")), "'", $str );        # `
    $str = ereg_replace( chr(ord("´")), "'", $str );        # ´
    $str = ereg_replace( chr(ord("“")), "\"", $str );        # “
    $str = ereg_replace( chr(ord("”")), "\"", $str );        # ”
    $str = ereg_replace( chr(ord("´")), "'", $str );        # ´
    $str = ereg_replace( chr(ord("’")), "'", $str );        # ’

	$str = str_replace(chr(130), ',', $str);    // baseline single quote
	$str = str_replace(chr(131), 'NLG', $str);  // florin
	$str = str_replace(chr(132), '"', $str);    // baseline double quote
	$str = str_replace(chr(133), '...', $str);  // ellipsis
	$str = str_replace(chr(134), '**', $str);   // dagger (a second footnote)
	$str = str_replace(chr(135), '***', $str);  // double dagger (a third footnote)
	$str = str_replace(chr(136), '^', $str);    // circumflex accent
	$str = str_replace(chr(137), 'o/oo', $str); // permile
	$str = str_replace(chr(138), 'Sh', $str);   // S Hacek
	$str = str_replace(chr(139), '<', $str);    // left single guillemet
	$str = str_replace(chr(140), 'OE', $str);   // OE ligature
	$str = str_replace(chr(145), "'", $str);    // left single quote
	$str = str_replace(chr(146), "'", $str);    // right single quote
	$str = str_replace(chr(147), '"', $str);    // left double quote
	$str = str_replace(chr(148), '"', $str);    // right double quote
	$str = str_replace(chr(149), '-', $str);    // bullet
	$str = str_replace(chr(150), '-', $str);    // endash
	$str = str_replace(chr(151), '--', $str);   // emdash
	$str = str_replace(chr(152), '~', $str);    // tilde accent
	$str = str_replace(chr(153), '(TM)', $str); // trademark ligature
	$str = str_replace(chr(154), 'sh', $str);   // s Hacek
	$str = str_replace(chr(155), '>', $str);    // right single guillemet
	$str = str_replace(chr(156), 'oe', $str);   // oe ligature
	$str = str_replace(chr(159), 'Y', $str);    // Y Dieresis

    $unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                                'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                                'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                                'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                                'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', '`'=>"'", '´'=>"'", '„'=>',', '“'=>'"', '”'=>'"',
 								'’'=>"", '‘'=>"'", '�'=>'' );
    $str = strtr( $str, $unwanted_array );

    # Bullets, dashes, and trademarks
    $str = ereg_replace( chr(149), "&#8226;", $str );    # bullet •
    $str = ereg_replace( chr(150), "&ndash;", $str );    # en dash
    $str = ereg_replace( chr(151), "&mdash;", $str );    # em dash
    $str = ereg_replace( chr(153), "&#8482;", $str );    # trademark
    $str = ereg_replace( chr(169), "&copy;", $str );    # copyright mark
    $str = ereg_replace( chr(174), "&reg;", $str );        # registration mark

	$str = ereg_replace( "&", "&amp;", $str);
	
	
    return $str;
}

function nohtml($str) 
{
	$str = preg_replace("'<script[^>]*?>.*?</script>'si", "", $str);
	$str = preg_replace("'<head[^>]*?>.*?</head>'si", "", $str);
	$patterns[0] = "/</"; 
	$patterns[1] = "/>/"; 
	$replacements[0] = ""; 
	$replacements[1] = "";
	$str = preg_replace($patterns, $replacements, $str); 
	$allowed = "";
	$str = preg_replace("/<((?!\/?($allowed)\b)[^>]*>)/xis", "", $str);
	$str = preg_replace("/<($allowed).*?>/i", "<\\1>", $str);
	$str = preg_replace("/[\\r\\n]/", " ", $str);
	$str = preg_replace("/[\\r\\n]/", " ", $str);
	$str = addslashes($str);
	return $str;
}

function stripIllegalCharacters($str){
	$str = strip_tags($str);
	$str = utf8_encode($str);
	$str = nohtml($str);
	$str = str_replace(array("+", "-", "?"), " ", $str);
	$str = str_replace(array("&"), "and", $str);
	$str = str_replace(array("\'"), "", $str);
	$str = str_replace(array('\"'), '"', $str);
	
	return $str;
}

// Please DO NOT alter the code below;

header('Content-type: application/rss+xml; charset=utf-8');

print('<rss version="2.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:atom="http://www.w3.org/2005/Atom">'.PHP_EOL);
print('<channel>'.PHP_EOL);
print('<atom:link href="http://'.$wb_ent_options['channeldomain'].'/wp-content/themes/enterprise/feed/itunes.php" rel="self" type="application/rss+xml" />'.PHP_EOL);
//print('<itunes:new-feed-url>http://feeds.feedburner.com/AmericanPrinterTV</itunes:new-feed-url>'.PHP_EOL);
print('  <ttl>60</ttl>'.PHP_EOL);
print('  <title>'. $title . '</title>'.PHP_EOL);
print('  <link>'. $link . '</link>'.PHP_EOL);
print('  <description><![CDATA['. $description . ']]></description>'.PHP_EOL);
print('  <language>'. $lang . '</language>'.PHP_EOL);
print('  <copyright>'. $copyright . '</copyright>'.PHP_EOL);
print('  <itunes:subtitle>'. $subtitle .'</itunes:subtitle>'.PHP_EOL);
print('  <itunes:author>'. $author .'</itunes:author>'.PHP_EOL);
print('  <itunes:summary>'. $summary .'</itunes:summary>'.PHP_EOL);
print('  <itunes:owner>'.PHP_EOL);
print('    <itunes:name>'. $ownername .'</itunes:name>'.PHP_EOL);
print('    <itunes:email>'. $owneremail .'</itunes:email>'.PHP_EOL);
print('  </itunes:owner>'.PHP_EOL);
print('  <itunes:image href="'. $imageurl .'" />'.PHP_EOL);
print('  <itunes:category text="'. $category .'"></itunes:category>'.PHP_EOL);
print('  <itunes:explicit>'. $explicit .'</itunes:explicit>'.PHP_EOL.PHP_EOL);


if( $wb_ent_options['vidshortcode']['enabled'] ){
   $query = sprintf("SELECT p.ID,p.post_title, p.post_content, p.post_date, p.post_name 
   FROM wp_posts p, wp_terms t, wp_term_taxonomy tt, wp_term_relationships tr
   WHERE t.term_id=tt.term_id
   AND tt.term_taxonomy_id=tr.term_taxonomy_id
   AND tr.object_id=p.ID   
   AND p.post_status='publish'
   AND p.post_type='post'
   AND p.post_password = ''
   AND t.term_id<>'%s'
   GROUP BY p.ID
   ORDER BY p.post_date DESC", $itunesID);   
}
else{   
   $query = sprintf("SELECT p.ID, b.media_id, p.post_title, p.post_content, p.post_date, p.post_name FROM wp_posts p, wp_terms t, wp_term_taxonomy tt, wp_term_relationships tr, wb_media b
   WHERE t.term_id=tt.term_id
   AND tt.term_taxonomy_id=tr.term_taxonomy_id
   AND tr.object_id=p.ID
   AND p.ID=b.post_id
   AND p.post_status='publish'
   AND p.post_type='post'
   AND p.post_password = ''
   AND t.term_id<>'%s'
   GROUP BY p.ID
   ORDER BY p.post_date DESC", $itunesID);
}
//echo $query;

// Perform query
$result = mysql_query($query);
while ($row = mysql_fetch_array($result)) {

   if( $wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\['.$wb_ent_options['vidshortcode']['tag'].'(.*)videoid(.*)="(.*)"(.*)mode(.*)'.$shortcodeChannel['name'].'(.*)\/\]/', $row['post_content'], $match) >= 1 ){         
      $videoid = $match[3][0];
      $row['post_content'] = removeShortCode($wb_ent_options['vidshortcode']['tag'],$row['post_content']);
      $curlString = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id='.$videoid.'&video_fields=id,name,startDate,publishedDate,length,shortDescription,longDescription,thumbnailURL,renditions,FLVURL,tags&media_delivery=http&token='.$wb_ent_options['vidshortcode']['token'];            
   }
   else if( $wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\['.$wb_ent_options['vidshortcode']['tag'].'(.*)videoid(.*)="(.*)"(.*)\/\]/', $row['post_content'], $match) >= 1 ){
      $videoid = $match[3][0];
      $row['post_content'] = removeShortCode($wb_ent_options['vidshortcode']['tag'],$row['post_content']);
      $curlString = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id='.$videoid.'&video_fields=id,name,startDate,publishedDate,length,shortDescription,longDescription,thumbnailURL,renditions,FLVURL,tags&media_delivery=http&token='.$wb_ent_options['brightcoveinfo']['readurltoken'];             
   }
   else{   
      $videoid = $row['media_id'];
      $curlString = 'http://api.brightcove.com/services/library?command=find_video_by_id&video_id='.$videoid.'&video_fields=id,name,startDate,publishedDate,length,shortDescription,longDescription,thumbnailURL,renditions,FLVURL,tags&media_delivery=http&token='.$wb_ent_options['brightcoveinfo']['readurltoken'];
   }
   
	if ($videoid && trim($videoid)!= '') {
/*
      echo '
      $postId is '.$row['ID'];
     
      echo '
      $videoid is '.$videoid;	      
   
   echo '
   $curlString is '.$curlString;	   
	   */
		$ch = curl_init();
		$timeout = 0; // set to zero for no timeout
		curl_setopt ($ch, CURLOPT_URL, $curlString );
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$file_contents = curl_exec($ch);
		curl_close($ch);
		//echo $file_contents.'<br />';

		$returndata = json_decode($file_contents);
		
		//print_r($returndata);
		$dl = array();

		$url = preg_split("/\?/", $returndata->{"FLVURL"}, -1, PREG_SPLIT_NO_EMPTY);
		$dl = array(
						'date' => $returndata->{"startDate"},
						'id' => $returndata->{"id"},
						'name' => $returndata->{"name"},
						'desc' => $returndata->{"shortDescription"},
						'url' => $url[0],
						'thumb' => $returndata->{"thumbnailURL"}
						);
		//print_r($dl);
		//print_r($returndata);
		if ($returndata->renditions) {
			foreach($returndata->renditions as $quality) {
				$vid = $quality->{"size"};
				$vidurl = $quality->{"url"};
				//print_r($quality);
				if ($quality->{"encodingRate"}==$enc) {
					$i=$vid;
					$currentvid=$vidurl;
					break;
				} if ($vid<$i) {
					$i=$vid;
					$currentvid=$vidurl;
				}
				$dl["rendition"][$quality->{"size"}] = array(
						"rate" => $quality->{"encodingRate"},
						"height" => $quality->{"frameHeight"},
						"width" => $quality->{"frameWidth"},
						"id" => $quality->{"id"},
						"size" => $quality->{"size"},
						"url" => $quality->{"url"},
						"duration" => $quality->{"videoDuration"});
			}

			ksort($dl["rendition"]); // Sort renditions by key (which is also SIZE)
			$i=0;
			foreach ($dl["rendition"] as $rendition) {
				if ($i==4) {
					$url = $rendition['url'];
					$size = $rendition['size'];
					$duration = $rendition['duration'];
					break;
				}
				$i++;
			}

			if (is_array($url)) $url = $url[0];

			$extension = substr($url, -3, 3);
			//print_r( $extension);
			if ($extension!='flv') {
				print('  <item>'.PHP_EOL);
				print('    <title>');
				print_r(normalize_special_characters($row['post_title']));//$returndata->{"name"});
				print('</title>'.PHP_EOL);

				print('    <itunes:author>');
				print($author);
				print('</itunes:author>'.PHP_EOL);

				print('    <itunes:subtitle>');
				//print_r($returndata->{"shortDescription"});
				print_r(normalize_special_characters($row['post_title']));
				print('</itunes:subtitle>'.PHP_EOL);

				print('    <itunes:summary>');
				print_r(normalize_special_characters($row['post_content']));
				print('</itunes:summary>'.PHP_EOL);

				print('    <enclosure url="');
					echo $url;
					print('" length="');
					print_r($size);
					print('" type="video/mp4"></enclosure>'.PHP_EOL);

				print('    <guid>');
				print_r('http://'.$wb_ent_options['channeldomain'].'/'.$row['post_name']);
				print('</guid>'.PHP_EOL);

				$date = $row['post_date'];
				print('    <pubDate>');
				
            //getting the pubdate to RFC822
            $postId = $row['ID'] ;
            
            $sql2 = "SELECT DATE_FORMAT(post_date,'%a, %d %b %Y %T') AS rfcpubdate 
            FROM wp_posts 
            WHERE ID = $postId "
            ;
         
            $result2 = mysql_query($sql2) ;
            
            $rfcpubdate = mysql_fetch_row($result2);
            
            foreach($rfcpubdate as $date){            
               echo "$date CST" ;            
            }				
								
				print('</pubDate>'.PHP_EOL);

				print('    <itunes:duration>');
				print_r(floor($duration/1000));
				print('</itunes:duration>'.PHP_EOL);

				print('    <itunes:keywords>');
				print_r($returndata->customFields->{"itunestags"});
				print('</itunes:keywords>'.PHP_EOL);

				print('  </item>'.PHP_EOL.PHP_EOL);
			}
		} else {
			continue;
		}
	}
	
}

print('</channel>'.PHP_EOL.'</rss>');

?>