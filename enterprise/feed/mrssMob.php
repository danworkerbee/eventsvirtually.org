<?php
/*
 Filename:      mrssMob.php
 Description:
 Author:        Workerbee TV Web Team
 Change Log:
 2017-12-15          [Mark] Removed old Brightcove API and get https sources for videostill thumbnail and mp4
 2019-01-02          [Mark] Updated db connection host
 */

$prewd = getcwd();
chdir(realpath(dirname(__FILE__)));

error_reporting(E_ALL);
include ('../../../../wp-config.php');

//get theme options
try {
	$dbh = new PDO('mysql:host='.DB_HOST.';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
	$getThemeOptions = $dbh->prepare("SELECT option_value FROM wp_options WHERE option_name = 'wb_ent_options'");
	$getThemeOptions->execute();
	$getThemeOptionsNumrows = $getThemeOptions->rowCount();
	$getThemeOptionsResult = $getThemeOptions->fetch();
	$getThemeOptions = null;
	$dbh = null;
} catch (PDOException $e) {
	echo "Error!: Could not connect to DB";
}

$wb_ent_options = unserialize($getThemeOptionsResult['option_value']);
/* static values */
$wb_exchange_category_ID 	= 0;
$wb_archived_category_ID 	= 0;
$wb_exclusived_category_ID 	= 0;
$wb_excluded_post			= "";
$wb_date_create_url			= "";
$wb_date_modified_url		= "";
$wb_sort_field				= "post_date";

$strReplaceSearch = array(
		"¡", "¢", "£", "¤", "¥", "¦", "§", "¨", "©", "ª",
		"«", "¬", "®", "¯", "°", "±", "²", "³", "´",
		"µ", "¶", "·", "¸", "¹", "º", "»", "¼", "½", "¾",
		"¿", "×", "÷", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ",
		"Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", "Ð",
		"Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û",
		"Ü", "Ý", "Þ", "ß", "à", "á", "â", "ã", "ä", "å",
		"æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï",
		"ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú",
		"û", "ü", "ý", "þ", "ÿ", "  ", "\r", "\n", "\t", "\v",
		"“", "‘", "’", "“", "”", "…", "–", "—", "“", "”", "-", " ", "•", "€", "™", "&amp;", ""
);

$strReplaceChars = array(
		"&#161;", "&#162;", "&#163;", "&#164;", "&#165;", "&#166;", "&#167;", "&#168;", "&#169;", "&#170;",
		"&#171;", "&#172;", "&#174;", "&#175;", "&#176;", "&#177;", "&#178;", "&#179;", "&#180;",
		"&#181;", "&#182;", "&#183;", "&#184;", "&#185;", "&#186;", "&#187;", "&#188;", "&#189;", "&#190;",
		"&#191;", "&#215;", "&#247;", "&#192;", "&#193;", "&#194;", "&#195;", "&#196;", "&#197;", "&#198;",
		"&#199;", "&#200;", "&#201;", "&#202;", "&#203;", "&#204;", "&#205;", "&#206;", "&#207;", "&#208;",
		"&#209;", "&#210;", "&#211;", "&#212;", "&#213;", "&#214;", "&#216;", "&#217;", "&#218;", "&#219;",
		"&#220;", "&#221;", "&#222;", "&#223;", "&#224;", "&#225;", "&#226;", "&#227;", "&#228;", "&#229;",
		"&#230;", "&#231;", "&#232;", "&#233;", "&#234;", "&#235;", "&#236;", "&#237;", "&#238;", "&#239;",
		"&#240;", "&#241;", "&#242;", "&#243;", "&#244;", "&#245;", "&#246;", "&#248;", "&#249;", "&#250;",
		"&#251;", "&#252;", "&#253;", "&#254;", "&#255;", " ", " ", " ", " ", " ",
		"\"", "'", "'", "\"", "\"", "...", "-", "-", "\"", "\"", "-", " ", " ", "", "&#153;", "&#038;", ""
);

//check if parameters are being passed
$parameters = parse_url($_SERVER['REQUEST_URI']);
if ($parameters['query'] == '') {
	header('HTTP/1.1 400 BAD REQUEST');
	$error['ERROR'] = array('Code' => '0', 'Message' => 'You are missing parameters.');
	$error = json_encode($error);
	echo $error;
	exit;
}


global $wb_ent_options, $bc_account_id, $wb_bc_auth_token_world;
$wb_bc_auth_token_world = wb_get_auth_token();
error_reporting(0);
if( !defined('DB_USER') ){
	include $_SERVER['DOCUMENT_ROOT'].'/wp-config.php';
}
$wb_ent_options = wb_get_option('wb_ent_options');
$bc_account_id = $wb_ent_options['brightcoveinfo']['publisherid'];

//sdtvrssmob09123
$hash = md5(sha1($_GET['apiKey']));

//check api key
if ($hash != md5(sha1($wb_ent_options['apiinfo']['apikey']))) {
	header('HTTP/1.1 400 BAD REQUEST');
	$error['ERROR'] = array('Code' => '00', 'Message' => 'Invalid API Key');
	$error = json_encode($error);
	echo $error;
	exit;
}

//remove shortcode
function removeShortCode($codeTag, $string) {
	return preg_replace("/\[" . $codeTag . " (.*)\/\]/i", "", $string);
}

//check output options
if (!isset($_GET['output']) || $_GET['output'] == '' || empty($_GET['output'])) {
	
	header('HTTP/1.1 400 BAD REQUEST');
	$error['ERROR'] = array('Code' => '01', 'Message' => 'Please provide an output format It can be xml or json. Example &output=json');
	$error = json_encode($error);
	echo $error;
	exit;
} else {
	$output = strtolower($_GET['output']);
	$output = strip_tags(trim($output));
}

//check limit options / must be numeric
$limit = strip_tags(trim($_GET['limit']));
if (!is_numeric($limit)) {
	
	
	header('HTTP/1.1 400 BAD REQUEST');
	if ($output == 'json') {
		$error['ERROR'] = array('Code' => '03', 'Message' => 'Limit must be Numeric, Example: &limit=10');
		$error = json_encode($error);
		echo $error;
	}
	
	if ($output == 'xml') {
		echo '<?xml version="1.0" ?><error><code>03</code><message>Limit must be Numeric, Example: &limit=10</message></error>';
	}
	exit;
}

//To show the .mp4 file url link - default is no, yes to show
$downloadable = strip_tags(trim($_GET['show_mp4']));

//To add date created (post_date) in sql query
$wb_date	= "";
if( $_GET['dateCreated'] ){
	//Check if there is date created entry
	$wb_date = $_GET['dateCreated'];
	$wb_date_test = validateDate($wb_date, 'Y-m-d');
	if ( $wb_date_test == true ){
		$wb_date_create_query = " AND date_format(p.post_date, '%Y-%m-%d' ) = '".$wb_date."'";
		$wb_date_create_url	  = "&dateCreated=".$wb_date;
		$wb_sort_field		  = "post_date";
	}else{
		header('HTTP/1.1 400 BAD REQUEST');
		if ($output == 'json') {
			$error['ERROR'] = array('Code' => '02', 'Message' => 'Invalid date create format');
			$error = json_encode($error);
			echo $error;
		}
		
		if ($output == 'xml') {
			echo '<?xml version="1.0" ?><error><code>02</code><message>Invalid date create format</message></error>';
		}
		exit;
	}
}

//To add date last modified (post_modified) in sql query
$wb_date_modified	= "";
if( $_GET['dateModified'] ){
	//Check if there is date created entry
	$wb_date_modified = $_GET['dateModified'];
	$wb_date_test_modified = validateDate($wb_date_modified, 'Y-m-d');
	if ( $wb_date_test_modified == true ){
		$wb_date_create_query_modified = " AND date_format(p.post_modified, '%Y-%m-%d' ) = '".$wb_date_modified."'";
		$wb_date_modified_url = "&dateModified=".$wb_date_modified;
		$wb_sort_field		  = "post_modified";
	}else{
		header('HTTP/1.1 400 BAD REQUEST');
		if ($output == 'json') {
			$error['ERROR'] = array('Code' => '02', 'Message' => 'Invalid date modified format');
			$error = json_encode($error);
			echo $error;
		}
		
		if ($output == 'xml') {
			echo '<?xml version="1.0" ?><error><code>02</code><message>Invalid date modified format</message></error>';
		}
		exit;
	}
}

/* post date created FROM and TO */
if ( isset( $_GET['dateCreatedFrom'] ) && isset( $_GET['dateCreatedTo'] ) ){
	$wb_date_create_query			= "";
	
	$wb_date_created_from 	= $_GET['dateCreatedFrom'];
	$wb_date_created_to 	= $_GET['dateCreatedTo'];
	if ( validateDate($wb_date_created_from, 'Y-m-d') == true && validateDate($wb_date_created_to, 'Y-m-d') == true ){
		//echo "date valid ";
	}else{
		header('HTTP/1.1 400 BAD REQUEST');
		if ($output == 'json') {
			$error['ERROR'] = array('Code' => '02', 'Message' => 'Invalid date created format');
			$error = json_encode($error);
			echo $error;
		}
		
		if ($output == 'xml') {
			echo '<?xml version="1.0" ?><error><code>02</code><message>Invalid date format</message></error>';
		}
		exit;
	}
	
	if ( $wb_date_created_from > $wb_date_created_to ){
		header('HTTP/1.1 400 BAD REQUEST');
		if ($output == 'json') {
			$error['ERROR'] = array('Code' => '02', 'Message' => 'Invalid date created range');
			$error = json_encode($error);
			echo $error;
		}
		
		if ($output == 'xml') {
			echo '<?xml version="1.0" ?><error><code>02</code><message>Invalid date created range </message></error>';
		}
		exit;
	}
	$wb_date_create_query_modified	= "";
	$wb_date_create_query	= " AND date_format(p.post_date, '%Y-%m-%d' ) >= '".$wb_date_created_from."' AND date_format(p.post_date, '%Y-%m-%d' ) <= '".$wb_date_created_to."'";
	$wb_date_create_url		= "&dateCreatedFrom=".$wb_date_created_from."&dateCreatedTo=".$wb_date_created_to;
	$wb_sort_field		  	= "post_date";
}
/* post date created FROM and TO */


/* post date modified FROM and TO */
if ( isset( $_GET['dateModifiedFrom'] ) && isset( $_GET['dateModifiedTo'] ) ){
	$wb_date_create_query_modified 	= "";
	$wb_date_modified_from 	= $_GET['dateModifiedFrom'];
	$wb_date_modified_to 	= $_GET['dateModifiedTo'];
	if ( validateDate($wb_date_modified_from, 'Y-m-d') == true && validateDate($wb_date_modified_to, 'Y-m-d') == true ){
		//echo "date valid ";
	}else{
		header('HTTP/1.1 400 BAD REQUEST');
		if ($output == 'json') {
			$error['ERROR'] = array('Code' => '02', 'Message' => 'Invalid date modified format');
			$error = json_encode($error);
			echo $error;
		}
		
		if ($output == 'xml') {
			echo '<?xml version="1.0" ?><error><code>02</code><message>Invalid date format</message></error>';
		}
		exit;
	}
	
	if ( $wb_date_modified_from > $wb_date_modified_to ){
		header('HTTP/1.1 400 BAD REQUEST');
		if ($output == 'json') {
			$error['ERROR'] = array('Code' => '02', 'Message' => 'Invalid date modified range');
			$error = json_encode($error);
			echo $error;
		}
		
		if ($output == 'xml') {
			echo '<?xml version="1.0" ?><error><code>02</code><message>Invalid date modified range </message></error>';
		}
		exit;
	}
	//$wb_date_create_query			= "";
	$wb_date_create_query_modified	= " AND date_format(p.post_modified, '%Y-%m-%d' ) >= '".$wb_date_modified_from."' AND date_format(p.post_modified, '%Y-%m-%d' ) <= '".$wb_date_modified_to."'";
	$wb_date_modified_url	= "&dateModifiedFrom=".$wb_date_modified_from."&dateModifiedTo=".$wb_date_modified_to;
	$wb_sort_field		  	= "post_modified";
}
/* post date modified FROM and TO */

//check the sort options
if (isset($_GET['sort'])) {
	$sort = strip_tags(trim($_GET['sort']));
	$sort = strtolower($sort);
	
	if (($sort == 'asc') || ($sort == 'desc')) {
		$sort = "ORDER BY p.".$wb_sort_field." " . strtolower($sort);
	} else {
		
		header('HTTP/1.1 400 BAD REQUEST');
		if ($output == 'json') {
			$error['ERROR'] = array('Code' => '02', 'Message' => 'You can only use ASC/DESC');
			$error = json_encode($error);
			echo $error;
		}
		
		if ($output == 'xml') {
			echo '<?xml version="1.0" ?><error><code>02</code><message>You can only use ASC/DESC</message></error>';
		}
		exit;
	}
} else {
	$sort = 'ORDER BY p.'.$wb_sort_field.' DESC';
}

//if result is with member ID
$memberid = $_GET['memberid'];
if( $memberid != "" ){
	if( is_numeric( $memberid ) ){
		$member_id_query = " AND p.post_author=".$memberid;
	}else{
		header('HTTP/1.1 400 BAD REQUEST');
		if ($output == 'json') {
			$error['ERROR'] = array('Code' => '03', 'Message' => 'Member ID must be Numeric, Example: &memberid=1');
			$error = json_encode($error);
			echo $error;
		}
		exit();
	}
}

//Get the number of post per page to activate pagination
$paging = "no";
if( $_GET['paging'] !="" ){
	$paging = strip_tags(trim($_GET['paging']));
}

$page_current = ( isset($_GET['page']) && is_numeric($_GET['page']) ) ? ($_GET['page']) : 1;
$page_limit = $limit; // include with the URL get

$limit_string = '';
$offset_string = '';
//cho "<br />".$_SERVER[HTTP_HOST]."<br />";
//paging link
$next_page_url = isset($_SERVER['HTTPS']) ? "https" : "http";
$next_page_url = $next_page_url . "://" . $_SERVER[HTTP_HOST] . "/wp-content/themes/enterprise/feed/mrssMob.php";
$next_page_url = $next_page_url . "?apiKey=" . $_GET['apiKey'];
$next_page_url = $next_page_url . "&output=" . $output;
$next_page_url = $next_page_url . "&limit=" . $limit;

$next_page_url = $next_page_url . $wb_date_create_url;
$next_page_url = $next_page_url . $wb_date_modified_url;

if( isset($_GET['sort'] ) ){
	$next_page_url = $next_page_url . "&sort=" . $_GET['sort'];
}
if( $downloadable != "" ){
	if( $downloadable == "yes" ){
		$next_page_url = $next_page_url . "&show_mp4=yes";
	}elseif($downloadable == "no"){
		$next_page_url = $next_page_url . "&show_mp4=no";
	}
	
}

//if( $page_current != 'all' && trim($_GET['page']) != 'all' ){
if( $paging == "yes" ){
	$offset = ( $page_current - 1 )  * $page_limit;
	$limit_string = ' LIMIT '.$page_limit;
	$offset_string = ' OFFSET '.$offset;
	$next_page_url = $next_page_url . "&paging=1";
}else{
	$limit_string = ' LIMIT '.$page_limit;
}
//}

//check call function
if (!isset($_GET['callFunction']) || $_GET['callFunction'] == '' || empty($_GET['callFunction'])) {
	$callFunction = "empty";
}else {
	$callFunction = strip_tags(trim($_GET['callFunction']));
}


//get private && unlisted videos
$unlistedAndPrivateVids = array();

try {
	$dbh = new PDO('mysql:host='.DB_HOST.';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
	
	
	$catUnlisted = isset($wb_ent_options['videocats']['unlisted']) ? $wb_ent_options['videocats']['unlisted'] : 0;
	$catPrivate = isset($wb_ent_options['videocats']['private']) ? $wb_ent_options['videocats']['private'] : 0;
	$catHowTo = isset($wb_ent_options['videocats']['howto']) ? $wb_ent_options['videocats']['howto'] : 0;
	
	if ($wb_ent_options['vidshortcode']['enabled']) {
		$getPrivateUnlistedPosts = $dbh->prepare("
         SELECT p.ID, p.post_title, p.post_name, p.post_content
            FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
            WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
            AND tt.term_id=t.term_id
            AND tr.object_id=p.ID
            AND p.post_status='publish'
            AND ( t.term_id=?
            OR t.term_id=? OR t.term_id=? )
            GROUP BY p.ID
            ORDER BY p.post_date DESC
      ");
		
		$getPrivateUnlistedPosts->bindParam(1, $catUnlisted);
		$getPrivateUnlistedPosts->bindParam(2, $catPrivate);
		$getPrivateUnlistedPosts->bindParam(3, $catHowTo);
	} else {
		$getPrivateUnlistedPosts = $dbh->prepare("
         SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content
         FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media b
         WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
         AND tt.term_id=t.term_id
         AND tr.object_id=p.ID
         AND p.ID=b.post_id
         AND p.post_status='publish'
         AND ( t.term_id=?
         OR t.term_id=? OR t.term_id=? )
         GROUP BY p.ID
         ORDER BY p.post_date DESC
      ");
		
		$getPrivateUnlistedPosts->bindParam(1, $catUnlisted);
		$getPrivateUnlistedPosts->bindParam(2, $catPrivate);
		$getPrivateUnlistedPosts->bindParam(3, $catHowTo);
	}
	
	$getPrivateUnlistedPosts->execute();
	$getPrivateUnlistedPostsNumrows = $getPrivateUnlistedPosts->rowCount();
	$getPrivateUnlistedPostsResult = $getPrivateUnlistedPosts->fetchAll();
	
	$getPrivateUnlistedPosts = null;
	$dbh = null;
	
} catch (PDOException $e) {
	echo "Error!: Could not connect to DB";
}

foreach ($getPrivateUnlistedPostsResult as $currentPrivateUnlistedPost) {
	$unlistedAndPrivateVids[] = $currentPrivateUnlistedPost['ID'];
}

if( $unlistedAndPrivateVids ){
	$wb_excluded_post = implode (", ", $unlistedAndPrivateVids);
	$wb_exclude_post_query = "AND p.ID NOT IN (" . $wb_excluded_post . ")";
}

$metaQuery = "";
if (!$wb_ent_options['apiinfo']['displayall']) {
	$metaQuery = "
          AND p.ID = pm.post_id
          AND pm.meta_key = 'mobile_option_RSS'";
}

$metaQuery = "
          AND p.ID = pm.post_id
          AND pm.meta_key = 'mobile_option_RSS'";

try {
	$dbh = new PDO('mysql:host='.DB_HOST.';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
	
	$time00 = date("h:i:sa");
	
	//setting the query
	if (isset($_GET['url'])) {
		$myProtocol = mySSL();
		$serverProtocol = "http://";
		if($myProtocol==1){
			$serverProtocol = "https://";
		}
		
		define( 'siteProtocol', $serverProtocol );
		
		$stringLenght = strlen($_GET['url']);
		if ($stringLenght > 300) {
			header('HTTP/1.1 400 BAD REQUEST');
			if ($output == 'json') {
				$error['ERROR'] = array('Code' => '04', 'Message' => 'You have reached the maximum number of characters. Limit is 300');
				$error = json_encode($error);
				echo $error;
			}
			
			if ($output == 'xml') {
				echo '<?xml version="1.0" ?><error><code>04</code><message>You have reached the maximum number of characters. Limit is 300</message></error>';
			}
			exit;
		}
		
		$url = '%' . strip_tags(trim($_GET['url'])) . '%';
		$url_remove =  siteProtocol . $_SERVER['HTTP_HOST'] . "/";
		
		$url = str_replace($url_remove, '', $url);
		$url = str_replace('https://', '', $url);
		$url = str_replace('http://', '', $url);
		$url = str_replace('://', '', $url);
		$url = str_replace('/', '', $url);
		$url = str_replace($_SERVER['HTTP_HOST'], '', $url);
		
		if ($wb_ent_options['vidshortcode']['enabled']) {
			$getPosts = $dbh->prepare("
					SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content, p.post_password, pm.meta_value, date_format(p.post_modified, '%Y-%m-%d' ) as post_modified, date_format(p.post_date, '%Y-%m-%d' ) as post_date, p.post_status, p.post_password
					FROM wp_posts p, wp_postmeta pm
					WHERE p.post_type = 'post'
					$metaQuery
					$member_id_query
					$wb_exclude_post_query
					$wb_date_create_query
					$wb_date_create_query_modified
					AND p.post_name LIKE ?
					AND p.post_status IN ('publish','private')
					GROUP BY p.ID
					$sort
					LIMIT 200
					");
					
					$getPostsNoPaging = $dbh->prepare("
							SELECT p.ID
							FROM wp_posts p, wb_media b, wp_postmeta pm
							WHERE p.post_type = 'post'
							$metaQuery
							$member_id_query
							$wb_exclude_post_query
							$wb_date_create_query
							$wb_date_create_query_modified
							AND p.post_name LIKE ?
							AND p.post_status IN ('publish','private')
							GROUP BY p.ID
							");
		} else {
			$getPosts = $dbh->prepare("
					SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content, p.post_password, pm.meta_value, date_format(p.post_modified, '%Y-%m-%d' ) as post_modified, date_format(p.post_date, '%Y-%m-%d' ) as post_date, p.post_status, p.post_password
					FROM wp_posts p, wb_media b, wp_postmeta pm
					WHERE p.post_type = 'post'
					$metaQuery
					$member_id_query
					$wb_exclude_post_query
					$wb_date_create_query
					$wb_date_create_query_modified
					AND p.ID = b.post_id
					AND p.post_name LIKE ?
					AND p.post_status IN ('publish','private')
					GROUP BY p.ID
					$sort
					$limit_string $offset_string
					");
					
					$getPostsNoPaging = $dbh->prepare("
							SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content, p.post_password, pm.meta_value, p.post_modified, p.post_date, p.post_status, p.post_password
							FROM wp_posts p, wb_media b, wp_postmeta pm
							WHERE p.post_type = 'post'
							$metaQuery
							$member_id_query
							$wb_exclude_post_query
							$wb_date_create_query
							$wb_date_create_query_modified
							AND p.ID=b.post_id
							AND p.post_name LIKE ?
							AND p.post_status IN ('publish','private')
							GROUP BY p.ID
							");
		}
		
		$getPosts->bindParam(1, $url);
		$getPostsNoPaging->bindParam(1, $url);
		$next_page_url = $next_page_url . "&url=" . $_GET['url'];
		
	}elseif (isset($_GET['q'])) {
		$stringLenght = strlen($_GET['q']);
		if ($stringLenght > 64) {
			
			header('HTTP/1.1 400 BAD REQUEST');
			if ($output == 'json') {
				$error['ERROR'] = array('Code' => '04', 'Message' => 'You have reached the maximum number of characters. Limit is 64');
				$error = json_encode($error);
				echo $error;
			}
			
			if ($output == 'xml') {
				echo '<?xml version="1.0" ?><error><code>04</code><message>You have reached the maximum number of characters. Limit is 64</message></error>';
			}
			exit;
		}
		
		
		$q = '%' . strip_tags(trim($_GET['q'])) . '%';
		$next_page_url = $next_page_url . "&q=" . $_GET['q'];
		
		if ($wb_ent_options['vidshortcode']['enabled']) {
			$getPosts = $dbh->prepare("
					SELECT p.ID, p.post_title, p.post_name, p.post_content, p.post_password, pm.meta_value, date_format(p.post_modified, '%Y-%m-%d' ) as post_modified, date_format(p.post_date, '%Y-%m-%d' ) as post_date, p.post_status, p.post_password
					FROM wp_posts p, wp_postmeta pm
					WHERE p.post_type = 'post'
					$metaQuery
					$member_id_query
					$wb_date_create_query
					$wb_date_create_query_modified
					$wb_exclude_post_query
					AND (p.post_status='publish')
					AND p.post_title LIKE ?
					AND p.post_status = 'publish'
					GROUP BY p.ID
					$sort
					$limit_string $offset_string
					");
					
					$getPostsNoPaging = $dbh->prepare("
							SELECT p.ID, p.post_title, p.post_name, p.post_content, p.post_password, pm.meta_value, p.post_modified, p.post_date, p.post_status, p.post_password
							FROM wp_posts p, wp_postmeta pm
							WHERE p.post_type = 'post'
							$metaQuery
							$member_id_query
							$wb_date_create_query
							$wb_date_create_query_modified
							$wb_exclude_post_query
							AND (p.post_status='publish')
							AND p.post_title LIKE ?
							AND p.post_status = 'publish'
							GROUP BY p.ID
							");
							
		} else {
			$getPosts = $dbh->prepare("
					SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content, p.post_password, pm.meta_value, date_format(p.post_modified, '%Y-%m-%d' ) as post_modified, date_format(p.post_date, '%Y-%m-%d' ) as post_date, p.post_status, p.post_password
					FROM wp_posts p, wb_media b, wp_postmeta pm
					WHERE p.post_type = 'post'
					$metaQuery
					$member_id_query
					$wb_date_create_query
					$wb_date_create_query_modified
					$wb_exclude_post_query
					AND p.ID=b.post_id
					AND p.post_title LIKE ?
					AND p.post_status IN ('publish','private')
					GROUP BY p.ID
					$sort
					$limit_string  $offset_string
					");
					
					
					$getPostsNoPaging = $dbh->prepare("
							SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content, p.post_password, pm.meta_value, p.post_modified, p.post_date, p.post_status, p.post_password
							FROM wp_posts p, wb_media b, wp_postmeta pm
							WHERE p.post_type = 'post'
							$metaQuery
							$member_id_query
							$wb_date_create_query
							$wb_date_create_query_modified
							$wb_exclude_post_query
							AND p.ID=b.post_id
							AND p.post_title LIKE ?
							AND p.post_status IN ('publish','private')
							GROUP BY p.ID
							");
		}
		$getPosts->bindParam(1, $q);
		$getPostsNoPaging->bindParam(1, $q);
		
	} else {
		if ($wb_ent_options['vidshortcode']['enabled']) {
			$getPosts = $dbh->prepare("
					SELECT p.ID, p.post_title, p.post_name, p.post_content, p.post_password, pm.meta_value, date_format(p.post_modified, '%Y-%m-%d' ) as post_modified, date_format(p.post_date, '%Y-%m-%d' ) as post_date, p.post_status, p.post_password
					FROM wp_posts p, wp_postmeta pm
					WHERE p.post_type = 'post'
					$metaQuery
					$member_id_query
					AND (p.post_status='publish')
					AND p.post_status = 'publish'
					$wb_date_create_query
					$wb_date_create_query_modified
					$wb_exclude_post_query
					AND p.post_password = ''
					GROUP BY p.ID
					$sort
					$limit_string  $offset_string
					");
					$getPostsNoPaging = $dbh->prepare("
							SELECT p.ID, p.post_title, p.post_name, p.post_content, p.post_password, pm.meta_value, p.post_modified, p.post_date, p.post_status, p.post_password
							FROM wp_posts p, wp_postmeta pm
							WHERE post_type = 'post'
							$metaQuery
							$member_id_query
							$wb_date_create_query
							$wb_date_create_query_modified
							$wb_exclude_post_query
							AND (p.post_status='publish')
							AND p.post_status = 'publish'
							AND p.post_type = 'post'
							AND p.post_password = ''
							GROUP BY p.ID
							$limit_string  $offset_string
							");
		} else {
			$getPosts = $dbh->prepare("
					SELECT p.ID, b.media_id, b.media_thumb, b.src, p.post_title, p.post_name, p.post_content, p.post_password, pm.meta_value, date_format(p.post_modified, '%Y-%m-%d' ) as post_modified, date_format(p.post_date, '%Y-%m-%d' ) as post_date, p.post_status, p.post_password
					FROM wp_posts p, wb_media b, wp_postmeta pm
					WHERE p.post_type = 'post'
					AND b.media_id <> ''
					$wb_exclude_post_query
					$metaQuery
					$member_id_query
					AND p.ID=b.post_id
					AND p.post_status IN ('publish','private')
					$wb_date_create_query
					$wb_date_create_query_modified
					GROUP BY p.ID
					$sort
					$limit_string $offset_string
					");
					
					$getPostsNoPaging = $dbh->prepare("
							SELECT p.ID
							FROM wp_posts p, wb_media b, wp_postmeta pm
							WHERE p.post_type = 'post'
							AND b.media_id <> ''
							$wb_exclude_post_query
							$metaQuery
							$member_id_query
							$wb_date_create_query
							$wb_date_create_query_modified
							AND p.ID=b.post_id
							AND p.post_status IN ('publish','private')
							GROUP BY p.ID
							");
		}
	}
	
	$getPosts->execute();
	$getPostsNumrows = $getPosts->rowCount();
	$result = $getPosts->fetchAll();
	$getPostsNoPaging->execute();
	$getPostsNumrowsNoPaging = $getPostsNoPaging->rowCount();
	$wb_get_bc_ctr = 1;
	$wb_video_id_set_string = array();
	$wb_video_id_set_string_comma = "";
	for( $x=0;$x < count($result); $x++ ){
		$permalink = get_permalink($result[$x]['ID']);
		array_push($result[$x]['permalink'] = $permalink);
		
		array_push($wb_video_id_set_string, $result[$x]['media_id']);
		if ( $wb_get_bc_ctr == 10 || ( ( $x + 1 ) == count( $result ) )  || count( $result ) == 1 ){
			$wb_video_id_set_string_comma = implode(",",$wb_video_id_set_string);
			$wb_bc_information = json_decode(wb_get_video_information($wb_video_id_set_string_comma), true);
			
			if( count( $result ) == 1){
				array_push($result[$x]['poster'] = $wb_bc_information["images"]["poster"]["src"]);
				array_push($result[$x]['thumbnail'] = $wb_bc_information["images"]["thumbnail"]["src"]);
			}else{
				foreach ($wb_bc_information as $wb_current_bc_information ){
					for( $y=0; $y < count($result); $y++ ){
						if( $result[$y]['media_id'] == $wb_current_bc_information["id"] ){
							
							$wb_https_image_link = "";
							foreach ( $wb_current_bc_information['images']['poster']['sources'] as $wb_current_image_source ){
								$wb_pos = substr($wb_current_image_source['src'], 0, 5);
								if ( $wb_pos == "https" ){
									$wb_https_image_link = $wb_current_image_source['src'];
								}
							}
							
							if ( $wb_https_image_link != "" ){
								array_push($result[$y]['poster'] = wb_remove_url_arguments($wb_https_image_link));
							}else{
								array_push($result[$y]['poster'] = wb_remove_url_arguments($wb_current_bc_information["images"]["poster"]["src"]));
							}
							
							
							
							$wb_https_image_link = "";
							foreach ( $wb_current_bc_information['images']['thumbnail']['sources'] as $wb_current_image_source ){
								$wb_pos = substr($wb_current_image_source['src'], 0, 5);
								if ( $wb_pos == "https" ){
									$wb_https_image_link = $wb_current_image_source['src'];
								}
							}
							
							if ( $wb_https_image_link != "" ){
								array_push($result[$y]['thumbnail'] = wb_remove_url_arguments($wb_https_image_link));
							}else{
								array_push($result[$y]['thumbnail'] = wb_remove_url_arguments($wb_current_bc_information["images"]["poster"]["src"]));
							}
							break;
						}
					}
				}
			}
			
			unset($wb_video_id_set_string);
			$wb_video_id_set_string = array();
			$wb_get_bc_ctr = 1;
			$wb_video_id_set_string_comma = "";
		} else {
			$wb_get_bc_ctr++;
		}
		
		if( $downloadable == "yes" && $result[$x]['src'] == "" ){
			$wb_video_source = json_decode( wb_get_video_source( $result[$x]['media_id'] ), true );
			$current_video_size = 0;
			$wb_video_size = 0;
			
			$wb_last_video_size = 0;
			foreach ( $wb_video_source as $wb_current_source ){
				if( $wb_current_source['size'] > 0 && $wb_current_source['container'] == "MP4" && intval($wb_last_video_size) < intval($wb_current_source['size']) && $wb_current_source['src'] !="" && strrpos( $wb_current_source['src'],'https' ) !== false ){
					$wb_video_duration = $wb_current_source['duration'];
					$wb_video_source_link = $wb_current_source['src'];
					$wb_video_size = $wb_current_source['size'];
					$wb_last_video_size = $wb_video_size;
				}
				
				//this will get the highest non 640x360 video mp4 available and will serve as video source fallback
				if( $wb_current_source['container'] == "MP4"){
					$wb_fallback_video_source_link = $wb_current_source['src'];
					$wb_fallback_video_duration = $wb_current_source['duration'];
					$wb_fallback_video_size = $wb_current_source['size'];
				}
			}
			
			if( $wb_video_source_link == "" ){
				$wb_video_duration = $wb_fallback_video_duration;
				$wb_video_source_link = $wb_fallback_video_source_link;
				$wb_video_size = $wb_fallback_video_size;
			}
			
			$seconds = $wb_video_duration;
			array_push($result[$x]['mp4_source'] = $wb_video_source_link);
			array_push($result[$x]['mp4_duration'] = $wb_video_duration);
			array_push($result[$x]['mp4_size'] = $wb_video_size);
		}
		
		$categories_id_arr = wp_get_post_categories($result[$x]['ID']);
		$categories = array();
		$cat_ctr = 0;
		
		$wb_in_exchange = "no";
		$wb_isArchive = "no";
		$wb_isExclusive = "no";
		
		foreach( $categories_id_arr as $current_cat ){
			
			if( $wb_ent_options['videocats']['library'] 	!= $current_cat &&
					$wb_ent_options['videocats']['feature'] 	!= $current_cat &&
					$wb_ent_options['videocats']['webcast'] 	!= $current_cat &&
					$wb_ent_options['videocats']['playlist']	!= $current_cat &&
					$wb_ent_options['videocats']['private'] 	!= $current_cat &&
					$wb_ent_options['videocats']['unlisted'] 	!= $current_cat
					){
						$cat = get_category( $current_cat );
						$url_client= "http://";
						if (isset($_SERVER['HTTPS'])) {
							if ($_SERVER['HTTPS'] == 1) {
								$url_client= "https://";
							} elseif ($_SERVER['HTTPS'] == 'on') {
								$url_client= "https://";
							}
						} elseif ($_SERVER['SERVER_PORT'] == HTTPS_PORT) {
							$url_client= "https://";
						}
						
						$url = $url_client .  $_SERVER['HTTP_HOST'] . "?cat=" . $current_cat;
						
						$categories[] = array(
								"category" => $cat_ctr,
								"name" => $cat->name,
								"url" => $url
						);
						
						if ( $current_cat == $wb_exchange_category_ID ){
							$wb_in_exchange = "yes";
						}
						
						if ( $current_cat == $wb_archived_category_ID ){
							$wb_isArchive = "yes";
						}
						
						if ( $current_cat == $wb_exclusived_category_ID ){
							$wb_isExclusive = "yes";
						}
						
						$cat_ctr++;
			}
		}
		if( $result[$x]['post_password'] == "" ){
			array_push($result[$x]['privacy_type']=$result[$x]['post_status']);
		}else{
			array_push($result[$x]['privacy_type']="private");
		}
		//array_push($result[$x]['in_exchange'] 	= $wb_in_exchange);
		//array_push($result[$x]['isArchive'] 	= $wb_isArchive);
		//array_push($result[$x]['isExclusive'] 	= $wb_isExclusive);
		
		array_push($result[$x]['categories'] 	= $categories);
		unset($categories);
	}
	
	$last_page = $getPostsNumrowsNoPaging / $page_limit;
	if ( $paging == "yes"){
		$previous_page_url = $next_page_url;
		if( $page_current == "1"){
			$previous_page_url = "";
		}else{
			$previous_page_url = $previous_page_url . "&page=" . ( $page_current - 1 );
		}
		if( $page_current < $last_page ){
			$next_page_url = $next_page_url . "&page=" . ( $page_current + 1 );
		}else{
			$next_page_url = "";
		}
	}
	
	unset($wb_video_id_set_string);
	$getPostsNoPaging = null;
	$getPosts = null;
	$dbh = null;
} catch (PDOException $e) {
	echo "Error!: Could not connect to DB";
}

$postCount = 0;
$wb_current_page = 1;
// json output
if ($output == 'json') {
	foreach ($result as $row) {
		if (in_array($row['ID'], $unlistedAndPrivateVids)) {
			//continue; //commenting this will include the unlisted published post
		}
		$jsonValues = json_decode($row['meta_value']);
		if ($jsonValues->pushMobile == 1 || $wb_ent_options['apiinfo']['displayall']) {
			//if( $postCount >= $limit ){
			//   break;
			//}
			$mediaId = $row['media_id'];
			
			if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)mode(.*)' . $wb_ent_options['vidshortcode']['name'] . '(.*)\/\]/', $row['post_content'], $match) >= 1) {
				//echo 'inside first if';
				//echo 'match is '.print_r($match, true);
				$mediaId = $match[3][0];
				$row['post_content'] = removeShortCode($wb_ent_options['vidshortcode']['tag'], $row['post_content']);
			} else if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)\/\]/', $row['post_content'], $match) >= 1) {
				//echo 'inside second if';
				//echo 'match is '.print_r($match, true);
				$mediaId = $match[3][0];
				$row['post_content'] = removeShortCode($wb_ent_options['vidshortcode']['tag'], $row['post_content']);
			} else {
				//echo 'inside third if';
				$mediaId = $row['media_id'];
			}
			
			$post_title = str_replace($strReplaceSearch, $strReplaceChars, $row['post_title']);
			$post_title = strip_tags($post_title);
			//removeShortCode
			$post_content = removeShortCode($wb_ent_options['vidshortcode']['tag'], $post_content);
			$post_content = str_replace($strReplaceSearch, $strReplaceChars, $row['post_content']);
			$post_content = str_replace($strReplaceSearch, $strReplaceChars, $post_content);
			$post_content = strip_tags($post_content);
			//removeShortCode
			$jsonResultArray['post_' . $postCount]['id'] 			= $row['ID'];
			$jsonResultArray['post_' . $postCount]['title'] 		= $post_title;
			$jsonResultArray['post_' . $postCount]['description'] 	= $post_content;
			$jsonResultArray['post_' . $postCount]['thumbnail'] 	= $row['thumbnail'];
			$jsonResultArray['post_' . $postCount]['videoStill'] 	= $row['poster'];
			$jsonResultArray['post_' . $postCount]['permalink'] 	= $row['permalink'];
			$jsonResultArray['post_' . $postCount]['media_id'] 		= $mediaId;
			if(trim($downloadable) == 'yes'){
				$jsonResultArray['post_' . $postCount]['mp4'] 		= $row['mp4_source'];
			}
			$jsonResultArray['post_' . $postCount]['dateCreated'] 	= $row['post_date'];
			$jsonResultArray['post_' . $postCount]['dateModified'] 	= $row['post_modified'];
			$jsonResultArray['post_'.$postCount]['privacyType'] 	= $row['privacy_type'];
			//is YPO Exchange Archive Exclusive
			/*this are not included in aacte response
			 $jsonResultArray['post_' . $postCount]['in_exchange'] 	= $row['in_exchange'];
			 $jsonResultArray['post_' . $postCount]['isArchive'] 	= $row['isArchive'];
			 $jsonResultArray['post_' . $postCount]['isExclusive'] 	= $row['isExclusive'];
			 */
			//$jsonResultArray['post_' . $postCount]['isExclusive'] 	= "yes";
			
			$jsonResultArray['post_' . $postCount]['categories'] 	= $row['categories'];
			
			$postCount++;
		}
		
		//$jsonResultArray['post_count'] = $postCount;
	}//en while
	//check if there are result to show and display
	if (empty($jsonResultArray)) {
		header('HTTP/1.1 400 BAD REQUEST');
		$error['ERROR'] = array('Code' => '05', 'Message' => 'No posts were found with your search critiria');
		$error = json_encode($error);
		echo $error;
	} else if($callFunction != "empty"){
		header('HTTP/1.1 200');
		$jsonResultArray = json_encode($jsonResultArray);
		if (function_exists($callFunction)) {
			$callFunction($jsonResultArray);
		}
		else {
			echo "'$callFunction' is not available.<br />\n";
		}
	}
	else {
		header('HTTP/1.1 200');
		header('Content-Type: application/json');
		$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		//echo $actual_link;
		//echo "<br />current page " . $page_current;
		//echo "last page " . ceil($last_page) . "<br />" . $last_page;
		$jsonResultArray['total_results'] 	= $getPostsNumrowsNoPaging;
		if ( $paging == "yes"){
			$jsonResultArray['next_page'] 		= $next_page_url;
			$jsonResultArray['previous_page'] 	= $previous_page_url;
		}
		$jsonResultArray 					= json_encode($jsonResultArray);
		echo $jsonResultArray;
	}
	
	
}//end JSON results
//


function myDefaultFunction($jsonArrayInfo){
	echo "This is the default function <br /> ";
	//echo "myFunction($jsonArrayInfo)";
	
	$arrayPostAll = array();
	$arrayinfo = explode("},", $jsonArrayInfo);
	foreach ($arrayinfo as $info => $key){
		$array_post = explode(":{", $key);
		$remove =  array('"', '{', '}');
		$array_key = str_replace($remove, '', $array_post[0]);
		$array_content_fixed = explode('","', $array_post[1]);
		$arrayPostAll[$array_key] = process_array($array_content_fixed);;
	}
	
	echo "Array post <br />";
	
	displayArray($arrayPostAll);
	
} // end of myDefaultFunction

function process_array($arrayContent){
	$arrayFinal = array();
	$maxItems = count($arrayContent);
	for($i=0; $i<$maxItems; $i++){
		$array = explode('":', $arrayContent[$i]);
		$arraykey = str_replace('"', '', $array[0]);
		$arraycnt = str_replace('"', '', $array[1]);
		$arraycnt = str_replace('\/', '/', $arraycnt);
		$arrayFinal[$arraykey]= $arraycnt;
	}
	return $arrayFinal;
	
}

function displayArray($array){?>
<link rel="stylesheet" href="<?php echo get_site_url(); ?>/wp-content/plugins/workerbee-live-lms-ppv-v2/css/wb-live-lms-ppv-bootstrap-theme.css?v=<?= time(); ?>" type="text/css" media="all" />
<link rel="stylesheet" href="<?php echo get_site_url(); ?>/wp-content/plugins/workerbee-live-lms-ppv-v2/css/wb-live-lms-ppv-bootstrap.css?v=<?= time(); ?>" type="text/css" media="all" />        
<link rel="stylesheet" href="<?php echo get_site_url(); ?>/wp-content/themes/enterprise/css/mrss-style.css?v=<?= time(); ?>" type="text/css" media="all" />        
    <div class="container-fluid" style="max-width:990px;">
  
     <?php
    foreach($array as $arr => $key){       
        //echo $key['title']."<br />";
        ?>
 
<div class="row post_div">
     <div class="col-sx-6 col-sm-6 col-md-6 col-lg-6"><img class="img-responsive" src="<?php echo $key['videoStill']; ?>" /></div>
     <div class="col-sx-6 col-sm-6 col-md-6 col-lg-6"><h3><?php echo $key['title']; ?></h3>
    <p><?php echo $key['description']; ?></p>
     <a href="<?php echo $key['permalink']; ?>" class="btn btn-primary btn-lg " role="button">View Post</a>

     </div>
   </div>
       <?php 
    } // end of foreach
    ?>
    
</div>
<?php     
}

//xms output
if ($output == 'xml') {

    header('Content-type: application/rss+xml; charset=utf-8; ');
    ?>
	<rss version='2.0' xmlns:media='http://search.yahoo.com/mrss/'>
	
        <channel>            
            <title><?php echo $wb_ent_options['channelname']; ?></title>

            <link><![CDATA[<?php echo get_site_url(); ?>]]></link>

            <description><![CDATA[<?php echo $wb_ent_options['channelname']; ?>]]></description> 

            <?php
            foreach ($result as $row) {

                if (in_array($row['ID'], $unlistedAndPrivateVids)) {
                    //continue;
                }


                $jsonValues = json_decode($row['meta_value']);


                if ($jsonValues->pushMobile == 1 || $wb_ent_options['apiinfo']['displayall']) {
                    if( $postCount >= $limit ){
                       break;
                    }
                    

                    $mediaId = $row['media_id'];

                    if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)mode(.*)' . $wb_ent_options['vidshortcode']['name'] . '(.*)\/\]/', $row['post_content'], $match) >= 1) {
                        //echo 'inside first if';
                        //echo 'match is '.print_r($match, true);
                        $mediaId = $match[3][0];
                        $row['post_content'] = removeShortCode($wb_ent_options['vidshortcode']['tag'], $row['post_content']);
                    } else if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)\/\]/', $row['post_content'], $match) >= 1) {
                        //echo 'inside second if';
                        //echo 'match is '.print_r($match, true);                
                        $mediaId = $match[3][0];
                        $row['post_content'] = removeShortCode($wb_ent_options['vidshortcode']['tag'], $row['post_content']);
                    } else {
                        //echo 'inside third if';
                        $mediaId = $row['media_id'];
                    }
					
                    if( $row['post_password'] !="" ){
                    	$wb_privacy_type = "password protected";
                    }else{
                    	$wb_privacy_type = $row['post_status'];
                    }
                    
                    $post_title 	= str_replace($strReplaceSearch, $strReplaceChars, $row['post_title']);
                    $post_title 	= strip_tags($post_title);
                    $post_content 	= removeShortCode($wb_ent_options['vidshortcode']['tag'], $post_content);
                    $post_content 	= str_replace($strReplaceSearch, $strReplaceChars, $row['post_content']);
                    $post_content 	= str_replace($strReplaceSearch, $strReplaceChars, $post_content);
                    $post_content 	= strip_tags($post_content);
				    $permalink 		= get_permalink($row['ID']);
                    $cat_ctr 		= 0;
                    ?>

                    <item>

                        <title><![CDATA[<?php echo $post_title; ?>]]></title>

                        <link><![CDATA[<?php echo $permalink; ?>]]></link>

                        <description><![CDATA[<?php echo $post_content; ?>]]></description>

                        <guid isPermaLink='true'><![CDATA[<?php echo $row['permalink']; ?>]]></guid>
						
						<?php if(trim($downloadable) == 'yes'){ ?>
                        <media:content url='<?php echo htmlspecialchars($row['mp4_source']); ?>' fileSize='<?php echo round(($row['mp4_size']/1048576),2) . "MB"; ?>' duration='<?php echo $row['mp4_duration']; ?>' type='video/x-sgi-movie' height='360' width='640' medium='video' isDefault='true'>

                            <media:title><![CDATA[<?php echo $post_title; ?>]]></media:title>

                            <media:description><![CDATA[<?php echo $post_content; ?>]]></media:description>

                            <media:thumbnail url='<?php echo htmlspecialchars($row['thumbnail']); ?>'  height='90' width='120' />

                        </media:content>
						<?php } ?>
						<dateCreated><?php echo $row['post_date']; ?></dateCreated>
						<dateModified><?php echo $row['post_modified']; ?></dateModified>
						<privacyType><?php echo $row['privacy_type']; ?></privacyType>
						<categories>
						<?php foreach( $row['categories']as $current_cat ){?>
							<category_<?php echo $cat_ctr; ?>>
								<name><?php echo $current_cat['name']; ?></name>
								<url><?php echo htmlspecialchars($current_cat['url']); ?></url>
							</category_<?php echo $cat_ctr; ?>>
						<?php $cat_ctr++; }?></categories>
				    </item>
					
					<?php
                }
            }//end while
            ?>
		<total_results><?php echo $getPostsNumrowsNoPaging; ?></total_results>
		<?php if ( $paging == "yes"){ ?>
		<previous_page><?php echo htmlspecialchars($previous_page_url); ?></previous_page>
		<next_page><?php echo htmlspecialchars($next_page_url); ?></next_page>
		<?php } ?>
        </channel>

    </rss>
    <?php
} //end rss/xml


function mySSL()
{
	if (isset($_SERVER['HTTPS'])) {
		if ($_SERVER['HTTPS'] == 1) {
			return true;
		} elseif ($_SERVER['HTTPS'] == 'on') {
			return true;
		}
	} elseif ($_SERVER['SERVER_PORT'] == HTTPS_PORT) {
		return true;
	}
	
	return false;
}

function wb_get_option($optionName = '') {
	if (trim($optionName) != '') {
		if (function_exists(get_option)) {
			return get_option($optionName);
		} else {
			try {
				$dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
				
				$getWpOptions = $dbh->prepare("
                    SELECT option_value
                    FROM `wp_options`
                    WHERE option_name = ?
                 ");
				$getWpOptions->bindParam(1, $optionName);
				$getWpOptions->execute();
				$getWpOptionsNumrows = $getWpOptions->rowCount();
				$getWpOptionsResult = $getWpOptions->fetch();
				echo '$getWpOptionsResult is ' . print_r($getWpOptionsResult, true);
				$getWpOptions = null;
				$dbh = null;
			} catch (PDOException $e) {
				echo "Error!: Could not connect to DB";
			}
		}
	}
}

// Generate Authentication Token //
function wb_get_auth_token(){
	GLOBAL $bc_account_id;
	$data 			= array();
	$id 			= "559779e8e4b072e9641b841d"; //from settings not sure if this is needed
	$client_id     	= "07aa7b60-2e1c-4775-8bc5-e3f5220d2d8d"; //API From settings
	$client_secret 	= "6a8-bE1KqdzoAbxupjTiDpEriPI4lZHUDS2mxDfU697lWuCRJNRJlJchL-ZbUCsPmC3lj-r8uram6vpPKldFmg"; //API From settings
	$auth_string   	= "{$client_id}:{$client_secret}";
	$request       	= "https://oauth.brightcove.com/v4/access_token?grant_type=client_credentials";
	$ch            	= curl_init($request);
	curl_setopt_array($ch, array(
			CURLOPT_POST           => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_SSL_VERIFYPEER => FALSE,
			CURLOPT_USERPWD        => $auth_string,
			CURLOPT_HTTPHEADER     => array('Content-type: application/x-www-form-urlencoded'),
			CURLOPT_POSTFIELDS => $data
	));
	$response = curl_exec($ch);
	curl_close($ch);
	// Check for errors
	if ($response === FALSE) {
		die(curl_error($ch));
	}
	// Decode the response
	$responseData = json_decode($response, TRUE);
	$access_token = $responseData["access_token"];
	return $access_token;
}
//Return information both poster and thumbnail
function wb_get_video_images( $video_id ){
	GLOBAL $bc_account_id;
	global $wb_bc_auth_token_world;
	
	//$access_token = wb_get_auth_token();
	$access_token = $wb_bc_auth_token_world;
	$url = "https://cms.api.brightcove.com/v1/accounts/$bc_account_id/videos/$video_id/images";
	$header = array();
	$access = 'Authorization: Bearer ' . $access_token;
	$header[] = 'Content-Type: application/json';
	$header[] = $access;
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response= curl_exec($ch);
	curl_close($ch);
	return $response;
}

//Return information of video source
function wb_get_video_source( $video_id ){
	GLOBAL $bc_account_id;
	global $wb_bc_auth_token_world;
	
	//$access_token = wb_get_auth_token();
	$access_token = $wb_bc_auth_token_world;
	$url = "https://cms.api.brightcove.com/v1/accounts/$bc_account_id/videos/$video_id/sources";
	$header = array();
	$access = 'Authorization: Bearer ' . $access_token;
	$header[] = 'Content-Type: application/json';
	$header[] = $access;
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response= curl_exec($ch);
	curl_close($ch);
	return $response;
}

//Returns Video Information JSON
function wb_get_video_information( $video_id ){
	GLOBAL $bc_account_id;
	global $wb_bc_auth_token_world;
	
	//$access_token = wb_get_auth_token();
	$access_token = $wb_bc_auth_token_world;
	
	$url = "https://cms.api.brightcove.com/v1/accounts/$bc_account_id/videos/$video_id";
	$header = array();
	$access = 'Authorization: Bearer ' . $access_token;
	$header[] = 'Content-Type: application/json';
	$header[] = $access;
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response= curl_exec($ch);
	curl_close($ch);
	
	return $response;
}

//function to validate date entries
function validateDate($date, $format = 'Y-m-d H:i:s')
{
	$d = DateTime::createFromFormat($format, $date);
	return $d && $d->format($format) == $date;
}
?>