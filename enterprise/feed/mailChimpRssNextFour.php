<?php
/*
 Filename:      mailChimpRssNextFour.php
 Description:   Returns Most Rescent Featured Post
 Author:        WorkerbeeTV Web Team
 Change log:
 2017-12-14	[Mark] Replaced Brightcove API
 */
include ('../../../../wp-config.php');
$strReplaceSearch = array(
		"¡", "¢", "£", "¤", "¥", "¦", "§", "¨", "©", "ª",
		"«", "¬", "®", "¯", "°", "±", "²", "³", "´",
		"µ", "¶", "·", "¸", "¹", "º", "»", "¼", "½", "¾",
		"¿", "×", "÷", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ",
		"Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", "Ð",
		"Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û",
		"Ü", "Ý", "Þ", "ß", "à", "á", "â", "ã", "ä", "å",
		"æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï",
		"ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú",
		"û", "ü", "ý", "þ", "ÿ", "  ", "\r", "\n", "\t", "\v",
		"“", "‘", "’", "“", "”", "…", "–", "—", "“", "”", "-", " ", "•", "€", "™"
);
$strReplaceChars = array(
		"&#161;", "&#162;", "&#163;", "&#164;", "&#165;", "&#166;", "&#167;", "&#168;", "&#169;", "&#170;",
		"&#171;", "&#172;", "&#174;", "&#175;", "&#176;", "&#177;", "&#178;", "&#179;", "&#180;",
		"&#181;", "&#182;", "&#183;", "&#184;", "&#185;", "&#186;", "&#187;", "&#188;", "&#189;", "&#190;",
		"&#191;", "&#215;", "&#247;", "&#192;", "&#193;", "&#194;", "&#195;", "&#196;", "&#197;", "&#198;",
		"&#199;", "&#200;", "&#201;", "&#202;", "&#203;", "&#204;", "&#205;", "&#206;", "&#207;", "&#208;",
		"&#209;", "&#210;", "&#211;", "&#212;", "&#213;", "&#214;", "&#216;", "&#217;", "&#218;", "&#219;",
		"&#220;", "&#221;", "&#222;", "&#223;", "&#224;", "&#225;", "&#226;", "&#227;", "&#228;", "&#229;",
		"&#230;", "&#231;", "&#232;", "&#233;", "&#234;", "&#235;", "&#236;", "&#237;", "&#238;", "&#239;",
		"&#240;", "&#241;", "&#242;", "&#243;", "&#244;", "&#245;", "&#246;", "&#248;", "&#249;", "&#250;",
		"&#251;", "&#252;", "&#253;", "&#254;", "&#255;", " ", " ", " ", " ", " ",
		"\"", "'", "'", "\"", "\"", "...", "-", "-", "\"", "\"", "-", " ", " ", "", "&#153;"
);
//get theme options
try {
	$dbh = new PDO('mysql:host=localhost;dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
	
	$getThemeOptions = $dbh->prepare("SELECT option_value FROM wp_options WHERE option_name = 'wb_ent_options'");
	
	$getThemeOptions->execute();
	$getThemeOptionsNumrows = $getThemeOptions->rowCount();
	$getThemeOptionsResult = $getThemeOptions->fetch();
	
	$getThemeOptions = null;
	$dbh = null;
} catch (PDOException $e) {
	echo "Error!: Could not connect to DB";
}

$wb_ent_options = unserialize($getThemeOptionsResult['option_value']);
$catLibrary = $wb_ent_options['videocats']['library'];

GLOBAL $bc_account_id, $access_token;
$bc_account_id  =  $wb_ent_options['brightcoveinfo']['publisherid'];
$access_token = wb_get_auth_token();

header('Content-type: application/rss+xml; charset=utf-8');
?>
<rss version='2.0' xmlns:media='http://search.yahoo.com/mrss/' xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<atom:link href="http://<?php echo $wb_ent_options['channeldomain']; ?>/wp-content/themes/enterprise/feed/mailChimpRssNext1.php" rel="self" type="application/rss+xml" />
		<title><?php echo $wb_ent_options['channelname']; ?></title>
		<link><![CDATA[<?php echo get_site_url(); ?>]]></link>
		<description><![CDATA[<?php echo $wb_ent_options['channelname']; ?>]]></description> 
	<?php
        try {
            $dbh = new PDO('mysql:host=localhost;dbname=' . DB_NAME . '', DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
            
            if ($wb_ent_options['vidshortcode']['enabled']) {
                $getPosts = $dbh->prepare("
                    SELECT p.ID, p.post_title, p.post_name, p.post_content 
                      FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p
                      WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
                      AND tt.term_id=t.term_id
                      AND tr.object_id=p.ID
                      AND p.post_status='publish'
                      AND t.term_id=?
                      GROUP BY p.ID
                      ORDER BY p.post_date DESC
                      LIMIT 5");                
            }
            else{
                $getPosts = $dbh->prepare("
                    SELECT p.ID, b.media_id, b.media_thumb, p.post_title, p.post_name, p.post_content 
                      FROM wp_term_taxonomy tt, wp_term_relationships tr, wp_terms t, wp_posts p, wb_media b
                      WHERE tt.term_taxonomy_id = tr.term_taxonomy_id
                      AND tt.term_id=t.term_id
                      AND tr.object_id=p.ID
                      AND p.ID=b.post_id
                      AND p.post_status='publish'
                      AND t.term_id=?
                      GROUP BY p.ID
                      ORDER BY p.post_date DESC
                      LIMIT 5");                
            }

            $getPosts->bindParam(1, $catLibrary);   
            $getPosts->execute();
            $getPostsNumrows = $getPosts->rowCount();
            $getPostsResult = $getPosts->fetchAll();

            $getPosts = null;
            $dbh = null;
        } catch (PDOException $e) {
            echo "Error!: Could not connect to DB";
        }
        
        $postCounter = 0;
        //print_r($getPostsResult);
        
foreach ( $getPostsResult as $row ) {
	if( $postCounter == 0 ){
		$postCounter++;
		continue;
	}
	
	$mediaId = $row['media_id'];
	$countMedia = count($mediaId);
    //$curlString = '';

	if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)mode(.*)' . $wb_ent_options['vidshortcode']['name'] . '(.*)\/\]/', $row['post_content'], $match) >= 1) {
    	$row['mediaId'] = $match[3][0];
     	$row['post_content'] = removeShortCode($wb_ent_options['vidshortcode']['tag'],$row['post_content']);
   	} else if ($wb_ent_options['vidshortcode']['enabled'] && preg_match_all('/\[' . $wb_ent_options['vidshortcode']['tag'] . '(.*)videoid(.*)="(.*)"(.*)\/\]/', $row['post_content'], $match) >= 1) {
    	$row['mediaId'] = $match[3][0];
        $row['post_content'] = removeShortCode($wb_ent_options['vidshortcode']['tag'],$row['post_content']);
   	} else {
    	$row['mediaId'] = $row['media_id'];
   	}
    
   	$mediaId = $row['media_id'];
    
    if ( $getPostsNumrows > 0 && $mediaId != "" ) {
    	$wb_post_image_information 		= wb_get_video_images($mediaId);
        $wb_post_image_information_arr 	= json_decode($wb_post_image_information, true);
        $wb_new_api_thumbnail 			= $wb_post_image_information_arr['thumbnail']['src'];
        $wb_new_api_poster 				= $wb_post_image_information_arr['poster']['src'];
        $videoStill 					= $wb_new_api_poster;
        $wb_get_info 					= json_decode(wb_get_video_information( $mediaId));
        $videoLength 					= floor($wb_get_info->duration / 1000);  // video length
   	}
 	
   	if( $postCounter == 0 ){
		$imageLink = explode('?pubId', $videoStill);
   	}else{
   		$imageLink = explode('?pubId', $wb_new_api_thumbnail);
   	}
   	
	if( stripos($imageLink[0], 'https://') !== false ){
   		$imageLink[0] = str_replace('https://', 'http://', $imageLink[0]);
	}
	$wb_rss_image_directory = '/home/' . $wb_ent_options['serveruser'] . '/' . $wb_ent_options['publicdir'] . '/wp-content/themes/enterprise/images/rssStills/';
	if (!file_exists($wb_rss_image_directory)) {
		mkdir($wb_rss_image_directory);
	}			
	
	copy($videoStill, '../images/rssStills/'.$row['ID'].'_still.jpg');
	$imageSize = filesize('/home/'.$wb_ent_options['serveruser'].'/'.$wb_ent_options['publicdir'].'/wp-content/themes/enterprise/images/rssStills/'.$row['ID'].'_still.jpg');
	
	if( trim($imageSize) == '' ){
   		$imageSize = 0;
	}

	//momentary fix
	$pos = strpos($downloadLink, 'http://brightcove04.brightcove.com/media/');
	if($pos !== false){
   		$downloadLink = '';
	}

	$post_title 	= str_replace('&trade;', '', $row['post_title'] );
	$post_content 	= str_replace('&trade;', '', $row['post_content'] );
	$post_content 	= str_replace('&mdash;', '', $post_content );
	$post_title 	= strip_tags($post_title);
	$post_title 	= limit($post_title,60);
	$post_content 	= strip_tags($post_content);
	
	//removeShortCode
	$post_content 	= str_replace($strReplaceSearch, $strReplaceChars, removeShortCode('VIDEO', $post_content) );
	$post_content 	= limit($post_content,200);
	$permalink 		= get_permalink( $row['ID'] ).'?utm_source=mailchimp&utm_medium=newsletter&utm_content='.urlencode($post_title).'&utm_name=ASQTVNewsletter'.date('Y-m-d'); 
?>
		<item>
		   <category><?php echo $postCounter; ?></category>
		   <?php 
		   
		      //getting the pubdate to RFC822
		      $postId = $row['ID'] ;
		      
		      $sql2 = "SELECT DATE_FORMAT(post_date,'%a, %d %b %Y %T') AS rfcpubdate 
		      FROM wp_posts 
		      WHERE ID = $postId "
		      ;
		   
		      $result2 = mysql_query($sql2) ;
		      
		      $rfcpubdate = mysql_fetch_row($result2);
		      
		      foreach($rfcpubdate as $date){
		      
		         echo "<pubDate>$date CST</pubDate>" ;
		      
		      }
		   ?>
		
			<title><![CDATA[<?php echo $post_title; ?>]]></title>
		
			<link><![CDATA[<?php echo $permalink ; ?>]]></link>
		
			<description><![CDATA[<?php echo str_replace('"', '', strip_tags($post_content)) ; ?>]]></description>
		
			<guid isPermaLink='true'><![CDATA[<?php echo $permalink ; ?>]]></guid>
		
			<media:content url='<?php echo ( trim($imageLink[0]) != '') ? $imageLink[0]: '' ; ?>' type='image/jpg' height='144' width='256' medium='image' >
		
				<media:title type="html"><![CDATA[<?php echo $post_title; ?>]]></media:title>
		
				<media:description type="html"><![CDATA[<?php echo $post_content; ?>]]></media:description>
		
			</media:content>
		
			<enclosure url="<?php echo ( trim($imageLink[0]) != '') ? $imageLink[0]: '' ; ?>" length="<?php echo $imageSize; ?>" type="image/jpg" />
		
		</item>
<?php      
		$postCounter++;
	}
?>
	</channel>
</rss>
<?php
   function removeShortCode($codeTag, $string){
      return preg_replace("/\[".$codeTag." (.*)\/\]/i", "", $string);
   }

   	function limit($str,$length,$end='...',$encoding=null){
    	if(!$encoding) $encoding = 'UTF-8';
       		$str = trim($str);
       		$len = mb_strlen($str,$encoding);
       	if($len <= $length) return $str;
       	else {
        	$return = mb_substr($str,0,$length,$encoding);
           	return (preg_match('/^(.*[^\s])\s+[^\s]*$/', $return, $matches) ? $matches[1] : $return).$end;
       	}
   	}
   
   // Generate Authentication Token //
	function wb_get_auth_token(){
	   	GLOBAL $bc_account_id;
	   	$data 			= array();
	   	$id 			= "559779e8e4b072e9641b841d"; //from settings not sure if this is needed
	   	$client_id     	= "07aa7b60-2e1c-4775-8bc5-e3f5220d2d8d"; //API From settings
	   	$client_secret 	= "6a8-bE1KqdzoAbxupjTiDpEriPI4lZHUDS2mxDfU697lWuCRJNRJlJchL-ZbUCsPmC3lj-r8uram6vpPKldFmg"; //API From settings
	   	$auth_string   	= "{$client_id}:{$client_secret}";
	   	$request       	= "https://oauth.brightcove.com/v4/access_token?grant_type=client_credentials";
	   	$ch            	= curl_init($request);
	   	curl_setopt_array($ch, array(
	   			CURLOPT_POST           => TRUE,
	   			CURLOPT_RETURNTRANSFER => TRUE,
	   			CURLOPT_SSL_VERIFYPEER => FALSE,
	   			CURLOPT_USERPWD        => $auth_string,
	   			CURLOPT_HTTPHEADER     => array('Content-type: application/x-www-form-urlencoded'),
	   			CURLOPT_POSTFIELDS => $data
	   	));
	   	$response = curl_exec($ch);
	   	curl_close($ch);
	   	// Check for errors
	   	if ($response === FALSE) {
	   		die(curl_error($ch));
	   	}
	   	// Decode the response
	   	$responseData = json_decode($response, TRUE);
	   	$access_token = $responseData["access_token"];
	   	return $access_token;
	}
   	//Return information both poster and thumbnail
   	function wb_get_video_images( $video_id ){
	   	GLOBAL $bc_account_id, $access_token;
	   	
	   	//$access_token = wb_get_auth_token();
	   	
	   	$url = "https://cms.api.brightcove.com/v1/accounts/$bc_account_id/videos/$video_id/images";
	   	$header = array();
	   	$access = 'Authorization: Bearer ' . $access_token;
	   	$header[] = 'Content-Type: application/json';
	   	$header[] = $access;
	   	
	   	$ch = curl_init();
	   	curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
	   	curl_setopt($ch, CURLOPT_URL, $url);
	   	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	   	$response= curl_exec($ch);
	   	curl_close($ch);
	   	return $response;
	}
   
   	//Returns Video Information JSON
   	function wb_get_video_information( $video_id ){
	   	GLOBAL $bc_account_id, $access_token;
	   	
	   	//$access_token = wb_get_auth_token();
	   	
	   	$url = "https://cms.api.brightcove.com/v1/accounts/$bc_account_id/videos/$video_id";
	   	$header = array();
	   	$access = 'Authorization: Bearer ' . $access_token;
	   	$header[] = 'Content-Type: application/json';
	   	$header[] = $access;
	   	
	   	$ch = curl_init();
	   	curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
	   	curl_setopt($ch, CURLOPT_URL, $url);
	   	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	   	$response= curl_exec($ch);
	   	curl_close($ch);
	   	
	   	return $response;
   	}
?>