<?php
/**
 * filename: userExport.php
 * description: this will be the default template to be used to export users from wordpress
 * author: Jullie Quijano
 * date created: 2017-03-27
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: User Export Page
 */
global $wpdb;
$wb_ent_options = get_option('wb_ent_options');
//echo '$wb_ent_options is '.print_r($wb_ent_options, true);

$ipRestrictionArray = explode(',', $wb_ent_options['workerbeeip'] );

$allowedIps = array();
foreach($ipRestrictionArray as $currentIp){
    if(trim($currentIp) != '' ){
        $allowedIps[] = trim($currentIp);
    }
    
}

$userIp = trim($_SERVER['REMOTE_ADDR']);

if( !in_array($userIp, $allowedIps) || trim($wb_ent_options['workerbeeip']) == '' )
{
//echo 'Please check back later. Thanks.';
exit;
}
/*
 * Instructions:
 *      --update the $filetype (csv, xls)
 *      --update the user roles you would like to export
 *      --update the $default_export_fields (fields right from the table). The column name will be the actual name/title of the column for that field. The default will be:
            $default_export_fields = array(
               'user_login' => 'column_name',    
               'user_pass' => 'column_name',    
               'user_nicename' => 'column_name',    
               'user_email' => 'column_name',    
               'user_url' => 'column_name',    
               'user_status' => 'column_name',    
               );
 *      --update the $default_export_meta (any field from the usermeta table). The column name will be the actual name/title of the column for that field. Sample will be:
                $default_export_meta = array(    
                    'first_name' => 'First Name',
                    'last_name' => 'Last Name',
                    'nickname' => 'Nickname',    
                    'billing_company' => 'Company',
                    'billing_country' => 'Country',
                    'billing_address_1' => 'Billing Address',
                    'billing_address_2' => 'Billing Address 2',
                    'billing_city' => 'City',
                    'billing_postcode' => 'Postal Code',
                    'billing_state' => 'State',
                    'billing_phone' => 'Phone Number',
                );
 * 
 */

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$filetype = 'csv';
$userRoles = array("wb_cdel_ambassador");
/*
* to get a list of all users 
* /
global $wp_roles;
$all_roles = $wp_roles->roles;
print_r($all_roles) ; 
/* */
$default_export_fields = array(
    'ID' => 'ID',
    'user_login' => 'Username',    
    'user_email' => 'Emaill Address',
    
    );

    //the next ones are usermeta
$default_export_meta = array(    
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//process usermeta
$default_export_meta_keys = array_keys($default_export_meta);
$meta_key_query_string = '';
if( count($default_export_meta_keys) > 0 ){
    
    $meta_key_query_array = array();
    foreach($default_export_meta_keys as $current_meta_key){
        $meta_key_query_array[] = '( meta_key = "'.$current_meta_key.'" )';
    }
    $meta_key_query_string = ' AND ('.implode(' OR ', $meta_key_query_array).' )';
}

$column_names = array_values($default_export_fields);
if (($key = array_search('ID', $column_names)) !== false) {
    unset($column_names[$key]);
}


//the arguments can be further modified, see https://codex.wordpress.org/Function_Reference/get_users
$all_users_args = array(		
	'role__in'     => $userRoles,
	'orderby'      => 'email',
	'order'        => 'ASC',
	'fields'       => array_keys($default_export_fields),        
);
//echo '$all_users_args is '.print_r($all_users_args, true);
/* */
$all_users = get_users( $all_users_args );
//echo '$all_users is '.print_r($all_users, true);
$wpdb->flush();

$final_all_users = array();
$user_counter = 0;
foreach( $all_users as $current_user ){
    $current_user = (array)$current_user;
    $current_user_id = $current_user['ID'];
    unset($current_user['ID']);
    $final_all_users[$user_counter] = $current_user;
    $meta_values_array = array();
    
    //echo '$current_user is '.print_r($current_user, true);
    
    $current_user_query = $wpdb->get_results('SELECT meta_key,meta_value FROM wp_usermeta WHERE user_id = '.$current_user_id.$meta_key_query_string, ARRAY_A);    
    
    //echo 'query is is '.'SELECT meta_key,meta_value FROM wp_usermeta WHERE user_id = '.$current_user['ID'].$meta_key_query_string;
    //echo '$current_user_query is '.print_r($current_user_query, true);
    
    foreach($current_user_query as $current_meta){     
        $final_all_users[$user_counter][$current_meta['meta_key']] = $current_meta['meta_value'];
        if( !in_array( $default_export_meta[$current_meta['meta_key']], $column_names) ){
            array_push($column_names, $default_export_meta[$current_meta['meta_key']]);
        }
    }
    
    
    $user_counter++;
}

//echo '$column_names is '.print_r($column_names, true);
//echo '$final_all_users is '.print_r($final_all_users, true);


header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename='.$wb_ent_options['clientcode'].'_'.date('Ymj').'.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, $column_names);
foreach($final_all_users as $current_user){
    fputcsv($output, $current_user);
}
/**/
?>