<?php
/**
 * filename: hub-splash.php
 * description: This will be the template to use for the AANA Hub Splash page
 * author: Jullie Quijano
 * date created: 2020-06-14
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: Hub - Splash
 */
define('DONOTCACHEPAGE', true);
global $postId, $wb_ent_options, $current_user;

$wb_page_id 		= get_the_ID();
$wb_page_title 		= get_the_title( $wb_page_id );
$wb_page_content	= get_post( $wb_page_id );
$wb_content_output 	= apply_filters( 'the_content', $wb_page_content->post_content );
$wb_all_meta_data 	= get_metadata("post", $wb_page_id);

//get_header('hub');
get_header("hub");
if ($wb_ent_options['sharetype'] == 'floating' && $wb_ent_options['hasshare'] && function_exists(sharebar)) {
	if ( !in_category( $wb_ent_options['videocats']['howto'] )){
		if (  !in_category($wb_ent_options['videocats']['members']) ){
			sharebar();
		}
	}
}

$banner_image = get_the_post_thumbnail_url($postId, 'full');

?>
<div id="wb_ent_content" class="clearfix row-fluid" style="padding: 0;">
    <div id="wb_ent_main" class="span8 clearfix" role="main" style="width: 100% !important;">
		<h1 id="hub-title"><?php echo $wb_page_title; ?></h1>
		<div id="hub-page-content">
			<?php echo $wb_content_output; ?>
		</div>	
        <div id="aana-hub">
        
        <!-- START HTML CODE HERE -->
            <div class="clearfix row-fluid">
            	<?php if ( $wb_all_meta_data["wb_live_hub_banner_upload_url"][0] != "" ){?>
				<div class="wb-ent-event-hub-banner">
					<a href="<?php echo urldecode($wb_all_meta_data["wb_live_hub_banner_link"][0]); ?>">
						<img src="<?php echo urldecode($wb_all_meta_data["wb_live_hub_banner_upload_url"][0]); ?>" alt="ANNA Splash Screen Hub" />
					</a>
                </div>
				<?php } ?>
				
				<?php if ( $wb_all_meta_data["wb_live_hub_show_buttons"][0] == "yes" ){ ?>
				<div class="wb-ent-sched-btn-group">
					<?php if ( $wb_all_meta_data["wb_live_hub_left_button_text"][0] ){ ?>
					<a href="<?php echo urldecode($wb_all_meta_data["wb_live_hub_left_button_link"][0]); ?>"><button type="button" class="btn btn-primary wb-ent-aana-exhibitor-showcase-btn"><?php echo $wb_all_meta_data["wb_live_hub_left_button_text"][0]; ?></button></a>
					<?php } ?>
					<?php if ( !is_user_logged_in() ){ ?>
					<a href="<?php echo urldecode($wb_all_meta_data["wb_live_hub_center_button_link"][0]); ?>"><button type="button" class="btn btn-primary wb-ent-aana-purchase-pass-btn"><?php echo $wb_all_meta_data["wb_live_hub_center_button_text"][0]; ?></button></a>
					<a href="<?php echo urldecode($wb_all_meta_data["wb_live_hub_right_button_link"][0]); ?>"><button type="button" class="btn btn-primary wb-ent-aana-click-here-txt-link"><?php echo $wb_all_meta_data["wb_live_hub_right_button_text"][0]; ?></button></a>
					<?php 
            			}else{ 
							?>
								<p id="wb-hub-thank-purchase"><?php echo $wb_all_meta_data["wb_live_hub_welcome_text"][0]; ?></p>
					<?php } ?>
				</div>
				<?php } ?>
				
				<div class="wb-ent-events-schedule-area" id="schedule-area">
					<h2 id="hub-region">Choose Your Region</h2>
					<div class="row-fluid">
					    <div class="span6 hub-region" id="hub-asia">
					    	<a href="#">
					    		<p>Asia</p>
					    		<img/>
					    	</a>
					    </div>
					    <div class="span6 hub-region" id="hub-europe">
					    	<a href="#">
					    		<p>Europe<br/>Middle East<br />Africa</p>
					    		<img/>
					    	</a>
						</div>
					</div>		
					<div class="row-fluid">
						<div class="span6 hub-region" id="hub-india">
							<a href="#">
								<p>India</p>
						    	<img/>
						    </a>
					 	</div>
					    <div class="span6 hub-region" id="hub-america">
							<a href="#">
								<p>North & South<br />America</p>
						    	<img/>
						    </a>
						</div>
					</div>					
				</div>
			</div>
        	<!-- END HTML HERE -->
        </div>
	<!--</div>  end #main -->
	<!-- SumoSelect -->
    <style>
    	#schedule-area .row-fluid{
    		margin-bottom: 22px;
    	}
    	.hub-region{
    		position: relative;
    		max-width: 502px;
    	}
    	.hub-region p{
    		position: absolute; 
		    top: 42%;
		    left: 50%;
		    transform: translate(-50%, -50%);
		    font-style: normal;
		    font-weight: bold;
		    font-size: 24px;
		    color: #FFFFFF;
		    font-family: 'Montserrat', sans-serif;
		    text-align: center;
    	}
    	.hub-region a img{
    		display: block;
		  	-moz-box-sizing: border-box;
		  	box-sizing: border-box;
		  	width: 100%;
    		height: 145px;
    	}
    	#hub-asia a img{
    	  	background: url(/wp-content/uploads/2020/08/Rectangle-47-1.png) no-repeat;
		}
		#hub-europe a img{
    	  	background: url(/wp-content/uploads/2020/08/Rectangle-47-2.png) no-repeat;
		}
		#hub-india a img{
    	  	background: url(/wp-content/uploads/2020/08/Rectangle-47-3.png) no-repeat;
		}
		#hub-america a img{
    	  	background: url(/wp-content/uploads/2020/08/Rectangle-47-4.png) no-repeat;
		}
		#hub-asia a img:hover{
    		background-image: url(/wp-content/uploads/2020/08/Rectangle-47.png) !important;
    	}
    	#hub-europe a img:hover{
    		background-image: url(/wp-content/uploads/2020/08/Rectangle-47-9.png) !important;
    	}
    	#hub-india a img:hover{
    		background-image: url(/wp-content/uploads/2020/08/Rectangle-47-10.png) !important;
    	}
    	#hub-america a img:hover{
    		background-image: url(/wp-content/uploads/2020/08/Rectangle-47-11.png) !important;
    	}
    </style>
    <script type="text/javascript" src="<?php echo WB_VH_DIR_URL . 'assets/js/aana-hub.js'; ?>"></script>
	<link type="text/css" rel="stylesheet" href="<?php echo WB_VH_DIR_URL . 'assets/css/aana-hub.css';?>" />
	<link type="text/css" rel="stylesheet" href="<?php echo WB_VH_DIR_URL . 'assets/css/fontawesome-free-5.13.1-web/css/all.css' ?>" />
<?php //get_sidebar(); // sidebar 1  ?>

</div> <!-- end content -->
<script type="text/javascript">
	$(document).ready(function () {
		<?php 
			if ( $wb_all_meta_data["wb_live_hub_show_search_sidebar"][0] == 'no' ){
				echo " $('#searchDiv').remove();";
			}
			if ( $wb_all_meta_data["wb_live_hub_show_video_library_sidebar"][0] == 'no' ){
				echo " $('#side-nav #vid_topic').remove();";
			}
			if ( $wb_all_meta_data["wb_live_hub_show_side_banner"][0] == 'no' ){
				echo " $('#side-nav .sidebar-banner').remove();";
			}
			if ( $wb_all_meta_data["wb_live_hub_show_podcast_library_sidebar"][0] == 'no' ){
				echo " $('#side-nav #podcast_topic').remove();";
			}
			if ( $wb_all_meta_data["wb_live_hub_show_subscribe_sidebar"][0] == 'no' ){
				echo " $('#side-nav #subscribe-link').remove();";
			}
			if ( $wb_all_meta_data["wb_live_hub_show_wb_nav"][0] == 'no' ){
				echo " $('#wb-nav').remove();";
			}
		?>
	});
</script>    
<?php
get_footer();
?>