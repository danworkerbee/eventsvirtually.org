<?php
/**
 * filename: index.php
 * description: 
 * author: Jullie Quijano
 * date created: 2014-03-25
 * 
 */
$wb_ent_options = get_option('wb_ent_options');

$ipRestrictionArray = explode(',', $wb_ent_options['workerbeeip'] );

$allowedIps = array();
foreach($ipRestrictionArray as $currentIp){
    if(trim($currentIp) != '' ){
        $allowedIps[] = trim($currentIp);
    }
    
}

$userIp = trim($_SERVER['REMOTE_ADDR']);

if( ($wb_ent_options['devmode'] && !in_array($userIp, $allowedIps)) || trim($wb_ent_options['workerbeeip']) == '' )
{
echo 'Please check back later. Thanks.';
exit;
}  
// IMPORTANT
// change the permalink in wordpress to the post name  /%postname%
// category base : category
// tag base : keyword

if( $wb_ent_options['frontpage']['type'] == 'apfhome' ){
   header('Location: '.get_site_url().'/'.$wb_ent_options['homelink']); 
}
elseif( $wb_ent_options['frontpage']['type'] == 'recentepisode' ){
	$currentVideo = wb_get_most_recent($wb_ent_options['videocats']['webcast']);
	$video = wb_get_post_details($currentVideo);
	header('Location: '.get_site_url().'/'.$video['postName']);
}
elseif( $wb_ent_options['frontpage']['type'] == 'recentfeature' ){
	$currentVideo = wb_get_most_recent($wb_ent_options['videocats']['feature']);
	$video = wb_get_post_details($currentVideo);
	header('Location: '.get_site_url().'/'.$video['postName']);
}
elseif( $wb_ent_options['frontpage']['type'] == 'recentvideo' ){
	$currentVideo = wb_get_most_recent($wb_ent_options['videocats']['library']);
	$video = wb_get_post_details($currentVideo);
	header('Location: '.get_site_url().'/'.$video['postName']);
}
elseif( $wb_ent_options['frontpage']['type'] == 'selectedPage' ){
	header('Location: '.get_site_url().'/?p='.$wb_ent_options['frontpage']['pageId']);
}
elseif( $wb_ent_options['frontpage']['type'] == 'customurl' ){
	header('Location: '.$wb_ent_options['frontpage']['customurl']);
}
else{
   header('Location: '.get_site_url().'/'.$video['postName']);
}
?>