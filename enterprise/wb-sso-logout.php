<?php
/**
 * filename: wb-sso-logout.php
 * description: this template is used to logout a user from WordPress
 * author: Jullie Quijano
 * date created: 2018-06-11
 * 
 * 
 * @package WordPress
 * @subpackage Enterprise

 * Template Name: SSO Logout
 */
global $wb_ent_options;

wp_logout();

header('location: /');
