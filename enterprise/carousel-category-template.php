<?php
/**
 * filename: category.php
 * description: this will be the template to be used to display categories
 * author: Jullie Quijano
 * date created: 2014-03-25
 *
 *
 * @package WordPress
 * @subpackage Enterprise
 
 * Template Name: Category
 */
global $wb_ent_options, $privatePostsIds, $unlistedPostsIds, $moretext, $carousel_term_meta;
get_header();
?>
<div id="wb_ent_content" class="clearfix row-fluid">
    <div id="wb_ent_main" class="span8 clearfix" role="main" style="border: 0px solid black;">
        <div id="viewing-tips">

            <?php 
            $videoCounter = 0;
            if (have_posts()) : 
            ?>	
            	<!-- 
                <div class="page-header"><h1 class="pagetitle"><?php _e('Videos in', 'enterprise') ?> "<?php printf(__('%s', 'enterprise'), single_cat_title()); ?>"</h1></div>
                 -->
                <div class="wb-carousel-inner">
               		<div class="item active">
                    	<img src="<?php echo $carousel_term_meta; ?>" alt="ISRI" class="img-responsive btn-block" id="carousel-image">
                    </div> <!-- end of <div class="item active"> -->                           
                 </div>
                <style>
                	.wb-carousel-inner{
                		margin-bottom: 15px;
                	}
                	.wb-carousel-inner div img#carousel-image{
                		max-width: 834px;
                		width: 100%;
                	}
                	#wb-channel-content li{
					    margin-left: 2.1%;
				        clear: unset;
				        
					}
					#wb-channel-content li:nth-child(3n+1) {
					    margin-left: 0px !important;
					    clear: both;
					}
					
					#wb-channel-content li div.thumbnail{
						padding: 0px;
					}
					ul#wb-channel-content li div a h3{
					    color: #465778;
					    font-family: 'ITCFranklinGothicW01-Md 812698',arial,san-serif !important;
					    font-size: 21px;
					    font-weight: 500;
					    padding: 0 15px;
					    margin-top: 5px;
					}
					ul#wb-channel-content li div.thumb-content{
						margin-left: 0px;
					}
					li.wb-subpage-block-wrapper{
						border: 1px solid #d8e3ee;
    					border-radius: 0px;
    				    min-height: 332px;
					}
					ul#wb-channel-content li div p{
						font-size: 16px;
					    line-height: 24px;
					    padding: 0px 15px;
					}
					ul#wb-channel-content li div p a.label{
						white-space: pre-line;
					}
					
					@media (max-width: 767px){
						ul#wb-channel-content li.wb-subpage-block-wrapper{
							margin-left: 0px;
						}
					}
                </style>
                
                
                <div id="video-results" class="video-list-1">
                    
                <?php 
                  $wb_current_category = get_category( get_query_var( 'cat' ) );
                  $wb_current_cat_id = $wb_current_category->cat_ID;  //Current page category ID
                			        
                  $wb_current_category_description = explode(',',       $wb_current_category->description); 
                //Get the image stored in the description (if needed)
                  $wb_category_carousel_image = $wb_current_category_description[0];
                			               
                  if ( $wb_current_cat_id == wb_how_to_videos_category ){
                  	//if (  ! is_user_logged_in() ) {
                  	//	wp_redirect( '/home', 301 );
                  	//	exit;
                  	//}
                 	include ( get_stylesheet_directory() .'/wb-advance-search.php' );
                  }
                ?>
                    
                    <ul class="thumbnails" id="wb-channel-content">                  

                        <?php while (have_posts()) : the_post(); ?>
                            <?php
                            $post_id = $wp_query->post->ID;
                            if( ( !is_user_logged_in() ) && (in_array( $post_id,  $privatePostsIds) || in_array( $post_id,  $unlistedPostsIds)  ) ){
                               continue; 
                            }
                            $videoCounter++;                            
                            $video = wb_get_post_details($post_id);
                            ?>

                            <?php
                            $image = '<img src="' . $video['smlThumb'] . '" alt="' . $video['title'] . '" />';
                            $postContent = wb_format_string($video['desc'], false, false, 170, '... <a href="' . get_permalink() . '"><span class="more-link">'.$moretext.'</span></a>');
                            ?>



                            <li class="span4 wb-subpage-block-wrapper">
                                <div class="thumbnail no-style span12">
                                    <a href="<?php the_permalink(); ?>"> <?php echo $image; ?> </a>

                                </div>
                                <div class="span12 thumb-content">
                                    <a href="<?php the_permalink(); ?>"><h3 class="search_title_unlock"><?php the_title(); ?></h3></a>
                                    <p><?php echo $postContent; ?></p>
                                    <?php
                                    if (count($video['tags']) > 0 && is_array($video['tags'])) {                                        
                                        ?>
                                    <p class="tags">
                                    <span class="tags-title"><?php _e('Tags:', 'enterprise'); ?></span>
                                    <?php
                                    /*11.27.2014*/
                                    $count = 0;
                                    $tagTotal = count($video['tags']);
                                    
                                    if($wb_ent_options['videolistinfo']['keywordlimit']){$wb_keyword_limit = $wb_ent_options['videolistinfo']['keywordlimit'];} 
                                      else {$wb_keyword_limit = 0;}
                                    foreach ($video['tags'] as $currentTag) {
                                        $count++;                                        
                                        if($count <= $wb_keyword_limit && $wb_keyword_limit != 0){
                                        ?>
                                        <a rel="tag" href="/keyword/<?php echo $currentTag->slug; ?>" class="label"><?php echo $currentTag->name; ?><?php if($tagTotal > 1 && $count < $tagTotal){echo ',';}?></a>
                                        <?php
                                        } else if($wb_keyword_limit == 0){
                                        ?>
                                        <a rel="tag" href="/keyword/<?php echo $currentTag->slug; ?>" class="label"><?php echo $currentTag->name; ?><?php if($tagTotal > 1 && $count != $tagTotal){echo ',';}?></a>
                                        <?php
                                        }
                                        
                                    }
                                    $count = 0;
                                    /*end 11.27.2014*/
                                    /*
                                    foreach ($video['tags'] as $currentTag) {
                                    ?>
                                        <a rel="tag" href="/keyword/<?php echo $currentTag->slug; ?>" class="label"><?php echo $currentTag->name; ?></a>
                                        <?php
                                        }
                                        */
                                        ?>

                                    </p> 
                                        <?php
                                    }
                                    ?>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                </div>
                <h1 class="center" id="wb-advance-search-novideo" style="display: none;"><?= _e('No videos found.', 'enterprise') ?></h1>
                <?php
                if ( $wb_current_cat_id != wb_how_to_videos_category ){
	                // Find total number of pages
	                $pageNumber = (get_query_var('paged')) ? get_query_var('paged') : 1;
	                $theQuery = $wp_query->request;
	                $theQuery = substr_replace($theQuery, "", strpos($theQuery, "LIMIT"), strlen($theQuery));
	                $get_max_pages = $wpdb->query($theQuery);
	                $max_pages = ceil(($get_max_pages) / $wb_ent_options['videolistinfo']['vidsperpage']);
	                if ($max_pages > 1) {
	                    ?>
	                    <div class="pageNav">
	                        <div id="paginationLeft">&nbsp;<?php previous_posts_link('&laquo; Previous') ?></div>
	                        <div id="paginationMid">Page <?= $pageNumber ?> <?= _e('of', 'enterprise') ?> <?= $max_pages ?></div>
	                        <div id="paginationRight"><?php next_posts_link('Next &raquo;') ?>&nbsp;</div>
	                    </div>
	                    <?php
	                }
                }
            endif;
            if( $videoCounter <= 0 ){
            ?>
               <h1 class="center"><?= _e('No videos found.', 'enterprise') ?></h1>
               <p><?= _e('Try a different search or', 'enterprise') ?>:</p>

                <?php
                $query = $wpdb->query("SELECT post_name FROM wp_posts
                    WHERE post_type='post'
                    AND post_status='publish'
                    ORDER BY RAND()
                    LIMIT 1");

                foreach ($query as $row) {
                    $videoname = $row['post_name'];
                }
                ?>
                <div id="linkList404">
                    <p>&raquo; <a href="<?php echo get_site_url().'/' . $videoname ?>"><?php _e('Watch a Random Video', 'enterprise') ?></a></p>
                    <p>&raquo; <a href="<?php echo get_site_url().'/' ?>"><?php _e('Go Back to Library', 'enterprise') ?></a></p>
                    <p>&raquo; <a href="<?php echo get_site_url().'/' ?>viewing-tips"><?= _e('Read Viewing Tips', 'enterprise') ?></a></p>
                    <p>&raquo; <a href="<?php echo get_site_url().'/' ?>contact"><?= _e('Contact Us', 'enterprise') ?></a></p>
                </div>            
                <p>&nbsp;</p>
            <?php 
            }
            ?>
        </div>
    </div>
    <style>
    	#wb-channel-content li div a img{
    		max-width: 100%;
		    max-height: 100% !important;
		    width: 100%;
    	}
    </style>
    <?php
    get_sidebar();
    ?>
    </div>  <!--  wb_ent_content end -->
    <?php 
    get_footer();
    ?>